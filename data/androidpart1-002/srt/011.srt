1
00:00:00,000 --> 00:00:10,000
[MUSIC]

2
00:00:14,729 --> 00:00:18,804
Now Eclipse comes with a debugger that
gives developers powerful tools for

3
00:00:18,804 --> 00:00:21,920
examining the internal state
of a running application.

4
00:00:21,920 --> 00:00:26,170
And this can be useful of course, when
you have a bug in your application, and

5
00:00:26,170 --> 00:00:28,410
you're not sure exactly what's causing it.

6
00:00:28,410 --> 00:00:32,690
So let's use the Eclipse debugger
to examine an application.

7
00:00:35,040 --> 00:00:39,390
Here, I'm showing a simple Android
application that I've named The Answer.

8
00:00:41,140 --> 00:00:45,684
And this app is supposed to show you
the answer to life, the universe and

9
00:00:45,684 --> 00:00:51,015
everything, which as The Hitchhiker's
Guide to the Universe tells us, is 42.

10
00:00:51,015 --> 00:00:52,045
As you can see,

11
00:00:52,045 --> 00:00:57,047
however, instead of displaying 42,
this app says, we may never know.

12
00:00:57,047 --> 00:01:00,350
So clearly there’s a bug
in here somewhere.

13
00:01:00,350 --> 00:01:02,750
Let’s use the debugger
to track that bug down.

14
00:01:03,920 --> 00:01:05,413
So here we are in Eclipse.

15
00:01:05,413 --> 00:01:10,710
First, I’ll open up the answer
application and show you its source code.

16
00:01:10,710 --> 00:01:14,610
Now the truth is that this is
a pretty silly program, and

17
00:01:14,610 --> 00:01:17,590
the bug will be fairly obvious
once you've looked at the code.

18
00:01:18,600 --> 00:01:20,790
But my goal here is to
show you the debugger.

19
00:01:20,790 --> 00:01:25,290
So just between us, let's pretend that
we don't really know what the bug is.

20
00:01:26,460 --> 00:01:29,485
Now you can see that there's
an array called answers,

21
00:01:29,485 --> 00:01:33,377
through which the program will search,
looking for the answer 42.

22
00:01:33,377 --> 00:01:39,840
There's the onCreate method that is the
starting point when the class is executed.

23
00:01:40,870 --> 00:01:45,380
Inside onCreate, there's a call
to a method called FindAnswer.

24
00:01:45,380 --> 00:01:47,280
And FindAnswer is listed down below.

25
00:01:49,260 --> 00:01:52,395
Now FindAnswer is supposed to
search the answers array, and

26
00:01:52,395 --> 00:01:56,530
return the number 42 if it's in there or
minus one if it's not.

27
00:01:58,120 --> 00:02:00,710
The program stores
FindAnswer's return value,

28
00:02:00,710 --> 00:02:03,790
and then uses it to compute
a string called output.

29
00:02:05,240 --> 00:02:09,110
And finally,
the program displays the output string.

30
00:02:09,110 --> 00:02:12,080
So since I don't know where the bug is,
I'll just,

31
00:02:12,080 --> 00:02:15,560
I decide to use the debugger
to debug the program.

32
00:02:15,560 --> 00:02:18,370
And the first thing I'll do
then is to set a break point

33
00:02:18,370 --> 00:02:22,690
that stops the program when it
enters the onCreate method.

34
00:02:22,690 --> 00:02:27,470
Now instead of running the program, I'll
debug it by right-clicking the project

35
00:02:27,470 --> 00:02:32,745
name, selecting Debug As, and
then selecting Android Application.

36
00:02:32,745 --> 00:02:38,084
I'll click OK, and when the application
reaches onCreate, it stops and

37
00:02:38,084 --> 00:02:43,695
shows me the debug view with the current
line highlighted in the source code.

38
00:02:43,695 --> 00:02:48,257
Now at this point,
I'll step through the code line by line,

39
00:02:48,257 --> 00:02:50,857
examining the program's state.

40
00:02:50,857 --> 00:02:54,512
To step forward,
I select Step Over from the toolbar,

41
00:02:54,512 --> 00:02:57,060
which advances the program one line.

42
00:02:59,380 --> 00:03:01,720
I'm now at the line that
calls find answer, and

43
00:03:01,720 --> 00:03:04,410
stores the return result
in the variable val.

44
00:03:06,310 --> 00:03:10,870
Once I step over this line, you'll see
that the value of the variable val is now

45
00:03:10,870 --> 00:03:13,690
minus ten, which is already a problem.

46
00:03:13,690 --> 00:03:15,254
It should have been 42.

47
00:03:16,450 --> 00:03:19,690
So the problem is certainly down
there in the FindAnswer method, so

48
00:03:19,690 --> 00:03:22,920
let's look more closely at FindAnswer.

49
00:03:22,920 --> 00:03:25,987
And to do that,
I'll put a break point at FindAnswer, and

50
00:03:25,987 --> 00:03:27,688
then restart the application.

51
00:03:45,077 --> 00:03:46,035
Now this time,

52
00:03:46,035 --> 00:03:51,269
when the application stops at onCreate,
I'll hit the Step Over button, which will

53
00:03:51,269 --> 00:03:56,370
let the application continue executing
until the FindAnswer break point is hit.

54
00:03:57,850 --> 00:04:00,061
I can step forward in this method and

55
00:04:00,061 --> 00:04:04,939
examine the values of variables to
determine that 42 is indeed in the array.

56
00:04:12,668 --> 00:04:18,058
And that FindAnswer sees it but
considers it the wrong answer.

57
00:04:18,058 --> 00:04:22,495
Anyway, I'm sure you've figured
out a long ago, the problem is

58
00:04:22,495 --> 00:04:28,160
that I'm accidentally checking for
not equal, rather than for equals, equals.

59
00:04:28,160 --> 00:04:33,460
So I'll fix that and start over, and
this time we'll see the correct behavior.

60
00:04:34,490 --> 00:04:37,400
Now just to make sure,
let's go back and fire up the answer.

61
00:04:41,960 --> 00:04:46,579
And yes, the answer to the life,
the universe and everything is again 42.

62
00:04:47,720 --> 00:04:50,970
Now I think debuggers help most when
you have a reasonably good idea of

63
00:04:50,970 --> 00:04:55,660
where your problem is so
you can focus in on a few lines of code.

64
00:04:55,660 --> 00:04:59,280
But in many other situations,
that kind of low level instruction by

65
00:04:59,280 --> 00:05:04,890
instruction examination that debuggers
are so good at supporting is less helpful.

66
00:05:04,890 --> 00:05:08,180
Instead, it may be better to use
more general tools, that in,

67
00:05:08,180 --> 00:05:12,530
instrument your system, and let you
monitor its behavior as it runs overtime.

68
00:05:13,530 --> 00:05:18,202
Android provides a number of
these monitoring tools in

69
00:05:18,202 --> 00:05:21,865
its Dalvik Debug Monitor Service or DDMS.

70
00:05:21,865 --> 00:05:24,910
Today, I'll talk about
four of these tools.

71
00:05:24,910 --> 00:05:29,210
A file explorer for
viewing a device's file system.

72
00:05:29,210 --> 00:05:33,130
Logcat for logging and
displaying runtime events.

73
00:05:33,130 --> 00:05:36,446
Traceview for
displaying method execution traces.

74
00:05:36,446 --> 00:05:40,985
And hierarchyview for
examining user interface layouts.

75
00:05:40,985 --> 00:05:44,348
Let's look at these tools and
see how you access them from Eclipse.

76
00:05:52,031 --> 00:05:57,072
First, let's open the DDMS perspective
which exposes a number of panes and

77
00:05:57,072 --> 00:06:00,390
buttons for
accessing the different DDMS tools.

78
00:06:01,600 --> 00:06:06,980
We now see a device pane that shows
the devices that are currently connected,

79
00:06:06,980 --> 00:06:11,796
and shows a list of the buggable
processes running on these devices.

80
00:06:11,796 --> 00:06:14,770
The first tool we'll look
at is the File Explorer.

81
00:06:15,880 --> 00:06:19,970
This view shows,
shows you the files on your file system.

82
00:06:19,970 --> 00:06:25,180
For example, here we see that this
device has a /data/data directory in

83
00:06:25,180 --> 00:06:28,040
which applications can store
their persistent data.

84
00:06:29,110 --> 00:06:34,999
This device also has
an external memory card.

85
00:06:34,999 --> 00:06:38,248
And there's some directories there for
storing music and movies, and

86
00:06:38,248 --> 00:06:38,960
other things.

87
00:06:40,210 --> 00:06:43,159
The next pane I'll talk
about shows the Logcat view.

88
00:06:44,480 --> 00:06:47,380
Android logs many events by default.

89
00:06:47,380 --> 00:06:51,690
That is, as the software runs it spits
out information about the things that

90
00:06:51,690 --> 00:06:53,660
are happening on the device.

91
00:06:53,660 --> 00:06:57,277
And this information is stored and
can be displayed in the Logcat view.

92
00:06:58,380 --> 00:07:02,240
Android also provides methods that let
developers log their own information.

93
00:07:05,140 --> 00:07:07,630
Another DDMS tool is Traceview.

94
00:07:07,630 --> 00:07:11,604
Traceview allows you to trace
the methods that your application calls.

95
00:07:15,912 --> 00:07:19,185
Let's do this for the answer
application that I showed earlier.

96
00:07:19,185 --> 00:07:22,279
So this is the source code for
the application.

97
00:07:22,279 --> 00:07:24,229
And to simplify my demonstration,

98
00:07:24,229 --> 00:07:27,155
I'll first put a break point
in the onCreate method.

99
00:07:27,155 --> 00:07:29,666
And now I'll start
debugging the application.

100
00:07:42,096 --> 00:07:45,090
Now as you can see,
it stopped at the break point.

101
00:07:46,100 --> 00:07:49,010
And now, I want to start profiling.

102
00:07:49,010 --> 00:07:50,327
So I'll open the DDMS view.

103
00:08:04,298 --> 00:08:07,470
And click on the button
that says Start Profiling.

104
00:08:08,500 --> 00:08:12,052
And now, I'll return back to the Debug
view, and resume the application.

105
00:08:18,287 --> 00:08:20,350
I'll let the program run for a while.

106
00:08:21,570 --> 00:08:23,779
And now I'll return to the DDMS view, and

107
00:08:23,779 --> 00:08:27,333
press the same button as before,
this time to stop the profiling.

108
00:08:33,474 --> 00:08:38,253
At this point Traceview will
process the trace information and

109
00:08:38,253 --> 00:08:40,333
show it to me graphically.

110
00:08:40,333 --> 00:08:44,207
The Traceview display shows two panels,
one is a Timeline view,

111
00:08:44,207 --> 00:08:48,230
showing each method,
each method call's sequence and duration.

112
00:08:57,430 --> 00:09:00,590
The other is the Profile view,
which shows statistics and

113
00:09:00,590 --> 00:09:03,180
other information about
each called method.

114
00:09:04,320 --> 00:09:06,205
Let's look for our old friend FindAnswer.

115
00:09:07,680 --> 00:09:10,760
When I click on FindAnswer
in the Profile view,

116
00:09:10,760 --> 00:09:14,640
I can see where it was called
up in the Timeline view.

117
00:09:14,640 --> 00:09:18,504
And now I can zoom in on the timeline
to see more information and

118
00:09:18,504 --> 00:09:20,003
surrounding context.

119
00:09:24,419 --> 00:09:29,157
And if I want to be more precise in
my profiling, Android also allows me

120
00:09:29,157 --> 00:09:35,570
to insert statements directly into
the program that enable targeted tracing.

121
00:09:35,570 --> 00:09:39,590
And doing this requires that you set some
permissions however for your application.

122
00:09:39,590 --> 00:09:41,620
And we haven't talked
about permissions yet, but

123
00:09:41,620 --> 00:09:43,190
we'll talk about them in a later lesson.

124
00:09:44,290 --> 00:09:49,610
And finally, on my Mac, if I Cmd + Click
on FindAnswer in the Profile pane,

125
00:09:49,610 --> 00:09:52,430
Eclipse will take me to the file
containing that method.

126
00:09:54,330 --> 00:09:59,040
The last two I'll talk about
today is the UI Hierarchy View.

127
00:09:59,040 --> 00:10:03,100
This tool allows you to analyze your
application's user interface and

128
00:10:03,100 --> 00:10:04,930
understand how it's organized.

129
00:10:04,930 --> 00:10:08,234
For example, let's look again at
the Hello Android application.

130
00:10:12,363 --> 00:10:15,193
First, let's run
the Hello Android application

131
00:10:27,270 --> 00:10:32,981
And once it's up and running, we'll
go to the Menu bar and select Window,

132
00:10:32,981 --> 00:10:37,708
then select Open Perspective,
then select Hierarchy View.

133
00:10:37,708 --> 00:10:41,810
Now if you don't see it, try selecting
Other and searching there instead.

134
00:10:43,300 --> 00:10:48,540
Now once the Hierarchy Viewer starts
we'll go to the Window panel and

135
00:10:48,540 --> 00:10:52,880
double click the entry corresponding
to the Hello Android application.

136
00:10:52,880 --> 00:10:54,680
It's the one that has
the words hello android in it.

137
00:10:56,720 --> 00:11:00,890
And double-clicking on this entry will
bring up a graphical representation of

138
00:11:00,890 --> 00:11:02,700
Hello Android's user interface,

139
00:11:03,720 --> 00:11:07,670
which like most user interfaces
is organized in a hierarchy.

140
00:11:07,670 --> 00:11:10,430
For instance,
there's an outermost application window,

141
00:11:10,430 --> 00:11:12,049
which holds the entire user interface.

142
00:11:17,100 --> 00:11:19,131
It contains two children, a title bar.

143
00:11:27,923 --> 00:11:31,420
And the large rectangular
body of the application.

144
00:11:31,420 --> 00:11:34,080
Each of these in turn
contain further elements.

145
00:11:34,080 --> 00:11:38,020
Here I'll click on the outermost window,
and I can ex,

146
00:11:38,020 --> 00:11:43,030
inspect detailed information about that
window, such as its layout parameters,

147
00:11:43,030 --> 00:11:46,100
which for example,
shows how big the window is in pixels.

148
00:11:47,230 --> 00:11:49,830
The region occupied by each
element is shown here in

149
00:11:49,830 --> 00:11:52,580
the Layout view in
the bottom right corner.

150
00:11:52,580 --> 00:11:57,897
And the same information is also
available for the title bar, and

151
00:11:57,897 --> 00:12:02,933
the same for the body, and
the text, too, that it contains.

152
00:12:02,933 --> 00:12:06,467
And that's all for today's lesson on
the Android development environment.

153
00:12:06,467 --> 00:12:11,338
Please join me next time when we'll
discuss the fundamental building blocks of

154
00:12:11,338 --> 00:12:12,871
Android applications.

155
00:12:12,871 --> 00:12:13,871
Thank you.

