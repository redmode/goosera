1
00:00:00,000 --> 00:00:10,000
[MUSIC]

2
00:00:15,483 --> 00:00:18,818
Today we're going to talk about
the Android Development Environment,

3
00:00:18,818 --> 00:00:22,439
an integrative set of tools to help you
create your own Android applications.

4
00:00:23,890 --> 00:00:27,620
In our last lesson, I introduced
you to the Android platform itself.

5
00:00:27,620 --> 00:00:31,620
I talked about how the Android platform
is a software stack for building and

6
00:00:31,620 --> 00:00:33,406
running mobile applications.

7
00:00:33,406 --> 00:00:36,605
And if you've used an Android
device to write email,

8
00:00:36,605 --> 00:00:41,650
to take photos, to get directions,
to play games and so on, then you've

9
00:00:41,650 --> 00:00:46,630
seen the Android platform in action and
you have a good idea about what it can do.

10
00:00:46,630 --> 00:00:48,517
So the next question then,

11
00:00:48,517 --> 00:00:53,120
is how do we use the Android platform
to write our own Android apps?

12
00:00:53,120 --> 00:00:56,159
Well, one part of the answer is that
you need to learn the details of

13
00:00:56,159 --> 00:00:59,550
the Android platform itself, and
we'll do that throughout this class.

14
00:01:00,850 --> 00:01:03,900
Another part of the answer though,
is that you need to learn and

15
00:01:03,900 --> 00:01:08,940
master the tools that will allow you
to make use of the platform in order to

16
00:01:08,940 --> 00:01:13,290
create those new applications, and
that's what we're going to focus on today.

17
00:01:13,290 --> 00:01:14,130
Now, the tools that I'm

18
00:01:14,130 --> 00:01:17,700
talking about are called
the Android Development Environment, and

19
00:01:17,700 --> 00:01:21,822
you should think of them as your workbench
for creating Android applications.

20
00:01:21,822 --> 00:01:26,280
And like any skilled crafts person,
the more comfortable you are with your

21
00:01:26,280 --> 00:01:31,692
tools the easier it's going to
be to produce top-quality work.

22
00:01:31,692 --> 00:01:35,640
Now in this lesson we'll
cover several topics.

23
00:01:35,640 --> 00:01:40,020
First I'll explain how you can set up
your development environment, including

24
00:01:40,020 --> 00:01:45,110
installing the Android SDK, the Eclipse
Integrated Development Environment, or

25
00:01:45,110 --> 00:01:49,440
IDE, and the Android Developer Tools.

26
00:01:49,440 --> 00:01:52,590
Then I'll show you
the Eclipse IDE in action, and

27
00:01:52,590 --> 00:01:55,740
we'll create a very simple
Android application.

28
00:01:57,160 --> 00:02:01,790
After that, I'll show you some specific
tools, such as the Android emulator, which

29
00:02:01,790 --> 00:02:05,320
allow you to run Android applications
without needing a physical device.

30
00:02:05,320 --> 00:02:09,400
And then I'll show you how to use
the Eclipse Debugger to help you

31
00:02:09,400 --> 00:02:14,130
diagnose any eventual problems that
you might have in your application.

32
00:02:14,130 --> 00:02:17,610
And lastly, I'll show you a variety
of other tools designed to

33
00:02:17,610 --> 00:02:20,039
help you further perfect
your applications.

34
00:02:21,535 --> 00:02:25,270
Now before you get started, make sure that
you have a supported operating system.

35
00:02:25,270 --> 00:02:31,390
For Windows, these include Windows XP,
Windows Vista, and Windows 7.

36
00:02:31,390 --> 00:02:38,540
For Mac, you'll need Mac OSX 10, 10.58 or
later running on an Intel-based CPU.

37
00:02:39,580 --> 00:02:41,580
Several Linux distributions
will work as well.

38
00:02:42,660 --> 00:02:45,470
Please see the Android developers
website for more information.

39
00:02:45,470 --> 00:02:51,606
Now you should also make sure that you
have the Java development kit version 6,

40
00:02:51,606 --> 00:02:57,926
so the JDK6 installed, and be aware that
JDK6 is not the latest version of Java,

41
00:02:57,926 --> 00:03:04,085
so some of you may have version 7, but
that's not fully supported by Android.

42
00:03:04,085 --> 00:03:07,320
Again, check the following website for
more information.

43
00:03:09,380 --> 00:03:10,990
So now we're ready to get started.

44
00:03:10,990 --> 00:03:17,520
So first, download and install
the Android Developer Tools or ADT bundle.

45
00:03:17,520 --> 00:03:20,530
Now I suggest you do this right now and
that way you

46
00:03:20,530 --> 00:03:25,040
can follow along with my examples while
you watch the rest of this lesson.

47
00:03:25,040 --> 00:03:28,370
You can find the ADT bundle
at the URL shown here.

48
00:03:29,530 --> 00:03:34,210
Now I'll also point out that Google
has recently released a new IDE called

49
00:03:34,210 --> 00:03:39,430
Android Studio, which they expect will
ultimately become the preferred IDE for

50
00:03:39,430 --> 00:03:41,420
doing Android development.

51
00:03:41,420 --> 00:03:45,710
However, since Android Studio is still
currently in a preview or pre-release

52
00:03:45,710 --> 00:03:51,000
state, I'm going to do my demonstrations
for this class using Eclipse.

53
00:03:51,000 --> 00:03:55,560
But you're free to, to use Eclipse or
to use Android Studio as you wish.

54
00:03:55,560 --> 00:03:59,460
Now, the ADT Bundle provides
you with several things.

55
00:03:59,460 --> 00:04:04,170
First, it has the latest Android
platform including the latest libraries,

56
00:04:04,170 --> 00:04:08,030
reusable software,
tools and documentation.

57
00:04:08,030 --> 00:04:13,010
It also includes the Eclipse IDE and
Android-specific plug-ins.

58
00:04:13,010 --> 00:04:17,110
It has additional tools that support
developing, running, testing and

59
00:04:17,110 --> 00:04:19,510
debugging your Android apps.

60
00:04:19,510 --> 00:04:23,170
And it has the latest system image for
the emulator so that you can run and

61
00:04:23,170 --> 00:04:27,020
debug your Android applications without
needing to have an actual device.

62
00:04:29,040 --> 00:04:31,770
In our last lesson,
we talked about the Android platform.

63
00:04:31,770 --> 00:04:34,900
So today, we'll talk about the other
three elements one by one.

64
00:04:34,900 --> 00:04:36,990
But before we do that,

65
00:04:36,990 --> 00:04:41,342
let's look at a very simple Android
application called Hello Android.

66
00:04:41,342 --> 00:04:44,410
And I'll be referring back to this
application throughout the lesson.

67
00:04:47,780 --> 00:04:49,640
Now here I'm showing my
phone's home screen.

68
00:04:51,660 --> 00:04:54,196
First, I'll click on the Launcher icon and

69
00:04:54,196 --> 00:04:58,960
then I'll click on the Hello Android
icon to launch the application.

70
00:04:58,960 --> 00:05:03,390
The application starts, and
displays the words hello world.

71
00:05:03,390 --> 00:05:06,300
Now that's about as simple as it gets.

72
00:05:06,300 --> 00:05:09,468
But let's walk through the process of

73
00:05:09,468 --> 00:05:14,715
writing the Hello Android
application using the Eclipse IDE.

74
00:05:14,715 --> 00:05:18,270
Now this is the Eclipse IDE
open to full screen.

75
00:05:18,270 --> 00:05:22,040
Let's start by creating a new
Android application project.

76
00:05:22,040 --> 00:05:25,450
Now one way to do that is to
go to the File menu, then New,

77
00:05:25,450 --> 00:05:28,950
and then select
Android Application Project.

78
00:05:30,160 --> 00:05:34,950
Once you do this, you'll see a series
of dialog boxes that ask you for

79
00:05:34,950 --> 00:05:37,190
information about this application.

80
00:05:38,220 --> 00:05:42,180
On the first screen,
enter HelloAndroid, all one word, for

81
00:05:42,180 --> 00:05:44,980
the Application Name and
for the Project Name.

82
00:05:46,090 --> 00:05:51,970
Where it asks for a Package Name, I'm
using course.examples.helloandroid, but

83
00:05:51,970 --> 00:05:53,470
you can use something else if you'd like.

84
00:05:54,770 --> 00:05:58,900
Now go ahead and
leave the other boxes alone, and hit Next.

85
00:05:58,900 --> 00:06:03,600
Now for this exercise, let's just
click through the next few screens.

86
00:06:03,600 --> 00:06:07,300
They control application characteristics
that we haven't discussed yet,

87
00:06:07,300 --> 00:06:09,780
but we'll come back to them
when the time is right.

88
00:06:11,530 --> 00:06:15,120
Once you arrive at the screen
titled Blank Activity go ahead and

89
00:06:15,120 --> 00:06:16,070
hit the Finish button.

90
00:06:18,270 --> 00:06:21,677
Eclipse will generate the application
source code and lay out files.

91
00:06:21,677 --> 00:06:29,230
And once it's finished you will open
a file called activity_main.XML.

92
00:06:29,230 --> 00:06:33,860
This file defines the application's
user interface layout, and

93
00:06:33,860 --> 00:06:36,540
based on this file Eclipse
will show you whatever it

94
00:06:36,540 --> 00:06:41,220
can about how this application
will look when it runs.

95
00:06:41,220 --> 00:06:42,175
As you can see here,

96
00:06:42,175 --> 00:06:46,960
Hello Android will display a simple
screen with the words hello world.

97
00:06:48,070 --> 00:06:51,300
Let's also take a quick look at
the source code for this application.

98
00:06:51,300 --> 00:06:56,384
Let's open up the file mainactivity.java.

99
00:06:56,384 --> 00:07:00,800
Now that opens up an Editor window
showing the contents of the file.

100
00:07:00,800 --> 00:07:05,913
And I won't talk too much about this
source code right now except to say that

101
00:07:05,913 --> 00:07:11,434
when this application runs, the onCreate
method of this class will be called and

102
00:07:11,434 --> 00:07:17,690
that code will set up and display
the application's user interface screen.

103
00:07:17,690 --> 00:07:19,330
So let's run this code
to see what happens.

104
00:07:20,820 --> 00:07:25,440
One way to run an application is to
click on the project in the File pane,

105
00:07:25,440 --> 00:07:29,570
then select Run As and
then select Android Application.

106
00:07:30,640 --> 00:07:35,423
Now a dialog box pops up asking
where to run the application.

107
00:07:35,423 --> 00:07:38,670
In this case I only have my phone
connected so I'll choose that.

108
00:07:40,420 --> 00:07:44,560
And now we'll wait while the application
is copied to the phone and then run.

109
00:07:46,750 --> 00:07:47,970
And there you have it.

110
00:07:47,970 --> 00:07:52,580
Your first Android application
running on a real device.

111
00:07:52,580 --> 00:07:56,795
Now, the phone I used is a Nexus 4.

112
00:07:56,795 --> 00:07:59,995
Let's suppose I'd like to test
this out on a different phone.

113
00:08:01,375 --> 00:08:04,460
Let's say the Galaxy Nexus, but
I don't have one of those phones.

114
00:08:05,540 --> 00:08:09,180
In that case, rather than run out and
buy a Galaxy Nexus,

115
00:08:09,180 --> 00:08:14,640
I can run Hello Android on an emulated
Galaxy Nexus, using the Android Emulator.

116
00:08:14,640 --> 00:08:19,177
And in order to do that, I first need to
create the emulated phone instance that

117
00:08:19,177 --> 00:08:22,035
I'll use inside the emulator.

118
00:08:22,035 --> 00:08:25,420
Now these emulated phone instances
are called virtual devices.

119
00:08:26,460 --> 00:08:31,493
Let's go back to Eclipse and
set up a virtual device corresponding to

120
00:08:31,493 --> 00:08:40,035
a Galaxy Nexus So
now we're back in Eclipse.

121
00:08:41,550 --> 00:08:46,610
First, I'll go up to the Tool Bar, and
launch the Android Virtual Device Manager.

122
00:08:48,010 --> 00:08:50,200
It might be a bit hard to see here,
but look for

123
00:08:50,200 --> 00:08:53,580
the icon that shows an Android in a box.

124
00:08:55,270 --> 00:09:00,180
Clicking that will display a dialog box
showing existing virtual devices and

125
00:09:00,180 --> 00:09:02,120
allowing you to make new ones.

126
00:09:02,120 --> 00:09:07,140
Let's click on the New button which
will bring up another dialog box.

127
00:09:07,140 --> 00:09:10,750
Under AVD name, or
Android Virtual Device Name,

128
00:09:10,750 --> 00:09:13,730
you can give this virtual
device any name you want.

129
00:09:18,580 --> 00:09:23,330
Now go to the device pull down menu and
select Galaxy Nexus.

130
00:09:23,330 --> 00:09:25,625
That will fill in a bunch
of information for

131
00:09:25,625 --> 00:09:27,868
you, such as the target platform to use.

132
00:09:27,868 --> 00:09:31,539
In this case let's use Android 4.2.2.

133
00:09:31,539 --> 00:09:37,118
I'm going to add some memory to the SD
card just in case we may need it later,

134
00:09:37,118 --> 00:09:38,813
and then I'll hit OK.

135
00:09:38,813 --> 00:09:41,020
You can see the virtual
device you just created.

136
00:09:43,090 --> 00:09:45,380
Click on that, and then click Start.

137
00:09:47,610 --> 00:09:49,660
Now go ahead and
click through the next screen,

138
00:09:49,660 --> 00:09:54,430
and then we'll need to wait several
minutes while the virtual device boots up.

139
00:09:55,550 --> 00:09:56,680
I'll come back later when it's done.

140
00:10:00,000 --> 00:10:02,870
Okay, we're almost there,
just a few seconds more.

141
00:10:04,830 --> 00:10:08,420
And there's the home screen
of an emulated Galaxy Nexus.

142
00:10:09,870 --> 00:10:12,720
Now that the Galaxy Nexus is running,
let's return to Eclipse.

143
00:10:14,420 --> 00:10:15,720
And this time I'll install and

144
00:10:15,720 --> 00:10:20,770
run Hello Android on the emulated phone,
rather than on a physical device.

145
00:10:20,770 --> 00:10:25,982
So just like we did before, let's click
on the Project Name, select Run As, and

146
00:10:25,982 --> 00:10:28,295
then select Android Application.

147
00:10:28,295 --> 00:10:33,430
The dialog box up, box pops up,
showing us connected devices.

148
00:10:33,430 --> 00:10:37,060
And this time,
instead of selecting an actual device,

149
00:10:37,060 --> 00:10:41,960
select the virtual device that
you just started and click OK.

150
00:10:41,960 --> 00:10:47,620
Eclipse will install the application
on the virtual device, and then run it.

151
00:10:47,620 --> 00:10:48,270
And here you go.

152
00:10:49,400 --> 00:10:51,110
Hello Android, running on the emulator.

153
00:10:52,420 --> 00:10:55,640
Today I've shown you that you can build
applications like Hello Android on

154
00:10:55,640 --> 00:10:59,220
an actual device,
inside an emulator, or both.

155
00:10:59,220 --> 00:11:02,960
And there are pluses and
minuses with each of these approaches.

156
00:11:02,960 --> 00:11:06,970
For example, the benefits of
using an emulator include that

157
00:11:06,970 --> 00:11:07,950
the emulator is cheaper.

158
00:11:07,950 --> 00:11:11,970
You don't have to buy all the devices
that you may want to test on.

159
00:11:11,970 --> 00:11:15,090
Also unlike the phone,
the emulator allows you to

160
00:11:15,090 --> 00:11:20,270
easily configure hardware characteristics,
like the size of the SD card,

161
00:11:20,270 --> 00:11:24,280
the display size, whether the device
has a track ball, and so on.

162
00:11:25,380 --> 00:11:27,990
Also, many modifications that you make or

163
00:11:27,990 --> 00:11:32,910
any of the modifications that you make,
are isolated to this emulated device.

164
00:11:32,910 --> 00:11:36,920
So you don't have to worry that your
testing will mess up your phone or

165
00:11:36,920 --> 00:11:38,300
its data or configuration.

166
00:11:39,400 --> 00:11:43,670
Now on the other hand,
emulators have some downsides as well.

167
00:11:43,670 --> 00:11:46,780
For example,
the Android emulator is pretty slow and

168
00:11:46,780 --> 00:11:50,310
that can be frustrating when you're trying
to rapidly experiment and tune your

169
00:11:50,310 --> 00:11:54,460
app and you have to wait for the emulator
to start up and run and shut down.

170
00:11:55,540 --> 00:11:58,930
Also, some features are not
well supported by the emulator.

171
00:11:58,930 --> 00:12:02,810
For instance, there's no support for
Bluetooth connectivity, and no support for

172
00:12:02,810 --> 00:12:07,290
connecting accessories to
the emulator via USB cables.

173
00:12:07,290 --> 00:12:12,260
Also, some software features aren't
available by default in the emulator, so

174
00:12:12,260 --> 00:12:14,020
certain applications won't run on it.

175
00:12:15,050 --> 00:12:20,300
And finally, at the end of the day,
the emulator is not a device.

176
00:12:20,300 --> 00:12:22,810
You can't know how your
application is going to look and

177
00:12:22,810 --> 00:12:27,970
perform on an actual device just
by seeing it on an emulator.

178
00:12:29,530 --> 00:12:34,000
Let's take a look at some of the advanced
features that the emulator supports.

179
00:12:34,000 --> 00:12:37,800
For instance, you can configure
the emulator to emulate the speed and

180
00:12:37,800 --> 00:12:41,200
latency of different cellular networks.

181
00:12:41,200 --> 00:12:44,960
You can configure the emulator to emulate
different battery states, such as

182
00:12:44,960 --> 00:12:49,620
whether you're running low on battery
power or currently charging the device.

183
00:12:49,620 --> 00:12:53,070
You can also inject mock location
coordinates to make testing of

184
00:12:53,070 --> 00:12:55,130
locationware applications much easier.

185
00:12:56,220 --> 00:13:02,110
Typically these features help to test code
that must respond to environment events.

186
00:13:02,110 --> 00:13:04,810
For instance,
applications are often designed to do

187
00:13:04,810 --> 00:13:07,000
different things depending
on the battery level.

188
00:13:09,930 --> 00:13:12,110
Let me show you how easy this is.

189
00:13:12,110 --> 00:13:16,660
Here I'm showing a terminal window and
an emulator running on a virtual device.

190
00:13:16,660 --> 00:13:19,719
In the terminal window, I'll use
Telnet to connect to the emulator.

191
00:13:20,910 --> 00:13:24,340
You can see the port number on which
the emulator's listening in that

192
00:13:24,340 --> 00:13:26,940
emulator window's Title bar.

193
00:13:26,940 --> 00:13:29,743
And in this case, that's 5554.

194
00:13:29,743 --> 00:13:35,036
So I'll type, Telnet localhost 5554.

195
00:13:35,036 --> 00:13:39,500
Now I'll set the network characteristics
to emulate a slower edge network.

196
00:13:41,580 --> 00:13:45,550
I'll type network, speed,
edge, and notice that

197
00:13:45,550 --> 00:13:50,392
the Cellular Network Status icon in
the Notification bar has now changed.

198
00:13:50,392 --> 00:13:57,873
Now I'll change it back
to 3G network speed.

199
00:13:57,873 --> 00:14:01,600
And now I'll change the battery status to
reflect a phone that is running out of

200
00:14:01,600 --> 00:14:02,400
battery power.

201
00:14:03,570 --> 00:14:06,940
I'll type in power capacity 5.

202
00:14:06,940 --> 00:14:11,660
And again, you can see that the Battery
Status indicator in the Notification bar

203
00:14:11,660 --> 00:14:15,340
has changed to reflect
the lower battery level.

204
00:14:15,340 --> 00:14:19,580
Now I'll change the charging status to
indicate that the phone is not plugged in.

205
00:14:20,590 --> 00:14:25,433
I'll type power status not charging.

206
00:14:25,433 --> 00:14:27,007
And again, as you can see,

207
00:14:27,007 --> 00:14:30,775
the Battery Status indicator no
longer shows a lightning bolt and

208
00:14:30,775 --> 00:14:35,450
has changed color to indicate that the
device is almost completely out of power.

209
00:14:37,040 --> 00:14:40,291
Also if I open the Maps application,
you can see

210
00:14:40,291 --> 00:14:45,586
that the emulator thinks that I'm
currently somewhere near Washington DC.

211
00:14:52,293 --> 00:15:00,064
However I can change that by
inputting a new set of location

212
00:15:00,064 --> 00:15:07,209
coordinates like so, geo fix 0.00 40.00.

213
00:15:07,209 --> 00:15:11,879
And as you can see the Maps
application now shows that

214
00:15:11,879 --> 00:15:17,518
I've been transported somewhere
near a beach in sunny Spain.

215
00:15:17,518 --> 00:15:22,842
The Android Emulator also allows you
to emulate network interactions,

216
00:15:22,842 --> 00:15:26,427
such as receiving a phone call or
an SMS message.

217
00:15:26,427 --> 00:15:30,190
Back in the terminal window, I'll
reconnect to the emulator over Telnet.

218
00:15:31,250 --> 00:15:33,620
And then,
I'll give the emulator a command,

219
00:15:33,620 --> 00:15:37,650
which will cause it to emulate
an incoming SMS message.

220
00:15:37,650 --> 00:15:43,455
Now that command is SMS send,
followed by the sender's phone number

221
00:15:43,455 --> 00:15:49,740
3015555555, and then the text message.

222
00:15:49,740 --> 00:15:52,150
In this case, this is a text message.

223
00:15:53,250 --> 00:15:57,380
Now keep your eye on the Emulator window
to see what happens when I hit Return.

224
00:16:03,870 --> 00:16:04,590
And, as you can see,

225
00:16:04,590 --> 00:16:09,830
the emulator's Notification bar now
contains a notification indicating that

226
00:16:09,830 --> 00:16:14,790
the emulated phone just
received the SMS message.

227
00:16:14,790 --> 00:16:19,215
And we can pull down on
the Notification bar and

228
00:16:19,215 --> 00:16:26,721
then start the messaging application to
get a closer look at that SMS message.

229
00:16:34,328 --> 00:16:37,765
Android also allows two emulators
to interact with each other.

230
00:16:40,968 --> 00:16:43,637
Here, I'm showing two
emulator instances running.

231
00:16:43,637 --> 00:16:46,615
In one,
I'll open the phone application, and

232
00:16:46,615 --> 00:16:49,080
start to dial the number of the second.

233
00:16:50,870 --> 00:16:55,783
The number of the second emulator
is the port number shown at

234
00:16:55,783 --> 00:16:59,920
the top of that emulator,
in this case 5554.

235
00:17:08,796 --> 00:17:12,863
Now you can see that the second
emulator has received the call, and

236
00:17:12,863 --> 00:17:14,320
it is ringing its user.

237
00:17:15,640 --> 00:17:22,050
So I'll pick up the incoming call in the
second emulator, and as you can see, now

238
00:17:22,050 --> 00:17:27,290
the first emulator's interface has changed
to reflect that the call is connected.

239
00:17:28,440 --> 00:17:32,160
Now if one the parties hit
the Hold button that will also be

240
00:17:32,160 --> 00:17:34,135
reflected in both phone applications.

241
00:17:35,540 --> 00:17:41,024
And once the users are done with the call,
I can hang up one of the emulators,

242
00:17:41,024 --> 00:17:45,499
and both emulators show that
the call has been disconnected.

243
00:17:45,499 --> 00:17:48,195
And of course, that's just a few of
the many things that you can do with

244
00:17:48,195 --> 00:17:48,800
the emulator.

245
00:17:48,800 --> 00:17:52,170
There are many,
many other interesting features as well.

246
00:17:52,170 --> 00:17:56,850
And if you want to know more, I recommend
you take a look at the emulator page on

247
00:17:56,850 --> 00:17:58,947
the Android developers website.

