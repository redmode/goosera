1
00:00:00,000 --> 00:00:10,000
[MUSIC]

2
00:00:12,965 --> 00:00:16,170
>> Hi, I'm professor Adam Porter
of the University of Maryland.

3
00:00:16,170 --> 00:00:20,690
And it's my pleasure to welcome you to
Programming Mobile Applications for

4
00:00:20,690 --> 00:00:23,170
Android handheld systems.

5
00:00:23,170 --> 00:00:26,550
Now as I'm sure you know,
handheld systems such as smart phones and

6
00:00:26,550 --> 00:00:29,350
tablets are becoming extremely popular.

7
00:00:29,350 --> 00:00:33,220
In fact, some people even say that
handheld systems are spreading faster than

8
00:00:33,220 --> 00:00:36,650
any other technology in
the history of the world.

9
00:00:36,650 --> 00:00:39,800
For example, did you know that
each year there are three times as

10
00:00:39,800 --> 00:00:43,759
many smart phones activated as there
are children born in the entire world?

11
00:00:44,910 --> 00:00:47,950
And did you know that today more people
access the Internet using mobile

12
00:00:47,950 --> 00:00:51,798
devices than using desktop and
laptop computers combined?

13
00:00:51,798 --> 00:00:53,770
Or how about this?

14
00:00:53,770 --> 00:00:55,790
Did you know that by 2015,

15
00:00:55,790 --> 00:01:00,400
it's projected that over 500 billion
mobile apps will have been downloaded?

16
00:01:01,430 --> 00:01:05,110
Now, I certainly can't predict the future,
but I think it's safe to say that if

17
00:01:05,110 --> 00:01:09,520
you want to work in the IT sector
over the next five to ten years,

18
00:01:09,520 --> 00:01:13,450
then you probably should know
something about handheld systems and

19
00:01:13,450 --> 00:01:14,570
about the apps that run on them.

20
00:01:14,570 --> 00:01:16,960
And that's where this class comes in.

21
00:01:18,050 --> 00:01:21,340
In this course, you'll get a broad
overview of the Android platform,

22
00:01:21,340 --> 00:01:24,720
one of the two most popular mobile
platforms in the world today.

23
00:01:24,720 --> 00:01:29,460
And we'll be focusing primarily on
the user facing parts of app development,

24
00:01:29,460 --> 00:01:33,230
the things that users see and
touch when they're using an app.

25
00:01:33,230 --> 00:01:37,500
And remember, that this course is just
one part of a multipart series, or

26
00:01:37,500 --> 00:01:42,010
specialization called mobile cloud
computing with Android, or MoCCA,

27
00:01:42,010 --> 00:01:42,600
for short.

28
00:01:43,620 --> 00:01:47,110
And while this course focuses on
the user-facing parts of app development,

29
00:01:47,110 --> 00:01:52,580
those other courses focus on other behind
the scenes aspects of app development.

30
00:01:52,580 --> 00:01:55,971
Things like handling concurrency,
security, and

31
00:01:55,971 --> 00:02:00,836
connecting your apps to powerful
services running remotely in the cloud.

32
00:02:00,836 --> 00:02:03,808
And of course, I think that each of
these topics is really important.

33
00:02:03,808 --> 00:02:08,429
So I encourage you to find our more about
MoCCA by taking a look at this link.

34
00:02:15,116 --> 00:02:19,852
Now before I go too much into this course,
let me take a second to introduce myself.

35
00:02:19,852 --> 00:02:23,803
Like I said earlier, my name is
Adam Porter, and I've been a professor or

36
00:02:23,803 --> 00:02:28,120
computer science at the University
of Maryland for almost 25 years.

37
00:02:28,120 --> 00:02:32,160
And I was studying and working in the IT
field for about ten years before that.

38
00:02:33,320 --> 00:02:37,500
Today, at the university, and as a private
consultant, I publish research and

39
00:02:37,500 --> 00:02:41,060
advise clients on issues related
to software engineering.

40
00:02:41,060 --> 00:02:46,050
So broadly speaking, the tools, techniques
and processes that help people develop

41
00:02:46,050 --> 00:02:51,230
software that works as it's supposed to
and is delivered on time and on budget.

42
00:02:51,230 --> 00:02:55,905
And that work has been funded and
used by major organizations including IBM,

43
00:02:55,905 --> 00:03:00,270
AT&T Lucent and the United States Navy,
just to name a few.

44
00:03:02,450 --> 00:03:03,660
Now over the past few years,

45
00:03:03,660 --> 00:03:07,186
I've started spending a lot more
of my time on handheld systems.

46
00:03:07,186 --> 00:03:08,900
For instance, in around 2008,

47
00:03:08,900 --> 00:03:12,090
I worked with a colleague from
Apple to develop the University of

48
00:03:12,090 --> 00:03:17,150
Maryland's first course on programming
handheld systems for the iPhone.

49
00:03:17,150 --> 00:03:18,720
And then a year or two later,

50
00:03:18,720 --> 00:03:24,060
as Android matured, I created a similar
course focused this time on Android.

51
00:03:24,060 --> 00:03:27,110
In fact, after many iterations and
modifications,

52
00:03:27,110 --> 00:03:30,880
that course evolved into the very course
that you're taking part in today.

53
00:03:32,090 --> 00:03:35,150
So since I've taught some version
of this course several times,

54
00:03:35,150 --> 00:03:37,540
let's talk a little bit more
about what this course and

55
00:03:37,540 --> 00:03:40,830
what I think you can do to
get the most out of it.

56
00:03:40,830 --> 00:03:43,470
Now first of all, as I said,
this course is based on

57
00:03:43,470 --> 00:03:48,100
one that I teach to juniors and
seniors at the University of Maryland.

58
00:03:48,100 --> 00:03:50,900
So it's actually very
important to me that what I do

59
00:03:50,900 --> 00:03:56,480
in this Coursera course feeds back to
my on-campus students, and vice versa.

60
00:03:56,480 --> 00:04:00,400
What I do for my on-campus course should
improve this Coursera course as well.

61
00:04:01,660 --> 00:04:04,740
Now in particular, this means
that this course is not a gentle

62
00:04:04,740 --> 00:04:06,420
introduction to Android.

63
00:04:06,420 --> 00:04:08,890
It's not a taste of Android.

64
00:04:08,890 --> 00:04:09,780
No.
This is more or

65
00:04:09,780 --> 00:04:13,440
less the same course I teach
to my on campus students.

66
00:04:13,440 --> 00:04:17,730
So, we start at a pretty high level and
we go kind of fast.

67
00:04:17,730 --> 00:04:22,140
And I can do that in my on campus
course because I know our system and

68
00:04:22,140 --> 00:04:23,570
I know my students.

69
00:04:23,570 --> 00:04:26,680
And I know that they have a certain
level of prior knowledge and

70
00:04:26,680 --> 00:04:28,910
that they know what I expect from them.

71
00:04:28,910 --> 00:04:32,380
I know, for example,
that my students know Java very well.

72
00:04:32,380 --> 00:04:35,720
I know that they also programmed
a bit in languages besides Java.

73
00:04:35,720 --> 00:04:39,080
I also know that they have
taken other background courses.

74
00:04:40,200 --> 00:04:43,680
And moreover, I know that they
expect to get their hands dirty.

75
00:04:43,680 --> 00:04:44,200
The dig for

76
00:04:44,200 --> 00:04:49,320
information on their own, and I know that
they'll ask questions when they need to.

77
00:04:49,320 --> 00:04:52,810
And so, if that doesn't sound right for
you, you don't know Java,

78
00:04:52,810 --> 00:04:56,780
you're completely new to programming,
or you don't have the time or

79
00:04:56,780 --> 00:04:59,700
the inclination to look up
information on your own,

80
00:04:59,700 --> 00:05:03,960
then this might not be the right
course for you at this time.

81
00:05:03,960 --> 00:05:08,140
Instead you might consider a course that's
targeted to a more beginning student,

82
00:05:08,140 --> 00:05:12,370
such as Lawrence Angrave's Coursera
course, The creative, Serious and

83
00:05:12,370 --> 00:05:14,850
Playful Science of Android Apps.

84
00:05:14,850 --> 00:05:17,812
Or you might want to find a good
Java tutorial and do that first.

85
00:05:17,812 --> 00:05:21,192
So, don't be afraid to try one
of these other things first and

86
00:05:21,192 --> 00:05:23,408
then come back here when you're ready.

87
00:05:23,408 --> 00:05:30,605
We're not going anywhere,
and we'll keep the light on.

88
00:05:30,605 --> 00:05:32,316
Now, as we go through the course,

89
00:05:32,316 --> 00:05:35,156
we'll generally stick to
the following organization.

90
00:05:35,156 --> 00:05:39,080
Each week, I'll start by posting
a set of the video lectures.

91
00:05:39,080 --> 00:05:43,610
And those lectures generally last anywhere
from an hour to an hour and a half.

92
00:05:43,610 --> 00:05:47,100
I tend to speak pretty slowly because we
have many non-native English speakers in

93
00:05:47,100 --> 00:05:51,060
this class, so
if you understand English well,

94
00:05:51,060 --> 00:05:54,490
then feel feel,
feel free to crank up the playback speed.

95
00:05:54,490 --> 00:05:55,860
And in each of the video lectures,

96
00:05:55,860 --> 00:05:58,830
I'll normally start by
talking about some concept or

97
00:05:58,830 --> 00:06:04,260
API, and as I go through the lecture,
I'll demo several sample applications.

98
00:06:04,260 --> 00:06:07,470
And I'll show screencast where I
walk through the source code for

99
00:06:07,470 --> 00:06:10,420
those sample applications
that I've just demonstrated.

100
00:06:10,420 --> 00:06:15,220
In fact, all told, I'll show you about
110 of these sample applications.

101
00:06:15,220 --> 00:06:18,130
And you can get the source code for
all of these applications at

102
00:06:18,130 --> 00:06:21,860
the courses GitHub repository,
which is available at this link.

103
00:06:26,050 --> 00:06:27,330
Now each week,
I'll also assign a graded quiz.

104
00:06:27,330 --> 00:06:30,340
Quizzes are meant to reinforce
the lecture material, and

105
00:06:30,340 --> 00:06:34,240
sometimes to introduce additional
concepts and resources.

106
00:06:34,240 --> 00:06:37,570
And you can take these quizzes
as many times as you like.

107
00:06:37,570 --> 00:06:41,250
And finally, each week,
I'll also assign one or more labs.

108
00:06:41,250 --> 00:06:45,470
In these labs we'll usually ask
you to implement an application.

109
00:06:45,470 --> 00:06:50,850
In most cases, the labs will come with
skeleton code that implements, say,

110
00:06:50,850 --> 00:06:53,190
75 to 90% of the application.

111
00:06:53,190 --> 00:06:56,780
So you'll need to read and
understand that skeleton code and

112
00:06:56,780 --> 00:06:59,650
then you'll need to fill in
the parts that are missing.

113
00:06:59,650 --> 00:07:02,700
So you can think of these
labs as a kind of drill.

114
00:07:02,700 --> 00:07:07,780
They focus in on just a few specific
things, not on everything at once.

115
00:07:07,780 --> 00:07:11,690
And so to make sure that you do get chance
to put it all together at the end of

116
00:07:11,690 --> 00:07:15,210
weeks four and
eight I will also assign labs in

117
00:07:15,210 --> 00:07:18,210
which you develop a simple
app completely from scratch.

118
00:07:25,820 --> 00:07:26,520
Okay.

119
00:07:26,520 --> 00:07:27,830
So before I finish up,

120
00:07:27,830 --> 00:07:32,360
let me say a few words about the specific
topics we'll cover in this course.

121
00:07:32,360 --> 00:07:36,850
The material in this course is delivered
as two four week mini courses,

122
00:07:36,850 --> 00:07:39,600
called part one and part two.

123
00:07:39,600 --> 00:07:43,620
Part one introduces the Android
platform and its development tools.

124
00:07:43,620 --> 00:07:48,590
And it explains the basic concepts you'll
need to create simple Android apps.

125
00:07:48,590 --> 00:07:53,010
Part two, on the other hand, dives into
the additional services that come into

126
00:07:53,010 --> 00:07:56,730
play when you're ready to
create more advanced apps.

127
00:07:56,730 --> 00:08:00,190
Now as I said,
each of these parts runs for four weeks.

128
00:08:00,190 --> 00:08:03,440
In week one,
we'll talk about the Android platform and

129
00:08:03,440 --> 00:08:05,600
the Android development environment.

130
00:08:05,600 --> 00:08:08,300
In week two,
we'll cover how apps are created and

131
00:08:08,300 --> 00:08:12,850
we'll go over the activity class, which is
the primary class that's responsible for

132
00:08:12,850 --> 00:08:15,680
presenting an apps user interface.

133
00:08:15,680 --> 00:08:20,850
The following week, I'll introduce intents
and permissions, which allow one app or

134
00:08:20,850 --> 00:08:25,650
activity to start up and
use other apps and activities.

135
00:08:25,650 --> 00:08:28,170
And I'll also talk about
the fragment class,

136
00:08:28,170 --> 00:08:33,390
another class that plays a key role
in presenting an apps user interface.

137
00:08:33,390 --> 00:08:38,560
And finally, in week four, I'll go into
detail about the full range of classes and

138
00:08:38,560 --> 00:08:43,140
patterns used to create
sophisticated user interfaces.

139
00:08:43,140 --> 00:08:43,770
At this point,

140
00:08:43,770 --> 00:08:47,538
we'll have finished part one, and
we'll be ready to dive in on part two.

141
00:08:47,538 --> 00:08:53,400
In week five, we'll discuss a number of
topics, including user notifications,

142
00:08:53,400 --> 00:08:56,950
event notification using
the broadcast receiver class and

143
00:08:56,950 --> 00:09:01,430
using alarms to invoke code
at prescheduled times.

144
00:09:01,430 --> 00:09:06,670
Week six discusses how to handle
concurrency with threads, Asynctasks and

145
00:09:06,670 --> 00:09:11,150
handlers, and also discusses how
to acquire data over the internet.

146
00:09:12,170 --> 00:09:15,560
Week seven gets into more visual topics,
including graphics and

147
00:09:15,560 --> 00:09:19,540
animation, touch processing,
and multimedia.

148
00:09:19,540 --> 00:09:22,367
And finally, during week eight,
we'll focus on

149
00:09:22,367 --> 00:09:26,492
using the many sensors that now come
standard on most handheld devices.

150
00:09:26,492 --> 00:09:31,252
And we'll talk about how to acquire and
display location information.

151
00:09:31,252 --> 00:09:36,267
And we'll then close out week eight with
a lecture on how to manage and store data.

152
00:09:43,396 --> 00:09:44,464
And one more thing.

153
00:09:44,464 --> 00:09:48,376
To help make sure that the skills you're
learning in this course are applicable to

154
00:09:48,376 --> 00:09:50,188
things you might do in the work world,

155
00:09:50,188 --> 00:09:53,090
I've partnered up with some
folks on Amazon App Store team.

156
00:09:53,090 --> 00:09:56,740
And they have agreed to create some
guest lectures to share some of

157
00:09:56,740 --> 00:10:02,320
their hard-won experience about building
and publishing successful Android apps.

158
00:10:02,320 --> 00:10:06,630
Amazon will also offer a free e-book
to all students of this course.

159
00:10:06,630 --> 00:10:10,020
And I'll have more information
on this later in the course.

160
00:10:10,020 --> 00:10:12,900
And also, as a special part of
this partnership with Amazon,

161
00:10:12,900 --> 00:10:16,420
the top fifty students from this
course will be given the option to

162
00:10:16,420 --> 00:10:20,220
share their resumes directly
with Amazon recruiters.

163
00:10:20,220 --> 00:10:22,649
Again, we'll have more on this
as the course progresses.

164
00:10:25,000 --> 00:10:27,280
So that's all for
my overview on the course.

165
00:10:27,280 --> 00:10:30,870
Please join me next time as I
introduce you to the Android platform.

