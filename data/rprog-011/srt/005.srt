1
00:00:00,000 --> 00:00:03,062
[BLANK_AUDIO]

2
00:00:03,062 --> 00:00:08,320
I just wanted to briefly describe how to
install R for a Windows machine.

3
00:00:08,320 --> 00:00:09,430
So the first thing you need to do is

4
00:00:09,430 --> 00:00:11,900
load, launch your web browser, so I'll do
that here.

5
00:00:11,900 --> 00:00:13,480
I'm using Chrome, but it doesn't really
matter.

6
00:00:13,480 --> 00:00:16,761
And you need to go to the Comprehensive R
Archive Network or CRAN.

7
00:00:16,761 --> 00:00:20,781
So I'll just type that in here.

8
00:00:20,781 --> 00:00:23,770
And you'll see that there's a, at the top,
there's three options.

9
00:00:23,770 --> 00:00:27,920
There's Linux, Mac and Windows, so you can
go to the Mac version here.

10
00:00:27,920 --> 00:00:30,035
And you want to go, click on the base link
here.

11
00:00:30,035 --> 00:00:33,860
[COUGH] And at the very top, you'll see
download

12
00:00:33,860 --> 00:00:36,830
R 3.0.3 for Windows, and that's exactly
what you want.

13
00:00:36,830 --> 00:00:39,980
So you can just click on this link, and
the download will start.

14
00:00:39,980 --> 00:00:41,590
And so, depending on how fast your
internet

15
00:00:41,590 --> 00:00:43,340
connection is, this might take a few
minutes.

16
00:00:47,510 --> 00:00:48,870
Okay, so the download's finished.

17
00:00:48,870 --> 00:00:49,960
I'm going to click on this.

18
00:00:51,960 --> 00:00:53,810
And you'll probably have to click on Yes
for this.

19
00:00:55,260 --> 00:00:58,040
And so, you can choose your language here,
there are a number

20
00:00:58,040 --> 00:01:01,540
of choices in terms of the translations
that you can choose from.

21
00:01:01,540 --> 00:01:03,350
So I'm going to choose English because
that's my

22
00:01:03,350 --> 00:01:05,958
language, then you can just click through
the installer.

23
00:01:05,958 --> 00:01:08,480
It will kind of walk you through the
various steps and so

24
00:01:08,480 --> 00:01:11,200
we'll do that right now, just see what the
options are.

25
00:01:11,200 --> 00:01:13,100
So you click on Next, you have to agree to

26
00:01:13,100 --> 00:01:16,590
the license, which is the gen, GNU General
Public License, so.

27
00:01:16,590 --> 00:01:18,260
Feel free to read it and then click on
Next.

28
00:01:19,760 --> 00:01:21,590
The use of the default installation
directory

29
00:01:21,590 --> 00:01:23,200
is fine so I'm going to go through this.

30
00:01:24,215 --> 00:01:27,280
The use, the default user installation is
fine.

31
00:01:27,280 --> 00:01:30,353
There are other kind of installation
setups that you can choose from.

32
00:01:30,353 --> 00:01:32,684
If you know you only, you have a 32 bit

33
00:01:32,684 --> 00:01:36,602
machine maybe an older machine but you
could click on that.

34
00:01:36,602 --> 00:01:38,502
By default it will install both versions,
so

35
00:01:38,502 --> 00:01:40,658
you don't really need to worry about that.

36
00:01:40,658 --> 00:01:43,308
So just click through Next on this one.

37
00:01:43,308 --> 00:01:45,216
And you can choose to kind of take all the

38
00:01:45,216 --> 00:01:47,604
defaults, or you can try to customize your
startup.

39
00:01:47,604 --> 00:01:49,380
I'm going to customize the startup just so
you

40
00:01:49,380 --> 00:01:51,110
can see kind of what the options are here.

41
00:01:53,110 --> 00:01:55,270
So this option here shows you whether,
asks

42
00:01:55,270 --> 00:01:57,290
you whether you want an MDI or SDI
interface.

43
00:01:57,290 --> 00:02:01,200
So what that means is basically, do you
want R to kind of run in one big window.

44
00:02:02,200 --> 00:02:05,320
With kind of different sub-windows with,
inside of a big window or

45
00:02:05,320 --> 00:02:08,070
do you want it to run in kind of like
separate windows?

46
00:02:08,070 --> 00:02:11,030
I prefer to use the SDI mode where the, so
the console will

47
00:02:11,030 --> 00:02:14,310
be in one window and the kind of graphics
window will be a separate window.

48
00:02:14,310 --> 00:02:16,580
I just kind of, I feel like it, I like
that a little better.

49
00:02:16,580 --> 00:02:19,590
It's easier to work with so I'm going to
click on the SDI option.

50
00:02:21,390 --> 00:02:24,634
And then you can choose how you want to
look at your help files.

51
00:02:24,634 --> 00:02:27,490
So the HTML help is a little bit nicer,
it's prettier to

52
00:02:27,490 --> 00:02:31,590
read and the plain text help is well, it's
just plain text.

53
00:02:31,590 --> 00:02:33,990
So maybe I'll just click on plain text
just to be different.

54
00:02:35,250 --> 00:02:38,150
And then you can choose whether want
Standard or Internet2 internet access.

55
00:02:38,150 --> 00:02:41,049
This, generally speaking, you should not
mess with, so you just click on Next.

56
00:02:43,370 --> 00:02:45,780
You can create a shortcut in the Startup

57
00:02:45,780 --> 00:02:47,550
me, Start menu, so that's usually a good
idea.

58
00:02:47,550 --> 00:02:51,370
And you can usually click choose the
defaults here in terms of creating a

59
00:02:51,370 --> 00:02:54,360
desktop icon unless your desktop is very

60
00:02:54,360 --> 00:02:56,420
cluttered and you want to you know, avoid
that.

61
00:02:56,420 --> 00:02:58,880
So these are, these defaults are fine, so
I'll click on

62
00:02:58,880 --> 00:03:01,510
Next, and then it will start installing
the files on your computer.

63
00:03:04,710 --> 00:03:06,756
So now it's done and we can just click on

64
00:03:06,756 --> 00:03:10,670
Finish here and, and you've now installed
R on your computer.

65
00:03:10,670 --> 00:03:14,050
And so I'll just close this browser here
and I see you've got, I got a

66
00:03:14,050 --> 00:03:16,040
desktop icon here, so I'll just double
click

67
00:03:16,040 --> 00:03:18,570
on that and there we are, we're running R.

