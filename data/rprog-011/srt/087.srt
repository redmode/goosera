1
00:00:02,600 --> 00:00:04,457
Hi everyone, thanks again for joining our
programming.

2
00:00:04,457 --> 00:00:06,662
I just want to make one quick note about
the

3
00:00:06,662 --> 00:00:09,785
version of R that you'll be using in this
class.

4
00:00:09,785 --> 00:00:14,911
You might notice from some of the videos
that the videos are made using R version

5
00:00:14,911 --> 00:00:20,100
3.0.3, which was the most current version
at the time when the video was made.

6
00:00:20,100 --> 00:00:25,050
However as of now, the most current
version of R is 3.1.1.

7
00:00:25,050 --> 00:00:28,120
And so I encourage you to install that
version of R.

8
00:00:28,120 --> 00:00:31,620
Which should be the default version that
you find when you go to the R website.

9
00:00:31,620 --> 00:00:35,630
So don't try digging around for older
versions of R you won't need them.

10
00:00:35,630 --> 00:00:37,790
So I, in general I always encourage people
to

11
00:00:37,790 --> 00:00:41,400
use the latest version of software because
those will

12
00:00:41,400 --> 00:00:43,740
have the latest features and they will
have hopefully

13
00:00:43,740 --> 00:00:47,950
fixed bugs that may have been present in
earlier versions.

14
00:00:47,950 --> 00:00:51,910
So, for this version of the course for use
for R version 3.1.1.

15
00:00:51,910 --> 00:00:52,340
Thanks a lot.

