1
00:00:00,000 --> 00:00:03,963
[BLANK_AUDIO]

2
00:00:03,963 --> 00:00:07,900
In this video I'm going to briefly show
how you can install R on the Mac.

3
00:00:07,900 --> 00:00:09,110
It's a very simple process.

4
00:00:09,110 --> 00:00:10,960
You just have to, it only takes a few
steps.

5
00:00:10,960 --> 00:00:14,240
So the first thing you need to do is open
your web browser.

6
00:00:14,240 --> 00:00:14,740
And

7
00:00:16,720 --> 00:00:19,200
go to CRAN, so that's the Comprehensive R
Archive

8
00:00:19,200 --> 00:00:21,010
Network, so I can just type in CRAN here.

9
00:00:23,520 --> 00:00:24,990
And you'll see that [COUGH] there are a
number

10
00:00:24,990 --> 00:00:28,600
of options for you to download here for
different platforms.

11
00:00:28,600 --> 00:00:30,790
And so we're going to download the Mac
platform here.

12
00:00:30,790 --> 00:00:33,053
So we can go to Download R for the Mac,

13
00:00:33,053 --> 00:00:37,223
and you see that the latest version here
is version 3.0.3.

14
00:00:37,223 --> 00:00:39,569
And you want to download this package file
here, so just

15
00:00:39,569 --> 00:00:42,119
click on this, and you'll see that the
download meter will

16
00:00:42,119 --> 00:00:44,567
start going And it might take a few
minutes depending

17
00:00:44,567 --> 00:00:47,457
on the speed of your internet connection,
so just be patient.

18
00:00:47,457 --> 00:00:52,624
[BLANK_AUDIO]

19
00:00:52,624 --> 00:00:55,552
Okay, so it's finished downloading.

20
00:00:55,552 --> 00:00:56,920
So I'm just going to open up here.

21
00:00:58,660 --> 00:01:01,682
The package I can see that it'll start the
installer.

22
00:01:01,682 --> 00:01:05,730
And I'll guide you through all the steps
you need to install R 3.0.3.

23
00:01:05,730 --> 00:01:08,480
So I'll click Continue here and this is

24
00:01:08,480 --> 00:01:10,480
just the description of what's going to
get installed.

25
00:01:10,480 --> 00:01:11,110
Hit Continue.

26
00:01:12,680 --> 00:01:16,280
The Software License Agreement is the GNU
General Public License, version 2.

27
00:01:16,280 --> 00:01:19,860
So you should agree the license after
having read it, of course.

28
00:01:21,230 --> 00:01:23,930
And then you can just click Install here.

29
00:01:23,930 --> 00:01:25,800
And you might have to type in your
administrative password.

30
00:01:25,800 --> 00:01:28,888
So just go ahead and do that.

31
00:01:28,888 --> 00:01:31,343
And it'll start installing the files on
your computer.

32
00:01:31,343 --> 00:01:37,394
[BLANK_AUDIO]

33
00:01:37,394 --> 00:01:42,170
Now that's finished I can hit Close and
then it'll be in my Applications folder.

34
00:01:42,170 --> 00:01:45,240
So I can just go to my Applications folder
which is right here.

35
00:01:45,240 --> 00:01:49,100
And they're in alphabetical order so lets
scroll down to R and there it is.

36
00:01:51,520 --> 00:01:54,620
And there you have it, you have installed
R on your computer.

37
00:01:54,620 --> 00:01:57,390
And now you can go ahead you can just use
it directly or if

38
00:01:57,390 --> 00:02:01,450
you want to you can install an interface
like R Studio if you like.

