1
00:00:00,340 --> 00:00:01,860
Hello, and welcome to a worked

2
00:00:01,860 --> 00:00:05,600
exercise for Python for Informatics:
Exploring information.

3
00:00:05,600 --> 00:00:08,200
I am your host, Charles Severance, and

4
00:00:08,200 --> 00:00:11,690
this material is copyright Creative
Commons Attribution.

5
00:00:11,690 --> 00:00:13,200
So again, you're supposed to try to

6
00:00:13,200 --> 00:00:17,280
work these exercises yourself before 
you attempt them.

7
00:00:17,280 --> 00:00:19,910
Just, these are good crutches to 
make sure that you

8
00:00:19,910 --> 00:00:22,670
understand, but make sure you 
give it a try yourself.

9
00:00:23,740 --> 00:00:26,270
And if you don't understand 
how to basically

10
00:00:26,270 --> 00:00:28,280
create and edit programs, you 
should go back

11
00:00:28,280 --> 00:00:31,560
to www.pythonlearn.com, and 
work through some of the

12
00:00:31,560 --> 00:00:34,810
installation instructions for 
Python and your text editor.

13
00:00:35,830 --> 00:00:40,403
So here's our exercise, we're going to do
the classic rate times pay with overtime.

14
00:00:40,403 --> 00:00:48,740
And so the idea is that, oops, you have,
pay times rate and for 40 hours you

15
00:00:48,740 --> 00:00:51,960
get the basic rate and if you work above
it, you get one and a half times that.

16
00:00:51,960 --> 00:00:58,490
So if it's $10 an hour, you get $15 an
hour for the hours above 40.

17
00:00:59,490 --> 00:01:02,760
So let's go ahead and start 
TextWrangler, and start

18
00:01:02,760 --> 00:01:08,280
a command line. I'm going to close that,
close our drawer, make it smaller.

19
00:01:11,860 --> 00:01:16,290
And I'm going to, I'm going to pull my,
go into the

20
00:01:16,290 --> 00:01:20,940
Desktop, go to py4inf, see I've got a few
files here.

21
00:01:22,850 --> 00:01:31,010
And in this case, I am going 
to open hours1.py.

22
00:01:31,010 --> 00:01:36,230
And I'm going to immediately 
save it as hours2.py.

23
00:01:36,230 --> 00:01:39,640
I mean, why rewrite all that 
stuff we did before?

24
00:01:39,640 --> 00:01:42,300
So this I guess I can get rid 
of this one now.

25
00:01:42,300 --> 00:01:44,230
So it's hours2.py.

26
00:01:44,230 --> 00:01:49,240
And so now we have, the, 
I'll keep that in.

27
00:01:49,240 --> 00:01:51,000
We have the rate and the hours.

28
00:01:51,000 --> 00:01:53,360
I'll put that print statement back in.

29
00:01:53,360 --> 00:01:54,620
And the problem is I can't

30
00:01:57,210 --> 00:02:00,404
I can't just do pay equals rate times
hours, so I need an if-then-else.

31
00:02:00,404 --> 00:02:01,070
So let's do an if.

32
00:02:02,230 --> 00:02:06,300
If the hours is less than 40.

33
00:02:06,300 --> 00:02:10,800
And we can say, pay equals rate times
hours, that's fine.

34
00:02:10,800 --> 00:02:12,270
So now let's come up.

35
00:02:12,270 --> 00:02:13,550
I'll make that four spaces.

36
00:02:16,800 --> 00:02:17,300
There we go.

37
00:02:18,490 --> 00:02:20,380
I could write another statement
that says like this.

38
00:02:21,400 --> 00:02:27,090
If, actually it should be less than or 
equal to 40.

39
00:02:27,090 --> 00:02:31,990
If pay is greater than 40, we need
a different calculation.

40
00:02:33,450 --> 00:02:37,410
And, and that one is pay,

41
00:02:39,850 --> 00:02:43,060
is, there's a couple of ways to do this,
but I'm going to do it this way.

42
00:02:43,060 --> 00:02:46,900
Rate times 40, which means those are the,
those are the hours.

43
00:02:46,900 --> 00:02:48,680
That's your basic rate.

44
00:02:48,680 --> 00:02:54,960
Then I'm going to add rate times 1.5,
that's one and a half times

45
00:02:54,960 --> 00:03:01,040
the rate, times, and then the hours above
40, which is hours minus 40.

46
00:03:01,040 --> 00:03:04,780
So this bit here is the 
hours above 40.

47
00:03:04,780 --> 00:03:07,940
Now operator precedence would say that

48
00:03:07,940 --> 00:03:11,520
these multiplications are done before this
subtraction.

49
00:03:11,520 --> 00:03:15,160
And that might not work so well, so let's

50
00:03:15,160 --> 00:03:17,340
go ahead and run this and see what
happens, okay?

51
00:03:19,220 --> 00:03:21,850
So we got pay is rate times hours, and pay

52
00:03:21,850 --> 00:03:24,650
is equal to rate times 1.5 times hours
minus 40.

53
00:03:24,650 --> 00:03:26,844
Let's go ahead and run this.

54
00:03:26,844 --> 00:03:33,115
I got python, hours.

55
00:03:34,115 --> 00:03:40,441
[BLANK_AUDIO]

56
00:03:40,441 --> 00:03:44,142
Let's do 30 hours and $10, that's 
looking good.

57
00:03:44,142 --> 00:03:46,770
So, that is, we have, you know, and

58
00:03:46,770 --> 00:03:50,110
that's running through this bit of code
right here.

59
00:03:52,800 --> 00:03:58,880
And let's do exactly 40 hours, and that's
looking good.

60
00:03:58,880 --> 00:04:02,230
So let's do 50 hours because we can

61
00:04:02,230 --> 00:04:05,270
calculate that in our head, that's 10
hours extra.

62
00:04:05,270 --> 00:04:12,807
And then a rate of $10 should be 500.

63
00:04:12,807 --> 00:04:20,173
That doesn't seem right, so 50 times 10,
what's wrong here?

64
00:04:20,173 --> 00:04:21,632
50 times 10?

65
00:04:21,632 --> 00:04:28,206
[BLANK_AUDIO]

66
00:04:28,206 --> 00:04:30,717
Hours is less than 40.

67
00:04:30,717 --> 00:04:35,337
[BLANK_AUDIO]

68
00:04:35,337 --> 00:04:37,901
Now I'm confused.

69
00:04:37,901 --> 00:04:41,500
Let's run this, well, oops.

70
00:04:41,500 --> 00:04:43,090
What did I do here?

71
00:04:45,810 --> 00:04:49,110
Pay is greater than 40, that does 
not seem right.

72
00:04:49,110 --> 00:04:50,880
Did I save this file?

73
00:04:50,880 --> 00:04:51,390
I'm crazy.

74
00:04:51,390 --> 00:04:59,905
50 hours, $10 an hour, oh, okay, I must
not have saved the file, okay.

75
00:04:59,905 --> 00:05:01,511
So, [LAUGH].

76
00:05:01,511 --> 00:05:06,820
Pay is not defined on line eight.

77
00:05:06,820 --> 00:05:08,300
Oops, I was typing too fast.

78
00:05:08,300 --> 00:05:13,570
That needs to be hours 
greater than 40.

79
00:05:13,570 --> 00:05:14,250
Okay.

80
00:05:14,250 --> 00:05:19,440
So that's better, so now we have hours
less than 40 and hours greater than 40.

81
00:05:19,440 --> 00:05:22,390
So let's run that one again.

82
00:05:22,390 --> 00:05:24,490
50 hours at $10 an hour.

83
00:05:24,490 --> 00:05:29,930
50 hours at $10 an hour.

84
00:05:29,930 --> 00:05:35,021
That'd be like 600 something, this does
not look like the right number.

85
00:05:35,021 --> 00:05:45,021
[BLANK_AUDIO]

86
00:05:46,141 --> 00:05:48,725
We, and so we got I will, I will just take

87
00:05:48,725 --> 00:05:52,022
this print back out, print of rate and
hour, save that.

88
00:05:52,022 --> 00:05:54,634
Let's run it again.

89
00:05:54,634 --> 00:06:01,750
50 hours, $10 per hour, $1100.

90
00:06:01,750 --> 00:06:04,330
That does not look right.

91
00:06:05,550 --> 00:06:06,620
Let's take a look at this.

92
00:06:09,820 --> 00:06:15,030
Rate times 40, that's pretty obvious, 
it'd be $400.

93
00:06:15,030 --> 00:06:20,420
Rate times one and a half, that'd be $15,
times number of hours minus 40.

94
00:06:20,420 --> 00:06:23,220
Oh, so this is an operator precedence
problem.

95
00:06:24,470 --> 00:06:30,190
So, if you recall, multiplication happens

96
00:06:30,190 --> 00:06:36,010
before addition, and so Python is going to
do this calculation first.

97
00:06:36,010 --> 00:06:41,218
This is the actual calculation, it's
multiplying the rate times one and

98
00:06:41,218 --> 00:06:46,250
a half, times the number of hours, and
then it's subtracting 40.

99
00:06:46,250 --> 00:06:50,560
This ends up being a rather large number,
and that's how our calculation is wrong.

100
00:06:50,560 --> 00:06:53,120
That is not how we meant to do this.

101
00:06:53,120 --> 00:06:58,660
Oops, so let's put parentheses in, because
we want to subtract 40 from

102
00:06:58,660 --> 00:07:03,310
the hours before we do the multiplication.
We want this to happen first.

103
00:07:05,360 --> 00:07:07,338
We want the hours to subtract 
the 40 so we'll end up

104
00:07:07,338 --> 00:07:09,840
with 10 hours and then multiply that
by the rate.

105
00:07:09,840 --> 00:07:10,880
So let me save that.

106
00:07:12,900 --> 00:07:17,260
And run it. 50 hours, $10 an hour.

107
00:07:17,260 --> 00:07:19,660
550.

108
00:07:19,660 --> 00:07:21,258
Much better, much better.

109
00:07:21,258 --> 00:07:23,557
[WHISTLE] That looks pretty much better.

110
00:07:23,557 --> 00:07:26,630
Okay, so that's, so we're kind of done
with that.

111
00:07:26,630 --> 00:07:29,920
We had that little mistake there, of the
parentheses not working.

112
00:07:29,920 --> 00:07:32,610
And the other thing is, is that the much
better

113
00:07:32,610 --> 00:07:34,765
way to write this is just putting 
an else here.

114
00:07:34,765 --> 00:07:36,980
Alright?

115
00:07:36,980 --> 00:07:38,870
So we just say else.

116
00:07:38,870 --> 00:07:40,080
And so now, we have an if-then-else.

117
00:07:40,080 --> 00:07:43,000
If it's less than 40, less than or equal
to 40, we do this.

118
00:07:43,000 --> 00:07:44,890
If it's greater than 40, we do that.

119
00:07:44,890 --> 00:07:46,130
And then we do print pay.

120
00:07:46,130 --> 00:07:47,270
So let's save that one.

121
00:07:51,040 --> 00:07:54,400
So 50 hours, $10 an hour, 550.

122
00:07:54,400 --> 00:07:57,340
So this if-then-else formulation is a

123
00:07:57,340 --> 00:08:00,060
much better way to do this 
particular problem.

