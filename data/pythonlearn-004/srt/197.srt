1
00:00:00,068 --> 00:00:00,118
[MUSIC]

2
00:00:00,118 --> 00:00:01,444
Hello, it's Anthony Bourdain.

3
00:00:01,444 --> 00:00:03,440
No, no, it's actually not Anthony
Bourdain.

4
00:00:03,440 --> 00:00:06,113
It's Dr. Chuck, and we are here in Miami,

5
00:00:06,113 --> 00:00:10,050
Florida for another office hours for our
Coursera classes.

6
00:00:10,050 --> 00:00:12,750
And I want you to meet some of your fellow
students.

7
00:00:12,750 --> 00:00:13,650
So say hi.

8
00:00:13,650 --> 00:00:14,680
>> Hey, I'm Michael.

9
00:00:15,770 --> 00:00:17,930
First Coursera course ever.

10
00:00:17,930 --> 00:00:20,800
And, looking forward to learning how to
program.

11
00:00:20,800 --> 00:00:21,950
>> Okay.

12
00:00:21,950 --> 00:00:27,230
>> Hi, I'm Taylor, first Coursera course
ever too, and I'm enjoying it.

13
00:00:27,230 --> 00:00:29,200
>> Okay, make sure I sign that book before
you leave.

14
00:00:29,200 --> 00:00:29,540
>> Okay.

15
00:00:29,540 --> 00:00:29,800
>> Okay.

16
00:00:29,800 --> 00:00:34,570
>> Hi, I'm Monica, I'm from Brazil and I'm
really enjoying the class.

17
00:00:34,570 --> 00:00:37,300
>> You came a long way to come to office
hours in Miami Beach.

18
00:00:37,300 --> 00:00:39,543
>> Yeah, it's really important to me.

19
00:00:39,543 --> 00:00:40,062
>> [LAUGH].

20
00:00:40,062 --> 00:00:42,741
>> Hi, my name is Doug Rosen. This is my

21
00:00:42,741 --> 00:00:47,790
seventh Coursera course, my fifth Python
course, and I love Python.

22
00:00:47,790 --> 00:00:50,290
>> And you came all the way from New York
City?

23
00:00:50,290 --> 00:00:53,460
>> Well, no, I just love the New York
Yankees, but I,

24
00:00:53,460 --> 00:00:57,290
I'm originally from Michigan, but I've
lived in Florida for about 14 years.

25
00:00:57,290 --> 00:00:57,525
>> Cool.

26
00:00:57,525 --> 00:00:59,659
>> Hi. My name is Ivan

27
00:00:59,659 --> 00:01:01,640
I live in Miami but I am from Spain.

28
00:01:01,640 --> 00:01:05,870
I want to learn how to write code and
change the world.

29
00:01:05,870 --> 00:01:08,290
>> I'm, Jorge, and I'm finally glad

30
00:01:08,290 --> 00:01:10,740
to meet the wandering wizard of the
keyboards.

31
00:01:10,740 --> 00:01:12,160
With my hands off.

32
00:01:12,160 --> 00:01:18,160
>> Hi, I'm Erica, I'm taking Python
to be a better linguist.

33
00:01:18,160 --> 00:01:19,112
>> But you already know Python.

34
00:01:19,112 --> 00:01:20,980
sort of.

35
00:01:20,980 --> 00:01:21,640
>> It could get better.

36
00:01:21,640 --> 00:01:22,340
>> Yeah.

37
00:01:22,340 --> 00:01:22,830
>> Hi, I'm Paul.

38
00:01:22,830 --> 00:01:26,570
I'm Cambridge, England originally, but in
Miami now and it's much better.

39
00:01:26,570 --> 00:01:27,681
>> Cambridge, I love Cambridge.

40
00:01:27,681 --> 00:01:29,170
>> [LAUGH] Cambridge is a great place.

41
00:01:29,170 --> 00:01:31,734
So I've done the Internet History class
and I've got two

42
00:01:31,734 --> 00:01:34,790
kids, Jess and John, and they just enrolled
in the Python class.

43
00:01:34,790 --> 00:01:37,520
>> So wave to your kids, so that they'll
see you in class.

44
00:01:37,520 --> 00:01:38,160
>> Hey, kids.

45
00:01:38,160 --> 00:01:39,490
>> There you go.

46
00:01:39,490 --> 00:01:40,720
>> Hi, My name is Mike.

47
00:01:40,720 --> 00:01:43,918
I like the Coursera platform and Dr. Chuck
rocks.

48
00:01:44,918 --> 00:01:46,335
>> Hi, I'm Genevieve, this is my first

49
00:01:46,335 --> 00:01:50,300
course in Coursera and I'm really loving
the Python class.

50
00:01:50,300 --> 00:01:51,550
>> Well, welcome.

51
00:01:51,550 --> 00:01:52,620
>> Hi, my name's Kristin.

52
00:01:52,620 --> 00:01:55,350
This is my first course in Coursera and
will not be my last.

53
00:01:55,350 --> 00:01:57,310
I'm super excited about the course.

54
00:01:57,310 --> 00:01:58,270
>> Okay so,

55
00:02:01,650 --> 00:02:02,540
so there we are.

56
00:02:02,540 --> 00:02:05,868
Another successful office hours for
Coursera.

57
00:02:05,868 --> 00:02:08,580
I think up next we're going to Las Vegas.

58
00:02:08,580 --> 00:02:09,959
So, we'll see you in Las Vegas.

