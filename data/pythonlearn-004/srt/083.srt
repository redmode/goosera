1
00:00:01,110 --> 00:00:04,120
Hello and welcome to the podcast on

2
00:00:04,120 --> 00:00:10,080
how to screen capture on Windows,
Microsoft Windows Vista.

3
00:00:10,080 --> 00:00:15,700
So, this is pretty easy actually, because
there's a

4
00:00:15,700 --> 00:00:19,870
program does it on Windows Vista, the
snipping tool.

5
00:00:19,870 --> 00:00:24,820
So let's just say I wanted to take a
picture of this JEdit running right here.

6
00:00:24,820 --> 00:00:26,180
It doesn't really matter.

7
00:00:26,180 --> 00:00:33,750
So, I am going to write hit the Start
button and type snip.

8
00:00:33,750 --> 00:00:35,260
And there is the Snipping tool.

9
00:00:35,260 --> 00:00:37,290
And so, I will launch the Snipping tool.

10
00:00:38,880 --> 00:00:43,620
Sure I'll, I like launching adding
Snipping tool to my quick launch bar.

11
00:00:43,620 --> 00:00:48,970
And basically, it's telling me here that
the Snipping tool is

12
00:00:48,970 --> 00:00:52,130
kind of grayed out the rest of the screen,
and it's allowing

13
00:00:52,130 --> 00:00:54,870
me to do several kinds of snips, a
free-form snip

14
00:00:54,870 --> 00:00:59,314
where I draw with my cursor, a rectangular
snip, or a window snip.

15
00:00:59,314 --> 00:01:03,940
Probably the most effective is
a window snip so I'll select Window Snip.

16
00:01:03,940 --> 00:01:08,650
Changes my cursor and it says, take that.

17
00:01:08,650 --> 00:01:13,110
Now, I'm going to throw that away.

18
00:01:13,110 --> 00:01:14,440
Oh, wait a sec.

19
00:01:14,440 --> 00:01:17,330
I'm going to just say do this again.

20
00:01:17,330 --> 00:01:18,188
Where did that show up?

21
00:01:18,188 --> 00:01:19,795
Switch desktop.

22
00:01:19,795 --> 00:01:23,144
Where's the snipping, Speedy Snipping
tool, there.

23
00:01:23,144 --> 00:01:26,771
Now, all right, again.

24
00:01:26,771 --> 00:01:30,830
Snipping tool.

25
00:01:30,830 --> 00:01:31,120
Okay.

26
00:01:31,120 --> 00:01:32,580
So one thing you'll probably want to do

27
00:01:32,580 --> 00:01:36,930
before you go snipping is change the
options.

28
00:01:36,930 --> 00:01:39,010
And see how this ink selection?

29
00:01:39,010 --> 00:01:41,690
It's cool while you're cruising around
figuring out which window, but once you

30
00:01:41,690 --> 00:01:44,870
take the snip, I don't quite know why they
default this to on.

31
00:01:44,870 --> 00:01:49,570
So turn off Show ink selection after
snips are captured.

32
00:01:49,570 --> 00:01:50,340
Then say OK.

33
00:01:50,340 --> 00:01:54,240
And then say, I would like to do a window
snip.

34
00:01:54,240 --> 00:01:56,950
And now it's showing me which window it's
snipping,

35
00:01:56,950 --> 00:01:59,720
and you'll notice how I change between
them, and

36
00:01:59,720 --> 00:02:02,280
I click on the window I'd like, and now

37
00:02:02,280 --> 00:02:05,150
I don't get this funky red around the
outside.

38
00:02:05,150 --> 00:02:11,680
And I simply say Save As. I'm going to put
it on my desktop just for yucks.

39
00:02:13,670 --> 00:02:19,110
Instead of in pictures, you can stick it
anywhere, my snip and I want it

40
00:02:19,110 --> 00:02:24,550
to be JPG, that's going to make perfect
sense, and it's saving it and now I can

41
00:02:24,550 --> 00:02:29,820
close the snip program. And son of a gun, if I
have something being snipped and

42
00:02:29,820 --> 00:02:34,989
grabbed and you can click on it here and open
it up in the picture viewer.

43
00:02:34,989 --> 00:02:37,200
Just to make sure, come on, picture viewer

44
00:02:37,200 --> 00:02:43,061
[BLANK_AUDIO]

45
00:02:43,061 --> 00:02:45,759
I don't understand why the picture viewer
is

46
00:02:45,759 --> 00:02:48,457
not starting. Well, there we go. 
So there is

47
00:02:48,457 --> 00:02:51,226
is the picture viewer and so 
it looks pretty nice, and

48
00:02:51,226 --> 00:02:53,607
so if you were to have to turn this in

49
00:02:53,607 --> 00:02:55,860
Now, we've got a bunch of these because I
clicked so many times.

50
00:02:55,860 --> 00:02:59,540
If you have to turn this in, you just
upload this file. And that's

51
00:02:59,540 --> 00:03:04,720
pretty much all it takes to do screen
capture in Windows Vista, very nice.

