1
00:00:00,610 --> 00:00:02,520
Hello, and welcome to the worked

2
00:00:02,520 --> 00:00:06,480
exercise for Python For Informatics,
Exploring Information.

3
00:00:06,480 --> 00:00:08,410
I'm Charles Severance, and I'm your guide

4
00:00:08,410 --> 00:00:11,970
and guest programmer for this 
particular example.

5
00:00:12,970 --> 00:00:16,720
As always, this material is copyright
Creative Commons Attribution.

6
00:00:16,720 --> 00:00:18,250
So we've been working through a series of

7
00:00:18,250 --> 00:00:20,770
exercises that are doing time-and-a-half
for overtime and

8
00:00:20,770 --> 00:00:23,600
pay of various kinds to look at various

9
00:00:23,600 --> 00:00:26,350
if-then-else, and now we're on to
functions.

10
00:00:26,350 --> 00:00:28,020
And so, we're going to create a

11
00:00:28,020 --> 00:00:30,685
function called computepay that 
takes two parameters,

12
00:00:30,685 --> 00:00:35,590
hour and rate, and it actually does
exactly the same thing as the other one.

13
00:00:35,590 --> 00:00:38,240
It's just we're doing it in a 
different way.

14
00:00:38,240 --> 00:00:39,340
Okay?

15
00:00:39,340 --> 00:00:43,335
So, we will start.

16
00:00:43,335 --> 00:00:47,640
[BLANK_AUDIO]

17
00:00:47,640 --> 00:00:51,819
I'm running TextWrangler and the 
Terminal program.

18
00:00:51,819 --> 00:00:55,613
[BLANK_AUDIO]

19
00:00:55,613 --> 00:00:58,625
Make the Terminal program smaller and 
move it to the bottom.

20
00:00:58,625 --> 00:01:04,043
TextWrangler smaller and let's go 

21
00:01:04,043 --> 00:01:09,965
cd desktop, cd python for informatics, 

22
00:01:09,965 --> 00:01:16,260
and I've got some files here. 
Let's let's make use of hours2.py.

23
00:01:16,260 --> 00:01:18,406
So, I'm going to open hours2.py.

24
00:01:18,406 --> 00:01:22,146
Hours3.py was my, or actually, 
no, I'll use,

25
00:01:22,146 --> 00:01:26,080
I'll use hours3.py because that's my 
try-accept code.

26
00:01:26,080 --> 00:01:31,456
Okay, so the idea is we're 
supposed to encapsulate

27
00:01:31,456 --> 00:01:36,220
this bit of code in a function 
called computepay.

28
00:01:36,220 --> 00:01:37,300
So one of the things you do with
functions

29
00:01:37,300 --> 00:01:40,380
is you have to define them before 
you use them.

30
00:01:41,990 --> 00:01:45,655
So we're going to put the function up
here and use the def keyword,

31
00:01:45,655 --> 00:01:53,480
def computepay, and we're going to 
accept some parameters.

32
00:01:53,480 --> 00:01:57,470
I'm going to call this, I'm going 
to call it h and r

33
00:01:57,470 --> 00:02:01,880
just to kind of emphasize the difference
between the parameters we use inside the

34
00:02:01,880 --> 00:02:09,670
function and the arguments in the call.

35
00:02:09,670 --> 00:02:11,510
So the code that needs to be in this

36
00:02:11,510 --> 00:02:14,988
in this function is these lines 
of code right here.

37
00:02:14,988 --> 00:02:18,306
So we're going to to cut them out of
here, and then

38
00:02:18,306 --> 00:02:22,712
we're going to indent them, make them the
body of the function.

39
00:02:22,712 --> 00:02:28,123
[BLANK_AUDIO]

40
00:02:28,123 --> 00:02:34,009
Okay, so, now the variables in this
function are not h, are not

41
00:02:34,009 --> 00:02:40,780
hours and rate, they're p, I'm going 
to just change them all to this.

42
00:02:40,780 --> 00:02:41,870
This, I'm just changing this to

43
00:02:41,870 --> 00:02:44,370
kind of emphasize the difference 
between these variables

44
00:02:44,370 --> 00:02:50,568
that live in the function and the
variables that live in the calling code.

45
00:02:50,568 --> 00:02:55,129
So in the function they're called h and r
and p, and the only one other thing I'm

46
00:02:55,129 --> 00:02:58,951
going to do is I'm going to put 
a return statement in

47
00:02:58,951 --> 00:03:02,159
to return p as the result of the
function.

48
00:03:02,159 --> 00:03:05,069
And so Python will come in,
it'll see the def,

49
00:03:05,069 --> 00:03:07,979
it'll read all this stuff, it
won't do it.

50
00:03:07,979 --> 00:03:11,229
Then the first executable says try, it
still works the same way,

51
00:03:11,229 --> 00:03:20,300
and I'm going to change this to be 
In the main code.

52
00:03:20,740 --> 00:03:22,610
Rate and hours.

53
00:03:22,610 --> 00:03:25,513
Then I'm going to put a print statement
just for yuckydoodles.

54
00:03:28,513 --> 00:03:36,097
print In computepay,

55
00:03:40,097 --> 00:03:44,370
and print out h and r.

56
00:03:45,730 --> 00:03:51,031
And then before I do my return here, I'm
going to say

57
00:03:53,031 --> 00:04:03,727
[SOUND] And I'll print out, I'm 
going to print out

58
00:04:03,727 --> 00:04:08,550
what I'm sending back and print out 
the computed pay.

59
00:04:08,550 --> 00:04:11,498
And then, now the only thing I've 
got to do left is,

60
00:04:11,498 --> 00:04:15,140
I've got the rate and the hours, and
I'm going to call my function.

61
00:04:15,140 --> 00:04:21,660
So, I'm going to say pay equals
computepay, or invoke the function,

62
00:04:21,660 --> 00:04:25,770
and pass in rate and hours 
as my arguments.

63
00:04:25,770 --> 00:04:27,790
And then print it out.

64
00:04:27,790 --> 00:04:29,242
So here's the main code.

65
00:04:29,242 --> 00:04:37,380
Oop, well, I'll just say We are back, 
and then print out pay.

66
00:04:37,380 --> 00:04:41,846
So I have a print before and 
after the call to computepay.

67
00:04:41,846 --> 00:04:47,129
And I have a print once computepay
starts and when computepay finishes.

68
00:04:50,400 --> 00:04:53,232
I have to sort of reorganize my 
screen a little bit here.

69
00:04:53,232 --> 00:04:57,984
[BLANK_AUDIO]

70
00:04:57,984 --> 00:04:58,710
So we can see them all.

71
00:05:01,990 --> 00:05:06,110
Ooh, I gotta save this as something else
so I don't ruin hours2.

72
00:05:06,110 --> 00:05:10,270
I'll call this hours4.py.

73
00:05:10,270 --> 00:05:16,303
Now I'll say python hours4.py.

74
00:05:16,303 --> 00:05:17,619
[INAUDIBLE]

75
00:05:17,619 --> 00:05:26,330
[BLANK_AUDIO]

76
00:05:26,330 --> 00:05:29,694
Okay, [COUGH] so I'll type, so what's
happened here is it's typing

77
00:05:29,694 --> 00:05:33,700
Enter Hours, so it's on this
line here, line eleven.

78
00:05:33,700 --> 00:05:38,008
It's already made it all the way 
through here, okay?

79
00:05:38,008 --> 00:05:39,990
Now I'll have a print statement in 
a second here.

80
00:05:41,100 --> 00:05:43,075
So, so, so, there we go.

81
00:05:43,075 --> 00:05:45,430
It's made it through, it hasn't done any
work but it remembered

82
00:05:45,430 --> 00:05:48,810
this code and stored it under the name
computepay and it's here.

83
00:05:48,810 --> 00:05:50,651
So now I'll do hours.

84
00:05:50,651 --> 00:05:52,660
40 hours and $10.

85
00:05:52,660 --> 00:05:54,572
So I'm running through this 
code right here.

86
00:05:54,572 --> 00:05:56,400
And then I'm going here next.

87
00:05:56,400 --> 00:05:58,425
Because we're not going to bad, 
put bad hours in.

88
00:05:58,425 --> 00:06:05,090
So the next line that happened is 
In the main code, which is here.

89
00:06:05,090 --> 00:06:08,360
Then it called computepay, right there,

90
00:06:08,360 --> 00:06:17,864
so it got here, and h is 10 
and r is 40.

91
00:06:17,864 --> 00:06:21,808
It doesn't quite look right, but the
computepay, we're done,

92
00:06:21,808 --> 00:06:24,210
and that's 400, and 
everything is cool.

93
00:06:24,210 --> 00:06:25,842
So it's back, right?

94
00:06:25,842 --> 00:06:34,712
[BLANK_AUDIO]

95
00:06:34,712 --> 00:06:35,979
So, let's do this.

96
00:06:35,979 --> 00:06:37,910
Let's do another one.

97
00:06:39,080 --> 00:06:44,771
And I'm putting 50 hours and $10 an hour.

98
00:06:44,771 --> 00:06:49,780
Now that one should have overtime,
because we put in 50 hours, but it's not.

99
00:06:49,780 --> 00:06:51,525
So what is the problem here?

100
00:06:54,525 --> 00:06:55,620
Ten and 50 are coming in.

101
00:06:59,340 --> 00:07:00,620
We, we're getting the numbers right.

102
00:07:00,620 --> 00:07:03,750
We're going into computepay as 10 and 50,
and then we're coming back.

103
00:07:05,680 --> 00:07:07,170
Hmm.

104
00:07:07,170 --> 00:07:11,145
So, we're printing out the hours, 
the h and the r are variable.

105
00:07:11,145 --> 00:07:15,224
[BLANK_AUDIO]

106
00:07:15,224 --> 00:07:17,160
Oh, ooh, ooh, ooh, ooh, 
I see it, I see it.

107
00:07:18,620 --> 00:07:25,840
Mistake is right here and right here.

108
00:07:25,840 --> 00:07:26,500
Can you see it?

109
00:07:29,040 --> 00:07:33,550
We have, these are parameters, 
and these are arguments.

110
00:07:33,550 --> 00:07:35,829
These are the real variables,
rate and hours.

111
00:07:36,970 --> 00:07:38,900
The parameters are h and r.

112
00:07:40,330 --> 00:07:43,307
We got two parameters, but we've 
got the order wrong.

113
00:07:43,307 --> 00:07:45,701
[BLANK_AUDIO]

114
00:07:45,701 --> 00:07:53,540
So, here I have rate first, so 
h here is rate.

115
00:07:53,540 --> 00:07:57,139
And look, if we look at what we print out
here in computepay, the first

116
00:07:57,139 --> 00:08:01,010
thing we print out is h, but it's really
10 which is supposed to be rate.

117
00:08:02,170 --> 00:08:04,140
Oops, so I gotta fix this.

118
00:08:04,140 --> 00:08:06,730
That should be r to correspond to the
first parameter,

119
00:08:06,730 --> 00:08:09,990
and that should be h to correspond 
to the second parameter.

120
00:08:09,990 --> 00:08:13,210
And just to keep my sanity, I'm 
going to print out r comma h,

121
00:08:13,210 --> 00:08:17,010
so I'll say rate, hours 
just to be consistent.

122
00:08:17,010 --> 00:08:18,870
Actually no, no, no, no.

123
00:08:18,870 --> 00:08:22,930
Since I'm prompting for 
hours and rate, h comma r.

124
00:08:22,930 --> 00:08:30,310
I'll change this to h comma r, 
and then make this be

125
00:08:30,310 --> 00:08:35,100
hours comma rate. I think it makes
more sense for the hours to be first.

126
00:08:35,100 --> 00:08:37,000
I don't know why that makes more sense,

127
00:08:37,000 --> 00:08:40,500
but now we're corresponding our mnemonic
variables inside of our functions,

128
00:08:40,500 --> 00:08:43,960
our h and r, that allude to hours and rate.

129
00:08:43,960 --> 00:08:45,930
But they're not the same as 
hours and rate, so

130
00:08:45,930 --> 00:08:50,420
h is an alias to hours when 
this is called.

131
00:08:50,420 --> 00:08:51,280
Okay?

132
00:08:51,280 --> 00:08:53,120
So now let's run it again and 
see if she works.

133
00:08:55,430 --> 00:09:02,874
So I'll do 50 hours, $10 an hour.

134
00:09:02,874 --> 00:09:07,456
Oh, I'll bet I didn't save it.

135
00:09:07,456 --> 00:09:08,796
[MUSIC]

136
00:09:08,796 --> 00:09:11,058
50 hours, $10 an hour, yay it works,

137
00:09:11,058 --> 00:09:15,230
it works, it works, it works, we've got
ourselves our overtime.

138
00:09:15,230 --> 00:09:20,510
Okay, so, so I'm going to say h equals.

139
00:09:21,540 --> 00:09:26,130
Then I'm going to add r equals here, 
just so that we know

140
00:09:26,130 --> 00:09:29,480
which rate and what h and r are.

141
00:09:29,480 --> 00:09:31,358
Just add some strings there.

142
00:09:31,358 --> 00:09:33,406
[COUGH] Run it again.

143
00:09:33,406 --> 00:09:35,026
If I'd have done that in the 
first place, I

144
00:09:35,026 --> 00:09:38,660
probably would not have made that, 
seen that mistake much faster.

145
00:09:38,660 --> 00:09:43,750
So, we've got 50 hours and $10 per hour,
and so it's working.

146
00:09:43,750 --> 00:09:48,620
So just to sort of continue on here
on this, I could do this.

147
00:09:49,620 --> 00:09:55,160
I could call this function again, and I
could say, just to show you how I'm

148
00:09:55,160 --> 00:09:59,320
reusing this code, I'm going to make a
variable named z,


149
00:09:59,320 --> 00:10:06,877
And I'm going to say 12.2 is my parameter.
And my second parameter is $10 an hour.

150
00:10:06,877 --> 00:10:10,889
And so now I'm using constants instead of
variables and you will see that it's

151
00:10:10,889 --> 00:10:14,311
going to call computepay once this time
and h and r are going to be hours

152
00:10:14,311 --> 00:10:18,028
and rate the first time, and the second
time they're going to be called again

153
00:10:18,028 --> 00:10:21,880
but 12 and 10 are going to be what 
h and r are the second time.

154
00:10:21,880 --> 00:10:27,678
And then print ZZZZ, comma z.

155
00:10:27,678 --> 00:10:29,100
That's just a silly message.

156
00:10:30,940 --> 00:10:31,440
Okay?

157
00:10:33,110 --> 00:10:36,600
So, we're going to, this, 
this stuff is unchanged,

158
00:10:36,600 --> 00:10:38,210
it just prompts and gets us the rate.

159
00:10:38,210 --> 00:10:40,750
We're going to call computepay 
twice this time.

160
00:10:40,750 --> 00:10:45,050
One time with hours and rate, and the
second time with 12.2 and 10.0.

161
00:10:45,050 --> 00:10:46,590
So lets take a look at how that runs.

162
00:10:48,440 --> 00:10:53,380
Right, may, may need a little longer, well
let's see, 50 hours, $10 an hour.

163
00:10:55,800 --> 00:11:00,960
Oh yeah, it would help for me to save,
help for me to save.

164
00:11:00,960 --> 00:11:02,460
Don't make that mistake.

165
00:11:02,460 --> 00:11:04,681
50 hours, $10 an hour.

166
00:11:04,681 --> 00:11:07,471
Okay. So In the main code, then it

167
00:11:07,471 --> 00:11:12,521
then it calls computepay and 
does one computation

168
00:11:12,521 --> 00:11:15,311
and then it's here, it's back 
in the main code,

169
00:11:15,311 --> 00:11:19,290
right there, and then it calls
computepay again.

170
00:11:19,290 --> 00:11:24,790
This time the variables h and r, oops,
this times the variables the h and r

171
00:11:24,790 --> 00:11:30,190
are 12.2 and 10.0. The last time, 
h and r were 50 and 10.

172
00:11:30,190 --> 00:11:31,050
Okay?

173
00:11:31,050 --> 00:11:37,140
The key is that this h and r, the
parameters in the function, are ephemeral.

174
00:11:37,140 --> 00:11:38,875
They come and they go.

175
00:11:38,875 --> 00:11:42,290
computepay works with different, 
different values each time.

176
00:11:42,290 --> 00:11:47,410
But it uses h and r internally 
as kind of its scratch area for this.

177
00:11:47,410 --> 00:11:50,270
Let me see if I can get this all on 
one screen. Yes I can.

178
00:11:50,270 --> 00:11:53,690
Okay, so this just is showing we're
running it twice

179
00:11:53,690 --> 00:11:56,760
once h is 50 and the next time it's 12.

180
00:11:56,760 --> 00:11:58,610
Because it's simply the first parameter.

181
00:11:58,610 --> 00:12:03,132
And the first time the parameter was 50, 
the second time it was 12.2.

182
00:12:03,132 --> 00:12:04,030
Okay?

183
00:12:04,030 --> 00:12:05,330
So, let's just walk through,

184
00:12:07,870 --> 00:12:09,680
scribbling what's going on here.

185
00:12:09,680 --> 00:12:11,900
Just, we'll just kind of go through this.

186
00:12:11,900 --> 00:12:14,955
So Python starts at the beginning and
sees def, and it says

187
00:12:14,955 --> 00:12:19,800
oh, don't run this code, remember
it, name it computepay.

188
00:12:20,060 --> 00:12:24,450
So h and r don't exist, they're not, they
don't exist until the function is invoked.

189
00:12:24,450 --> 00:12:25,310
Then we get to the try.

190
00:12:27,130 --> 00:12:29,010
We've got some insurance on 
these four lines.

191
00:12:29,010 --> 00:12:31,764
We read our, and convert our input, and
our rate, our hours

192
00:12:31,764 --> 00:12:34,590
and our rate, and that's good, so 
we don't run the except.

193
00:12:36,220 --> 00:12:38,071
Then we print In the main code.

194
00:12:38,071 --> 00:12:39,811
Okay?

195
00:12:39,811 --> 00:12:44,243
And then we come to this line and 
we see computepay, and so that

196
00:12:44,243 --> 00:12:50,513
goes up here [SOUND] and h and r are 
hours and rate and it runs this code.

197
00:12:50,513 --> 00:12:57,420
It returns p, and that p comes back as the
value here and ends up being put into pay.

198
00:12:57,420 --> 00:12:59,571
Then we print We are back.

199
00:12:59,571 --> 00:13:03,443
Then we run into this statement and we
have to go call computepay

200
00:13:03,443 --> 00:13:08,240
so it goes up again and this time 
h and r are 12 and 10.

201
00:13:08,240 --> 00:13:12,035
It runs through the code, blah, blah, blah,
blah, blah, and then

202
00:13:12,035 --> 00:13:16,117
returns 122 and that returns to this call,
not the previous one.

203
00:13:16,117 --> 00:13:20,320
And then that puts 122 into z. 
And then the program continues on.

204
00:13:21,330 --> 00:13:27,170
So, it, again this does nothing and it
calls it twice, and has two sets

205
00:13:27,170 --> 00:13:32,710
of arguments and the parameters represent
different arguments in the different calls.

206
00:13:32,710 --> 00:13:33,800
Okay?

207
00:13:33,800 --> 00:13:37,240
So let me just clean this up 
one last time and

208
00:13:37,240 --> 00:13:40,294
get rid of the, oops, get rid of 
the debug print statements.

209
00:13:41,530 --> 00:13:42,410
Get rid of that.

210
00:13:44,576 --> 00:13:47,371
Get rid of this.

211
00:13:47,371 --> 00:13:50,420
So you can see what it's really 
supposed to look like.

212
00:13:50,420 --> 00:13:51,710
We don't need that.

213
00:13:51,710 --> 00:13:56,328
Well, I'll just comment that one out to
keep it in case of later.

214
00:13:56,328 --> 00:14:00,420
This is debug print, so I'll keep that
and get rid of those two.

215
00:14:00,420 --> 00:14:01,990
So now it doesn't do any printing.

216
00:14:03,230 --> 00:14:06,165
So let's run it one more time.

217
00:14:06,165 --> 00:14:10,284
40, 10, good stuff.

218
00:14:10,284 --> 00:14:13,082
50, 10, good stuff.

219
00:14:13,082 --> 00:14:14,178
So, it works.

220
00:14:14,178 --> 00:14:20,320
So, there's the, there's the ultimate
program and it looks pretty good.

