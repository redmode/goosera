1
00:00:00,570 --> 00:00:02,910
I have so many people to thank who have
helped me with

2
00:00:02,910 --> 00:00:06,480
this course that it's going to take an
entire video all by itself.

3
00:00:06,480 --> 00:00:08,780
The course starts with the book Python
for Informatics.

4
00:00:08,780 --> 00:00:12,364
This book starts over ten years ago, when
Allen Downey wrote a book

5
00:00:12,364 --> 00:00:15,884
about Java and made that an open book. And
then Jeffrey Elkner, who

6
00:00:15,884 --> 00:00:19,340
is a high school teacher in Virginia, he
took that and turned it

7
00:00:19,340 --> 00:00:24,170
into a Python book. And then Allen Downey
switched from teaching Java to Python.

8
00:00:24,170 --> 00:00:26,390
And then he took his own book back, and

9
00:00:26,390 --> 00:00:28,800
everyone kept editing this book and making
it better.

10
00:00:28,800 --> 00:00:30,170
It's called Think Python.

11
00:00:30,170 --> 00:00:33,901
But they published it with an open, free,
remixable license.

12
00:00:33,901 --> 00:00:38,739
And so a couple years ago, I was teaching
SI 502 and needed a book.

13
00:00:38,739 --> 00:00:40,515
And I thought I would write my own book,
but a

14
00:00:40,515 --> 00:00:43,930
colleague of mine, Atul Prakash, said
you should look at Think Python.

15
00:00:43,930 --> 00:00:44,340
So I did.

16
00:00:44,340 --> 00:00:46,450
There were lots of things I liked about
the book.

17
00:00:46,450 --> 00:00:48,180
It was short and to the point.

18
00:00:48,180 --> 00:00:50,490
But it had a few too many computer
science things in it,

19
00:00:50,490 --> 00:00:53,820
so I started rewriting the book and
changing the parts that I didn't want.

20
00:00:53,820 --> 00:00:58,360
And I was able to modify that book
dramatically in about 30 days.

21
00:00:58,360 --> 00:01:01,324
And by January, I was able to produce a
book in

22
00:01:01,324 --> 00:01:06,010
time for my class. But the problem was I
needed physical books.

23
00:01:06,010 --> 00:01:09,030
And that's where folks from the University
of Michigan Libraries came in.

24
00:01:09,030 --> 00:01:12,440
Terri Geitgey runs a facility called the
Espresso Book Machine,

25
00:01:12,440 --> 00:01:15,974
which is an on-demand print service, which
basically meant that I

26
00:01:15,974 --> 00:01:18,888
could walk in with a print-ready PDF,
and then they

27
00:01:18,888 --> 00:01:21,870
would be able to print that in
just a few hours.

28
00:01:21,870 --> 00:01:26,570
And so in less than 30 days, I went from
no book to physical books.

29
00:01:26,570 --> 00:01:28,360
All with a bunch, help from a bunch of
people.

30
00:01:30,080 --> 00:01:34,160
In addition to the books, Open Michigan
has helped me.

31
00:01:34,160 --> 00:01:35,960
Open Michigan is our group on

32
00:01:35,960 --> 00:01:39,130
campus that's responsible for open
education resources.

33
00:01:39,130 --> 00:01:42,485
And, working with folks like Dave
Malicke, I've been trained

34
00:01:42,485 --> 00:01:46,310
to build copyright-free materials, both
the book and the slides.

35
00:01:46,310 --> 00:01:50,402
Because we teachers are always dragging
stuff from Google and sticking it

36
00:01:50,402 --> 00:01:54,920
in our, in our slides and and violating
copyright without even knowing it.

37
00:01:54,920 --> 00:01:59,180
And so I've been trained to make slides
that I can actually release to you,

38
00:01:59,180 --> 00:02:03,500
and in a way that makes it so that you can
reuse them without violating copyright.

39
00:02:04,740 --> 00:02:06,700
Another thing that I need to say thanks to is

40
00:02:06,700 --> 00:02:08,630
the folks that built the iBooks version of
the book.

41
00:02:08,630 --> 00:02:11,740
Jason Coleman, of the University of
Michigan Library, he built that.

42
00:02:11,740 --> 00:02:15,380
It took a long time. It's got awesome
videos, it's got in-book exercises,

43
00:02:15,380 --> 00:02:19,280
etc. It's only for the iBook, but it's
free, and it's really quite awesome.

44
00:02:20,306 --> 00:02:23,314
University of Michigan leadership around
our Coursera

45
00:02:23,314 --> 00:02:28,020
effort, Martha Pollock, James Hilton,
Gautam Kaul, James Devaney,

46
00:02:28,020 --> 00:02:31,840
all have given us an environment where
it's fun to explore and to

47
00:02:31,840 --> 00:02:36,390
play and experiment with what it means to
work in this new environment.

48
00:02:36,390 --> 00:02:40,000
Tim O'Brien, who's right behind the
camera, runs our facility here.

49
00:02:40,000 --> 00:02:42,706
And it's a lot of fun to be part of this
and have all

50
00:02:42,706 --> 00:02:47,280
these neat lights and cameras and stuff to
play with building materials for you.

51
00:02:47,280 --> 00:02:48,905
My management at the School of

52
00:02:48,905 --> 00:02:51,505
Information, Deans Tom Finholt and Beth Yakel,

53
00:02:51,505 --> 00:02:55,405
were very helpful in getting this course to
come to live because there's lot

54
00:02:55,405 --> 00:02:58,915
of paperwork to fill out on campus, lots
of procedures to do it,

55
00:02:58,915 --> 00:03:00,995
and so having management that's very

56
00:03:00,995 --> 00:03:04,510
supportive and helpful is really very
essential.

57
00:03:04,510 --> 00:03:07,060
And my fellow faculty members, like Atul
Prakash, who

58
00:03:07,060 --> 00:03:09,600
helped me get started on the Think Python
book.

59
00:03:09,600 --> 00:03:12,430
And Tanya Rosenblat and Paul Resnik both

60
00:03:12,430 --> 00:03:15,060
have used early versions of my software
and sort

61
00:03:15,060 --> 00:03:17,510
of tolerated as I got my software working,

62
00:03:17,510 --> 00:03:19,820
the software that we're now using in the
course.

63
00:03:19,820 --> 00:03:23,380
And Michael Hess is one of our technical
support people.

64
00:03:23,380 --> 00:03:27,640
As well as a fellow faculty member here at
the School of Information.

65
00:03:27,640 --> 00:03:29,432
And he has given me access to a very

66
00:03:29,432 --> 00:03:31,992
large server that helps in making sure
that I can

67
00:03:31,992 --> 00:03:34,872
use the technical resources that I need at
no charge,

68
00:03:34,872 --> 00:03:38,370
so that we don't have to charge for the
course.

69
00:03:38,370 --> 00:03:40,990
The Skulpt software is an essential part
of this class.

70
00:03:40,990 --> 00:03:43,460
It's also a part of the Rice class as well.

71
00:03:43,460 --> 00:03:46,500
It is what allows you to write Python and
run it in the browser.

72
00:03:46,500 --> 00:03:48,750
It itself is an amazing piece of
technology.

73
00:03:48,750 --> 00:03:50,410
And the leader for that is a fellow named

74
00:03:50,410 --> 00:03:54,190
Brad Miller, from Luther College, who also
teaches Python there.

75
00:03:54,190 --> 00:03:56,728
And literally without Brad, I don't think
there would

76
00:03:56,728 --> 00:03:58,710
be any way for me to have built this
course.

77
00:04:00,020 --> 00:04:03,674
I need to thank everybody from Coursera
who's helped me and mentored

78
00:04:03,674 --> 00:04:07,340
me and helped me understand what it means
to teach at scale.

79
00:04:07,340 --> 00:04:09,760
Daphne and Andrew have been very
supportive.

80
00:04:09,760 --> 00:04:12,350
I completely agree with all of the things
that we want to do

81
00:04:12,350 --> 00:04:16,970
in terms of getting this teaching as
widely distributed as we possibly can.

82
00:04:16,970 --> 00:04:20,392
Pang Wei, early on, was one of the good
mentors that really helped me

83
00:04:20,392 --> 00:04:24,990
understand that teaching at scale is not
the same as teaching in the classroom.

84
00:04:24,990 --> 00:04:29,120
How assessment needs to be different, how
interaction needs to be different.

85
00:04:29,120 --> 00:04:31,870
Kate McShane Urban is our current, she's
like our, she

86
00:04:31,870 --> 00:04:35,850
watches over everything that goes on
and makes sure that everything works.

87
00:04:35,850 --> 00:04:38,110
I've made a number of really nice friends
on the technical

88
00:04:38,110 --> 00:04:40,810
team who have helped me build the learning
tools interoperability

89
00:04:40,810 --> 00:04:45,930
that lets me plug things like maps and
autograders and just plug stuff in.

90
00:04:45,930 --> 00:04:48,800
Brandon Saeta and Nick Dellamaggiore are
two of

91
00:04:48,800 --> 00:04:50,840
the folks that have really helped make all
that work.

92
00:04:50,840 --> 00:04:55,990
They're also responsible for many of the
integration strategies around Coursera.

93
00:04:55,990 --> 00:04:56,880
And so that's really cool.

94
00:04:58,050 --> 00:05:01,130
Another group that I need to thank comes
from IEEE Computer Magazine.

95
00:05:01,130 --> 00:05:04,532
I'm an author, column author for IEEE
Computer Magazine, and

96
00:05:04,532 --> 00:05:09,020
I write this monthly column
called Computing Conversations.

97
00:05:09,020 --> 00:05:11,780
And it's part of what funds me to travel
around and

98
00:05:11,780 --> 00:05:15,090
make these videos that I share with you in
the class.

99
00:05:15,090 --> 00:05:18,460
The chief editor, Ron Vetter;
Jennifer Stout who's a, does

100
00:05:18,460 --> 00:05:22,000
most of the editing of the columns, and
Kerry Clark Walsh.

101
00:05:22,000 --> 00:05:24,242
What's cool about IEEE Computer is these
are

102
00:05:24,242 --> 00:05:27,133
copyrighted materials, but they have given
me permission

103
00:05:27,133 --> 00:05:29,021
to share them with you at no charge

104
00:05:29,021 --> 00:05:31,770
because they just want you to have this
information.

105
00:05:31,770 --> 00:05:37,500
I also need to thank Sue, Mazen, and Mauro,
my Community Teaching Assistants.

106
00:05:37,500 --> 00:05:38,820
They have been with me since I

107
00:05:38,820 --> 00:05:41,570
started Internet History, Technology, and
Security and

108
00:05:41,570 --> 00:05:43,770
for Programming for Everybody, they have been

109
00:05:43,770 --> 00:05:46,840
very deeply involved in the
behind-the-scenes preparation.

110
00:05:46,840 --> 00:05:51,780
And so months before the class started,
they were looking over my materials.

111
00:05:51,780 --> 00:05:54,435
And even though the materials have, always
have room

112
00:05:54,435 --> 00:05:57,208
for improvement, the fact that they're as
good as they

113
00:05:57,208 --> 00:05:59,745
are when we, when we release them is is due

114
00:05:59,745 --> 00:06:03,012
in a large part to the Community Teaching
Assistants' help.

115
00:06:03,012 --> 00:06:05,352
I also need to thank all the students

116
00:06:05,352 --> 00:06:08,862
in my classes who have, who've dealt with

117
00:06:08,862 --> 00:06:12,230
draft versions of books and half-finished
software, like SI 182,

118
00:06:12,230 --> 00:06:16,940
SI 502, SI 301, SI 364, and SI 664.

119
00:06:16,940 --> 00:06:20,120
In many ways, they were the guinea pigs
for the student experience.

120
00:06:22,100 --> 00:06:27,110
But, really, the most important folks to
thank are you, the Coursera students.

121
00:06:28,220 --> 00:06:34,180
I just went through 10 or 15 people's
names, and they helped me.

122
00:06:34,180 --> 00:06:36,410
They helped produce what you're
experiencing.

123
00:06:38,720 --> 00:06:40,640
I don't think they help me just because
they like me.

124
00:06:40,640 --> 00:06:43,380
They help me because it's kind of the
nature of what we do.

125
00:06:43,380 --> 00:06:46,780
We are at an institution that values
teaching.

126
00:06:46,780 --> 00:06:49,885
And when someone like me comes to a fellow
person at the

127
00:06:49,885 --> 00:06:54,216
University of Michigan and says, I need help
to make my teaching better,

128
00:06:54,216 --> 00:06:57,633
people help. And frankly, we've been doing this

129
00:06:57,633 --> 00:07:00,849
for quite some time and what's really neat now,
is  we're doing it

130
00:07:00,849 --> 00:07:04,462
for a much larger group of people. And so it's

131
00:07:04,462 --> 00:07:08,080
really you, the Coursera students, that
makes all this effort worthwhile.

132
00:07:08,080 --> 00:07:12,300
So, in closing, I just want to thank you,
and I'll see you in class.

