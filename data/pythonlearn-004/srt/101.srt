1
00:00:00,700 --> 00:00:04,310
Hello and welcome to Programming for
Everybody. My name's Charles Severance I'm

2
00:00:04,310 --> 00:00:05,910
a Clinical Associate Professor at the

3
00:00:05,910 --> 00:00:08,620
University of Michigan School of
Information.

4
00:00:08,620 --> 00:00:11,040
And I'm your instructor for this class.

5
00:00:11,040 --> 00:00:12,040
I'm glad you're here.

6
00:00:12,040 --> 00:00:13,910
So the first thing I always like to talk
about when

7
00:00:13,910 --> 00:00:16,290
I teach a class is who should take 
the class.

8
00:00:17,570 --> 00:00:19,690
This class is a beginning class.

9
00:00:19,690 --> 00:00:22,500
This class is a beginning class for
beginners.

10
00:00:22,500 --> 00:00:24,050
This class is a beginning class for

11
00:00:24,050 --> 00:00:27,000
people who don't know anything about
programming.

12
00:00:27,000 --> 00:00:29,940
And so, I am committed to teaching you

13
00:00:29,940 --> 00:00:33,330
the basics, and not assuming that 
you know anything.

14
00:00:33,330 --> 00:00:34,650
There's no math in this.

15
00:00:34,650 --> 00:00:37,350
There's a light amount of work every week.

16
00:00:37,350 --> 00:00:40,050
I'm focussed more on learning 
than I am about

17
00:00:40,050 --> 00:00:42,140
sort of getting you to be a professional
programmer.

18
00:00:43,620 --> 00:00:45,910
So I also like to talk a little bit
about who I am.

19
00:00:47,420 --> 00:00:50,700
If you like, there is a URL here for the
TED talk

20
00:00:50,700 --> 00:00:52,770
that I gave, the TEDx talk that I gave
about MOOCs.

21
00:00:52,770 --> 00:00:55,880
And you can kind of see the philosophy of
how I do MOOCs.

22
00:00:55,880 --> 00:00:58,260
I think I might have been wearing 
this exact same jacket

23
00:00:58,260 --> 00:01:00,960
and this exact same shirt for the MOOC.

24
00:01:00,960 --> 00:01:02,985
So I won't go into that.

25
00:01:02,985 --> 00:01:06,890
You'll see that I am very
passionate about a few things.

26
00:01:06,890 --> 00:01:10,650
One of the things that I am very
passionate about is open.

27
00:01:10,650 --> 00:01:15,530
And I wrote a book about an open
source project called Sakai, which

28
00:01:15,530 --> 00:01:17,660
is a open source learning 
management system

29
00:01:17,660 --> 00:01:19,400
that's used by many people 
around the world.

30
00:01:19,400 --> 00:01:22,920
I am committed enough that I 
even have a tattoo.

31
00:01:22,920 --> 00:01:29,050
I have, my research area is teaching and
learning tools and expanding sort

32
00:01:29,050 --> 00:01:33,220
of the ways we can use technology to teach
in better and better ways.

33
00:01:33,220 --> 00:01:36,143
And my work with Sakai as well 
as my work

34
00:01:36,143 --> 00:01:40,140
with IMS Learning Tools Interoperability
is part of that.

35
00:01:40,140 --> 00:01:42,870
This tattoo that you see has Sakai
at the middle.

36
00:01:42,870 --> 00:01:45,180
And it has, in a circle, all of the

37
00:01:45,180 --> 00:01:48,530
the, my collaborators that have
implemented

38
00:01:48,530 --> 00:01:51,268
IMS Learning Tools Interoperability.

39
00:01:51,268 --> 00:01:53,289
Although there's hundreds of others, 
there's only

40
00:01:53,289 --> 00:01:55,000
a few that got tattoos, and at Coursera.

41
00:01:55,000 --> 00:01:58,640
And we're going to be using it in this
class, also implements

42
00:01:58,640 --> 00:02:02,840
this IMS Learning Tools Interoperability,
and so you will see how I can

43
00:02:02,840 --> 00:02:06,620
connect tools that I build, plug them
deeply into Coursera,

44
00:02:06,620 --> 00:02:10,030
and really improve, I think, the
pedagogy of my course.

45
00:02:12,170 --> 00:02:13,690
If you want to get a sense of my

46
00:02:13,690 --> 00:02:17,660
sense of humor, just google iPad 
Steering Wheel Mount.

47
00:02:17,660 --> 00:02:20,260
Some people think this is serious.

48
00:02:20,260 --> 00:02:24,170
I call it satire, but I think 
good satire is the

49
00:02:24,170 --> 00:02:27,890
satire that you can't always tell 
if it's satire or not.

50
00:02:27,890 --> 00:02:32,840
So, I want to talk a little bit about
my personal very first programming

51
00:02:32,840 --> 00:02:38,239
experience because it really has colored my
approach to teaching from that very first moment.

52
00:02:39,460 --> 00:02:43,270
So I came out of high school in 1975.

53
00:02:43,270 --> 00:02:46,660
And, we didn't have computers, the family
didn't have a computer.

54
00:02:46,660 --> 00:02:48,950
I did have a hand calculator in 
my senior year

55
00:02:48,950 --> 00:02:50,610
in high school and I thought that was
pretty amazing.

56
00:02:50,610 --> 00:02:54,680
And so I went to college in 
the fall of 1975

57
00:02:54,680 --> 00:02:58,720
and an advisor said you should take one of
these programming things.

58
00:02:58,720 --> 00:03:01,240
We're putting everybody into the
programming class.

59
00:03:01,240 --> 00:03:04,190
And I said oh whatever, 
I don't know, sure.

60
00:03:04,190 --> 00:03:08,610
So, I was a very good student in high
school but in college I was

61
00:03:08,610 --> 00:03:12,250
amazed that they would let me skip
class and they never took attendance.

62
00:03:12,250 --> 00:03:15,460
And I thought I should experiment 
with how well this works.

63
00:03:15,460 --> 00:03:18,580
And this programming class was at 
10:30 in the morning.

64
00:03:18,580 --> 00:03:21,570
And so I was usually very tired 
and I didn't really

65
00:03:21,570 --> 00:03:23,550
want to get up in time to go 
to a 10:30 class.

66
00:03:23,550 --> 00:03:24,940
So I started skipping the classes.

67
00:03:25,950 --> 00:03:28,320
And one time I had that thing that we're

68
00:03:28,320 --> 00:03:30,330
all afraid of when we're attending a
class, and

69
00:03:30,330 --> 00:03:35,140
that is I showed up at a midterm and I
didn't know it was the day of the midterm.

70
00:03:35,140 --> 00:03:39,880
And they handed me a piece of paper and I
had no idea what any of the words meant

71
00:03:39,880 --> 00:03:41,850
because I'd skipped so many classes.

72
00:03:41,850 --> 00:03:44,640
But somehow after that I somehow
recovered.

73
00:03:44,640 --> 00:03:47,240
And, and a few short weeks later

74
00:03:47,240 --> 00:03:49,460
I actually figured out what 
those words meant.

75
00:03:49,460 --> 00:03:54,790
And then I realized, as any 17-year-old
might, that this programming thing

76
00:03:54,790 --> 00:04:00,130
is kind of fun, because if I communicate
properly with this device

77
00:04:00,130 --> 00:04:04,730
it will do whatever I say. And I did a lot
of cool stuff and I caught

78
00:04:04,730 --> 00:04:06,920
up in the class and became one of the

79
00:04:06,920 --> 00:04:10,030
best students in the class, all in one
semester.

80
00:04:10,030 --> 00:04:14,850
And at the end, I had pulled my grade up
from a Fail to a D.

81
00:04:16,680 --> 00:04:20,010
But at the same time I was hooked, and I
immediately changed my major to computer

82
00:04:20,010 --> 00:04:21,710
science, and then I went on to finish

83
00:04:21,710 --> 00:04:25,730
my Bachelor's, Master's, and PhD, all
inside computer science.

84
00:04:27,080 --> 00:04:29,200
And so I understand that when you

85
00:04:29,200 --> 00:04:32,520
first meet programming, you're likely to
be confused.

86
00:04:32,520 --> 00:04:35,710
I know I was confused, and I turned out to
be pretty good at it.

87
00:04:35,710 --> 00:04:38,150
But my first reaction was how 
confused I was.

88
00:04:38,150 --> 00:04:41,130
So accept the fact that you're 
going to be confused.

89
00:04:41,130 --> 00:04:43,120
Relax, enjoy it.

90
00:04:43,120 --> 00:04:44,600
You're going to learn a lot.

91
00:04:44,600 --> 00:04:48,420
And you might actually really like this
even though you think you're not going to.

92
00:04:48,420 --> 00:04:50,610
So let's talk a little bit about the
course.

93
00:04:51,790 --> 00:04:52,990
The course is really based on a

94
00:04:52,990 --> 00:04:56,670
textbook I have written called 
Python for Infomatics.

95
00:04:56,670 --> 00:05:00,100
And this book is 100% free.

96
00:05:00,100 --> 00:05:07,970
Every copy is, all the electronic formats,
PDF, Mobi, ePub, HTML, all free.

97
00:05:07,970 --> 00:05:10,190
And you can buy a printed copy of the

98
00:05:10,190 --> 00:05:12,228
book if you like, but you don't have to.

99
00:05:13,700 --> 00:05:16,370
For $8.99, that's all the book costs.

100
00:05:16,370 --> 00:05:18,380
It's not a very thick book.

101
00:05:18,380 --> 00:05:21,750
It's $8.99, but don't bother buying the
book if you don't want to buy the book.

102
00:05:23,250 --> 00:05:26,410
And what's really cool is this is not an
arrangement with the publisher.

103
00:05:26,410 --> 00:05:28,790
At the end of the class you can 
keep these copies

104
00:05:28,790 --> 00:05:31,060
and you can give these copies 
to other people as well.

105
00:05:31,060 --> 00:05:33,470
So this is truly open materials 
for this course.

106
00:05:34,760 --> 00:05:41,000
In addition, my slides, audio, video, and
even my autograding software

107
00:05:41,000 --> 00:05:46,060
is all either open source or licensed with
a Creative Commons Attribution license.

108
00:05:46,060 --> 00:05:50,990
And that means that you can take the
videos, the PowerPoints, the whatever.

109
00:05:50,990 --> 00:05:53,390
We give you a website that will give

110
00:05:53,390 --> 00:05:55,965
you all these materials in an easily
downloadable and

111
00:05:55,965 --> 00:05:58,690
remixable format. And this website 
is part of a

112
00:05:58,690 --> 00:06:01,250
movement we have here at the 
University of Michigan,

113
00:06:01,250 --> 00:06:05,530
like many other universities, that we call
Open Michigan, and it is the idea of

114
00:06:05,530 --> 00:06:08,960
taking courses, not just this course but
hundreds and

115
00:06:08,960 --> 00:06:11,580
hundreds of courses and putting up the
materials.

116
00:06:11,580 --> 00:06:16,080
I have spent extra time to make my
materials in a form

117
00:06:16,080 --> 00:06:20,620
that you can actually reuse and remix and
adjust them to your needs.

118
00:06:20,620 --> 00:06:24,250
Because I really want to empower teachers.

119
00:06:24,250 --> 00:06:26,120
We're all going to learn in this class.

120
00:06:26,120 --> 00:06:27,970
But what I think is the most 
important thing

121
00:06:27,970 --> 00:06:30,670
is to create a whole generation of
teachers who

122
00:06:30,670 --> 00:06:34,480
are capable of teaching a basic
programming class locally,

123
00:06:34,480 --> 00:06:37,650
perhaps in their own language 
in small classes.

124
00:06:37,650 --> 00:06:41,450
These giant classes are great because they
get the word out.

125
00:06:41,450 --> 00:06:43,930
But what we do is, we create a large
learning community,

126
00:06:43,930 --> 00:06:46,310
and some people do well in that, but not
everybody does.

127
00:06:46,310 --> 00:06:48,500
And so, if, if I truly want to believe

128
00:06:48,500 --> 00:06:52,390
in getting programming to be truly for
everybody,

129
00:06:52,390 --> 00:06:53,700
I need to get to the point where we have

130
00:06:53,700 --> 00:06:56,310
lots of teachers, not just me teaching
lots of people.

131
00:06:56,310 --> 00:06:59,050
Although I'm really enjoying teaching 
lots of people.

132
00:06:59,050 --> 00:07:01,020
And so there's a link there, I'll have a
link in the

133
00:07:01,020 --> 00:07:04,900
class that will link you to the open
educational resources in the class.

134
00:07:06,650 --> 00:07:10,410
So as I mentioned I'm, my whole research
is playing with new pedagogies

135
00:07:10,410 --> 00:07:12,530
and new technologies in 
teaching and learning.

136
00:07:12,530 --> 00:07:13,990
I want to experiment with this.

137
00:07:13,990 --> 00:07:17,120
I want to, the thing that's cool about
MOOCs and Coursera

138
00:07:17,120 --> 00:07:20,150
is the ability to teach at scale 
which is, I think,

139
00:07:20,150 --> 00:07:23,850
is unprecedented and it makes me as a
teacher think very

140
00:07:23,850 --> 00:07:27,490
carefully like wait a second, how can I
teach at scale?

141
00:07:27,490 --> 00:07:29,960
Can I still do a good job 
teaching at scale?

142
00:07:29,960 --> 00:07:33,630
And I find that I have to start building
cool technology.

143
00:07:33,630 --> 00:07:35,470
So maybe you've not taken another
Coursera course,

144
00:07:35,470 --> 00:07:37,670
but here's our Coursera user interface.

145
00:07:37,670 --> 00:07:43,210
The video lectures tend to be the core of it.
And interspersed among the video lectures

146
00:07:43,210 --> 00:07:47,270
you will see little assignments, and the

147
00:07:47,270 --> 00:07:49,470
assignments have, there will be 
quizzes as well.

148
00:07:49,470 --> 00:07:55,000
But the assignments end up with this 
terminal icon and you click on them.

149
00:07:55,000 --> 00:07:57,970
There's also sort of a video that goes
along with each assignment.

150
00:07:57,970 --> 00:08:02,890
When you click on the terminal video you
come into this little autograder.

151
00:08:02,890 --> 00:08:07,130
And so I've built this autograder, this
autograder is software that I have created

152
00:08:07,130 --> 00:08:10,260
that autogrades your programming
assignments and then automatically

153
00:08:10,260 --> 00:08:12,090
instantly sends a grade back to Coursera.

154
00:08:13,290 --> 00:08:15,640
And if you look at this autograder, and
I'll talk more about this

155
00:08:15,640 --> 00:08:19,400
in the course, you type your Python code
here. I hope you'll actually be

156
00:08:19,400 --> 00:08:22,720
able to run the code on your laptop and
then just paste it in here,

157
00:08:22,720 --> 00:08:25,370
but some people will actually
just write all the code inside here.

158
00:08:25,370 --> 00:08:28,880
You run the code by pressing Check Code
and your output comes here,

159
00:08:28,880 --> 00:08:31,870
the defined output, and the problem is
there as well.

160
00:08:31,870 --> 00:08:35,240
And the program automatically checks to
see if your output matches.

161
00:08:36,780 --> 00:08:41,090
And it looks at your code a little bit to
make sure you're not cheating, for example.

162
00:08:41,090 --> 00:08:46,110
But it actually finishes grading and when
you submit your grade, you get a grade.

163
00:08:46,110 --> 00:08:50,910
The other thing that I've done is,
an example

164
00:08:50,910 --> 00:08:52,970
of this is in the Week 1 extra credit.

165
00:08:52,970 --> 00:08:55,150
So this first week even has 
extra credit in it.

166
00:08:55,150 --> 00:08:57,380
I'm not going to say everyone has to have

167
00:08:57,380 --> 00:08:59,490
a laptop and everyone has to install
Python, but

168
00:08:59,490 --> 00:09:02,400
if you have a laptop and can install
Python,

169
00:09:02,400 --> 00:09:04,320
I would really love it if you did.

170
00:09:04,320 --> 00:09:06,650
So I'm going to give you extra credit if
you do that.

171
00:09:06,650 --> 00:09:08,760
And the way you'll prove that you've
actually

172
00:09:08,760 --> 00:09:11,920
successfully installed Python is take a
couple of screenshots.

173
00:09:11,920 --> 00:09:16,440
And upload them to what I call the, my
social peer grading system.

174
00:09:16,440 --> 00:09:20,810
And what you'll do is you will upload 
two screenshots.

175
00:09:20,810 --> 00:09:22,340
It tells you what to do.

176
00:09:22,340 --> 00:09:26,560
Upload two screenshots and then students
will grade each other's screenshots.

177
00:09:26,560 --> 00:09:30,900
Now if you've used Coursera before, you know 
that the peer grading takes a while.

178
00:09:30,900 --> 00:09:33,650
This peer grading, if someone else has
already turned in their assignment

179
00:09:33,650 --> 00:09:37,980
you can upload your assignment, and then 
within a couple of minutes grade

180
00:09:37,980 --> 00:09:41,510
the other assignments that are waiting to
be graded, and then you're done!

181
00:09:41,510 --> 00:09:43,710
And so, as long as you're not the 
first person or

182
00:09:43,710 --> 00:09:47,190
the last person, you get almost instant
feedback and you

183
00:09:47,190 --> 00:09:50,960
can almost instantly finish your grading
and be completely done

184
00:09:50,960 --> 00:09:53,440
with the assignment, because really this
is not a hard assignment.

185
00:09:53,440 --> 00:09:57,470
Most Coursera peer grading is writing
assignments that are a little more challenging.

186
00:09:57,470 --> 00:09:58,300
This is really easy.

187
00:09:58,300 --> 00:10:02,220
You just gotta check somebody's screenshot
to see if they did the right thing.

188
00:10:02,220 --> 00:10:04,180
So we will have several assignments that
are done through

189
00:10:04,180 --> 00:10:05,150
this social peer grading.

190
00:10:05,150 --> 00:10:07,890
And my goal, and I built this software.

191
00:10:07,890 --> 00:10:09,930
And my goal is to make it so that

192
00:10:09,930 --> 00:10:12,250
you could submit your stuff, you could 
do some grading,

193
00:10:12,250 --> 00:10:14,560
and you could be done in 10 or 15 minutes.

194
00:10:14,560 --> 00:10:16,730
Rather than peer grading take a long time.

195
00:10:16,730 --> 00:10:20,300
So this is an experiment and it's
something that I was able to build.

196
00:10:20,300 --> 00:10:22,470
Because for the pedagogy of this course

197
00:10:22,470 --> 00:10:25,650
I have a lot of these like check-off
assignments where

198
00:10:25,650 --> 00:10:27,720
it's like did you do it, and it's really
easy to grade.

199
00:10:27,720 --> 00:10:29,940
You look at it and go like yeah, you did
the right thing.

200
00:10:29,940 --> 00:10:32,650
Or you'd say no they didn't, and give
them some feedback.

201
00:10:32,650 --> 00:10:38,240
So, and then, even the grading, you're
going to have to look at this little

202
00:10:38,240 --> 00:10:41,930
page to see the grades that you got in
these externally hosted tools.

203
00:10:43,060 --> 00:10:47,030
As I mentioned, I would like you to
install Python if you possibly could.

204
00:10:47,030 --> 00:10:47,810
It's optional.

205
00:10:47,810 --> 00:10:50,020
You will be able to do whole course on a,

206
00:10:50,020 --> 00:10:54,060
on a phone, although writing all this
code on a phone is crazy.

207
00:10:54,060 --> 00:10:56,940
But, you know, there are crazy
people in the world.

208
00:10:56,940 --> 00:11:00,100
And so I will give you this extra credit
assignment.

209
00:11:00,100 --> 00:11:02,360
Some people might ask what version of
Python we're using.

210
00:11:02,360 --> 00:11:04,586
There are Python 2 and Python 3, and

211
00:11:04,586 --> 00:11:07,560
we're going to be using Python 2 
for this class.

212
00:11:07,560 --> 00:11:09,120
All the skills that you learn 
in Python 2


213
00:11:09,120 --> 00:11:11,190
will transfer if you really needed 
to do Python 3.

214
00:11:11,190 --> 00:11:13,604
There's tiny changes.

215
00:11:13,604 --> 00:11:15,060
The market is slowly but surely

216
00:11:15,060 --> 00:11:17,320
transforming from Python 2 to 
Python 3

217
00:11:18,700 --> 00:11:22,180
Another technical problem is that 
I know some of you

218
00:11:22,180 --> 00:11:25,250
live in countries that don't allow the
use of YouTube.

219
00:11:25,250 --> 00:11:28,640
And so I'll have some videos that I cannot
include in

220
00:11:28,640 --> 00:11:30,690
Coursera because of copyright
restrictions, but

221
00:11:30,690 --> 00:11:31,920
I can point to them in YouTube.

222
00:11:33,200 --> 00:11:35,210
And you'll just not be able to see them.

223
00:11:35,210 --> 00:11:36,610
And don't worry too much about that.

224
00:11:36,610 --> 00:11:41,100
They're usually cute or funny or
instructive, but they're not essential.

225
00:11:41,100 --> 00:11:43,870
So everything that's essential I will
have in the class, and if

226
00:11:43,870 --> 00:11:48,040
you're in a country that can't use
YouTube, I am sorry about that.

227
00:11:48,040 --> 00:11:51,800
But I have not put anything that's
essential for the class up on YouTube.

228
00:11:52,800 --> 00:11:55,640
So let me tell you a couple other things
about the course.

229
00:11:56,830 --> 00:11:58,900
One of the things that is really cool

230
00:11:58,900 --> 00:12:01,849
about Coursera is the notion of community
teaching assistants.

231
00:12:03,150 --> 00:12:06,040
Sue, Mauro, and Mazen are my community
teaching assistants.

232
00:12:06,040 --> 00:12:10,620
They come with me from my Internet History
and Technology class.

233
00:12:10,620 --> 00:12:12,610
They're my community teaching assistants
there, and they've helped

234
00:12:12,610 --> 00:12:15,360
me kind of make this class, and that's
really cool.

235
00:12:15,360 --> 00:12:17,380
There will be many community teaching
assistants.

236
00:12:17,380 --> 00:12:19,200
These three are my leads.

237
00:12:19,200 --> 00:12:21,640
And there will be many community teaching
assistants, and you're going to

238
00:12:21,640 --> 00:12:24,840
find that it's really essential what they
do in the class.

239
00:12:24,840 --> 00:12:28,070
Especially this first time through, cause
we're going to

240
00:12:28,070 --> 00:12:31,850
need to be watching all kinds of things
happening.

241
00:12:31,850 --> 00:12:33,820
Another thing that I do that's kind of
interesting and

242
00:12:33,820 --> 00:12:37,400
cool is what I call face-to-face 
office hours.

243
00:12:37,400 --> 00:12:43,160
I have quite an active travel schedule.
And when I can, as often as I can,

244
00:12:43,160 --> 00:12:47,330
around the world wherever I happen to
be giving a keynote speech or whatever,

245
00:12:47,330 --> 00:12:51,420
I go to a coffee shop or some other 
public place, and

246
00:12:51,420 --> 00:12:55,200
say from ten to noon, I'll be here at this
coffee shop.

247
00:12:55,200 --> 00:12:57,200
At a Starbucks in Amsterdam, we had 15 people

248
00:12:57,200 --> 00:12:59,530
show up at the Starbucks in Amsterdam.

249
00:12:59,530 --> 00:12:59,960
Where else?

250
00:12:59,960 --> 00:13:03,970
One's in London there, that's Barcelona,
and that's Seoul, Korea.

251
00:13:03,970 --> 00:13:06,250
We had three people show up 
at Seoul, Korea.

252
00:13:06,250 --> 00:13:09,920
And and so I've got a series

253
00:13:09,920 --> 00:13:13,170
of videos from this, it's called 
office-hours.dr-chuck.com.

254
00:13:13,170 --> 00:13:16,680
And it'll redirect you to a YouTube
playlist.

255
00:13:16,680 --> 00:13:19,620
And, if you come to office hours I have a
sticker for you.

256
00:13:19,620 --> 00:13:23,650
I will give you personally a sticker which
is the logo for the course.

257
00:13:23,650 --> 00:13:25,540
So you can look forward to that.

258
00:13:25,540 --> 00:13:28,310
When we meet in person I will have a 
sticker for you.

259
00:13:30,390 --> 00:13:34,820
Another thing that I've been doing lately,
not so much to help in

260
00:13:34,820 --> 00:13:40,270
learning the in course, but because there's
a lot of public discussion about MOOCs.

261
00:13:40,270 --> 00:13:42,760
And I think sadly most of the people who

262
00:13:42,760 --> 00:13:45,030
are doing the public discussing about MOOCs,

263
00:13:46,270 --> 00:13:48,090
other than those who are actually 
involved in it,

264
00:13:48,090 --> 00:13:50,340
most of the people doing the
public discussing about MOOCs

265
00:13:50,340 --> 00:13:52,780
or the public talk about MOOCs are not

266
00:13:52,780 --> 00:13:55,820
actually involved in them and so they are
saying really

267
00:13:55,820 --> 00:13:58,700
uninformed things about MOOCs. And so one
of the things that

268
00:13:58,700 --> 00:14:01,980
I'm passionate about is getting your voice
to be heard.

269
00:14:01,980 --> 00:14:05,680
Now I have this YouTube channel, and if 
you are willing,

270
00:14:05,680 --> 00:14:10,370
go to the voices.dr-chuck.com
and add, send me

271
00:14:10,370 --> 00:14:14,060
via Twitter or something a link if you

272
00:14:14,060 --> 00:14:16,430
record one of these little short 
two-minute videos.

273
00:14:16,430 --> 00:14:17,600
And you can take a look at the videos.

274
00:14:17,600 --> 00:14:19,350
They talk about who you are, what your

275
00:14:19,350 --> 00:14:21,660
education is, and what you like about
MOOCs.

276
00:14:21,660 --> 00:14:22,640
They're, they're kind of different.

277
00:14:22,640 --> 00:14:24,590
They don't all have to be the same.

278
00:14:24,590 --> 00:14:27,740
But if you're interested, I'd love to see
your, you as part

279
00:14:27,740 --> 00:14:30,920
of my little voices of the, of the
students in MOOCs project.

280
00:14:32,370 --> 00:14:35,610
So programming courses can be very
challenging.

281
00:14:35,610 --> 00:14:39,770
One of my goals is to teach you how to
take a programming class.

282
00:14:39,770 --> 00:14:42,140
So that you can take other programming
classes on

283
00:14:42,140 --> 00:14:45,670
Coursera or elsewhere and be successful in
those classes.

284
00:14:45,670 --> 00:14:49,530
I think programming is wonderful,
programming for me is joyful

285
00:14:49,530 --> 00:14:53,430
and feels like artistic and I just, I
get endorphins.

286
00:14:53,430 --> 00:14:55,560
And some people look at a sunset and get
excited,

287
00:14:55,560 --> 00:14:58,000
I look at really beautiful code and I get
excited.

288
00:14:59,850 --> 00:15:01,470
But part of the problem with programming
is if

289
00:15:01,470 --> 00:15:03,620
you don't learn the language or don't, 
you don't

290
00:15:03,620 --> 00:15:06,140
see the beauty until you kind of
understand the

291
00:15:06,140 --> 00:15:09,740
basics, so it's a little clunky at the
beginning.

292
00:15:09,740 --> 00:15:12,270
And even the first like five weeks of this
class you might be

293
00:15:12,270 --> 00:15:16,540
scratching your head and thinking like,
what the heck is he talking about?

294
00:15:16,540 --> 00:15:22,120
And so the key thing is not to try to
memorize everything.

295
00:15:22,120 --> 00:15:24,640
The key thing is to learn or accept the

296
00:15:24,640 --> 00:15:27,540
fact in that the first half of the class

297
00:15:27,540 --> 00:15:29,930
I'm describing for you a big picture that
you're not

298
00:15:29,930 --> 00:15:32,775
going to understand until you understand
all the small pieces.

299
00:15:32,775 --> 00:15:36,520
So I'll talk in chapters two,
three, four, five, and six

300
00:15:36,520 --> 00:15:39,720
about these things that seem small
and insignificant.

301
00:15:39,720 --> 00:15:43,050
And it's really not till chapters seven
through ten that the

302
00:15:43,050 --> 00:15:46,790
pieces start to come together and you go,
like, I get it.

303
00:15:46,790 --> 00:15:50,610
So it helps sometimes to look forwards and
backwards in the material.

304
00:15:50,610 --> 00:15:53,070
Don't just look at the paragraph.

305
00:15:53,070 --> 00:15:54,180
If you're looking at a paragraph and it

306
00:15:54,180 --> 00:15:57,460
doesn't make sense, forget about it, 
come back later.

307
00:15:57,460 --> 00:16:00,600
Look way down at the end of the book, look
at the beginning of the book.

308
00:16:00,600 --> 00:16:02,990
Just bounce around, ask people.

309
00:16:02,990 --> 00:16:05,174
If you're having trouble on 
assignment four, go

310
00:16:05,174 --> 00:16:07,350
back and redo assignment one 
or assignment two.

311
00:16:07,350 --> 00:16:12,000
And that's because there's no way 
that I can,

312
00:16:12,000 --> 00:16:14,750
one step at a time, teach you with perfect

313
00:16:14,750 --> 00:16:18,710
understanding, because I'm teaching you a
big complex picture

314
00:16:18,710 --> 00:16:20,900
with little pieces of view at a time.

315
00:16:20,900 --> 00:16:23,930
And it doesn't make sense until you've
made it all the way through.

316
00:16:23,930 --> 00:16:27,360
So if it doesn't make sense in week
seven, we've got a problem.

317
00:16:27,360 --> 00:16:29,730
If you're a little confused, 
just trust me.

318
00:16:29,730 --> 00:16:35,010
Just learn what I'm trying to teach you,
and don't just do the homework.

319
00:16:36,050 --> 00:16:39,020
Because you will find that if you do the
homework

320
00:16:39,020 --> 00:16:41,850
for assignment week one it's like, well
that was easy.

321
00:16:41,850 --> 00:16:42,790
And you don't learn anything.

322
00:16:42,790 --> 00:16:45,350
And then you do assignment two, 
and that was easy.

323
00:16:45,350 --> 00:16:46,660
Assignment three, that was 
a little harder.

324
00:16:46,660 --> 00:16:47,990
And you didn't look at the lectures,


325
00:16:47,990 --> 00:16:50,120
didn't read the book, or didn't do 
anything? Then it's,

326
00:16:50,120 --> 00:16:53,410
week four will be harder, and then you'll
be like what the heck is he talking about?

327
00:16:53,410 --> 00:16:54,710
And then you'll be lost.

328
00:16:54,710 --> 00:16:56,780
So learn what I am trying to teach you.

329
00:16:56,780 --> 00:17:00,870
Trust me, trust me that, that 
what I am telling you

330
00:17:00,870 --> 00:17:03,930
is going to come together and 
make sense at the end.

331
00:17:03,930 --> 00:17:07,580
So learn what I'm trying to teach you, at
the moment I'm trying to teach you.

332
00:17:07,580 --> 00:17:10,550
So don't just do the homework and go like,
whew, I'm done.

333
00:17:10,550 --> 00:17:12,150
This is not a regular class.

334
00:17:12,150 --> 00:17:14,560
This is a class where you're supposed to
learn something.

335
00:17:14,560 --> 00:17:17,490
So trust that I put only what I think is

336
00:17:17,490 --> 00:17:19,900
essential in this class and I'm not
going to waste your time.

337
00:17:21,390 --> 00:17:23,690
But don't take that as I'm just going to

338
00:17:23,690 --> 00:17:25,330
do the homework and not learn anything,
because at

339
00:17:25,330 --> 00:17:26,830
the end of the day you won't learn a

340
00:17:26,830 --> 00:17:28,780
thing, you know, if you just do the
homework.

341
00:17:28,780 --> 00:17:32,710
Okay, if you feel stuck take a break,
relax.

342
00:17:32,710 --> 00:17:36,550
Concentrating, thinking more deeply,
beating yourself up,

343
00:17:36,550 --> 00:17:38,190
that doesn't help.

344
00:17:38,190 --> 00:17:39,200
Go take a walk.

345
00:17:39,200 --> 00:17:39,780
Come back.

346
00:17:39,780 --> 00:17:40,970
It'll make more sense.

347
00:17:40,970 --> 00:17:42,600
Ask somebody.

348
00:17:42,600 --> 00:17:44,920
We just want you to learn, 
one way or the other.

349
00:17:44,920 --> 00:17:45,870
Okay?

350
00:17:45,870 --> 00:17:47,700
The grades and the homework, that's
going to be easy.

351
00:17:51,130 --> 00:17:52,120
Okay.

352
00:17:52,120 --> 00:17:56,750
So this is the first time the class has
been taught, first time.

353
00:17:56,750 --> 00:17:59,590
I am using all kinds of crazy technology.

354
00:17:59,590 --> 00:18:02,960
I'm using features in Coursera that 
no one's ever used before

355
00:18:02,960 --> 00:18:04,640
that just were invented for, not
for me, but

356
00:18:04,640 --> 00:18:06,420
for many people, but I'm the first
one that's going to use it.

357
00:18:08,030 --> 00:18:09,890
We're plugging these tools in, I'm building

358
00:18:09,890 --> 00:18:12,390
new autograders, I'm building all 
these things.

359
00:18:12,390 --> 00:18:14,760
It could mess up, it could fail.

360
00:18:14,760 --> 00:18:16,410
I could be confused.

361
00:18:16,410 --> 00:18:17,890
I could be scrambling and fixing.

362
00:18:17,890 --> 00:18:19,240
I could lose a bunch of grades.

363
00:18:20,800 --> 00:18:23,510
But if we have a meltdown, relax.

364
00:18:23,510 --> 00:18:27,960
I will watch the meltdown, I will fix the
meltdown, I will adjust deadlines.

365
00:18:27,960 --> 00:18:30,080
Stick with me on this, okay?

366
00:18:30,080 --> 00:18:31,260
Help me through it.

367
00:18:31,260 --> 00:18:33,840
Help tell me how to make this 
software better.

368
00:18:33,840 --> 00:18:36,240
I know the first time there's going 
to be problems.

369
00:18:36,240 --> 00:18:38,400
But after that I hope that we have

370
00:18:38,400 --> 00:18:42,070
software and a methodology that 
everybody can use.

371
00:18:42,070 --> 00:18:43,280
And you can tweet me.

372
00:18:43,280 --> 00:18:46,690
I'm always watching Twitter even when I'm
sort of out of the country.

373
00:18:46,690 --> 00:18:51,200
So tweet me if something seems to be
broken and I will get

374
00:18:51,200 --> 00:18:54,410
right on it and I will try to fix it as
soon as possible.

375
00:18:54,410 --> 00:18:57,620
And when you tweet me, I'll end up with
some screen like this.

376
00:18:57,620 --> 00:19:03,250
I mean, I literally love fixing the code
that you're working with.

377
00:19:03,250 --> 00:19:05,590
And, I mean, I just get a charge out of it,
like, woah, it's not

378
00:19:05,590 --> 00:19:07,950
quite right but I figured out what's wrong

379
00:19:07,950 --> 00:19:10,241
and like [SOUND] and it's all 
working for you.

380
00:19:10,241 --> 00:19:13,180
And so, so this, for me, is fun.

381
00:19:13,180 --> 00:19:15,550
The meltdown's not upsetting to me.

382
00:19:15,550 --> 00:19:17,590
I hope we have no meltdowns, then I'll be
very proud.

383
00:19:17,590 --> 00:19:21,670
But if we have a meltdown, I will be on it
and I'll fix it, trust me.

384
00:19:25,470 --> 00:19:30,190
And then when we get all done with this,
there's a graduation ceremony.

385
00:19:30,190 --> 00:19:32,710
There's a graduation ceremony toward the
end of the class.

386
00:19:32,710 --> 00:19:38,150
I tape this graduation ceremony with my
good friend Curt Bonk and, and so

387
00:19:38,150 --> 00:19:42,310
look forward to the, stick with it, and
look forward to the graduation ceremony.

388
00:19:42,310 --> 00:19:46,090
And so that's pretty much all I have for
this beginning lecture.

389
00:19:46,090 --> 00:19:48,530
I am glad that you're sticking with it

390
00:19:48,530 --> 00:19:50,360
and I'm glad that you're giving 
this a try.

391
00:19:50,360 --> 00:19:53,690
And I really look forward to you,
interacting with you, on this class.

