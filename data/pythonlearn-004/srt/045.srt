1
00:00:00,430 --> 00:00:02,320
Welcome to our Worked Exercise.

2
00:00:02,320 --> 00:00:08,040
This is Dr. Charles Severance and Worked
Exercises from Python for Informatics.

3
00:00:08,040 --> 00:00:11,910
This material is copyright Creative
Commons Attribution 3.0.

4
00:00:11,910 --> 00:00:15,350
And if you need basic guidance on 
how to install

5
00:00:15,350 --> 00:00:17,490
Python or use your text editor in general,
or how to

6
00:00:17,490 --> 00:00:20,440
use the command line, stuff that we'll be
using in this

7
00:00:20,440 --> 00:00:25,069
exercise, go back to Pythonlearn.com and
work through the introductory materials.

8
00:00:26,230 --> 00:00:27,670
So here's our exercise.

9
00:00:27,670 --> 00:00:30,510
We're supposed to rewrite our pay program 
and we're going to use

10
00:00:30,510 --> 00:00:33,298
try and except in this situation so that
we can handle

11
00:00:33,298 --> 00:00:37,580
non-numeric input gracefully. And we're
supposed to print out an error

12
00:00:37,580 --> 00:00:40,830
if there's any non-numeric input and
otherwise print out the right thing.

13
00:00:40,830 --> 00:00:41,800
So let's get started.

14
00:00:44,530 --> 00:00:49,200
Let's go ahead and start TextWrangler and
start Terminal program

15
00:00:50,730 --> 00:00:55,220
and make it a little smaller, a little bit
to the bottom.

16
00:00:56,290 --> 00:01:00,600
Make this a little smaller, get rid of
these.

17
00:01:00,600 --> 00:01:05,690
So I'm going to start with a program that
I've already got written.

18
00:01:05,690 --> 00:01:09,310
And so I'll open the last one I worked
with, which is

19
00:01:09,310 --> 00:01:14,530
hours2, and I'm going to save this
quickly as hours3.

20
00:01:14,530 --> 00:01:16,780
We programmers do this all the time.

21
00:01:16,780 --> 00:01:21,120
I mean, why waste time typing if I just
got done typing it?

22
00:01:21,120 --> 00:01:24,035
There we go. And that's why these exercises
build on it, so you get

23
00:01:24,035 --> 00:01:27,260
used to this notion of just kind of
hacking code that you had before.

24
00:01:28,368 --> 00:01:33,300
Okay, so I'll put the print of rate and
hours back in.

25
00:01:33,300 --> 00:01:36,270
This part from here down is pretty safe.

26
00:01:36,270 --> 00:01:43,230
So I'll go into my desktop, go into Python
for Informatics and say

27
00:01:43,300 --> 00:01:46,800
python hours3

28
00:01:48,420 --> 00:01:55,840
and I'll say, you know, 40, 10, super
duper, and I say, gluh gluh, crash.

29
00:01:55,840 --> 00:01:58,130
So where is it blowing up?

30
00:01:58,130 --> 00:02:00,000
Well, it's blowing up on the float.

31
00:02:00,000 --> 00:02:03,350
So the float is the, the dangerous thing

32
00:02:03,350 --> 00:02:06,760
and this is, this float is the dangerous 
thing too, right?

33
00:02:06,760 --> 00:02:10,970
And so, this is where it's going to blow
up and now, because of this, the rest of

34
00:02:10,970 --> 00:02:13,280
the program hasn't run and this is 
exactly the

35
00:02:13,280 --> 00:02:16,170
situation where you use a try and 
except for.

36
00:02:16,170 --> 00:02:19,860
Okay, so lets do try and we're 
going to just,

37
00:02:19,860 --> 00:02:23,415
we're going to use a sledge hammer 
on this one.

38
00:02:23,415 --> 00:02:25,540
We're going to put all four of these
statements in the try

39
00:02:25,540 --> 00:02:28,270
block, but only two of the statements are
in some danger

40
00:02:30,870 --> 00:02:33,050
and in a second, I'll show you 
what can go

41
00:02:33,050 --> 00:02:37,030
wrong with this approach, but for now
we're in the beginning.

42
00:02:37,030 --> 00:02:41,790
e-x-c-e-p-t colon, and this is where we put

43
00:02:41,790 --> 00:02:46,850
our error message, where we go print and
what are we supposed to print?

44
00:02:48,000 --> 00:02:53,540
A let's just kind of steal that error.

45
00:02:53,540 --> 00:02:55,670
Here, please enter numeric input.

46
00:02:55,670 --> 00:02:57,310
So I just cut and paste that.

47
00:02:57,310 --> 00:03:01,020
So now we've taken insurance out and

48
00:03:01,020 --> 00:03:03,270
if everything goes well, this except is
ignored.

49
00:03:03,270 --> 00:03:05,250
If anything goes wrong, and really 
it's going to be the

50
00:03:05,250 --> 00:03:08,438
floats, one of the two floats are 
going to go wrong.

51
00:03:08,438 --> 00:03:09,950
Away it goes.

52
00:03:09,950 --> 00:03:12,680
So let's go ahead and run this.

53
00:03:14,100 --> 00:03:17,244
Make this a little bigger and I will 
save it.

54
00:03:19,244 --> 00:03:21,798
Okay.

55
00:03:23,798 --> 00:03:28,023
python hours3.

56
00:03:29,023 --> 00:03:31,190
Let's do it right first.

57
00:03:32,610 --> 00:03:34,450
Looking good.

58
00:03:34,450 --> 00:03:35,570
Now let's do it wrong.

59
00:03:38,740 --> 00:03:42,210
Now I typed in a bad number and it said

60
00:03:42,210 --> 00:03:46,515
error, please enter numeric input, but then
it blew up.

61
00:03:46,515 --> 00:03:48,780
Traceback to line 9.

62
00:03:48,780 --> 00:03:51,610
This is line 9 right here.

63
00:03:51,610 --> 00:03:56,780
Oops, well, that's because I didn't tell
it to do, I didn't tell it to stop.

64
00:03:56,780 --> 00:03:59,884
It just kept on going, right?

65
00:03:59,884 --> 00:04:03,450
And so there's a couple ways we
can solve this.

66
00:04:03,450 --> 00:04:09,560
We could solve it cleanly but I'm just
going to be crude at this point.

67
00:04:09,560 --> 00:04:17,723
Now I'll put all this stuff in
the except

68
00:04:19,723 --> 00:04:26,978
because that way if it dies,

69
00:04:26,978 --> 00:04:28,232
it's going to run this

70
00:04:28,232 --> 00:04:30,620
and then the program's going 
to be done.

71
00:04:30,620 --> 00:04:33,700
Okay? And if it succeeds, it's going to
fall through and

72
00:04:33,700 --> 00:04:37,500
it's only going to do this code if that
other code worked.

73
00:04:37,500 --> 00:04:40,340
And I will show you in a moment the
fallacy and

74
00:04:40,340 --> 00:04:43,400
why this isn't the best way, but this is
proven effective, right?

75
00:04:43,400 --> 00:04:47,500
We're early on, so all we have to do is
find a solution.

76
00:04:47,500 --> 00:04:49,794
In a bit, we'll talk about what the better
solution is.

77
00:04:49,794 --> 00:04:52,530
So here we go, python hours3.

78
00:04:52,530 --> 00:04:53,400
So we type fred.

79
00:04:54,660 --> 00:04:55,920
Kabloom, it dies.

80
00:04:55,920 --> 00:04:59,110
So it died on this line right here and
then it

81
00:04:59,110 --> 00:05:02,200
transferred to the except and ran this and
the program was over.

82
00:05:03,440 --> 00:05:07,850
And if we give a good one and then
something

83
00:05:07,850 --> 00:05:11,260
bad, it, it, this line dies, and then it
continues on.

84
00:05:13,160 --> 00:05:19,190
So let me show you a couple of fallacies
or some mistakes in this.

85
00:05:19,190 --> 00:05:20,400
So watch this.

86
00:05:20,400 --> 00:05:23,100
I'm going to make a mistake here.

87
00:05:24,290 --> 00:05:24,790
Print

88
00:05:28,020 --> 00:05:29,840
I'll say print r comma h.

89
00:05:30,970 --> 00:05:32,730
So I'll print r comma h.

90
00:05:32,730 --> 00:05:35,721
So I'll type python hours: 40.

91
00:05:37,500 --> 00:05:38,000
10.

92
00:05:39,370 --> 00:05:42,010
Error, please enter numeric input.

93
00:05:43,570 --> 00:05:46,060
So what in the heck was happening?

94
00:05:46,060 --> 00:05:50,320
So like, it is numeric input.

95
00:05:50,320 --> 00:05:51,150
Where did it die?

96
00:05:52,410 --> 00:05:53,780
So what, I'll tell you what, I'll just
show you

97
00:05:53,780 --> 00:05:56,130
what happened here and we won't worry too
much about this.

98
00:05:57,230 --> 00:05:58,680
It came into the try.

99
00:05:58,680 --> 00:06:00,050
It did this.

100
00:06:00,050 --> 00:06:00,920
The 40 is good.

101
00:06:00,920 --> 00:06:02,140
So that worked.

102
00:06:02,140 --> 00:06:03,080
The rate is good.

103
00:06:03,080 --> 00:06:04,500
So that worked.

104
00:06:04,500 --> 00:06:09,350
What worked was r is not defined. But it
turns out that this is another error.

105
00:06:09,350 --> 00:06:10,350
It's just a different error.

106
00:06:10,350 --> 00:06:13,310
It's not the same as this one and we just

107
00:06:13,310 --> 00:06:16,380
didn't type it right and so it jumps to
the except.

108
00:06:16,380 --> 00:06:19,910
Doesn't do any of this code but it jumps
to the except.

109
00:06:19,910 --> 00:06:24,430
So that's why it was numeric input but
away we go, okay?

110
00:06:25,780 --> 00:06:27,420
So, that's not so good.

111
00:06:27,420 --> 00:06:31,310
You don't really want to put too much code
inside your try and except.

112
00:06:31,310 --> 00:06:33,980
So let's take a look and do this one a
little better.

113
00:06:33,980 --> 00:06:38,410
I'm going to cut this back out, I'll move
it down to the bottom.

114
00:06:38,410 --> 00:06:40,220
Now I gotta de-indent it.

115
00:06:40,220 --> 00:06:42,425
I'll fix this rate in hours.

116
00:06:57,425 --> 00:06:59,425
Indent.

117
00:07:08,168 --> 00:07:09,864
I'm trying

118
00:07:09,864 --> 00:07:10,960
to get my indents right.

119
00:07:10,960 --> 00:07:11,240
Okay.

120
00:07:11,240 --> 00:07:12,400
So now my indents are right.

121
00:07:12,400 --> 00:07:16,480
So let me go ahead and run the program
just to make sure I'm not crazy.

122
00:07:16,480 --> 00:07:18,070
40, 10, that's good.

123
00:07:19,290 --> 00:07:21,380
50, 10, that's good.

124
00:07:21,380 --> 00:07:22,670
So I'm good.

125
00:07:22,670 --> 00:07:26,530
Okay, the problem now is I've
reintroduced my error

126
00:07:26,530 --> 00:07:30,507
that if I give it a bad thing, okay?

127
00:07:30,507 --> 00:07:32,570
So let's take a look at the kind 
of things
 
128
00:07:32,570 --> 00:07:36,310
that we can do to make this a 
little more elegant.

129
00:07:36,310 --> 00:07:37,450
So so we're right here.

130
00:07:37,450 --> 00:07:38,200
This is the problem.

131
00:07:38,200 --> 00:07:40,400
We, we need to stop.

132
00:07:40,400 --> 00:07:42,130
We don't want to continue at this point.

133
00:07:42,130 --> 00:07:47,370
If there was a thing that we could say
just, just stop here.

134
00:07:49,660 --> 00:07:52,110
If there was such a thing in Python, we'd

135
00:07:52,110 --> 00:07:55,370
be fine but that would be bad Python
syntax.

136
00:07:55,370 --> 00:07:57,140
But there is such a thing in Python, if

137
00:07:57,140 --> 00:07:59,540
you want to quit your program, 
you type quit.

138
00:08:00,880 --> 00:08:02,280
It's a function.

139
00:08:02,280 --> 00:08:03,390
We haven't covered functions yet.

140
00:08:03,390 --> 00:08:04,690
We will soon.

141
00:08:04,690 --> 00:08:06,210
It's a function that never comes back.

142
00:08:06,210 --> 00:08:09,540
Most functions come back, but the quit
function does not come back.

143
00:08:09,540 --> 00:08:11,690
So that when it comes down here and it

144
00:08:11,690 --> 00:08:14,440
prints this, the next thing and then it
doesn't continue.

145
00:08:14,440 --> 00:08:16,830
So this is a way of saying we are done.

146
00:08:18,750 --> 00:08:23,300
So now let's run this and put in some bad
information

147
00:08:24,320 --> 00:08:27,920
and it indeed quits and so it doesn't
continue down here.

148
00:08:27,920 --> 00:08:29,445
Let's make sure it works with good data.

149
00:08:29,445 --> 00:08:34,740
Put 4 hours well, okay, I guess 
that's right.

150
00:08:34,740 --> 00:08:39,625
So let me comment out my debug print
statement here and finish it up.

151
00:08:43,625 --> 00:08:45,190
And run it one last time.

152
00:08:46,290 --> 00:08:48,380
Error, please numeric input.

153
00:08:48,380 --> 00:08:50,620
Good hours, bad rate.

154
00:08:50,620 --> 00:08:55,880
That's good. And good hours, good rate, and
that's good.

155
00:08:55,880 --> 00:08:57,710
So we've accomplished this one.

