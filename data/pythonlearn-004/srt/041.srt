1
00:00:00,780 --> 00:00:02,984
Hello and welcome to the, a worked

2
00:00:02,984 --> 00:00:07,500
exercise for Python for Informatics,
Exploring Information.

3
00:00:07,500 --> 00:00:09,170
My name is Dr. Charles Severance, and

4
00:00:09,170 --> 00:00:12,470
I'm your host for this particular 
worked exercise.

5
00:00:12,470 --> 00:00:15,530
This material is copyright Creative
Commons Attribution.

6
00:00:17,950 --> 00:00:20,940
Again, try and work these 
exercises yourself.

7
00:00:22,070 --> 00:00:23,680
In the beginning they're intended to be
easy, they're

8
00:00:23,680 --> 00:00:26,160
intended to be worked from the book and
the other materials.

9
00:00:26,160 --> 00:00:28,840
Not, and this is supposed to be 
used as a last resort.

10
00:00:28,840 --> 00:00:33,330
But you're here, so I assume you
know those rules.

11
00:00:33,330 --> 00:00:36,570
I assume that you also have 
everything installed.

12
00:00:36,570 --> 00:00:38,480
Python's installed, you know how to use
your text editor,

13
00:00:38,480 --> 00:00:41,010
you know how to use your command line.

14
00:00:41,010 --> 00:00:42,510
This

15
00:00:43,480 --> 00:00:45,750
screencast will not, sort of, 
go through all of those.

16
00:00:45,750 --> 00:00:47,450
Although it will review some of them.

17
00:00:47,450 --> 00:00:51,650
Go back to pythonlearn.com and go through
the software installation screencasts

18
00:00:51,650 --> 00:00:54,360
to get to the point where you're
ready to run programs.

19
00:00:54,360 --> 00:00:56,380
So here's our exercise.

20
00:00:56,380 --> 00:00:58,440
We're going to write a program

21
00:00:58,440 --> 00:01:02,660
that's going to enter the hours and enter
the rate and then compute pay.

22
00:01:02,660 --> 00:01:05,470
It's going to prompt and read some
strings and convert

23
00:01:05,470 --> 00:01:09,860
from floating point and then do some
multiplication, and away we go.

24
00:01:11,670 --> 00:01:17,060
So, I'm going to minimize that. I will
start up my TextWrangler.

25
00:01:18,650 --> 00:01:20,804
And this is the last program 
I was working on.

26
00:01:20,804 --> 00:01:22,983
[BLANK_AUDIO]

27
00:01:22,983 --> 00:01:24,674
And I'll hide those, get rid of those.

28
00:01:24,674 --> 00:01:29,370
Start up my Terminal, make it 
a little smaller.

29
00:01:29,370 --> 00:01:33,480
Pull it down here and TextWrangler.

30
00:01:33,480 --> 00:01:34,740
So now I have a new document.

31
00:01:35,810 --> 00:01:41,800
And so I'm going to do what I always do and
so the first thing we've got to

32
00:01:41,800 --> 00:01:44,350
do is we've got to prompt for some hours.

33
00:01:44,350 --> 00:01:45,870
And so I'm going to make a 
variable called inp

34
00:01:45,870 --> 00:01:48,580
which is just sort of mnemonic for some

35
00:01:48,580 --> 00:01:53,860
input, it's a string, it's raw input,
always, always

36
00:01:53,860 --> 00:01:56,050
reads a string, it doesn't 
read it out, even

37
00:01:56,050 --> 00:01:57,810
though they're numbers, it reads them 
as a string.

38
00:01:57,810 --> 00:01:59,390
We'll have to convert them.

39
00:01:59,390 --> 00:02:06,240
raw_input [SOUND] Enter hours

40
00:02:06,240 --> 00:02:09,830
colon space.

41
00:02:09,830 --> 00:02:13,540
And I'm just going to keep it simple,
and I'm going to print inp out.

42
00:02:13,540 --> 00:02:21,070
Now I have to save this file, Save As, and
I'll put in Python here, and I'm going to

43
00:02:21,070 --> 00:02:26,540
call it hours1.py, because we're going to
do a whole series of these things.

44
00:02:28,590 --> 00:02:29,990
Save that.

45
00:02:29,990 --> 00:02:33,830
Now you'll notice that as soon as it saves
as a .py then it does syntax coloring.

46
00:02:33,830 --> 00:02:35,680
Thank you very much.

47
00:02:35,680 --> 00:02:39,480
So then I go into my command window, cd
Desktop, I have

48
00:02:39,480 --> 00:02:43,318
everything in this folder py4inf on my
desktop, right there, that folder.

49
00:02:43,318 --> 00:02:52,570
cd [INAUDIBLE]. Then I do a ls.

50
00:02:54,210 --> 00:02:56,580
Again, dir for Unix, and then, here we go.

51
00:02:56,580 --> 00:03:03,120
So I say python h-o, hours1.py

52
00:03:03,120 --> 00:03:07,970
again, and I, I use tab a lot, because I
just don't like typing.

53
00:03:07,970 --> 00:03:09,500
So let's run it.

54
00:03:09,500 --> 00:03:15,360
So I enter some hours, 123, and it 
prints out 123.

55
00:03:15,360 --> 00:03:18,650
And just to show you it's just a string,
I'll just put

56
00:03:18,650 --> 00:03:23,180
some crap in there and out comes exactly
as you'd expect, crap.

57
00:03:23,180 --> 00:03:24,550
So it's not really.

58
00:03:24,550 --> 00:03:25,680
And let's do this.

59
00:03:25,680 --> 00:03:31,260
Let's just say, print type of inp just to
sort of prove ourselves.

60
00:03:34,670 --> 00:03:35,900
It's a string, right?

61
00:03:35,900 --> 00:03:36,740
It is a string.

62
00:03:36,740 --> 00:03:37,940
It is not an integer.

63
00:03:37,940 --> 00:03:41,310
So how do we convert from a string to an
integer? Aah, float.

64
00:03:41,310 --> 00:03:43,370
We use the built-in function float.

65
00:03:43,370 --> 00:03:47,562
And now I'm going to call my variable
hours to be quite mnemonic

66
00:03:47,562 --> 00:03:50,754
and I'll call the float conversion

67
00:03:53,754 --> 00:04:02,480
and then I'll print hours and 
print the type of hours.

68
00:04:02,480 --> 00:04:05,710
Now you might say, hey Chuck, you know,
you're a pretty smart guy.

69
00:04:05,710 --> 00:04:07,290
This is only like five lines of code.

70
00:04:07,290 --> 00:04:09,600
How come you're doing this slowly?

71
00:04:09,600 --> 00:04:12,840
How come you take steps? That's what we
call interative development.

72
00:04:12,840 --> 00:04:14,570
So I'm just being really safe here.

73
00:04:14,570 --> 00:04:16,680
I'll get to the whole program in a bit.

74
00:04:16,680 --> 00:04:20,040
I mean, that's this dot, dot, dot 
stuff down here.

75
00:04:20,040 --> 00:04:21,000
There'll be more.

76
00:04:21,000 --> 00:04:23,810
But I'm, I'm just sort of playing 
with it one bit at

77
00:04:23,810 --> 00:04:28,170
a time and, and making sure that 
I know what's going on.

78
00:04:28,170 --> 00:04:32,590
And even, even in, I don't, I'm doing this
a little bit more exaggerated here,

79
00:04:32,590 --> 00:04:36,160
but when I write programs, I usually start
with hello world and then add to it.

80
00:04:36,160 --> 00:04:39,200
It's, it's sort of the, you start with
sort of a nice

81
00:04:39,200 --> 00:04:42,600
blank canvas, and put some basic stuff on
the back of it. Okay.

82
00:04:42,600 --> 00:04:45,400
So, so now I'm going to read some input.

83
00:04:45,400 --> 00:04:47,540
I'm going to, I have to prove that 
this is a string.

84
00:04:47,540 --> 00:04:50,810
I'm going to convert the string to a
floating point, put in a different number.

85
00:04:50,810 --> 00:04:52,720
Then I'll print out that thing 
and the type of it

86
00:04:52,720 --> 00:04:55,130
just to sort be sure that it works.

87
00:04:55,130 --> 00:04:58,870
So I have python hours1.py and 
I go 1, 2, 3.

88
00:04:58,870 --> 00:05:01,246
And it turns out that now I, 
I've converted it to

89
00:05:01,246 --> 00:05:03,820
1, 2, 3, 4 and I've got myself a float.

90
00:05:03,820 --> 00:05:07,920
And of course now, if I call, if I put
junk in there, it's going

91
00:05:07,920 --> 00:05:15,260
to get us a traceback error, because it
just cannot convert dskj to a float.

92
00:05:15,260 --> 00:05:17,730
But we're not going to, we're going to fix
that later.

93
00:05:17,730 --> 00:05:20,650
Okay? So now I'm going to start 
deleting some

94
00:05:20,650 --> 00:05:24,960
of this stuff, because that's pretty clear
that that's working.

95
00:05:24,960 --> 00:05:25,550
Okay?

96
00:05:25,550 --> 00:05:27,690
So now, I've got to prompt for rate.

97
00:05:27,690 --> 00:05:31,930
Well, good programmers don't type 
if they don't have to.

98
00:05:31,930 --> 00:05:35,870
So I'm just going to cut and paste this
and then I'm going to change this to rate.

99
00:05:35,870 --> 00:05:39,300
So raw_input Enter Rate. 
And then that will be

100
00:05:39,300 --> 00:05:41,390
a string of course, then I will 
change that to a

101
00:05:41,390 --> 00:05:43,950
float and I'm going to put that 
into rate.

102
00:05:43,950 --> 00:05:49,230
And just to be sure, I'm going to 
print out rate, hours.

103
00:05:49,230 --> 00:05:51,060
Now at this point, they should be 
floating point

104
00:05:51,060 --> 00:05:54,160
numbers, so let's run that and 
we'll start with a

105
00:05:54,160 --> 00:05:58,010
simple test, 40 hours, 10 dollars an hour.

106
00:05:58,010 --> 00:06:01,030
There we go, they've been converted to floats,
just peachy fine.

107
00:06:01,030 --> 00:06:07,560
So I'm going to create a variable called
pay, rate times hours

108
00:06:07,560 --> 00:06:13,280
and we'll print out pay. And I will just get
rid of this one with a comment.

109
00:06:15,920 --> 00:06:17,530
And did you notice how I used a comment,

110
00:06:17,530 --> 00:06:20,070
a pound sign, there, because, I'll, I'm, I'm
not deleting that.

111
00:06:20,070 --> 00:06:22,210
I'll keep that in case something goes wrong.

112
00:06:22,210 --> 00:06:23,340
I don't think it's going to, but

113
00:06:23,340 --> 00:06:25,860
here we go.

114
00:06:25,860 --> 00:06:26,780
Whoa, syntax error.

115
00:06:26,780 --> 00:06:29,850
Invalid syntax, pay rate times hours.

116
00:06:29,850 --> 00:06:31,170
And oh, I forgot the equals sign.

117
00:06:31,170 --> 00:06:33,100
Need to make that an assignment statement.

118
00:06:34,210 --> 00:06:36,273
Let's try this again.

119
00:06:36,273 --> 00:06:40,941
python hours1, 40, 10, 400.

120
00:06:40,941 --> 00:06:43,820
There we go. And let me go and check.

121
00:06:45,090 --> 00:06:45,870
Let's run the data.

122
00:06:45,870 --> 00:06:47,090
35 and 275.

123
00:06:47,090 --> 00:06:50,144
I'll run my program again.

124
00:06:50,144 --> 00:06:56,330
35, $2.75 an hour and 6.23.

125
00:06:56,330 --> 00:06:58,460
Is my sample example right?

126
00:06:58,460 --> 00:06:59,680
Yes it is.

127
00:06:59,680 --> 00:07:00,680
So there we go.

128
00:07:00,680 --> 00:07:01,800
So here you have it.

129
00:07:06,040 --> 00:07:07,810
Hours and rate equal pay.

