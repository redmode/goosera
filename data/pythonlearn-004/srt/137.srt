1
00:00:01,070 --> 00:00:01,430
Hello.

2
00:00:01,430 --> 00:00:06,130
Welcome to my video on how to run
Python on Windows.

3
00:00:06,130 --> 00:00:06,980
I'm Charles Severance

4
00:00:06,980 --> 00:00:09,730
and this is my book, Python for
Informatics.

5
00:00:09,730 --> 00:00:11,470
And I have a bunch of free courses.

6
00:00:11,470 --> 00:00:12,880
The book is free.

7
00:00:12,880 --> 00:00:16,440
And so I, I hope you enjoy 
learning how to program.

8
00:00:18,630 --> 00:00:20,860
So the first thing we're going to do is
download some software.

9
00:00:21,990 --> 00:00:27,544
You need a programmer editor, and so
Notepad Plus Plus is a good one.

10
00:00:27,544 --> 00:00:31,350
It has syntax highlighting and other things,
and it doesn't mess up your files.

11
00:00:31,350 --> 00:00:34,710
It understands that you just want
a plain text file.

12
00:00:34,710 --> 00:00:36,710
So I'm going to download 
and install this.

13
00:00:36,710 --> 00:00:38,650
You want to be an administrator

14
00:00:40,310 --> 00:00:42,010
when you're doing this in your
Windows box. It might,

15
00:00:42,010 --> 00:00:45,620
I'm an administrator on my
particular Windows box.

16
00:00:45,620 --> 00:00:46,130
Going to run it.

17
00:00:49,010 --> 00:00:52,760
And you might get prompted for a password
here, but, I am just going

18
00:00:52,760 --> 00:00:54,970
to say yes because I am the administrator,

19
00:00:54,970 --> 00:00:57,156
and I'll run through the installation
process here.

20
00:01:02,660 --> 00:01:03,760
So that's all done.

21
00:01:03,760 --> 00:01:07,160
I'm not going to bother running it right
now, so there we go.

22
00:01:07,160 --> 00:01:12,120
So I've done Notepad, the next thing
I want to do is I want to download Python.

23
00:01:12,120 --> 00:01:16,400
It's here on the Python download
page. For my class I use Python 2.7.

24
00:01:16,400 --> 00:01:20,080
You could use Python 3, they're slightly
different languages,

25
00:01:20,080 --> 00:01:23,760
I think it's fine to learn both languages,
although we're really at

26
00:01:23,760 --> 00:01:26,910
the point where increasingly people are
starting to use Python 3,

27
00:01:26,910 --> 00:01:29,600
but there's so many things that 
are there for Python 2.

28
00:01:29,600 --> 00:01:33,820
So, the question often asked is like,
which one of these two?

29
00:01:33,880 --> 00:01:35,540
Do I go the X86 or the non?

30
00:01:35,540 --> 00:01:38,100
It's pretty safe to do this one, but

31
00:01:40,290 --> 00:01:48,660
so, if I go Start, and then I type PC info,
this will tell me something about this PC.

32
00:01:48,660 --> 00:01:52,530
Different versions of Windows have this in
different places, but basically I get to

33
00:01:52,530 --> 00:01:57,046
the point where it says I have a system
type 64-bit operating system.

34
00:01:57,046 --> 00:01:59,400
So I can get a 64-bit download, which is

35
00:01:59,400 --> 00:02:02,610
nice to do. So I will go back to Control Panel.


36
00:02:02,610 --> 00:02:08,980
And I'm going to download my Windows
64-bit installer.

37
00:02:11,220 --> 00:02:11,910
And I'm going to run it.

38
00:02:13,780 --> 00:02:16,890
And again, I'm going to need to
be an administrator.

39
00:02:16,890 --> 00:02:18,260
I'm going to install it for all users.

40
00:02:18,260 --> 00:02:21,760
I'm going to leave it in the directory
that it wants to put it on my local drive.

41
00:02:21,760 --> 00:02:23,310
Python27.

42
00:02:23,310 --> 00:02:25,460
You can put it elsewhere, but then you're
going to have to remember it.

43
00:02:25,460 --> 00:02:28,160
Please don't put spaces in the path,
no matter what you do.

44
00:02:29,870 --> 00:02:32,380
And again, I'm the superuser,

45
00:02:32,380 --> 00:02:33,350
so I'm going to say Yes.

46
00:02:36,270 --> 00:02:37,040
So, we're all done.

47
00:02:39,040 --> 00:02:41,980
Okay, so now I'm going to minimize
my web browser.

48
00:02:41,980 --> 00:02:45,600
So the next thing I'm going to do is I'm
going to run Notepad.

49
00:02:45,600 --> 00:02:49,860
And of course you can have shortcuts,
but I'm going to use the Start button.

50
00:02:49,860 --> 00:02:51,828
I'm going to hit Start and I'm

51
00:02:51,828 --> 00:02:56,370
going to type, in my keyboard, notepad,
n-o-t-e-p-a-d.

52
00:02:56,370 --> 00:02:59,600
And up comes Notepad Plus, and 
I will take that.

53
00:02:59,600 --> 00:03:01,020
So here we have Notepad Plus.

54
00:03:02,190 --> 00:03:05,668
And I'm going to write a simple
Python program.

55
00:03:05,668 --> 00:03:11,060
p-r-i-n-t Hello world

56
00:03:11,060 --> 00:03:12,310
Okay?

57
00:03:12,310 --> 00:03:14,120
And I'm going to file, I'm going
to save this.

58
00:03:14,120 --> 00:03:17,480
So I'm going to say File > Save As.

59
00:03:17,480 --> 00:03:19,490
And I'm going to put this on my desktop.

60
00:03:19,490 --> 00:03:21,810
I might, I could make a folder.

61
00:03:21,810 --> 00:03:22,945
Sure, let's make a folder.

62
00:03:22,945 --> 00:03:26,230
[SOUND]

63
00:03:27,230 --> 00:03:31,210
I'll call this learn, because I'm Python
learning.

64
00:03:31,210 --> 00:03:34,440
So I'm going up a folder. You'll notice
the folder showed up as soon as I made it.

65
00:03:36,550 --> 00:03:42,390
Now I'm going to make this file, I'm
going to call it first prog p-y.

66
00:03:44,990 --> 00:03:46,960
Now you'll notice it did syntax coloring
as soon as it

67
00:03:46,960 --> 00:03:49,810
realizes that this is a .py file.

68
00:03:49,810 --> 00:03:53,430
It knows the Python language and so it's
giving me some help.

69
00:03:53,430 --> 00:03:55,569
And if I go into learn, I will see that

70
00:03:55,569 --> 00:03:59,940
there is indeed, sitting in this folder, a
file called firstprog.py.

71
00:03:59,940 --> 00:04:05,960
Unfortunately, Windows hides the .py but
shows us this little python icon.

72
00:04:05,960 --> 00:04:07,750
You can run it by clicking on this but

73
00:04:07,750 --> 00:04:10,130
this is not my preferred way
of running it.

74
00:04:10,130 --> 00:04:11,100
Okay?

75
00:04:11,100 --> 00:04:14,170
So I'm just going to hide, minimize that

76
00:04:14,170 --> 00:04:19,590
and I'm going to say Start, and then
I'm going to type c-m-d.

77
00:04:19,590 --> 00:04:21,880
This is the Command Prompt.

78
00:04:21,880 --> 00:04:26,220
Sometime you'll do Start > Run > cmd on really
old versions of Windows, or just Start > cmd.

79
00:04:26,220 --> 00:04:28,830
And then run Command Prompt.

80
00:04:28,830 --> 00:04:32,170
And, you know, this, this will initially
seem, you know,

81
00:04:32,170 --> 00:04:34,850
like, why am I using this old-school
command prompt?

82
00:04:34,850 --> 00:04:38,160
But after a while, part of the goal
is to really get

83
00:04:38,160 --> 00:04:41,960
to be good at your computer and knowing
how the computer works.

84
00:04:41,960 --> 00:04:44,080
So this is the Command Prompt
and you type commands.

85
00:04:44,080 --> 00:04:45,420
So one of the commands you can type is dir.

86
00:04:45,420 --> 00:04:50,000
You're in your home directory, that's what
Users\csev means.

87
00:04:50,000 --> 00:04:51,820
And I'm going to go into my desktop

88
00:04:51,820 --> 00:04:55,760
with a cd, which stands for change
directory, D-e-s-k-t-o-p.

89
00:04:55,760 --> 00:04:58,050
Now I'm my desktop, move my little
cursor there.

90
00:04:58,050 --> 00:05:01,370
And I do a dir and you'll see a whole
series of things there.

91
00:05:01,370 --> 00:05:02,920
And then you'll see this learn folder,
l-e-a-r-n.

92
00:05:02,920 --> 00:05:07,890
So I'm going to change into that
directory, cd learn, and do a dir there.

93
00:05:07,890 --> 00:05:12,630
And lo and behold, I see exactly what I
was seeing in here.

94
00:05:12,630 --> 00:05:17,630
So this is really just a graphical image
of what's in that folder.

95
00:05:17,630 --> 00:05:21,440
I mean, you can even sort of,
see the path

96
00:05:21,440 --> 00:05:23,448
if you look here, that it's in my

97
00:05:23,448 --> 00:05:27,545
C:\Users\csev\Desktop\learn, which
exactly is where I'm at.

98
00:05:27,545 --> 00:05:29,500
C:\Users\csev\Desktop\learn.

99
00:05:31,240 --> 00:05:34,180
This may seem, and when we, when we start
reading and writing files, it's important

100
00:05:34,180 --> 00:05:39,660
that you put your files in the same folder
here as first, as the programs.

101
00:05:39,660 --> 00:05:40,890
So to run the Python program

102
00:05:40,890 --> 00:05:44,220
you simply type firstprog.py.

103
00:05:44,220 --> 00:05:47,050
It's as, it's as if

104
00:05:48,440 --> 00:05:49,340
Whoops.   

105
00:05:49,340 --> 00:05:52,430
f-i-r-s-t-p-r-o-g dot p-y.

106
00:05:53,620 --> 00:05:56,170
It's as if you built an extension
of the system. The little

107
00:05:56,170 --> 00:06:00,600
program you wrote in Python
is sitting there for you to run.

108
00:06:00,600 --> 00:06:02,350
And I can make a change, like

109
00:06:06,280 --> 00:06:08,489
something else, and save that file.

110
00:06:09,770 --> 00:06:11,310
And then I can run it again.

111
00:06:11,310 --> 00:06:12,500
Now, here's a trick.

112
00:06:12,500 --> 00:06:15,840
I can hit the up arrow and just run the
same program again.

113
00:06:19,240 --> 00:06:22,555
Syntax error, p-r, oh, I put an m in.

114
00:06:22,555 --> 00:06:27,640
[NOISE] print, Save.

115
00:06:27,640 --> 00:06:28,960
Now I just come back here.

116
00:06:28,960 --> 00:06:30,530
I hit up arrow.

117
00:06:30,530 --> 00:06:31,320
And run it again.

118
00:06:31,320 --> 00:06:34,710
So now I have my little two-line program,
and away it goes.

119
00:06:34,710 --> 00:06:37,830
So the only other thing to know
is to how

120
00:06:37,830 --> 00:06:43,120
to start Python for the Command, so you
can do interactive playing with Python.

121
00:06:43,120 --> 00:06:44,960
And this way you can type Python
commands.

122
00:06:44,960 --> 00:06:47,160
And you can add it to your path
and go look

123
00:06:47,160 --> 00:06:49,940
on Google as to how to add something to
your path.

124
00:06:49,940 --> 00:06:56,240
because, it's in, it's in, but 
it's easier just to say c:\python27,

125
00:06:56,240 --> 00:07:02,165
cuz that's where we, oops, python27\python.

126
00:07:03,215 --> 00:07:05,260
I cannot type.

127
00:07:05,260 --> 00:07:05,760
[SOUND]

128
00:07:09,450 --> 00:07:11,000
And so this is the chevron prompt.

129
00:07:11,000 --> 00:07:12,820
Right there's the chevron prompt.

130
00:07:12,820 --> 00:07:18,160
So I can type a print "yo", or I can type,

131
00:07:19,780 --> 00:07:24,128
take me to your python leader.

132
00:07:25,128 --> 00:07:27,630
Syntax error.

133
00:07:27,630 --> 00:07:29,690
Yeah, we've been through this before.

134
00:07:29,690 --> 00:07:31,180
I HATE COMPUTERS.

135
00:07:32,860 --> 00:07:33,650
Okay.

136
00:07:33,650 --> 00:07:35,140
So to get out, but here you gotta be

137
00:07:35,140 --> 00:07:37,180
careful because you can't run
programs, and so you

138
00:07:37,180 --> 00:07:40,360
can type quit to get out and now you're

139
00:07:40,360 --> 00:07:43,080
back at the Command Prompt, and
you can type dir.

140
00:07:43,080 --> 00:07:48,620
So you can learn the commands here. All
we really know is we know dir, cd, and

141
00:07:48,620 --> 00:07:50,540
when in doubt, just close this window,

142
00:07:50,540 --> 00:07:53,320
if you get lost in the wrong folder or
something.

143
00:07:53,320 --> 00:07:57,500
So that's pretty much all I wanted to do.

144
00:07:57,500 --> 00:08:01,410
Please don't make directory names or
folder names with spaces in.

145
00:08:01,410 --> 00:08:06,020
It just makes life confusing, so stick
with simple names.

146
00:08:06,020 --> 00:08:09,180
Don't put long spaces in either folder
names or file names

147
00:08:09,180 --> 00:08:12,600
because then it's harder to
do this stuff, okay?

148
00:08:12,600 --> 00:08:13,010
Thanks.

