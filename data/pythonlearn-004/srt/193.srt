1
00:00:00,470 --> 00:00:01,320
Hello everybody.

2
00:00:01,320 --> 00:00:02,400
This is Dr. Chuck.

3
00:00:02,400 --> 00:00:03,770
We are in New York City.

4
00:00:03,770 --> 00:00:09,880
We are at the largest ever office hours on
like 7th Street and 38th, right?

5
00:00:09,880 --> 00:00:11,220
Yeah, you can take a picture.

6
00:00:11,220 --> 00:00:11,913
That's okay.

7
00:00:11,913 --> 00:00:13,530
You can take a picture, too.

8
00:00:13,530 --> 00:00:17,220
That's right, I keep thinking I should
have you guys film it yourselves.

9
00:00:17,220 --> 00:00:19,080
We need camera people, but okay, here we
go.

10
00:00:19,080 --> 00:00:21,550
So, so we'll just say hi.

11
00:00:21,550 --> 00:00:22,680
>> Hi, I'm Dustin.

12
00:00:22,680 --> 00:00:26,070
It's my first Coursera course and I'm here
to just learn something new.

13
00:00:26,070 --> 00:00:26,620
>> Cool.

14
00:00:26,620 --> 00:00:27,540
>> Hi, I'm Carmen.

15
00:00:27,540 --> 00:00:31,880
It's my first Coursera course and I'm here
to learn to program.

16
00:00:31,880 --> 00:00:32,310
>> Okay.

17
00:00:32,310 --> 00:00:32,810
>> I am Katy.

18
00:00:32,810 --> 00:00:37,310
I've taken a couple of Coursera classes
and thrilled to be here.

19
00:00:37,310 --> 00:00:37,990
>> Hi I'm Mark.

20
00:00:37,990 --> 00:00:41,520
This is the first one that I really hope
to complete.

21
00:00:41,520 --> 00:00:41,700
>> Good.

22
00:00:41,700 --> 00:00:42,825
I hope you complete as well.

23
00:00:42,825 --> 00:00:43,550
>> [LAUGH] Right.

24
00:00:43,550 --> 00:00:44,690
And I do look forward to working

25
00:00:44,690 --> 00:00:47,080
with Chuck, and collaborating with my
colleagues.

26
00:00:47,080 --> 00:00:47,420
>> Cool.

27
00:00:47,420 --> 00:00:50,550
>> Hi, I'm Banesh, I've lost count of how
many Coursera classes I've taken.

28
00:00:50,550 --> 00:00:54,690
But it's been fun to, have the instructor
here.

29
00:00:54,690 --> 00:00:55,080
>> Cool.

30
00:00:55,080 --> 00:00:57,130
>> Hi, my name is Anna.

31
00:00:57,130 --> 00:01:00,570
Hello from Russia, and thanks Dr. Chuck for
everything.

32
00:01:00,570 --> 00:01:02,690
>> We've had many people from Russia in
the course.

33
00:01:02,690 --> 00:01:03,960
>> Hi, I'm Hamed.

34
00:01:03,960 --> 00:01:06,180
I will be the future Coursera student.

35
00:01:06,180 --> 00:01:06,570
>> Okay.

36
00:01:06,570 --> 00:01:09,186
Welcome to the class, in the future.

37
00:01:09,186 --> 00:01:10,400
>> Hi, I'm Rodney.

38
00:01:10,400 --> 00:01:12,017
This is my first Coursera course.

39
00:01:12,017 --> 00:01:14,651
I'm trying to master Python.

40
00:01:14,651 --> 00:01:15,479
>> Hi, I'm Shannon.

41
00:01:15,479 --> 00:01:18,560
It's also my first Coursera course and I'm
excited to start it.

42
00:01:18,560 --> 00:01:19,320
>> Okay.

43
00:01:19,320 --> 00:01:20,640
>> Hey, I'm Brian.

44
00:01:20,640 --> 00:01:22,080
Just want to learn some coding.

45
00:01:22,080 --> 00:01:22,640
>> Okay, Brian.

46
00:01:22,640 --> 00:01:23,620
Welcome to the class.

47
00:01:23,620 --> 00:01:27,660
>> Hi, I'm Aletra and I've been with
Coursera for two years, so.

48
00:01:27,660 --> 00:01:28,540
>> Yeah, yeah.

49
00:01:28,540 --> 00:01:30,350
You were in my very first class.

50
00:01:30,350 --> 00:01:30,630
>> Yes.

51
00:01:30,630 --> 00:01:32,140
>> Yeah, welcome.

52
00:01:32,140 --> 00:01:33,774
>> Yes, I am Michael.

53
00:01:33,774 --> 00:01:39,060
It's my first Coursera course and I hope
that this is going to be a lot of fun.

54
00:01:39,060 --> 00:01:40,250
>> But you're also a teacher right?

55
00:01:40,250 --> 00:01:40,840
>> I'm a teacher.

56
00:01:40,840 --> 00:01:41,720
>> What do you teach?

57
00:01:41,720 --> 00:01:42,650
>> I teach biology.

58
00:01:42,650 --> 00:01:44,720
>> You teach biology, like high school?

59
00:01:44,720 --> 00:01:46,380
>> High school, middle school.

60
00:01:46,380 --> 00:01:48,060
>> Okay, so you gotta find like the
football

61
00:01:48,060 --> 00:01:51,140
coach and get your football coach to teach
Python.

62
00:01:51,140 --> 00:01:51,720
>> Right.

63
00:01:51,720 --> 00:01:52,610
>> Of course.

64
00:01:52,610 --> 00:01:53,280
>> Hi.

65
00:01:53,280 --> 00:01:53,970
I'm Chris.

66
00:01:53,970 --> 00:01:55,970
This is my 14th or 15th MOOC and I.

67
00:01:55,970 --> 00:01:56,630
>> Holy mackerel.

68
00:01:56,630 --> 00:02:00,580
I just wanted to do a meet up because
the professor's so cool.

69
00:02:00,580 --> 00:02:01,540
>> Thanks.

70
00:02:01,540 --> 00:02:02,880
>> Hi, I'm Shem.

71
00:02:02,880 --> 00:02:04,820
This is my first Coursera course.

72
00:02:04,820 --> 00:02:07,823
I work in IT and have, after speaking with

73
00:02:07,823 --> 00:02:11,490
all these beautiful people here, I am so
excited.

74
00:02:11,490 --> 00:02:14,489
Here I come, programming.

75
00:02:14,489 --> 00:02:17,010
>> Okay, so thanks everybody.

76
00:02:17,010 --> 00:02:21,150
Okay, well we'll see everybody
online.

77
00:02:21,150 --> 00:02:24,210
Next week we'll be in Miami, Miami Beach,
Florida.

78
00:02:24,210 --> 00:02:26,035
So we'll see you in Miami.

79
00:02:26,035 --> 00:02:26,422
>> [LAUGH]

