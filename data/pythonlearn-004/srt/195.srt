1
00:00:00,220 --> 00:00:01,010
Hello everybody.

2
00:00:01,010 --> 00:00:05,180
This is Dr. Chuck and we are here in
Washington D.C. in Chinatown at

3
00:00:05,180 --> 00:00:07,210
yet another of the face-to-face office

4
00:00:07,210 --> 00:00:10,370
hours for Internet History and Programming
for Everybody.

5
00:00:10,370 --> 00:00:13,880
And I'd like you to meet some of your
students.

6
00:00:13,880 --> 00:00:17,160
And they'll, they'll say hi, and whatever
they want to say say.

7
00:00:17,160 --> 00:00:17,860
Hi there.

8
00:00:17,860 --> 00:00:21,630
>> Hi, I'm Kate and I'm looking forward to
taking the class.

9
00:00:21,630 --> 00:00:22,510
I'm an overachiever.

10
00:00:22,510 --> 00:00:24,490
And came to office hours before class.

11
00:00:24,490 --> 00:00:25,010
>> Well, that's right.

12
00:00:25,010 --> 00:00:27,630
This, I sent it to the class before.

13
00:00:27,630 --> 00:00:28,370
>> Hi I'm Lulu.

14
00:00:28,370 --> 00:00:30,755
I'm here in Washington, D.C.

15
00:00:30,755 --> 00:00:34,627
I'm really looking forward to learning how
to write

16
00:00:34,627 --> 00:00:38,270
a bit like consistent and easy-to-read
code.

17
00:00:38,270 --> 00:00:42,370
So I'm really looking forward to this
class.

18
00:00:42,370 --> 00:00:43,380
>> Hello, my name is Franklin.

19
00:00:43,380 --> 00:00:48,060
This is my first Coursera course and also my
first endeavor in programming.

20
00:00:48,060 --> 00:00:50,050
So, looking forward to it.

21
00:00:50,050 --> 00:00:52,220
>> You have quite the Boston accent there.

22
00:00:52,220 --> 00:00:53,380
>> Louisiana, actually.

23
00:00:53,380 --> 00:00:54,870
>> Oh, Louisiana, okay.

24
00:00:54,870 --> 00:00:56,770
I figured you were from Britain,
actually.

25
00:00:56,770 --> 00:00:57,520
>> Oh, no, not at all.

26
00:00:57,520 --> 00:00:59,300
>> Oh, okay, okay.

27
00:00:59,300 --> 00:01:03,390
>> Hi, I'm Chris, and I'm doing something
interesting, at least trying to.

28
00:01:03,390 --> 00:01:04,660
>> Cool.

29
00:01:04,660 --> 00:01:08,030
>> Hi, I'm Sue, and I really appreciate
the work that Chuck's put

30
00:01:08,030 --> 00:01:11,740
into doing this, because I'm having a
great time learning Python right now.

31
00:01:11,740 --> 00:01:13,860
>> Do you have any suggestions for
students?

32
00:01:13,860 --> 00:01:14,510
>> Students?

33
00:01:14,510 --> 00:01:15,512
>> That are coming in?

34
00:01:15,512 --> 00:01:19,413
>> I would suggest that you try to think
of other things that

35
00:01:19,413 --> 00:01:24,870
you can do with the programming that
you're learning in the class right now.

36
00:01:24,870 --> 00:01:28,100
To do something fun with the little things
that you've already learned to do.

37
00:01:28,100 --> 00:01:31,980
So I've done things where, we've learned
to do user input and I've written

38
00:01:31,980 --> 00:01:37,710
some codes for myself where you ask for
like little quizzes and spit out output.

39
00:01:37,710 --> 00:01:41,110
Or make people guess numbers to try to
guess the number their friend put in.

40
00:01:41,110 --> 00:01:42,790
>> Are you the one that did the bunny?

41
00:01:42,790 --> 00:01:44,000
In ASCII art?

42
00:01:44,000 --> 00:01:44,320
>> No.

43
00:01:44,320 --> 00:01:45,350
>> Okay, somebody did a bunny.

44
00:01:45,350 --> 00:01:46,045
>> I wish.

45
00:01:46,045 --> 00:01:50,530
[LAUGH] No, I did a Myers Briggs
questionnaire.

46
00:01:50,530 --> 00:01:53,570
>> Oh so you're way way, you're way ahead.

47
00:01:53,570 --> 00:01:54,500
>> I'm playing with it.

48
00:01:54,500 --> 00:01:56,490
>> Cool, good advice.

49
00:01:57,650 --> 00:02:01,031
>> Hello I'm Simon, I'm taking the Python
for Infomatics

50
00:02:01,031 --> 00:02:04,530
class and it's a great class. Stay up with
it.

51
00:02:04,530 --> 00:02:08,650
And watch all the videos including all the 
IEEE interviews.

52
00:02:10,670 --> 00:02:13,355
>> Hi I'm Richard and I'm very grateful to

53
00:02:13,355 --> 00:02:16,390
Dr. Chuck. I'm taking the Python class as
well.

54
00:02:16,390 --> 00:02:20,230
And it's just really helping demystify
technology for me and just have

55
00:02:20,230 --> 00:02:23,785
fun, stick with it. I'm having a great time,
learning a lot. Thanks.

56
00:02:23,785 --> 00:02:26,420
>> OK
>> Hi, I'm Amy.

57
00:02:26,420 --> 00:02:28,520
I've already taken the Internet Security
class with

58
00:02:28,520 --> 00:02:30,580
Dr. Chuck, and I'm going to take the
programming class.

59
00:02:30,580 --> 00:02:34,060
I highly recommend the Internet History
and Security class.

60
00:02:34,060 --> 00:02:35,000
>> I've got a question about that.

61
00:02:35,000 --> 00:02:36,730
How about the, the last part?

62
00:02:36,730 --> 00:02:38,098
Was that particularly hard?

63
00:02:38,098 --> 00:02:39,050
>> The last part?

64
00:02:39,050 --> 00:02:40,420
>> The security part.

65
00:02:40,420 --> 00:02:42,247
>> Yes, although it was very topical,
actually.

66
00:02:42,247 --> 00:02:43,183
>> It was, it was very topical.

67
00:02:43,183 --> 00:02:43,936
>> Absolutely.

68
00:02:43,936 --> 00:02:44,930
>> Did you do okay in the last part?

69
00:02:44,930 --> 00:02:47,030
>> I did, I ended up with a 95.

70
00:02:47,030 --> 00:02:49,590
>> And you decoded all the encrypted
questions and stuff?

71
00:02:49,590 --> 00:02:49,755
>> Yes.

72
00:02:49,755 --> 00:02:50,240
Uh-huh.

73
00:02:50,240 --> 00:02:51,050
I've done that.

74
00:02:51,050 --> 00:02:51,470
>> That's cool.

75
00:02:51,470 --> 00:02:53,792
In that class I encrypt quiz questions and
you've got to decrypt

76
00:02:53,792 --> 00:02:56,450
the quiz questions, and then you've got to
answer the quiz questions.

77
00:02:56,450 --> 00:02:59,180
So that's, that I love that part.

78
00:02:59,180 --> 00:03:01,980
And that part that whole class, that whole
class is

79
00:03:01,980 --> 00:03:04,810
designed to kind of trick you into making
it a

80
00:03:04,810 --> 00:03:06,590
little harder, and a little harder and a
little harder

81
00:03:06,590 --> 00:03:08,690
and at the end you're doing stuff that's
kind of hard.

82
00:03:08,690 --> 00:03:10,340
But I've sort of tricked you into it so

83
00:03:10,340 --> 00:03:12,830
you never really, it's like the, it's like
the frog.

84
00:03:13,860 --> 00:03:14,999
Boiling a frog, right?

85
00:03:14,999 --> 00:03:15,310
>> Mm-hm.

86
00:03:15,310 --> 00:03:19,760
>> Make it harder but you never really
noticed exactly what day it got harder. So

87
00:03:19,760 --> 00:03:22,550
yeah, so I love that class.

88
00:03:22,550 --> 00:03:24,518
Hi my name's Jeff, and after talking with
everyone

89
00:03:24,518 --> 00:03:26,764
here, I'm really looking forward to taking
the class,

90
00:03:26,764 --> 00:03:30,540
the Python class starting on June second
with Dr. Chuck, so.

91
00:03:30,540 --> 00:03:31,260
>> Cool!

92
00:03:31,260 --> 00:03:34,918
So, so again, there we go reporting
again from Washington, D.C.

93
00:03:34,918 --> 00:03:37,927
I think next year, next week I'm in
New York City

94
00:03:37,927 --> 00:03:40,287
and the week after that I'm in Miami, and
then I

95
00:03:40,287 --> 00:03:43,160
think I'm not on a plane for a couple
weeks after that.

96
00:03:43,160 --> 00:03:44,720
So see you in class.

