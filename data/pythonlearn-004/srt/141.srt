1
00:00:00,380 --> 00:00:01,910
Hello, and welcome to Week 2.

2
00:00:01,910 --> 00:00:04,975
I'm going to walk you through
Assignment 2.2.

3
00:00:07,130 --> 00:00:12,360
Spoiler alert: the best thing to do is to
do these before you watch this, right?

4
00:00:12,360 --> 00:00:15,580
So just click over there, and go
ahead and do it.

5
00:00:15,580 --> 00:00:19,339
If you're doing it on your desktop or
laptop that's great, then cut and paste.

6
00:00:20,540 --> 00:00:21,920
So, a spoiler alert.

7
00:00:21,920 --> 00:00:24,400
But I'm going to show you how to do it
because some of you in

8
00:00:24,400 --> 00:00:28,680
the forums have talked about how you're
still a little lost, and that's okay.

9
00:00:28,680 --> 00:00:29,840
You're going to be lost.

10
00:00:29,840 --> 00:00:35,380
It just, that's why I give you these so
that you don't get too stuck.

11
00:00:35,380 --> 00:00:39,150
Because with repetition, this all becomes
second nature.

12
00:00:39,150 --> 00:00:40,945
And give yourself a couple of more weeks

13
00:00:40,945 --> 00:00:43,140
[NOISE] You'll, you'll know everything.

14
00:00:43,140 --> 00:00:49,830
So, so, so this is my last weather alert,
so let's go take a look at Assignment 2.2.

15
00:00:49,830 --> 00:00:55,690
So here I am, Assignment 2.2, I'm going to
go click in to the Autograder and write

16
00:00:55,690 --> 00:00:59,850
a program that uses raw input to prompt a
user for their name and welcomes them.

17
00:00:59,850 --> 00:01:05,160
And you're going to do this, so I'm going
to first do this not in the Autograder.

18
00:01:05,160 --> 00:01:08,970
I'm going to do it on my desktop, and
later I'll do it in the Autograder.

19
00:01:08,970 --> 00:01:13,050
So by now I hope you know if you're
working on your desktop.

20
00:01:13,050 --> 00:01:16,460
For those of you who aren't using desktop,
you can just watch this.

21
00:01:16,460 --> 00:01:21,820
So I've got to write a program that uses
raw input.

22
00:01:21,820 --> 00:01:24,120
And so raw input is a function.

23
00:01:24,120 --> 00:01:29,070
I need a variable, name equals raw_input.

24
00:01:29,070 --> 00:01:31,230
If you've watched the lectures

25
00:01:34,330 --> 00:01:36,290
What is your name.

26
00:01:37,690 --> 00:01:41,860
So basically, raw input pauses a program,
prompts with the what is your

27
00:01:41,860 --> 00:01:46,050
name prompt, and then whatever you type is
moved into the variable name.

28
00:01:46,050 --> 00:01:47,090
Okay?

29
00:01:47,090 --> 00:01:53,490
And then I will say, print Hello, comma, name.

30
00:01:53,490 --> 00:01:57,070
And that says print the string hello, then
afterwards fill in that variable name.

31
00:01:57,070 --> 00:01:59,970
So this is the first line and then the
second line.

32
00:01:59,970 --> 00:02:01,400
Okay? And so that's Hello name.

33
00:02:01,400 --> 00:02:02,810
And so that's our little program.

34
00:02:04,340 --> 00:02:09,350
And I'm going to save this file,
File > SaveAs. I'm going to

35
00:02:09,350 --> 00:02:12,690
put it on my desktop and make a
new folder called

36
00:02:15,750 --> 00:02:20,710
pr4e and go into that folder, and I'm
going to save my file as

37
00:02:20,710 --> 00:02:27,520
assignment 22.py.

38
00:02:27,520 --> 00:02:29,800
It's best to not use spaces in your
assignment

39
00:02:29,800 --> 00:02:34,180
names, just use like dashes, and don't get
too tricky.

40
00:02:34,180 --> 00:02:37,350
I mean, your operating system often
can't handle this.

41
00:02:37,350 --> 00:02:39,380
So I've got this, it's in my

42
00:02:39,380 --> 00:02:41,300
home directory, that's what that little
tilde means.

43
00:02:41,300 --> 00:02:46,810
desktop pr4e assignment 22.py. And I go to
Terminal program,

44
00:02:46,810 --> 00:02:49,770
or command line if you're running
on Windows.

45
00:02:49,770 --> 00:02:52,430
And then I'm going to go into my desktop
because

46
00:02:52,430 --> 00:02:55,620
I'm in my home directory already,
with cd desktop.

47
00:02:55,620 --> 00:03:02,490
Then I'm going to go into pr4e and then
I'm going to type the command ls.

48
00:03:02,490 --> 00:03:04,680
Now if you're on Windows you type dir.

49
00:03:04,680 --> 00:03:08,760
And now I see that I've got this file,
it's the file that I just saved up there.

50
00:03:08,760 --> 00:03:11,075
And in Windows, you would actually type,

51
00:03:11,075 --> 00:03:16,860
assn-2-2.py, you don't need to type
python.

52
00:03:16,860 --> 00:03:21,430
But in the Mac, you type python and then
the following, a-s-s-n-2.

53
00:03:21,430 --> 00:03:25,060
So you'll notice that I said a-s-s and I
do this so fast.

54
00:03:25,060 --> 00:03:28,620
a-s-s-n-2 and I hit the tab key and it
completes for me automatically.

55
00:03:28,620 --> 00:03:30,590
And that says load up Python and then

56
00:03:30,590 --> 00:03:36,070
read the program instructions in that
file, Python a-s-s-n-2.py.

57
00:03:36,070 --> 00:03:37,520
What's your name.

58
00:03:37,520 --> 00:03:38,080
Sarah.

59
00:03:39,380 --> 00:03:41,100
Hello Sarah, okay?

60
00:03:41,100 --> 00:03:42,520
So then we ran it.

61
00:03:42,520 --> 00:03:45,330
Now, let's go back to the Autograder.

62
00:03:45,330 --> 00:03:49,570
Now, if you are just using the Autograder

63
00:03:49,570 --> 00:03:51,430
to do your development, you'd type in this
program.

64
00:03:51,430 --> 00:03:53,610
But if you're using your desktop, the
clever thing to do

65
00:03:53,610 --> 00:03:58,079
is just copy, copy and then come over
here and then paste.

66
00:03:59,570 --> 00:04:01,970
Paste in the working code.

67
00:04:01,970 --> 00:04:02,320
Okay?

68
00:04:02,320 --> 00:04:04,840
And then I'll type Check Code, and this is
going to, the

69
00:04:04,840 --> 00:04:08,730
raw input here is going to prompt up, pop
up a dialog box.

70
00:04:08,730 --> 00:04:10,140
And the prompt is what is your name,

71
00:04:10,140 --> 00:04:12,220
and it tells you you're supposed to
type Sarah.

72
00:04:15,150 --> 00:04:16,240
Well, then you make a mistake,

73
00:04:18,790 --> 00:04:19,190
Alright, Sahar.

74
00:04:19,190 --> 00:04:21,480
I can't type Sarah because I type too
fast.

75
00:04:21,480 --> 00:04:22,680
So it's going to run, the program's
going to

76
00:04:22,680 --> 00:04:24,650
run and now it's going to say, mismatch,

77
00:04:24,650 --> 00:04:26,015
and say, please correct your code and

78
00:04:26,015 --> 00:04:27,990
re-run because I didn't match the
desired output.

79
00:04:27,990 --> 00:04:31,870
I don't have to change my program, I just
have to type the right thing.

80
00:04:31,870 --> 00:04:34,620
So this time when it asks me, I'll type
Sarah.

81
00:04:39,920 --> 00:04:42,620
And now, Hello Sarah.

82
00:04:42,620 --> 00:04:42,950
And my.

83
00:04:42,950 --> 00:04:43,800
I get a match.

84
00:04:43,800 --> 00:04:46,550
And it upgrade, it does the grade updating
on the server.

85
00:04:46,550 --> 00:04:47,190
Okay?

86
00:04:47,190 --> 00:04:48,760
So that's, that's pretty much it.

87
00:04:48,760 --> 00:04:52,960
I mean if you really wanted to, you would,
you know, type name equals.

88
00:04:52,960 --> 00:04:54,688
You can just do all your coding right
here.

89
00:04:54,688 --> 00:05:01,980
[SOUND] What, what, I type so terribly.

90
00:05:01,980 --> 00:05:03,140
What is your name?

91
00:05:06,130 --> 00:05:07,490
And again, I could run this.

92
00:05:09,130 --> 00:05:10,230
Oh wait.

93
00:05:10,230 --> 00:05:12,140
Raw input is not defined on line 1.

94
00:05:12,140 --> 00:05:14,620
Well, I missed a w, I missed a w.

95
00:05:14,620 --> 00:05:16,370
So I can edit it here.

96
00:05:16,370 --> 00:05:18,215
And then I can say run it.

97
00:05:18,215 --> 00:05:27,580
[SOUND] But then there's no output because
they don't have a print statement here.

98
00:05:27,580 --> 00:05:28,725
And I got a little coaching here.

99
00:05:28,725 --> 00:05:30,350
It says you must use the print statement.

100
00:05:30,350 --> 00:05:33,150
So even, even if your output looks
perfect, it

101
00:05:33,150 --> 00:05:35,130
sometimes wants to coach you on
what you did.

102
00:05:35,130 --> 00:05:38,060
It says, hey, you must use the print
statement to print the line of output.

103
00:05:38,060 --> 00:05:41,190
Thank you, virtual coach in the
Autograder.

104
00:05:41,190 --> 00:05:43,880
And so, you don't want to do that.

105
00:05:43,880 --> 00:05:44,690
Okay.

106
00:05:44,690 --> 00:05:46,360
So then I'll put a print.

107
00:05:46,360 --> 00:05:48,630
Print name.

108
00:05:48,630 --> 00:05:50,540
And I'll check that.

109
00:05:51,620 --> 00:05:52,170
And Sarah.

110
00:05:52,170 --> 00:05:54,910
And it says Sarah.

111
00:05:54,910 --> 00:05:56,750
Now it looks likes good, but it's
like it wants

112
00:05:56,750 --> 00:06:02,120
Hello SaraH, so now we We gotta go,
hello Sarah.

113
00:06:02,120 --> 00:06:04,230
Now the space gets added just by that.

114
00:06:04,230 --> 00:06:06,088
The comma actually adds a space.

115
00:06:06,088 --> 00:06:09,220
I keep draggin' these things on my screen.

116
00:06:10,310 --> 00:06:10,930
Hello Sarah.

117
00:06:10,930 --> 00:06:13,290
Now the output matches, it looks happy.

118
00:06:13,290 --> 00:06:14,559
And my grade's updated on the server.

119
00:06:15,700 --> 00:06:16,760
So there you go.

120
00:06:16,760 --> 00:06:22,810
Kind of using both the terminal and a
command line to do program 2.2,

121
00:06:22,810 --> 00:06:26,820
and then pasting it in the Autograder or
just writing it in the Autograder.

122
00:06:26,820 --> 00:06:29,900
I hope this has helped you and I'll see
you in the forums

