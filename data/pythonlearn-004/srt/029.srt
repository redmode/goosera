1
00:00:00,350 --> 00:00:02,701
Hello and welcome to Pythonlearn.com.

2
00:00:02,701 --> 00:00:07,994
We are on this particular video we're
going to show you how to write a simple

3
00:00:07,994 --> 00:00:12,930
program using a text editor and a terminal
on the Raspberry Pi.

4
00:00:12,930 --> 00:00:19,150
Raspberry Pi is a really cool system that
runs Unix Linux and

5
00:00:19,150 --> 00:00:22,670
so we have a desktop, once you got your
Raspberry Pi working.

6
00:00:22,670 --> 00:00:26,050
You should have a desktop. It has a Start
icon at the bottom.

7
00:00:26,050 --> 00:00:28,410
And we're going to start.

8
00:00:28,410 --> 00:00:29,770
We actually don't have to install
anything.

9
00:00:29,770 --> 00:00:33,380
For the Macintosh and the Windows we had
to install various things but

10
00:00:33,380 --> 00:00:37,150
now everything comes on Linux because it's
sort of set up for programmers.

11
00:00:38,460 --> 00:00:40,560
I'll start a text, a simple text editor
and

12
00:00:40,560 --> 00:00:43,520
I'll write a little Python program in the
text editor.

13
00:00:43,520 --> 00:00:44,778
We call these scripts.

14
00:00:44,778 --> 00:00:51,240
And the classic thing to do is to write a
program

15
00:00:51,240 --> 00:00:55,420
that says "hello world".
And I am going to do a Save to this file,

16
00:00:55,420 --> 00:01:04,600
and I am going to put it on my desktop and
make a new folder called assignment 0.

17
00:01:04,600 --> 00:01:10,670
And then, in assignment 0 I'm going to
call my file hello.py and save it.

18
00:01:10,670 --> 00:01:13,330
I got my screen a little small, so you can
see a little better.

19
00:01:13,330 --> 00:01:16,540
You'll notice on that desktop that folder
showed up, and if I click inside

20
00:01:16,540 --> 00:01:20,130
the folder, there is indeed hello.py.

21
00:01:20,130 --> 00:01:21,880
So now what we're going to do is we're
going to

22
00:01:21,880 --> 00:01:24,500
there's a lot of ways you could run
this.

23
00:01:25,620 --> 00:01:28,310
There are things, there are tools called
idle and

24
00:01:28,310 --> 00:01:30,690
you may have even learned and used these
before.

25
00:01:30,690 --> 00:01:34,150
I prefer showing you really how stuff
works.

26
00:01:34,150 --> 00:01:37,740
The technique I'm going to show you works
pretty much the same on a

27
00:01:37,740 --> 00:01:41,670
Macintosh or Windows, where you actually
talk to the operating system and say hey,

28
00:01:41,670 --> 00:01:44,960
run my Python for me.
So I'm going to open up a terminal window.

29
00:01:44,960 --> 00:01:48,860
And a terminal window lets us type
operating system commands.

30
00:01:48,860 --> 00:01:52,620
And so for example, the first command is
ls,

31
00:01:52,620 --> 00:01:56,490
That lists the directories and files that
of wherever you're at.

32
00:01:56,490 --> 00:01:59,040
The pwd stands for print working
directory.

33
00:01:59,040 --> 00:02:00,786
It says where are you at.

34
00:02:00,786 --> 00:02:04,130
And I'm in the /home/c7, that's my
account.

35
00:02:04,130 --> 00:02:06,790
And cd says change directory.
And I'm going to change

36
00:02:06,790 --> 00:02:08,180
into the desktop.

37
00:02:08,180 --> 00:02:10,590
And just as, I can type d e tab, and

38
00:02:10,590 --> 00:02:13,120
as long as that's unique it automatically
goes to the desktop.

39
00:02:13,120 --> 00:02:14,340
And then Enter.

40
00:02:14,340 --> 00:02:18,960
If I type ls, well I see kind of what's
kind of on that desktop.

41
00:02:18,960 --> 00:02:23,690
That desktop has a folder called assn0.
A-S-S-N-0.

42
00:02:23,690 --> 00:02:27,486
And, then I go into that folder, assn0, so
you navigate one

43
00:02:27,486 --> 00:02:32,440
folder in, and you'll see it's telling you
roughly where you're at.

44
00:02:32,440 --> 00:02:34,540
And if I do an ls here in this command
line,

45
00:02:34,540 --> 00:02:37,820
it says, oh, you've got one file, and it's
named hello.py.

46
00:02:37,820 --> 00:02:41,320
And if you look at this, it's showing you
the exact same information.

47
00:02:41,320 --> 00:02:44,300
So we're really, this, this terminal's a
different view.

48
00:02:44,300 --> 00:02:48,170
And I like it because we can type commands
here, and

49
00:02:51,210 --> 00:02:53,958
be smaller, there we go.
You type commands.

50
00:02:53,958 --> 00:02:55,320
I can tell you exactly what to type.

51
00:02:55,320 --> 00:02:57,570
It's easier to write documentation
frankly.

52
00:02:57,570 --> 00:02:59,720
So, we want to run this program.

53
00:02:59,720 --> 00:03:04,020
We wrote the program over here and now we
want to run the program in this window.

54
00:03:04,020 --> 00:03:09,090
So we type python hello.py.

55
00:03:09,090 --> 00:03:12,030
And that says, go find Python.

56
00:03:12,030 --> 00:03:13,920
That's like Microsoft Word or some other

57
00:03:13,920 --> 00:03:16,400
program that happens to be the Python
itself

58
00:03:16,400 --> 00:03:20,790
that we're running.
And feed it hello.py, this one file.

59
00:03:20,790 --> 00:03:23,190
So, when I type Enter, it's going to run
it.

60
00:03:23,190 --> 00:03:25,280
And so it's going to interpret these
commands.

61
00:03:25,280 --> 00:03:28,320
And I told Python that the first thing and
last thing

62
00:03:28,320 --> 00:03:31,410
I wanted to do was to do a print of hello
world.

63
00:03:31,410 --> 00:03:34,730
That, of course, is they're traditional.
This is a script.

64
00:03:34,730 --> 00:03:40,825
It is a series of commands.
Print howdy world.

65
00:03:40,825 --> 00:03:47,880
Print yo world.
I can have all kinds of things in here.

66
00:03:47,880 --> 00:03:52,920
Print peace out.
I don't know what to type.

67
00:03:53,920 --> 00:03:56,800
Now there are some short cuts that you
will get used to using pretty fast.

68
00:03:56,800 --> 00:04:00,590
I can keep saying File Save, but you'll
notice Ctrl+S is a save.

69
00:04:00,590 --> 00:04:02,790
And you'll notice this little asterisk up
here.

70
00:04:02,790 --> 00:04:05,830
Watch when I press Ctrl+S, that asterisk
means the file's been

71
00:04:05,830 --> 00:04:07,560
modified, but not saved.

72
00:04:07,560 --> 00:04:09,830
This will save you a little bit of
insanity.

73
00:04:09,830 --> 00:04:10,850
Make sure you know that.

74
00:04:10,850 --> 00:04:15,490
It's like oh it's not saved, so Ctrl+S,
pressed, and now it's been saved.

75
00:04:15,490 --> 00:04:18,600
That's how I know it's saved and it's been
saved over here.

76
00:04:18,600 --> 00:04:24,280
And now I'm going to say, python hello.py,
to

77
00:04:24,280 --> 00:04:28,150
run it again. And, oh now it's yelling at
me.

78
00:04:31,370 --> 00:04:32,820
Oops, I see what I did wrong.

79
00:04:32,820 --> 00:04:38,270
I should've said print, I should've said
instead of pring I should've said print.

80
00:04:38,270 --> 00:04:41,060
You'd think it would be smart enough to
know that like why

81
00:04:41,060 --> 00:04:44,210
isn't this guy doing print. So I now have
to save it again,

82
00:04:44,210 --> 00:04:47,180
so I do Ctrl+S. And now here is now is
another trick.

83
00:04:47,180 --> 00:04:49,450
You can hit the up arrow and run the
previous command again.

84
00:04:49,450 --> 00:04:53,715
So I hit the up arrow, and hit Enter, and
there is my output.

85
00:04:53,715 --> 00:04:56,100
Okay?
So not too hard.

86
00:04:56,100 --> 00:04:59,200
Everything's already pre-installed on the
Raspberry Pi Linux.

87
00:04:59,200 --> 00:05:01,880
Pretty much all Linux are going to have
something like this.

88
00:05:01,880 --> 00:05:04,080
And so away we go.
Thanks for listening.

