1
00:00:00,200 --> 00:00:02,770
Hello, and welcome to the optional extra

2
00:00:02,770 --> 00:00:06,590
credit assignment for Python for
Everybody, Week 1.

3
00:00:06,590 --> 00:00:08,630
I really want to encourage you to follow

4
00:00:08,630 --> 00:00:10,680
through the scripts, install Python, and
write

5
00:00:10,680 --> 00:00:15,160
your first simple one-line program, using
a text editor and a command line.

6
00:00:15,160 --> 00:00:18,870
I really think that you're going to learn
a lot from this, not just, you know, how to

7
00:00:18,870 --> 00:00:22,740
program, but how to use your computer in
a sort of a more professional way.

8
00:00:22,740 --> 00:00:26,380
Now if you're using a phone or an iPad, you
can't do it.

9
00:00:26,380 --> 00:00:27,340
And that's okay.

10
00:00:27,340 --> 00:00:29,490
But if you're using a laptop, I'd like to
see you

11
00:00:29,490 --> 00:00:31,750
learn a little bit more about how to use
that laptop.

12
00:00:33,100 --> 00:00:36,770
And so this what you'll do is, you'll do,
do, write the program.

13
00:00:36,770 --> 00:00:37,850
Take two screen shots.

14
00:00:37,850 --> 00:00:41,080
One of the text editor and one of

15
00:00:41,080 --> 00:00:43,820
the program being run, and then you'll
upload them.

16
00:00:43,820 --> 00:00:48,200
This is a peer grading system that I
wrote and host on my own server and

17
00:00:48,200 --> 00:00:52,830
it's supposed to make it so that you can,
you know, peer grade really quickly.

18
00:00:53,900 --> 00:00:58,640
So, let's assume that you have your your
two screenshots.

19
00:00:59,710 --> 00:01:00,600
You go here.

20
00:01:00,600 --> 00:01:03,950
Now the one thing you'll notice is it asks
you this course

21
00:01:03,950 --> 00:01:05,730
utilizes third-party learning
tools

22
00:01:05,730 --> 00:01:09,270
from service providers LTI Tools,
dr-chuck.com.

23
00:01:09,270 --> 00:01:11,170
You're sharing your data with these third
parties.

24
00:01:11,170 --> 00:01:12,880
That third party is me, in this case.

25
00:01:12,880 --> 00:01:13,690
And this is really good.

26
00:01:13,690 --> 00:01:18,900
Coursera is making sure that I'm not
getting your data unless you want it.

27
00:01:18,900 --> 00:01:23,250
So you say OK, and then you continue to
my server, external server.

28
00:01:23,250 --> 00:01:26,540
And it gives now the [COUGH] what you're
supposed to do.

29
00:01:26,540 --> 00:01:28,720
Install the Python, write a Hello World

30
00:01:28,720 --> 00:01:31,720
program, take some screen shots,
yada, yada.

31
00:01:31,720 --> 00:01:34,560
And then what you do is when you have
those screen shots, you upload those.

32
00:01:34,560 --> 00:01:37,330
So, I'm going to grab, I've already this,
of course.

33
00:01:37,330 --> 00:01:40,235
I'm going to grab a second file, and I can
put some

34
00:01:40,235 --> 00:01:45,720
comments, I am using a Mac or something,
whatever you want to say.

35
00:01:45,720 --> 00:01:51,180
And then you submit, and when you submit
you can see your submissions.

36
00:01:51,180 --> 00:01:55,300
You're not allowed to remove your
submissions once the process started.

37
00:01:55,300 --> 00:01:58,850
And if there are other students to grade,
you get to grade the other students.

38
00:01:58,850 --> 00:02:03,180
And your, part of your grade comes from
you doing grading,

39
00:02:03,180 --> 00:02:06,700
and another part of the grade comes from
others grading you.

40
00:02:06,700 --> 00:02:10,370
And so you'll get six points for the
grades that you get from

41
00:02:10,370 --> 00:02:14,570
others, and you'll get four points for the
grades you give to others.

42
00:02:14,570 --> 00:02:16,460
You've got to grade at least two

43
00:02:16,460 --> 00:02:18,650
submissions, you can grade up to five
submissions.

44
00:02:18,650 --> 00:02:21,320
So I'll just, it'll randomly pick another
student to grade.

45
00:02:21,320 --> 00:02:23,880
So here we go, I'm going to, I'm going to
take a look

46
00:02:23,880 --> 00:02:28,230
at this person's things, and like okay,
there's, there's their Python.

47
00:02:28,230 --> 00:02:32,950
That looks pretty good, and then there is
their editor.

48
00:02:32,950 --> 00:02:33,670
That looks pretty good.

49
00:02:33,670 --> 00:02:40,130
So I'm going to give this person six
points, and say love

50
00:02:40,130 --> 00:02:45,120
the pics and then grade it.

51
00:02:45,120 --> 00:02:49,220
And I've graded one student, and I could
grade, if I wanted, another student.

52
00:02:49,220 --> 00:02:50,500
So, I will grade another student.

53
00:02:51,550 --> 00:02:53,340
And let's see who this one is.

54
00:02:53,340 --> 00:02:58,110
This is, well, this is oh, this is another
version of me.

55
00:02:59,150 --> 00:03:04,260
That's my instructor account so and so
there we go, session.

56
00:03:04,260 --> 00:03:09,280
Yours shouldn't say "hello Dr. Chuck", it
should say "hello you" and so

57
00:03:09,280 --> 00:03:18,818
I'll give myself six points nice work,
instructor Chuck.

58
00:03:18,818 --> 00:03:20,864
Grade.

59
00:03:20,864 --> 00:03:22,110
Now.

60
00:03:22,110 --> 00:03:22,910
No one has graded it.

61
00:03:22,910 --> 00:03:24,960
So I got 40 percent as my grade.

62
00:03:24,960 --> 00:03:27,580
I just have to stop, right.

63
00:03:27,580 --> 00:03:32,480
I have to quit because there is no one else,
I mean I've graded everything I can grade.

64
00:03:32,480 --> 00:03:36,460
I can grade more students up to five, and
I

65
00:03:36,460 --> 00:03:38,150
just have to wait till others come and
grade me.

66
00:03:38,150 --> 00:03:40,920
But the grade will come back in, once they
grade me.

67
00:03:40,920 --> 00:03:43,800
So, you know, in maybe 20 minutes or an
hour, or whatever.

68
00:03:43,800 --> 00:03:46,130
Someone will grade mine, and my grade will
go up, okay?

69
00:03:46,130 --> 00:03:49,150
And that's it, and when you're all done,
you gotta close the window.

70
00:03:49,150 --> 00:03:51,160
And go check your grade in the grade book.

71
00:03:51,160 --> 00:03:54,750
At this point you should have a 0.4 but as
the

72
00:03:54,750 --> 00:03:58,120
grades, others evaluate you, your grade'll
go up to point one.

73
00:03:58,120 --> 00:04:01,336
So, it shouldn't take you very long to get
all the way up to point one.

74
00:04:01,336 --> 00:04:04,560
And so, that's my little really
lightweight, I call it a social

75
00:04:04,560 --> 00:04:08,820
peer grading system that you should get
feedback within an hour or so.

76
00:04:08,820 --> 00:04:11,200
I use it for like check-off
assignments, just

77
00:04:11,200 --> 00:04:13,280
to make sure that you pretty much did it
right.

78
00:04:13,280 --> 00:04:14,920
There was, you saw it was really easy to

79
00:04:14,920 --> 00:04:16,760
grade and real easy to take a look at
them.

80
00:04:16,760 --> 00:04:19,560
So, I think this is a fun way to

81
00:04:19,560 --> 00:04:21,950
grade assignments and you should get
really quick feedback.

