1
00:00:00,600 --> 00:00:01,170
Hello.

2
00:00:01,170 --> 00:00:05,510
And welcome to our short little 
lecture on peer grading assignments.

3
00:00:06,910 --> 00:00:09,610
Since this is often a 
first class for people

4
00:00:09,610 --> 00:00:13,210
on Coursera and peer grading assignments
may be something that

5
00:00:13,210 --> 00:00:15,590
are seen differently in different
cultures, I figure I'll

6
00:00:15,590 --> 00:00:18,770
just do a little bit of a lecture on that.

7
00:00:18,770 --> 00:00:20,980
First off, they're extra credit.

8
00:00:20,980 --> 00:00:22,600
They're completely optional.

9
00:00:22,600 --> 00:00:27,640
I understand that non-English speakers 
may not want to participate.

10
00:00:27,640 --> 00:00:28,310
And that's okay.

11
00:00:28,310 --> 00:00:30,400
And you can earn all the credit 
in the class just

12
00:00:30,400 --> 00:00:35,050
by doing the regular homework assignments
and taking the quizzes or exams.

13
00:00:35,050 --> 00:00:36,210
So don't worry about it.

14
00:00:36,210 --> 00:00:37,900
But the beauty is that you can take a

15
00:00:37,900 --> 00:00:40,720
bit of risk on this because 
the points are extra.

16
00:00:40,720 --> 00:00:41,350
Right?

17
00:00:41,350 --> 00:00:45,400
They, they don't count against you if you
don't get a perfect grade, unlike the

18
00:00:45,400 --> 00:00:47,710
normal points where you sort of have to

19
00:00:47,710 --> 00:00:49,810
achieve a high grade to 
maintain your percentage.

20
00:00:49,810 --> 00:00:52,720
And so, it's extra credit, 
and then give it a try.

21
00:00:54,270 --> 00:00:57,270
So you have basically a week to 
write and then a week to grade.

22
00:00:57,270 --> 00:01:01,410
And the grading is part of it. You get
points for doing the grading as well.

23
00:01:01,410 --> 00:01:04,600
And so if you don't do grading, then you
don't get full points.

24
00:01:04,600 --> 00:01:08,530
You can get, and so you get points for what
you write and points for what you grade.

25
00:01:08,530 --> 00:01:10,700
And you have a few other people.

26
00:01:10,700 --> 00:01:12,210
Now, the key difference.

27
00:01:12,210 --> 00:01:13,690
This isn't a writing class, right?

28
00:01:13,690 --> 00:01:18,710
We're not, I'm not your English teacher,
I'm not your composition teacher.

29
00:01:18,710 --> 00:01:20,190
I'm a computer scientist.

30
00:01:20,190 --> 00:01:23,920
And the people you're writing for, 
it's a peer-grading assignment.

31
00:01:23,920 --> 00:01:25,700
You're not writing for me.

32
00:01:25,700 --> 00:01:27,290
You're writing for them.

33
00:01:27,290 --> 00:01:31,380
And so, we have a lot of various
education levels.

34
00:01:31,380 --> 00:01:35,030
Not everybody is at the 
same level as whatever.

35
00:01:35,030 --> 00:01:37,920
This class is supposed to be 
sort of a beginning class.

36
00:01:37,920 --> 00:01:40,290
It's not supposed to be a 
super advanced class.

37
00:01:40,290 --> 00:01:42,470
So we really write to that level.

38
00:01:42,470 --> 00:01:47,590
We write to, you know, a 
17 or 18 year old person.

39
00:01:48,590 --> 00:01:49,900
And we want to explain it.

40
00:01:49,900 --> 00:01:53,270
So think of all your writing as 
explaining to somebody, to

41
00:01:53,270 --> 00:01:56,050
someone sitting right next to you, 
rather than trying to impress me.

42
00:01:57,510 --> 00:02:00,800
And understand that we're 
a lot of different people.

43
00:02:00,800 --> 00:02:05,010
This is some data from a previous 
class that I taught.

44
00:02:05,010 --> 00:02:07,980
About half of the students are 
non-English speakers.

45
00:02:07,980 --> 00:02:10,699
I, I teach in English because 
it's the only language I know.

46
00:02:11,960 --> 00:02:14,260
And a lot of students, it'll be 
their first online class.

47
00:02:14,260 --> 00:02:18,120
And so let's just kind of understand that
we're, we're building culture here.

48
00:02:18,120 --> 00:02:20,220
We're not, we're not, there's not
something for us to

49
00:02:20,220 --> 00:02:21,880
fall back on. If we were doing this in a

50
00:02:21,880 --> 00:02:26,120
live classroom, everyone in the 
classroom, in the freshman class, would

51
00:02:26,120 --> 00:02:29,850
be somewhere between, you know, 
17 and 19 years old.

52
00:02:29,850 --> 00:02:32,380
And, and, often from the 
same culture, especially

53
00:02:32,380 --> 00:02:34,830
in undergraduate, and we're 
quite different than that.

54
00:02:34,830 --> 00:02:38,360
We're also very diverse 
in terms of the age that we are.

55
00:02:40,390 --> 00:02:46,600
This class is really aimed at the 18-24
range, kind of this age right here.

56
00:02:46,600 --> 00:02:51,090
And so, if you think about it, that's a
very small fraction of the class.

57
00:02:51,090 --> 00:02:53,760
The great majority of the class is older.

58
00:02:53,760 --> 00:02:55,200
And often educated.

59
00:02:55,200 --> 00:02:56,110
Right?

60
00:02:56,110 --> 00:02:57,840
We'll see that on the next slide.

61
00:02:57,840 --> 00:03:03,020
And so understand that this is how we're,
the questions are being asked.

62
00:03:03,020 --> 00:03:05,420
This is the student for whom 
this class is designed.

63
00:03:06,940 --> 00:03:11,250
And the other thing is, is that we 
have a lot of education in this class.

64
00:03:11,250 --> 00:03:13,050
This is not this particular class, 
it's another class.

65
00:03:13,050 --> 00:03:15,440
But they're, all the data is 
showing that this

66
00:03:15,440 --> 00:03:17,930
is the typical makeup, although 
I want to change that.

67
00:03:17,930 --> 00:03:20,410
I want to make it so that 
more of the target

68
00:03:20,410 --> 00:03:24,070
audience of early college/high school 
are the ones taking this class.

69
00:03:24,070 --> 00:03:26,730
But, for now, we often have the

70
00:03:26,730 --> 00:03:29,600
great majority of the people 
are pretty educated.

71
00:03:29,600 --> 00:03:33,280
So, oh, in this, just kind of 
three-quarters are educated.

72
00:03:33,280 --> 00:03:35,710
Do not write, if you have this degree,

73
00:03:35,710 --> 00:03:37,610
don't write as if you have 
a Masters degree.

74
00:03:38,910 --> 00:03:41,610
You know, don't write as if 
you have, like, a JD

75
00:03:41,610 --> 00:03:44,790
or a doctoral degree and show off 
how smart you are.

76
00:03:44,790 --> 00:03:48,150
Be thoughtful, and use language 
that people can

77
00:03:48,150 --> 00:03:50,691
understand in the class, 
for in the class.

78
00:03:51,970 --> 00:03:57,460
So if you're uncomfortable writing in
English, Google Translate does a reasonable

79
00:03:57,460 --> 00:04:00,670
job, but not perfect, and we'll 
talk about that in a second.

80
00:04:00,670 --> 00:04:04,580
And, and please, please, please don't,
don't run something through Google

81
00:04:04,580 --> 00:04:08,200
Translate and then not tell 
the graders that you did that.

82
00:04:08,200 --> 00:04:10,950
So please just say, I wrote this 
in Spanish and I

83
00:04:10,950 --> 00:04:13,660
translated it in English because my
English is not so good.

84
00:04:14,860 --> 00:04:17,060
Because that's a signal to the 
graders to say

85
00:04:17,060 --> 00:04:20,590
that they really need to ignore 
small grammar errors.

86
00:04:20,590 --> 00:04:24,320
So again I'll talk about that 
in this next slide.

87
00:04:24,320 --> 00:04:27,360
So here's a bit of text that I took from

88
00:04:27,360 --> 00:04:32,280
a description of my Python programming
thing, Python programming class.

89
00:04:32,280 --> 00:04:37,990
And, you know, what I did was I ran it
through Google Translate

90
00:04:37,990 --> 00:04:42,310
a couple of times, and you see all kinds
of weird artifacts, right?

91
00:04:42,310 --> 00:04:45,770
This course is specifically designed 
to be a first programming class.

92
00:04:47,070 --> 00:04:51,500
This course is, is a programming course,
that's not too bad of a change.

93
00:04:52,970 --> 00:04:54,400
Mastery of each of the topics in the

94
00:04:54,400 --> 00:04:57,040
class, mastery of each subject 
in the classroom.

95
00:04:57,040 --> 00:04:59,530
That actually might be better, 
the writing of that one 

96
00:04:59,530 --> 00:05:01,470
might be better than my writing.

97
00:05:01,470 --> 00:05:02,760
But then if we get down here we'll

98
00:05:02,760 --> 00:05:06,670
use simple data analysis as the
programming exercises.

99
00:05:06,670 --> 00:05:10,120
We will use the analysis as simple
programming exercises.

100
00:05:10,120 --> 00:05:12,510
That's starting to sound not so good,

101
00:05:12,510 --> 00:05:15,190
but again that's an artifact 
introduced by translation.

102
00:05:15,190 --> 00:05:17,310
And this last one is terrible, right?

103
00:05:17,310 --> 00:05:21,260
Process data is valuable for everyone
regardless of your career.

104
00:05:21,260 --> 00:05:22,460
Look what happened.

105
00:05:22,460 --> 00:05:23,310
Treat that.

106
00:05:23,310 --> 00:05:27,490
Understand how to treat the data 
is valid for all regardless of race.

107
00:05:27,490 --> 00:05:29,760
This is terribly written.

108
00:05:29,760 --> 00:05:31,090
That's not what I wrote.

109
00:05:31,090 --> 00:05:33,890
That is an effect for Google Translate.

110
00:05:33,890 --> 00:05:34,950
Now when you're writing for Google

111
00:05:34,950 --> 00:05:38,040
Translate, you do want to 
keep your language

112
00:05:38,040 --> 00:05:40,400
less flowery in your native language,

113
00:05:40,400 --> 00:05:43,060
whether it's Spanish or French or
whatever.

114
00:05:43,060 --> 00:05:46,350
You want to use basic structure, 
sentence structure.

115
00:05:46,350 --> 00:05:50,280
Not a lot of, you know, use active voice,
be very direct in what you say.

116
00:05:50,280 --> 00:05:54,870
And give it a try to translate it back,
although what I've found is Google Translate

117
00:05:54,870 --> 00:06:00,450
is surprisingly good at 
translating it back, to

118
00:06:00,450 --> 00:06:03,420
one to go into Spanish and then back out.

119
00:06:03,420 --> 00:06:06,250
What I tend to do is I go from English to

120
00:06:06,250 --> 00:06:10,450
Spanish to French to English and then I
start seeing the artifacts.

121
00:06:10,450 --> 00:06:13,750
So you might want to do that. 
You might want to take your French

122
00:06:13,750 --> 00:06:18,310
to Spanish to English to French and 
then take a look at the initial

123
00:06:18,310 --> 00:06:22,140
French and the changed French and see 
if you can't make it so you can

124
00:06:22,140 --> 00:06:27,210
see in my situation I could see to rewrite
this sentence a little bit, right?

125
00:06:27,210 --> 00:06:31,150
I, I probably wouldn't rewrite anything
else in this but I might, if I

126
00:06:31,150 --> 00:06:35,780
did my English to Spanish to French to
English and I saw this, I'm like,

127
00:06:35,780 --> 00:06:37,860
oops, I'm going to just 
rewrite that sentence

128
00:06:37,860 --> 00:06:39,510
a little bit and run it through those

129
00:06:39,510 --> 00:06:43,920
three translations again, to give you a
sense of how it's going to be seen.

130
00:06:43,920 --> 00:06:44,250
Right?

131
00:06:44,250 --> 00:06:46,960
So that you finally are translating 
the English back in, so.

132
00:06:48,240 --> 00:06:53,390
So again, give it a try, put your name, 
I mean don't put your name, put

133
00:06:53,390 --> 00:06:59,240
the fact that you translated it front
and center in the top of your document.

134
00:06:59,240 --> 00:07:01,590
Plagiarism always comes up in these
classes and

135
00:07:04,010 --> 00:07:08,230
in the earliest of Coursera classes, now
well over a year and a half ago,

136
00:07:08,230 --> 00:07:11,630
it was fiercely discussed.

137
00:07:11,630 --> 00:07:13,420
I think we're better now.

138
00:07:13,420 --> 00:07:17,590
We understand that plagiarism is
potentially a cultural issue.

139
00:07:17,590 --> 00:07:20,390
And different cultures think of 
plagiarism differently.

140
00:07:20,390 --> 00:07:24,020
And so it's important to establish 
rules about plagiarism.

141
00:07:24,020 --> 00:07:26,410
How I perceive plagiarism in this course.

142
00:07:28,390 --> 00:07:30,670
Plagiarism is simply including 
material and

143
00:07:30,670 --> 00:07:32,700
not acknowledging that you've 
included the material.

144
00:07:33,710 --> 00:07:37,400
It's not wrong to include material 
from other sources.

145
00:07:37,400 --> 00:07:42,150
It's wrong to include material from other
sources and represent it as your material.

146
00:07:42,150 --> 00:07:44,180
So when you're turning in an essay, 
the words

147
00:07:44,180 --> 00:07:48,860
in that essay you're representing, are,
came from you, right?

148
00:07:48,860 --> 00:07:50,410
You know, if the question is, 
why is the sky blue,

149
00:07:50,410 --> 00:07:53,790
and you go to, to WikiHow or
something and

150
00:07:53,790 --> 00:07:56,390
you cut and paste something, 
and that's your answer, then,

151
00:07:56,390 --> 00:07:59,870
and you didn't say where it 
came from, that's not good.

152
00:07:59,870 --> 00:08:01,660
So it's perfectly okay to use quotes.

153
00:08:01,660 --> 00:08:04,590
I mean, that's part of writing is
researching.

154
00:08:04,590 --> 00:08:09,000
And if you really love how 
something was said, bring it in.

155
00:08:10,070 --> 00:08:11,540
So here's kind of an example.

156
00:08:12,960 --> 00:08:18,230
You know, let's say I ask you to write a
story, an essay about Vint Cerf.

157
00:08:18,230 --> 00:08:23,370
And I saw this really good 
sentence in Wikipedia,

158
00:08:23,370 --> 00:08:24,890
the bit in the red here in the top.

159
00:08:25,900 --> 00:08:29,460
So the, so this, I saw this bit in
Wikipedia.

160
00:08:29,460 --> 00:08:32,870
And I just wanted to talk about,
you know, his early career.

161
00:08:32,870 --> 00:08:35,390
So, I think Vint Cerf is an important
figure in history of the Internet.

162
00:08:35,390 --> 00:08:36,710
That's my statement.

163
00:08:36,710 --> 00:08:40,460
Then I say in the early days Cerf was a
program manager, blah, blah, blah, blah.

164
00:08:40,460 --> 00:08:44,350
And then I say he later, back to my words,
he later helped define.

165
00:08:44,350 --> 00:08:47,040
Now as a teacher I see this 
really, really quickly.

166
00:08:47,040 --> 00:08:49,990
Because this first part is very
conversational

167
00:08:49,990 --> 00:08:52,310
this next part is very
conversational,

168
00:08:52,310 --> 00:08:55,270
and this is very textbook-like.

169
00:08:55,270 --> 00:08:57,730
it takes a while, but after a while, 
it's not hard

170
00:08:57,730 --> 00:09:02,190
to see when the tone of a, of a 
writing changes.

171
00:09:02,190 --> 00:09:03,790
And all of a sudden it gets really formal

172
00:09:03,790 --> 00:09:07,360
and very fact-oriented, versus very
narrative-oriented.

173
00:09:07,360 --> 00:09:09,770
When I'm writing, I tend to be 
narrative-oriented.

174
00:09:09,770 --> 00:09:11,190
Tell a story.

175
00:09:11,190 --> 00:09:15,020
And when Wikipedia writes, they tend to be
fact-oriented, not telling a story.

176
00:09:15,020 --> 00:09:16,600
So if you intermix your writing with

177
00:09:16,600 --> 00:09:19,310
Wikipedia writing, it kind of 
jumps out at people.

178
00:09:19,310 --> 00:09:21,820
Now, let's say you read that 
and you can't even

179
00:09:21,820 --> 00:09:24,860
think of a better way to, to, 
to phrase it.

180
00:09:24,860 --> 00:09:26,970
Don't just like move the words around.

181
00:09:26,970 --> 00:09:29,590
Just say, put it in as a quote.

182
00:09:29,590 --> 00:09:29,810
Right?

183
00:09:29,810 --> 00:09:31,980
That's what I did in the second one.

184
00:09:31,980 --> 00:09:34,940
I think that Vint Cert's an important
figure in the history of the Internet.

185
00:09:34,940 --> 00:09:36,600
According to Wikipedia,

186
00:09:36,600 --> 00:09:39,395
and then a little, [COUGH], a little
reference mark there.

187
00:09:39,395 --> 00:09:42,880
[SOUND]

188
00:09:42,880 --> 00:09:46,030
And then I literally cut and paste the
exact text from Wikipedia.

189
00:09:46,030 --> 00:09:48,870
Totally legitimate.

190
00:09:48,870 --> 00:09:50,820
I have indicated that it is a quote.

191
00:09:50,820 --> 00:09:52,120
I made it italics.

192
00:09:52,120 --> 00:09:53,540
You don't have to make it italics.

193
00:09:53,540 --> 00:09:56,220
And I had a little thing, and 
then I say he later helped define.

194
00:09:56,220 --> 00:09:58,140
I mean, it's the same thing, but at

195
00:09:58,140 --> 00:10:02,160
least I am valuing the 
Wikipedia contribution.

196
00:10:02,160 --> 00:10:06,570
Now you can't take your whole article 
and say, oh, why is the sky blue.

197
00:10:06,570 --> 00:10:09,730
I pasted the entire answer from
WikiHow, or whatever.

198
00:10:09,730 --> 00:10:11,030
But, do you understand the point?

199
00:10:11,030 --> 00:10:15,900
The point is, if they said it 
really well, include it.

200
00:10:15,900 --> 00:10:17,420
Acknowledge it.

201
00:10:17,420 --> 00:10:20,510
Plagiarism is not the inclusion 
of the text.

202
00:10:20,510 --> 00:10:22,380
Plagiarism is the lack of the

203
00:10:22,380 --> 00:10:25,090
acknowledgement of the 
inclusion of the text.

204
00:10:25,090 --> 00:10:25,850
Okay.

205
00:10:25,850 --> 00:10:29,710
I think I've saying, I think
I've said enough about that.

206
00:10:29,710 --> 00:10:31,140
Okay.

207
00:10:31,140 --> 00:10:33,010
So even though I made a big 
fuss about plagiarism,

208
00:10:33,010 --> 00:10:36,025
I don't expect you to be the 
plagiarism checkers, right?

209
00:10:36,025 --> 00:10:38,180
Read it.

210
00:10:38,180 --> 00:10:40,600
If it feels like there's a 
sentence or two that was

211
00:10:40,600 --> 00:10:42,780
plagiarized, or a paragraph, 
go ahead and stick it in Google.

212
00:10:42,780 --> 00:10:46,190
Google will find in the Wikipedia page if it
just came from Wikipedia.

213
00:10:47,400 --> 00:10:51,750
You know, if you think there's plagiarism,
take off points as appropriate.

214
00:10:51,750 --> 00:10:55,670
You know, if you think that, you know,
you're awarding six points, and you think

215
00:10:55,670 --> 00:10:56,830
that, you know, it's only worth two

216
00:10:56,830 --> 00:10:58,500
because of plagiarism, then 
just make it two.

217
00:11:00,020 --> 00:11:02,600
And please add comments describing
the plagiarism.

218
00:11:02,600 --> 00:11:05,050
Say like, you know, I think 
your fifth sentence came

219
00:11:05,050 --> 00:11:07,640
straight from Wikipedia, and so 
I've taken three points off.

220
00:11:08,870 --> 00:11:11,620
Please do not, do not, do not, do not,

221
00:11:11,620 --> 00:11:17,580
do not, DO NOT add comments about your
opinions about plagiarism.

222
00:11:17,580 --> 00:11:18,560
Okay?

223
00:11:18,560 --> 00:11:20,610
And don't say, like, do you realize

224
00:11:20,610 --> 00:11:24,510
that plagiarism is the world's most
grievous crime?

225
00:11:24,510 --> 00:11:27,390
Or, I, let me tell you a story 
about some one

226
00:11:27,390 --> 00:11:30,230
time I caught somebody in plagiarism 
in a class that I'm teaching.

227
00:11:31,380 --> 00:11:34,180
Just say, I think these two sentences came

228
00:11:34,180 --> 00:11:36,640
from Wikipedia, so I'm only giving you
three points.

229
00:11:36,640 --> 00:11:37,820
Stop there.

230
00:11:37,820 --> 00:11:41,140
Stay with what you, is sort of 
factually about it.

231
00:11:41,140 --> 00:11:46,960
Do not add your feelings about plagiarism
please, please, please, please.

232
00:11:46,960 --> 00:11:50,770
It's simple enough to say I think these
sentences came from Wikipedia.

233
00:11:50,770 --> 00:11:56,939
Do not talk about your views or, 
just stay away from that stuff.

234
00:11:58,580 --> 00:12:00,810
So the kind of grading rubrics 
that I give you in these,

235
00:12:01,950 --> 00:12:05,660
I really am measuring whether or not this
was well written for peers.

236
00:12:05,660 --> 00:12:07,300
Is it interesting to read?

237
00:12:07,300 --> 00:12:09,060
Does it support its arguments?

238
00:12:09,060 --> 00:12:10,870
Is it on topic?

239
00:12:10,870 --> 00:12:12,930
Does it include references?

240
00:12:12,930 --> 00:12:13,720
Okay?

241
00:12:13,720 --> 00:12:14,750
So that's what you're really doing.

242
00:12:14,750 --> 00:12:17,400
So just enjoy the reading of these things.

243
00:12:17,400 --> 00:12:19,490
Don't, don't go like, wow, I am a grader.

244
00:12:19,490 --> 00:12:20,800
I'm an English teacher.

245
00:12:20,800 --> 00:12:22,100
I don't want you to become an 
English teacher.

246
00:12:22,100 --> 00:12:23,568
I want you to just become a reader.

247
00:12:23,568 --> 00:12:27,280
[COUGH] Think of it as you're 
reading a blog post.

248
00:12:27,280 --> 00:12:28,710
Did you enjoy the blog post?

249
00:12:28,710 --> 00:12:29,990
Would you like it?

250
00:12:29,990 --> 00:12:31,490
Would you recommend it to your friends?

251
00:12:31,490 --> 00:12:33,430
Did the, did you enjoy the reading?

252
00:12:33,430 --> 00:12:35,540
So, don't be too harsh.

253
00:12:35,540 --> 00:12:38,120
Don't try to think about this 
as a writing class.

254
00:12:38,120 --> 00:12:40,120
We are communicating ideas, 

255
00:12:40,120 --> 00:12:43,810
and writing is one way that 
we communicate ideas.

256
00:12:43,810 --> 00:12:48,050
So, in terms of grades, don't get too
worried if you get a bad score.

257
00:12:48,050 --> 00:12:50,430
It is not the average 
of all the scores,

258
00:12:50,430 --> 00:12:52,090
there is the, some of the high and the low

259
00:12:52,090 --> 00:12:54,800
stuff gets thrown out, and the score
really comes from

260
00:12:54,800 --> 00:12:58,109
the middle graders, and that's why 
we have multiple graders.

261
00:12:59,360 --> 00:13:04,490
So just relax. If you get a six in your
first one, don't worry about it.

262
00:13:04,490 --> 00:13:06,680
See what you can learn from it, right?

263
00:13:06,680 --> 00:13:08,480
And some of the people will be wrong.

264
00:13:08,480 --> 00:13:11,550
They'll just be, some people are bad
writers, and some people are

265
00:13:11,550 --> 00:13:16,530
bad graders, and you will get some people
who are simply bad graders.

266
00:13:16,530 --> 00:13:18,810
Relax, it's okay.

267
00:13:18,810 --> 00:13:21,440
Hopefully your score won't be 
negatively affected by that.

268
00:13:21,440 --> 00:13:25,730
We're not going to [SIGH]
punish bad graders.

269
00:13:25,730 --> 00:13:28,740
We're not going to sort of 
swoop in and and,

270
00:13:28,740 --> 00:13:31,120
and be the police on the 
grading system.

271
00:13:32,690 --> 00:13:35,870
Generally it works fine with a few
anomalies.

272
00:13:35,870 --> 00:13:39,040
And you just have to tolerate a, 
a few people

273
00:13:39,040 --> 00:13:41,880
that kind of say things that, that they
probably shouldn't say.

274
00:13:41,880 --> 00:13:47,010
So just take what is good and ignore
anything that's bad.

275
00:13:47,010 --> 00:13:49,010
And I remind you that they're 
extra credit.

276
00:13:50,010 --> 00:13:50,270
Right?

277
00:13:50,270 --> 00:13:53,040
If you get six points, it's 
six free points.

278
00:13:53,040 --> 00:13:56,930
It doesn't, it doesn't change how many
points you must earn, it

279
00:13:56,930 --> 00:14:00,970
actually makes it so you can miss points,
or miss quizzes, or whatever.

280
00:14:00,970 --> 00:14:04,090
And I just think part of our goal is to
learn how to do peer grading.

281
00:14:04,090 --> 00:14:07,340
You know? I mean, I think part of 
our goal is to learn how to

282
00:14:07,340 --> 00:14:09,500
learn together, and I think that this peer

283
00:14:09,500 --> 00:14:12,290
grading is a great way of 
exchanging writing.

284
00:14:12,290 --> 00:14:14,690
I mean, it's a, it's like a, writing a blog

285
00:14:14,690 --> 00:14:17,660
post and then forcing five other 
people to read it.

286
00:14:17,660 --> 00:14:20,770
And it, there's a lot of great stuff 
that comes from this.

287
00:14:20,770 --> 00:14:22,630
So let's just learn a lot from this.

288
00:14:22,630 --> 00:14:25,820
So I guess that's all I have.

289
00:14:25,820 --> 00:14:30,800
And I really look forward to reading your
essays online in this class.

290
00:14:30,800 --> 00:14:34,540
I have chosen essay questions 
that I am curious.

291
00:14:34,540 --> 00:14:37,070
I'm curious about the answers 
that you write, so

292
00:14:37,070 --> 00:14:39,810
I'll be looking at them, and 
learning from them.

293
00:14:39,810 --> 00:14:40,170
Who knows?

294
00:14:40,170 --> 00:14:42,450
Maybe we'll have to turn them 
into a book some day.

295
00:14:42,450 --> 00:14:42,860
Cheers.

