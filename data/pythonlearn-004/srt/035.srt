1
00:00:00,170 --> 00:00:05,550
Hello and welcome to our first assignment,
writing the classic Hello World program.

2
00:00:05,550 --> 00:00:08,885
We do this Hello World program to some
degree to

3
00:00:08,885 --> 00:00:12,510
sort of prove that we can get ourselves
inside the computer.

4
00:00:12,510 --> 00:00:15,860
I think of Hello World as putting
ourselves in

5
00:00:15,860 --> 00:00:19,470
the computer and then having ourselves
talk back to us.

6
00:00:19,470 --> 00:00:21,640
The version of us inside the computer
talking back.

7
00:00:21,640 --> 00:00:24,610
So I think it's a interesting
philosophical question.

8
00:00:25,892 --> 00:00:28,280
So I'm going to show you how to use our
autograder.

9
00:00:28,280 --> 00:00:31,020
This program is a one-line program, so
it's really easy, so

10
00:00:31,020 --> 00:00:34,040
you can kind of get used to doing how the
autograder is.

11
00:00:34,040 --> 00:00:38,650
I really want you to do the optional
assignment of installing Python on your

12
00:00:38,650 --> 00:00:43,140
desktop or your laptop, so you can do the
programs on your desktop or your laptop.

13
00:00:43,140 --> 00:00:45,480
because I think you're going to learn a
lot more when you do that.

14
00:00:47,420 --> 00:00:50,020
And as you look at some of the future
videos that I

15
00:00:50,020 --> 00:00:53,740
do for the assignments, I do them all on
the desktop, right?

16
00:00:53,740 --> 00:00:57,490
I, I, I hope that if you're reading those,
you actually learn something about

17
00:00:57,490 --> 00:01:00,979
how to program the desktop, even if you
choose not to do it yourself.

18
00:01:02,290 --> 00:01:05,730
And you'll find that sort of as 
the class starts,

19
00:01:05,730 --> 00:01:09,380
the exercise videos are exactly what
you're doing for homework.

20
00:01:09,380 --> 00:01:12,010
And then they slowly but surely 
sort of move

21
00:01:12,010 --> 00:01:16,060
away and you have increasing
responsibility for writing the code.

22
00:01:16,060 --> 00:01:19,160
Now don't just work from the homework
backwards.

23
00:01:19,160 --> 00:01:21,970
Because if you do you're, you're going to
cheat yourself.

24
00:01:21,970 --> 00:01:23,660
Because all of a sudden, about the sixth

25
00:01:23,660 --> 00:01:25,870
week, you'll be like, I don't know
anything.

26
00:01:25,870 --> 00:01:27,770
And the answer is, well, that's because
you took

27
00:01:27,770 --> 00:01:29,610
the easy way out in the first five weeks.

28
00:01:29,610 --> 00:01:32,500
And then the right thing to do is 
to start over and try it

29
00:01:32,500 --> 00:01:34,060
again, but then be more disciplined the

30
00:01:34,060 --> 00:01:36,710
first time through, and actually learn the
material.

31
00:01:36,710 --> 00:01:37,210
So,

32
00:01:39,030 --> 00:01:43,140
so let's wait a sec, oh yeah, I gotta, I'm
wearing my cool Coursera t-shirt.

33
00:01:43,140 --> 00:01:45,750
And I'm probably the only teacher that
you've got

34
00:01:45,750 --> 00:01:49,170
that has a really cool Coursera tattoo
along with

35
00:01:49,170 --> 00:01:51,230
all the other tools that have to do with

36
00:01:51,230 --> 00:01:54,310
teaching and learning technology, which is
my research area.

37
00:01:54,310 --> 00:01:55,925
And we're going to be using Learning Tools

38
00:01:55,925 --> 00:02:00,230
Interoperability in my research to do all
this stuff.

39
00:02:00,230 --> 00:02:03,570
So, at this point you're probably 
watching this

40
00:02:03,570 --> 00:02:06,930
video right there, the Turn in Hello World
video.

41
00:02:06,930 --> 00:02:11,710
And so each of the autograder assignments
is going to be in Video Lectures.

42
00:02:11,710 --> 00:02:14,720
That's why I call it Video Lectures and
Autograder Links.

43
00:02:14,720 --> 00:02:17,380
And so, you click this. And when you click
this it's going

44
00:02:17,380 --> 00:02:20,970
to open the autograder up in a new window,
a new tab.

45
00:02:20,970 --> 00:02:26,170
You see now I have two tabs. And so this
is a piece of software that I wrote.

46
00:02:26,170 --> 00:02:27,814
You can see that the URL has changed.

47
00:02:27,814 --> 00:02:29,500
This is a server that I control.

48
00:02:30,820 --> 00:02:36,930
And it, it contains a simple 
JavaScript-based Python interpreter.

49
00:02:36,930 --> 00:02:39,710
So you're running this code actually right
in your browser.

50
00:02:41,240 --> 00:02:43,560
And so there's a little editor place.

51
00:02:43,560 --> 00:02:45,680
You can use this to edit your programs.

52
00:02:45,680 --> 00:02:47,447
For the first few, it will be fine.

53
00:02:47,447 --> 00:02:50,819
And it saves all your data, so that's
fine.

54
00:02:50,819 --> 00:02:53,549
But as the programs get longer and
longer and

55
00:02:53,549 --> 00:02:56,861
your output gets longer and longer it's a
little painful.

56
00:02:56,861 --> 00:03:01,810
So, the, the idea here is we have the
program that you're supposed to write.

57
00:03:01,810 --> 00:03:02,820
Write a program that uses a print

58
00:03:02,820 --> 00:03:04,810
statement to say "hello world" as shown
below.

59
00:03:04,810 --> 00:03:06,810
We have the output you're supposed to
achieve.

60
00:03:06,810 --> 00:03:08,750
And, then, here's the output of your
program.

61
00:03:10,440 --> 00:03:13,290
And so to run it, you type Check Code.

62
00:03:13,290 --> 00:03:17,440
And so, what this is saying is, oh,
something's wrong on line one.

63
00:03:17,440 --> 00:03:22,490
And so, I will, I'll fix the mistake
because this is supposed to be a t.

64
00:03:22,490 --> 00:03:24,150
And then I'll make different mistake 
where I

65
00:03:24,150 --> 00:03:27,290
change this to Hello world with capitals,
right?

66
00:03:27,290 --> 00:03:28,570
So now I'm going to run this program.

67
00:03:28,570 --> 00:03:29,760
It's going to actually work.

68
00:03:30,990 --> 00:03:33,810
And you'll see that it runs, but I don't
match.

69
00:03:33,810 --> 00:03:35,320
This is what this mismatch and it's

70
00:03:35,320 --> 00:03:39,148
saying, please correct your code and
rerun.

71
00:03:39,148 --> 00:03:43,820
Now, I'm going to, I'm just going to press
Done for a moment to show you something.

72
00:03:44,820 --> 00:03:47,200
So I've left, and now I'm going to 
go back in.

73
00:03:47,200 --> 00:03:51,030
And if you'll notice, it stored my code,
right?

74
00:03:51,030 --> 00:03:53,500
And, and so it remembers the code that,
the

75
00:03:53,500 --> 00:03:56,120
last code that you've done the Check Code
on.

76
00:03:56,120 --> 00:03:58,100
And and I can run check code again.

77
00:03:58,100 --> 00:03:59,869
And, of course, I get the mismatch.

78
00:03:59,869 --> 00:04:03,864
And then, when I finally get it right by
getting this h to

79
00:04:03,864 --> 00:04:07,689
be a lower case h and then I check code
and it's done.

80
00:04:07,689 --> 00:04:11,043
And then it sends the grade 
automatically back

81
00:04:11,043 --> 00:04:14,408
up to Coursera and so Coursera 
has your grade.

82
00:04:14,408 --> 00:04:15,580
And then you press Done.

83
00:04:15,580 --> 00:04:18,880
Now what's nice is at any point in time
later in the class, you

84
00:04:18,880 --> 00:04:22,670
can come back and you can take a look at
the code that you wrote.

85
00:04:22,670 --> 00:04:24,450
You can even replay, play it again.

86
00:04:24,450 --> 00:04:27,790
You can sort of cut and paste and save it
to your hard drive.

87
00:04:27,790 --> 00:04:29,724
I might need to write a little download
gadget so

88
00:04:29,724 --> 00:04:31,880
that you can actually download it to your
hard drive.

89
00:04:31,880 --> 00:04:35,290
But so that's pretty much, oh, the, the
last

90
00:04:35,290 --> 00:04:37,330
I forgot to show you is this Reset Code
button.

91
00:04:38,660 --> 00:04:41,550
you'll, it stores the code that you wrote.

92
00:04:41,550 --> 00:04:45,100
If you type Reset Code, it says, are you
want to

93
00:04:45,100 --> 00:04:47,500
reset the code that I provided you in the
first place?

94
00:04:47,500 --> 00:04:51,610
And so, if I say OK here, it goes back
to code that has the mistake.

95
00:04:53,450 --> 00:04:55,610
And so, that's pretty much it.

96
00:04:55,610 --> 00:04:59,330
Like I said, I encourage you to try to use
your own laptop to do

97
00:04:59,330 --> 00:05:01,160
the programming and then cut and paste

98
00:05:01,160 --> 00:05:03,420
your working programs into here to test
them.

99
00:05:03,420 --> 00:05:05,040
But I fully understand if you decide to

100
00:05:05,040 --> 00:05:07,850
use your cell phone to do all the
programs.

101
00:05:07,850 --> 00:05:11,480
It'll be a little tired typing all the
programs in on your cell phone.

102
00:05:11,480 --> 00:05:13,150
But, but of course, you can do that.

103
00:05:13,150 --> 00:05:15,000
So again welcome to class.

