1
00:00:02,080 --> 00:00:06,220
Hello, and welcome to our How to Use
Notepad to Run Python Applications.

2
00:00:06,220 --> 00:00:08,270
This is part of my open and

3
00:00:08,270 --> 00:00:13,540
free textbook, Python for Informatics:
Exploring Information.

4
00:00:13,540 --> 00:00:17,890
At this point, you should have, hopefully
by now, installed Python.

5
00:00:17,890 --> 00:00:22,000
If you haven't yet installed Python,
you can go,

6
00:00:22,000 --> 00:00:25,090
My book is really any Python 
version 2 is fine,

7
00:00:25,090 --> 00:00:28,380
 2.5, 2.6, ,2.7.

8
00:00:28,380 --> 00:00:30,490
The simplest thing to do is to go down to

9
00:00:30,490 --> 00:00:35,740
this link right here which is the Windows
x86 MSI installer.

10
00:00:35,740 --> 00:00:39,170
If you're in doubt as to whether or not
you're a Windows 64

11
00:00:39,170 --> 00:00:44,170
or x 32 bit, just go ahead and grab the
32 bit.

12
00:00:44,170 --> 00:00:45,290
Download it, run it.

13
00:00:45,290 --> 00:00:47,240
I won't go through that here.
It's straightforward.

14
00:00:47,240 --> 00:00:51,100
It's pretty much the same as running most
install packages.

15
00:00:52,240 --> 00:00:56,520
The other thing that you're going to
want to install is a programmer editor.

16
00:00:56,520 --> 00:01:03,110
I recommend Notepad Plus for Windows users.
You really don't want to use Notepad.

17
00:01:03,110 --> 00:01:06,370
It will mess up your programs
and turn them

18
00:01:06,370 --> 00:01:09,680
into various things and 
not save them the right way.

19
00:01:09,680 --> 00:01:11,090
So we just want a programmer

20
00:01:11,090 --> 00:01:13,220
editor that understands programs, that's
going to, it'll

21
00:01:13,220 --> 00:01:16,640
be good for HTML, it'll be good for CSS,
good for lots of things.

22
00:01:16,640 --> 00:01:17,470
So, again, download that.

23
00:01:17,470 --> 00:01:18,620
There's a couple of ways.

24
00:01:18,620 --> 00:01:21,520
You can just go Notepad Plus and
Google

25
00:01:21,520 --> 00:01:23,300
and find it and download it 
and install it.

26
00:01:23,300 --> 00:01:26,190
But I'm going to assume that you've done
both of those things.

27
00:01:26,190 --> 00:01:30,820
So now what I'm going to do is I'm
going to show you how to run the program,

28
00:01:30,820 --> 00:01:32,780
assuming you've installed these two
things.

29
00:01:33,810 --> 00:01:36,940
So lets go ahead and start Notepad Plus
Plus.

30
00:01:36,940 --> 00:01:37,800
I've already got it here.

31
00:01:38,870 --> 00:01:39,990
And there we go.

32
00:01:39,990 --> 00:01:43,086
So now I'm going to write my first Python
program.

33
00:01:43,086 --> 00:01:49,640
[SOUND].
Print, "hello world", world.

34
00:01:49,640 --> 00:01:52,100
I'm not a very good typer.
Okay.

35
00:01:52,100 --> 00:01:54,500
So then what we got to do is, I've got to
save it somewhere.

36
00:01:54,500 --> 00:01:56,930
So we're going to run this.

37
00:01:56,930 --> 00:01:59,500
This is just editing a file, so 
I'm going to save it.

38
00:01:59,500 --> 00:02:02,210
Save As, and I'm going to put it on to

39
00:02:02,210 --> 00:02:09,400
my desktop, and I'm going to name the file
firstprog.py.

40
00:02:09,400 --> 00:02:11,690
Now you might want to put this in a
folder.

41
00:02:11,690 --> 00:02:15,430
If you are going to be using, please don't
put names in your folders, okay?

42
00:02:15,430 --> 00:02:17,150
I mean spaces in your folders.

43
00:02:17,150 --> 00:02:18,720
So I can put this in a folder but I'm
going to put it in

44
00:02:18,720 --> 00:02:23,860
my desktop, so the saving shows up right
here. It is indeed on my desktop.

45
00:02:23,860 --> 00:02:26,590
Now to actually run this we are going to
run it through the command line.

46
00:02:26,590 --> 00:02:30,020
There are many ways to do this but I
prefer running it through the command

47
00:02:30,020 --> 00:02:35,440
line so you really get a sense of control
rather than clicking on things and

48
00:02:35,440 --> 00:02:41,010
So, so this is this version of Windows I
have to start a command prompt CMD.

49
00:02:41,010 --> 00:02:46,880
I start CMD, sometimes it's start run CMD
depending on your Windows, but

50
00:02:46,880 --> 00:02:50,490
one way or another you get this little
black and white beautiful little screen.

51
00:02:51,890 --> 00:02:54,650
And where I am at is telling me the
current directory.

52
00:02:54,650 --> 00:02:57,270
I'm in the C drive>Users>ScreenCaster.

53
00:02:57,270 --> 00:03:00,460
That's my home directory. And I can 
go one folder

54
00:03:00,460 --> 00:03:01,840
down to go into my desktop.

55
00:03:02,950 --> 00:03:06,290
And when I am in my desktop in this
window, it's the same as here.

56
00:03:06,290 --> 00:03:09,880
So if I do a dir, which is the command to
list the files,

57
00:03:09,880 --> 00:03:15,248
I see first firstprog.py sitting in the exact
same place that I am.

58
00:03:15,248 --> 00:03:15,630
Okay?

59
00:03:15,630 --> 00:03:23,360
So the way that you run this is you simply
type firstprog.py at the command line.

60
00:03:23,360 --> 00:03:25,760
We are going to run a Python program and

61
00:03:25,760 --> 00:03:30,290
Python sort of understands, Windows
understands that files that end with a

62
00:03:30,290 --> 00:03:34,810
suffix .py are Python programs.
And so,

63
00:03:36,030 --> 00:03:41,870
here is my first program.
But it's saying I've got a syntax error.

64
00:03:41,870 --> 00:03:46,290
It says something, something, something
Print hello world, let me look at

65
00:03:46,290 --> 00:03:51,200
this more closely, print oh, oh, oh, I
should have called that print.

66
00:03:51,200 --> 00:03:51,350
Oh.

67
00:03:51,350 --> 00:03:53,410
And there we go, we see syntax
highlighting, so

68
00:03:53,410 --> 00:03:58,140
now i am going to save this, 
oops, File, Save.

69
00:03:58,140 --> 00:04:00,980
Now here is a little trick that 
you can do. Seeing as how

70
00:04:00,980 --> 00:04:05,700
I just typed firstprog.py, and I
want to run the same command again,

71
00:04:05,700 --> 00:04:07,000
I just hit the up arrow.

72
00:04:07,000 --> 00:04:09,730
And it automatically knows what my
previous command is.

73
00:04:09,730 --> 00:04:12,210
You can scroll thru these things.
A way to go.

74
00:04:12,210 --> 00:04:13,460
firstprog.py.

75
00:04:13,460 --> 00:04:16,490
Okay, that works a little better.
It says Hello world now.

76
00:04:16,490 --> 00:04:19,660
So you just basically will write,

77
00:04:23,240 --> 00:04:24,200
Second.
You know.

78
00:04:24,200 --> 00:04:27,570
Your do stuff here in your Python and you
will save it.

79
00:04:28,710 --> 00:04:29,390
Save it.

80
00:04:29,390 --> 00:04:30,940
And then you'll hit the up arrow and run
it.

81
00:04:30,940 --> 00:04:32,120
And there we go.

82
00:04:32,120 --> 00:04:35,260
I'm, I'm changing.
So, this is a Python program.

83
00:04:35,260 --> 00:04:39,570
It's stored both on your desktop, and here
you can run it.

84
00:04:39,570 --> 00:04:40,070
Okay?

85
00:04:40,070 --> 00:04:46,230
So that is basically, the, the, the idea
of how this, how this works and,

86
00:04:48,930 --> 00:04:52,490
Oh, let me show you one more thing.
Sorry, I was checking my notes.

87
00:04:52,490 --> 00:04:55,270
If you want to run the Python interpreter

88
00:04:55,270 --> 00:04:59,400
interactively, you have to type its full
path.

89
00:04:59,400 --> 00:05:03,090
C, colon, in my case I've got python two
seven installed here.

90
00:05:03,090 --> 00:05:10,590
python 27 backslash python dot exe.

91
00:05:12,500 --> 00:05:14,130
Oh, I didn't do that very well.
But here

92
00:05:14,130 --> 00:05:20,050
I can type the up arrow and I type exe.
And so now I'm at the chevron prompt.

93
00:05:20,050 --> 00:05:22,610
And so I can say like print.

94
00:05:22,610 --> 00:05:28,390
Yo, I can say, x equals 5.
Print 5.

95
00:05:28,390 --> 00:05:33,750
Now to get out of the chevron prompt,
you either type quit with parentheses

96
00:05:33,750 --> 00:05:39,160
or Ctrl+Z and I will type Ctrl+XZ to exit.
And that exits

97
00:05:40,650 --> 00:05:44,470
the Python interpreter, the interactive
mode of Python, okay?

98
00:05:44,470 --> 00:05:45,840
Thanks a lot, bye

