1
00:00:01,320 --> 00:00:01,950
Hello.

2
00:00:01,950 --> 00:00:07,550
Welcome to running Python programs with
TextWrangler on the Macintosh.

3
00:00:07,550 --> 00:00:10,410
So I'm here in Google, and I've typed
TextWrangler

4
00:00:10,410 --> 00:00:13,220
and I can just go to the TextWrangler
website.

5
00:00:14,830 --> 00:00:16,800
And we're going to download TextWrangler.

6
00:00:19,060 --> 00:00:20,930
And we're playing with usability.

7
00:00:20,930 --> 00:00:24,120
I guess we've got just a little, nice
little download button here, Download Now.

8
00:00:30,760 --> 00:00:33,800
Okay, so we're going to download.
Right there, that, maybe that'll work.

9
00:00:33,800 --> 00:00:34,934
Okay, so here we come.

10
00:00:34,934 --> 00:00:39,348
[BLANK_AUDIO]

11
00:00:39,348 --> 00:00:43,950
Okay, so now it's downloaded and is
mounting up the image.

12
00:00:46,230 --> 00:00:49,014
And so I'll come over here and all

13
00:00:49,014 --> 00:00:53,930
we do is we drag the TextWrangler into
Applications.

14
00:00:53,930 --> 00:00:58,010
And then since I'm not a super user on
this account I have

15
00:00:58,010 --> 00:01:02,702
to authenticate, which means I have to type
an account and a password.

16
00:01:02,702 --> 00:01:06,690
And I've already done this once and so I'm
not going to bother copying it but

17
00:01:08,780 --> 00:01:13,480
So we'll cancel this but it will copy in
and you'll have TextWrangler installed.

18
00:01:13,480 --> 00:01:15,380
And I can close all these windows.

19
00:01:16,830 --> 00:01:19,690
And you have TextWrangler, and I'm
going to unmount this

20
00:01:19,690 --> 00:01:22,500
TextWrangler drive so I have a nice, clean
desktop.

21
00:01:22,500 --> 00:01:28,460
So I want to run TextWrangler, and the
easiest way is to start in

22
00:01:29,490 --> 00:01:34,099
with Spotlight, and do TextWrangler, and
click on TextWrangler when it comes up.

23
00:01:35,140 --> 00:01:38,790
And so it's going to ask me a few
questions when we first start out.

24
00:01:38,790 --> 00:01:43,310
And it's got this cool little window here
which I'm going to close that shows us

25
00:01:43,310 --> 00:01:48,380
recently opened documents.
And so I'm going to write some Python code

26
00:01:48,380 --> 00:01:54,598
here.
And I'm going to say print

27
00:01:54,598 --> 00:02:01,640
hello world, oops.
I want to say hello world.

28
00:02:01,640 --> 00:02:04,058
And now I'm going to save this, by saying

29
00:02:04,058 --> 00:02:08,510
File>Save As and I'm going to instead of
going

30
00:02:08,510 --> 00:02:10,320
straight to my desktop you may not, you
might

31
00:02:10,320 --> 00:02:12,060
not have to push this little button to see
these.

32
00:02:12,060 --> 00:02:15,015
I'm going to make a folder called
assignment 0.

33
00:02:16,040 --> 00:02:23,670
And then inside assignment 0, I am going
to name my file firstprog.py.

34
00:02:23,670 --> 00:02:26,730
Now you'll notice that this assignment 0
appeared as

35
00:02:26,730 --> 00:02:29,400
soon as I made it in the desktop, because
this

36
00:02:29,400 --> 00:02:33,990
folder Desktop under Places is the same
as as my desktop.

37
00:02:33,990 --> 00:02:38,680
So now I'm going to save my file. And so
you'll notice that it does what's called

38
00:02:38,680 --> 00:02:42,410
syntax coloring and shows me the fact that
it's

39
00:02:42,410 --> 00:02:46,190
now got a double quoted string in hello
world.

40
00:02:46,190 --> 00:02:51,876
So now I'm going to run Python.
Now there's couple of ways to do it.

41
00:02:51,876 --> 00:02:54,802
First, I'll show you the built-in way in
TextWrangler.

42
00:02:57,950 --> 00:02:59,590
You go up to here, #!

43
00:02:59,590 --> 00:03:03,700
And say, Run in Terminal.

44
00:03:03,700 --> 00:03:06,170
And this will start up a terminal and then

45
00:03:06,170 --> 00:03:10,120
run the Python command interpreter and
feed it our program.

46
00:03:10,120 --> 00:03:14,020
So we say Run in Terminal.
And so, there's some stuff here.

47
00:03:14,020 --> 00:03:16,420
This is just sort of cruddy overhead.

48
00:03:16,420 --> 00:03:19,130
But right here it basically is my output
for my

49
00:03:19,130 --> 00:03:23,490
program, and it says oh, in the file
Users/demo/Desktop

50
00:03:23,490 --> 00:03:26,730
we got a syntax error on line 1: Hello
World, invalid syntax.

51
00:03:26,730 --> 00:03:31,420
Now it doesn't, doesn't tell us much about
how we might fix this, showing just how,

52
00:03:33,620 --> 00:03:37,400
not clever Python is, but it certainly
knows that we've made a mistake. But we

53
00:03:37,400 --> 00:03:39,960
know it's in line one so let's go back and
take a little more closer.

54
00:03:39,960 --> 00:03:43,770
We meant to say print, oh I forgot the t.

55
00:03:43,770 --> 00:03:49,000
So let's go ahead and say t and we'll save
it.

56
00:03:50,860 --> 00:03:54,160
Oh, I didn't mean to lock it. I'll just
save it.

57
00:03:54,160 --> 00:03:56,030
File>Save.

58
00:03:57,250 --> 00:03:58,860
And then I'm going to run it Python

59
00:03:58,860 --> 00:04:02,593
again, Run in Terminal.
And then I see my little Python output.

60
00:04:02,593 --> 00:04:06,621
So you gotta realize that everything other
than this bit here is

61
00:04:06,621 --> 00:04:10,793
the Python output. And I can make changes,
and I close each time.

62
00:04:10,793 --> 00:04:12,962
Hello there, world.

63
00:04:12,962 --> 00:04:18,240
Oops, hello there world.
And I will use a shortcut.

64
00:04:18,240 --> 00:04:21,875
Now, this Cmd+S, there's a little Apple
symbol,

65
00:04:21,875 --> 00:04:24,000
Apple+S will save it, so I'm not going to

66
00:04:24,000 --> 00:04:25,190
actually save it.

67
00:04:25,190 --> 00:04:27,710
I'm just going to save it with Cmd+S and
then I'm going

68
00:04:27,710 --> 00:04:30,430
to run it, oops, I didn't mean to run a
debugger.

69
00:04:30,430 --> 00:04:31,610
No way, did not want to do that.

70
00:04:31,610 --> 00:04:34,200
Close, die.
Let's try that again.

71
00:04:34,200 --> 00:04:35,940
I'm going to run it in Terminal.

72
00:04:37,120 --> 00:04:39,800
And then away we go and you'll see that my
program has changed.

73
00:04:39,800 --> 00:04:44,010
And so we can see that I can run this
program and change it over and over.

74
00:04:45,500 --> 00:04:47,720
Now after a while this will get kind of
tiring.

75
00:04:50,170 --> 00:04:52,150
I'll hide the terminal, get rid of it.

76
00:04:52,150 --> 00:04:56,260
And especially when you need to be in a
particular directory and if you're

77
00:04:56,260 --> 00:04:59,390
opening files and so you're going to want
to run the terminal by hand.

78
00:04:59,390 --> 00:05:03,670
And learn a few terminal commands, and so
I'm going to type Terminal.

79
00:05:05,020 --> 00:05:06,950
Terminal is, we've actually been using
this, but

80
00:05:06,950 --> 00:05:08,300
now I'm going to open a separate one.

81
00:05:08,300 --> 00:05:12,210
And now I'm at this command prompt where I
can type commands.

82
00:05:12,210 --> 00:05:15,960
Make that a little bit bigger so you can
see it better.

83
00:05:15,960 --> 00:05:18,860
And one of the commands is ls, which
stands

84
00:05:18,860 --> 00:05:21,870
for, I don't know, list all my files or
something.

85
00:05:23,060 --> 00:05:26,840
In Windows, this would be dir but in Unix,
it's ls. And

86
00:05:26,840 --> 00:05:30,670
one of the folders that I see is the
Desktop folder there.

87
00:05:30,670 --> 00:05:36,800
So I am going to say, cd Desktop, and then
I'm going to say ls, and

88
00:05:36,800 --> 00:05:41,405
you'll see I have effectively this little
folder that I have got there

89
00:05:41,405 --> 00:05:46,260
is shown as one of the folders is in, in
my Desktop folder.

90
00:05:46,260 --> 00:05:47,080
So, it's a subfolder.

91
00:05:47,080 --> 00:05:52,490
So, I'm going to go into assn, oops, n0,
and do another ls.

92
00:05:53,970 --> 00:05:55,520
Now, if you're wondering where you're at,
you can always

93
00:05:55,520 --> 00:05:59,220
type this command pwd, which stands for
print working directory.

94
00:05:59,220 --> 00:06:01,355
So, I'm in the main hard drive

95
00:06:01,355 --> 00:06:07,137
under Users/demo/Desktop/assn0, and then
within assn0 I've

96
00:06:07,137 --> 00:06:09,900
got one file, firstprog.py.

97
00:06:09,900 --> 00:06:13,920
If I want a little more detail, I add
minus l to my ls and it

98
00:06:13,920 --> 00:06:18,510
can tell me that it's a file, how big it
is, when it was created, et cetera.

99
00:06:18,510 --> 00:06:24,390
Now if I want to run this, I simply type
the command python firstprog.py.

100
00:06:24,390 --> 00:06:28,100
And Python says start the Python
interpreter

101
00:06:28,100 --> 00:06:33,220
and then feed it firstprog.py, and then
Python

102
00:06:33,220 --> 00:06:35,710
runs my little program and gives me
output.

103
00:06:35,710 --> 00:06:37,230
Now a couple of shortcuts.

104
00:06:37,230 --> 00:06:40,540
First shortcut, hit the up arrow.

105
00:06:40,540 --> 00:06:42,190
And you can hit the up arrow a couple of
times.

106
00:06:42,190 --> 00:06:43,650
You can hit it a couple of times.

107
00:06:43,650 --> 00:06:44,420
I am hitting up arrow.

108
00:06:44,420 --> 00:06:47,850
Then I'll hit down arrow and I can run
previous commands.

109
00:06:49,810 --> 00:06:50,430
Okay?

110
00:06:50,430 --> 00:06:54,350
I can also type part of a file name like

111
00:06:54,350 --> 00:06:58,535
python fir and then hit tab and it actually
auto completes

112
00:06:58,535 --> 00:07:00,200
because it knows the file name and looks
at

113
00:07:00,200 --> 00:07:02,160
the file name and it says oh, fir is the

114
00:07:02,160 --> 00:07:04,000
beginning of the only f-, there's only one
file

115
00:07:04,000 --> 00:07:06,730
that begins with fir, so I'll finish it
for you.

116
00:07:06,730 --> 00:07:09,000
And so, the command line is pretty easy.

117
00:07:09,000 --> 00:07:10,990
So, if you want to make a change, you can

118
00:07:10,990 --> 00:07:14,650
just have the command line sitting there
in one window.

119
00:07:18,770 --> 00:07:21,380
And make a change.

120
00:07:21,380 --> 00:07:26,640
And I'm going to just hit Cmd+S to save
it, and then I'm going to come here.

121
00:07:26,640 --> 00:07:29,360
I'm going to hit up arrow, and I'm going
to hit Enter.

122
00:07:29,360 --> 00:07:32,480
So now I can, you know, make a mistake,

123
00:07:34,540 --> 00:07:35,590
like a syntax error.

124
00:07:39,150 --> 00:07:42,250
I can hit Cmd+S to save it, then I can

125
00:07:42,250 --> 00:07:44,460
go over here and hit up arrow, and run it
again.

126
00:07:44,460 --> 00:07:48,170
And it tells me whoa, line two you've got
a syntax error.

127
00:07:48,170 --> 00:07:51,950
And then I will go ahead and fix this,
print sorry.

128
00:07:54,070 --> 00:07:57,410
And so now my command, now I've got valid
Python and

129
00:07:57,410 --> 00:08:00,970
if I hit up arrow, and hit Enter, it says
two lines.

130
00:08:00,970 --> 00:08:01,590
Print sorry.

131
00:08:02,610 --> 00:08:05,440
Now you'll also notice that it's doing what's
called syntax coloring.

132
00:08:05,440 --> 00:08:10,070
It's showing us what part's keywords,
what part's not a keyworld, word, and I

133
00:08:10,070 --> 00:08:13,380
don't know if you noticed, but when I had
the mistake it was black.

134
00:08:13,380 --> 00:08:17,070
Because blue means it's a Python keyword,
prin is just

135
00:08:17,070 --> 00:08:19,020
text, so it doesn't really know what we
meant by that.

136
00:08:19,020 --> 00:08:22,404
And then when I get the syntax right, it
turns blue.

137
00:08:22,404 --> 00:08:24,480
It's, it's not really critical.

138
00:08:24,480 --> 00:08:27,320
It's just, it helps you understand what's
going on.

139
00:08:27,320 --> 00:08:29,520
Syntax coloring gives you good clues.

140
00:08:29,520 --> 00:08:31,960
You can see what line you're on, down
here in

141
00:08:31,960 --> 00:08:34,830
the lower left-hand corner what line
you're on, what column you're in.

142
00:08:34,830 --> 00:08:37,890
And that can actually help you as you look
at syntax errors

143
00:08:37,890 --> 00:08:41,970
when it tells you oh, this syntax error is
on line two.

144
00:08:41,970 --> 00:08:44,420
So that gives you a good start to using

145
00:08:44,420 --> 00:08:50,830
downloading and installing text edit and
using it to do Python on on Macintosh.

146
00:08:50,830 --> 00:08:54,270
Thank you.

