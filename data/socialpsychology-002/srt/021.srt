

1
00:00:05,008 --> 00:00:11,770
In the 1960's, a decade in which people began questioning authority on a large 

2
00:00:11,770 --> 00:00:16,580
scale in the United States and many other countries, Charles Hofling and his 

3
00:00:16,580 --> 00:00:22,240
associates published a disturbing study that showed how obedience in hospital 

4
00:00:22,240 --> 00:00:27,860
settings could literally be lethal if people didn't question authority. 

5
00:00:27,860 --> 00:00:32,780
In the study, 22 nurses received telephone requests from an unknown doctor 

6
00:00:32,780 --> 00:00:38,555
to administer twice the maximum daily dosage of an unknown medication called 

7
00:00:38,555 --> 00:00:42,020
"Astroten." Well, this request should have been 

8
00:00:42,020 --> 00:00:46,540
squarely rejected because hospital rules prohibited the administration of 

9
00:00:46,540 --> 00:00:51,920
unapproved medications even in cases when the nurses knew who the physician was and 

10
00:00:51,920 --> 00:00:58,450
when the dosage wasn't excessive. Nonetheless, 21 out of 22 nurses obeyed 

11
00:00:58,450 --> 00:01:02,315
the doctor's order. The pill, as it turned out, was just a 

12
00:01:02,315 --> 00:01:04,890
pink-colored glucose capsule, a sugar pill. 

13
00:01:04,890 --> 00:01:09,285
There is no such thing as Astroten, and nobody was harmed in the study.  

14
00:01:09,285 --> 00:01:15,120
But another interesting part of the study is that the researchers asked 12 other 

15
00:01:15,120 --> 00:01:21,310
nurses and 21 student nurses to predict what they would do if in this situation. 

16
00:01:22,510 --> 00:01:26,660
The result? Ten of 12 nurses and all 21 student 

17
00:01:26,660 --> 00:01:30,990
nurses thought that, if they had been asked to administer the medication, they 

18
00:01:30,990 --> 00:01:36,080
would have rejected the request. So, on the one hand we have very high 

19
00:01:36,080 --> 00:01:40,450
levels of obedience, and on the other, we have people underestimating how 

20
00:01:40,450 --> 00:01:47,730
vulnerable they are to authority figures -- a potentially deadly combination. 

21
00:01:47,730 --> 00:01:51,260
Now, of course, there's nothing wrong with obedience itself, and there's 

22
00:01:51,260 --> 00:01:54,430
certainly nothing wrong with obeying doctor's orders. 

23
00:01:54,430 --> 00:01:59,310
The primary danger here is blind obedience in which the authority figure 

24
00:01:59,310 --> 00:02:04,420
is never questioned at all, and the risks aren't just medical. 

25
00:02:04,420 --> 00:02:08,435
For example, a study published by Art Brief and his colleagues in the year 

26
00:02:08,435 --> 00:02:13,150
2000 showed how obedience in the workplace might lead to racial 

27
00:02:13,150 --> 00:02:17,170
discrimination. In this study, white college students 

28
00:02:17,170 --> 00:02:21,340
were asked to select job applicants from a pool of candidates for a marketing 

29
00:02:21,340 --> 00:02:25,800
representative position. Roughly half the students received a fake 

30
00:02:25,800 --> 00:02:30,770
letter from the company's president saying, "Our organization attempts to 

31
00:02:30,770 --> 00:02:34,820
match the characteristics of our representatives with the characteristics 

32
00:02:34,820 --> 00:02:37,680
of the population to which they'll be assigned. 

33
00:02:37,680 --> 00:02:42,000
The particular territory to which your selected representative will be assigned 

34
00:02:42,000 --> 00:02:45,497
contains relatively few minority group members. 

35
00:02:45,497 --> 00:02:50,560
Therefore, in this particular situation, I feel that it's important that you do 

36
00:02:50,560 --> 00:02:54,463
not hire anyone that is a member of a minority group." 

37
00:02:54,463 --> 00:02:58,400
The results? White students who were given the 

38
00:02:58,400 --> 00:03:03,245
statement selected fewer than half as many black applicants for an interview as did students who 

39
00:03:03,245 --> 00:03:08,125
received no such statement. So, once again, people obeyed the 

40
00:03:08,125 --> 00:03:13,620
authority figure without any question, only this time, the instructions were to 

41
00:03:13,620 --> 00:03:18,460
discriminate, and the participants were college students -- not a group normally 

42
00:03:18,460 --> 00:03:23,420
known for its high level of obedience. Well, as important and alarming as these 

43
00:03:23,420 --> 00:03:26,940
studies are, when most social psychologists think about research on 

44
00:03:26,940 --> 00:03:31,460
obedience, they think of one person, and that person is Stanley Milgram. 

45
00:03:31,460 --> 00:03:37,060
Thanks to the generosity of Professor Milgram's widow, Alexandra Milgram,

46
00:03:37,060 --> 00:03:41,900
and with the assistance of Alexander Street Press and Professor Tom Blass, who 

47
00:03:41,900 --> 00:03:46,990
wrote a superb biography of Stanley Milgram, I'm pleased to say that we'll be 

48
00:03:46,990 --> 00:03:52,460
able to watch a 40-minute video about Stanley Milgram's research on obedience -- 

49
00:03:52,460 --> 00:03:57,100
arguably the most famous set of experiments ever conducted in social 

50
00:03:57,100 --> 00:04:00,550
psychology. They are also, without doubt, among the 

51
00:04:00,550 --> 00:04:05,120
most ethically controversial -- a topic we'll return to in the next lecture. 

52
00:04:05,120 --> 00:04:10,320
But before you watch the obedience video, which, by the way, Stanley Milgram wrote, 

53
00:04:10,320 --> 00:04:15,200
directed, and narrated, I want to share one important observation 

54
00:04:15,200 --> 00:04:20,690
made by Lee Ross, the person who coined the term fundamental attribution error. 

55
00:04:20,690 --> 00:04:25,060
Professor Ross wrote that, "The Milgram experiments ultimately may have less to 

56
00:04:25,060 --> 00:04:31,384
say about destructive obedience than about ineffective and indecisive 

57
00:04:31,384 --> 00:04:35,100
disobedience." As you'll see in the video, it's not the 

58
00:04:35,100 --> 00:04:39,385
case that the experimental participants failed to object; it's that their 

59
00:04:39,385 --> 00:04:43,150
objections don't prevent them from going on to harm other people,

60
00:04:43,150 --> 00:04:46,650
a very interesting and important distinction. 

61
00:04:46,650 --> 00:04:50,480
But now, instead of commenting further, let me simply invite you to watch the 

62
00:04:50,480 --> 00:04:53,890
video, and I'll have more to say about it in our next lecture. 

