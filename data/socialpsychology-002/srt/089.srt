
1
00:00:22,384 --> 00:00:23,529
I'm Dan Gilbert. 

2
00:00:23,529 --> 00:00:27,620
I'm a social psychologist and a professor here at Harvard University. 

3
00:00:27,620 --> 00:00:31,340
For centuries, happiness has been a subject for poets and philosophers, 

4
00:00:31,340 --> 00:00:36,100
but in the last few decades, psychologists, neuroscientists, even economists 

5
00:00:36,100 --> 00:00:39,150
have begun to study happiness using the same scientific methods 

6
00:00:39,150 --> 00:00:44,971
they use to study every other aspect of human behavior.

7
00:00:47,800 --> 00:00:51,400
Of course, scientists have only recently gotten into the happiness business, 

8
00:00:51,400 --> 00:00:54,220
but that business has been booming for a long time. 

9
00:00:55,510 --> 00:00:58,874
 >> You have to train your mind to recall the simple successes. 

10
00:00:58,874 --> 00:01:01,900
>> Turn on the television, surf the web, visit a bookstore, 

11
00:01:01,900 --> 00:01:04,120
and you'll see that there's no shortage of people 

12
00:01:04,120 --> 00:01:07,438
offering advice about happiness. >> Success university. 

13
00:01:07,438 --> 00:01:11,460
 >> Going to be fun, y'all. >> All my needs, desires, and goals. 

14
00:01:11,460 --> 00:01:15,000
 >> They're all part of a large and growing business known as the "self-help industry."

15
00:01:15,000 --> 00:01:17,726
>> It's a liberation from the horizontal image. 

16
00:01:17,726 --> 00:01:21,000
 >> This industry may or may not have made anyone happy, 

17
00:01:21,000 --> 00:01:25,500
but it's definitely made some people very rich. 

18
00:01:29,800 --> 00:01:35,000
 >> Last year, Americans spent about $10 billion on self-help. 

19
00:01:35,000 --> 00:01:37,000
 >> You're in a preferred section. 

20
00:01:37,000 --> 00:01:40,400
 >> Much of that money was brought in by conferences like this one, 

21
00:01:40,400 --> 00:01:44,200
run by Hay House, a leading self-help publisher. 

22
00:01:48,200 --> 00:01:51,300
Hay House sells books and DVDs on everything from

23
00:01:51,300 --> 00:01:56,000
diet and personal fitness to fairy magic and angel therapy.

24
00:01:56,000 --> 00:01:58,000
 >> Wow, there's a lot of money to be spent here.

25
00:01:58,000 --> 00:02:00,800
 >> Yes, there is. >> Yes. >> How much money are you going to spend here? 

26
00:02:00,800 --> 00:02:03,900
 >> Not that much. >> Well, we didn't even make it around the tables, 

27
00:02:03,900 --> 00:02:07,400
but it looks like we're going to be spending a lot. Stuff looks great. 

28
00:02:07,400 --> 00:02:10,000
 >> The desire for happiness drives different people 

29
00:02:10,000 --> 00:02:13,350
toward different things. Some have come here hoping to lose weight. 

30
00:02:13,350 --> 00:02:15,690
Some are hoping to find a new partner, 

31
00:02:15,690 --> 00:02:18,700
a new career, or a whole new view of the universe. 

32
00:02:18,700 --> 00:02:23,000
 >> Unbelievably, amazingly, in the mind-over-body connection… >>Yeah.

33
00:02:23,000 --> 00:02:26,400
>> Getting a better job, love… >> Yeah, all the -- >> …relationships, all that kind of stuff. 

34
00:02:26,400 --> 00:02:28,000
>> All the good things in life. 

35
00:02:28,000 --> 00:02:30,800
But some people have come here with very serious problems, 

36
00:02:30,800 --> 00:02:34,020
and they may not be in the right place. 

37
00:02:34,020 --> 00:02:38,370
Many have been drawn by Louise Hay, the founder of Hay House. 

38
00:02:38,370 --> 00:02:41,100
She claims to have cured herself of cancer through 

39
00:02:41,100 --> 00:02:43,900
positive thinking and other self-help techniques, 

40
00:02:43,900 --> 00:02:47,300
and she tells others that they can do it, too. 

41
00:02:47,300 --> 00:02:52,090
Although her claims have never been medically verified,  she's been a guest on Oprah, 

42
00:02:52,090 --> 00:02:56,000
and her memoir, "Heal Your Life," has sold over 35 million copies, 

43
00:02:56,000 --> 00:03:00,189
making her one of the best-selling authors of all time. 

44
00:03:00,189 --> 00:03:05,000
 >> Please join me in welcoming Louise Hay. 

45
00:03:15,900 --> 00:03:20,990
 >> I choose the thoughts that make me feel good. 

46
00:03:20,990 --> 00:03:24,855
>> Life loves you. I love you. Do you love you? 

47
00:03:24,855 --> 00:03:27,508
>> Yes. >> That's the important thing. 

48
00:03:27,508 --> 00:03:30,160
 >> We love you. >> Thank you, thank you. 

49
00:03:30,160 --> 00:03:35,000
I accept. I do. You're damn right! 

50
00:03:35,000 --> 00:03:39,500
>> But Hay also believes that thoughts are the cause of all misfortunes. 

51
00:03:39,500 --> 00:03:42,400
>> Because remember, somebody who has depression 

52
00:03:42,400 --> 00:03:46,575
is choosing thoughts that make them feel rotten. 

53
00:03:46,575 --> 00:03:49,500
 >> That by thinking the wrong way we can give ourselves cancer 

54
00:03:49,500 --> 00:03:53,500
or AIDS or depression, cause cars to hit us, or make ourselves 

55
00:03:53,500 --> 00:03:58,050
the target of a terrorist attack. >> When I became 80 I decided that 

56
00:03:58,050 --> 00:04:06,000
it was going to be the best decade of my entire life, and it is! I'm 82!

57
00:04:06,000 --> 00:04:09,000
>> As I go through your book I see all sorts of maladies

58
00:04:09,000 --> 00:04:12,615
and various kinds of thoughts that you say can cause them. 

60
00:04:12,615 --> 00:04:18,020
Alzheimer's is an inability to face life; leprosy, inability to handle life at all. 

61
00:04:18,020 --> 00:04:20,500
 >> Oh, you're picking the fun ones. >> People who have Alzheimer's 

62
00:04:20,500 --> 00:04:24,810
are really removing themselves from life completely. 

63
00:04:24,810 --> 00:04:27,200
 >> How do you know? How do you know what you're saying is right? 

64
00:04:27,200 --> 00:04:28,700
 >> Oh, my inner ding. >> Your inner ding? 

66
00:04:28,700 --> 00:04:30,400
 >> My inner ding. >> Ding? 

67
00:04:30,400 --> 00:04:35,100
 >> It speaks to me, yes. It feels right, or it doesn't feel right. 

68
00:04:35,100 --> 00:04:38,500
Happiness is choosing thoughts that make you feel good. 

69
00:04:38,500 --> 00:04:43,390
It's really very simple. >> I hear you saying that even if there were 

70
00:04:43,390 --> 00:04:47,500
no proof for what you believed, or even if there was 

71
00:04:47,500 --> 00:04:50,000
"scientific evidence" against it, it wouldn't change. 

72
00:04:50,000 --> 00:04:53,400
>> Oh, I don't believe in scientific evidence. I really don't. 

73
00:04:53,400 --> 00:04:57,000
And science is fairly new -- it hasn't been around that long. 

74
00:04:57,000 --> 00:04:59,700
We think it's such a big deal, but it's, you know, 

75
00:04:59,700 --> 00:05:02,600
it's just a way of looking at life. >> But you're telling it to 

76
00:05:02,600 --> 00:05:04,930
millions of people, and you're telling them it's right for them, too. 

77
00:05:04,930 --> 00:05:14,000
>> I tell them what's right for me. My inner ding -- that's all I can say. 

78
00:05:14,000 --> 00:05:16,800
>> You know what? Science is fairly new, 

79
00:05:16,800 --> 00:05:20,000
but when we want to know what causes stars to explode, 

80
00:05:20,000 --> 00:05:24
or butterflies to migrate, we rely on scientific evidence 

81
00:05:24,000 --> 00:05:27,500
rather than our inner ding. So why not do the same thing 

82
00:05:27,500 --> 00:05:30,514
to find out what causes human happiness? 

83
00:05:30,514 --> 00:05:34,900
>> For thousands of years philosophers were asking about happiness 

84
00:05:34,900 --> 00:05:38,400
and talking about happiness but without any data, and only probably 

85
00:05:38,400 --> 00:05:42,250
in the last thirty or forty years have we actually collected scientific data, 

86
00:05:42,250 --> 00:05:46,100
rigorous data, measured happiness, and started to begin to see 

87
00:05:46,100 --> 00:05:51,500
what is it that leads to happiness. >>Psychologist Ed Diener is a pioneer 

88
00:05:51,500 --> 00:05:56,000
in the study of happiness. Since 1980, he and his collaborators have 

89
00:05:56,000 --> 00:05:59,800
crossed the globe to measure it,  surveying diverse communities from  

90
00:05:59,800 --> 00:06:06,000
the Masai tribe in Africa to the Inuit from Northern Canada to the Amish 

91
00:06:06,000 --> 00:06:11,800
of rural Pennsylvania, and many, many others. And the methods Diener uses 

92
00:06:11,800 --> 00:06:16,570
are as diverse as his subjects. >> In happiness research, we ask people 

93
00:06:16,570 --> 00:06:22,080
how happy they are on a one to ten scale. We also look at brain waves. 

94
00:06:22,080 --> 00:06:26,830
 You can look at hormones like cortisol that reflect stress, for example. 

95
00:06:26,830 --> 00:06:31,520
You can also ask family and friends how happy that person is because family and 

96
00:06:31,520 --> 00:06:37,700
friends see how happy they say they are and how happy they act in everyday lives. 

97
00:06:37,700 --> 00:06:41,500
>> Diener has gathered data on nearly a quarter million people, 

98
00:06:41,500 --> 00:06:44,000
and has found that the happy ones have better health, 

99
00:06:44,000 --> 00:06:47,420
better relationships, better sex lives, better careers, and

100
00:06:47,420 --> 00:06:52,540
better just about everything else. >> We think that's an evolutionary thing, 

101
00:06:52,540 --> 00:06:57,030
and the people who are happy probably out-achieve the other people 

102
00:06:57,030 --> 00:06:60,300
and out-reproduce them, and there's more of us around now.

104
00:06:60,300 --> 00:07:03,736
>> Diener's findings have been confirmed by psychologists, 

105
00:07:03,736 --> 00:07:07,460
neuroscientists, and economists around the world. 

106
00:07:07,460 --> 00:07:10,960
We now know a great deal about what actually makes people happy, 

107
00:07:10,960 --> 00:07:14,800
and it isn't always what we think. 

107
00:07:14,800 --> 00:07:16,390
>> What does happiness mean to you? 

108
00:07:16,390 --> 00:07:20,183
>> I'm a firm believer in money is happiness. Okay? I have to be honest. 

109
00:07:20,183 --> 00:07:22,150
>> My children, because I love my children. 

110
00:07:22,150 --> 00:07:24,509
>> I like my summer camp the best. 

111
00:07:24,509 --> 00:07:26,975
>> Is that the best? That makes you happy? 

112
00:07:26,975 --> 00:07:28,870
>> What do you do to make yourself happier? 

113
00:07:28,870 --> 00:07:32,900
>> Well I don't mean to be recalcitrant, but I don't do anything 

114
00:07:32,900 --> 00:07:36,375
in order to be happy. >> Yeah, rolling down your window, 

115
00:07:36,375 --> 00:07:39
and turn the music on, letting the breeze come in. 

116
00:07:39,000 --> 00:07:41,600 
>> What would you say is your greatest source of happiness? 

117
00:07:41,600 --> 00:07:45,400
>> Well, I get a great deal of happiness from beautiful jewelry. 

118
00:07:45,400 --> 00:07:47,520
>> Jewelry? >> Yes. 

119
00:07:47,520 --> 00:07:50,500
>> There are a lot of different opinions about what causes happiness, 

120
00:07:50,500 --> 00:07:54,200
but research suggests that not all of them are exactly on target. 

121
00:07:54,200 --> 00:07:57,000
>> And do you think marriage is a source of happiness? 

122
00:07:57,000 --> 00:07:59,450
>> Most of the time. 

123
00:07:59,450 --> 00:08:02,600
> Depends on how your, what your marriage is like. 

124
00:08:02,600 --> 00:08:04,500
>> What do you think about marriage? Do you think it's 

125
00:08:04,500 --> 00:08:06,600
a source of happiness for most people? >> No. 

126
00:08:06,600 --> 00:08:09,000
>> What about marriage? Do you think married people are 

127
00:08:09,000 --> 00:08:11,300
happier than unmarried people? >> No. 

128
00:08:11,300 --> 00:08:14,690
I don't think they're less happy -- I think it just depends. 

129
00:08:14,690 --> 00:08:17,300
>> Not everyone lives happily ever after, of course, 

130
00:08:17,300 --> 00:08:21,800
but studies do show that, on average, married people are wealthier, 

131
00:08:21,800 --> 00:08:25,900
healthier, and yes, happier than others. 

132
00:08:25,900 --> 00:08:29,219
Do you think children make people happy? >> I think so. 

133
00:08:29,219 --> 00:08:32,096
>> I mean, I think they will be a part of my happiness. 

134
00:08:32,096 --> 00:08:33,780
>> Yes or no: Do children make you happy? 

135
00:08:33,780 --> 00:08:37,600
 >> Yes, mine do. >> So what about those bundles of joy? 

136
00:08:37,600 --> 00:08:41,000
Well, studies show that parents are no happier than non-parents, 

137
00:08:41,000 --> 00:08:43,500
and are often less happy. 

137
00:08:43,500 --> 00:08:44,794
Can money buy happiness? 

138
00:08:44,794 --> 00:08:48,300
>> No. No, it doesn't. >> Do you think money can buy happiness? 

139
00:08:48,300 --> 00:08:51,000
Can it bring happiness? >> No. 

140
00:08:51,000 --> 00:08:53,200
>> Does money buy happiness? >> Not at all. 

141
00:08:53,200 --> 00:08:55,881
>> What about money? Do you think money makes people happy? 

142
00:08:55,881 --> 00:08:57,580
 >> Absolutely. >> Yeah. >> Mm-hmm. 

143
00:08:57,580 --> 00:09:02,500
>> And the more the better? >> For me, especially, the more the better. 

144
00:09:02,500 --> 00:09:05,450
>> Guess what? Money does cause happiness. 

145
00:09:05,450 --> 00:09:07,550
On average, the more money people have, 

146
00:09:07,550 --> 00:09:12,260
the happier they tend to be. So, money? Yes. Marriage? Probably. 

147
00:09:12,260 --> 00:09:16,780
Children? Not so much. What science is teaching us about these 

148
00:09:16,780 --> 00:09:19,810
and many other things is that they are related to happiness, 

149
00:09:19,810 --> 00:09:23,400
but not necessarily in the way you might think. 

150
00:09:23,400 --> 00:09:27,400
 >> In a recent experiment, researchers went to a health club  

151
00:09:27,400 --> 00:09:29,600
and asked a simple question. 

151
00:09:29,600 --> 00:09:32,300
>> You're going to live in the woods for two days and two nights.

152
00:09:32,300 --> 00:09:35,450
 You can have food or water but not both. 

153
00:09:35,450 --> 00:09:39,690
Which would you rather have: food or water? >> Food. 

154
00:09:39,690 --> 00:09:45,010
>> Uh, I'd say food. >> Okay, circle that. 

155
00:09:45,010 --> 00:09:48,550
>> Some of the people were asked this question before they worked out. 

156
00:09:48,550 --> 00:09:52,800
Some were asked after they worked out. And when they were asked 

157
00:09:52,800 --> 00:09:57,150
made a huge difference in how they answered. In fact, people were 50%  

158
00:09:57,150 --> 00:10:01,000
more likely to say water if they'd just finished working out than if 

159
00:10:01,000 --> 00:10:05,934
they were about to start. >> Water. >> Definitely water. >> Okay.

160
00:10:05,934 --> 00:10:09,250
>> Why? People were thirsty after exercising, and because 

161
00:10:09,250 --> 00:10:14,400
they were thirsty now, they imagined being thirsty in the future. 

162
00:10:14,400 --> 00:10:17,390
This is something our brains do all the time. 

163
00:10:17,390 --> 00:10:19,360
When we think about how we'll feel in the future, 

164
00:10:19,360 --> 00:10:23,900
our thoughts are shaped by how we feel in the present. 

165
00:10:23,900 --> 00:10:28,000
It's as if our brains can't believe that we might feel differently later 

166
00:10:28,000 --> 00:10:34,570
than we do now. >> That's not the only thing our brains can't quite believe. 

167
00:10:34,570 --> 00:10:37,900
We all know that feelings inevitably change over time -- 

168
00:10:37,900 --> 00:10:42,000
that new love fades, that fresh wounds heal, and yet when we imagine 

169
00:10:42,000 --> 00:10:46,000
our future joys and our future sorrows, we tend not to think about 

170
00:10:46,000 --> 00:10:50,000
how time will change them. >> We sure do have the compelling intuition 

171
00:10:50,000 --> 00:10:55,000
that somehow improving our life circumstances, our situations -- 

172
00:10:55,000 --> 00:10:59,000
whether it has to do with relationships or jobs or health -- that  

173
00:10:59,000 --> 00:11:02,000
that's going to make us happier, and the truth is that it does make us happier, 

174
00:11:02,000 --> 00:11:06,665
but it doesn't make us happier for as long or as intensely as we think it would. 

175
00:11:06,665 --> 00:11:11,200
>> Psychologist Sonja Lyubomirsky suggests that we're sadly mistaken 

176
00:11:11,200 --> 00:11:14,440
when we expect that the things that make us happy for a day 

177
00:11:14,440 --> 00:11:18,800
will make us happy for a lifetime. >> It's a phenomenon called 

178
00:11:18,800 --> 00:11:23,000
"hedonic adaptation," which means that human beings basically are, 

179
00:11:23,000 --> 00:11:27,900
are just incredibly good at getting used to, getting accustomed to, adapting 

180
00:11:27,900 --> 00:11:33,080
positive changes, especially, in their lives, like the new car or the new house. 

181
00:11:33,080 --> 00:11:37,700
And so that's one of the reasons that we keep sort of buying more stuff, 

182
00:11:37,700 --> 00:11:41,500
is that we, you know, we get the handbag that gives us a boost, it wears off, 

183
00:11:41,500 --> 00:11:46,730
and then we get a new handbag. >> Adaptation affects our experience 

184
00:11:46,730 --> 00:11:51,620
of both good things and bad things in ways that we find difficult to anticipate. 

185
00:11:51,620 --> 00:11:55,270
This is something my colleagues and I have been studying in the lab.

186
00:11:55,270 --> 00:11:57,800
>> And we'd be very happy to give you one as a gift for participating 

187
00:11:57,800 --> 00:12:01,740
in the experiment. Which one would you like to have? 

188
00:12:01,740 --> 00:12:05,660
 >> In one experiment, we brought people into a room filled with art posters 

189
00:12:05,660 --> 00:12:10,000
and offered to give them one. We told half of the participants that 

190
00:12:10,000 --> 00:12:13,300
their decision was irreversible, and that once they'd made it, 

191
00:12:13,300 --> 00:12:18,000
they could never change their minds. We told the other half that they could 

192
00:12:18,000 --> 00:12:22,400
exchange the poster they chose for a different poster any time they wanted. 

193
00:12:22,400 --> 00:12:25,200
After making their choices, we asked both groups 

194
00:12:25,200 --> 00:12:29,400
to rank their posters, and guess what? The people who knew they couldn't 

195
00:12:29,400 --> 00:12:34,919
exchange their posters liked them more than the people who knew they could. 

196
00:12:34,919 --> 00:12:40,000
Why? Because they had no choice, and so they adapted. 

197
00:12:40,000 --> 00:12:41,895
 >> The lesson of this study was clear. 

198
00:12:41,895 --> 00:12:45,800
People find ways to like things when they're stuck with them. 

199
00:12:45,800 --> 00:12:49,000
When we can't change our circumstances, we change the way we 

200
00:12:49,000 --> 00:12:53,800
think about those circumstances, and yet most of us don't seem to realize 

201
00:12:53,800 --> 00:12:56,680
this will happen. >> There is some set point to happiness 

202
00:12:56,680 --> 00:13:00,130
that you tend to return toward. And for some people, it's very happy, 

203
00:13:00,130 --> 00:13:04,000
and for other people, it's mildly happy. And, unfortunately, for a few people 

204
00:13:04,000 --> 00:13:08,400
it may not be happy. But, the set point is good in the sense that 

205
00:13:08,400 --> 00:13:12,200
when we have something terrible in our life happen to us, 

206
00:13:12,200 --> 00:13:15,120
we get very depressed -- we get less satisfied with our life -- 

207
00:13:15,120 --> 00:13:18,400
but slowly over time, we come back toward that set point. 

208
00:13:18,400 --> 00:13:23,000
 >> The question is what self-help actually works and what self-help 

209
00:13:23,000 --> 00:13:28,830
is false and doesn't work.  >> Psychologist Martin Seligman is a 

210
00:13:28,830 --> 00:13:32,100
professor at the University of Pennsylvania and one of the leaders 

211
00:13:32,100 --> 00:13:34,554
of a new movement he calls "positive psychology." 

212
00:13:34,554 --> 00:13:39,480
 >> I realized that psychology had been all about whining and suffering 

213
00:13:39,480 --> 00:13:43,350
and anxiety, depression, victims, suicide, drug addiction, 

214
00:13:43,350 --> 00:13:46,014
and I had spent 35 years working on those things, 

215
00:13:46,014 --> 00:13:49,340
but that that was half-baked. We're pretty good at misery, 

216
00:13:49,340 --> 00:13:53,800
but what we weren't good at was what made life worth living. 

217
00:13:53,800 --> 00:13:56,600
 >> What distinguishes positive psychology from any other 

218
00:13:56,600 --> 00:13:59,530
kind of self-help? According to Seligman, it isn't the message, 

219
00:13:59,530 --> 00:14:04,000
but the method.  >> We do random assignment, placebo-controlled studies 

220
00:14:04,000 --> 00:14:09,000
and ask, "Does that make people happier six months later and less depressed?" 

221
00:14:09,000 --> 00:14:16,400
So, this is an empirically-based endeavor and a scientifically-based endeavor. 

222
00:14:16,400 --> 00:14:19,800
 >> Positive psychology has some very promising new ideas, 

223
00:14:19,800 --> 00:14:23,100
but for now, it's probably fair to say that they need more testing. 

224
00:14:23,100 --> 00:14:27,000
Of course, some scientists think that the most promising ideas 

225
00:14:27,000 --> 00:14:30,890
aren't new at all. One of the best studied techniques for improving 

226
00:14:30,890 --> 00:14:35,400
well-being was developed not at a clinic or a laboratory, 

227
00:14:35,400 --> 00:14:39,386
but on a mountaintop 2,500 years ago. 

228
00:14:41,386 --> 00:14:46,068
 >> Meditation changes our relationship to our thoughts and emotions. 

229
00:14:46,068 --> 00:14:49,020
There's nothing magical about it. 

230
00:14:49,020 --> 00:14:57,250
It's not mysterious. It can be addressed with the known mechanisms of science. 

231
00:14:57,250 --> 00:14:59,600
>> Psychologist Richard Davidson has been studying the 

232
00:14:59,600 --> 00:15:04,600
benefits of meditation since 1992, when the Dalai Lama invited him 

233
00:15:04,600 --> 00:15:09,190
to India to measure the brain activity of Buddhist monks. 

234
00:15:09,190 --> 00:15:15,618
>> He had heard about me through mutual friends and knew that I was a 

235
00:15:15,618 --> 00:15:22,600
respectable, rigorous neuroscientist who had interests in meditation, 

236
00:15:22,600 --> 00:15:27,710
and he was interested in promoting scientific research on meditation, 

237
00:15:27,710 --> 00:15:31,260
and we thought that this would be an amazing opportunity. 

238
00:15:31,260 --> 00:15:37,450
We can bring portable equipment with us, and schlep it up the mountain, 

239
00:15:37,450 --> 00:15:44,300
and see if we can collect some data. So we had quite a journey. 

240
00:15:44,300 --> 00:15:48,020
We brought about 5,000 pounds of equipment with us. 

241
00:15:48,020 --> 00:15:55,000
We looked like this crazy traveling rock band with, you know, these enormous cases. 

242
00:15:55,000 --> 00:15:57,230
>> Unfortunately, once Davidson was ready 

243
00:15:57,230 --> 00:16:00,650
to collect the data, the monks had a change of heart. 

244
00:16:00,650 --> 00:16:04,100
>> They said that they'd be happy to teach us everything they know 

245
00:16:04,100 --> 00:16:09,000
about meditation, but they didn't really want to do the tests. 

246
00:16:09,000 --> 00:16:13,500
>> But the Dalai Lama himself wanted to continue fostering the dialogue between

247
00:16:13,500 --> 00:16:17,450
Buddhism and Western science. >> This kind of a dialogue very 

248
00:16:17,450 --> 00:16:26,000
helpful to expand human knowledge on this, not yet explored. 

249
00:16:26,000 --> 00:16:32,000
And still vast subject. Our knowledge very limited. 

250
00:16:32,000 --> 00:16:38,900
So, particularly about inner world, inner signs, inner space. 

251
00:16:38,900 --> 00:16:41,720
 >> In 2000, the Dalai Lama began sending  

252
00:16:41,720 --> 00:16:45,500
monks to Davidson's laboratory at the University of Wisconsin, 

253
00:16:45,500 --> 00:16:49,199
where they participated in a series of neuropsychological studies. 

254
00:16:49,199 --> 00:16:53,355
The results surprised Davidson himself. >> I thought about meditation 

255
00:16:53,355 --> 00:16:58,260
as relaxation. In fact, what we see in the brain of an 

256
00:16:58,260 --> 00:17:02,000
advanced meditation practitioner when he or she is meditating is 

257
00:17:02,000 --> 00:17:08,160
heightened activation, not relaxation. >> Davidson's studies of Buddhist monks 

258
00:17:08,160 --> 00:17:11,800
and other experienced meditators suggested that while they may appear 

259
00:17:11,800 --> 00:17:15,710
to be resting, their minds are actually intensely focused, 

260
00:17:15,710 --> 00:17:20,200
and as they meditate more and more, their brains begin to change. 

261
00:17:20,200 --> 00:17:24,000
When Davidson compared scans of the monks to scans of people who'd 

262
00:17:24,000 --> 00:17:28,300
never meditated, he found that the monks showed considerably more activity 

263
00:17:28,300 --> 00:17:33,510
in the left prefrontal cortex -- a part of the brain that registers positive emotions. 

264
00:17:33,510 --> 00:17:37,410
>>There are certain circuits in the brain that are more sensitive to the processing 

265
00:17:37,410 --> 00:17:41,700
of negative emotions and other circuits that are more sensitive to the processing 

266
00:17:41,700 --> 00:17:46,670
of positive emotions. And more than any other organ in our body, 

267
00:17:46,670 --> 00:17:50,860
the brain is built to change in response to experience. 

268
00:17:50,860 --> 00:17:56,135
It's a learning machine. The scientific evidence suggests that 

269
00:17:56,135 --> 00:18:01,400
we can actually train our mind and shape its circuitry in our brain 

270
00:18:01,400 --> 00:18:07,000
in very specific ways to promote increased levels of well-being. 

271
00:18:07,000 --> 00:18:12,585
>> I think scientific findings also very, very useful, very convincing. 

272
00:18:12,585 --> 00:18:22,548
It can be useful for medical science, and also can be useful for bringing 

273
00:18:22,548 --> 00:18:28,612
new generation more healthier mind, more compassionate mind. 

274
00:18:35,612 --> 00:18:39,590
 >> So, is there a secret to happiness? 

275
00:18:39,590 --> 00:18:42,960
Well, yes, but it isn't very much of a secret. 

276
00:18:42,960 --> 00:18:45,720
Scientific research suggests that a lot of things matter -- 

277
00:18:45,720 --> 00:18:49,810
money, health, work -- but that one thing matters a lot, 

278
00:18:49,810 --> 00:18:54,020
and that thing is social relationships. 

279
00:18:54,020 --> 00:18:59,400
We're one of the most social animals on our planet. We need each other for just about everything,

280
00:18:59,400 --> 00:19:01,640
so it isn't very surprising that nature's designed us

281
00:19:01,640 --> 00:19:05,800
to experience happiness when we're connected to others -- 

282
00:19:05,800 --> 00:19:13,800
family, friends, lovers. In fact, new evidence suggests that 

283
00:19:13,800 --> 00:19:16,700
happiness may be as contagious as the common cold,

284
00:19:16,700 --> 00:19:21,270
that it can be passed along from person to person, and that it can travel farther 

285
00:19:21,270 --> 00:19:25,000
and faster than anyone imagined. 

286
00:19:28,000 --> 00:19:35,990
This is Framingham, Massachusetts. In 1948, medical researchers began 

287
00:19:35,990 --> 00:19:39,610
tracking the residents of this town, trying to learn about the behaviors 

288
00:19:39,610 --> 00:19:45,180
that cause heart disease. Their work continues today. 

289
00:19:45,180 --> 00:19:48,900
Known as the Framingham Heart Study, it's one of the world's most extensive 

290
00:19:48,900 --> 00:19:53,400
epidemiological studies, containing six decades' worth of data on the 

291
00:19:53,400 --> 00:19:58,950
health of more than 15,000 people. And hidden in this mountain of data 

292
00:19:58,950 --> 00:20:04,500
is a deep and powerful secret about the nature of human happiness. 

293
00:20:04,500 --> 00:20:10,430
 >> Your happiness depends upon whole large clusters of people 

294
00:20:10,430 --> 00:20:14,000
you don't even know, and, and who you have no way of knowing. 

295
00:20:14,000 --> 00:20:17,950
 >> Nicholas Christakis is a physician and a sociologist. 

296
00:20:17,950 --> 00:20:21,390
He became interested in the Framingham study not because he was interested in 

297
00:20:21,390 --> 00:20:25,680
heart disease, but because he was interested in social connections. 

298
00:20:25,680 --> 00:20:29,920
These individuals had had their social network ties repeatedly assessed over

299
00:20:29,920 --> 00:20:33,600
this 32-year period and, as it turns out, had had various aspects of 

300
00:20:33,600 --> 00:20:37,390
their emotional lives assessed as well. So what we could tell is we could tell 

301
00:20:37,390 --> 00:20:42,100
who these people were connected to every 4 years or so for 30 years, and 

302
00:20:42,100 --> 00:20:45,782
we could tell, for example, whether they were happy, or depressed, or lonely,

303
00:20:45,782 --> 00:20:50,000
and in some cases anxious, repeatedly across time. 

304
00:20:50,000 --> 00:20:54,480
>> Using the Framingham data, Christakis and his colleagues mapped out a web 

305
00:20:54,480 --> 00:20:58,910
of more than 50,000 connections between friends, family, and coworkers who 

306
00:20:58,910 --> 00:21:03,660
participated in the study. Then they began to calculate how changes 

307
00:21:03,660 --> 00:21:07,810
in the happiness of any single person affected the happiness of others 

308
00:21:07,810 --> 00:21:12,260
in this vast web of relationships. The results were astounding. 

309
00:21:12,260 --> 00:21:15,800
>> What we found is that if your friend becomes happy, 

310
00:21:15,800 --> 00:21:20,510
it increases the probability that you will become happy by about 15%. 

311
00:21:20,510 --> 00:21:22,980
And if your friend's friend becomes happy -- someone you don't even know, 

312
00:21:22,980 --> 00:21:27,420
perhaps -- it increases the probability that you'll become happy by about 10%. 

313
00:21:27,420 --> 00:21:29,815
It's only when we get to four degrees of separation -- 

314
00:21:29,815 --> 00:21:35,230
only when we're speaking of your friend's friend's friend's friend -- that 

315
00:21:35,230 --> 00:21:40,500 
that person's emotional state is no longer associated with your emotional state. 

316
00:21:40,500 --> 00:21:43,600
>> Everyone knows that people can make each other happy,

317
00:21:43,600 --> 00:21:46,330
but before this study, no one had any idea 

318
00:21:46,330 --> 00:21:49,700
just how powerful this effect could be. 

319
00:21:49,700 --> 00:21:53,580
Christakis's work shows that happiness spreads through social networks 

320
00:21:53,580 --> 00:21:59,455
like a virus -- that happiness is, in a very real sense, contagious. 

321
00:21:59,455 --> 00:22:02,670
There's no doubt about it: We're all connected, 

322
00:22:02,670 --> 00:22:06,000
and those connections exert a powerful influence on our happiness. 

323
00:22:06,000 --> 00:22:10,400
We start out in highchairs, we end up in wheelchairs, 

324
00:22:10,400 --> 00:22:14,320
and somewhere between the two, we all have to figure out where 

325
00:22:14,320 --> 00:22:18,830
our happiness lies and then start marching in that direction. 

326
00:22:18,830 --> 00:22:22,000
So, who brought the map? 

327
00:22:22,000 --> 00:22:27,000
Well, everyone did. Grandmothers and uncles, priests and rabbis, 

328
00:22:27,000 --> 00:22:29,980
taxi drivers, bartenders, motivational speakers --

329
00:22:29,980 --> 00:22:32,740
they all have something to say about happiness. 

330
00:22:32,740 --> 00:22:38,380
But they don't all say the same thing. And that's where science comes in. 

331
00:22:38,380 --> 00:22:42,610
By using scientific methods to identify the causes of human happiness, 

332
00:22:42,610 --> 00:22:46,831
we're learning that the secret of happiness was never very secret. 

333
00:22:46,831 --> 00:22:51,950
We're connected to each other. We belong to each other. 

334
00:22:51,950 --> 00:22:56,200
We're made for each other. Life is a journey through time, 

335
00:22:56,200 --> 00:23:03,000
and happiness is what happens when we make that journey together.
 

