


1
00:00:06,950 --> 00:00:11,500
Previously, I cited research from psychology, political science, and history 

2
00:00:11,500 --> 00:00:16,190
suggesting that when it comes to reducing terrorism, war is not the answer.

3
00:00:16,190 --> 00:00:20,000
And I cited statistics showing a rise in global terrorism 

4
00:00:20,000 --> 00:00:23,400
following the wars in Iraq and Afghanistan.

5
00:00:23,400 --> 00:00:27,400
In fact, since the war on terror began roughly a dozen years ago,

6
00:00:27,400 --> 00:00:30,800
the level of terrorism has never been higher.

7
00:00:30,800 --> 00:00:34,300
So in the lecture on terrorism, I suggested a number of peace and 

8
00:00:34,300 --> 00:00:37,800
security measures that could be taken instead of going to war, 

9
00:00:37,800 --> 00:00:40,600
the last of which was to take the third side --

10
00:00:40,600 --> 00:00:43,760
the topic that I'd like to cover in this video.

11
00:00:43,760 --> 00:00:46,900
I'm very pleased to say that a world renowned expert 

12
00:00:46,900 --> 00:00:50,870
on negotiation, Dr. William Ury, has generously agreed

13
00:00:50,870 --> 00:00:54,300
to let me use an 8-minute excerpt of a TED talk he gave 

14
00:00:54,300 --> 00:00:58,540
about taking the third side. Dr. Ury cofounded

15
00:00:58,540 --> 00:01:01,870
Harvard University's program on negotiation,

19
00:01:01,870 --> 00:01:04,900
and he served as a negotiation adviser and mediator 

17
00:01:04,900 --> 00:01:08,100
in some of the world's most difficult conflicts,

18
00:01:08,100 --> 00:01:11,800
including ethnic wars in the Middle East, the Balkans, and 

19
00:01:11,800 --> 00:01:16,200
and he served as a negotiation adviser and mediator

20
00:01:16,200 --> 00:01:19,700
United States and Soviet Union create nuclear crisis centers

21
00:01:19,700 --> 00:01:22,700
designed to avert an accidental nuclear war,

22
00:01:22,700 --> 00:01:26,430
and in that capacity, he served as a consultant to

23
00:01:26,430 --> 00:01:29,600
the Crisis Management Center at the White House.

24
00:01:30,100 --> 00:01:34,700
It gives me great pleasure to introduce Dr. William Ury.

25
00:01:36,530 --> 00:01:39,560
>> Well, the subject of difficult negotiation reminds me

26
00:01:39,560 --> 00:01:42,710
of one of my favorite stories from the Middle East,

27
00:01:42,710 --> 00:01:46,520
of a man who left to his three sons 17 camels.

28
00:01:46,520 --> 00:01:49,540
And to the first son he left half the camels, to the second son 

29
00:01:49,540 --> 00:01:52,100
he left a third of the camels, and to the youngest son he left 

30
00:01:52,100 --> 00:01:55,115
a ninth of the camels. Well, three sons got into a negotiation.

31
00:01:55,115 --> 00:01:59,100
Seventeen doesn't divide by 2. It doesn't divide by 3. 

32
00:01:59,100 --> 00:02:03,190
It doesn't divide by 9. Probably tempers started to get strained.

33
00:02:03,190 --> 00:02:08,000
Finally, in desperation they went and they consulted a wise old woman. 

34
00:02:08,000 --> 00:02:10,460
The wise old woman thought about their problem for a long time,

35
00:02:10,460 --> 00:02:13,380
and finally she came back and said, "Well, I don't know if I can help you,

36
00:02:13,380 --> 00:02:18,570
but at least if you want, you can have my camel." So then they had 18 camels.

37
00:02:18,570 --> 00:02:21,320
The first son took his half. Half of 18 is 9.

38
00:02:21,320 --> 00:02:24,190
The second son took his third. A third of 18 is 6.

39
00:02:24,190 --> 00:02:27,500
The youngest son took his ninth. A ninth of 18 is 2.

40
00:02:27,500 --> 00:02:34,700
You get 17. They had 1 camel left over. They gave it back to the wise old woman.

41
00:02:35,680 --> 00:02:38,250
Now, if you think about that story for a moment, I think it 

42
00:02:38,250 --> 00:02:42,490
resembles a lot of the difficult negotiations we get involved in.

43
00:02:42,490 --> 00:02:45,200
They start off like 17 camels -- no way to resolve it. 

44
00:02:45,200 --> 00:02:48,700
Somehow, what we need to do is step back from those situations 

45
00:02:48,700 --> 00:02:52,200
like that wise old woman, look at the situation through fresh eyes,

46
00:02:52,200 --> 00:02:57,400
and come up with an 18th camel. Now, finding that 18th camel 

47
00:02:57,400 --> 00:03:01,150
in the world's conflict has been my life passion. I basically

48
00:03:01,150 --> 00:03:03,620
see humanity a bit like those three brothers.

49
00:03:03,620 --> 00:03:06,000
We're all one family.

50
00:03:06,000 --> 00:03:07,820
We know that scientifically.

51
00:03:07,820 --> 00:03:10,600
Thanks to the communications revolution, all the tribes 

52
00:03:10,600 --> 00:03:15,900
on the planet, all 15,000 tribes, are in touch with each other. 

53
00:03:15,900 --> 00:03:19,600
And it's a big family reunion, and yet, like many family reunions, 

54
00:03:19,600 --> 00:03:24,330
it's not all peace and light. There's a lot of conflict.

55
00:03:24,330 --> 00:03:27,510
And the question is how do we deal with our differences?

56
00:03:27,510 --> 00:03:30,400
How do we deal with our deepest differences given the 

57
00:03:30,400 --> 00:03:34,600
human propensity for conflict and the human genius at devising 

58
00:03:34,600 --> 00:03:38,850
weapons of enormous destruction? That's the question.

59
00:03:38,850 --> 00:03:43,750
As I've spent the last better part of three decades, almost four,

60
00:03:43,750 --> 00:03:49,200
traveling the world, trying to work, getting involved in conflicts 

61
00:03:49,200 --> 00:03:53,000
ranging from Yugoslavia to the Middle East to Chechnya 

62
00:03:53,000 --> 00:03:55,400
to Venezuela -- some of the most difficult conflicts on 

63
00:03:55,400 --> 00:03:59,400
the face of the planet -- I've been asking myself that question.

64
00:03:59,400 --> 00:04:02,850
And I think I've found in some ways what is the secret to peace.

65
00:04:02,850 --> 00:04:05,670
It's actually surprisingly simple.

66
00:04:05,670 --> 00:04:10,100
It's not easy, but it's simple. It's not even new. 

67
00:04:10,100 --> 00:04:13,500
It's maybe our, one of our most ancient human heritages.

68
00:04:13,500 --> 00:04:19,400
The secret to peace is us. It's us who act as a surrounding community

69
00:04:19,400 --> 00:04:23,500
around any conflict, who can play a constructive role.

70
00:04:23,500 --> 00:04:28,400
Let me give you just a story, an example. About 20 years ago, 

71
00:04:28,400 --> 00:04:32,250
I was in South Africa working with the parties in that conflict, and

72
00:04:32,250 --> 00:04:35,800
I had an extra month so I spent some time living with several groups 

73
00:04:35,800 --> 00:04:40,000
of San Bushmen. I was curious about them, about the way in which they 

74
00:04:40,000 --> 00:04:44,500
resolve conflict, because after all, they're, within living memory, 

75
00:04:44,500 --> 00:04:46,800
they were hunters and gatherers living pretty much like 

76
00:04:46,800 --> 00:04:50,920
our ancestors lived for maybe 99% of the human story.

77
00:04:50,920 --> 00:04:54,000
And all the men have these poison arrows that they use for hunting -- 

78
00:04:54,000 --> 00:04:58,000
absolutely fatal. So how do they deal with their differences?

79
00:04:58,000 --> 00:05:02,800
Well, what I learned is whenever tempers rise in those communities, 

80
00:05:02,800 --> 00:05:07,240
someone goes and hides the poison arrows out in the bush, and then

81
00:05:07,240 --> 00:05:12,150
everyone sits around in a circle like this, and they sit and they talk 

82
00:05:12,150 --> 00:05:15,410
and they talk. It may take two days, three days, four days, but they 

83
00:05:15,410 --> 00:05:20,400
don't rest until they find a resolution, or better yet, a reconciliation.

84
00:05:20,400 --> 00:05:23,800
And if tempers are still too high, then they send someone off 

85
00:05:23,800 --> 00:05:26,700
to visit some relatives as a cooling off period. 

86
00:05:26,700 --> 00:05:31,200
Well, that system is, I think, probably the system that kept us alive to

87
00:05:31,200 --> 00:05:37,780
this point given our human tendencies. That system I call "the third side" 

88
00:05:37,780 --> 00:05:40,400
because, if you think about it, normally when we think of conflict, 

89
00:05:40,400 --> 00:05:43,390
when we describe it, there's always two sides.

90
00:05:43,390 --> 00:05:46,100
You know, it's Arabs versus Israelis, labor versus management, 

91
00:05:46,100 --> 00:05:49,200
husband versus wife, Republicans versus Democrats, 

92
00:05:49,200 --> 00:05:53,590
but what we don't often see is that there's always a third side.

93
00:05:53,590 --> 00:05:57,209
And the third side of the conflict is us. It's the surrounding community.

94
00:05:57,209 --> 00:06:00,200
It's the friends, the allies, the family members, the neighbors, 

95
00:06:00,200 --> 00:06:03,700
and we can play an incredibly constructive role.

96
00:06:03,700 --> 00:06:07,820
Perhaps the most fundamental way in which the third side can

97
00:06:07,820 --> 00:06:12,090
help is to remind the parties of what's really at stake.

98
00:06:12,090 --> 00:06:14,300
You know, for the sake of the kids, for the sake of the family, 

99
00:06:14,300 --> 00:06:17,400
for the sake of the community, for the sake of the future,

100
00:06:17,400 --> 00:06:20,530
let's stop fighting for a moment and start talking.

101
00:06:20,530 --> 00:06:23,700
Because the thing is, when we're involved in conflict, 

102
00:06:23,700 --> 00:06:26,230
it's very easy to lose perspective.

103
00:06:26,230 --> 00:06:28,210
It's very easy to react.

104
00:06:28,210 --> 00:06:31,190
Human beings -- we're reaction machines.

105
00:06:31,190 --> 00:06:33,200
And as the saying goes, when angry 

106
00:06:33,200 --> 00:06:37,600
you will make the best speech you will ever regret.

107
00:06:38,220 --> 00:06:41,600
And so, the third side reminds us that, the third side 

108
00:06:41,600 --> 00:06:45,200
helps us go to the balcony, which is a metaphor for a place of

109
00:06:45,200 --> 00:06:48,100
perspective where we can keep our eyes on the prize.

110
00:06:48,100 --> 00:06:52,090
Let me tell you a little story from my own negotiating experience.

111
00:06:52,090 --> 00:06:56,300
Some years ago, I was involved as a facilitator in some very tough 

112
00:06:56,300 --> 00:07:00,500
talks between the leaders of Russia and the leaders of Chechnya.

113
00:07:01,100 --> 00:07:05,800
There was a war going on, as you know, and we met in The Hague, 

114
00:07:05,800 --> 00:07:09,800
in the Peace Palace -- in the same room where the Yugoslav war crimes 

115
00:07:09,800 --> 00:07:14,500
tribunal was taking place. And the talks got off to a rather rocky start 

116
00:07:14,500 --> 00:07:18,060
when the Vice President of Chechnya began by pointing at the Russians

117
00:07:18,060 --> 00:07:20,130
and said, "You should stay right here in your seats 

118
00:07:20,130 --> 00:07:23,480
because you're going to be on trial for war crimes." 

119
00:07:23,480 --> 00:07:24,300
And then he went on.

120
00:07:24,300 --> 00:07:26,630
And then he turned to me and said: "You're an American.

121
00:07:26,630 --> 00:07:29,870
Look at what you Americans are doing in Puerto Rico." 

122
00:07:29,870 --> 00:07:31,500
And my mind started racing.

123
00:07:31,500 --> 00:07:33,410
Well, Puerto Ricoâ¦ what do I know about Puerto Rico?

124
00:07:33,410 --> 00:07:35,200
I started reacting.

125
00:07:35,200 --> 00:07:39,060
But then I tried to remember to go to the balcony, and then, 

126
00:07:39,060 --> 00:07:41,800
when he paused, and everyone looked at me for a response, 

127
00:07:41,800 --> 00:07:45,200
from the balcony perspective I was able to thank him for his remarks 

128
00:07:45,200 --> 00:07:49,700
and say: "I appreciate your criticism of my country and I take it as a sign 

129
00:07:49,700 --> 00:07:53,440
that we're among friends and can speak candidly to one another.

130
00:07:53,440 --> 00:07:56,360
And what we're here to do is not to talk about Puerto Rico or the past.

131
00:07:56,360 --> 00:07:59,700
We're here to do, just to see if we can figure out a way to stop 

132
00:07:59,700 --> 00:08:02,520
the suffering and the bloodshed in Chechnya." 

133
00:08:02,520 --> 00:08:04,505
The conversation got back on track.

134
00:08:04,505 --> 00:08:09,450
That's the role of the third side; it's to help the parties go to the balcony.

135
00:08:09,450 --> 00:08:13,400
In the last 35 years, as I've worked in some of the most dangerous, 

136
00:08:13,400 --> 00:08:16,200
difficult, and intractable conflicts around the planet. 

137
00:08:16,200 --> 00:08:22,400
I have yet to see one conflict that I felt could not be transformed.

138
00:08:22,400 --> 00:08:26,900
It's not easy, of course, but it's possible.

139
00:08:26,900 --> 00:08:28,860
It was done in South Africa,

140
00:08:28,860 --> 00:08:32,400
it was done in Northern Ireland. It could be done anywhere.

141
00:08:32,400 --> 00:08:37,900
It simply depends on us. It depends on us taking the third side.

142
00:08:37,900 --> 00:08:42,020
So, let me invite you to consider taking the third side.

143
00:08:42,020 --> 00:08:45,970
Even as a very small step: We're about to take a break in a moment...

144
00:08:45,970 --> 00:08:50,400
just go up to someone who's from a different culture, a different country, 

145
00:08:50,400 --> 00:08:53,820
a different ethnicity, some difference, and engage them 

146
00:08:53,820 --> 00:08:57,710
in a conversation. Listen to them. That's a third side act.

147
00:08:57,710 --> 00:09:04,400
The secret to peace is the third side. The third side is us. 

148
00:09:04,400 --> 00:09:10,200
Each of us, with a single step, can take the world, can bring the world, 

149
00:09:10,200 --> 00:09:15,275
a step closer to peace. There's an old African proverb that goes: 

150
00:09:15,275 --> 00:09:20,700
"When spider webs unite, they can halt even a lion." 

151
00:09:20,700 --> 00:09:24,700
If we're able to unite our third side webs of peace,

152
00:09:24,700 --> 00:09:29,600
we can even halt the lion of war. Thank you very much.

153
00:09:31,800 --> 00:09:36,290
>> A very wise perspective. Dr. Ury literally wrote the book on 

154
00:09:36,290 --> 00:09:39,480
"The Third Side." And if you're interested in learning more or 

155
00:09:39,480 --> 00:09:42,000
you'd like some tips on negotiation, a good place to start is 

156
00:09:42,000 --> 00:09:46,500
with one of his books. The Third Side goes into more detail about

157
00:09:46,500 --> 00:09:50,830
peace-building measures and conflict resolution, and "Getting to Yes"

158
00:09:50,830 --> 00:09:53,950
is a bestseller that's sold over 8 million copies

159
00:09:53,950 --> 00:09:57,600
and has been translated into more than 30 languages.

160
00:09:57,600 --> 00:10:01,668
A hallmark of the approach advocated by Dr. Ury and his coauthor,

161
00:10:01,668 --> 00:10:06,730
Roger Fisher, is what they call "principled negotiation."

162
00:10:06,730 --> 00:10:10,310
Principled negotiation boils down to four key points.

163
00:10:10,310 --> 00:10:13,830
First, separate the problem from the people.

164
00:10:13,830 --> 00:10:16,100
Don't get into personal attacks.

165
00:10:16,100 --> 00:10:21,780
Second, focus on underlying interests, rather than expressed positions.

166
00:10:21,780 --> 00:10:26,260
Positions are fine, but they're not the whole story -- you have to dig deeper.

167
00:10:26,260 --> 00:10:30,270
Third, generate a variety of options before deciding what to do.

168
00:10:30,270 --> 00:10:33,360
Don't narrow your options prematurely.

169
00:10:33,360 --> 00:10:38,000
And fourth, insist that the result be based on some objective standard: 

170
00:10:38,000 --> 00:10:41,400
market value, scientific judgment, what have you.

171
00:10:41,400 --> 00:10:43,700
This last point is important because of the distortion 

172
00:10:43,700 --> 00:10:47,080
that often arises between parties in a negotiation.

173
00:10:48,290 --> 00:10:51,330
In fact, Fisher and Ury warn that whatever you say,

174
00:10:51,330 --> 00:10:53,700
you should expect that the other side will almost always 

175
00:10:53,700 --> 00:10:57,460
hear something different, and this applies to negotiations 

176
00:10:57,460 --> 00:11:00,080
not just between countries or corporations,

177
00:11:00,080 --> 00:11:04,030
but between family members, friends, tenants and landlords.

178
00:11:04,030 --> 00:11:07,190
This approach can be applied at any level.

179
00:11:07,190 --> 00:11:10,000
Let's pause for a pop-up question about a key element of 

180
00:11:10,000 --> 00:11:13,000
principled negotiation, just to make sure that it's clear.

181
00:11:16,200 --> 00:11:20,000
Returning to Dr. Ury's talk about the third side, you might be thinking, 

182
00:11:20,000 --> 00:11:23,700
"Well, it's a lovely idea, that individuals and communities can 

183
00:11:23,700 --> 00:11:28,690
bring about peace, but isn't it a little bit idealistic -- a little bit naive?" 

184
00:11:28,690 --> 00:11:31,940
Individual citizens aren't going to be any match for police

185
00:11:31,940 --> 00:11:35,980
and soldiers with guns, or governments that rule by force, right?

186
00:11:37,060 --> 00:11:39,200
Well, you could have said the same thing about 

187
00:11:39,200 --> 00:11:44,228
apartheid in South Africa, but apartheid is gone.

188
00:11:44,228 --> 00:11:50,590
You could've said that about colonial rule in India, but colonial rule is gone.

189
00:11:50,590 --> 00:11:54,200
In fact, you could've said that about dictatorships and colonial rule 

190
00:11:54,200 --> 00:11:58,000
in many countries, but over the past century democracy has been on 

191
00:11:58,000 --> 00:12:03,520
the rise, often as a result of ordinary citizens making their voices heard, 

192
00:12:03,520 --> 00:12:07,000
and in some cases, making their voices heard with the help of other 

193
00:12:07,000 --> 00:12:11,880
parties taking the third side. To me, one of the best examples of how

194
00:12:11,880 --> 00:12:15,500
individual actions can have amazing effects is the case of 

195
00:12:15,500 --> 00:12:20,600
Wangari Maathai, a Kenyan who in 1971 became the first 

196
00:12:20,600 --> 00:12:25,200
East African woman to receive a PhD, and who went on to found the 

197
00:12:25,200 --> 00:12:29,710
Greenbelt Movement in 1977. The background is that

198
00:12:29,710 --> 00:12:33,900
beginning in the 1960s, Kenya suffered tremendous deforestation 

199
00:12:33,900 --> 00:12:37,900
as a result of government corruption selling off public lands,

200
00:12:37,900 --> 00:12:41,310
growing cash crops like tea, and other factors.

201
00:12:41,310 --> 00:12:45,650
And this deforestation was increasing poverty, hunger, and conflict

202
00:12:45,650 --> 00:12:49,570
because the water was drying up, people couldn't get fuel for cooking,

203
00:12:49,570 --> 00:12:52,000
habitat was being destroyed, and so on.

204
00:12:52,400 --> 00:12:57,300
Now, if you look at this landscape and imagine the task of changing it, 

205
00:12:57,300 --> 00:13:01,200
you might feel overwhelmed. How could you counteract the deforestation

206
00:13:01,200 --> 00:13:05,800
of a country as large as Kenya? Wangari Maathai's simple 

207
00:13:05,800 --> 00:13:09,800
but powerful solution was this: You plant trees.

208
00:13:09,800 --> 00:13:11,400
Lots of trees.

209
00:13:11,400 --> 00:13:14,700
So she spoke to other women and began a tree planting campaign 

210
00:13:14,700 --> 00:13:18,200
that the Green Belt Movement continues to this day.

211
00:13:18,200 --> 00:13:22,400
All told, over 50 million trees have been planted,

212
00:13:22,400 --> 00:13:26,600
and over 30,000 women have been trained in forestry, beekeeping, 

213
00:13:26,600 --> 00:13:29,890
and other occupations that are providing them with income,

214
00:13:29,890 --> 00:13:33,950
preserving the environment, and fighting climate change.

215
00:13:33,950 --> 00:13:37,100
In 2004, this work was recognized when Wangari Maathai 

216
00:13:37,100 --> 00:13:42,200
became the first African woman to win the Nobel Peace Prize.

217
00:13:42,200 --> 00:13:46,600
In her memoir "Unbowed" (which, by the way, is a fascinating book), 

218
00:13:46,600 --> 00:13:50,022
she summed up her approach to mass mobilization with these

219
00:13:50,022 --> 00:13:54,900
nine words: "You don't need a diploma to plant a tree."

220
00:13:56,100 --> 00:13:59,130
And of course, we don't need deforestation as a motivation to

221
00:13:59,130 --> 00:14:03,100
protect the environment because all of us are facing climate change, 

222
00:14:03,100 --> 00:14:06,240
which has the structure of a social dilemma in which the pursuit

223
00:14:06,240 --> 00:14:11,700
of individual self-interest can lead to catastrophe at the group level.

224
00:14:11,700 --> 00:14:14,400
This week's assigned reading covers two of the most famous

225
00:14:14,400 --> 00:14:19,640
social dilemmas: the prisoner's dilemma and tragedy of the commons.

226
00:14:19,640 --> 00:14:24,700
When you read this material, think about human behavior and climate change.

227
00:14:24,700 --> 00:14:27,300
This week we also have a terrific bonus reading on 

228
00:14:27,300 --> 00:14:31,100
the "Psychology of Climate Change Communication,"

229
00:14:31,100 --> 00:14:33,000
a reading that was generously contributed by 

230
00:14:33,000 --> 00:14:37,800
the Columbia University Center for Research on Environmental Decisions.

231
00:14:38,800 --> 00:14:42,700
Now, before we end, I thought you might like to see a 2-minute video

232
00:14:42,700 --> 00:14:45,950
of Wangari Maathai telling a very interesting story

233
00:14:45,950 --> 00:14:50,080
about a hummingbird confronted by a forest fire.

234
00:14:50,080 --> 00:14:54,800
You might think of the forest fire as a metaphor for war, for climate change, 

235
00:14:54,800 --> 00:14:58,000
or really for any problem that seems overwhelming.

236
00:15:01,000 --> 00:15:06,130
We are constantly being bombarded by problems that we face, 

237
00:15:06,130 --> 00:15:10,000
and sometimes we can get completely overwhelmed.

238
00:15:10,000 --> 00:15:16,600
The story of the hummingbird is about this huge forest being consumed by a fire.

239
00:15:17,360 --> 00:15:20,700
All the animals in the forest come out, and 

240
00:15:20,700 --> 00:15:26,300
they are transfixed as they watch the forest burning.

241
00:15:26,300 --> 00:15:32,800
And they feel very overwhelmed, very powerless, except this little

242
00:15:32,800 --> 00:15:37,030
hummingbird that says, "I'm going to do something about the fire."

243
00:15:37,030 --> 00:15:39,520
So it flies to the nearest stream,

244
00:15:39,520 --> 00:15:43,900
takes a drop of water, and puts it on the fire, and goes up and down, 

245
00:15:43,900 --> 00:15:48,210
up and down, up and down, as fast as it can.

246
00:15:48,210 --> 00:15:51,000
In the meantime, all the other animals, much bigger animals, 

247
00:15:51,000 --> 00:15:55,300
like the elephant with a big trunk, could bring much more water,

248
00:15:55,300 --> 00:16:00,800
they are standing there helpless. And they are saying to the hummingbird: 

249
00:16:00,800 --> 00:16:05,490
"What do you think you can do? You're too little. This fire is too big.

250
00:16:05,490 --> 00:16:10,340
Your wings are too little and your beak's so small. You can only bring a

251
00:16:10,340 --> 00:16:15,375
small drop of water at a time." But as they continue to discourage it, 

252
00:16:15,375 --> 00:16:21,100
it turns to them without wasting any time and tells them, 

253
00:16:21,100 --> 00:16:25,500
"I am doing the best I can."

254
00:16:25,500 --> 00:16:30,000
And that, to me, is what all of us should do. 

255
00:16:30,000 --> 00:16:32,875
We should always feel like a hummingbird.

256
00:16:32,900 --> 00:16:38,290
I may feel insignificant, but I certainly don't want to be like the animals

257
00:16:38,290 --> 00:16:42,600
watching as the planet goes down the drain. I will be a hummingbird.

258
00:16:42,600 --> 00:16:46,300
I will do the best I can.

259
00:16:48,100 --> 00:16:52,850
>> In the lecture on aggression, I mentioned the power of non-participation.

260
00:16:52,850 --> 00:16:57,300
For example, we don't have to listen to sexist lyrics or watch violent movies.

261
00:16:57,300 --> 00:17:00,740
But when it comes to working for peace and social justice,

262
00:17:00,740 --> 00:17:04,500
there's also the power of participation, of not falling

263
00:17:04,500 --> 00:17:07,900
prey to the bystander effect and diffusion of responsibility.

264
00:17:07,900 --> 00:17:13,600
And here is where the message of William Ury and Wangari Maathai come together. 

265
00:17:13,600 --> 00:17:17,470
Whether you're taking the third side when two parties are in conflict, 

266
00:17:17,470 --> 00:17:19,890
or you're taking steps to protect the environment,

267
00:17:19,890 --> 00:17:23,900
what's most important is to be active rather than passive.

268
00:17:24,600 --> 00:17:28,700
On that note, I'll end with a quote attributed to Mahatma Gandhi, 

269
00:17:28,700 --> 00:17:34,300
who was a master at creating social change in the face of enormous obstacles:

270
00:17:34,300 --> 00:17:41,900
"Whatever you do will be insignificant, but it is very important that you do it."
