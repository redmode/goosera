

1
00:00:33,608 --> 00:00:40,143
It was a typical July afternoon in the little town of Coleman, Texas. 

2
00:00:40,143 --> 00:00:45,000
A hot wind from the Panhandle was blowing fine-grained Texas topsoil right 

3
00:00:45,000 --> 00:00:51,173
through my in-law's house while my wife and I, along with her parents, indulged 

4
00:00:51,173 --> 00:00:56,540
in what, for Coleman, was a rip-roaring afternoon of entertainment. 

5
00:00:56,540 --> 00:00:59,285
>> Domino. >> Just my luck, 

6
00:00:59,285 --> 00:01:02,000
I get a son-inlaw that can beat me at dominos. 

7
00:01:04,840 --> 00:01:09,000
>> Since those days back in Coleman, I've gone on to study people 

8
00:01:09,000 --> 00:01:12,705
and how they interact in all kinds of organizations. 

9
00:01:15,000 --> 00:01:20,090
But the more I learn about what is now termed organizational dynamics, 

10
00:01:20,090 --> 00:01:24,398
the more I'm reminded of one particular hot summer afternoon. 

11
00:01:27,000 --> 00:01:32,000
>> Five. >> Fifteen. 

12
00:01:33,000 --> 00:01:35,130
It's getting on to 4 o'clock. 

13
00:01:35,130 --> 00:01:37,400
What do you want to do about supper? 

14
00:01:41,000 --> 00:01:45,500
>> Well, what do you all say we get in the car and go to Abilene, 

15
00:01:45,500 --> 00:01:47,000
have supper at the cafe?

16
00:01:53,000 --> 00:01:54,543
>> Well, that sounds like a good idea. 

17
00:01:54,543 --> 00:02:00,620
I'd like to go. How about you, Jerry? 

18
00:02:00,620 --> 00:02:06,000
>> How far is it to Abilene? >> 53 miles. 

19
00:02:06,000 --> 00:02:08,125
>> You got the air conditioning fixed in that Buick yet? 

20
00:02:08,500 --> 00:02:16,000
>> Nope. >> Yeah, well, sounds like a good idea to me, 

21
00:02:16,000 --> 00:02:20,000
but I don't want to go unless your momma wants to go. 

22
00:02:20,000 --> 00:02:24,530
>> Well, of course I want to go! 

23
00:02:24,530 --> 00:02:26,000
You don't think I want to stay here and 

24
00:02:26,000 --> 00:02:30,000
eat leftovers out of the icebox, do you? 

25
00:02:41,500 --> 00:02:47,000
>> And so, we all piled into the furnace and headed off for Abilene. 

26
00:03:14,000 --> 00:03:18,000
After a supper that could've provided a first-rate testimonial 

27
00:03:18,000 --> 00:03:23,030
for antacid, some four hours and 106 miles later, 

28
00:03:23,030 --> 00:03:24,260
we returned to Coleman. 

29
00:03:28,550 --> 00:03:33,900
For a long while, there was absolute, teetotal silence. 

30
00:03:39,000 --> 00:03:41,174
>> It was a great trip, wasn't it? 

31
00:03:41,174 --> 00:03:46,048
>> And then, the floodgates were opened. >> Well, to tell you the truth, 

32
00:03:46,048 --> 00:03:51,500
I'd rather have stayed right here, if only y'all hadn't pressured me to go along. 

33
00:03:51,500 --> 00:03:52,964
>> What do you mean "y'all"? 

34
00:03:52,964 --> 00:03:57,760
I didn't want to go. I only went along to satisfy the rest of you. 

35
00:03:57,760 --> 00:04:01,149
You're the culprits. >> Don't call me a culprit. 

36
00:04:01,149 --> 00:04:03,610
You and Daddy and Momma are the ones who wanted to go. 

37
00:04:03,610 --> 00:04:09,510
I just went along to be sociable. >> Hey, I didn't want to go to Abilene 

38
00:04:09,510 --> 00:04:12,577
to begin with. I was just making conversation. 

39
00:04:12,577 --> 00:04:17,000
How'd I know that y'all would take me up on it and ruin my whole day? 

40
00:04:17,000 --> 00:04:22,030
>> I can't believe we went to Abilene when nobody wanted to go. 

41
00:04:22,030 --> 00:04:23,400
>> Now, if you're wondering if our family 

42
00:04:23,400 --> 00:04:27,530
ever resolved our conflict, the answer is no, 

43
00:04:27,530 --> 00:04:32,185
because strange as it may seem, we never really were in conflict. 

44
00:04:32,185 --> 00:04:35,000
>> You didn't think I wanted to go, did you? 

45
00:04:35,000 --> 00:04:39,480
>> But, why did you suggest that we go to Abilene in the first place? 

46
00:04:40,570 --> 00:04:44,460
>> I was just testing the waters. Y'all took me serious -- 

47
00:04:44,460 --> 00:04:47,500
ruined a perfectly good Sunday afternoon. 

48
00:04:50,000 --> 00:04:55,000
>> I later came to call it the "Abilene Paradox," when groups of people 

49
00:04:55,000 --> 00:05:00,000
take actions in contradiction to what they, as individuals, really want 

50
00:05:00,000 --> 00:05:06,000
and end up defeating the very purposes they set out to achieve. 

51
00:05:06,000 --> 00:05:11,000
In fact, the Abilene Paradox says that the inability to manage agreement, 

52
00:05:11,000 --> 00:05:14,260
rather than conflict, is one of the most urgent issues 

53
00:05:14,260 --> 00:05:18,580
facing our organizations, then and now. 

54
00:05:18,580 --> 00:05:22,000
>> I'm sorry to have kept you waiting. >> Some years later, 

55
00:05:22,000 --> 00:05:26,000
I was reminded of the Abilene paradox when I got hired as  

56
00:05:26,000 --> 00:05:30,000
a part-time consultant for an intriguing assignment. 

57
00:05:30,000 --> 00:05:36,770
>> Production is on a downturn, and our profit picture's in um, transition. 

58
00:05:36,770 --> 00:05:38,935
None of our staff know what to do about it -- 

59
00:05:38,935 --> 00:05:46,000
that's why we asked you here. You have any questions? 

60
00:05:46,000 --> 00:05:53,740
>> Yeah, what's this "Project X"? >> An albatross. 

61
00:05:53,740 --> 00:05:58,840
The guys down in R&D think they can turn peanut oil into jet fuel. 

62
00:05:58,840 --> 00:06:02,780
Don't get me wrong -- it's a great idea on paper, 

63
00:06:02,780 --> 00:06:06,760
but between you and me, it'll never fly. >> Why not? 

64
00:06:06,760 --> 00:06:10,000
>> The technology just isn't there yet. >> Have you considered 

65
00:06:10,000 --> 00:06:12,808
some kind of formal reassessment of the viability of the project? 

66
00:06:12,808 --> 00:06:18,630
>> Oh, not me! Oh, the stockholders love it -- 

67
00:06:18,630 --> 00:06:23,280
big spread in the Wall Street Journal. Besides, I have a vice president 

68
00:06:23,280 --> 00:06:26,425
who's willing to stake her entire reputation on the success of this project. 

69
00:06:26,425 --> 00:06:29,778
>> Well, is she aware of your reservations? 

70
00:06:29,778 --> 00:06:32,970
>> Are you kidding? I've got an ulcer myself over this. 

71
00:06:32,970 --> 00:06:37,000
You think I want to give her one? >> So how do you handle this? 

72
00:06:37,000 --> 00:06:41,000
>> Every few days, I go over to her office and say things like, 


73
00:06:41,000 --> 00:06:47,600
"Hang in there! Victory or death! Stiff upper lip!" Stuff like that. 

74
00:06:47,600 --> 00:06:51,100
Isn't that what a president's supposed to do? 

75
00:06:53,000 --> 00:06:57,500
>> I decided to find out what the VP thought about Project X.

76
00:06:57,500 --> 00:07:01,000 
>> Peanut oil into jet fuel -- you got to be kidding. 

77
00:07:01,000 --> 00:07:03,400
No, Project X is a dead-end street. 

78
00:07:03,400 --> 00:07:06,425
>> I don't get it. If you know that, why not stop it? 

79
00:07:06,425 --> 00:07:08,330
>> You try to stop it when the President's in your office every few days, 

80
00:07:08,330 --> 00:07:12,500
telling you to "Hang in there! Victory or death! Stiff upper lip!" 

81
00:07:12,500 --> 00:07:14,718
>> So why don't you tell him you think it's a dead end? 

82
00:07:14,718 --> 00:07:19,000
>> Not me. That project is a sacred cow. 

83
00:07:19,380 --> 00:07:21,790
Anyway, who knows? Maybe those people down in R&D can pull 

84
00:07:21,790 --> 00:07:25,114
a rabbit out of the hat. Believe me, it wouldn't be the first time. 

85
00:07:25,114 --> 00:07:29,300
>> Like the president before her, the VP strongly advised me 

86
00:07:29,300 --> 00:07:34,000
to look elsewhere for the cause of ACME's problems, 

87
00:07:34,000 --> 00:07:38,500
but by the time I got around to interviewing the R&D director, 

88
00:07:38,500 --> 00:07:43,000
it was clear that there was just no ignoring Project X. 

89
00:07:43,000 --> 00:07:49,000
>> Did you ever try to turn peanut oil into jet fuel? 

90
00:07:49,000 --> 00:07:50,936
Craziest thing I ever heard. 

91
00:07:50,936 --> 00:07:54,090
>> But the VP said that your reports -- 

92
00:07:54,090 --> 00:07:57,180
>> Oh, I write those reports ambiguously enough so that the board 

93
00:07:57,180 --> 00:08:02,320
can interpret them in any way they please. In fact, I sort of slant them 

94
00:08:02,320 --> 00:08:07,200
to the positive side, given how committed the brass are to the project. 

95
00:08:07,200 --> 00:08:14,000
>> But, if the project is unworkable, why don't you tell them? 

96
00:08:14,000 --> 00:08:17,000
>> See the woman at that workstation? 

97
00:08:17,500 --> 00:08:19,700
>> It, it's empty. 

98
00:08:19,700 --> 00:08:23,239
>> Exactly. Rumor has it she's the last one 

99
00:08:23,239 --> 00:08:26,800
who criticized Project X. 

100
00:08:26,800 --> 00:08:30,000
Look, I'm middle aged. 

101
00:08:30,000 --> 00:08:35,000
I got two kids in college. I got alimony payments. 

102
00:08:35,000 --> 00:08:38,000
There's no job market for a guy with my skills. 

103
00:08:38,000 --> 00:08:39,800
>> I should've seen it then. 

104
00:08:39,800 --> 00:08:45,250
The culprit wasn't Project X. Everyone believed privately 

105
00:08:45,250 --> 00:08:48,500
that the project was doomed to failure, but they were simply 

106
00:08:48,500 --> 00:08:53,300
unable or unwilling to share that belief with each other. 

107
00:08:53,300 --> 00:08:57,170
They were on a trip to Abilene when nobody wanted to go. 

108
00:09:00,520 --> 00:09:05,000
A few years later, I discovered that the Abilene Paradox applies 

109
00:09:05,000 --> 00:09:12,000
not only to corporations but also to couples. >> Hello, Professor Harvey. 

110
00:09:14,740 --> 00:09:19,000
>> Why do you look so down, Sue? >> You'd be down, too, if you 

111
00:09:19,000 --> 00:09:24,000
were marrying George on Saturday. >> But George is a great guy. 

112
00:09:24,000 --> 00:09:30,500
>> Sure he is, but I don't love him. I don't even like him. 

113
00:09:30,500 --> 00:09:34,000
In fact, I can't even stand him! 

114
00:09:34,000 --> 00:09:37,000
>> Then why in the world are you getting married? 

115
00:09:37,000 --> 00:09:42,700
>> He gave me an engagement ring. >> If you don't love him, 

116
00:09:42,700 --> 00:09:45,728
don't you think you ought to give it back? >> I can't do that. 

117
00:09:45,728 --> 00:09:49,120
Momma loves him, and, and she has a heart condition, and 

118
00:09:49,120 --> 00:09:52,000
if I backed out now she'd probably have a heart attack. 

119
00:09:52,000 --> 00:09:57,000
Oh, and besides, there's a room full of presents, 

120
00:09:57,000 --> 00:10:01,000
and the bridesmaid, she has her dress. 

121
00:10:01,000 --> 00:10:07,500
>> But, how did all this happen? >> Well, you got me. 

122
00:10:07,500 --> 00:10:13,650
I gave Sue a ring. Now, I'm marrying a girl I don't even love. 

123
00:10:13,650 --> 00:10:14,970
>> Well, if you don't love her, don't you think

124
00:10:14,970 --> 00:10:18,400
you ought to ask for the ring back? >> No, I can't do that. 

125
00:10:18,400 --> 00:10:22,880
I'd be a laughing stock. Well, besides, her momma thinks 

126
00:10:22,880 --> 00:10:26,650
the world of me, and then, there's all the presents. 

127
00:10:26,650 --> 00:10:30,360
I ain't all that good looking. I may not get another chance if -- 

128
00:10:30,360 --> 00:10:35,000
>> The crazy thing was, Sue and George were in total agreement 

129
00:10:35,000 --> 00:10:40,000
about their impending marriage, but they still seemed determined 

130
00:10:40,000 --> 00:10:44,800
to spend a lifetime together on the road to Abilene. 

131
00:10:46,000 --> 00:10:48,760
Pretty absurd, isn't it? 

132
00:10:48,760 --> 00:10:52,000
People taking actions in direct contradiction to what they really 

133
00:10:52,000 --> 00:10:56,020
think is best, and as a result, compounding their problems 

134
00:10:56,020 --> 00:11:03,000
rather than solving them. The big question is, why? 

135
00:11:03,000 --> 00:11:08,930
To answer that, we're going to need to explore the psychological principles 

136
00:11:08,930 --> 00:11:13,500
from which the Abilene Paradox draws its enormous power. 

137
00:11:15,000 --> 00:11:21,000
>> To be, or not to be, that is the question: 

138
00:11:21,000 --> 00:11:24,300
Whether 'tis nobler in the mind to suffer the slings and arrows 

139
00:11:24,300 --> 00:11:29,000
of outrageous fortune, or by opposing end them. 

140
00:11:29,000 --> 00:11:33,000
>> First of all, there's action anxiety, which is epitomized by perhaps 

141
00:11:33,000 --> 00:11:38,000
the most famous dramatic character of all time: Shakespeare's Hamlet. 

142
00:11:38,000 --> 00:11:43,000
>> For in that sleep of death, what dreams may come... 

143
00:11:44,000 --> 00:11:47,322
Thus, conscience doth make cowards of us all. 

144
00:11:47,800 --> 00:11:52,400
>> To maintain my integrity or to compromise it, that is the question. 

145
00:11:52,400 --> 00:11:58,000
Whether 'tis nobler in the mind to risk managing a doomed research project... 

146
00:11:58,000 --> 00:12:01,000
>> When we are anxious about what action to choose, even though, 

147
00:12:01,000 --> 00:12:06,000
unlike Hamlet, we may have a perfectly sensible choice in mind, 

148
00:12:06,000 --> 00:12:09,000
we may simply refuse to act at all. 

149
00:12:09,000 --> 00:12:14,700
>> Thus, fear of unemployment doth make cowards of us all. 

150
00:12:14,700 --> 00:12:18,490
>> Well, answer this, Sue. What'll happen if you call it off? 

151
00:12:18,490 --> 00:12:23,830
>> Second, we often find ourselves conjuring up negative fantasies 

152
00:12:23,830 --> 00:12:28,000
of the disaster we're certain will occur if we actually do what we think 

153
00:12:28,000 --> 00:12:32,000
we should. >> You know what'll happen if I call off this wedding. 

154
00:12:32,000 --> 00:12:35,000
>> No, I don't. Tell me. 

155
00:12:42,000 --> 00:12:46,315
>> Momma? I don't know how to tell you this, 

156
00:12:46,315 --> 00:12:54,000
but George and I -- we're not gonna get married. 

157
00:12:56,000 --> 00:13:00,000
>> Such negative fantasies serve an important function. 

158
00:13:00,000 --> 00:13:03,000
They give us an iron-clad excuse for inaction 

159
00:13:03,000 --> 00:13:07,000
when what is really called for is action. 

160
00:13:07,000 --> 00:13:11,700
But can we ever really play it safe? The answer is no, we can't. 

161
00:13:11,700 --> 00:13:17,000
Mommas do sometimes suffer heart attacks, and R&D directors do 

162
00:13:17,000 --> 00:13:21,000
sometimes lose their jobs.  >> For your lack of support for Project X, 

163
00:13:21,000 --> 00:13:24,049
you're fired. Effective immediately. 

164
00:13:24,049 --> 00:13:26,130
Clean out your desk, and turn in your keys. 

165
00:13:28,000 --> 00:13:32,000
>> Excuse me, Sir? What did you say? 

166
00:13:33,000 --> 00:13:39,000
>> I said, "I'm glad we're all fired up about Project X." 

167
00:13:39,000 --> 00:13:43,000
>> Oh, right, right. Absolutely. 

168
00:13:43,000 --> 00:13:46,000
>> Real risk is a condition of human existence. 

169
00:13:46,000 --> 00:13:49,000
It's one of life's givens. 

170
00:13:48,710 --> 00:13:53,420
All of our actions have the potential of a negative outcome. 

171
00:13:53,420 --> 00:13:58,000
By attempting to avoid risk, all we're really doing is choosing to 

172
00:13:58,000 --> 00:14:05,020
take a trip to Abilene instead -- a choice that often has far greater risk. 

173
00:14:05,020 --> 00:14:09,000
>> Then, you agree we shouldn't divert any of the Project X funding 

174
00:14:09,000 --> 00:14:14,888
into other smaller projects? >> Well, no. 

175
00:14:14,888 --> 00:14:20,600
It would dissipate our focus, delaying a possible breakthrough. 

176
00:14:21,700 --> 00:14:30,600
>> Well, I feel that way too. I um, think that we should push on. 

177
00:14:32,000 --> 00:14:35,000
We're close now. 

178
00:14:43,700 --> 00:14:48,000
All those in favor of continuing Project X? 

179
00:14:48,000 --> 00:14:52,900
>> But, what is it we're really risking that makes us so fearful? 

180
00:14:53,500 --> 00:14:59,200
Embarrassment, being ostracized, being branded a non-team player. 

181
00:15:04,000 --> 00:15:07,000
In short, we fear separation. 

182
00:15:09,000 --> 00:15:14,300
But therein lies a paradox within a paradox, because the more unwilling 

183
00:15:14,300 --> 00:15:18,000
we are to risk being ostracized, the more likely we are to make 

184
00:15:18,000 --> 00:15:23,000
a choice that will inevitably lead to the separation we fear. 

185
00:15:23,000 --> 00:15:28,000
>> Can you believe it? We're in the Wall Street Journal again. 

186
00:15:28,000 --> 00:15:31,000
>> Great! They spell our names right this time? 

187
00:15:31,000 --> 00:15:34,500
>> Yes, unfortunately. 

188
00:16:02,470 --> 00:16:08,590
>> Please, be seated. We are gathered here today to join this 

189
00:16:08,590 --> 00:16:12,500
man and this woman in the immutable bonds of holy matrimony. 

190
00:16:12,500 --> 00:16:18,700
>> In a sense, we actually confuse fantasy and reality in our minds. 

191
00:16:21,500 --> 00:16:26,000
What we imagine will go wrong if we say what's in our hearts 

192
00:16:26,000 --> 00:16:30,780
becomes more real to us than the far more likely disaster 

193
00:16:30,780 --> 00:16:34,300
that will result from going along with the crowd. 

194
00:16:35,000 --> 00:16:41,000
>> Our operating costs are sucking this entire company down. 

195
00:16:41,000 --> 00:16:43,920
We failed to meet payroll. 

196
00:16:43,920 --> 00:16:47,000
>> One of the most foolproof signs of a trip to Abilene 

197
00:16:47,000 --> 00:16:52,000
is the assignment of blame, identifying a scapegoat. 

198
00:16:52,000 --> 00:16:56,450
>> Now, I want to know who's responsible. >> Don't look at me. 

199
00:16:56,450 --> 00:16:59,700
I was relying on the progress reports out of R&D. 

200
00:17:00,210 --> 00:17:04,680
>> But assigning blame is not just wasteful, it's irrelevant because it 

201
00:17:04,680 --> 00:17:08,000
perpetuates the illusion that conflict is the problem, 

202
00:17:08,000 --> 00:17:12,000
when everyone is actually in unanimous agreement. 

203
00:17:12,000 --> 00:17:16,000
>> Wait a minute! You're not laying this off on me. 

204
00:17:16,000 --> 00:17:21,000
>> Want to bet? You're on probation. I'm removing you 

205
00:17:21,000 --> 00:17:25,120
from all projects immediately. I'm turning over R&D to 

206
00:17:25,120 --> 00:17:27,500
the Vice President until further notice. 

207
00:17:39,000 --> 00:17:40,470
>> Consider carefully there -- 

208
00:17:40,470 --> 00:17:44,600
>> Nothing can be accomplished between people without an agreement, 

209
00:17:44,600 --> 00:17:49,240
voiced or not, and that includes a trip to Abilene. 

210
00:17:49,240 --> 00:17:53,000
>> If there's anyone present who can show reason why these two 

211
00:17:53,000 --> 00:17:58,000
should not be joined in eternal bonds of matrimony, let them speak now 

212
00:17:58,000 --> 00:18:03,760
or forever hold their peace. >> In a sense, all passengers on the 

213
00:18:03,760 --> 00:18:08,000
Abilene Express are in collusion with one another and,  

214
00:18:08,000 --> 00:18:12,500
therefore, are all equally to blame. 

215
00:18:24,000 --> 00:18:25,728
>> Hi. >> Hello. 

216
00:18:25,728 --> 00:18:27,942
>> You know I was wondering about our meeting, 

217
00:18:27,942 --> 00:18:34,000
and, um, have you ever thought that maybe it's Project X 

218
00:18:34,000 --> 00:18:36,990
that's causing the drain in R&D? >> No. 

219
00:18:36,990 --> 00:18:41,446
>> Of course, it's tempting to want to assign blame to the leader. 

220
00:18:41,446 --> 00:18:44,500
>> Besides, the boss is so gung ho.

221
00:18:44,500 --> 00:18:47,000
Well, he wouldn't cancel it even if it was. >> No. 

222
00:18:47,000 --> 00:18:50,920
You're right -- he wouldn't. >> However, blaming the leader 

223
00:18:50,920 --> 00:18:56,000
is really just another form of collusion. In fact, all members 

224
00:18:56,000 --> 00:19:01,000
are equally responsible for team action or inaction. 

225
00:19:01,000 --> 00:19:06,534
>> Even if, say, you and I tried to cancel it, he wouldn't let us. 

226
00:19:06,534 --> 00:19:11,920
>> No. You're right. He wouldn't. 

227
00:19:13,000 --> 00:19:15,450
>> More important, by the time a group 

228
00:19:15,450 --> 00:19:19,800
reaches this point, all its members are victims. 

229
00:19:30,816 --> 00:19:34,000
So, now that we understand how we get on the road to Abilene, 

230
00:19:34,000 --> 00:19:37,040
isn't there some way we can avoid such wasteful  

231
00:19:37,040 --> 00:19:42,320
and destructive excursions? The answer is yes. 

232
00:19:42,320 --> 00:19:47,490
First of all, each of us has to assess the real risk of taking action, 

233
00:19:47,490 --> 00:19:51,570
as well as the risk of taking no action at all. 

234
00:19:51,570 --> 00:19:55,000
>> Do you, George, take this woman, Sue, to be your 

235
00:19:55,000 --> 00:20:00,470
lawfully wedded wife? >> Second, each of us has a choice: 

236
00:20:00,470 --> 00:20:05,032
remain silent or own up to our own beliefs and feelings 

237
00:20:05,032 --> 00:20:08,480
without attributing beliefs and feelings to others. 

238
00:20:08,480 --> 00:20:17,740
>> for as long as you both shall live? >> I do. 

239
00:20:17,740 --> 00:20:22,700
>> And do you, Sue, take this man, George, to be your lawfully 

240
00:20:22,700 --> 00:20:28,080
wedded husband? To have and to hold, for richer or for poorer, 

241
00:20:28,080 --> 00:20:36,840
in sickness and in health, for as long as you both shall live? 

242
00:20:45,000 --> 00:20:47,800
>> I don't know about you, George, but I, I don't really 

243
00:20:47,800 --> 00:20:52,270
want to get married. >> You don't? 

244
00:20:52,270 --> 00:20:53,862
>> No, George. I'm sorry. 

245
00:20:53,862 --> 00:20:56,309
>> Sorry? 

246
00:21:09,000 --> 00:21:11,500
>> He was never really right for my Sue. 

247
00:21:12,000 --> 00:21:18,000
>> Now, we've cut back on technicians, clerical, utilities; I've scrapped 

248
00:21:18,000 --> 00:21:24,000
nonessentials. >> Third, what is often required is confrontation 

249
00:21:24,000 --> 00:21:28,340
in a group setting -- not with new information or arguments, 

250
00:21:28,340 --> 00:21:32,310
but with what the group has already silently agreed upon. 

251
00:21:32,310 --> 00:21:41,600
>> Now, I did get one anonymous suggestion, which was, "Cancel Project X." 

252
00:21:44,250 --> 00:21:52,000
>> Actually, that was me. Hear me out. 

253
00:21:52,000 --> 00:21:56,230
>> I know I've sung its praises as much as anybody, 

254
00:21:56,230 --> 00:22:02,000
but I, I honestly don't believe Project X will work. 

255
00:22:02,000 --> 00:22:09,122
I, I think it's a bottomless pit, and nothing's going to come out of it. 

256
00:22:12,000 --> 00:22:18,710
>> You know, I think terminating this project could significantly enhance 

257
00:22:18,710 --> 00:22:22,770
our financial profile. We'd have a chance -- just a chance -- of 

258
00:22:22,770 --> 00:22:30,400
recouping first quarter losses. >> Do you realize what you're saying? 

259
00:22:31,130 --> 00:22:36,000
>> Yes I do. I only wish I'd had the nerve to suggest it earlier. 

260
00:22:36,000 --> 00:22:37,900
This project has been ill-advised from the start. 

261
00:22:37,900 --> 00:22:40,510
It may even bankrupt us. 

262
00:22:41,270 --> 00:22:46,000
>> Do you mean that you and I and the rest of us have been dragging 

263
00:22:46,000 --> 00:22:51,800
along a research project that none of us thought would work? 

264
00:22:51,800 --> 00:22:55,000
>> It's crazy. >> I don't know why we did it. 

265
00:22:58,000 --> 00:23:02,330
>> Well, neither do I. 

266
00:23:02,330 --> 00:23:04,718
Alright, let's cancel this blasted project. 

267
00:23:04,718 --> 00:23:06,900
>> The Abilene Paradox. 

268
00:23:06,900 --> 00:23:10,100
You wouldn't think folks could make something so simple as being 

269
00:23:10,100 --> 00:23:15,000
in agreement such a problem, but paradoxes are usually such 

270
00:23:15,000 --> 00:23:18,000
because they're based on a logic different from what we  

271
00:23:18,000 --> 00:23:24,000
understand or expect. If we can break that logic, and also have 

272
00:23:24,000 --> 00:23:30,000
the courage of our convictions, our teams can grow and flourish.
