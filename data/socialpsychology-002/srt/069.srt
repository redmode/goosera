





1
00:00:10 --> 00:00:16,340
On April 15th, 1964, the New York Times 
reported that in Albany, New York, 

2
00:00:16,340 --> 00:00:25,230
a crowd of 4,000 people chanted "jump, 
jump, jump," as a suicidal teenager stood 

3
00:00:25,230 --> 00:00:29,880
on a twelfth-story hotel ledge. 
The teenager had been under psychiatric 

4
00:00:29,880 --> 00:00:34,220
care at the Albany Medical Center, and 
apparently nobody knew how he got out on 

5
00:00:34,220 --> 00:00:37,820
that ledge, but as the crowd grew, 
people started to 

6
00:00:37,820 --> 00:00:41,690
shout things like, "Aw, c'mon, you're 
chicken!" 

7
00:00:41,690 --> 00:00:44,250
"Jump! 
What's the matter, ya yellow?" 

8
00:00:44,250 --> 00:00:48,240
One woman complained to her friend, "I 
wish he'd do it and get it over with. 

9
00:00:48,240 --> 00:00:52,500
If he doesn't hurry up, we're going to 
miss our last bus." 

10
00:00:52,500 --> 00:00:56,089
People even started betting on whether he 
would jump. 

11
00:00:57,540 --> 00:01:00,400
Now, what would cause people to behave 
this way? 

12
00:01:00,400 --> 00:01:06,160
This is an example of what psychologists 
call "deindividuation," a phenomenon first 

13
00:01:06,160 --> 00:01:12,490
discussed in a 1952 article by Leon 
Festinger, Al Pepitone, and Ted Newcomb. 

14
00:01:12,490 --> 00:01:18,310
According to Festinger and his associates, 
deindividuation occurs when individuals 

15
00:01:18,310 --> 00:01:22,490
are not seen or paid attention to as 
individuals. 

16
00:01:22,490 --> 00:01:26,150
The members do not feel that they stand 
out as individuals, and there's a 

17
00:01:26,150 --> 00:01:30,500
reduction of inner restraints against 
doing various things. 

18
00:01:30,500 --> 00:01:34,874
What are the things? 
Well, on the positive side, they might be 

19
00:01:34,874 --> 00:01:39,800
uninhibited dancing, singing, or 
performing without feeling self-conscious.


20
00:01:39,800 --> 00:01:43,800
But there's also a darker side of deindividuation

21
00:01:43,800 --> 00:01:48,200
that can lead to lynchings, gang rapes, riots, 
stealing, cheating -- 

22
00:01:48,200 --> 00:01:54,100
things that violate norms of restraint 
that most of us rely on in daily life. 

23
00:01:54,300 --> 00:01:57,140
Well, Festinger's paper was interesting, 

24
00:01:57,140 --> 00:02:01,896
but psychologists didn't really do much 
with the idea of deindividuation until 

25
00:02:01,896 --> 00:02:06,700
about 1970, when Phil Zimbardo 
published a monograph entitled 

26
00:02:06,700 --> 00:02:13,157
"The Human Choice: Individuation, 
Reason, and Order versus Deindividuation, 

27
00:02:13,157 --> 00:02:17,860
Impulse, and Chaos." 
In his paper, Zimbardo listed a number of 

28
00:02:17,860 --> 00:02:22,500
"input variables," or antecedent conditions 
that increase 

29
00:02:22,500 --> 00:02:26,400
the chances of deindividuation: 
anonymity; 

30
00:02:26,400 --> 00:02:30,840
diffused responsibility in which 
individuals aren't held personally 

31
00:02:30,840 --> 00:02:34,850
accountable for their behavior; 
group size and group activity; 

32
00:02:34,850 --> 00:02:39,940
physical and mental arousal; 
an altered time perspective in which the 

33
00:02:39,940 --> 00:02:44,570
present is emphasized and the past and 
future seem distant; 

34
00:02:44,570 --> 00:02:48,770
sensory input overload, or a state of 
overstimulation; 

35
00:02:48,770 --> 00:02:53,970
physical involvement in the act; and 
altered states of consciousness as a 

36
00:02:53,970 --> 00:02:57,800
result of drugs, alcohol, even certain 
sleep patterns. 

37
00:02:57,800 --> 00:03:02,439
Zimbardo proposed that when taken 
together, these factors can lead to 

38
00:03:02,439 --> 00:03:07,900
deindividuation -- a state in which 
behavior's relatively unrestrained, 

39
00:03:07,900 --> 00:03:11,250
wild, and sometimes potentially violent. 

40
00:03:12,830 --> 00:03:17,480
Since the time that Zimbardo proposed his 
theory, dozens of laboratory and field 

41
00:03:17,480 --> 00:03:21,870
studies have looked at deindividuation, 
running the full range from very mild 

42
00:03:21,870 --> 00:03:25,800
cases all the way up to extreme cases of 
deindividuation,

43
00:03:25,800 --> 00:03:30,133
and even though the results are mixed and 
the effect isn't always strong,

44
00:03:30,133 --> 00:03:37,400
a 1998 meta-analysis of 60 independent 
studies concluded that in the main, 

45
00:03:37,400 --> 00:03:41,230
one key aspect of Zimbardo's theory is 
correct, 

46
00:03:41,230 --> 00:03:46,200
and that is that larger groups either 
induce or facilitate stronger 

47
00:03:46,200 --> 00:03:52,300
anti-normative behavior -- that is, 
behavior that violates social norms. 

48
00:03:52,300 --> 00:03:54,408
Moreover, the meta-analysis didn't find 

49
00:03:54,408 --> 00:03:59,770
significant gender differences in 
response to deindividuating situations. 

50
00:03:59,770 --> 00:04:03,500
Women were just as likely as men to 
violate norms when the setting 

51
00:04:03,500 --> 00:04:08,910
promoted deindividuation. 
So, this isn't simply a male thing. 

52
00:04:08,910 --> 00:04:13,750
More recently, the role of group size in 
Zimbardo's model was supported by a 

53
00:04:13,750 --> 00:04:18,800
study that looked at 60 cases in which an 
African American was killed by a 

54
00:04:18,800 --> 00:04:26,400
lynch mob in the United States -- 
events that occurred between 1899 and 1946. 

55
00:04:26,400 --> 00:04:30,200
What this study found is that lynchings 
tended to become 

56
00:04:30,200 --> 00:04:35,190
more vicious as the size 
of the mob grew -- that is, more likely to 

57
00:04:35,190 --> 00:04:40,260
involve atrocities like burning or 
mutilating the victim. 

58
00:04:40,260 --> 00:04:44,640
To take a notorious example â and I should 
warn you upfront that the next minute of 

59
00:04:44,640 --> 00:04:49,180
material is extremely disturbing,  
so you might want to fast-forward â here's 

60
00:04:49,180 --> 00:04:55,500
the mob that lynched 17-year-old Jesse 
Washington in Waco, Texas, 

61
00:04:55,500 --> 00:05:02,200
on May 15th, 1916. 
The lynching took place on the city hall lawn, 

62
00:05:02,200 --> 00:05:07,000
and there were over 10,000 people 
present, including the mayor, 

63
00:05:07,000 --> 00:05:12,320
chief of police, and children. 
You can see Washington, here, at the base 

64
00:05:12,320 --> 00:05:15,410
of a tree, kind of hard to make out, but 
there he is. 

65
00:05:15,410 --> 00:05:21,230
During the event (which later became known, 
by the way, as "The Waco Horror"), 

66
00:05:21,230 --> 00:05:25,900
members of the mob castrated Washington, 
cut off his fingers and toes, 

67
00:05:25,900 --> 00:05:31,400
and hung him over a bonfire, lowering and 
raising him over the fire for about 

68
00:05:31,400 --> 00:05:37,700
two hours -- hard to even imagine. 
Absolutely barbaric behavior, 

69
00:05:37,700 --> 00:05:43,000
fueled by a mob mentality â an extreme 
case of deindividuation. 

70
00:05:43,900 --> 00:05:48,800
On a much more mild note, a 2010 
laboratory study suggested that 

71
00:05:48,800 --> 00:05:54,800
even the feeling of anonymity might be 
enough to deindividuate people. 

72
00:05:54,800 --> 00:05:58,690
In one of the reported experiments, 
participants in a room with slightly 

73
00:05:58,690 --> 00:06:03,320
dimmed lighting cheated more than 
participants in a well lit room. 

74
00:06:03,320 --> 00:06:06,700
(People in the dimmed room were just told 
that some of the lights were out.)

75
00:06:07,220 --> 00:06:11,810
And in a second experiment, which 
involved a two-person computer game, 

76
00:06:11,810 --> 00:06:17,010
participants wearing sunglasses behaved 
more selfishly than participants wearing 

77
00:06:17,010 --> 00:06:21,100
clear glasses, even though there was no 
face-to-face interaction 

78
00:06:21,100 --> 00:06:25,100
in which sunglasses would 
have even been visible to the other person. 

79
00:06:25,100 --> 00:06:29,800
What's interesting is that in both experiments, 
people's anonymity 

80
00:06:29,800 --> 00:06:34,100
didn't actually change;
they simply felt more anonymous. 

81
00:06:36,100 --> 00:06:38,200
Well, as I mentioned earlier in the course, 

82
00:06:38,200 --> 00:06:41,975
Professor Zimbardo has kindly 
agreed to share with our class his 

83
00:06:41,975 --> 00:06:46,260
documentary Quiet Rage, 
a documentary that shows one of the most 

84
00:06:46,260 --> 00:06:50,100
dramatic demonstrations of what can 
happen when people 

85
00:06:50,100 --> 00:06:54,370
become deindividuated, 
when they become engulfed in a role 

86
00:06:54,370 --> 00:06:58,970
and lose their sense of personal identity or 
responsibility. 

87
00:06:58,970 --> 00:07:03,800
The video discusses a very famous study 
in social psychology called the 

88
00:07:03,800 --> 00:07:08,212
Stanford Prison Experiment, 
which was conducted by Phil Zimbardo over 

89
00:07:08,212 --> 00:07:13,400
40 years ago, and here you can see one of 
the guards wearing sunglasses. 

90
00:07:14,450 --> 00:07:19,600
So, please watch Quiet Rage and then come 
back for Part 2 of our discussion 

91
00:07:19,600 --> 00:07:25,772
about deindividuation. But first, a pop-up 
questionâ¦ 

