




1
00:00:06,840 --> 00:00:10,404
So far in the course, our discussion of 
social perception has focused mainly on 

2
00:00:10,404 --> 00:00:14,721
judgments about people. 
But social perception also includes 

3
00:00:14,721 --> 00:00:18,592
judgments about behavior, and that's 
what I want to focus on in 

4
00:00:18,592 --> 00:00:21,745
this video. 
Specifically, I want to say a few words 

5
00:00:21,745 --> 00:00:26,571
about attribution theory, 
one of the best known theories in the 

6
00:00:26,571 --> 00:00:30,694
history of social psychology. 
It's a theory about how people interpret 

7
00:00:30,694 --> 00:00:33,687
behavior -- how they make "causal attributions," or 

8
00:00:33,687 --> 00:00:40,520
causal explanations, for their behavior 
as well as the behavior of other people. 

9
00:00:40,520 --> 00:00:45,000
Now, why should anybody care? 
Why does it matter how people explain behavior?

10
00:00:45,000 --> 00:00:46,836
Well, because the way you explain 

11
00:00:46,836 --> 00:00:51,050
behavior often determines what you'll do 
about it. 

12
00:00:51,050 --> 00:00:55,510
If, for example, you fail an exam because 
you haven't prepared for it, 

13
00:00:55,510 --> 00:00:59,030
you might study harder the next time 
around. But if you attribute the failure 

14
00:00:59,030 --> 00:01:02,825
to the exam being unfair â if that's your 
explanation â 

15
00:01:02,825 --> 00:01:06,745
then maybe instead you'll talk with other 
students, maybe you'll file a complaint, 

16
00:01:06,745 --> 00:01:10,518
maybe you'll visit the instructor during 
office hours, 

17
00:01:10,518 --> 00:01:14,578
but it will be a different solution. 
Or going back to the example we 

18
00:01:14,578 --> 00:01:17,624
discussed earlier, 
if you're the leader of South Korea and 

19
00:01:17,624 --> 00:01:22,050
you attribute North Korea's armament to 
aggressive intentions, 

20
00:01:22,050 --> 00:01:26,397
you'll probably behave differently than 
if you explain North Korea's behavior in 

21
00:01:26,397 --> 00:01:32,255
terms of its own national defense. 
Think about how important attributions 

22
00:01:32,255 --> 00:01:38,084
are in the Palestinian-Israeli conflict! 
In addition, understanding how people 

23
00:01:38,084 --> 00:01:41,270
explain behavior is useful at the 
personal level. 

24
00:01:42,280 --> 00:01:45,470
For example, it can help us avoid 
conflict. 

25
00:01:45,470 --> 00:01:48,559
It can improve friendships and romantic 
relationships. 

26
00:01:48,559 --> 00:01:52,770
It can increase productivity and job 
satisfaction. 

27
00:01:52,770 --> 00:01:55,200
And it can lead to greater 
self-understanding. 

28
00:01:56,900 --> 00:02:00,500
So, let's start our discussion of 
attribution theory with a few words about 

29
00:02:00,500 --> 00:02:03,705
what exactly it is and where it came 
from. 

30
00:02:03,705 --> 00:02:08,949
Attribution theory is generally credited 
to Fritz Heider, who kicked everything 

31
00:02:08,949 --> 00:02:15,530
off with a 1958 book called The 
Psychology of Interpersonal Relations, 

32
00:02:15,530 --> 00:02:20,500
but it was social psychologists like 
Bernie Weiner and Hal Kelley who were 

33
00:02:20,500 --> 00:02:24,200
actually among the first to develop 
attribution theory into a detailed and 

34
00:02:24,200 --> 00:02:28,472
testable theory. 
According to Kelley, people generally 

35
00:02:28,472 --> 00:02:31,940
explain behavior in terms of three 
possible causes. 

36
00:02:31,940 --> 00:02:37,035
The first one is simply the person â 
that is, something about the person may 

37
00:02:37,035 --> 00:02:41,900
have caused the behavior. 
Second, entity â 

38
00:02:41,900 --> 00:02:46,030
some enduring feature of the situation or 
the stimulus, the entity, 

39
00:02:46,030 --> 00:02:51,970
something outside the person may have 
caused the behavior. And finally, time â 

40
00:02:51,970 --> 00:02:55,800
something about the particular occasion 
may have caused the behavior. 

41
00:02:55,800 --> 00:03:00,022
Kelley proposed that these attributions 
are based largely on three corresponding 

42
00:03:00,022 --> 00:03:06,040
sources of information. 
First, he said people look at consensus: 

43
00:03:06,040 --> 00:03:09,790
Do other people respond similarly in the 
same situation? 

44
00:03:09,790 --> 00:03:14,830
Is there a sort of consensus behavior in 
response to the situation? 

45
00:03:14,830 --> 00:03:19,800
Second, distinctiveness:  
Do other situations elicit the same behavior, 

46
00:03:19,800 --> 00:03:22,800
or is this one distinctive in 
some way? 

47
00:03:22,800 --> 00:03:27,800
And finally, consistency: 
Does the same thing happen time after time?

48
00:03:29,500 --> 00:03:31,614
So, to illustrate, suppose that you were 

49
00:03:31,614 --> 00:03:35,800
the only person to perform well on a 
variety of tests across 

50
00:03:35,800 --> 00:03:39,250
a range of occasions. 
Well done! 

51
00:03:39,250 --> 00:03:42,722
In terms of Kelley's theory, what kind of 
pattern of information would that 

52
00:03:42,722 --> 00:03:45,634
represent? 
Well, for one thing, we know that it 

53
00:03:45,634 --> 00:03:47,853
would be low consensus. 
Right? 

54
00:03:47,853 --> 00:03:52,500
Because it's not that everybody does this; 
that's not the consensus behavior, but in fact, 

55
00:03:52,500 --> 00:03:56,450
you're the only one who's performing well. 

56
00:03:56,450 --> 00:04:01,400
Second, it would be low distinctiveness 
because it happens across a wide variety 

57
00:04:01,400 --> 00:04:04,500
of tests. 
There's nothing distinctive about the 

58
00:04:04,500 --> 00:04:09,800
results of any one particular test; 
you're doing well in this test and that 

59
00:04:09,800 --> 00:04:13,380
test and that test and so on. 
And, finally, high consistency 

60
00:04:13,380 --> 00:04:16,613
because the same thing happens again and 
again and again across 

61
00:04:16,613 --> 00:04:20,062
a range of occasions. 
Even if it's the same test, every single 

62
00:04:20,062 --> 00:04:25,010
time you take it, you're doing well. 
In such a case, Kelley would predict a 

63
00:04:25,010 --> 00:04:31,040
person attribution -- an explanation in 
terms of your personal abilities. 

64
00:04:31,040 --> 00:04:35,400
Let's pause for a pop-up question just to 
make sure Kelley's framework is clear. 

65
00:04:38,739 --> 00:04:42,600
Kelley realized, of course, how 
oversimplified this model was, 

66
00:04:42,600 --> 00:04:46,435
but he proposed it as a general framework 
to think about how people make causal 

67
00:04:46,435 --> 00:04:50,263
attributions, and in most respects, the model has held 

68
00:04:50,263 --> 00:04:54,780
up quite well over time. 
It's received quite a bit of research support.

69
00:04:54,780 --> 00:04:56,800
I'll mention some exceptions in the next video

70
00:04:56,800 --> 00:05:02,090
when we cover attributional biases, but for 
now I want to talk about just one 

71
00:05:02,090 --> 00:05:05,780
factor that influences causal 
attributions, 

72
00:05:05,780 --> 00:05:08,650
whether they're accurate or whether 
they're biased, 

73
00:05:08,650 --> 00:05:12,630
and that factor is something social 
psychologists call "salience." 

74
00:05:12,630 --> 00:05:17,800
A salient stimulus is one that grabs your 
attention in some way, that's prominent. 

75
00:05:17,800 --> 00:05:23,200
So, [CLAP] that's salient. 
Or BLAHHH! That's salient. 

76
00:05:23,200 --> 00:05:28,540
Or WHOA, WHOA, right? 
Or this is salient. 

77
00:05:28,540 --> 00:05:32,431
You get the idea. 
And the research finding here is that 

78
00:05:32,431 --> 00:05:37,990
salient stimuli tend to be viewed as 
disproportionately causal. 

79
00:05:37,990 --> 00:05:42,150
That is, in general, the more salient a 
stimulus is, the more likely it is to be 

80
00:05:42,150 --> 00:05:47,510
viewed as causal, as having caused a 
behavior to occur -- 

81
00:05:47,510 --> 00:05:52,180
not always, not in every situation, but in 
many situations. 

82
00:05:52,180 --> 00:05:56,210
In other words, perceptions of causality 
are partly a function of where one's 

83
00:05:56,210 --> 00:05:59,810
attention is directed within the 
environment. 

84
00:05:59,810 --> 00:06:05,800
And attention is, in turn, a function of 
salience: we pay more attention to things 

85
00:06:05,800 --> 00:06:10,900
that are salient. 
In 1978, a landmark paper on this topic 

86
00:06:10,900 --> 00:06:15,130
was published by Shelley Taylor and Susan 
Fiske. 

87
00:06:15,130 --> 00:06:18,485
In this paper, they reviewed a large 
number of studies on the link between 

88
00:06:18,485 --> 00:06:22,754
salience and causal attribution, 
but let me just highlight a few to give 

89
00:06:22,754 --> 00:06:26,650
you a sense of these studies, and some of the most interesting results 

90
00:06:26,650 --> 00:06:31,095
that they generated. 
In one experiment, six observers watched 

91
00:06:31,095 --> 00:06:35,900
a dialogue between two people while 
seated either behind one of the 

92
00:06:35,900 --> 00:06:39,980
individuals, whom I've labeled A and B in 
the diagram, 

93
00:06:39,980 --> 00:06:45,146
or on the sidelines between the two. 
In this particular study, the observers 

94
00:06:45,146 --> 00:06:48,910
were actually the experimental subjects,
or participants, 

95
00:06:48,910 --> 00:06:53,745
and the two people in the center, Person 
A and Person B, were "confederates" of the 

96
00:06:53,745 --> 00:06:56,335
experimenter, 
meaning that they were members of the 

97
00:06:56,335 --> 00:07:01,229
research team playing a particular role. 
All of the observers watched the 

98
00:07:01,229 --> 00:07:05,942
conversation simultaneously, so the only 
informational difference was 

99
00:07:05,942 --> 00:07:09,840
the visual salience of the two people 
holding the discussion. 

100
00:07:09,840 --> 00:07:15,080
That is, everybody listened to the same 
conversation at the same time. 

101
00:07:15,080 --> 00:07:19,594
For people sitting behind Person A, 
looking into the face of Person B, the 

102
00:07:19,594 --> 00:07:24,647
most salient person was B. 
On the other hand, for people sitting 

103
00:07:24,647 --> 00:07:30,190
behind Person B, observing Person A, the 
most salient person was A. 

104
00:07:30,190 --> 00:07:34,539
And for people sitting on the sidelines, 
A and B were equally salient. 

105
00:07:35,560 --> 00:07:39,018
What were the results? 
Well, despite the fact that everyone 

106
00:07:39,018 --> 00:07:43,020
heard the same conversation, Taylor and 
Fiske found that observers tended to rate 

107
00:07:43,020 --> 00:07:48,760
the person in their visual field as 
having set the tone of the conversation, 

108
00:07:48,760 --> 00:07:52,646
having been the one who determined the 
type of information exchanged, and having 

109
00:07:52,646 --> 00:07:59,300
caused the other person's responses. 
That is, observers of Person A saw A as 

110
00:07:59,300 --> 00:08:05,000
more influential or causal. 
Observers of Person B saw B as more causal. 

111
00:08:05,000 --> 00:08:08,200
And observers on the sideline gave 
ratings that fell 

112
00:08:08,200 --> 00:08:13,387
in between the two other groups. 
Now, let's return for a moment to a 

113
00:08:13,387 --> 00:08:17,900
question that I posed earlier, which is: 
Why should we care about these findings? 

114
00:08:17,900 --> 00:08:21,515
Why does this matter? 
Well, if we want to make fair and 

115
00:08:21,515 --> 00:08:26,214
accurate judgments about what led 
somebody to behave in a certain way, 

116
00:08:26,214 --> 00:08:30,930
or who's responsible, or, in the case of a 
crime, even who's guilty, 

117
00:08:30,930 --> 00:08:33,900
then we probably don't want to be 
influenced by where we happen to be 

118
00:08:33,900 --> 00:08:37,620
looking at the moment that we're making 
the judgment, 

119
00:08:37,620 --> 00:08:41,510
and we sure don't want to be influenced 
without knowing it. 

120
00:08:41,510 --> 00:08:45,110
And yet, if we're not careful, this is 
exactly what can happen. 

121
00:08:45,110 --> 00:08:49,391
Let me give you an example. 
Research by Dan Lassiter and his 

122
00:08:49,391 --> 00:08:53,700
colleagues on false confessions and 
videotaped police interrogations 

123
00:08:53,700 --> 00:08:59,200
has found that when the camera angle focuses 
on the suspect, people are twice as 

124
00:08:59,200 --> 00:09:04,200
likely to see the suspect as guilty than 
when the camera is focused on both the 

125
00:09:04,200 --> 00:09:08,527
suspect and interrogator. 
And when it comes to preventing false 

126
00:09:08,527 --> 00:09:13,500
confessions -- that is, confessions when the 
suspect's actually innocent -- the best 

127
00:09:13,500 --> 00:09:17,470
practice of all may be to focus mainly on 
the interrogator 

128
00:09:17,470 --> 00:09:21,628
because it makes that person seem more 
responsible for pressuring the suspect 

129
00:09:21,628 --> 00:09:26,384
into a false confession. 
Professor Lassiter was kind enough to 

130
00:09:26,384 --> 00:09:30,146
share some video clips that he used in 
this research reenacting 

131
00:09:30,146 --> 00:09:34,255
a real life case that experts say was 
probably a false confession. 

132
00:09:34,255 --> 00:09:39,000
Let's watch one minute of the 
interrogation, first focusing on the suspect,

133
00:09:39,000 --> 00:09:42,200
which is the most common way 
that police interrogations are 

134
00:09:42,200 --> 00:09:45,812
videotaped, 
and then we'll watch the very same 

135
00:09:45,812 --> 00:09:49,735
interaction, this time focusing on the 
interrogator. 

136
00:09:51,000 --> 00:09:55,400
 >> Now, when you led her off the road, 
you said you tried to, 

137
00:09:55,400 --> 00:10:01,300
I guess either calm her down or 
hug her, and she pulled away. 

138
00:10:01,800 --> 00:10:05,959
 >> Yeah. 
 >> Was she pulling away from you or... 

139
00:10:05,959 --> 00:10:10,930
 >> Well, she was kind of pulling away 
and turning away from me, 

140
00:10:10,930 --> 00:10:15,900
and then I, I think she turned her back 
around, and I think she said maybe 

141
00:10:15,900 --> 00:10:20,700
something about not caring any more. 
 >> And did that make you angry 

142
00:10:20,700 --> 00:10:24,770
or frustrated or what? 
 >> I assume, yes. 

143
00:10:24,770 --> 00:10:29,200
 >> Assume yes what? 
 >> Well, I'm sure it came close to, 

144
00:10:29,200 --> 00:10:31,370
to killing me. 
 >> Did it make you -- 

145
00:10:31,370 --> 00:10:36,262
well, I asked you two questions, son. 
Did it make you angry or frustrated 

146
00:10:36,262 --> 00:10:40,039
or both or neither? 
 >> Well, it, it probably, probably 

147
00:10:40,039 --> 00:10:43,544
wasn't anger. 
It was probably just frustration. 

148
00:10:43,544 --> 00:10:46,784
 >> Now, when you struck her, did you 
pick up anything 

149
00:10:46,784 --> 00:10:50,030
on the ground to strike her with? 
 >> I, I don't think so. 

150
00:10:50,030 --> 00:10:53,378
 >> Could you have picked up a rock and 
hit her with it? 

151
00:10:53,378 --> 00:10:56,505
 >> I could have, you know. 
 >> Is that a possibility? 

152
00:10:56,505 --> 00:11:02,600
 >> Yeah. I mean, anything is a possibility,  
but I don't have memories. 

153
00:11:04,200 --> 00:11:06,535
 >> So, that's the interrogation focusing on 

154
00:11:06,535 --> 00:11:10,670
the suspect, which is how most 
interrogations are recorded, 

155
00:11:10,670 --> 00:11:13,620
and I must say that the suspect does look 
guilty. 

156
00:11:13,620 --> 00:11:17,400
But before we reach a verdict, let's 
watch the very same interrogation,  

157
00:11:17,400 --> 00:11:22,714
this time from the suspect's point of view, 
focusing on the interrogator, 

158
00:11:22,714 --> 00:11:26,260
who's clearly putting a lot of pressure on the 
suspect. 

159
00:11:26,260 --> 00:11:30,100
Remember that the experts think that this 
case is probably one of 

160
00:11:30,100 --> 00:11:33,957
a false confession, in which the interrogator 
is pressuring 

161
00:11:33,957 --> 00:11:39,600
the suspect and eventually the suspect 
confesses to a crime he didn't commit. 

162
00:11:42,400 --> 00:11:48,010
 >> Now, when you led her off the road, 
you said you tried to, I guess either 

163
00:11:48,010 --> 00:11:52,300
calm her down or hug her, and she pulled 
away. 

164
00:11:52,600 --> 00:11:57,600
 >> Yeah. 
 >> Was she pulling away from you or... 

165
00:11:58,600 --> 00:12:02,150
 >> She was kind of pulling away and 
turning away from me, 

166
00:12:02,150 --> 00:12:04,645
and then I, I think she turned her 
back around, 

167
00:12:04,645 --> 00:12:10,070
and I think she said maybe something 
about not caring anymore. 

168
00:12:10,070 --> 00:12:14,100
 >> And did that make you angry or 
frustrated or what? 

169
00:12:14,980 --> 00:12:17,182
 >> I assume, yes. 
 >> Assume yes what? 

170
00:12:17,182 --> 00:12:21,550
 >> Well, I'm sure it came close to, to 
killing me. 

171
00:12:21,550 --> 00:12:24,800
 >> Did it make you -- 
well, I asked you two questions, son. 

172
00:12:24,800 --> 00:12:29,425
Did it make you angry or frustrated or 
both or neither? 

173
00:12:29,425 --> 00:12:33,009
 >> Well, it, it probably, probably 
wasn't anger. It was probably just 

174
00:12:33,009 --> 00:12:36,152
frustration. 
 >> Now, when you struck her, did you 

175
00:12:36,152 --> 00:12:39,425
pick up anything on the ground to strike 
her with? 

176
00:12:39,425 --> 00:12:42,370
 >> I, I don't think so. 
 >> Could you have picked up a rock and 

177
00:12:42,370 --> 00:12:46,384
hit her with it? 
 >> I could have, you know. 

178
00:12:46,384 --> 00:12:48,740
 >> Is that a possibility? 
 >> Yeah. 

179
00:12:48,740 --> 00:12:52,980
I mean, anything is a possibility, but I 
don't have memories. 

180
00:12:55,400 --> 00:12:59,300
 >> Focusing on the interrogator, it's 
much easier to see 

181
00:12:59,300 --> 00:13:02,800
the pressure being applied and to see 
how an innocent person  

182
00:13:02,800 --> 00:13:07,491
might end up looking guilty. 
In fact, because of this research,  

183
00:13:07,491 --> 00:13:12,500
New Zealand adopted a national policy that 
videotaped police interrogations 

184
00:13:12,500 --> 00:13:17,033
have to focus on both the suspect and the 
interrogator, 

185
00:13:17,033 --> 00:13:21,970
so that the video is as fair as possible, 
as even-handed as possible. 

186
00:13:21,970 --> 00:13:27,100
This is a great example of moving from 
theory -- in this case, attribution theory -- 

187
00:13:27,100 --> 00:13:32,420
to research on police interrogations, to 
policy. 

188
00:13:34,100 --> 00:13:37,900
Research by Shelley Taylor, Susan Fiske, 
and their colleagues has also found  

189
00:13:37,900 --> 00:13:41,588
that racial minority members are seen as 
talking more 

190
00:13:41,588 --> 00:13:46,590
and being more influential when they're 
the only minority member in a group 

191
00:13:46,590 --> 00:13:50,878
than when the group contains other 
minority members, thereby making 

192
00:13:50,878 --> 00:13:55,985
the same person seem less salient. 
Now, of course, when things go well,  

193
00:13:55,985 --> 00:14:01,246
the effects of salience might be fine, but when 
there's a problem,  

194
00:14:01,246 --> 00:14:05,180
when a team is defeated, when a group fails,

195
00:14:05,180 --> 00:14:09,980
maybe even when the economy is weak,
then people who are salient 

196
00:14:09,980 --> 00:14:13,600
because they look different or because they 
sound different 

197
00:14:13,600 --> 00:14:18,430
are at risk of becoming scapegoated -- 
that is, blamed because their salience 

198
00:14:18,430 --> 00:14:24,040
makes them seem more causal,
makes them seem more responsible. 

199
00:14:24,040 --> 00:14:28,194
Taylor and Fiske found that this dynamic 
operates across a wide variety 

200
00:14:28,194 --> 00:14:32,800
of circumstances. For example, women tend 
to be seen as more  

201
00:14:32,800 --> 00:14:36,300
causal when they're the only female 
in a group than when there are  

202
00:14:36,300 --> 00:14:40,882
other women in the group, and people are 
seen as more causal when 

203
00:14:40,882 --> 00:14:44,706
they rock in a rocking chair rather than 
sitting motionless,

204
00:14:44,706 --> 00:14:47,792
or they sit under a bright light versus a 
dim light. 

205
00:14:47,792 --> 00:14:52,139
In fact, some studies have even found that 
when people are seated in front of a 

206
00:14:52,139 --> 00:14:56,980
large mirror, they tend to view 
themselves as disproportionately causal. 

207
00:14:58,380 --> 00:15:02,097
So, the main point to take away from 
these studies on salience is that causal 

208
00:15:02,097 --> 00:15:07
attribution is not simply a matter of 
logical deduction; it's also partly a 

209
00:15:07 --> 00:15:10,780
matter of sensory perception -- 
of what we happen to be looking at  

210
00:15:10,780 --> 00:15:13,910
at the moment or happen to be hearing at the 
moment. 

211
00:15:13,910 --> 00:15:18,990
Again, we're coming back to the 
psychological construction of reality. 

212
00:15:18,990 --> 00:15:23,500
Usually, that psychological construction 
helps us make very accurate causal 

213
00:15:5 --> 00:15:30,000
attributions -- usually, but not always. 
Sometimes we mess up. 

214
00:15:30,000 --> 00:15:32,590
And that's the topic of our next video. 

