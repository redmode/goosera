1
00:00:06,250 --> 00:00:08,510
Okay, so let's get started.

2
00:00:08,510 --> 00:00:13,020
I'm very pleased to welcome you all to today's Social Psychology Hangout.

3
00:00:13,020 --> 00:00:19,240
I want to say just a few words about our guest of honor, who is Mahzarin Banaji --

4
00:00:19,240 --> 00:00:22,260
somebody who is, in my opinion, not only one of the world's most

5
00:00:22,260 --> 00:00:26,104 
brilliant social psychologists, but also one of the kindest.

6
00:00:26,104 --> 00:00:32,200
She can deny it all she wants, but it's true, and there's quite a consensus, I think.

7
00:00:32,200 --> 00:00:36,190 
She is the Richard Clark Cabot Professor of Social Ethics 

8
00:00:36,190 --> 00:00:39,520
in the Department of Psychology at Harvard University.

9
00:00:39,520 --> 00:00:44,000
She is a past President of the Association for Psychological Science,

10
00:00:44,000 --> 00:00:47,600
which is the world's largest professional society that's 

11
00:00:47,600 --> 00:00:50,810
specifically devoted to psychological science.

12
00:00:50,810 --> 00:00:53,680
And closer to home, she was a founding member 

13
00:00:53,680 --> 00:00:56,700 
of the Social Psychology Network Advisory Board.

14
00:00:56,700 --> 00:01:00,540
So she knows Social Psychology Network inside and out.

15
00:01:00,540 --> 00:01:04,300
She's also a co-creator of the Implicit Association Test, 

16
00:01:04,300 --> 00:01:07,700
which I think most members of our class know well.

17
00:01:07,700 --> 00:01:12,300
And she is co-author of "Blind Spot: Hidden Biases of Good People,"

18
00:01:12,300 --> 00:01:16,270
which is a book that was published just last year, and which 

19
00:01:16,270 --> 00:01:21,300
very skillfully summarizes the research literature on implicit bias.

20
00:01:21,300 --> 00:01:25,850
So, it's designed for a general audience, and I highly recommend it.

21
00:01:25,850 --> 00:01:30,280
I also cannot let this opportunity go without thanking her publicly for

22
00:01:30,280 --> 00:01:34,130
something that our class has made great use of,

23
00:01:34,130 --> 00:01:37,910
which is psychology-related articles in Wikipedia.

24
00:01:37,910 --> 00:01:42,000
It is no accident that these articles often are excellent.

25
00:01:42,000 --> 00:01:45,660
What happened is that when Professor Banaji was elected President of

26
00:01:45,660 --> 00:01:50,700
the Association for Psychological Science, and began serving in 2010, 

27
00:01:50,700 --> 00:01:56,300
she had the foresight to take Wikipedia seriously when many people in academia 

28
00:01:56,300 --> 00:02:01,120
were dismissing it and thinking that is was a fad, or something like this, and instead,

29
00:02:01,120 --> 00:02:06,100
she called on colleagues and students to participate in making it better --

30
00:02:06,100 --> 00:02:11,140
colleagues from around the world -- to add articles about missing articles, or

31
00:02:11,140 --> 00:02:16,010
missing topics, and to improve the articles that were already there.

32
00:02:16,010 --> 00:02:21,389
Since she made that call, more than 3,000 people around the world have gotten busy

33
00:02:21,389 --> 00:02:27,000
improving psychology-related topics and neuroscience-related topics in Wikipedia.

34
00:02:27,000 --> 00:02:32,800
So, if you find that you are making use of a really terrific article in Wikipedia,

35
00:02:32,800 --> 00:02:39,520
you indirectly have Professor Banaji to thank, and I want to thank you very much myself.

36
00:02:39,520 --> 00:02:42,600
So, what I thought we would do is just begin with some very brief introductions --

37
00:02:42,600 --> 00:02:49,530
even just 30 seconds each. You can let us know what your name is and where you are,

38
00:02:49,530 --> 00:02:53,890
what your country is, and anything else that you'd like to briefly share, and

39
00:02:53,890 --> 00:02:58,650
then we'll get to questions and answers. So, anybody like to begin?

40
00:02:58,650 --> 00:03:02,600
And then, of course, we'll turn it over to Professor Banaji as well.

41
00:03:02,600 --> 00:03:11,700
>> I can start. >> Okay. >> My name is Deanna and I am in British Columbia, Canada.

42
00:03:11,700 --> 00:03:19,730
I am a wife, a mother, a grandmother, and a full-time director of an Internet marketing company.

43
00:03:19,730 --> 00:03:28,330
>> Terrific. >> Marzia from Venice, Italy, and I'm a Director of Marketing for a luxury goods company.

44
00:03:28,330 --> 00:03:32,538
>> You have a beautiful background.

45
00:03:32,538 --> 00:03:42,820
>> Hi, I'm Maria. I'm here in San Francisco, but I'm actually Filipino, like, yeah.

46
00:03:42,820 --> 00:03:49,000
I'm a software developer and currently, I'm actually, like, part of my Day of Compassion --

47
00:03:49,000 --> 00:03:55,810
or, life of compassion -- I've been trying to encourage more minorities in tech,

48
00:03:55,810 --> 00:04:01,130
being a double minority myself. So, yeah. >> Terrific. Great.

49
00:04:01,130 --> 00:04:10,337
>> I'm Nycolle. I'm from San Pablo, Brazil. I'm a public relation for an NGO.

50
00:04:10,337 --> 00:04:17,120
>> Hi, I'm Meila. I'm from Jakarta, Indonesia. I work as consultant before, 

51
00:04:17,120 --> 00:04:21,930
but now I'm changing my career because I wanted to work in NGO, and

52
00:04:21,930 --> 00:04:27,700
I think this social psychology class is very important for me, so why I joined. Thank you!

53
00:04:27,700 --> 00:04:33,900
>> Welcome. Thank you! >> Hi, I'm Sumit from India and I'm working as Assistant Manager,  

54
00:04:33,900 --> 00:04:41,400
Proposals, a top five IT company, in one of the top IT company, and I'm learning a lot 

55
00:04:41,400 --> 00:04:47,610
about social psychology -- about the society as well as about, a lot about myself.

56
00:04:47,610 --> 00:04:50,280
I'm really looking forward to the rest of the class.

57
00:04:50,280 --> 00:04:52,810
>> My name is Sashank. I am from India, from Visakhapatnam.

58
00:04:52,810 --> 00:04:56,440
>> Oh, okay -- you're not far from where I grew up.

59
00:04:56,440 --> 00:05:03,300
>> Yes. So, I am presently a student -- age 20 -- studying in an engineering college.

60
00:05:03,300 --> 00:05:07,300
>> Hi! I'm Miriam, I'm from Mexico City. I'm 36 years old.

61
00:05:07,300 --> 00:05:14,270
I studied business administration, and I have my own business for organization.

62
00:05:14,270 --> 00:05:20,250
>> Okay, so I think, then, to our guest of honor, Professor Banaji,

63
00:05:20,250 --> 00:05:24,210
I think maybe what you could do is begin just by saying a few words

64
00:05:24,210 --> 00:05:30,160
to set the stage of what implicit bias is -- just to remind everybody -- and then

65
00:05:30,160 --> 00:05:35,510
maybe you could talk a little bit about the IAT and "Blindspot" or any other topics.

66
00:05:35,510 --> 00:05:40,650
Just a brief introduction, and then we can open up for questions and answers.

67
00:05:40,650 --> 00:05:47,390
>> Okay, sure. First of all, I should say thank you to Scott for teaching this fabulous MOOC.

68
00:05:47,390 --> 00:05:50,900
There are many, many, many MOOCs, as you know, but there are very few 

69
00:05:50,900 --> 00:05:55,810
who have a professor who's as caring and, and, and knows as much 

70
00:05:55,810 --> 00:06:00,000
about the length and breadth of social psychology, who's doing it.

71
00:06:00,000 --> 00:06:06,950
So, I just want to say thank you to Scott, because, just like with the Wikipedia project,

72
00:06:06,950 --> 00:06:12,050
any attempt to get these ideas into the hands of the most number of people

73
00:06:12,050 --> 00:06:16,620
is the best way we have of changing the world. So, thank you to Scott.

74
00:06:16,620 --> 00:06:23,900
So, just to say a little about implicit things ("implicit" here being the opposite of 

75
00:06:23,900 --> 00:06:30,500
the word "explicit"), we all know the kinds of thoughts that we have in our minds, 

76
00:06:30,500 --> 00:06:36,430
that we have some contact with. So, if you ask me, you know, who, you know, 

77
00:06:36,430 --> 00:06:40,800
which movie I recently saw that I liked, I can tell you about that.

78
00:06:40,800 --> 00:06:45,350
These are easy in some ways for us, because we can grab onto whatever

79
00:06:45,350 --> 00:06:50,470
is in our minds, and we can talk about them with other people.

80
00:06:50,470 --> 00:06:55,390
But, most of us in psychology and in the neurosciences these days

81
00:06:55,390 --> 00:07:00,450
believe that a large part of the way that our minds work is without our 

82
00:07:00,450 --> 00:07:06,100
conscious awareness, and to a great extent when we see harm in the world, 

83
00:07:06,100 --> 00:07:11,900
what we need to do is to begin to ask, "In what ways can good people be doing harm?" 

84
00:07:11,900 --> 00:07:16,200
And I'm sure this has been a big theme in the course so far, 

85
00:07:16,200 --> 00:07:22,300
as Scott would have taught it to you. So, my little part of work has involved 

86
00:07:22,300 --> 00:07:26,210
trying to get at that aspect of our minds, the part of our minds that 

87
00:07:26,210 --> 00:07:32,640
we don't easily have any access to, but that might be affecting our behavior.

88
00:07:32,640 --> 00:07:38,160
So, a simple example might be some of the doctors that we studied in Boston.

89
00:07:38,160 --> 00:07:43,800
These are wonderful people. They care about the poor. They work in a hospital 

90
00:07:43,800 --> 00:07:47,900
that takes care of people who do not have health insurance, and so on.

91
00:07:47,900 --> 00:07:52,200
So, we would say they're doing really wonderful good work.

92
00:07:52,200 --> 00:07:58,040
And yet, what we see is that even in these wonderful doctors, that,

93
00:07:58,040 --> 00:08:02,000
when they take our test -- the test that you may have learned about, 

94
00:08:02,000 --> 00:08:08,700
the Implicit Association Test, in which we measure how quickly people can associate 

95
00:08:08,700 --> 00:08:14,620
things that are good (words like love and peace and joy and friendship --

96
00:08:14,620 --> 00:08:20,000
those kinds of words) with two groups -- White and Black -- and what we find 

97
00:08:20,000 --> 00:08:26,240
is that the doctors who show a difficulty in associating good things 

98
00:08:26,240 --> 00:08:31,890
with dark-skinned people, or Black Americans, those doctors also

99
00:08:31,890 --> 00:08:37,500
don't prescribe the right medication to dark-skinned patients.

100
00:08:37,500 --> 00:08:42,020
And this is, of course, is very shocking to them because they never intended to do this.

101
00:08:42,020 --> 00:08:47,190
They intend to treat everybody equally. And so we become interested 

102
00:08:47,190 --> 00:08:52,060
in these because we now have to set a higher bar for ourselves, right?

103
00:08:52,060 --> 00:08:57,170
It's not good enough, we say, to just be consciously unbiased --

104
00:08:57,170 --> 00:09:05,880
we need to reveal ourselves fully. And once we reveal ourselves to ourselves,

105
00:09:05,880 --> 00:09:07,980
maybe we'll make different kinds of decisions.

106
00:09:07,980 --> 00:09:12,300
So, that's at the heart of unconscious bias, and it comes from the blindspots

107
00:09:12,300 --> 00:09:18,960
that are in our minds, just like there are blindspots in our eye. Is that a good start?

108
00:09:18,960 --> 00:09:24,000
>> Yeah, that's great. That really sets the stage beautifully, I think, and so I'm sure, 

109
00:09:24,000 --> 00:09:28,190
I'm sure that our, our students would have questions for you.

110
00:09:28,190 --> 00:09:35,550
>> I wanted to ask, because when I took the Implicit Association Test,

111
00:09:35,550 --> 00:09:38,740
as soon as I finished it -- I mean, at least the first one --

112
00:09:38,740 --> 00:09:43,780
I couldn't stop thinking at Benjamin Libet's experiment 30 years ago.

113
00:09:43,780 --> 00:09:48,690
I mean, now that I'm aware, because I took the test, how can I handle that because

114
00:09:48,690 --> 00:09:53,200
it is encoded in my neurons? I mean, I don't know how to handle it.

115
00:09:53,200 --> 00:09:57,100
>> It's a great question, and I think about it a lot. 

116
00:09:57,100 --> 00:10:04,500
Um, and I try to think about solutions, which you're already jumping right to the solution.

117
00:10:04,500 --> 00:10:10,060
Libet did some studies in which he showed that a few milliseconds

118
00:10:10,060 --> 00:10:16,460
before you have the experience, "I want to eat this chocolate cake,"

119
00:10:16,460 --> 00:10:21,030
your brain had already made that decision. It's just that we didn't know that, right?

120
00:10:21,030 --> 00:10:25,000
So, what Libet showed was that there was a time difference between 

121
00:10:25,000 --> 00:10:30,310
a brain signal that showed that you had already made up your mind 

122
00:10:30,310 --> 00:10:33,310
and before you actually could subjectively experience it.

123
00:10:33,310 --> 00:10:39,690
So, the point that's being raised is -- with the IAT, you see automatic kinds of associations, 

124
00:10:39,690 --> 00:10:45,000
with Libet's studies, you see that the brain is, you know, a step faster 

125
00:10:45,000 --> 00:10:49,500
earlier than our experience of it is -- all of this leads to the question, 

126
00:10:49,500 --> 00:10:54,900
"So, what should we do about the fact that we don't even know what we're feeling?"

127
00:10:54,900 --> 00:11:01,000
So, I would say that it's very exciting to actually be living at a time when we have 

128
00:11:01,000 --> 00:11:08,000
the technologies to be able to look at what our unconscious minds are doing. Right?

129
00:11:08,000 --> 00:11:12,000
So, imagine if you went back to the earlier days of medicine, 

130
00:11:12,000 --> 00:11:15,050
long before we knew what was happening inside our bodies.

131
00:11:15,050 --> 00:11:18,600
You know, people thought that thought is happening in the stomach.

132
00:11:18,600 --> 00:11:23,150
Some people had a view that thinking happens in our feet. Right?

133
00:11:23,150 --> 00:11:27,200
So, before we could actually look at the body, we had all these wrong theories

134
00:11:27,200 --> 00:11:34,390
about how our bodies work. But now, the body is no longer, um, you know, unknown to us,

135
00:11:34,390 --> 00:11:39,630
we know exactly what our bodies do, but if I said to you, all eight of you,

136
00:11:39,630 --> 00:11:45,600
"Tell me, what is your pancreas doing right now?" you wouldn't know what it's doing.

137
00:11:45,600 --> 00:11:48,600
You may have a small sense of your heart beating.

138
00:11:48,600 --> 00:11:51,200
You may have some sense of your stomach a little bit, 

139
00:11:51,200 --> 00:11:53,850
but you don't know what you're liver is doing or whatever, right?

140
00:11:53,850 --> 00:11:58,520
So, I think if we start to think about our minds like that, and to think that

141
00:11:58,520 --> 00:12:03,250
once we know what is going on in our mind -- even at levels that are 

142
00:12:03,250 --> 00:12:08,300
not available to conscious awareness -- then we can begin to find solutions to

143
00:12:08,300 --> 00:12:12,540
the problems, much like in medicine we are doing with diseases. Right?

144
00:12:12,540 --> 00:12:19,150
So, just as with a disease -- a bad disease even like cancer -- the number of ways in

145
00:12:19,150 --> 00:12:24,800
which we can now control that disease only come about because we can understand it.

146
00:12:24,800 --> 00:12:30,970
So, my first answer to you is that there is an enormous benefit to simply knowing 

147
00:12:30,970 --> 00:12:37,300
what's going on, because you as individuals, once you realize that you are doing things 

148
00:12:37,300 --> 00:12:43,520
that are harmful to other people, or even to yourself -- are you costing yourself something?

149
00:12:43,520 --> 00:12:48,540
Are you thinking, "Oh, I must work for, um… what was it one of you said, 

150
00:12:48,540 --> 00:12:53,980
that you're thinking of shifting from one kind of a company to an NGO?

151
00:12:53,980 --> 00:12:59,000
And I think Professor Plous is possibly responsible for this shift 

152
00:12:59,000 --> 00:13:02,940
in your being a happier person for the rest of your life.

153
00:13:02,940 --> 00:13:08,100
Well, how did you make that decision, right, if only by making yourself aware 

154
00:13:08,100 --> 00:13:11,200
of the fact that there are alternatives. So, I'm here to say that 

155
00:13:11,200 --> 00:13:16,700
the first thing we can do is just by being aware, we will change our behavior.

156
00:13:16,700 --> 00:13:21,550
Even if your mind doesn't change, you can change your behavior.

157
00:13:21,550 --> 00:13:27,640
>> Hi, um, okay, so, over here, I'm in San Francisco right now, and apparently in the U.S., 

158
00:13:27,640 --> 00:13:34,230
the tech industry has, like, for engineering and programming, there are a lot less women.

159
00:13:34,230 --> 00:13:41,000
So, it's like around 10 to 14%, but in other countries, um, like the Philippines, for example, 

160
00:13:41,000 --> 00:13:48,300
I've seen, like, at least 30 or 40% in the workforce, who are in tech positions 

161
00:13:48,300 --> 00:13:53,850
like programmers, DBAs, systems administrators. So, there is a big disparity,

162
00:13:53,850 --> 00:14:00,200
and I was wondering if it had something to do with the collectivist societies in Asia,

163
00:14:00,200 --> 00:14:06,500
or, I don't know, um, I was wondering if you had any, um, insights regarding that or

164
00:14:06,500 --> 00:14:14,510
anything, any findings from your research. >> Yeah. You know, this idea that, um,

165
00:14:14,510 --> 00:14:20,720
male is for math and engineering and technology is an idea that,

166
00:14:20,720 --> 00:14:26,540
at least in the United States, people often believe is a genetic difference.

167
00:14:26,540 --> 00:14:30,900
People like the president of my university have made the claim that

168
00:14:30,900 --> 00:14:34,980
men are just better at math, and so on, and certainly if you look around --

169
00:14:34,980 --> 00:14:36,700
>> Former president!

170
00:14:36,700 --> 00:14:40,930
>> Former president of my university said that.

171
00:14:40,930 --> 00:14:44,100
And so, you know, there is a strong belief, and I would always point out that 

172
00:14:44,100 --> 00:14:49,690
in South India, you know, it didn't matter. All of us, boys, girls we all had to do math.

173
00:14:49,690 --> 00:14:56,000
There was never any sense and so, I've often taken that kind of cultural variation 

174
00:14:56,000 --> 00:14:59,650
to argue that it cannot be genetic, because if it's genetic,

175
00:14:59,650 --> 00:15:03,950
you should see it equally represented around the world, right?

176
00:15:03,950 --> 00:15:07,970
Because, around the world, women can have babies and men cannot.

177
00:15:07,970 --> 00:15:13,800
That's genetic. Okay? But if in Russia, women are becoming doctors in large numbers,

178
00:15:13,800 --> 00:15:18,320
and in other countries women are participating in technology, and other places they're not, 

179
00:15:18,320 --> 00:15:23,150
then we have to believe that there must be something cultural going on.

180
00:15:23,150 --> 00:15:26,970
And I think in this regard, you're absolutely right.

181
00:15:26,970 --> 00:15:33,790
I'm very worried about this in the United States, um, because in an earlier generation,

182
00:15:33,790 --> 00:15:37,300
there were actually more women in the technology world.

183
00:15:37,300 --> 00:15:42,400
So, when computer science first emerged as a separate discipline, 

184
00:15:42,400 --> 00:15:46,000
there were many, many women in it. Do you know why?

185
00:15:46,000 --> 00:15:48,070
Because they could type. They were typists.

186
00:15:48,070 --> 00:15:51,900
See, they had become secretaries and stuff, and because they could type, and

187
00:15:51,900 --> 00:15:58,400
computers involve typing, they were seen as really good at being able to do that.

188
00:15:58,400 --> 00:16:02,280
So, for a couple of generations, there were actually many more women

189
00:16:02,280 --> 00:16:06,260
in computer science, and then over the years, it has become identified more and

190
00:16:06,260 --> 00:16:11,080
more with engineering and so on, and as a result, women have dropped out.

191
00:16:11,080 --> 00:16:16,570
So, do you know this book that was written a little while ago called "Lean In" by

192
00:16:16,570 --> 00:16:24,130
Sheryl Sandberg, who is the COO of Facebook? And she wrote this book called "Lean In,"

193
00:16:24,130 --> 00:16:31,000
asking that women should demand a place at the table, that they should not lean out --

194
00:16:31,000 --> 00:16:35,022
that they should lean in, and ask to be taken seriously.

195
00:16:35,022 --> 00:16:39,780
>> Marzu, could you talk a little bit about your own personal story of how you leaned in?

196
00:16:39,780 --> 00:16:46,100 
I mean, it took many years, but maybe from where you grew up to where you are now? >>Yeah.

197
00:16:46,100 --> 00:16:51,830
>> You know, life for all of us is a big series of accidents. Luck plays a very important role.

198
00:16:51,830 --> 00:16:56,000
You end up in a certain place, and you think, "I did it," when in fact, 

199
00:16:56,000 --> 00:16:59,610
dozens of other people, from your mother or a cousin or somebody 

200
00:16:59,610 --> 00:17:04,450
who gave you a book at some point suddenly changes everything for you.

201
00:17:04,450 --> 00:17:09,500
And so, I'm very aware of the large number of people who made this happen, 

202
00:17:09,500 --> 00:17:13,700
but I want to give you an example of my not having leaned in,

203
00:17:13,700 --> 00:17:19,120
and I'll tell you how much now I think that that was obviously the wrong thing.

204
00:17:19,120 --> 00:17:22,700
So, first of all let me give you an example of something that happened 

205
00:17:22,700 --> 00:17:29,150
very recently in my own department. We were running a search for a professor,

206
00:17:29,150 --> 00:17:35,800
and we noticed that a lot of people who were really good had not applied for the job.

207
00:17:35,800 --> 00:17:41,000
So, we asked them. It turned out we all, we could just, off the top of our heads, 

208
00:17:41,000 --> 00:17:45,700
without even thinking, we could generate names of six women 

209
00:17:45,700 --> 00:17:49,060
who should have applied for our job, but who did not apply.

210
00:17:49,060 --> 00:17:53,590
So, we called them, and asked them, "Why didn't you apply for the job?"

211
00:17:53,590 --> 00:17:56,170
And all of them said exactly the same thing.

212
00:17:56,170 --> 00:18:00,440
They said, "I didn't think I was good enough for Harvard,"

213
00:18:00,440 --> 00:18:02,510
all of them in different ways.

214
00:18:02,510 --> 00:18:06,110
One of them even said, your job advertisement said that you are looking 

215
00:18:06,110 --> 00:18:11,880
for people who are "exceptional," and I didn't think I was exceptional.

216
00:18:11,880 --> 00:18:13,960
The same woman had applied to Stanford, you know,

217
00:18:13,960 --> 00:18:17,900
but she didn't apply here because our job said "exceptional."

218
00:18:17,900 --> 00:18:23,020
So I, my first response to that was one of real panic. I thought, "Oh my God,

219
00:18:23,020 --> 00:18:25,900
you know, all the best people are not even applying for our job!

220
00:18:25,900 --> 00:18:29,340
What are we going to do?" Because you can't make people apply.

221
00:18:29,340 --> 00:18:31,370
And then I got annoyed at these young women.

222
00:18:31,370 --> 00:18:35,230
I said, what can I, what can we do? You know, we advertise a job.

223
00:18:35,230 --> 00:18:38,150
It's, it's your responsibility to apply for it.

224
00:18:38,150 --> 00:18:42,318
If you don't apply for the job, how can we offer you the job?

225
00:18:42,318 --> 00:18:47,000
>> And then I remembered, and then I remembered that, I don't know,

226
00:18:47,000 --> 00:18:52,735
27 years ago, I did not apply for the job that I first took.

227
00:18:52,735 --> 00:18:58,630
Scott knows my first job was at Yale University, and I did not apply for that job.

228
00:18:58,630 --> 00:19:04,440
My husband applied. He sent my CV, he sent my résumé in

229
00:19:04,440 --> 00:19:08,290
to Yale because I had said, "I don't think I'm good enough."

230
00:19:08,290 --> 00:19:12,000
Right now, 30 years later or 40 years later, whatever, that obviously 

231
00:19:12,000 --> 00:19:13,850
was the wrong thing to have done. You know what, you know, 

232
00:19:13,850 --> 00:19:16,872
if I did not have a husband who had thought about that,

233
00:19:16,872 --> 00:19:21,420
I wouldn't have had these amazing jobs that I've had.

234
00:19:21,420 --> 00:19:26,220
And so, I'm here to tell all of you here, no matter whether you're male or female,

235
00:19:26,220 --> 00:19:33,130
you know, whatever your ethnicity is, don't cost yourself in those early stages.

236
00:19:33,130 --> 00:19:37,350
Just apply. It doesn't matter -- maybe you won't get the job,

237
00:19:37,350 --> 00:19:42,840
but it shouldn't be your decision. Just apply for anything that looks remotely

238
00:19:42,840 --> 00:19:46,940
good enough that you would be interested in, and don't worry about the rest.

239
00:19:48,730 --> 00:19:56,780
>> Lean in. >> Lean in, yes. Or marry a feminist man!

240
00:19:56,780 --> 00:20:00,330
>> And how about the story of how you came through from

241
00:20:00,330 --> 00:20:05,140
your upbringing to social psychology? >> Yeah, that's a, that's a very interesting --

242
00:20:05,140 --> 00:20:10,520
I think that we have one person from India, somebody, I think, is it Sumit?

243
00:20:10,520 --> 00:20:16,005
I don't know if he is on, but yeah, um, so the…. >> Yes, yes. >> Yes, okay, well, 

244
00:20:16,005 --> 00:20:20,700
my interest, I was, Sumit, I was born and raised in Hyderabad, and --

245
00:20:20,700 --> 00:20:27,980
>> Okay. >> I was, I was, I spent two years, um, at JNU (a college in New Delhi),

246
00:20:27,980 --> 00:20:32,570
and I was, I was not at all certain of what I wanted to do. You know,

247
00:20:32,570 --> 00:20:37,195
smart people in India are supposed to be in mathematics or physics or engineering,

248
00:20:37,195 --> 00:20:41,470
and that -- I didn't want to do that, but there didn't seem to be any alternatives.

249
00:20:41,470 --> 00:20:45,000
Anyway, on the way back on a train -- as you know, not all of you will know, 

250
00:20:45,000 --> 00:20:50,000
but train stations in India are very big things, like people will get off the train, 

251
00:20:50,000 --> 00:20:54,780
and there are shops, and certainly there was a book store at one of these train stations,

252
00:20:54,780 --> 00:21:00,150
when I got off the train going from Delhi to Hyderabad, and in the bookstore 

253
00:21:00,150 --> 00:21:08,400
was a five-volume set of "The Handbook of Social Psychology," published in 1968,

254
00:21:08,400 --> 00:21:12,930
and the only reason I bought them was because they were so cheap.

255
00:21:12,930 --> 00:21:18,790
I bought them -- all five of them -- and I read the first volume,

256
00:21:18,790 --> 00:21:23,360
and one of the first chapters was by a real hero of Scott's and mine 

257
00:21:23,360 --> 00:21:29,840
by the name of Elliot Aronson. And he had written a chapter there, on methodology.

258
00:21:29,840 --> 00:21:32,360
Who can imagine a more boring chapter, you know?

259
00:21:32,360 --> 00:21:35,800
And yet, I was completely mesmerized by this.

260
00:21:35,800 --> 00:21:38,870
I thought, "Wow, who are these people who actually take the 

261
00:21:38,870 --> 00:21:42,640
real social world, and they bring it into the laboratory,

262
00:21:42,640 --> 00:21:47,340
and they do these really precise experiments on social relations?"

263
00:21:47,340 --> 00:21:53,040
And the answer was, in America. So I packed my bags, and I came.

264
00:21:53,040 --> 00:21:55,770
My bag -- I brought one suitcase. Most of it was filled with 

265
00:21:55,770 --> 00:21:59,125
the five volumes of the "Handbook of Social Psychology,"

266
00:21:59,125 --> 00:22:03,000
one pair of jeans, and two shirts. And I came with, I think,

267
00:22:03,000 --> 00:22:09,520
about $70 in my pocket, and I knew nobody. I knew not a soul in the country.

268
00:22:09,520 --> 00:22:18,110
I just landed in Columbus, Ohio, and I guess the rest of my life is history, yeah.

269
00:22:18,110 --> 00:22:26,000
>> That's extreme leaning in. >> Extreme lean in. Always buy cheap books!

270
00:22:26,000 --> 00:22:31,020
>> Other questions? >> Hello, Dr. Banaji? >> Yes, I have one.

271
00:22:31,020 --> 00:22:36,240
>> Yes? >> Um, what do you think about the importance of the media --

272
00:22:36,240 --> 00:22:39,570
>> Oh. >> and the, and the human being.

273
00:22:39,570 --> 00:22:43,080
>> You know, your question is so, so great.

274
00:22:43,080 --> 00:22:48,000
People often talk about the fact that there is now so much media, and they worry about it.

275
00:22:48,000 --> 00:22:54,100
But I'm very excited about it, because it now, it means that now I have both

276
00:22:54,100 --> 00:22:59,800
the opportunity and the responsibility of shaping which media I listen to. 

277
00:22:59,800 --> 00:23:05,010
Right? I can set up all the defaults -- all the automatic stuff on my computer,

278
00:23:05,010 --> 00:23:10,000
on my TV, of which shows it will tape or not tape, so I can now determine, 

279
00:23:10,000 --> 00:23:14,600
to a large extent, what my brain is being fed. I don't have to depend 

280
00:23:14,600 --> 00:23:19,120
on what the media out there is telling me. I can make my own media.

281
00:23:19,120 --> 00:23:23,740
I can make my own path, picking the things that I think are important.

282
00:23:23,740 --> 00:23:26,360
Sometimes there are things that I don't agree with. Right?

283
00:23:26,360 --> 00:23:30,390
So, I need to know why people think a certain way, so I need to visit 

284
00:23:30,390 --> 00:23:33,830
new sources of a kind where I may not agree with what they say,

285
00:23:33,830 --> 00:23:38,000
but I need to listen to what they're saying. But I can shape my own media, 

286
00:23:38,000 --> 00:23:41,970
and so I would say one of the most important things that we can all do 

287
00:23:41,970 --> 00:23:46,150
is not passively just receive whatever media is being sent to us, 

288
00:23:46,150 --> 00:23:50,720
but to shape it in some way, so that what comes into your inbox,

289
00:23:50,720 --> 00:23:56,570
and what you end up setting as your defaults, are very precisely chosen by you,

290
00:23:56,570 --> 00:24:00,870
so that you are shaping what you hear, and what you listen to.

291
00:24:00,870 --> 00:24:03,630
>> And that also goes to the earlier question about what to do 

292
00:24:03,630 --> 00:24:08,420
concerning implicit bias -- >> Yes. >> because if you're exposing yourself

293
00:24:08,420 --> 00:24:13,350
to things that associate certain groups with good, or certain groups with bad, 

294
00:24:13,350 --> 00:24:18,100
that will have an effect simply by exposure over time.

295
00:24:18,100 --> 00:24:20,520
>> "Simply by exposure" is a very powerful phrase.

296
00:24:20,520 --> 00:24:24,290
Sometimes you think, "Oh, I just saw a picture on a billboard."

297
00:24:24,290 --> 00:24:28,110
Well, that picture can make a big difference, because it may 

298
00:24:28,110 --> 00:24:31,930
remind you of certain things that otherwise would not be possible.

299
00:24:31,930 --> 00:24:34,800
So, one of the things that I have done -- sometimes people ask me, "Now that you

300
00:24:34,800 --> 00:24:39,130
know about your own implicit bias, Mahzarin, what are you doing about it?"

301
00:24:39,130 --> 00:24:43,650
And one of the things that I do do is, I make a screensaver for myself.

302
00:24:43,650 --> 00:24:50,000
Every few months, I scan in certain new images that just rotate through in front of me. 

303
00:24:50,000 --> 00:24:54,500
Right? I mean, most of us have the sad life that we look at a computer monitor

304
00:24:54,500 --> 00:24:57,700
many more hours a day than we look at even other human beings. Right?

305
00:24:57,700 --> 00:25:02,290
I certainly sit in front of a computer for many, many hours a day.

306
00:25:02,290 --> 00:25:07,800
But I don't want to lose the opportunity of using that device to teach 

307
00:25:07,800 --> 00:25:12,600
my brain certain things that I don't have the time to do because I'm busy.

308
00:25:12,600 --> 00:25:18,000
I will be affected by whatever is in front of me. So if I choose what will rotate --

309
00:25:18,000 --> 00:25:22,300
I'll give you one example. A picture that I have on my screen that rotates through 

310
00:25:22,300 --> 00:25:28,400
every few days is a picture of a woman. She is a construction worker. Okay?

311
00:25:28,400 --> 00:25:33,420
So, she's wearing jeans and heavy boots and a hard hat -- you know,

312
00:25:33,420 --> 00:25:40,000
a yellow-colored hard hat -- and she is feeding her baby on lunch break.

313
00:25:40,000 --> 00:25:43,040
Now think about it, okay? Now think about it.

314
00:25:43,040 --> 00:25:45,550
When I say to you "construction worker," you know, 

315
00:25:45,550 --> 00:25:48,200
what's the image that comes to your mind? Not this one. 

316
00:25:48,200 --> 00:25:55,400
But my hope is that if you see this, it becomes a possibility and, you know, 

317
00:25:55,400 --> 00:25:59,670
I cannot point out a study out to you, but I'd like to show some day that

318
00:25:59,670 --> 00:26:04,500
when you see more images like that that, that it actually loosens up enough

319
00:26:04,500 --> 00:26:08,590
in your mind that now you'll be able to imagine all sorts of things.

320
00:26:08,590 --> 00:26:12,500
You'll be able to imagine a short man as the president of the country,

321
00:26:12,500 --> 00:26:16,080
which is apparently nearly impossible in the United States.

322
00:26:16,080 --> 00:26:19,242
If you are short, you cannot be the President. Right?

323
00:26:19,242 --> 00:26:22,650
In every election, the slightly taller person wins.

324
00:26:22,650 --> 00:26:26,520
So, you don't want, you don't want a country where your height is going to

325
00:26:26,520 --> 00:26:30,300
determine whether you get a job or not. It should be your brain.

326
00:26:30,300 --> 00:26:32,890
It should be your ideas. It should be your values.

327
00:26:32,890 --> 00:26:38,200
So, so I think these are just little ways in which we can feed our brains the right stuff.

328
00:26:38,200 --> 00:26:43,710
So, if you think about media in the same way as you think about food --

329
00:26:43,710 --> 00:26:47,755
so if you put healthy food into your body, you'll have a healthier body --

330
00:26:47,755 --> 00:26:53,090
and the media really is like the "food" for the brain, and I think that's the way to

331
00:26:53,090 --> 00:26:58,650
think about it, just as you would want to eat organic good food, um,

332
00:26:58,650 --> 00:27:05,310
"What's the equivalent of that when you think about information?" is the question.

333
00:27:05,310 --> 00:27:09,810
>> Great. >> Yeah, I am Meila from Jakarta, Indonesia.

334
00:27:09,810 --> 00:27:15,820
I was taking a look at the Project Implicit task website on the social attitude task,

335
00:27:15,820 --> 00:27:20,156
and I noticed that there is no Indonesia listed there. >> Yeah.

336
00:27:20,156 --> 00:27:23,980
>> How do you see bias in Indonesia?

337
00:27:23,980 --> 00:27:28,400
>> Yeah. Ah, we have to be very careful in how we expand the website, um...

338
00:27:28,400 --> 00:27:32,370
For example, it took us years to create a Brazil website.

339
00:27:32,370 --> 00:27:37,140
You know why? Because we realized that we can make the tests up, 

340
00:27:37,140 --> 00:27:42,850
but without knowledge about that culture, we would do a terrible job of even

341
00:27:42,850 --> 00:27:48,530
posing the right questions about what ethnicity do you belong to, and so on.

342
00:27:48,530 --> 00:27:54,740
So, we only create sites when we can have a collaborator from that country

343
00:27:54,740 --> 00:28:01,735
who is willing to collaborate with us to do it, and so we just wait until we find somebody,

344
00:28:01,735 --> 00:28:07,790
and we say, could you please, you know, work with us on the translation, and so on.

345
00:28:07,790 --> 00:28:12,150
So, if you know of a good psychologist who is from Jakarta 

346
00:28:12,150 --> 00:28:17,975
and who is willing to collaborate with us, we will do it.

347
00:28:19,448 --> 00:28:24,500
>> There you go -- some new ground broken today! >> Yeah.

348
00:28:24,500 --> 00:28:28,490
>> Before we go, Marzu, there's one thing that we haven't talked about, 

349
00:28:28,490 --> 00:28:32,330
and that is your book, so maybe we could close, just say a few words, 

350
00:28:32,330 --> 00:28:36,200
because I think that a lot of students might be interested in learning more about this. 

351
00:28:36,200 --> 00:28:41,600
So, maybe, what is in the book or what led you to write the book -- some background.

352
00:28:41,600 --> 00:28:45,760
>> Yeah. Right. You know, as Scott may have already told you, 

353
00:28:45,760 --> 00:28:48,970
you know, people in my field, we don't write books much.

354
00:28:48,970 --> 00:28:54,480
We write articles that go into research journals, and then they are read by

355
00:28:54,480 --> 00:28:58,550
you know, five other people besides ourselves, if we're lucky.

356
00:28:58,550 --> 00:29:05,270
And so, the book was written as a way of giving people outside our field --

357
00:29:05,270 --> 00:29:08,700
the book is really useless for Scott, because Scott already knows all this.

358
00:29:08,700 --> 00:29:14,820
But for anybody who is not from the field, the book is almost like a manual.

359
00:29:14,820 --> 00:29:19,510
It's not boring like a manual, but it basically tells you all the steps of how

360
00:29:19,510 --> 00:29:24,370
we made these discoveries, how we came to see that we ourselves were biased,

361
00:29:24,370 --> 00:29:29,670
even though we have not a shred of bias in our own minds consciously, and still,

362
00:29:29,670 --> 00:29:37,130
I, too, was showing bias in the White-Black case, in the male-female case, and so on.

363
00:29:37,130 --> 00:29:43,900
And so, that led us to write the book in order to get this idea out to large groups of people.

364
00:29:43,900 --> 00:29:49,460
And so, you know, the price is falling on Amazon, even as we are talking.

365
00:29:49,460 --> 00:29:52,800
So, yeah, if you, if you're interested, please feel free to read it.

366
00:29:52,800 --> 00:29:59,000
I am always kind of shy to push the book, but I think it's the best description of

367
00:29:59,000 --> 00:30:05,550
what we talked about today, with a lot more examples and a lot more of the research.

368
00:30:05,550 --> 00:30:10,280
>> It's a fun book. It's a fun read. I enjoyed it. I did get a lot of out of it.

369
00:30:10,280 --> 00:30:14,730
>> Thank you. >> Thank you so much for your time!

370
00:30:14,730 --> 00:30:18,725
Everybody, I guess we can virtually give a round of applause.

371
00:30:18,725 --> 00:30:23,710
>> Aw, no -- it's great to talk with you all.

372
00:30:23,710 --> 00:30:26,660
Have a wonderful rest of the semester -- how many more weeks?

373
00:30:26,660 --> 00:30:30,440
Or is this just the start, or the end? >> Just one last week of material.

374
00:30:30,440 --> 00:30:35,580
>> Oh, fabulous! Well, good luck to all of you.

375
00:30:35,580 --> 00:30:36,808
>> Thank you! >> Thank you! >> Thank you!

376
00:30:36,808 --> 00:30:38,638
>> Thank you for participating as well.

377
00:30:38,638 --> 00:30:40,038
>> Thank you all, thank you all!

