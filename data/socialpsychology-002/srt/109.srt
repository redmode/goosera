1
00:00:14,067 --> 00:00:16,650
Over the last couple of weeks, the lectures and readings 

2
00:00:16,650 --> 00:00:19,800
have focused on peace and social justice.

3
00:00:19,800 --> 00:00:25,000
Yet for most people, the desire for peace and social justice arises from something

4
00:00:25,000 --> 00:00:30,200
that social psychologists haven't really studied much until recently: compassion.

5
00:00:30,200 --> 00:00:34,600
In English, the word compassion comes from Latin, and means, literally, 

6
00:00:34,600 --> 00:00:40,300
"to suffer with." The last part of the word is related in origin to patient -- 

7
00:00:40,300 --> 00:00:45,030
that is, "one who suffers," and if there's any doubt about the importance of

8
00:00:45,030 --> 00:00:49,810
compassion, all we need to do is imagine living in a world that doesn't have it.

9
00:00:51,020 --> 00:00:52,920
That's not a world I'd want to live in.

10
00:00:52,920 --> 00:00:55,100
What we need -- I think most of us would agree --

11
00:00:55,100 --> 00:00:59,000
is more compassion, as well as a better understanding of it.

12
00:00:59,000 --> 00:01:02,100
Here, for example, are a few questions we might ask.

13
00:01:02,100 --> 00:01:06,460
First, who tends to be most compassionate, and why?

14
00:01:06,460 --> 00:01:10,430
For example, does compassion increase as people age?

15
00:01:10,430 --> 00:01:12,850
Is compassion something that can be taught,

16
00:01:12,850 --> 00:01:16,700
and if so, what are the most effective ways to do it?

17
00:01:16,700 --> 00:01:21,020
Is compassion different at the individual and group level?

18
00:01:21,020 --> 00:01:27,620
How does compassion vary across cultures? How does it vary across generations?

19
00:01:27,620 --> 00:01:34,800
And possibly most important of all, what are the factors that increase or decrease compassion?

20
00:01:34,800 --> 00:01:39,450
These questions are just beginning to receive serious research attention.

21
00:01:39,450 --> 00:01:44,600
For instance, in 2013, a study published in the journal Psychological Science

22
00:01:44,600 --> 00:01:49,500
found that after only two weeks of compassion training, people showed changes

23
00:01:49,500 --> 00:01:55,100
in brain activation in response to images of suffering and distress.

24
00:01:55,100 --> 00:02:01,200
And in 2013, the Association for Psychological Science ran a magazine cover story

25
00:02:01,200 --> 00:02:07,000
on compassion research, looking at things like the evolutionary role of compassion,

26
00:02:07,000 --> 00:02:12,500
and physical and psychological health benefits associated with compassionate behaviour.

27
00:02:12,500 --> 00:02:17,300
An interesting overlap with social psychology is that, since 1999, 

28
00:02:17,300 --> 00:02:22,200
I've ended the introductory Social Psychology class that I teach at Wesleyan 

29
00:02:22,000 --> 00:02:25,590
with an assignment called "The Day of Compassion" in which 

30
00:02:25,590 --> 00:02:30,100
I challenge students to live a full 24 hours as compassionately as possible, 

31
00:02:30,100 --> 00:02:34,500
and analyze the experience from a social psychological perspective,

32
00:02:34,500 --> 00:02:40,200
answering questions like the following: First, how did you define compassion?

33
00:02:40,200 --> 00:02:43,730
If your behavior was different than normal, who do you like more:

34
00:02:43,730 --> 00:02:47,230
the "Day of Compassion you" or the "normal you"?

35
00:02:47,230 --> 00:02:50,780
What are the costs and benefits of behaving compassionately?

36
00:02:50,780 --> 00:02:53,450
Do the benefits out weight the costs?

37
00:02:53,450 --> 00:02:58,730
How did others respond to your compassion? First off, did they notice any difference?

38
00:02:58,730 --> 00:03:03,500
And if so, what attributions did they make for your behavior?

39
00:03:03,500 --> 00:03:06,390
Finally, if you wanted to encourage others to behave as you did 

40
00:03:06,390 --> 00:03:10,330
during the Day of Compassion, what techniques would you use?

41
00:03:11,330 --> 00:03:15,900
Each year that students carry out this assignment, they tell me how interesting it was, 

42
00:03:15,900 --> 00:03:21,020
both in terms of social psychology and personal insights that they have about themselves.

43
00:03:21,020 --> 00:03:25,680
And each year, I'm incredibly impressed at how students rise to the challenge, 

44
00:03:25,680 --> 00:03:28,740
and find creative ways to behave compassionately.

45
00:03:28,740 --> 00:03:32,570
Here are just a few acts of compassion that they've taken over the years, 

46
00:03:32,570 --> 00:03:36,000
and what you can see is tremendous variety.

47
00:03:36,000 --> 00:03:38,900
Some of the acts involve people that the students know, 

48
00:03:38,900 --> 00:03:41,910
and others involve people they'll never meet.

49
00:03:41,910 --> 00:03:47,150
Some focus on humans, whereas others benefit animals and the environment.

50
00:03:47,150 --> 00:03:50,000
And as you might notice, some involve direct applications 

51
00:03:50,000 --> 00:03:53,530
of social psychology, which is always great to see.

52
00:03:54,540 --> 00:03:58,430
So, when thinking about this online course, two things occurred to me.

53
00:03:58,430 --> 00:04:01,700
First, it would be great to have a Day of Compassion involving 

54
00:04:01,700 --> 00:04:04,200
thousands of students from around the world.

55
00:04:04,200 --> 00:04:07,800
And second, there should be a Day of Compassion Award 

56
00:04:07,800 --> 00:04:11,600
for the most amazing and wonderful work that students submit.

57
00:04:11,600 --> 00:04:15,340
So, just to remind you of something I mentioned earlier in the course,

58
00:04:15,340 --> 00:04:18,850
this class will have a Day of Compassion Award,

59
00:04:18,850 --> 00:04:22,770
and the grand prize winner will be flown on an expense-paid trip

60
00:04:22,770 --> 00:04:26,260
to personally meet anthropologist Jane Goodall -- 

61
00:04:26,260 --> 00:04:29,700
the face of compassion to millions of people around the world, 

62
00:04:29,700 --> 00:04:34,670
and of course, to the chimpanzees she's working so hard to protect.

63
00:04:34,670 --> 00:04:39,140
This particular chimpanzee, named Wounda, is a great example.

64
00:04:39,140 --> 00:04:42,500
Wounda was close to death, but the Jane Goodall Institute 

65
00:04:42,500 --> 00:04:47,140
was able to nurse her back to health and release her into the wild.

66
00:04:47,140 --> 00:04:51,110
Here's a remarkable three-minute video that shows the sort of compassion

67
00:04:51,110 --> 00:04:53,030
that Jane Goodall has become famous for.

68
00:04:54,470 --> 00:04:57,864
>> Her name is Wounda. That means "close to die." >> Wounda.

69
00:04:57,864 --> 00:05:03,900
>> Wounda, she had very serious illness -- she was really bad --

70
00:05:03,900 --> 00:05:08,000
and now we are going to give her paradise on the island.

71
00:08:23,500 --> 00:08:27,558
>> In addition to protecting chimpanzees, the Jane Goodall Institute 

72
00:08:27,558 --> 00:08:32,000
also has a program known as "Roots & Shoots" that teaches 

73
00:08:32,000 --> 00:08:36,920
compassion to young people in over 100,000 groups around the globe.

74
00:08:36,920 --> 00:08:40,690
Again, let me share a brief video with you describing the program,

75
00:08:40,690 --> 00:08:44,700
and then I'll explain what it has to do with the Day of Compassion.

76
00:09:22,488 --> 00:09:26,791
>> Everybody laughed at me. You'll never do that. Africa's far away.

77
00:09:26,791 --> 00:09:31,000
You don't have any money, and you're a girl.

78
00:09:31,000 --> 00:09:34,207
But if you really want something, you have to never give up.

79
00:09:36,207 --> 00:09:41,152
It's what I'd always dreamed of.

80
00:09:46,170 --> 00:09:50,200
Roots & Shoots is about choosing to make the world a better place,

81
00:09:50,200 --> 00:09:56,181
choosing projects to help people, to help other animals, and to help the environment.

82
00:09:56,181 --> 00:09:59,000
Anybody can start a Roots & Shoots project.

83
00:09:59,000 --> 00:10:04,050
It's about rolling up your sleeves and getting out there and doing it.

84
00:10:04,050 --> 00:10:09,747
>> Dr. Jane Goodall is really inspiring. >> We'll be able to change the world.

85
00:10:09,747 --> 00:10:15,300
>> The most important message for Roots & Shoots is that every individual matters,

86
00:10:15,300 --> 00:10:18,840
and every individual makes a difference, everyday.

87
00:10:18,840 --> 00:10:25,000
>> I want to be the next Jane. >> And I want to be, I be monkey.

88
00:10:34,000 --> 00:10:37,800
>> Now what does this program have to do with the Day of Compassion?

89
00:10:37,800 --> 00:10:42,700
Well, several things. First, the Roots & Shoots website contains a database 

90
00:10:42,700 --> 00:10:46,600
with hundreds of activities and projects that relate to compassion.

91
00:10:46,600 --> 00:10:50,500
So, if you want some ideas for things that you might do -- with kids 

92
00:10:50,500 --> 00:10:54,920
or with friends or family -- this is one place to start.

93
00:10:54,920 --> 00:11:00,000
Second, if you're interested, you can find a local Roots & Shoots group to join, 

94
00:11:00,000 --> 00:11:03,800
or even start one if your community doesn't already have a group.

95
00:11:03,800 --> 00:11:08,200
Third, if your Day of Compassion includes an activity or a project that 

96
00:11:08,200 --> 00:11:13,499
other people might try or find inspiring, the Jane Goodall Institute has said that 

97
00:11:13,499 --> 00:11:19,000
members of our class can enter their activities into the Roots & Shoots database.

98
00:11:19,670 --> 00:11:23,200
So, the work that you do during the Day of Compassion, has the potential 

99
00:11:23,200 --> 00:11:28,360
to live on and continue to do good long after the course has ended.

100
00:11:28,360 --> 00:11:32,700
Finally, as mentioned in an earlier lecture, Social Psychology Network will donate 

101
00:11:32,700 --> 00:11:37,500
funds to the Jane Goodall Institute as part of the Day of Compassion Award.

102
00:11:37,500 --> 00:11:40,000
So, by completing the assignment, you'll not only have 

103
00:11:40,000 --> 00:11:44,200
the chance to use social psychology to understand compassion, 

104
00:11:44,200 --> 00:11:48,000
but the award will contribute to programs that help other people -- 

105
00:11:48,000 --> 00:11:52,220
especially young people -- learn about compassion as well.

106
00:11:52,220 --> 00:11:56,080
Specifically, Social Psychology Network will donate $1,000

107
00:11:56,080 --> 00:12:00,300
to the Jane Goodall Institute, in honor of the winning student,

108
00:12:00,300 --> 00:12:05,200
$100 in the name of each student receiving Honorable Mention, 

109
00:12:05,200 --> 00:12:09,000
and $10 in the name of the first 800 other students 

110
00:12:09,000 --> 00:12:15,300
submitting the Day of Compassion assignment -- a combined total of $10,000.

111
00:12:15,300 --> 00:12:21,150
To be eligible, students have to complete at least four out of the five course assignments,

112
00:12:21,150 --> 00:12:25,000
score well enough in the course to receive a Statement of Accomplishment, 

113
00:12:25,000 --> 00:12:28,000
and get a large number of votes from other students 

114
00:12:28,000 --> 00:12:31,110
who read their Day of Compassion assignment.

115
00:12:31,110 --> 00:12:34,000
The selection process will have three stages.

116
00:12:34,000 --> 00:12:38,270
First, we'll identify all students who've completed four assignments and

117
00:12:38,270 --> 00:12:43,640
received a perfect or near-perfect score on the Day of Compassion assignment.

118
00:12:43,640 --> 00:12:48,225
Second, after excluding anyone who would rather not be considered for the award,

119
00:12:48,225 --> 00:12:52,730
we'll randomly assign roughly five finalists to every student in the class

120
00:12:52,730 --> 00:12:57,000
for you to vote on your favorites. That way even if they're 100 finalists,

121
00:12:57,000 --> 00:13:01,400
no one will have to read and vote on a huge number of essays.

122
00:13:01,400 --> 00:13:06,000
And then after the vote, the teaching staff will review the top 50 or so,

123
00:13:06,000 --> 00:13:10,000
and choose the grand prize winner and ten runners up, based on criteria 

124
00:13:10,000 --> 00:13:15,300
similar to what was listed in the Day of Compassion peer assessment.

125
00:13:15,300 --> 00:13:18,320
So, in conclusion, whether or not you're eligible for the award, 

126
00:13:18,320 --> 00:13:22,470
I hope that you'll take part in the assignment and the voting,

127
00:13:22,470 --> 00:13:25,510
learn about the dynamics of compassion in your own life, 

128
00:13:25,510 --> 00:13:29,110
and use the power of social psychology for the greater good.

