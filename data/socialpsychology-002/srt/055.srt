




1
00:00:06,880 --> 00:00:10,218
Welcome back. 
This lecture builds directly on the last 

2
00:00:10,218 --> 00:00:14,900
one, and is one of the most complicated 
and theoretical of the course. 

3
00:00:14,900 --> 00:00:19,250
It's all about twists and turns when it 
comes to how people explain behavior. 

4
00:00:19,250 --> 00:00:23,790
So, if you have trouble following any 
part of it, don't lose hope. 

5
00:00:23,790 --> 00:00:27,609
What I would suggest is to slow down the 
video playback speed, consider watching 

6
00:00:27,609 --> 00:00:31,143
the video a second time, or use the 
discussion forums to post any questions 

7
00:00:31,143 --> 00:00:36,386
that you have. 
Also, next week's readings will cover 

8
00:00:36,386 --> 00:00:40,710
attribution theory, including some of the 
research in this lecture. 

9
00:00:40,710 --> 00:00:45,100
So again, don't lose hope, stay with it, 
and if English isn't your first language, 

10
00:00:45,100 --> 00:00:50,860
let me simply remind you that we have a 
language help button on our course page. 

11
00:00:52,170 --> 00:00:55,900
So, in the last lecture we discussed 
Harold Kelley's formulation 

12
00:00:55,900 --> 00:00:59,560
of attribution theory. 
Remember, he proposed that people 

13
00:00:59,560 --> 00:01:04,100
typically explain behavior in terms of 
the person, entity, or time, 

14
00:01:04,100 --> 00:01:08,826
and that they base these attributions 
on three sources of information: 

15
00:01:08,826 --> 00:01:13,860
consensus, distinctiveness, and 
consistency. 

16
00:01:13,860 --> 00:01:17,400
For the most part, studies have supported 
this framework, 

17
00:01:17,400 --> 00:01:22,010
but there's one major exception: 
people don't always pay attention to 

18
00:01:22,010 --> 00:01:26,090
consensus information when they make 
causal attributions. 

19
00:01:26,090 --> 00:01:30,300
Some of the first researchers to document 
this problem were Dick Nisbett 

20
00:01:30,300 --> 00:01:34,031
and Gene Borgida, two social 
psychologists who argued that 

21
00:01:34,031 --> 00:01:38,189
when it comes to making attributions, 
there's lots of evidence that people 

22
00:01:38,189 --> 00:01:43,088
use distinctiveness information and 
consistency information, 

23
00:01:43,088 --> 00:01:48,300
but much less evidence that they take 
consensus information into account. 

24
00:01:48,500 --> 00:01:52,458
Nisbett and Borgida then conducted a very 
clever study that showed 

25
00:01:52,458 --> 00:01:57,400
the degree to which people sometimes 
ignore consensus information. 

26
00:01:58,390 --> 00:02:02,710
They told the participants in their study 
about two previously conducted 

27
00:02:02,710 --> 00:02:07,840
psychology experiments, both of which 
had yielded surprising results. 

28
00:02:07,840 --> 00:02:12,532
In one of the experiments, 32 out of 34 
people willingly received 

29
00:02:12,532 --> 00:02:16,990
electric shocks in an experiment 
supposedly on skin sensitivity. 

30
00:02:18,040 --> 00:02:21,636
And in the other experiment, 11 of 15 
people failed to help 

31
00:02:21,636 --> 00:02:26,810
someone who appeared to be having 
a seizure until the person began choking, 

32
00:02:26,810 --> 00:02:30,800
and 6 of 15 never helped the other 
person at all. 

33
00:02:30,800 --> 00:02:35,500
These are actual results. 
Now, some of Nisbett and Borgida's 

34
00:02:35,500 --> 00:02:38,726
participants were given the same 
consensus information that 

35
00:02:38,726 --> 00:02:44,050
I just gave you -- 
that is, 32 out of 34 people behaved in a certain way. 

36
00:02:44,050 --> 00:02:47,360
That was the consensus behavior in that situation. 

37
00:02:47,360 --> 00:02:49,114
Others were simply told about the 

38
00:02:49,114 --> 00:02:52,045
experimental procedure --  
they weren't give any consensus 

39
00:02:52,045 --> 00:02:55,489
information at all. 
Then Nisbett and Borgida asked their 

40
00:02:55,489 --> 00:03:01,150
participants several questions, including 
two key items: First, on a seven-point 

41
00:03:01,150 --> 00:03:06,150
scale, would you say that the behavior of 
a particular subject, 

42
00:03:06,150 --> 00:03:09,690
Bill, in the shock experiment, who 
willingly received the most extreme 

43
00:03:09,690 --> 00:03:14,600
shock, or Greg in the seizure experiment, 
who failed to help at all, was due to 

44
00:03:14,600 --> 00:03:18,750
that individual's personality or to the 
situation? 

45
00:03:18,800 --> 00:03:23,417
And second, how would you have behaved if 
you had been a  

46
00:03:23,417 --> 00:03:28,298
participant in the experiment? 
What Nisbett and Borgida found is that 

47
00:03:28,298 --> 00:03:33,640
giving people consensus information made 
no significant difference. 

48
00:03:33,640 --> 00:03:36,760
Even when people knew that the majority 
of participants in the original 

49
00:03:36,760 --> 00:03:42,200
experiments had received a shock or failed 
to help, they made dispositional 

50
00:03:42,200 --> 00:03:47,150
attributions for Bill and for Greg -- 
that is, attributions based on the 

51
00:03:47,150 --> 00:03:52,750
person's character or disposition. 
Consensus information also failed to 

52
00:03:52,750 --> 00:03:57,000
affect judgments of how people thought 
they would have acted 

53
00:03:57,000 --> 00:04:01,050
had they been in the original studies. 
Telling people that 32 out of 34 

54
00:04:01,050 --> 00:04:06,200
participants behaved a certain way made 
no significant difference in how people 

55
00:04:06,200 --> 00:04:10,150
thought they would behave. 
And the same pattern of results  

56
00:04:10,150 --> 00:04:16,380
was generated by our class in the Snapshot 
Quiz, which asked the following: 

57
00:04:16,380 --> 00:04:20,348
If you saw someone having a seizure (for 
example, someone fall to the floor and 

58
00:04:20,348 --> 00:04:25,270
begin shaking uncontrollably), what would 
you do? 

59
00:04:25,270 --> 00:04:29,100
Over 90% of you answered that you'd 
probably or definitely help, and I have 

60
00:04:29,100 --> 00:04:34,800
no doubt that many of you would. 
Let's pause so that you can see your answer.

61
00:04:37,500 --> 00:04:39,754
Where the results get interesting is in the 

62
00:04:39,754 --> 00:04:43,757
next snapshot quiz item, which gave 
you consensus information 

63
00:04:43,757 --> 00:04:48,150
that nearly three out of four people did 
not help in this situation. 

64
00:04:48,150 --> 00:04:52,359
Just as Nisbett and Borgida found, this 
extra information made relatively little 

65
00:04:52,359 --> 00:04:56,260
difference. 
The overwhelming majority of students 
in our class said, 

66
00:04:56,260 --> 00:05:01,415
in effect, "Well, I would have 
helped even if other people didn't." 

67
00:05:01,415 --> 00:05:04,970
Again, let's pause so that you can see 
your answer. 

68
00:05:08,050 --> 00:05:11,200
Now, of course, the pattern that I'm 
describing doesn't fit everyone, 

69
00:05:11,200 --> 00:05:15,750
and it's very possible that you're in the 
minority who would actually help, 

70
00:05:15,750 --> 00:05:19,620
but people oftentimes show what this 
week's assigned reading calls the  

71
00:05:19,620 --> 00:05:24,042
"false uniqueness effect" --  
a false belief that when it comes to our 

72
00:05:24,042 --> 00:05:29,810
good deeds and other desirable behaviors, 
we're more unique than we really are, 

73
00:05:29,810 --> 00:05:34,230
a false belief in which we see ourselves 
as a cut above the pack, 

74
00:05:34,230 --> 00:05:37,270
which, of course, not all members 
of the pack can be. 

75
00:05:38,610 --> 00:05:42,554
Since the time of Nisbett and Borgida's 
research, studies have found that people 

76
00:05:42,554 --> 00:05:46,880
do pay attention to consensus information 
in some instances, 

77
00:05:46,880 --> 00:05:51,170
but surprisingly often, knowledge about 
what other people do has relatively 

78
00:05:51,170 --> 00:05:56,598
little effect on causal attributions. 
In fact, the tendency to underestimate 

79
00:05:56,598 --> 00:06:01,020
the impact of situational factors and 
overestimate the role of dispositional 

80
00:06:01,020 --> 00:06:05,112
factors -- factors unique to the 
individual -- is known in social psychology 

81
00:06:05,112 --> 00:06:12,537
as the "fundamental attribution error." 
The fundamental attribution error is a 

82
00:06:12,537 --> 00:06:16,478
true error, not simply a bias or a 
difference in perspective, 

83
00:06:16,478 --> 00:06:20,900
because people are explaining behavior in 
terms of an individual's disposition  

84
00:06:20,900 --> 00:06:24,650
even when you can demonstrate that the 
person's disposition had nothing to do 

85
00:06:24,650 --> 00:06:30,166
with why the behavior occurred. 
For example, in one of the earliest 

86
00:06:30,166 --> 00:06:34,454
studies published on this topic, people 
were presented with an essay 

87
00:06:34,454 --> 00:06:40,415
written by someone who was either forced 
to defend a politically unpopular position 

88
00:06:40,415 --> 00:06:46,050
or someone who was described as having 
free choice in selecting a position, 

89
00:06:46,050 --> 00:06:49,950
and even when people were told directly 
that the essay's author was roped into 

90
00:06:49,950 --> 00:06:55,810
taking an unpopular position, they tended 
to attribute that position to the author. 

91
00:06:56,860 --> 00:07:00,073
In other words, when the experimenter 
said, "We asked the author to take this 

92
00:07:00,073 --> 00:07:03,400
particular position in the essay, but 
we'd like you to guess what 

93
00:07:03,400 --> 00:07:07,661
the author really believes," 
people tended to rate the author as 

94
00:07:07,661 --> 00:07:10,430
actually believing what was written in 
the essay. 

95
00:07:11,650 --> 00:07:16,204
The social psychologist who coined the 
term "fundamental attribution error" is 

96
00:07:16,204 --> 00:07:19,266
Lee Ross. 
And I thought you might be interested in seeing

97
00:07:19,266 --> 00:07:23,900
a brief video clip of him talking 
about the error and its implications. 

98
00:07:25,000 --> 00:07:27,684
 >> Now, the fundamental attribution 
error therefore 

99
00:07:27,684 --> 00:07:31,440
really relates in an intimate way to 
the central task of psychology. 

100
00:07:31,440 --> 00:07:35,700
Psychology is interested in sort of 
teasing apart the role of personality  

101
00:07:35,700 --> 00:07:39,874
and the role of the situation, but every 
individual layperson 

102
00:07:39,874 --> 00:07:43,600
in their everyday life is trying to do exactly 
the same thing 

103
00:07:43,600 --> 00:07:46,050
when they see behavior. They're 
trying to say, why did the actor do it? 

104
00:07:46,050 --> 00:07:49,156
What do I learn about the actor? 
What implications might it have for 

105
00:07:49,156 --> 00:07:53,850
the way other people would behave? 
The truth really is that the fundamental 

106
00:07:53,850 --> 00:07:58,000
attribution error relates to the 
fundamental mission of social psychology, 

107
00:07:58,000 --> 00:08:03,392
as I said, which has to do with 
appreciating the power of the situation. 

108
00:08:03,392 --> 00:08:07,410
And most of our most famous classic 
experiments, the Asch experiment 

109
00:08:07,410 --> 00:08:10,512
and the Milgram experiment, and all 
the other things that 

110
00:08:10,512 --> 00:08:14,445
students typically learn in 
introductory psych classes, 

111
00:08:14,445 --> 00:08:18,300
really depend on this error. 
It's the fact that we think behavior is 

112
00:08:18,300 --> 00:08:22,400
controlled by stable traits or 
dispositions that makes us very surprised 

113
00:08:22,400 --> 00:08:27,833
when we see that a social psychologist 
who cleverly arranges the situation 

114
00:08:27,833 --> 00:08:33,750
can get people to be highly obedient, or 
highly altruistic, or highly  

115
00:08:33,750 --> 00:08:37,170
conforming or even highly destructive 
and aggressive in their behavior. 

116
00:08:37,170 --> 00:08:41,100
It sort of shocks us, and the reason it 
shocks is that we haven't given  

117
00:08:41,100 --> 00:08:45,800
adequate weight to exactly the feature 
of the situation that's responsible for 

118
00:08:45,800 --> 00:08:50,810
the actor's behavior. 
 >> For those of you who read the amazing 

119
00:08:50,810 --> 00:08:56,000
article by David Rosenhan, "On 
Being Sane in Insane Places," you can see 

120
00:08:56,000 --> 00:09:00,635
how the fundamental attribution error 
might operate in psychiatric settings. 

121
00:09:00,750 --> 00:09:05,500
When the pseudo patients, for example, got 
bored and began to pace back and forth, 

122
00:09:05,500 --> 00:09:12,020
they were seen as emotionally disturbed, 
as having a dispositional problem. 

123
00:09:12,020 --> 00:09:15,320
Or when they took notes as part of the 
investigation, their behavior  

124
00:09:15,320 --> 00:09:19,870
was interpreted as evidence of an underlying 
mental illness.  

125
00:09:19,870 --> 00:09:23,600
"Patient engages in writing behavior," 
something along those lines. 

126
00:09:23,950 --> 00:09:27,200
One question over the years that 
researchers have asked about the 

127
00:09:27,200 --> 00:09:32,530
fundamental attribution error is how 
fundamental it actually is. 

128
00:09:32,530 --> 00:09:37,440
For instance, does it occur just as often 
in the East as it does in the West? 

129
00:09:37,440 --> 00:09:42,800
The answer here appears to be no. 
When situational factors are fairly obvious, 

130
00:09:42,800 --> 00:09:46,184
East Asians are much less likely 
than Westerners to commit 

131
00:09:46,184 --> 00:09:51,100
the fundamental attribution error. 
So, to the extent that the error is truly 

132
00:09:51,100 --> 00:09:56,750
fundamental, it's much more the case here 
in the West than it is in the East. 

133
00:09:56,750 --> 00:10:01,600
Compared to the East, Western cultures 
focus more on rugged individualism, 

134
00:10:01,600 --> 00:10:06,600
on the self-made person rather than the 
group or the situation. 

135
00:10:07,450 --> 00:10:11,400
Now, it's important not to confuse the 
fundamental attribution error 

136
00:10:11,400 --> 00:10:16,300
with a closely related phenomenon 
known as actor-observer differences in 

137
00:10:16,300 --> 00:10:18,600
attribution, so I want to talk to you about that 

138
00:10:18,600 --> 00:10:22,600
for a little bit. 
And by the way, in social psychology, an "actor" 

139
00:10:22,600 --> 00:10:27,700
is simply someone who takes an 
action -- it's not somebody in the movies. 

140
00:10:28,270 --> 00:10:33,200
The classic finding here is that actors 
are more likely to explain their behavior 

141
00:10:33,200 --> 00:10:38,100
as a function of situational factors than 
are observers 

142
00:10:38,100 --> 00:10:43,546
(that is, people watching the actor behave). 
Unlike the fundamental attribution error, 

143
00:10:43,546 --> 00:10:47,450
which is truly an error, the 
actor-observer difference in attribution 

144
00:10:47,450 --> 00:10:50,850
is simply a difference, a bias in 
viewpoint; 

145
00:10:50,850 --> 00:10:54,700
there's not necessarily a right or wrong 
answer. 

146
00:10:55,130 --> 00:10:58,300
You say that you're late to work because 
the traffic was bad 

147
00:10:58,300 --> 00:11:02,250
(a situational attribution), but your 
boss says that it's because 

148
00:11:02,250 --> 00:11:05,550
you're unreliable (a dispositional 
attribution). 

149
00:11:05,550 --> 00:11:09,390
That's an actor-observer difference in 
attribution. 

150
00:11:09,390 --> 00:11:11,865
When are these differences most likely to 
occur? 

151
00:11:12,700 --> 00:11:18,100
Well, a huge meta-analysis of 173 
different studies found that actors  

152
00:11:18,100 --> 00:11:21,980
do downplay dispositional explanations for 
their behavior, 

153
00:11:21,980 --> 00:11:26,790
but mainly when their behavior or the 
outcome is negative. 

154
00:11:26,790 --> 00:11:31,250
For example, when people fail an exam or 
crash a car, they're less likely than 

155
00:11:31,250 --> 00:11:35,300
observers to attribute the negative 
outcome to their ability level 

156
00:11:35,300 --> 00:11:40,700
or to other personal characteristics. 
In contrast, if the behavior or event 

157
00:11:40,700 --> 00:11:46,235
is positive, this difference often reverses, 
with people attributing their success  

158
00:11:46,235 --> 00:11:50,900
to dispositional factors. 
In other words, contrary to the classic formulation, 

159
00:11:50,900 --> 00:11:56,800
the meta-analysis found that to the 
extent actor-observer differences exist, 

160
00:11:56,800 --> 00:12:01,460
they're often self-serving biases 
that can cut in either direction, 

161
00:12:01,460 --> 00:12:05,059
with actors avoiding dispositional 
attributions when the outcome  

162
00:12:05,059 --> 00:12:08,600
is negative, but not when the outcome is 
positive. 

163
00:12:09,700 --> 00:12:14,200
So, one reason -- quite possibly, the main 
reason -- for actor-observer differences 

164
00:12:14,200 --> 00:12:18,200
in attribution is that people don't want to 
look bad, either to themselves 

165
00:12:18,200 --> 00:12:22,600
or to outside observers. 
But some researchers have argued that 

166
00:12:22,600 --> 00:12:26,810
there's another ingredient that might be 
fueling actor-observer differences -- 

167
00:12:26,810 --> 00:12:30,400
something that we covered in the last 
video: salience.

168
00:12:31,800 --> 00:12:34,300
To actors, especially actors explaining 
their role 

169
00:12:34,300 --> 00:12:37,520
in a negative outcome, the 
most salient thing is often the 

170
00:12:37,520 --> 00:12:43,184
situational obstacles that they faced. 
So, you'd expect actors to either view 

171
00:12:43,184 --> 00:12:47,152
the situation as relatively causal or at 
least not focus 

172
00:12:47,152 --> 00:12:51,970
heavily on their own disposition. 
But to observers, the most salient thing 

173
00:12:51,970 --> 00:12:55,630
is typically the actor, the person 
they're observing, 

174
00:12:55,630 --> 00:12:59,392
so you'd expect observers to explain 
behavior in terms of the actor's 

175
00:12:59,392 --> 00:13:03,298
personal characteristics. 
Of course, to the extent that observers 

176
00:13:03,298 --> 00:13:08,605
blame the actor when things go wrong 
and actors blame the situation, there's 

177
00:13:08,605 --> 00:13:12,108
a potential for conflict, 
so it would be great if there were a way 

178
00:13:12,108 --> 00:13:16,700
to minimize these differences, 
and it turns out that in fact, there is. 

179
00:13:16,700 --> 00:13:21,600
Through a little psychological judo, the 
relationship between salience and causal 

180
00:13:21,600 --> 00:13:26,050
attribution can actually be used to 
reverse the classic 

181
00:13:26,050 --> 00:13:30,999
actor-observer difference in attribution. 
The person who figured this out was 

182
00:13:30,999 --> 00:13:35,840
Michael Storms, who published a study 
on this issue 40-some years ago. 

183
00:13:35,840 --> 00:13:39,536
But before I describe the study, I should 
also warn you that the findings 

184
00:13:39,536 --> 00:13:45,000
and the role of salience itself in generating 
attributional differences are still 

185
00:13:45,000 --> 00:13:48,985
being debated by researchers, 
several of whom I consulted before 

186
00:13:48,985 --> 00:13:52,500
taping this lecture. 
So, the best I can say is that this area 

187
00:13:52,500 --> 00:13:56,600
of attribution research is a work in 
progress. 

188
00:13:56,600 --> 00:14:00,830
Anyhow, the procedure that Michael Storms 
used was simple. 

189
00:14:00,830 --> 00:14:05,200
The study involved 30 sessions, each with 
four participants randomly assigned 

190
00:14:05,200 --> 00:14:09,360
to play a particular role: 
two actors who held a five-minute 

191
00:14:09,360 --> 00:14:12,900
get-acquainted conversation that the 
experimenter videotaped, 

192
00:14:12,900 --> 00:14:18,600
and two off-camera observers, who watched the 
actors have their conversation. 

193
00:14:18,600 --> 00:14:22,670
During these conversations, two 
videotapes were made. 

194
00:14:22,670 --> 00:14:26,900
One shot from Actor 1's perspective 
looking at Actor 2, 

195
00:14:26,900 --> 00:14:32,160
and another from Actor 2's perspective, 
looking at Actor 1. 

196
00:14:32,160 --> 00:14:35,770
Here's a rough sense of what the 
experimental procedure looked like. 

197
00:14:35,770 --> 00:14:39,552
The two people in the center are the 
actors, and you can see an observer 

198
00:14:39,552 --> 00:14:45,668
and a video camera facing each actor. 
After the get-acquainted conversation, 

199
00:14:45,668 --> 00:14:49,900
participants were randomly assigned to 
one of three conditions. 

200
00:14:49,900 --> 00:14:55,000
In the same orientation condition, 
observers viewed a videotape of the same 

201
00:14:55,000 --> 00:14:59,400
actor they had been watching, and actors 
viewed a videotape of their 

202
00:14:59,400 --> 00:15:02,085
conversation partner. 
So, basically it was just like a rerun 

203
00:15:02,085 --> 00:15:06,674
from the same perspective. 
Here, for example, is what the actor on 

204
00:15:06,674 --> 00:15:10,800
the left would be shown. 
In the new orientation condition, 

205
00:15:10,800 --> 00:15:15,500
observers viewed a videotape of the actor 
they had not been watching, 

206
00:15:15,500 --> 00:15:20,900
and actors viewed a videotape of themselves, 
thereby reversing their visual 

207
00:15:20,900 --> 00:15:25,350
orientation. 
And in the no videotape condition, 

208
00:15:25,350 --> 00:15:30,190
participants didn't watch a videotape of 
the conversation. 

209
00:15:30,190 --> 00:15:33,500
What the study found is that when people 
were asked to explain 

210
00:15:33,500 --> 00:15:37,195
the actor's behavior, 
participants who either watched a videotape 

211
00:15:37,195 --> 00:15:40,907
from the same orientation or watched 
no videotape at all displayed 

212
00:15:40,907 --> 00:15:45,730
the classic actor observer difference in 
attribution, 

213
00:15:45,730 --> 00:15:49,305
but participants who viewed a videotape 
shot from the opposite perspective 

214
00:15:49,305 --> 00:15:53,100
showed a reversal of the classic 
difference. 

215
00:15:53,100 --> 00:15:56,406
In other words, when actors watched 
themselves, they tended 

216
00:15:56,406 --> 00:15:59,616
to make dispositional attributions for their 
behavior, 

217
00:15:59,616 --> 00:16:03,600
and when observers watched the conversation 
from the actor's point of view,  

218
00:16:03,600 --> 00:16:07,820
they tended to make situational attributions 
for the actor's behavior. 

219
00:16:09,250 --> 00:16:13,410
So, once again we have a demonstration 
that visual orientation 

220
00:16:13,410 --> 00:16:17,720
can affect how people explain behavior, 
including their own behavior. 

221
00:16:18,940 --> 00:16:22,657
In the next video, we'll move from 
explanations of behavior to behavior 

222
00:16:22,657 --> 00:16:26,074
itself -- 
specifically, the question of whether 

223
00:16:26,074 --> 00:16:29,535
attitudes and behavior are closely 
connected to each other -- 

224
00:16:29,535 --> 00:16:33,751
a topic of research that's turned up some 
surprising results. 

225
00:16:34,800 --> 00:16:37,500
First, though, let's end with a pop-up 
question. 

226
00:16:37,500 --> 00:16:41,500
Abracadabra! 

