




1
00:00:06,890 --> 00:00:11,570
Stanley Milgram's research on obedience 
grew out of earlier studies that Solomon Asch 

2
00:00:11,570 --> 00:00:15,690
conducted on conformity, 
the topic of this video and a close 

3
00:00:15,690 --> 00:00:20,440
cousin of obedience. 
Conformity is a change in behavior or 

4
00:00:20,440 --> 00:00:26,040
belief as a result of social pressure, and 
to see how Solomon Ash studied it, 

5
00:00:26,040 --> 00:00:29,010
let's look at an eight-minute video that 
Dateline NBC  

6
00:00:29,010 --> 00:00:31,710
has generously made available to our class. 

7
00:00:39,560 --> 00:00:42,460
 >> When it's least expected, you're 
elected. 

8
00:00:42,460 --> 00:00:46,340
You're the star today. Smile! 

9
00:00:46,340 --> 00:00:51,800
You're on Candid Camera. 
 >> This man finds himself in a strange predicament. 

10
00:00:51,800 --> 00:00:53,600
So does this man. 

11
00:00:53,600 --> 00:00:59,500
He's in an elevator facing front, but 
inexplicably everyone else is facing the back. 

12
00:00:61,100 --> 00:01:04,000
It's the dilemma presented by TV's Candid Camera 

13
00:01:04,000 --> 00:01:08,900
35 years ago, pitting 
individuality against the pull of group pressure. 

14
00:01:08,900 --> 00:01:11,100
 >> He's very unhappy, you'll see him. 

15
00:01:11,100 --> 00:01:15,900
He doesn't know whether to go inside and 
face front or outside and face back. 

16
00:01:18,200 --> 00:01:23,110
Here's a fella with his hat on. 
 >> Over and over, the unsuspecting TV 

17
00:01:23,110 --> 00:01:27,110
stars all reacted the same way: like 
sheep. 

18
00:01:27,110 --> 00:01:30,869
 >> A moment later, we'll open the door; 
everybody's changed positions.

19
00:01:45,140 --> 00:01:50,000
 >> Although it was just for laughs, 
Candid Camera also raised serious questions 

20
00:01:50,000 --> 00:01:54,650
about conformity for all of us. 
Would you be strong enough to stand up 

21
00:01:54,650 --> 00:01:58,200
for yourself if you're a minority of one? 

22
00:01:58,700 --> 00:02:03,240
What is conformity? 
 >> Conformity is when we give in to the 
group. 

23
00:02:03,240 --> 00:02:06,600
 >> Social psychologist Dr. 
Anthony Pratkanis has studied 

24
00:02:06,600 --> 00:02:11,300
conformity for two decades. 
 >> On some occasions, on many occasions, 

25
00:02:11,300 --> 00:02:15,050
it can be very, very 
difficult, very, very negative. 

26
00:02:15,050 --> 00:02:20,230
So, for instance, when the group is wrong 
and you're right, conformity is bad. 

27
00:02:20,230 --> 00:02:23,150
 >> How powerful is the pull of negative 
group pressure? 

28
00:02:23,150 --> 00:02:28,000
We wanted to know, so Dateline enlisted 
Professor Pratkanis and his psychology 

29
00:02:28,000 --> 00:02:33,000
students at the University of California, 
Santa Cruz, to replicate 

30
00:02:33,000 --> 00:02:38,130
Dr. Solomon Asch's classic 1950s conformity 
study. 

31
00:02:38,130 --> 00:02:42,000
They did what Asch did: 
design a simple exercise, matching the 

32
00:02:42,000 --> 00:02:46,010
line on the left with the one of 
identical length on the right, 

33
00:02:46,010 --> 00:02:50,520
and then have the students, who were 
plants, give intentionally wrong answers 

34
00:02:50,520 --> 00:02:55,374
claiming, for example, Line 1 on this 
card is the right match. 

35
00:02:55,374 --> 00:02:57,200
 >> One. 

36
00:02:57,200 --> 00:03:01,400
 >> One. 
 >> One. 
 >> One. 

37
00:03:03,200 --> 00:03:04,000
 >> One. 

38
00:03:04,000 --> 00:03:08,210
 >> The subjects, always in seat six, are 
then faced with the experiment's 

39
00:03:08,210 --> 00:03:13,030
fundamental question: conform by giving 
answers they know to be absurdly wrong, 

40
00:03:13,030 --> 00:03:17,652
or defy the group and give answers they 
know to be correct. 

41
00:03:17,652 --> 00:03:20,900
 >> Go ahead. 
 >> And there was one more part of the experiment

42
00:03:20,900 --> 00:03:25,295
designed to draw the subjects 
in and make the exercise more convincing. 

43
00:03:25,295 --> 00:03:28,120
Initially, the plants actually give 

44
00:03:28,120 --> 00:03:29,800
the correct answers... 

45
00:03:29,800 --> 00:03:34,900
 >> Two. 
 >> Two. 
 >> Two. 
 >> Two. 

46
00:03:35,000 --> 00:03:37,400
 >> at least the first few times. 

47
00:03:37,400 --> 00:03:40,820
 >> We have consensus. 
 >> But as the rounds continue and the 

48
00:03:40,820 --> 00:03:46,510
plants do give those wrong answers, try 
playing along. Watch these subjects, 

49
00:03:46,510 --> 00:03:49,490
who were told they were part of a visual 
perception experiment, 

50
00:03:49,490 --> 00:03:54,300
respond to the mounting pressure of the 
group solidarity and ask yourself: 

51
00:03:54,300 --> 00:03:58,700
What would you do? 
We'll start with Marie, as she hears the 

52
00:03:58,700 --> 00:04:00,100
clearly wrong answers. 

53
00:04:00,100 --> 00:04:05,400
 >> One. 
 >> One. 
 >> She glances at the leader, seemingly for help, 

54
00:04:05,400 --> 00:04:08,913
yet she sticks to her guns and 
defies the group. 

55
00:04:08,913 --> 00:04:12,400
 >> Three. 
 >> And keeps disagreeing... 

56
00:04:12,400 --> 00:04:16,000
 >> Three. 
 >> Three. 
 >> Three. 

57
00:04:16,000 --> 00:04:18,275
 >> although her resistance is never 
easy. 

58
00:04:18,275 --> 00:04:23,430
 >> One. 
 >> As Marie stares intently at this card, 

59
00:04:23,430 --> 00:04:30,590
she can't believe her ears. 
 >> One. >> One. >> One. 

60
00:04:30,590 --> 00:04:35,360
 >> The urge to join the group and 
conform builds with each wrong answer, 

61
00:04:35,360 --> 00:04:38,360
and she suffers for every right answer she 
gives. 

62
00:04:38,360 --> 00:04:41,500
 >> Two. 
 >> What does she finally do? 

63
00:04:41,500 --> 00:04:46,200
 >> Three. 
 >> Three. 
 >> Three. 

64
00:04:47,000 --> 00:04:48,100
 >> Two. 

65
00:04:48,100 --> 00:04:54,500
 >> She would not conform. 
 >> You see the worry on her face, concern, 

66
00:04:54,500 --> 00:04:57,240
very painful to stand up to the 
group, but she stood up to the group. 

67
00:04:57,240 --> 00:05:01,100
 >> It was hard for her to do that. 
 >> Yes, because the pressure of 

68
00:05:01,100 --> 00:05:03,500
 >> "I might be wrong. 
I might be the fool here." 

69
00:05:03,500 --> 00:05:05,530
 >> There was a lot of pressure in that 
room. 

70
00:05:05,530 --> 00:05:09,650
There was a lot of tension that I felt. 
I can see where somebody would buckle 

71
00:05:09,650 --> 00:05:12,400
and just lose it. 
 >> And, you didn't like being different? 

72
00:05:12,400 --> 00:05:14,800
 >> No, but I didn't want to be wrong either. 

73
00:05:15,800 --> 00:05:17,170
 >> Up next, Brian. 

74
00:05:17,170 --> 00:05:19,031
What will he do? 
 >> One. 

75
00:05:19,031 --> 00:05:22,000
 >> One. 
 >> The first few times the plants pick 

76
00:05:22,000 --> 00:05:26,338
the obviously incorrect line, Brian 
confidently defies the group. 

77
00:05:26,338 --> 00:05:27,554
 >> One. 
 >> Three. 

78
00:05:27,554 --> 00:05:30,380
 >> Confident, because just look at the 
card --

79
00:05:30,380 --> 00:05:34,140
Of course, the only logical answer is 
three. 

80
00:05:34,140 --> 00:05:37,290
And this card.
Nobody could possibly say anything but 

81
00:05:37,290 --> 00:05:40,300
three is the correct answer here 
as well, right? 

82
00:05:40,600 --> 00:05:42,711
 >> Just watch. 
 >> Two. 

83
00:05:42,711 --> 00:05:47,752
 >> Notice Brian's eyes start to droop, 
and listen to his voice get softer. 

84
00:05:47,752 --> 00:05:51,000
 >> Three. 
 >> When the group gets it wrong again. 

85
00:05:51,500 --> 00:05:55,700
And again. >> Three.  >> Three. 

86
00:05:56,000 --> 00:05:56,600
 >> Two. 

87
00:05:56,600 --> 00:06:01,100
 >> For four rounds now he's been 
fighting the good fight against conformity. 

88
00:06:01,100 --> 00:06:04,700
 >> Three. 
 >> Three. 

89
00:06:04,800 --> 00:06:06,500
 >> But, with the pressure mounting... 

90
00:06:06,500 --> 00:06:09,354
 >> Three. 
 >> Three. 

91
00:06:09,354 --> 00:06:12,675
 >> Three. 
 >> He just can't fight anymore. 

92
00:06:12,675 --> 00:06:15,580
 >> Three. 
 >> And in succeeding rounds with Brian 

93
00:06:15,580 --> 00:06:18,338
continuing to give answers he knows to be 
wrong... 

94
00:06:18,338 --> 00:06:20,436
 >> Two. 
 >> Two. 

95
00:06:20,436 --> 00:06:24,494
 >> he only gets sadder and softer. 
 >> Three. 

96
00:06:24,494 --> 00:06:28,200
 >> By the final trial, he's a thoroughly 
beaten man. 

97
00:06:28,200 --> 00:06:30,087
 >> One. 

98
00:06:30,087 --> 00:06:33,920
 >> I gave up. 
There were times when I didn't even look 

99
00:06:33,920 --> 00:06:38,210
at the lines and said the answer. 
I became quiet, 

100
00:06:38,210 --> 00:06:45,300
decided that I wasn't playing the game 
right, so I just played along. 

101
00:06:45,800 --> 00:06:49,300
 >> One of the participants in our 
demonstration was a bit different: 

102
00:06:49,300 --> 00:06:57,700
this man, George, older and an artist. 
 >> Basically I consider myself to be a nonconformist. 

103
00:06:57,700 --> 00:07:01,700
 >> And one of the student plants sized 
him up exactly that way. 

104
00:07:01,900 --> 00:07:04,400
 >> I felt he would take one look at us and say 

105
00:07:04,400 --> 00:07:07,600
a bunch of kids, and then go his 
own way. 

106
00:07:07,600 --> 00:07:08,900
 >> Would he? 

107
00:07:08,900 --> 00:07:13,546
 >> Two. 
 >> Two. 
 >> Two. 

108
00:07:13,546 --> 00:07:17,620
 >> Three. 
 >> He holds out for two rounds. 

109
00:07:17,620 --> 00:07:21,900
But what happens on the very next round? 

110
00:07:21,900 --> 00:07:29,200
 >> Three. 
 >> Three. 
 >> Three. 
 >> Three. 

111
00:07:30,200 --> 00:07:31,130
 >> Three. 

112
00:07:31,130 --> 00:07:34,768
 >> A total reversal. 
And what about the next round? 

113
00:07:34,768 --> 00:07:38,100
 >> Three. 
 >> Three. 
 >> Three. 

114
00:07:38,100 --> 00:07:41,600
 >> George, it so happens, not only goes along -- 

115
00:07:41,600 --> 00:07:44,860
he dives in headfirst. 
 >> One. 

116
00:07:44,860 --> 00:07:47,772
 >> He gives wrong answers with an air 
of total certainty. 

117
00:07:47,772 --> 00:07:49,000
 >> Three. 

118
00:07:49,000 --> 00:07:52,000
 >> I didn't want to be different anymore. 

119
00:07:52,000 --> 00:07:54,700
 >> The pressure got to me and I 
caved in. 

120
00:07:54,700 --> 00:07:57,180
 >> Three. 
 >> Three. 

121
00:07:57,180 --> 00:07:58,820
Even though I knew it wasn't the 
truth, 

122
00:08:00,630 --> 00:08:05,730
it made me feel more comfortable. 
 >> Of all the subjects, George 

123
00:08:05,730 --> 00:08:11,200
conformed the most -- the last nine times 
in a row. 

124
00:08:11,200 --> 00:08:14,400
Dr. Pratkanis acknowledges that despite 
decades of research 

125
00:08:14,400 --> 00:08:17,800
by social psychologists, there is no 
way of predicting 

126
00:08:17,800 --> 00:08:22,000
with certainty what George or 
the other subjects would do. 

127
00:08:22,400 --> 00:08:24,110
 >> It's important that we walk into 

128
00:08:24,110 --> 00:08:26,210
situations and say, hey, I'm a human 
being, 

129
00:08:26,210 --> 00:08:29,980
it could happen to me. 
 >> In the conformity test you just saw, 

130
00:08:29,980 --> 00:08:35,530
nearly 60% of the subjects, 9 of 16, 
succumbed to the group pressure. 

131
00:08:35,530 --> 00:08:39,700
These results, a half century later, 
essentially support the findings in 

132
00:08:39,700 --> 00:08:46,100
Dr. Solomon Asch's original 1950 experiment. 
And today, in everyday life, 

133
00:08:46,100 --> 00:08:50,520
all of us are influenced by group pressure. 
 >> Strike the pose. 

134
00:08:50,520 --> 00:08:56,500
 >> Whether it's in fashion trends, 
doing the wave at sporting events, 

135
00:08:57,500 --> 00:09:00,805
or following the herd in business...
 >> This way! I don't know.

136
00:09:00,805 --> 00:09:06,000
 >> as this TV commercial humorously 
suggests. 

137
00:09:07,800 --> 00:09:10,660
 >> Isn't that scene from Candid Camera 
just amazing? 

138
00:09:10,660 --> 00:09:15,260
I never get tired of looking at that. 
Anyway, an interesting historical footnote 

139
00:09:15,260 --> 00:09:20,800
is that Stanley Milgram was not 
only a research assistant for Solomon Asch, 

140
00:09:20,800 --> 00:09:24,610
but in fact, he conducted his 
doctoral dissertation on the topic of 

141
00:09:24,610 --> 00:09:27,740
conformity using a version of the Asch 
technique. 

142
00:09:27,740 --> 00:09:34,160
So, Asch's research on conformity in the 
1950s laid the groundwork for Milgram's 

143
00:09:34,160 --> 00:09:39,760
research on obedience in the early 1960s. 
Throughout his adult life, Milgram 

144
00:09:39,760 --> 00:09:44,510
considered Asch his main scientific 
influence, and he saw Asch's research as 

145
00:09:44,510 --> 00:09:50,400
a kind of intellectual jewel that could 
be endlessly rotated to yield interesting results. 

146
00:09:50,600 --> 00:09:52,810
In the basic procedure shown in the clip 

147
00:09:52,810 --> 00:09:57,750
from Dateline NBC, there were 18 trials 
in each experimental session,  

148
00:09:57,750 --> 00:10:03,230
and on 12 of these trials, which Ash 
called "critical trials," the majority 

149
00:10:03,230 --> 00:10:08,610
unanimously chose the wrong answer. 
Under these conditions, participants gave 

150
00:10:08,610 --> 00:10:13,800
incorrect answers in the direction of the 
majority on roughly one third 

151
00:10:13,800 --> 00:10:18,160
of all critical trials, 
three out of four participants conformed 

152
00:10:18,160 --> 00:10:22,710
on at least one trial, 
and one third of participants conformed on 

153
00:10:22,710 --> 00:10:27,550
half or more of the critical trials. 
In other words, most people in the 

154
00:10:27,550 --> 00:10:32,640
situation rendered at least some 
judgments that contradicted the evidence 

155
00:10:32,640 --> 00:10:37,630
of their senses. But, as Milgram mentioned, 
Asch also 

156
00:10:37,630 --> 00:10:41,860
rotated the jewel by conducting several 
variations of the experiment. 

157
00:10:41,860 --> 00:10:46,908
For example, in one set of experiments, 
Asch varied the number of opposing 

158
00:10:46,908 --> 00:10:52,880
confederates from 1 person all the way 
up to 15 people, wanting to see if 

159
00:10:52,880 --> 00:10:57,320
conformity increased as the majority 
increased. 

160
00:10:57,320 --> 00:11:01,810
What he found is that when participants 
were faced with only one confederate who 

161
00:11:01,810 --> 00:11:05,800
contradicted their views, 
they were hardly affected at all. 

162
00:11:05,800 --> 00:11:09,400
Most participants answered correctly on 
nearly all trials. 

163
00:11:09,400 --> 00:11:14,020
When participants were opposed by two 
confederates, they conformed to the 

164
00:11:14,020 --> 00:11:19,995
majority view 14% of the time. 
When participants were opposed by three 

165
00:11:19,995 --> 00:11:26,270
confederates, they conformed on 32% of 
the critical trials -- quite a jump. 

166
00:11:26,270 --> 00:11:32,550
But adding more confederates beyond that 
tends to have diminishing effects. 

167
00:11:32,550 --> 00:11:37,720
In another set of experiments, Asch broke 
the unanimity of the majority by adding 

168
00:11:37,720 --> 00:11:41,650
either a second participant, who was 
seated in the fourth position 

169
00:11:41,650 --> 00:11:46,836
and usually offered correct judgments 
(because remember, even 32% conformity 

170
00:11:46,836 --> 00:11:52,340
implies 68% nonconformity, so usually 
they're giving the correct answers), 

171
00:11:52,340 --> 00:11:57,120
or he planted a confederate in the 
fourth position who was instructed to 

172
00:11:57,120 --> 00:12:01,370
give the correct answer. 
Asch wanted to see what would happen when 

173
00:12:01,370 --> 00:12:07,800
the majority was no longer unanimous, 
and the effect he found was enormous. 

174
00:12:07,800 --> 00:12:12,100
Conformity fell to roughly one fourth of 
previous levels -- 

175
00:12:12,100 --> 00:12:16,160
that is, participants conformed on 
roughly one fourth as many critical 

176
00:12:16,160 --> 00:12:19,900
trials as when the majority was 
unanimous. 

177
00:12:19,900 --> 00:12:25,270
In fact, Asch found that a unanimous 
majority of three was far more powerful 

178
00:12:25,270 --> 00:12:28,350
than a majority of eight with one 
dissenter, 

179
00:12:28,350 --> 00:12:30,900
which says something about the power of 
dissent. 

180
00:12:31,590 --> 00:12:35,500
Asch even looked at whether he could 
increase the difference in line length 

181
00:12:35,500 --> 00:12:39,920
between the majority's answer and the 
correct answer so much that no 

182
00:12:39,920 --> 00:12:45,570
participants would conform to the majority. 
He failed to find that point, however. 

183
00:12:45,570 --> 00:12:51,000
Even when the lines differed by as much 
as seven inches -- a huge amount given how 

184
00:12:51,000 --> 00:12:55,770
close people were to the lines -- 
some participants continued to yield 

185
00:12:55,770 --> 00:12:59,640
to the majority. 
Solomon Asch published several other variations, 

186
00:12:59,640 --> 00:13:05,110
but let's return to his main 
core finding: that is, participants in the 

187
00:13:05,110 --> 00:13:09,340
original experiments conformed 
on roughly one out of every three 

188
00:13:09,340 --> 00:13:14,090
critical trials, and three out of four 
participants conformed at least once. 

189
00:13:15,150 --> 00:13:19,490
Does that seem like a lot or a little, and 
why? 

190
00:13:20,540 --> 00:13:24,650
Let's pause for a pop-up question so that 
you can answer that for yourself, 

191
00:13:24,650 --> 00:13:27,930
and then, in the next video, I'll share 
with you what psychologists have 

192
00:13:27,930 --> 00:13:32,910
traditionally said to that question, 
what they say today, and also we'll 

193
00:13:32,910 --> 00:13:38,870
discuss the role of gender, culture, and 
the impact of numeric minorities on 

194
00:13:38,870 --> 00:13:41,430
majorities. 
So we'll flip things around. 

