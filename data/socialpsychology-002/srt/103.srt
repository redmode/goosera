﻿1
00:00:06,880 --> 00:00:08,480
Welcome back.

2
00:00:08,480 --> 00:00:11,200
In this lecture I want to move our discussion of aggression

3
00:00:11,200 --> 00:00:16,920
from general principles to a specific type of aggression: terrorism.

4
00:00:16,920 --> 00:00:20,880
This is a topic that evokes very strong feelings and strong opinions

5
00:00:20,880 --> 00:00:23,920
for many people, so I hope that you  use the discussion forums

6
00:00:23,920 --> 00:00:28,200
to share what you think openly, but respectfully.

7
00:00:28,200 --> 00:00:32,440
Speaking personally, I have several indirect connections to tragedies

8
00:00:32,440 --> 00:00:35,560
involving terrorism, including a former colleague

9
00:00:35,560 --> 00:00:38,320
whose husband was killed in a terrorist bombing

10
00:00:38,320 --> 00:00:41,800
soon after they were married and a student whose father

11
00:00:41,800 --> 00:00:45,640
died in the September 11th attacks in 2001.

12
00:00:45,640 --> 00:00:48,800
In many ways, the world became a different place

13
00:00:48,800 --> 00:00:51,880
after the September 11th attacks, and the magnitude

14
00:00:51,880 --> 00:00:54,920
of this change is clear not only from the wars in Iraq

15
00:00:54,920 --> 00:00:58,840
and Afghanistan and the global efforts to fight terrorism,

16
00:00:58,840 --> 00:01:02,150
but in research on the psychology of terrorism.

17
00:01:02,150 --> 00:01:05,340
Here's a graph showing the number of articles on terrorism

18
00:01:05,340 --> 00:01:09,000
contained in PsycINFO, the world's largest database

19
00:01:09,000 --> 00:01:12,080
of psychological research, and what you can see is that

20
00:01:12,080 --> 00:01:17,440
psychological research on terrorism was fairly minimal until 2001,

21
00:01:17,440 --> 00:01:22,970
at which point there was a tremendous increase in research activity.

22
00:01:22,970 --> 00:01:26,600
During this video I'd like to share what this new body of research,

23
00:01:26,600 --> 00:01:31,000
as well as political science research, has to say about the psychology

24
00:01:31,000 --> 00:01:36,000
of terrorism. Specifically, here are the questions I'd like to address:

25
00:01:36,000 --> 00:01:39,920
First, what is terrorism. How is it defined?

26
00:01:39,920 --> 00:01:43,040
Second, what type of people become terrorists?

27
00:01:43,040 --> 00:01:48,680
Are these people lunatics? Is there such a thing as a terrorist personality?

28
00:01:48,680 --> 00:01:50,640
What causes terrorism?

29
00:01:50,640 --> 00:01:55,730
And perhaps most importantly, how can terrorism be reduced?

30
00:01:55,730 --> 00:01:59,320
So let's turn to the first question, on what terrorism is --

31
00:01:59,320 --> 00:02:04,080
how it's typically defined. The word "terror" was first used

32
00:02:04,080 --> 00:02:07,080
to refer to political violence during the French Revolution

33
00:02:07,080 --> 00:02:13,690
of the 1790s when the revolutionaries created a "reign of terror."

34
00:02:13,690 --> 00:02:17,120
In modern times, there's no universally accepted definition

35
00:02:17,120 --> 00:02:21,080
of terrorism, but the U.S. State Department's definition,

36
00:02:21,080 --> 00:02:25,120
which is one of the most widely used, is "premeditated,

37
00:02:25,120 --> 00:02:29,760
politically motivated violence perpetrated against noncombatant

38
00:02:29,760 --> 00:02:35,080
targets (that is, citizens, schools, hospitals, and so forth)

39
00:02:35,080 --> 00:02:38,680
by subnational groups or clandestine agents,

40
00:02:38,680 --> 00:02:43,100
usually intended to influence an audience."

41
00:02:43,100 --> 00:02:46,120
Because so much of the research since 2001

42
00:02:46,120 --> 00:02:50,320
concerns Middle Eastern terrorism and Al Qaeda, let me also define

43
00:02:50,320 --> 00:02:56,250
three other key terms: Arab, the Arab world, and Muslim.

44
00:02:56,250 --> 00:02:59,040
The term Arab is an ethnic label that refers to

45
00:02:59,040 --> 00:03:03,480
a cultural group united by history and the Arabic language.

46
00:03:03,480 --> 00:03:06,840
Most Arabs are Muslim, but there are also millions of

47
00:03:06,840 --> 00:03:11,400
Christian Arabs and thousands of Jewish Arabs.

48
00:03:11,400 --> 00:03:14,090
When people talk about the Arab world, they're usually

49
00:03:14,090 --> 00:03:16,840
referring to twenty-some countries in the Middle East and

50
00:03:16,840 --> 00:03:22,000
Northern Africa, including Egypt, the Sudan, Saudi Arabia, Iraq --

51
00:03:22,000 --> 00:03:27,800
but not Iran, where the primary language is Farsi rather than Arabic.

52
00:03:27,800 --> 00:03:32,480
All told, there are approximately 300 million Arabs in the world.

53
00:03:32,480 --> 00:03:36,400
That's a lot of people, but it's only a sixth of the world's total

54
00:03:36,400 --> 00:03:40,860
Muslim population -- that is, believers in Islam.

55
00:03:40,860 --> 00:03:45,600
Muslims are estimated to comprise 1.8 billion people.

56
00:03:45,600 --> 00:03:49,680
That's roughly a quarter of the world's population.

57
00:03:49,680 --> 00:03:53,760
Terms such as "Arab," "the Arab world," and "Muslim" are central to

58
00:03:53,760 --> 00:03:57,200
understanding not only the psychology of Middle Eastern terrorism,

59
00:03:57,200 --> 00:04:01,040
but the War on Terror and the wars in Iraq and Afghanistan.

60
00:04:01,040 --> 00:04:05,200
For example, even though former U.S. President George W. Bush

61
00:04:05,200 --> 00:04:09,640
referred to Saddam Hussein as an ally of Al Qaeda, the bipartisan

62
00:04:09,640 --> 00:04:14,040
9/11 Commission later concluded that "Bin Laden had in fact been

63
00:04:14,040 --> 00:04:18,200
sponsoring anti-Saddam Islamists in Iraqi Kurdistan and

64
00:04:18,200 --> 00:04:21,920
sought to attract them into his Islamic army."

65
00:04:21,920 --> 00:04:25,960
Weeks before the Iraq war, Bin Laden reportedly warned Muslims

66
00:04:25,960 --> 00:04:29,800
who opposed the United States that "The fighting should be in the

67
00:04:29,800 --> 00:04:34,800
name of God only, not in the name of national ideologies nor to

68
00:04:34,800 --> 00:04:39,360
seek victory for the ignorant governments that rule all Arab states,

69
00:04:39,360 --> 00:04:45,160
including Iraq." This, then, is where it becomes critical to understand

70
00:04:45,160 --> 00:04:49,040
the difference between terms like Muslim and Arab, because

71
00:04:49,040 --> 00:04:52,080
a good case can be made that in response to Al Qaeda

72
00:04:52,080 --> 00:04:55,860
attacking the United States on September 11th, the United States

73
00:04:55,860 --> 00:05:00,400
waged war against an adversary of Al Qaeda, not an ally.

74
00:05:00,400 --> 00:05:04,160
And this decision to wage war in turn raises another question,

75
00:05:04,160 --> 00:05:08,680
which is whether large scale military responses are effective as a way

76
00:05:08,680 --> 00:05:12,760
to fight terrorism -- a topic that we'll return to in just a few minutes.

77
00:05:12,760 --> 00:05:15,680
But first, I want to consider the question of who's

78
00:05:15,680 --> 00:05:19,450
most likely to become a terrorist in the first place.

79
00:05:19,450 --> 00:05:23,400
We've all heard theories that terrorists are crazy, paranoid, or

80
00:05:23,400 --> 00:05:27,160
ruthless psychopaths who are indifferent to the suffering of others.

81
00:05:27,160 --> 00:05:30,440
For example, a year after the September 11th attacks,

82
00:05:30,440 --> 00:05:33,880
the mayor of New York called terrorists "madmen".

83
00:05:33,880 --> 00:05:36,640
And a member of the U.S. Senate Armed Services Committee

84
00:05:36,640 --> 00:05:40,560
said that they weren't "rational." From a clinical point of view,

85
00:05:40,560 --> 00:05:42,600
though, there's no evidence that terrorists

86
00:05:42,600 --> 00:05:46,760
tend to be irrational, psychotic, or psychopaths.

87
00:05:46,760 --> 00:05:49,800
After all, a mentally unbalanced terrorist would present

88
00:05:49,800 --> 00:05:52,920
a security risk to the other terrorists in the group,

89
00:05:52,920 --> 00:05:57,240
because terrorists need to operate in secrecy. So, terrorist groups

90
00:05:57,240 --> 00:06:02,040
generally tend to look for highly reliable team players and screen out

91
00:06:02,040 --> 00:06:06,800
unstable individuals. After reviewing the evidence, one leading expert

92
00:06:06,800 --> 00:06:11,120
on the psychology of terrorism put it this way: "The outstanding

93
00:06:11,120 --> 00:06:15,000
common characteristic of terrorists is their normality."

94
00:06:16,200 --> 00:06:17,680
Their normality?

95
00:06:17,680 --> 00:06:21,000
I still remember being surprised when I first read this,

96
00:06:21,000 --> 00:06:24,680
but many other researchers have come to the same conclusion.

97
00:06:24,680 --> 00:06:28,120
So if terrorists aren't pathological lunatics,

98
00:06:28,120 --> 00:06:31,279
who are these "normal people"?

99
00:06:31,279 --> 00:06:34,960
Well, most terrorists are males in their teens and twenties --

100
00:06:34,960 --> 00:06:39,920
the same population that's most likely to commit violent crime in general.

101
00:06:39,920 --> 00:06:43,800
For example, all of the terrorists on September 11th were male,

102
00:06:43,800 --> 00:06:50,640
all but one were in their 20s, and most were between the ages of 20 and 23.

103
00:06:50,640 --> 00:06:53,760
There have certainly been cases of terrorists who are younger, older,

104
00:06:53,760 --> 00:06:57,760
or female, but that's the exception rather than the rule.

105
00:06:57,760 --> 00:07:02,500
And in the case of suicide attacks, terrorists also tend to be unmarried.

106
00:07:03,520 --> 00:07:06,080
Other than that, there aren't many demographic or

107
00:07:06,080 --> 00:07:10,160
psychological differences between terrorists and non-terrorists.

108
00:07:10,160 --> 00:07:14,440
So, let's dig deeper and focus specifically on suicide bombers.

109
00:07:14,440 --> 00:07:17,840
I'll give you a pop-up question listing several characteristics,

110
00:07:17,840 --> 00:07:21,880
and you take a guess which ones are associated with suicide bombers --

111
00:07:21,880 --> 00:07:25,080
that is, more likely to be found among suicide bombers

112
00:07:25,080 --> 00:07:27,840
than people their age living in the same area.

113
00:07:27,840 --> 00:07:30,860
Then I'll share with you what the research record shows.

114
00:07:33,600 --> 00:07:36,360
The image that many people have of suicide bombers

115
00:07:36,360 --> 00:07:39,440
is that they're religious fanatics, but research shows that

116
00:07:39,440 --> 00:07:42,600
before they're recruited suicide bombers don't tend to be

117
00:07:42,600 --> 00:07:46,680
more religious than other people living in their area. Likewise,

118
00:07:46,680 --> 00:07:52,240
they're not more likely to be unemployed, fatherless, or friendless.

119
00:07:52,240 --> 00:07:55,520
The one psychological area in which terrorists do seem to

120
00:07:55,520 --> 00:07:59,400
differ from other people is that they tend to be angry.

121
00:08:00,120 --> 00:08:02,880
One of the most common motivations for joining a terrorist

122
00:08:02,880 --> 00:08:07,200
organization is a desire for revenge, for retribution,

123
00:08:07,200 --> 00:08:11,600
to fight a perceived injustice, or respond to humiliation.

124
00:08:11,600 --> 00:08:15,440
For example, many terrorists report that violent acts by the police,

125
00:08:15,440 --> 00:08:20,440
soldiers, and others, are what led them to join a terrorist organization.

126
00:08:20,440 --> 00:08:24,600
After September 11th, the National Research Council issued a report

127
00:08:24,600 --> 00:08:29,680
entitled "Terrorism: Perspectives from the Behavioral and Social Sciences,"

128
00:08:29,680 --> 00:08:33,920
And here's what the report concluded: "There is no single or

129
00:08:33,920 --> 00:08:39,040
typical mentality -- much less a specific pathology -- of terrorists.

130
00:08:39,040 --> 00:08:42,920
However, terrorists apparently find significant gratification

131
00:08:42,920 --> 00:08:46,000
in the expression of generalized rage."

132
00:08:47,080 --> 00:08:50,970
So, if rage is a better explanation than psychopathology,

133
00:08:50,970 --> 00:08:54,560
what are terrorists so outraged about that they're willing to kill?

134
00:08:54,560 --> 00:08:58,160
That is, what are the root causes of terrorism?

135
00:08:58,160 --> 00:09:01,520
Here, too, researchers have examined a number of potential factors,

136
00:09:01,520 --> 00:09:06,370
including political repression, poverty, lack of education, and so forth.

137
00:09:06,370 --> 00:09:08,000
Let's consider some of these factors.

138
00:09:09,100 --> 00:09:12,280
First, if it's repression you'd expect to see terrorism

139
00:09:12,280 --> 00:09:16,720
with dictatorships (Stalinist Russia, Nazi Germany, and so on),

140
00:09:16,720 --> 00:09:20,360
but relatively little terrorism occurs under dictatorships,

141
00:09:20,360 --> 00:09:23,320
in part because these regimes can crush terrorism by

142
00:09:23,320 --> 00:09:28,560
using unlimited force. Well, if it's not political repression,

143
00:09:28,560 --> 00:09:32,120
then what about poverty as a cause of terrorism?

144
00:09:32,120 --> 00:09:35,160
Here, too, the evidence just isn't there.

145
00:09:35,160 --> 00:09:38,120
Traditionally, the countries designated by the United Nations

146
00:09:38,120 --> 00:09:42,320
as least developed have had less terrorism than other countries.

147
00:09:42,320 --> 00:09:45,320
And of course, many designated as developed, including

148
00:09:45,320 --> 00:09:49,120
the United States and various European countries, have experienced

149
00:09:49,120 --> 00:09:54,560
a number of terrorist attacks. Now, of course, even if terrorism isn't

150
00:09:54,560 --> 00:09:58,960
directly related to poverty, it still might be a function of socioeconomic

151
00:09:58,960 --> 00:10:03,160
status -- that is, it might be a combination of poverty and

152
00:10:03,160 --> 00:10:09,290
lack of education. Once again, though, the evidence suggests otherwise.

153
00:10:09,290 --> 00:10:13,360
For example, research has found that Palestinian suicide bombers have

154
00:10:13,360 --> 00:10:17,800
lower rates of poverty and higher rates of formal education than other

155
00:10:17,800 --> 00:10:22,640
Palestinians in the same age bracket. And all of the September 11th suicide

156
00:10:22,640 --> 00:10:27,240
terrorists came from middle-class or upper middle-class backgrounds.

157
00:10:27,240 --> 00:10:32,000
So, terrorism isn't limited to any particular socioeconomic class.

158
00:10:33,080 --> 00:10:34,480
Where does this leave us?

159
00:10:34,480 --> 00:10:39,360
The answer is that there doesn't seem to be any general cause of terrorism,

160
00:10:39,360 --> 00:10:42,845
but rather, many specific causes -- just as there are many specific

161
00:10:42,845 --> 00:10:47,100
causes of murder. Here's how one terrorism researcher described it:

162
00:10:48,320 --> 00:10:52,800
"The endeavor to find a 'general theory' of terrorism, one overall

163
00:10:52,800 --> 00:10:57,670
explanation of its roots, is a futile and misguided enterprise.

164
00:10:57,670 --> 00:11:01,080
The motives of the Russian revolutionaries of 1881 have

165
00:11:01,080 --> 00:11:04,880
as much to do with al Qa'ida and the various Jihads as does

166
00:11:04,880 --> 00:11:08,680
the terrorism of Oklahoma City with Peru's Shining Path

167
00:11:08,680 --> 00:11:12,440
or the Colombian revolutionaries and drug dealers."

168
00:11:12,440 --> 00:11:15,080
Of course, this doesn't mean that there are no patterns

169
00:11:15,080 --> 00:11:19,470
within particular regions or directed at particular countries.

170
00:11:19,470 --> 00:11:21,840
When it comes to International terrorism directed

171
00:11:21,840 --> 00:11:25,680
at the United States, for example, researchers have reported

172
00:11:25,680 --> 00:11:29,840
at least one cause that's been implicated in a number of attacks.

173
00:11:29,840 --> 00:11:34,720
That cause is American military, financial, and political support

174
00:11:34,720 --> 00:11:40,230
for unpopular regimes. In many cases anti-American terrorists

175
00:11:40,230 --> 00:11:44,000
are trying to drive the United States out of a geographic area

176
00:11:44,000 --> 00:11:47,800
or to get it to end its support for a regime,

177
00:11:47,800 --> 00:11:51,320
retaliate in response to U.S. policies and actions

178
00:11:51,320 --> 00:11:54,280
(including, by extension, American-backed actions

179
00:11:54,280 --> 00:11:59,160
of the regime's police and soldiers), restore a sense of honor,

180
00:11:59,160 --> 00:12:03,800
status, or control, or any combination of these objectives.

181
00:12:04,880 --> 00:12:08,120
In light of these motivations, the trillion dollar question --

182
00:12:08,120 --> 00:12:11,510
literally -- is how to reduce terrorism.

183
00:12:11,510 --> 00:12:15,040
Since 2001, the United States and its allies have mounted

184
00:12:15,040 --> 00:12:18,800
a large-scale military response: waging war in Iraq

185
00:12:18,800 --> 00:12:22,280
and Afghanistan; conducting hundreds of drone strikes

186
00:12:22,280 --> 00:12:26,450
in Pakistan, Yemen, and elsewhere; holding suspected terrorists

187
00:12:26,450 --> 00:12:29,750
as prisoners in Guantanamo Bay; and so forth.

188
00:12:29,750 --> 00:12:32,000
Has this strategy succeeded?

189
00:12:32,450 --> 00:12:36,480
Well, if the goal is to capture or kill specific terrorist leaders,

190
00:12:36,480 --> 00:12:39,440
the strategy has been fairly effective.

191
00:12:39,440 --> 00:12:43,760
For instance, in December of 2011, President Obama reported

192
00:12:43,760 --> 00:12:48,680
that 22 out of 30 top Al Qaeda leaders had been killed.

193
00:12:48,680 --> 00:12:53,120
Or, to take another example, of the 21 suspected terrorists indicted

194
00:12:53,120 --> 00:12:59,000
for the 1998 US Embassy bombings, 17 have been caught or killed.

195
00:13:00,240 --> 00:13:02,960
On the other hand, if the main goal of the war on terror

196
00:13:02,960 --> 00:13:05,800
is to reduce terrorism, the best available evidence

197
00:13:05,800 --> 00:13:08,040
from terrorism experts who've looked carefully at the

198
00:13:08,040 --> 00:13:12,240
historical record, is that military responses tend to have either

199
00:13:12,240 --> 00:13:16,720
no effect or tend to temporarily increase the level of terrorism.

200
00:13:16,720 --> 00:13:20,170
And that's certainly been the case over the last 10 or 12 years.

201
00:13:20,170 --> 00:13:22,880
For example, before the Iraq war there had not been

202
00:13:22,880 --> 00:13:28,440
a single documented case of suicide terrorism in Iraq -- not one case.

203
00:13:28,440 --> 00:13:33,240
But a 2011 study published in the Lancet, a leading medical journal,

204
00:13:33,240 --> 00:13:36,840
found that after the war began, there were over 1,000

205
00:13:36,840 --> 00:13:41,920
suicide bombings injuring over 30,000 Iraqi civilians and

206
00:13:41,920 --> 00:13:47,100
killing another 12,000, including more than 500 children.

207
00:13:48,230 --> 00:13:52,480
Similarly, suicide attacks were virtually unheard of in Afghanistan

208
00:13:52,480 --> 00:13:57,160
before the war began in 2001, but since 2009, they average

209
00:13:57,160 --> 00:14:01,520
roughly three per week. Using the global terrorism database,

210
00:14:01,520 --> 00:14:04,560
maintained at the University of Maryland, you can see the

211
00:14:04,560 --> 00:14:08,480
increase in terrorist incidents in Iraq and Afghanistan between

212
00:14:08,480 --> 00:14:12,950
1970 and 2010. So here we are at the

213
00:14:12,950 --> 00:14:16,360
Global Terrorism Database website, and we can zoom in

214
00:14:16,360 --> 00:14:22,200
a bit and then click on Data Rivers, which is an interactive tool.

215
00:14:22,200 --> 00:14:25,560
We'll click on the chart again to start it up,

216
00:14:25,560 --> 00:14:29,000
and then we can type in any country of interest.

217
00:14:29,000 --> 00:14:37,160
So, we'll type in "Afghanistan," and what you can see is just a

218
00:14:37,160 --> 00:14:41,990
tremendous increase in terrorist activity beginning in 2001.

219
00:14:41,990 --> 00:14:44,000
Or we can type in "Iraq."

220
00:14:48,760 --> 00:14:50,880
And what you see is much the same story. In fact,

221
00:14:50,880 --> 00:14:55,880
it's even more extreme, because it's over 1,100 incidents.

222
00:14:55,880 --> 00:15:00,560
According to an organization morbidly called "Iraq Body Count,"

223
00:15:00,560 --> 00:15:03,600
which counts Iraqi deaths documented in the media or by

224
00:15:03,600 --> 00:15:06,900
the U.S. government (in other words, almost certainly a

225
00:15:06,900 --> 00:15:10,640
substantial underestimate of the wartime death toll),

226
00:15:10,640 --> 00:15:16,080
there have been more than 122,000 Iraqi civilian deaths

227
00:15:16,080 --> 00:15:20,670
due to violence since the war there began in 2003.

228
00:15:20,670 --> 00:15:24,360
To put this number in perspective, roughly 3,000 people were

229
00:15:24,360 --> 00:15:28,640
killed in the September 11th attacks, roughly 40,000 died

230
00:15:28,640 --> 00:15:32,000
when the U.S. dropped an atomic bomb on Nagasaki,

231
00:15:32,000 --> 00:15:36,480
and roughly 140,000 died when the U.S. dropped an atomic

232
00:15:36,480 --> 00:15:41,800
bomb on Hiroshima -- tragedies almost unimaginable in size.

233
00:15:43,080 --> 00:15:45,040
Now, I've been talking about the increase in

234
00:15:45,040 --> 00:15:48,750
terrorism within Iraq and within Afghanistan, but I should

235
00:15:48,750 --> 00:15:52,880
also add that since 2003, when the Iraq War began,

236
00:15:52,880 --> 00:15:56,000
there's been an increase in terrorism worldwide.

237
00:15:56,440 --> 00:15:59,440
In 2004, the U.S. State Department reported that

238
00:15:59,440 --> 00:16:05,400
there had been 190 terrorist incidents worldwide in 2003.

239
00:16:05,400 --> 00:16:08,640
But in response to criticism that it was covering up an increase

240
00:16:08,640 --> 00:16:11,640
in terrorism, the State Department changed the way that it

241
00:16:11,640 --> 00:16:15,840
measured terrorism, and reported in 2005 that there had been

242
00:16:15,840 --> 00:16:23,000
651 incidents in 2004. Then it changed the methodology again,

243
00:16:23,000 --> 00:16:28,360
and reported over 11,000 incidents in 2005 -- a figure that

244
00:16:28,360 --> 00:16:33,270
didn't change much between 2005 and 2011.

245
00:16:33,270 --> 00:16:36,560
Likewise, here's a chart published by the U.S. Federal Bureau of

246
00:16:36,560 --> 00:16:41,280
Investigation, showing 27 terrorist plots against the United States,

247
00:16:41,280 --> 00:16:45,080
and as you can see, the number of plots increased over the decade

248
00:16:45,080 --> 00:16:50,920
following 9/11, not decreased. Here's what the report said:

249
00:16:50,920 --> 00:16:55,920
"Unfortunately, the appeal of the al Qaeda narrative has not diminished,

250
00:16:55,920 --> 00:17:00,000
and issues, like the war in Iraq, the United States and NATO presence in

251
00:17:00,000 --> 00:17:04,850
Afghanistan, and Guantanamo, serve to inflame and, perhaps,

252
00:17:04,850 --> 00:17:08,700
radicalize those sympathetic to al Qaeda's ideology."

253
00:17:09,900 --> 00:17:13,480
The moral of the story is that if terrorists are angry young men,

254
00:17:13,480 --> 00:17:16,810
then military strikes are probably not going to stop them

255
00:17:16,810 --> 00:17:19,640
and may make matters worse rather than better.

256
00:17:19,640 --> 00:17:22,440
So, the sooner that we take into account the psychological

257
00:17:22,440 --> 00:17:25,720
issues involved, based on sound research about what works

258
00:17:25,720 --> 00:17:29,440
to reduce terrorism and what doesn't -- rather than stereotypes

259
00:17:29,440 --> 00:17:33,000
about crazy terrorists -- the safer all of us will be,

260
00:17:33,000 --> 00:17:37,400
whether we're talking about Al Qaeda or any other sort of terrorism.

261
00:17:37,800 --> 00:17:41,520
Well, if war isn't the answer, what is?

262
00:17:41,520 --> 00:17:45,240
In the long run, education and socialization are critical

263
00:17:45,240 --> 00:17:49,280
so that the next generation doesn't see other groups as the enemy

264
00:17:49,280 --> 00:17:52,150
and violence as the solution.

265
00:17:52,150 --> 00:17:55,520
But there are also steps that can help in the short run.

266
00:17:55,520 --> 00:17:59,280
First, we can fortify self-protective security measures,

267
00:17:59,280 --> 00:18:03,560
which often works well and has the virtue of not being provocative.

268
00:18:03,560 --> 00:18:07,540
Second, we can improve information sharing among governments,

269
00:18:07,540 --> 00:18:10,880
branches of government, and intelligence organizations,

270
00:18:10,880 --> 00:18:13,810
as the 9/11 report advocated.

271
00:18:13,810 --> 00:18:18,480
Third, we can work to build strong alliances, including alliances

272
00:18:18,480 --> 00:18:21,640
with moderate, political, and religious leaders in countries that

273
00:18:21,640 --> 00:18:27,080
do harbor terrorists. Fourth, wealthy countries like the United States

274
00:18:27,080 --> 00:18:31,840
can export medicine and medical technologies rather than weapons.

275
00:18:31,840 --> 00:18:35,000
Personally, I think this is not only the right thing to do,

276
00:18:35,000 --> 00:18:37,400
but one of the most effective things that can be done

277
00:18:37,400 --> 00:18:42,080
to reduce conflict, because there's no faster way to generate goodwill

278
00:18:42,080 --> 00:18:47,200
than to save the life of someone's child, or parent, or friend.

279
00:18:47,200 --> 00:18:51,000
If thanks to medical technology someone's eyesight is restored,

280
00:18:51,000 --> 00:18:55,360
or limbs don't have to be amputated, or a child's life is saved,

281
00:18:55,360 --> 00:19:00,240
it's very dissonance arousing to hate the country that helped.

282
00:19:00,240 --> 00:19:05,440
And finally, find ways to take the third side.

283
00:19:05,440 --> 00:19:07,560
What's the third side?

284
00:19:07,560 --> 00:19:09,940
That's the topic of our next video.

