




1
00:00:06,519 --> 00:00:08,899
Earlier, we discussed the surprisingly 

2
00:00:08,899 --> 00:00:13,399
weak connection that attitudes and 
behavior sometimes have. 

3
00:00:13,399 --> 00:00:17,343
We also discussed cognitive dissonance 
research and saw that when attitudes and 

4
00:00:17,343 --> 00:00:22,940
behavior are connected, it's often the 
case that attitudes follow behavior 

5
00:00:22,940 --> 00:00:27,200
rather than cause behavior. 
In this video, I'd like to focus on 

6
00:00:27,200 --> 00:00:32,290
attitude change and the psychology of 
persuasion, an area in which there's been 

7
00:00:32,290 --> 00:00:37,705
just a tremendous amount of research. 
In fact, by some estimates, more than 

8
00:00:37,705 --> 00:00:44,100
1,000 studies on attitude change are 
published in psychology journals each year. 

9
00:00:44,300 --> 00:00:46,290
So with such a vast research literature, 

10
00:00:46,290 --> 00:00:49,800
we obviously need a way to organize the 
topic, 

11
00:00:49,800 --> 00:00:54,010
and one of the best frameworks to do 
exactly that was developed by 

12
00:00:54,010 --> 00:00:59,960
Bill McGuire, a Yale attitude researcher, 
in a landmark research review. 

13
00:00:59,960 --> 00:01:05,660
McGuire created a matrix with independent 
variables, or variables under the control 

14
00:01:05,660 --> 00:01:11,800
of the experimenter, arranged along the 
top, and mediating or dependent variables, 

15
00:01:11,800 --> 00:01:16,880
what he called "output steps," arranged 
along the side. 

16
00:01:16,880 --> 00:01:21,635
What you see here is a simplified version 
of the matrix adapted from the original. 

17
00:01:21,635 --> 00:01:26,800
McGuire's five categories of independent 
variable were the following: 

18
00:01:26,800 --> 00:01:32,010
Source variables -- that is, variables 
having to do with the persuader, 

19
00:01:32,010 --> 00:01:37,430
the source of the communication. 
Message variables -- that is, variables 

20
00:01:37,430 --> 00:01:41,440
concerning the way a message is 
constructed (so, for example, whether it 

21
00:01:41,440 --> 00:01:47,259
uses humor, 
or fear appeals, or informative statistics, and so on). 

22
00:01:48,100 --> 00:01:50,600
Channel variables -- that is, variables related to 

23
00:01:50,600 --> 00:01:55,500
whether a persuasive appeal is 
given in person, on television, 

24
00:01:55,500 --> 00:02:01,480
in magazine advertisements, the Internet, or 
other channels of communication. 

25
00:02:01,480 --> 00:02:06,620
Receiver variables, or variables having to 
do with the audience -- the receivers or 

26
00:02:06,620 --> 00:02:11,750
recipients of the message. 
And finally, target variables, or 

27
00:02:11,750 --> 00:02:15,700
variables that have to do with the thing 
you're targeting for change, 

28
00:02:15,700 --> 00:02:20,150
such as getting people to wear seat belts or buy a 
product, 

29
00:02:20,150 --> 00:02:25,720
vote for a candidate, and so on. 
The output variables included things such 

30
00:02:25,720 --> 00:02:31,740
as paying attention, liking, 
understanding, agreeing, remembering 

31
00:02:31,740 --> 00:02:35,500
the message, deciding, acting on it, and so 
forth. 

32
00:02:36,860 --> 00:02:40,810
When you think about it, the sheer number 
of variables that you could study, and the 

33
00:02:40,810 --> 00:02:45,330
number of ways these variables could 
interact with each other, is staggering, 

34
00:02:45,330 --> 00:02:50,070
so it's no wonder that there's so many 
studies on attitude change 

35
00:02:50,070 --> 00:02:54,680
(for example, the effects of source 
attractiveness on purchasing decisions 

36
00:02:54,680 --> 00:03:00,230
made by people in their twenties). 
Research on persuasion is also of great 

37
00:03:00,230 --> 00:03:05,450
practical importance, and the results 
aren't always what you'd predict. 

38
00:03:05,450 --> 00:03:09,560
Let's pause for five pop-up questions 
that should illustrate the point. 

39
00:03:13,370 --> 00:03:16,860
Very few people get all five of these 
items correct, 

40
00:03:16,860 --> 00:03:21,600
and even those who do aren't always 100% 
sure beforehand that their answers were 

41
00:03:21,600 --> 00:03:26,440
correct, which suggests that persuasion 
research has something to teach almost 

42
00:03:26,440 --> 00:03:29,580
all of us. 
I know that I continue to be surprised 

43
00:03:29,580 --> 00:03:34,400
when I read new studies on this topic. 
So, how are we going to cover 

44
00:03:34,400 --> 00:03:37,570
this large terrain? 
Well, selectively. 

45
00:03:37,570 --> 00:03:41,710
In this video, I'm going to focus on just 
three questions, 

46
00:03:41,710 --> 00:03:46,040
and the next video -- an animated guest 
lecture by Bob Cialdini and Steve 

47
00:03:46,040 --> 00:03:51,360
Martin -- will focus more broadly on 
"Secrets from the Science of Persuasion," 

48
00:03:51,360 --> 00:03:56,640
a wonderful title and a terrific lecture. 
Anyway, here are the three questions that 

49
00:03:56,640 --> 00:04:01,650
I'd like to focus on. First, if you want to 
be persuasive, is it 

50
00:04:01,650 --> 00:04:06,600
a good idea to explicitly discuss 
counter-arguments to your position? 

51
00:04:06,900 --> 00:04:13,350
Second, should you use a central route to 
persuasion or a more peripheral route? 

52
00:04:13,350 --> 00:04:18,700
(And I'll explain these terms in a moment.) 
And third, should you try to scare the 

53
00:04:18,700 --> 00:04:22,700
receiver -- that is, are fear appeals effective? 

54
00:04:23,340 --> 00:04:27,200
So let's turn to the first question: 
whether to acknowledge counter-arguments --  

55
00:04:27,200 --> 00:04:30,330
that is, arguments against your own 
position. 

56
00:04:31,430 --> 00:04:36,410
Political campaigns often operate on the 
assumption that it's best to stay on the 

57
00:04:36,410 --> 00:04:40,400
offensive, 
ignoring rather than refuting opposition charges. 

58
00:04:41,200 --> 00:04:43,750
Research tends to show, however, that it's better 

59
00:04:43,750 --> 00:04:48,970
to acknowledge and refute counter-arguments 
even before presenting your own 

60
00:04:48,970 --> 00:04:55,150
arguments, under a couple of conditions: 
First, when counter-arguments have been 

61
00:04:55,150 --> 00:04:59,850
made salient by familiarity or 
controversy -- that is, you know that the 

62
00:04:59,850 --> 00:05:04,500
receivers are going to come in contact 
with the opposition anyway -- 

63
00:05:04,690 --> 00:05:11,270
or second, when the receiver is highly intelligent 
or initially opposed to your position. 

64
00:05:11,270 --> 00:05:16,430
In other words, if you know from the 
start that the receiver is smart or 

65
00:05:16,430 --> 00:05:20,760
doesn't agree with your position, it's 
best to begin by acknowledging counter-arguments 

66
00:05:20,760 --> 00:05:26,030
to your position â to present what's known 
as a two-sided appeal. 

67
00:05:26,030 --> 00:05:31,610
"I know that you've heard X, Y, Z, but 
here's what's wrong with X, Y, Z." 

68
00:05:31,610 --> 00:05:36,530
The use of counter-arguments can also be 
effective in building resistance to 

69
00:05:36,530 --> 00:05:40,760
attitude change when people already hold 
the position you're advocating -- that is, 

70
00:05:40,760 --> 00:05:44,660
when they're already on your side. 
Here's how. 

71
00:05:44,660 --> 00:05:49,770
If you mildly criticize the position people 
hold, enough so that they defend their 

72
00:05:49,770 --> 00:05:54,700
position and generate reasons for it, but 
not so much that they change their mind, 

73
00:05:54,700 --> 00:05:59,400
it's like immunizing them with a 
low-dose vaccine. 

74
00:05:59,400 --> 00:06:04,900
In fact, this technique for building 
resistance is known in social psychology 

75
00:06:04,900 --> 00:06:10,830
as "attitude inoculation," and, as covered 
in one of your readings for this week, 

76
00:06:10,830 --> 00:06:15,300
it's been used in everything from political 
campaigns to helping children resist peer 

77
00:06:15,300 --> 00:06:18,950
pressure when it comes to smoking 
cigarettes. 

78
00:06:18,950 --> 00:06:25,250
The difference between a two-sided appeal 
and attitude inoculation is that in a 

79
00:06:25,250 --> 00:06:30,090
two-sided appeal, you raise the 
counter-arguments and then explain why 

80
00:06:30,090 --> 00:06:34,380
they're not convincing,
whereas in attitude inoculation, you 

81
00:06:34,380 --> 00:06:39,340
raise a mild objection, and it's the 
receiver who generates the reasons why 

82
00:06:39,340 --> 00:06:44,720
the objection isn't persuasive. 
The next question that I want to consider 

83
00:06:44,720 --> 00:06:49,600
is whether it's better to use a central 
route to persuasion or a peripheral route 

84
00:06:49,600 --> 00:06:54,000
to persuasion, and this, too, is a topic 
that's covered in more detail in this 

85
00:06:54,000 --> 00:06:58,440
week's assigned reading. 
A message that uses a central route to 

86
00:06:58,440 --> 00:07:03,600
persuasion is a message based on facts, 
statistics, and arguments. 

87
00:07:03,600 --> 00:07:07,930
This is the kind of message that someone 
who's highly involved with the issue 

88
00:07:07,930 --> 00:07:12,470
would find persuasive. 
In contrast, a message that relies on the 

89
00:07:12,470 --> 00:07:17,170
peripheral route to persuasion is a 
message that uses beautiful music, 

90
00:07:17,170 --> 00:07:22,880
idyllic settings, attractive 
models, or other cues that are really 

91
00:07:22,880 --> 00:07:26,710
incidental, or peripheral, to the issue at 
hand. 

92
00:07:26,710 --> 00:07:31,910
Peripheral cues are most effective when 
the audience isn't too involved or critical. 

93
00:07:32,010 --> 00:07:35,590
Most advertisements use a combination of 

94
00:07:35,590 --> 00:07:41,342
central arguments and peripheral cues. 
It's hard to find examples that are 100% 

95
00:07:41,342 --> 00:07:45,210
one or the other, but here's a car advertisement that 

96
00:07:45,210 --> 00:07:50,780
certainly focuses on the central route: 
"A Story in Numbers" that gives the 

97
00:07:50,780 --> 00:07:56,300
vehicle's torque, safety score, and other 
details that a highly involved consumer 

98
00:07:56,300 --> 00:08:01,560
might want to know. 
In contrast, look at this car ad, which 

99
00:08:01,560 --> 00:08:06,750
also appeared in a magazine but is 
probably the most peripheral ad I've ever 

100
00:08:06,750 --> 00:08:10,700
seen for a car. 
In fact, there aren't even any words, 

101
00:08:10,700 --> 00:08:14,130
let alone numbers, and there's no car in sight. 

102
00:08:14,130 --> 00:08:19,600
It's not even clear what the product or 
company is until we zoom in on 

103
00:08:19,600 --> 00:08:25,700
Marilyn Monroe's mole, at which point 
you can see it's Mercedes-Benz. 

104
00:08:26,100 --> 00:08:28,830
What's being sold isn't a specific car -- 

105
00:08:28,830 --> 00:08:34,400
it's an association with beauty and glamour, 
and indeed, the magazine that ran this 

106
00:08:34,400 --> 00:08:36,700
ad was Glamour magazine. 

107
00:08:38,810 --> 00:08:42,905
It can also be interesting sometimes to 
see the central and peripheral routes to 

108
00:08:42,905 --> 00:08:48,670
persuasion in political advertisements. 
Here's a TV commercial from the 2012 

109
00:08:48,670 --> 00:08:52,700
presidential campaign of Vladimir Putin 
in Russia. 

110
00:08:52,700 --> 00:08:56,870
After you watch, I'll ask you a question 
about whether the approach 

111
00:08:56,870 --> 00:09:00,600
was mainly central or mainly peripheral. 

112
00:09:56,400 --> 00:09:58,200
 >> The last question that I want to discuss 

113
00:09:58,200 --> 00:10:01,900
is whether fear appeals tend to 
be persuasive. 

114
00:10:02,490 --> 00:10:08,230
As it turns out, the answer is yes. 
Fear appeals can be pretty effective as 

115
00:10:08,230 --> 00:10:12,500
long as you give people specific steps 
they can take to avoid whatever the 

116
00:10:12,500 --> 00:10:16,120
threat is. 
If you just scare people silly without 

117
00:10:16,120 --> 00:10:20,080
saying how to avoid the threat, fear 
appeals can actually backfire. 

118
00:10:20,080 --> 00:10:24,030
They can drive people into a state of 
denial. 

119
00:10:24,030 --> 00:10:28,390
Returning to the case of political 
advertisements, the specific thing that 

120
00:10:28,390 --> 00:10:31,900
viewers can do is usually quite clear -- 
they can vote for 

121
00:10:31,900 --> 00:10:37,200
a particular candidate or else. 
That's one reason why so many political ads

122
00:10:37,200 --> 00:10:42,200
are based on fear appeals -- that if 
you don't vote for a particular candidate, 

123
00:10:42,200 --> 00:10:46,600
there'll be nuclear war. 
Unemployment will double. 

124
00:10:46,600 --> 00:10:52,300
Taxes will increase. The sky will fall. 

125
00:10:54,600 --> 00:10:57,160
Wow, that was pretty dramatic! 

126
00:10:57,160 --> 00:11:02,000
Let me share with you two classic 
examples of U.S. presidential campaign 

127
00:11:02,000 --> 00:11:05,860
advertisements that were based on fear 
appeals -- one from a Republican, 

128
00:11:05,860 --> 00:11:11,830
the other from a Democrat. 
The first one, courtesy of the Ronald Reagan 

129
00:11:11,830 --> 00:11:17,300
Foundation and Library, tries to 
instill fear by presenting Russia, 

130
00:11:17,400 --> 00:11:22,830
or maybe the Russian military, as a 
potentially dangerous bear. 

131
00:11:22,830 --> 00:11:28,120
The advertisement also uses peripheral 
cues like ominous music and a heartbeat 

132
00:11:28,120 --> 00:11:31,740
to build tension in the background. 
Take a look. 

133
00:11:34,870 --> 00:11:39,900
 >> There is a bear in the woods. 
For some people the bear is easy to see. 

134
00:11:40,200 --> 00:11:45,820
Others don't see it at all. 
Some people say the bear is tame. 

135
00:11:45,820 --> 00:11:51,500
Others say it's vicious and dangerous. 
Since no one can really be sure who's right, 

136
00:11:51,500 --> 00:11:58,040
isn't it smart to be as strong as 
the bear if there is a bear? 

137
00:12:04,800 --> 00:12:07,190
>> The next advertisement, courtesy of the 

138
00:12:07,190 --> 00:12:11,700
Democratic National Committee and the 
Lyndon Banes Johnson Presidential Library, 

139
00:12:11,700 --> 00:12:17,330
is one of the most famous TV 
campaign ads ever shown, 

140
00:12:17,330 --> 00:12:21,400
which is all the more remarkable because 
it was only shown once, 

141
00:12:21,400 --> 00:12:27,250
on September 7, 1964, and it effectively 
branded the Republican 

142
00:12:27,250 --> 00:12:30,970
candidate, Barry Goldwater, as a threat 
to peace. 

143
00:12:34,400 --> 00:12:53,275
 >> One, two, three, four, five, seven, 
six, six, eight, nine, nine. 

144
00:12:53,275 --> 00:13:05,600
 >> Ten, nine, eight, seven, six, five, 
four, three, two, one, zero. 

145
00:13:07,500 --> 00:13:13,400
These are the stakes: to make a world in 
which all of God's children 

146
00:13:13,400 --> 00:13:22,700
can live, or to go into the dark. 
We must either love each other, or we must die. 

147
00:13:23,100 --> 00:13:25,800
 >> Vote for president Johnson on November 3rd. 

148
00:13:26,200 --> 00:13:28,500
The stakes are too high for you to stay home. 

149
00:13:30,400 --> 00:13:32,500
 >> One thing these ads show is that 

150
00:13:32,500 --> 00:13:38,100
televised fear appeals have been part of 
the political landscape for 50 years or more. 

150
00:13:38,200 --> 00:13:40,700
televised fear appeals have been part of 
the political landscape for 50 years or more. 

151
00:13:40,700 --> 00:13:43,800
have been around much longer 
than television. 

152
00:13:43,800 --> 00:13:50,440
For example, the U.S presidential race in 
the year 1800 between the incumbent  

153
00:13:50,440 --> 00:13:55,700
John Adams and the challenger, Thomas 
Jefferson (who ended up winning)  

154
00:13:55,700 --> 00:14:00,920
was extremely negative. 
One can only imagine what a TV commercial 

155
00:14:00,920 --> 00:14:04,620
attacking Thomas Jefferson would have 
looked like. 

156
00:14:04,620 --> 00:14:08,850
Thanks to the Center for New American 
Media, I'll end this video with a brief 

157
00:14:08,850 --> 00:14:14,110
glimpse of how modern day persuasion 
techniques might have been applied if 

158
00:14:14,110 --> 00:14:18,100
television had been around in the year 
1800. 

159
00:14:28,221 --> 00:14:29,600
 >> A nation destroyed. 

160
00:14:29,600 --> 00:14:33,650
Dwellings in flames. 
Gray hairs bathed in blood. 

161
00:14:33,650 --> 00:14:38,000
Female chastity violated. 
Children writhing on the pike and halberd. 

162
00:14:38,100 --> 00:14:40,590
It happened in France, but it could 

163
00:14:40,590 --> 00:14:44,300
happen right here in America if Thomas 
Jefferson is elected President. 

164
00:14:44,900 --> 00:14:50,180
Murder, robbery, rape, and incest will be 
openly taught and practiced. 

165
00:14:50,180 --> 00:14:54,474
The soil will be soaked in blood and the 
nation black with crimes. 

166
00:14:54,474 --> 00:14:59,200
Great God of compassion and justice, 
shield our country from destruction. 

167
00:14:59,200 --> 00:15:00,400
Vote Federalist. 

168
00:15:00,400 --> 00:15:05,100
 >> Paid for by the Adams 1800 Committee, 
Alexander Hamilton, treasurer. 

