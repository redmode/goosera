





1
00:00:06,800 --> 00:00:12,050
Since the 1950's, over 2,000 studies have 
been conducted on cognitive dissonance 

2
00:00:12,050 --> 00:00:18,210
exploring things like who experiences it, 
why they experience it, how they 

3
00:00:18,210 --> 00:00:22,090
experience it, what are the implications, 
and so forth. 

4
00:00:22,090 --> 00:00:26,660
Although there are many different flavors 
of cognitive dissonance, most situations 

5
00:00:26,660 --> 00:00:32,100
fall into one of two general categories. 
The first is "predecisional dissonance," 

6
00:00:32,100 --> 00:00:37,280
in which dissonance or the possibility of 
dissonance influences the decisions 

7
00:00:37,280 --> 00:00:41,430
people make. 
So, in this case dissonance comes before 

8
00:00:41,430 --> 00:00:46,010
people make a decision. 
The other type is "postdecisional dissonance," 

9
00:00:46,010 --> 00:00:48,800
in which dissonance (or again, the 

10
00:00:48,800 --> 00:00:53,170
possibility of it), follows a choice that's 
already been made, 

11
00:00:53,170 --> 00:00:58,300
and efforts to avoid or reduce this 
dissonance affect later judgments. 

12
00:00:58,300 --> 00:01:02,200
So, in this case, dissonance comes after 
a decision. 

13
00:01:03,590 --> 00:01:08,710
The power of predecisional dissonance was 
beautifully illustrated in a classic 

14
00:01:08,710 --> 00:01:13,100
study by Jim Sherman and Larry Gorkin 
published in 1980. 

15
00:01:13,100 --> 00:01:17,430
To understand how it worked, consider the 
following story. 

16
00:01:17,430 --> 00:01:22,850
A father and his son are out driving. 
They are involved in an accident. 

17
00:01:22,850 --> 00:01:26,940
The father is killed, and the son is in 
critical condition. 

18
00:01:26,940 --> 00:01:31,160
The son is rushed to the hospital 
and prepared for the operation. 

19
00:01:31,160 --> 00:01:37,400
The doctor comes in, sees the patient, and 
exclaims, "I can't operate; it's my son!"

20
00:01:38,600 --> 00:01:42,590
Question: Is this story possible? 

21
00:01:42,590 --> 00:01:47,340
Most people, particularly before 1980 
(when the study was published), would answer 

22
00:01:47,340 --> 00:01:50,320
that it's not. 
Their reasoning would be that the patient 

23
00:01:50,320 --> 00:01:54,400
can't be the doctor's son if the patient's 
father has been killed. 

24
00:01:54,400 --> 00:01:58,950
At least, they would reason that way until 
it occurred to them that the surgeon 

25
00:01:58,950 --> 00:02:03,790
might be the patient's mother. 
Now, if you live in a country where women 

26
00:02:03,790 --> 00:02:07,880
can be surgeons, and this possibility 
hadn't occurred to you, and if you 

27
00:02:07,880 --> 00:02:11,900
consider yourself a relatively non-sexist 
person, 

28
00:02:11,900 --> 00:02:17,010
there's a good chance you're experiencing 
dissonance right now -- in this case, 

29
00:02:17,010 --> 00:02:22,600
an uncomfortable feeling that your behavior 
and beliefs are inconsistent. 

30
00:02:23,030 --> 00:02:27,540
Moreover, the theory predicts that if 
you're experiencing cognitive dissonance, 

31
00:02:27,540 --> 00:02:31,495
you'll be motivated to reduce that 
dissonance by behaving in a more 

32
00:02:31,495 --> 00:02:35,880
non-sexist way than ever, 
to show yourself that you're in favor of 

33
00:02:35,880 --> 00:02:40,172
equality between women and men. 
At least, that's what Sherman and Gorkin 

34
00:02:40,172 --> 00:02:43,632
hypothesized. In their experiment, 
they randomly assigned 

35
00:02:43,632 --> 00:02:49,450
77 college students, male and 
female, to one of three conditions in an 

36
00:02:49,450 --> 00:02:53,170
experiment on 
"the relationship between attitudes 

37
00:02:53,170 --> 00:02:58,060
toward social issues and the ability to 
solve logical problems." 

38
00:02:58,060 --> 00:03:02,400
In the sex role condition, students were 
given five minutes to explain how the 

39
00:03:02,400 --> 00:03:08,260
story of the female surgeon made sense. 
In the non-sex role condition, students 

40
00:03:08,260 --> 00:03:12,890
were given five minutes to solve an 
equally difficult problem concerning dots 

41
00:03:12,890 --> 00:03:15,920
and lines. 
That way, Sherman and Gorkin could make 

42
00:03:15,920 --> 00:03:20,900
sure that the results weren't simply due 
to students working on a difficult problem. 

43
00:03:21,400 --> 00:03:23,760
And in the control condition, students 

44
00:03:23,760 --> 00:03:28,770
were not given a problem to solve. 
In the sex-role and non-sex-role 

45
00:03:28,770 --> 00:03:33,380
conditions, the experimenter provided the 
correct solution after five minutes 

46
00:03:33,380 --> 00:03:37,230
had passed. 
Roughly 80% of the students were not able 

47
00:03:37,230 --> 00:03:40,880
to solve the problems within five 
minutes. 

48
00:03:40,880 --> 00:03:45,530
Next, the experimenter told students that 
the study was over, and she gave them 

49
00:03:45,530 --> 00:03:50,300
booklets for another person's study about 
legal decisions. 

50
00:03:50,300 --> 00:03:53,960
This wasn't a surprise, by the way. 
The students had been told previously 

51
00:03:53,960 --> 00:03:59,130
that they'd be participating in a couple 
of unrelated research projects. 

52
00:03:59,130 --> 00:04:03,400
The experimenter explained that the other 
researcher was out of town 

53
00:04:03,400 --> 00:04:07,490
and that they should put their completed 
booklet into an envelope addressed to 

54
00:04:07,490 --> 00:04:12,450
that person and drop the envelope in a 
nearby outbox. 

55
00:04:12,450 --> 00:04:17,300
Then she left, seemingly never to see 
their answers to the second experiment. 

56
00:04:18,350 --> 00:04:20,700
In reality, the second experiment was 

57
00:04:20,700 --> 00:04:25,460
nothing more than a cover story to 
collect information on sexism without 

58
00:04:25,460 --> 00:04:29,960
students detecting a connection to the 
first part of the experiment. 

59
00:04:29,960 --> 00:04:35,240
There was no second experiment. 
Pretty tricky, pretty tricky. 

60
00:04:35,240 --> 00:04:39,810
In this part of the study, students read 
about an affirmative action legal case in 

61
00:04:39,810 --> 00:04:43,610
which a woman claimed that she had been 
turned down for a university faculty 

62
00:04:43,610 --> 00:04:48,820
position on the basis of her gender. 
Students then gave their opinion about 

63
00:04:48,820 --> 00:04:52,450
three things: 
First, what they thought the verdict 

64
00:04:52,450 --> 00:04:55,980
should be. 
Second, how justified they thought the 

65
00:04:55,980 --> 00:04:59,940
university was in hiring a man rather 
than the woman. 

66
00:04:59,940 --> 00:05:04,590
And third, how they felt about 
affirmative action in general. 

67
00:05:04,590 --> 00:05:08,270
What were the results? 
Sherman and Gorkin found that compared 

68
00:05:08,270 --> 00:05:12,560
with students in the control group, and 
compared with students who were presented 

69
00:05:12,560 --> 00:05:15,190
with the problem concerning dots and 
lines, 

70
00:05:15,190 --> 00:05:20,530
people who had failed to solve the female 
surgeon problem were more likely to find 

71
00:05:20,530 --> 00:05:23,840
the university guilty of sex 
discrimination, 

72
00:05:23,840 --> 00:05:29,690
less likely to see the university as 
justified in hiring a male for the job, 

73
00:05:29,690 --> 00:05:33,310
and more supportive of affirmative action 
policies in general. 

74
00:05:34,460 --> 00:05:38,990
Based on these findings, Sherman and 
Gorkin concluded that students who failed 

75
00:05:38,990 --> 00:05:44,075
the female surgeon problem tried to 
reduce their dissonance by acting as 

76
00:05:44,075 --> 00:05:48,480
pro-equality as possible, 
trying to show themselves that they 

77
00:05:48,480 --> 00:05:54,112
weren't biased against women. 
Let me share one other study on predecisional 

78
00:05:54,112 --> 00:05:59,880
dissonance -- an entertaining and 
startling experiment published by Ronald 

79
00:05:59,880 --> 00:06:05,440
Comer and James Laird in an article entitled, 
"Choosing to Suffer as a 

80
00:06:05,440 --> 00:06:10,069
Consequence of Expecting to Suffer: 
Why Do People Do It?"

81
00:06:11,270 --> 00:06:15,730
In the study, 50 college students were 
assigned to one of three experimental 

82
00:06:15,730 --> 00:06:20,880
conditions: 
worm expectancy/worm choice, 

83
00:06:20,880 --> 00:06:26,710
worm expectancy/shock choice, and 
neutral expectancy control group. 

84
00:06:26,710 --> 00:06:30,580
I'll explain the right two columns in a 
few minutes, but first, let's just walk 

85
00:06:30,580 --> 00:06:33,670
through the study. 
As soon as students entered the 

86
00:06:33,670 --> 00:06:38,950
laboratory, they saw a table set up for 
the experimental tasks. 

87
00:06:38,950 --> 00:06:43,210
On one side of the table, there were 
eight covered cups weighing different 

88
00:06:43,210 --> 00:06:48,330
amounts for a weight discrimination task -- 
you know, judging which cup is heavier. 

89
00:06:49,470 --> 00:06:54,530
In the worm expectancy/shock choice 
condition, there was also an electric 

90
00:06:54,530 --> 00:07:00,880
shock apparatus sitting on the table. 
And in another area, there was a plate 

91
00:07:00,880 --> 00:07:09,280
with a dead worm on it, a cup of water, a napkin, 
and a fork for a worm eating task. 

92
00:07:09,280 --> 00:07:12,750
And by the way, no worms were harmed in 
the making of this video -- 

93
00:07:12,750 --> 00:07:15,770
that's a candy gummy worm, not a real 
worm. 

94
00:07:16,860 --> 00:07:21,460
The experimenter then read a statement 
reminding students that they were free to 

95
00:07:21,460 --> 00:07:25,150
refuse or to terminate participating at 
any time. 

96
00:07:27,530 --> 00:07:32,150
Three participants immediately pulled the 
ripcord and bailed out of the study. 

97
00:07:32,150 --> 00:07:37,560
But the other 47 students remained, and a 
post-experimental interview indicated 

98
00:07:37,560 --> 00:07:42,530
that they fully expected to eat a worm 
before the study was over. 

99
00:07:42,530 --> 00:07:46,920
Students in all three experimental 
conditions began by completing a 

100
00:07:46,920 --> 00:07:51,980
personality questionnaire that asked, 
among other things, how brave they thought 

101
00:07:51,980 --> 00:07:55,430
they were 
and how much they deserved to suffer. 

102
00:07:56,550 --> 00:08:01,750
Then the experimenter described each of 
the experimental tasks, seated students 

103
00:08:01,750 --> 00:08:05,880
in front of the task that they were 
assigned, and left the room to take care 

104
00:08:05,880 --> 00:08:11,400
of some preliminary details. 
Students in the neutral expectancy control group 

105
00:08:11,400 --> 00:08:15,430
were assigned to the weight discrimination task, 

106
00:08:15,430 --> 00:08:20,930
and students in the two worm expectancy 
groups were assigned to the worm eating 

107
00:08:20,930 --> 00:08:24,800
task -- 
that is, they expected to eat a worm. 

108
00:08:25,500 --> 00:08:30,540
Ten minutes later, after students seated 
in front of the dead worm had been given 

109
00:08:30,540 --> 00:08:35,690
lots of time to contemplate their fate, 
the experimenter returned and announced 

110
00:08:35,690 --> 00:08:40,730
that there would be a short delay. 
The experimenter said, "While you're 

111
00:08:40,730 --> 00:08:45,350
waiting, I was thinking, it's been quite 
a while since you filled out the pretask 

112
00:08:45,350 --> 00:08:49,870
personality survey. 
I don't even know if it's valid anymore. 

113
00:08:49,870 --> 00:08:55,420
Would you just fill it out again now?" 
In this way, the participants were led to 

114
00:08:55,420 --> 00:09:01,640
provide a second self-rating of how brave 
they were and how much they deserved to suffer -- 

115
00:09:01,640 --> 00:09:05,340
so the researchers could see whether 
these ratings changed at all during the 

116
00:09:05,340 --> 00:09:09,800
ten minutes or so that students had sat 
in front of their task. 

117
00:09:09,800 --> 00:09:14,450
Then, a few minutes later, the experimenter 
returned and asked students to rate how 

118
00:09:14,450 --> 00:09:19,930
pleasant or unpleasant the tasks were. 
Students simply gave their general 

119
00:09:19,930 --> 00:09:24,800
impressions because they hadn't actually 
performed the assigned task. 

120
00:09:24,800 --> 00:09:30,180
Students in the control condition and the 
worm choice condition evaluated the 

121
00:09:30,180 --> 00:09:34,260
weight discrimination task and the worm 
eating task. 

122
00:09:34,260 --> 00:09:38,020
And students in the shock choice 
condition evaluated the weight 

123
00:09:38,020 --> 00:09:43,840
discrimination task and a shock task in 
which they would give themselves a shock 

124
00:09:43,840 --> 00:09:47,780
on the hand. 
Finally, the experimenter shuffled 

125
00:09:47,780 --> 00:09:52,440
through his papers again and declared 
that -- whoops! -- the correct experimental 

126
00:09:52,440 --> 00:09:56,830
condition hadn't been assigned, 
and students would now be able to choose 

127
00:09:56,830 --> 00:10:02,570
the task they wanted to complete. 
Students in the control condition or the 

128
00:10:02,570 --> 00:10:07,360
worm choice condition were given a choice 
between doing the weight discrimination 

129
00:10:07,360 --> 00:10:12,240
task or the worm eating task, 
and students in the shock choice 

130
00:10:12,240 --> 00:10:17,550
condition were given a choice between the 
weight discrimination task and the shock 

131
00:10:17,550 --> 00:10:20,940
task --  
that is, a task that involved 

132
00:10:20,940 --> 00:10:25,210
the self-infliction of suffering 
but didn't have to do with worms. 

133
00:10:26,830 --> 00:10:30,970
After students indicated their 
preference, they were fully debriefed, 

134
00:10:30,970 --> 00:10:34,630
meaning that they were informed about the 
purpose of the experiment. 

135
00:10:34,630 --> 00:10:39,060
You'll be happy to hear that none of the 
students were actually required to eat 

136
00:10:39,060 --> 00:10:44,230
a worm, which may have come as a 
disappointment to some of them 

137
00:10:44,230 --> 00:10:49,810
because, when given the choice, 12 of the 
15 people assigned to the worm choice 

138
00:10:49,810 --> 00:10:55,670
condition preferred to eat the worm. 
And 10 of the 20 people in the shock 

139
00:10:55,670 --> 00:11:01,270
choice condition chose to give themselves 
painful electric shocks. 

140
00:11:01,270 --> 00:11:06,720
That is, a large number of students chose 
to suffer through a disgusting or painful 

141
00:11:06,720 --> 00:11:12,480
task rather than opting for the harmless 
weight discrimination task. 

142
00:11:12,480 --> 00:11:17,890
In contrast, all 15 people in the control 
condition chose the weight discrimination 

143
00:11:17,890 --> 00:11:23,750
task over the worm eating task. 
For people in the control condition, 

144
00:11:23,750 --> 00:11:28,170
a typical response might be, "Well, you 
know, thank you for giving me the choice 

145
00:11:28,170 --> 00:11:30,960
but I think that I'm just going to stick 
with the weight discrimination task. 

146
00:11:30,960 --> 00:11:35,020
You know, I've been really trying to cut 

147
00:11:35,020 --> 00:11:40,420
down on the number of worms that I eat, 
but thanks for thinking of me." 

148
00:11:40,420 --> 00:11:44,660
Now put on your dissonance hat for a 
second and answer the following 

149
00:11:44,660 --> 00:11:49,500
pop-up question: 
Why would so many people choose to suffer?

150
00:11:52,400 --> 00:11:54,570
The purpose in asking you this question 

151
00:11:54,570 --> 00:11:58,340
was simply to stimulate your thinking -- it 
wasn't to see if you have the correct answer, 

152
00:11:58,340 --> 00:12:01,980
because there isn't a single correct answer. 

153
00:12:01,980 --> 00:12:04,750
What the researchers concluded is that 

154
00:12:04,750 --> 00:12:10,223
most students who expected to eat a worm 
changed their beliefs in at least one of 

155
00:12:10,223 --> 00:12:13,920
three ways: 
First, they convinced themselves that 

156
00:12:13,920 --> 00:12:19,070
eating a worm wasn't that bad after all. 
They convinced themselves that they  

157
00:12:19,070 --> 00:12:22,460
were brave. 
Or they convinced themselves that they 

158
00:12:22,460 --> 00:12:26,500
deserved to suffer. 
By changing their beliefs this way, 

159
00:12:26,500 --> 00:12:31,450
students were presumably able to reduce 
the dissonance caused by agreeing to 

160
00:12:31,450 --> 00:12:37,380
suffer for no good reason. 
In commenting on how people will adapt 

161
00:12:37,380 --> 00:12:40,540
their belief systems to fit their 
situation, 

162
00:12:40,540 --> 00:12:45,700
even if the situation is assigned at 
random as in the case of an experiment, 

163
00:12:45,700 --> 00:12:51,590
Comer and Laird wrote the following: 
"Although it is superficially striking to 

164
00:12:51,590 --> 00:12:56,570
observe people choosing to eat a worm, 
the more impressive aspect of these 

165
00:12:56,570 --> 00:13:01,710
findings is the degree to which people 
will change their conceptual system to 

166
00:13:01,710 --> 00:13:05,140
make sense of the random events of their 
lives." 

167
00:13:06,440 --> 00:13:10,805
One can only wonder whether the same sort 
of mechanism is involved in other 

168
00:13:10,805 --> 00:13:15,180
self-destructive situations. 
For example, think about battered women 

169
00:13:15,180 --> 00:13:20,020
who choose to stay with their boyfriend 
or their husband and tell themselves that 

170
00:13:20,020 --> 00:13:23,390
they deserve to suffer or that they're 
brave, 

171
00:13:23,390 --> 00:13:28,350
or that it's really not that bad. 
Well, that's predecisional dissonance 

172
00:13:28,350 --> 00:13:32,110
because the dissonance is influencing 
later decisions -- 

173
00:13:32,110 --> 00:13:37,500
whether to stay with a boyfriend, whether 
to render a guilty verdict against a 

174
00:13:37,500 --> 00:13:40,600
university, 
whether to eat a worm, and so on. 

175
00:13:41,200 --> 00:13:44,529
What about postdecisional dissonance -- 
how does that work? 

176
00:13:45,660 --> 00:13:49,170
Well, one of the most straightforward 
demonstrations of postdecisional 

177
00:13:49,170 --> 00:13:55,650
dissonance -- really, about as vanilla as you 
can get -- was published in the 1960s by 

178
00:13:55,650 --> 00:14:01,480
Robert Knox and James Inkster. 
This was a very simple study in which 

179
00:14:01,480 --> 00:14:07,760
they approached 141 horse betters at 
Exhibition Park Race Track in Vancouver, 

180
00:14:07,760 --> 00:14:11,920
Canada -- 
69 people who were about to place a $2 

181
00:14:11,920 --> 00:14:18,780
bet in the next 30 seconds, and 72 people 
who had just finished placing a $2 bet 

182
00:14:18,780 --> 00:14:23,620
within the past 30 seconds. 
Knox and Inkster reasoned that people who 

183
00:14:23,620 --> 00:14:26,128
had just committed themselves to a course 
of action 

184
00:14:26,128 --> 00:14:32,400
by betting $2 would reduce any 
postdecisional dissonance by believing 

185
00:14:32,400 --> 00:14:36,390
more strongly than ever that they had 
picked a winner. 

186
00:14:36,390 --> 00:14:41,260
To test this hypothesis, Knox and Inkster 
asked people to rate their horse's chances 

187
00:14:41,260 --> 00:14:46,650
of winning on a seven-point scale 
in which a 1 indicated that the chances 

188
00:14:46,650 --> 00:14:52,420
were "slight" and a 7 indicated that 
the chances were "excellent." 

189
00:14:52,420 --> 00:14:56,960
According to their results, people who 
were about to place a bet rated the 

190
00:14:56,960 --> 00:15:03,820
chances that their horse would win at an 
average of 3.48, which corresponded to a 

191
00:15:03,820 --> 00:15:07,910
"fair chance of winning." 
On the other hand, people who had just 

192
00:15:07,910 --> 00:15:14,720
finished betting gave an average rating 
of 4.81, which corresponded to a "good 

193
00:15:14,720 --> 00:15:19,430
chance of winning." 
So the hunch that Knox and Inkster had 

194
00:15:19,430 --> 00:15:23,090
was right. 
In less than 60 seconds, a $2 commitment 

195
00:15:23,090 --> 00:15:27,900
increased people's 
confidence that they had picked a winner. 

196
00:15:27,900 --> 00:15:30,510
And the same finding holds when people 
vote. 

197
00:15:30,510 --> 00:15:34,360
People enter the voting booth with a 
certain degree of confidence, 

198
00:15:34,360 --> 00:15:37,810
but after pulling the lever or dropping 
a ballot into the box, 

199
00:15:37,810 --> 00:15:40,750
they're more likely to believe that their candidate 
will win 

200
00:15:40,750 --> 00:15:44,240
because they don't want to feel 
dissonance from the thought that they 

201
00:15:44,240 --> 00:15:50,310
just threw away their vote on a loser. 
Two other notes before we end: 

202
00:15:50,310 --> 00:15:55,650
One about how universal cognitive 
dissonance is ( for example, does it occur 

203
00:15:55,650 --> 00:16:01,800
in the East, as well as West) and the other 
about how useful it is to understand 

204
00:16:01,800 --> 00:16:07,086
cognitive dissonance theory. 
On the first question, research suggests 

205
00:16:07,086 --> 00:16:11,170
that cognitive dissonance does occur 
across the globe, 

206
00:16:11,170 --> 00:16:15,790
but the form it takes is somewhat 
different country to country. 

207
00:16:15,790 --> 00:16:19,840
Here in the West, we tend to feel 
dissonance from inconsistencies that 

208
00:16:19,840 --> 00:16:24,110
might suggest we're incompetent, or bad 
in some way, 

209
00:16:24,110 --> 00:16:27,900
whereas people in the East tend to be 
more concerned about choices and 

210
00:16:27,900 --> 00:16:31,540
behaviors that could lead to social 
rejection -- 

211
00:16:31,540 --> 00:16:35,690
for example, bad choices made on behalf 
of other people. 

212
00:16:36,800 --> 00:16:39,840
The second question has to do with 
usefulness, 

213
00:16:39,840 --> 00:16:45,330
and to address that question, I'd like to 
discuss a very interesting 2012 study 

214
00:16:45,330 --> 00:16:50,300
published in the Proceedings of the 
National Academy of Sciences -- a study 

215
00:16:50,300 --> 00:16:54,900
on cheating. 
The Snapshot Quiz included the following 

216
00:16:54,900 --> 00:16:59,200
item on this topic: 
"If you want to reduce cheating on things 

217
00:16:59,200 --> 00:17:05,000
like tax returns or exams, would it be 
more effective to have people sign a 

218
00:17:05,000 --> 00:17:11,000
declaration of honesty, an honor code, at 
the top or the bottom 

219
00:17:11,000 --> 00:17:15,800
of the tax return or exam?" 
Let's pause so that you can see how you answered.

220
00:17:19,000 --> 00:17:21,000
What these researchers found is that 

221
00:17:21,000 --> 00:17:24,910
people were much less likely to cheat if 
they signed a statement at the 

222
00:17:24,910 --> 00:17:29,000
beginning of a tax form, 
which would make it dissonance-arousing 

223
00:17:29,000 --> 00:17:34,200
to cheat on the questions that followed. 
Through a tricky procedure using a mock 

224
00:17:34,200 --> 00:17:37,200
tax form, 
the researchers were able to determine 

225
00:17:37,200 --> 00:17:43,000
that 63% of people in their particular 
study were honest when they signed a 

226
00:17:43,000 --> 00:17:47,730
statement at the top that said: "I declare 
that I will carefully examine this 

227
00:17:47,730 --> 00:17:51,000
return and that to the best of my knowledge and 

228
00:17:51,000 --> 00:17:58,920
belief, it is correct and complete." 
That number, 63%, fell to 21% in another 

229
00:17:58,920 --> 00:18:04,200
experimental condition when people signed 
an equivalent statement at the bottom of 

230
00:18:04,200 --> 00:18:08,640
a tax form, after they had already 
answered the questions. 

231
00:18:08,640 --> 00:18:13,340
And I should add that 36% of people were 
honest when not asked to sign a statement 

232
00:18:13,340 --> 00:18:20,980
of any kind -- significantly different from 
63% but not from 21%. 

233
00:18:20,980 --> 00:18:26,030
So, the bottom line is that if you want to 
reduce cheating, the signature shouldn't 

234
00:18:26,030 --> 00:18:30,900
go in the bottom line. 
It shouldn't go at the end of an exam or 

235
00:18:30,900 --> 00:18:33,250
a tax form that has already been 
completed. 

236
00:18:33,250 --> 00:18:38,200
Yet this is exactly where taxpayers in 
the United States are currently asked to 

237
00:18:38,200 --> 00:18:43,590
sign -- at the bottom, where it has no effect 
on cheating. 

238
00:18:44,690 --> 00:18:48,490
And that really illustrates the 
concluding point that I wanted to make -- 

239
00:18:48,490 --> 00:18:52,900
that knowing something about cognitive 
dissonance theory is useful. 

240
00:18:53,100 --> 00:18:57,180
In the next video we'll look at another 
research area that's got all sorts of 

241
00:18:57,180 --> 00:19:02,200
practical applications -- the psychology of 
persuasion. 

