




1
00:00:06,810 --> 00:00:10,800
In the last two lectures, we focused on 
how people interpret and explain 

2
00:00:10,800 --> 00:00:13,630
behavior. 
In this video, we're going to move to a 

3
00:00:13,630 --> 00:00:18,040
discussion of behavior itself, and the 
question of whether our attitudes 

4
00:00:18,040 --> 00:00:21,080
and our actions are closely related to each 
other. 

5
00:00:21,080 --> 00:00:25,815
Put briefly, the bottom line is that in 
many instances there's far less 

6
00:00:25,815 --> 00:00:31,170
consistency between attitudes and 
behavior than one might suspect. 

7
00:00:31,170 --> 00:00:35,270
Most people imagine that the attitude 
bone's connected to the behavior bone, 

8
00:00:35,270 --> 00:00:39,270
but it just ain't so. 
For all sorts of good reasons, attitudes 

9
00:00:39,270 --> 00:00:44,550
are often quite different than behavior. 
For example, nearly all of us would say 

10
00:00:44,550 --> 00:00:47,930
that we care about the environment, and 
we all know that it's not good for the 

11
00:00:47,930 --> 00:00:52,610
environment if we use styrofoam or 
plastic products once and throw them 

12
00:00:52,610 --> 00:00:55,470
away, but at some point over the past year 

13
00:00:55,470 --> 00:01:00,050
that's exactly what many of us have done, 
according to answers that our class gave 

14
00:01:00,050 --> 00:01:04,140
in the Snapshot Quiz. 
In social psychology, that would be 

15
00:01:04,140 --> 00:01:08,170
considered an example of attitude-behavior 
inconsistency. 

16
00:01:08,170 --> 00:01:11,910
We hold certain attitudes about the 
environment, we hold certain attitudes 

17
00:01:11,910 --> 00:01:16,510
about plastic products, yet for a variety 
of reasons — some of which may be quite 

18
00:01:16,510 --> 00:01:20,310
legitimate — we behave at variance with 
our attitudes. 

19
00:01:20,310 --> 00:01:23,220
We behave differently than the attitudes 
that we hold. 

20
00:01:23,220 --> 00:01:28,320
Now, that's an example of plastic cups. 
What about cases in which we have very 

21
00:01:28,320 --> 00:01:33,070
powerful attitudes, very deeply-held 
convictions and core beliefs? 

22
00:01:33,070 --> 00:01:38,440
Would we still see a discrepancy between 
attitudes and behavior in that case? 

23
00:01:38,440 --> 00:01:43,425
To answer this question, I want to turn 
to two classic studies: one done in the 

24
00:01:43,425 --> 00:01:50,580
1930's, the other in the 1970's. 
The first study, published in 1934 by a 

25
00:01:50,580 --> 00:01:54,783
Stanford sociologist named Richard 
LaPiere, is the one that really put 

26
00:01:54,783 --> 00:01:58,659
attitude-behavior inconsistency on the 
map. 

27
00:01:58,659 --> 00:02:05,020
Beginning in 1930 and continuing for two 
full years, LaPiere traveled extensively 

28
00:02:05,020 --> 00:02:07,770
throughout the United States with a young 
Chinese couple. 

29
00:02:07,770 --> 00:02:15,310
They visited 184 restaurants and cafes, 
and 67 hotels, auto camps, and 

30
00:02:15,310 --> 00:02:20,560
tourist homes scattered across the country. 
Despite the intense anti-Chinese 

31
00:02:20,560 --> 00:02:24,950
prejudice common in those days, LaPiere 
observed racial discrimination 

32
00:02:24,950 --> 00:02:30,400
toward his traveling companions only once in 251 
encounters. 

33
00:02:30,400 --> 00:02:35,950
In fact, LaPiere judged that his Chinese 
friends were received with 

34
00:02:35,950 --> 00:02:41,897
"more than ordinary consideration" 
on 72 occasions. 

35
00:02:41,897 --> 00:02:46,480
This was extraordinary given the anti-Chinese 
policies and practices that were 

36
00:02:46,480 --> 00:02:51,996
common in the United States back then. 
In fact, roughly 50 years earlier, 

37
00:02:51,996 --> 00:02:58,190
the U.S. Congress had passed a horrible 
Chinese Exclusion Act, barring Chinese 

38
00:02:58,190 --> 00:03:02,560
workers from even entering the country, 
let alone eating in restaurants — 

39
00:03:02,560 --> 00:03:07,850
a law that wasn't repealed until 10 years 
after LaPiere's study. 

40
00:03:07,850 --> 00:03:12,250
Based on his experience during the road 
trip, LaPiere concluded that one would 

41
00:03:12,250 --> 00:03:17,432
never suspect a majority of Americans as 
being prejudiced against the Chinese — 

42
00:03:17,432 --> 00:03:22,080
that is, if you looked only at how people 
behaved toward the Chinese couple. 

43
00:03:22,080 --> 00:03:27,870
The prejudice most Americans had was very 
apparent in terms of attitudes, 

44
00:03:27,870 --> 00:03:33,080
as expressed later on a survey. 
This survey was sent to the proprietors 

45
00:03:33,080 --> 00:03:37,550
of each establishment six months after 
LaPiere and the couple had visited, 

46
00:03:37,550 --> 00:03:41,820
and it asked the proprietors, among other 
things, whether they would accept Chinese 

47
00:03:41,820 --> 00:03:46,710
people as guests in their establishment. 
With persistence, LaPiere was able to 

48
00:03:46,710 --> 00:03:51,730
get responses from 81 different 
restaurants and cafes, and 47 different 

49
00:03:51,730 --> 00:04:00,540
hotels, auto camps, and tourist homes. 
Of the 128 respondents, 118 (or 92%) 

50
00:04:00,540 --> 00:04:03,981
indicated that they would not accept 
Chinese guests. 

51
00:04:03,981 --> 00:04:08,550
Nine of the respondents (or 7%) said 
their decision would depend on the 

52
00:04:08,550 --> 00:04:12,480
circumstances, and one woman from 
an auto camp — bless her heart -- 

53
00:04:12,480 --> 00:04:16,880
replied affirmatively, stating 
that she had hosted a Chinese couple 

54
00:04:16,880 --> 00:04:20,694
during the previous summer (that is, 
LaPiere's friends!). 

55
00:04:20,694 --> 00:04:24,900
Also, LaPiere wanted to make sure that 
these results weren't a function of the 

56
00:04:24,900 --> 00:04:30,310
Chinese couple having turned people off, 
so he surveyed 128 establishments that 

57
00:04:30,310 --> 00:04:34,810
were located in similar regions of the 
country but hadn't been visited 

58
00:04:34,810 --> 00:04:38,800
by the Chinese couple. 
The results were identical. 

59
00:04:38,800 --> 00:04:45,070
Again, he got 118 negative responses, 
9 responses saying that it would depend, 

60
00:04:45,070 --> 00:04:50,690
and 1 affirmative response. 
So, it seems that people can hold 

61
00:04:50,690 --> 00:04:53,860
attitudes that have very little to do 
with actual behavior. 

62
00:04:53,860 --> 00:04:58,330
But of course, experimental methods were 
not very developed back in the 1930s. 

63
00:04:58,330 --> 00:05:02,800
So, even though this was considered a 
groundbreaking study at the time, it had 

64
00:05:02,800 --> 00:05:07,390
some serious limitations. 
For example, there weren't independent 

65
00:05:07,390 --> 00:05:11,800
ratings of discrimination — just LaPiere's 
personal opinion. 

66
00:05:11,800 --> 00:05:15,130
It's also possible that the Chinese 
couple would have encountered more 

67
00:05:15,130 --> 00:05:19,150
discrimination if Professor LaPiere 
hadn't been with them. 

68
00:05:19,150 --> 00:05:22,520
And the study used only one couple, 
so we don't know what would have 

69
00:05:22,520 --> 00:05:27,450
happened to other Chinese couples. 
Maybe LaPiere's friends were unusually 

70
00:05:27,450 --> 00:05:30,230
charming. 
This isn't to say that we should ignore 

71
00:05:30,230 --> 00:05:33,280
the results, or that the conclusions were 
wrong — it's simply to say that the 

72
00:05:33,280 --> 00:05:37,850
research methods were limited. 
So, let's turn to that second classic 

73
00:05:37,850 --> 00:05:42,290
study that I mentioned, which had better 
experimental controls, and which in some 

74
00:05:42,290 --> 00:05:46,610
ways was even more dramatic in its 
demonstration of how attitudes and 

75
00:05:46,610 --> 00:05:48,580
behavior can be different from each 
other. 

76
00:05:48,580 --> 00:05:53,970
This study was published in 1973 by John 
Darley and Dan Batson. 

77
00:05:55,110 --> 00:05:59,240
Darley and Batson were interested in the 
factors that lead people to help someone 

78
00:05:59,240 --> 00:06:03,460
who's in trouble. 
Their participants were 47 seminary 

79
00:06:03,460 --> 00:06:06,210
students (that is, students training to 
become 

80
00:06:06,210 --> 00:06:10,200
a priest or a minister), 
and in the experiment, students were 

81
00:06:10,200 --> 00:06:14,610
asked to give a speech either about jobs 
at which seminary students would be 

82
00:06:14,610 --> 00:06:17,920
effective (in other words, an open-ended 
topic that 

83
00:06:17,920 --> 00:06:23,370
doesn't really have a specific focus),
or a speech on the Parable of the Good 

84
00:06:23,370 --> 00:06:26,420
Samaritan. 
For those of you who haven't heard of it before, 

85
00:06:26,420 --> 00:06:31,950
this is a Bible story about the 
importance of helping people in need. 

86
00:06:31,950 --> 00:06:37,110
In the story, a man traveling from 
Jerusalem to Jericho is robbed, 

87
00:06:37,110 --> 00:06:43,100
he's beaten savagely, left by the side of the 
road, and after a priest walks past him 

88
00:06:43,100 --> 00:06:47,020
without offering any help, a Samaritan 
comes to his aid. 

89
00:06:48,480 --> 00:06:52,630
In the study, an experimental assistant 
told students that their speeches would 

90
00:06:52,630 --> 00:06:57,290
be three to five minutes long and would 
be recorded by another assistant in a 

91
00:06:57,290 --> 00:07:00,500
different building. 
But as students went to that building, 

92
00:07:00,500 --> 00:07:05,000
they encountered a confederate who 
appeared to be in need. 

93
00:07:05,000 --> 00:07:08,530
In particular, Darley and Batson were 
interested to see whether helping was 

94
00:07:08,530 --> 00:07:11,620
related to two different independent 
variables 

95
00:07:11,620 --> 00:07:15,430
(that is, variables under the control of 
the experimenters): 

96
00:07:15,430 --> 00:07:20,470
first, whether students were about to 
discuss the Parable of the Good Samaritan 

97
00:07:20,470 --> 00:07:26,310
versus a topic unrelated to helping, and second, how much of a hurry the 

98
00:07:26,310 --> 00:07:29,200
students were in to get where they were 
going. 

99
00:07:289,200 --> 00:07:32,870
In the high-hurry condition, the 
experimental assistant looked at his 

100
00:07:32,870 --> 00:07:38,440
watch and said, "Oh, you're late. They 
were expecting you a few minutes ago. We'd 

101
00:07:38,440 --> 00:07:41,270
better get moving. 
The assistant should be waiting for you 

102
00:07:41,270 --> 00:07:45,430
so you'd better hurry. It shouldn't take 
but just a minute." 

103
00:07:45,430 --> 00:07:48,950
And with that, the seminary student was 
whisked out the door. 

104
00:07:49,720 --> 00:07:53,640
In the intermediate-hurry condition 
students were told, "The assistant is 

105
00:07:53,640 --> 00:07:59,800
ready for you, so please go right over." 
And in the low-hurry condition, students 

106
00:07:59,800 --> 00:08:03,850
were told, "It'll be a few minutes before 
they're ready for you, but you might as 

107
00:08:03,850 --> 00:08:07,850
well head on over. If you have to wait 
over there, it shouldn't be long." 

108
00:08:08,980 --> 00:08:13,200
Now, in order to get from one building to 
the other, each student had to pass 

109
00:08:13,200 --> 00:08:16,990
through an alley, and in that alley, Darley 
and Batson had placed 

110
00:08:16,990 --> 00:08:23,230
a poorly dressed man who sat 
slumped in a doorway, head down, 

111
00:08:23,230 --> 00:08:27,760
eyes closed, not moving. 
And by the way, the photos I'm showing 

112
00:08:27,760 --> 00:08:31,610
you come courtesy Professor Batson, who 
was kind enough to share them with our 

113
00:08:31,610 --> 00:08:36,150
class. 
Anyway, as the student went by, the man 

114
00:08:36,150 --> 00:08:39,000
coughed twice and groaned without lifting 
his head. 

115
00:08:40,260 --> 00:08:43,430
If the student stopped and asked if 
something was wrong, or the student 

116
00:08:43,430 --> 00:08:49,395
offered to help, the man would simply act 
startled and say something like this: 

117
00:08:49,395 --> 00:08:57,420
"Oh, thank you [COUGH]… but no, it's alright. I've got this 
respiratory condition [COUGH] and the 

118
00:08:57,420 --> 00:09:03,120
doctor's given me these pills to take, and 
I just took one. If I just sit and rest 

119
00:09:03,120 --> 00:09:08,280
for a few minutes, I'll be okay. 
Thanks very much for stopping though." 

120
00:09:08,280 --> 00:09:12,430
If the student insisted on taking the man 
into the building, the man accepted 

121
00:09:12,430 --> 00:09:15,760
whatever help was offered and thanked the 
student for taking the time to be of 

122
00:09:15,760 --> 00:09:19,290
assistance, but then, after the student 
was gone, 

123
00:09:19,290 --> 00:09:24,500
the man rated how much help had been offered. 
What did Darley and Batson find? 

124
00:09:25,600 --> 00:09:30,590
Students in a hurry were much less likely 
to offer help than students not in a hurry, 

125
00:09:30,590 --> 00:09:33,550
but giving the speech on the Parable of the 

126
00:09:33,550 --> 00:09:38,500
Good Samaritan -- on the importance of 
helping others -- did not significantly 

127
00:09:38,500 --> 00:09:43,180
affect helping behavior. 
In many cases, a seminary student (that is, 

128
00:09:43,180 --> 00:09:47,650
a student training to become a priest 
or a minister) who was about to give a 

129
00:09:47,650 --> 00:09:51,620
talk on the Parable of the Good 
Samaritan -- on the importance of not 

130
00:09:51,620 --> 00:09:56,290
leaving people in need by the side of the 
road -- walked right past the man in the 

131
00:09:56,290 --> 00:10:01,330
alley to avoid being late. 
It's hard to imagine a more dramatic 

132
00:10:01,330 --> 00:10:06,450
demonstration that abstract attitudes, in 
this case about the importance of giving 

133
00:10:06,450 --> 00:10:11,560
help to others, can differ from behavior -- 
not because the seminary students are bad 

134
00:10:11,560 --> 00:10:14,380
people, not at all, but because they're trying not to 

135
00:10:14,380 --> 00:10:19,200
disappoint the experimenter. 
Well, these results and the results of 

136
00:10:19,200 --> 00:10:25,790
LaPiere raise a question: Is it common 
in everyday life for attitudes and 

137
00:10:25,790 --> 00:10:30,650
behavior to be inconsistent? 
A few years before Darley and Batson's study, 

138
00:10:30,650 --> 00:10:34,910
a psychologist named Allan Wicker 
published a major research review on 

139
00:10:34,910 --> 00:10:39,920
exactly this question, and his answer was yes, 

140
00:10:39,920 --> 00:10:43,980
which of course issued a tremendous 
challenge to attitude researchers. 

141
00:10:43,980 --> 00:10:50,600
Wicker reviewed 46 studies and concluded 
that: "it is considerably more 

142
00:10:50,600 --> 00:10:55,880
likely that attitudes will be unrelated 
or only slightly related to overt 

143
00:10:55,880 --> 00:11:01,960
behaviors than that attitudes will be 
closely related to actions." 

144
00:11:01,960 --> 00:11:05,310
Now, let's stop for a moment, and I want 
you to answer this question. 

145
00:11:05,310 --> 00:11:09,560
Why wouldn't there be a strong 
relationship between attitudes and 

146
00:11:09,560 --> 00:11:11,410
behavior? 
What's getting in the way? 

147
00:11:12,450 --> 00:11:16,640
We'll pause so that you can take just 60 
seconds to think about this and list a 

148
00:11:16,640 --> 00:11:21,080
few possible reasons. Then I'll tell you 
some reasons that Allan Wicker proposed. 

149
00:11:23,990 --> 00:11:27,620
Once you start thinking about it, there 
are all sorts of reasons why attitudes 

150
00:11:27,620 --> 00:11:29,900
and behavior might differ from one 
another. 

151
00:11:29,900 --> 00:11:32,980
Here are some of the reasons that Allan 
Wicker wrote about. 

152
00:11:34,030 --> 00:11:38,605
Maybe journal editors have been rejecting 
reports of attitude-behavior consistency 

153
00:11:38,605 --> 00:11:43,700
because they seem obvious or unexciting, 
a variant of what's known as the 

154
00:11:43,700 --> 00:11:47,930
"file drawer problem" (that is, researchers 
filing away their rejected manuscripts,

155
00:11:47,930 --> 00:11:51,630
with research 
findings that never see the light of day). 

156
00:11:52,850 --> 00:11:58,160
Maybe the problem is that many attitudes 
relate to any particular behavior, and 

157
00:11:58,160 --> 00:12:00,890
many behaviors relate to any particular 
attitude. 

158
00:12:00,890 --> 00:12:06,120
So, it may be that a behavior is 
inconsistent with one attitude in order 

159
00:12:06,120 --> 00:12:11,260
to be consistent with another. 
For instance, maybe you use a plastic cup 

160
00:12:11,260 --> 00:12:14,640
because you're in a hurry -- you don't 
want to be late to a class or to your job, 

161
00:12:14,640 --> 00:12:17,550
just as Darley and Batson's seminary 

162
00:12:17,550 --> 00:12:20,120
students didn't want to be late to give 
their talk. 

163
00:12:21,590 --> 00:12:26,680
Wicker also noted that attitude items 
tended to be more general than behaviors. 

164
00:12:26,680 --> 00:12:31,439
In LaPiere's case, for example, 
respondents were surveyed about Chinese 

165
00:12:31,439 --> 00:12:36,830
people in general, but the behavior 
involved a specific Chinese couple -- 

166
00:12:36,830 --> 00:12:41,990
two people out of a billion people. 
Finally, Wicker pointed out that 

167
00:12:41,990 --> 00:12:46,480
attitudes and behavior are often elicited 
under very different conditions. 

168
00:12:46,480 --> 00:12:51,680
For example, people's attitudes are often 
measured in anonymous surveys, whereas 

169
00:12:51,680 --> 00:12:58,610
behaviors tend to be publicly observed. 
So, for these and other reasons, Wicker 

170
00:12:58,610 --> 00:13:03,680
concluded that attitudes and behavior are 
often surprisingly unrelated, 

171
00:13:03,680 --> 00:13:07,770
and since the time of his review, other 
researchers have found much the same thing. 

172
00:13:07,770 --> 00:13:11,540
For instance, a 2003 study found that 
students who, 

173
00:13:11,540 --> 00:13:15,935
at the level of attitudes, 
regarded credit card debt as 

174
00:13:15,935 --> 00:13:22,140
unacceptable, were just as likely as 
other students to be in debt themselves. 

175
00:13:22,140 --> 00:13:25,330
Does this mean that attitudes are never 
related to behavior? 

176
00:13:25,330 --> 00:13:29,300
Not at all. 
There have by now been over ten 
meta-analyses 

177
00:13:29,300 --> 00:13:32,820
on the link between attitudes 
and behavior, 

178
00:13:32,820 --> 00:13:38,000
and the evidence suggests that at least 
in some cases, the attitude bone and the 

179
00:13:38,000 --> 00:13:42,470
behavior bone are loosely connected, 
and there are some cases in which they're 

180
00:13:42,470 --> 00:13:46,880
strongly connected. 
For example, attitudes and behavior are 

181
00:13:46,880 --> 00:13:51,360
most likely to be related when they 
closely match each other rather than say, 

182
00:13:51,360 --> 00:13:56,520
having a very general attitude about 
Chinese people or the environment, 

183
00:13:56,520 --> 00:14:02,060
and a very specific behavior concerning a 
particular Chinese couple or particular 

184
00:14:02,060 --> 00:14:06,890
plastic cups. 
When the attitude is strongly held or 

185
00:14:06,890 --> 00:14:11,300
potent -- for instance, attitudes acquired 
through direct experience. 

186
00:14:12,390 --> 00:14:16,480
When the attitude is easy to recall and 
has been stable over time. 

187
00:14:17,670 --> 00:14:21,980
When people are made aware of themselves 
and their attitudes -- for example, the 

188
00:14:21,980 --> 00:14:26,410
attitude-behavior link happens to be 
stronger, typically, when people can see 

189
00:14:26,410 --> 00:14:32,220
themselves behave in front of a mirror. 
And when outside influences are kept to a 

190
00:14:32,220 --> 00:14:35,600
minimum -- instead of people being in a 
rush, for example. 

191
00:14:36,720 --> 00:14:41,300
So, the point isn't that attitudes never 
relate to behavior, but rather, that 

192
00:14:41,300 --> 00:14:45,150
there's often less of a relationship than 
we might expect. 

193
00:14:45,150 --> 00:14:50,280
In the next video, we'll build on this 
discussion of attitudes and behavior with 

194
00:14:50,280 --> 00:14:55,660
a review of cognitive dissonance theory, 
arguably the most well-known theory in 

195
00:14:55,660 --> 00:15:02,072
all of social psychology, and one that's 
led to some very surprising results. 

