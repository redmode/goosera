





1
00:00:06,830 --> 00:00:11,870
So far, we've focused on several ways that 
our social perceptions can be influenced, 

2
00:00:11,870 --> 00:00:17,430
or even distorted, by context effects, 
change blindness, confirmation biases, 

3
00:00:17,430 --> 00:00:19,770
and a host of other psychological 
factors. 

4
00:00:19,770 --> 00:00:23,520
So you might be thinking, do we ever get 
it right? 

5
00:00:23,520 --> 00:00:26,970
The answer, as we'll explore in this 
video, is yes. 

6
00:00:26,970 --> 00:00:31,075
Not only are we capable of getting it 
right, but we often make accurate 

7
00:00:31,075 --> 00:00:34,800
judgments with surprising speed and 
sensitivity. 

8
00:00:34,800 --> 00:00:40,020
In other words, a balanced view of social 
perception is that, on one hand, it can be 

9
00:00:40,020 --> 00:00:45,450
distorted by all sorts of factors, 
just as any other perception can be, but 

10
00:00:45,450 --> 00:00:49,720
on the other hand, it can also operate 
with surprising efficiency. 

11
00:00:49,720 --> 00:00:55,770
Let me give you an example. 
In 2011, a rather extraordinary study was 

12
00:00:55,770 --> 00:00:59,250
published by Nick Rule and Nalini 
Ambady 

13
00:00:59,250 --> 00:01:03,690
asking people to rate college yearbook 
photos on a variety of dimensions, 

14
00:01:03,690 --> 00:01:09,220
some of which had to do with how powerful 
the person in the photo seemed to be. 

15
00:01:09,220 --> 00:01:13,260
The photos were all cropped so that only 
the person's face was visible. 

16
00:01:13,260 --> 00:01:17,160
All the photos were of the same size.  
They were converted to grayscale rather 

17
00:01:17,160 --> 00:01:19,425
than color. So in other words, they were 
very much 

18
00:01:19,425 --> 00:01:23,280
standardized. 
But these weren't any old photos â they 

19
00:01:23,280 --> 00:01:28,330
were college yearbook photos of people 
who would later become the managing 

20
00:01:28,330 --> 00:01:33,750
partners, the head lawyer, for nearly 
three-quarters of the top 100 American 

21
00:01:33,750 --> 00:01:37,120
law firms according to the Law.com 
website. 

22
00:01:37,120 --> 00:01:42,380
And what the researchers found was that 
social judgments about how powerful the 

23
00:01:42,380 --> 00:01:46,965
person looked in college -- before the 
person even entered law school -- were 

24
00:01:46,965 --> 00:01:53,030
statistically related to how much profit 
the person's law firm made when the 

25
00:01:53,030 --> 00:01:59,027
person was now the managing partner, 
20 to 50 years after college. 

26
00:01:59,027 --> 00:02:04,520
That's an amazing degree of precision 
based on social judgments made from one 

27
00:02:04,520 --> 00:02:08,860
black and white photo taken decades 
earlier. Incredible! 

28
00:02:08,860 --> 00:02:14,250
Let me give you another example. 
John Gottman, a relationship researcher 

29
00:02:14,250 --> 00:02:19,790
at the University of Washington, invited 
124 newlywed couples to visit his 

30
00:02:19,790 --> 00:02:24,850
laboratory and be videotaped while they 
discussed an ongoing disagreement in 

31
00:02:24,850 --> 00:02:28,020
their marriage. 
These videos were then rated by 

32
00:02:28,020 --> 00:02:32,700
independent observers as to how much 
positive or negative emotion had been 

33
00:02:32,700 --> 00:02:37,520
displayed in the first three minutes 
of the couple's discussion. 

34
00:02:37,520 --> 00:02:41,770
The results? 
Social judgments of this brief marital 

35
00:02:41,770 --> 00:02:47,760
interaction significantly predicted which 
couples were divorced six years later. 

36
00:02:48,860 --> 00:02:52,720
It's surprising enough that anything 
could predict divorce six years into the 

37
00:02:52,720 --> 00:02:57,890
future, but it borders on shocking that 
the prediction could be made by strangers 

38
00:02:57,890 --> 00:03:01,430
watching the couple interact for only 
three minutes â  

39
00:03:01,430 --> 00:03:07,540
what's known in psychology as a thin 
slice of behavior, a brief observation, a 

40
00:03:07,540 --> 00:03:13,740
small sample of behavior. 
The term "thin slice" was coined in a 1992 

41
00:03:13,740 --> 00:03:19,310
Psychological Bulletin article by Nalini 
Ambady and Bob Rosenthal, the same person 

42
00:03:19,310 --> 00:03:24,760
who documented the Pygmalion effect, and 
it was popularized in Malcolm Gladwell's 

43
00:03:24,760 --> 00:03:29,760
runaway bestseller Blink. 
Ambady and Rosenthal conducted what's 

44
00:03:29,760 --> 00:03:34,490
known as a "meta-analysis" -- 
a statistical technique that combines and 

45
00:03:34,490 --> 00:03:40,090
analyzes results from different studies 
(literally, an analysis of analyses, 

46
00:03:40,090 --> 00:03:44,805
a meta-analysis). 
And what they found, based on a review of 

47
00:03:44,805 --> 00:03:49,457
38 different results reported by a wide 
variety of researchers, is that 

48
00:03:49,457 --> 00:03:54,260
judgments about people's personality, 
or whether they're telling the truth, or 

49
00:03:54,260 --> 00:03:58,640
whether they're clinically depressed, 
were just as accurate, if not more so, 

50
00:03:58,640 --> 00:04:03,950
when the judgments were based on thin 
slices of behavior less than five minutes 

51
00:04:03,950 --> 00:04:07,949
in length. 
In fact, a year after the 1992 article, 

52
00:04:07,949 --> 00:04:12,440
Ambady and Rosenthal published a study 
showing that ratings by people who 

53
00:04:12,440 --> 00:04:18,540
watched three ten-second video clips of a 
university teacher teaching a class, with 

54
00:04:18,540 --> 00:04:24,350
no students in the video and no sound, 
accurately predicted student ratings of 

55
00:04:24,350 --> 00:04:30,100
the teacher at the end of the semester. 
What I just showed you was exactly ten 

56
00:04:30,100 --> 00:04:35,470
seconds of a professor teaching. 
All it took was three video clips just 

57
00:04:35,470 --> 00:04:39,972
like that, all of it nonverbal -- 
things like whether the teacher seemed 

58
00:04:39,972 --> 00:04:46,020
confident, enthusiastic, warm, active, and 
so on. 

59
00:04:46,020 --> 00:04:50,340
Professor Ambady and her colleagues have 
also found that long term physical 

60
00:04:50,340 --> 00:04:55,720
therapy outcomes could be predicted after 
people watched only one minute of silent 

61
00:04:55,720 --> 00:05:00,660
video footage focused on the therapist's 
nonverbal behavior. 

62
00:05:00,660 --> 00:05:05,180
The sales effectiveness of a sales manager 
could be identified after listening to 

63
00:05:05,180 --> 00:05:10,890
the person's voice for only 20 seconds, 
with the words masked to keep the tone of 

64
00:05:10,890 --> 00:05:13,905
voice but hide the content of what was 
said. 

65
00:05:13,905 --> 00:05:20,110
And in a similar study, the more dominant 
a surgeon's voice was rated, the more 

66
00:05:20,110 --> 00:05:25,000
likely the surgeon was to have been sued 
for malpractice -- voice ratings that were 

67
00:05:25,000 --> 00:05:29,790
made after listening to only 40 seconds 
of the doctor speaking. 

68
00:05:31,150 --> 00:05:34,830
Now, if these results seem incredible, 
just hang onto your hat for two more 

69
00:05:34,830 --> 00:05:37,310
examples of thin-slice research. 

70
00:05:40,530 --> 00:05:42,310
The first line of research, kicked off by 

71
00:05:42,310 --> 00:05:47,200
a Princeton social psychologist named 
Alex Todorov, examined whether election 

72
00:05:47,200 --> 00:05:51,620
outcomes could be predicted solely on the 
basis of judgments about candidate 

73
00:05:51,620 --> 00:05:55,780
photos. 
In a 2005 article published in the 

74
00:05:55,780 --> 00:05:59,870
journal Science, he and his colleagues 
reported that when they asked people to 

75
00:05:59,870 --> 00:06:04,330
look at pairs of black-and-white 
portraits showing candidates for the U.S. 

76
00:06:04,330 --> 00:06:08,450
Senate, the candidate who won and the 
candidate who lost, 

77
00:06:08,450 --> 00:06:13,410
the individual who was judged as more 
competent ended up winning the election 

78
00:06:13,410 --> 00:06:18,529
over two thirds of the time. 
The photos you see here were one of the 

79
00:06:18,529 --> 00:06:23,380
pairs used in the study, which Professor 
Todorov was generous enough to share with 

80
00:06:23,380 --> 00:06:26,890
our class. 
And you can see which candidate you 

81
00:06:26,890 --> 00:06:30,210
judged as most competent in the Snapshot 
Quiz. 

82
00:06:32,790 --> 00:06:36,950
If it was Russ Feingold, the candidate on 
the left, you would have accurately 

83
00:06:36,950 --> 00:06:41,350
predicted the election results. 
How long did it take people to make 

84
00:06:41,350 --> 00:06:45,020
these judgments? 
They were able to make them after seeing 

85
00:06:45,020 --> 00:06:51,200
the paired photos for only one second, 
and taking an additional second to make 

86
00:06:51,200 --> 00:06:56,750
the judgment. That is, it took them a 
grand total of about two seconds to make 

87
00:06:56,750 --> 00:07:02,260
a judgment that predicted the winner! 
These were political candidates, I should 

88
00:07:02,260 --> 00:07:05,810
underscore, 
whom people had never even seen before, 

89
00:07:05,810 --> 00:07:08,910
and yet they rendered these judgments with 
lightning speed. 

90
00:07:08,910 --> 00:07:16,090
In fact, a 2007 follow-up study found 
that people's judgments were just as 

91
00:07:16,090 --> 00:07:20,520
predictive of an election outcome when 
they viewed the photos for a quarter of a 

92
00:07:20,520 --> 00:07:23,720
second 
than if they were given an unlimited 

93
00:07:23,720 --> 00:07:33,180
amount of time to look them over: 
68.5% in the first case and 62.5% in the second. 

94
00:07:33,180 --> 00:07:36,640
Here's what a quarter of a second looks 
like -- you ready? 

95
00:07:38,440 --> 00:07:40,460
I'll show you that one more time, just in 

96
00:07:40,460 --> 00:07:43,350
case you missed it. 
Here we go... 

97
00:07:44,260 --> 00:07:45,950
Remarkable, huh? 

98
00:07:46,150 --> 00:07:52,460
And a study published in 2012 found that 
even if you show people headshots for two 

99
00:07:52,460 --> 00:07:57,590
seconds without most of the face, 
their competence judgments still predict 

100
00:07:57,590 --> 00:08:03,910
the winner 65% of the time. 
So, clearly, people can size up strangers 

101
00:08:03,910 --> 00:08:09,270
with remarkable speed, but the results 
we've discussed leave two key questions 

102
00:08:09,270 --> 00:08:13,710
unanswered. 
First, can people make thin-slice 

103
00:08:13,710 --> 00:08:18,340
judgments that are accurate when it comes 
to personal characteristics and other 

104
00:08:18,340 --> 00:08:22,750
aspects of a person's identity? 
Remember, we don't know whether the 

105
00:08:22,750 --> 00:08:26,830
candidates who were judged competent 
actually are competent; 

106
00:08:26,830 --> 00:08:32,160
we just know that perceptions of 
competence predict election outcomes. 

107
00:08:32,160 --> 00:08:36,740
And second, if people can make accurate 
judgments, how fast can they do it? 

108
00:08:38,280 --> 00:08:42,660
Well, I warned you to hang onto your hat 
for two last examples of thin-slice 

109
00:08:42,660 --> 00:08:44,620
research. 
Here's the second one. 

110
00:08:47,290 --> 00:08:52,500
In 2008, the same research team 
that did the college yearbook study 

111
00:08:52,500 --> 00:08:57,230
published a report that examined whether 
people could accurately guess the sexual 

112
00:08:57,230 --> 00:09:02,170
orientation of 90 men whose faces were 
shown in black-and-white photos. 

113
00:09:03,179 --> 00:09:07,750
Half the photos showed heterosexual men,
half showed gay men, 

114
00:09:07,750 --> 00:09:11,870
and the faces were cropped from ear to 
ear, and from the chin to the top of the 

115
00:09:11,870 --> 00:09:15,399
head, with no background except for white. 

116
00:09:15,399 --> 00:09:19,630
This slide shows a transformation of my 
own photo that the researchers kindly 

117
00:09:19,630 --> 00:09:22,510
supplied 
so that our class can get to see the sort 

118
00:09:22,510 --> 00:09:26,690
of image that they used. 
In the real study, they didn't use any 

119
00:09:26,690 --> 00:09:30,610
photos of men with jewelry, with glasses, 
or facial hair, 

120
00:09:30,610 --> 00:09:33,508
just to keep the focus on facial features 
alone. 

121
00:09:33,508 --> 00:09:39,770
So, were people able to identify the 
sexual orientation of male strangers from 

122
00:09:39,770 --> 00:09:44,550
a photo? 
And if so, how rapidly were they able to do it?

123
00:09:45,180 --> 00:09:47,490
The answer is that people were able to 

124
00:09:47,490 --> 00:09:52,980
detect a stranger's sexual orientation 
above chance levels, even when the photos 

125
00:09:52,980 --> 00:09:56,240
were shown for only a twentieth of a 
second. 

126
00:09:56,240 --> 00:10:00,250
To give you a sense of how blisteringly 
fast that is, I've set up a simulated 

127
00:10:00,250 --> 00:10:02,845
trial that you can experience in this 
video. 

128
00:10:02,845 --> 00:10:08,290
So, if you were a participant, you would 
just look at the cross in the center 

129
00:10:08,290 --> 00:10:12,900
white area when it appears, 
watch the 50-millisecond flash of the 

130
00:10:12,900 --> 00:10:18,130
photo, followed by what's known as a 
"mask" for a tenth of a second afterward, 

131
00:10:18,130 --> 00:10:21,450
and then you'd press a key on your 
keyboard to give your answer: 

132
00:10:21,450 --> 00:10:27,270
the Z key if you think the person's gay, 
or the forward slash key if you think the 

133
00:10:27,270 --> 00:10:29,740
person's straight 
(that is, heterosexual). 

134
00:10:29,740 --> 00:10:34,400
So, you ready for the simulated trial? 
Here goes... 

135
00:10:40,420 --> 00:10:44,470
Isn't that something? 
That was 50 milliseconds -- a twentieth of 

136
00:10:44,470 --> 00:10:47,510
a second -- 
so fast that we barely even recognize 

137
00:10:47,510 --> 00:10:50,700
we've seen a face, and yet, even if we're not consciously 

138
00:10:50,700 --> 00:10:55,732
aware of what we've seen, our brain is 
able to process the information and 

139
00:10:55,732 --> 00:11:00,860
render judgments that are correct more 
often than you'd expect by chance alone. 

140
00:11:00,860 --> 00:11:05,500
So now, if we go back to an item in the 
Snapshot Quiz, the answer should be clear. 

141
00:11:06,100 --> 00:11:08,373
The item said: "Social judgments made 

142
00:11:08,373 --> 00:11:14,200
during the first minute of meeting a 
stranger are not usually reliable and accurate." 

143
00:11:14,200 --> 00:11:17,610
Let's pause for a moment so you can see 
how you answered. 

144
00:11:21,110 --> 00:11:22,760
What's the correct answer according to 

145
00:11:22,760 --> 00:11:26,800
the research record? Drum roll, pleaseâ¦ 

146
00:11:26,800 --> 00:11:32,550
False. How does it happen? Well, researchers are 
not entirely clear. 

147
00:11:32,550 --> 00:11:36,530
There have been several theories 
advanced. For example, one idea 

148
00:11:36,530 --> 00:11:41,410
is that there may be evolutionary value 
in being able to rapidly judge if other 

149
00:11:41,410 --> 00:11:46,505
people are threats, potential partners, or even effective leaders. 

150
00:11:46,505 --> 00:11:52,060
There's also some evidence that our 
brains are wired to process emotions more 

151
00:11:52,060 --> 00:11:56,700
rapidly than cognitions. 
For example, we might know how we feel 

152
00:11:56,700 --> 00:12:01,940
before we know why we feel that way. 
And of course, thin-slice judgments 

153
00:12:01,940 --> 00:12:06,705
happen so fast that they might simply 
avoid distractions that could lower their 

154
00:12:06,705 --> 00:12:11,200
accuracy. 
Regardless, we can now put the pieces 

155
00:12:11,200 --> 00:12:16,330
together from the past several videos to 
see both the strengths and weaknesses of 

156
00:12:16,330 --> 00:12:20,440
social perception. 
Social judgments can take place very 

157
00:12:20,440 --> 00:12:24,880
rapidly, sometimes with more accuracy 
than we might think. 

158
00:12:24,880 --> 00:12:29,720
At the same time, they're also prone to 
certain biases and distortions, and once 

159
00:12:29,720 --> 00:12:34,790
these judgements are formed, whether 
accurately or not, we tend to lock in 

160
00:12:34,790 --> 00:12:39,170
and search for confirming evidence rather 
than challenging the judgments we've 

161
00:12:39,170 --> 00:12:42,850
already made. 
So first impressions matter. 

162
00:12:44,420 --> 00:12:49,590
Let's end with a brief pop-up question on 
the topic of first impressions, a topic 

163
00:12:49,590 --> 00:12:51,950
that we'll explore further in the next 
video. 


