




1
00:00:06,639 --> 00:00:08,186
Welcome back. 

2
00:00:08,186 --> 00:00:12,092
I'm assuming that you've now seen the 
Quiet Rage documentary. 

3
00:00:12,092 --> 00:00:17,644
If not, please watch that video first and 
then come back and watch this one. 

4
00:00:17,644 --> 00:00:20,273
Okay? 
I won't go anywhere -- I promise I'll stay 

5
00:00:20,273 --> 00:00:22,899
right here in this video. 
Okay? 

6
00:00:22,899 --> 00:00:26,623
So if you haven't watched, shoo! Go 
watch! 

7
00:00:26,623 --> 00:00:30,779
We good? 
Okay. 

8
00:00:30,779 --> 00:00:35,069
I think one of the most amazing aspects 
of the Stanford Prison Experiment is how 

9
00:00:35,069 --> 00:00:41,700
closely it parallels real life cases of 
prison abuse and human rights violations. 

10
00:00:41,700 --> 00:00:47,390
For example, the kind of prison abuse we 
saw in Abu Ghraib in Iraq. 

11
00:00:47,390 --> 00:00:52,850
In both cases the guards stripped the 
prisoners, they used sexual tactics to 

12
00:00:52,850 --> 00:00:55,850
humiliate prisoners, put bags over their 
heads, 

13
00:00:55,850 --> 00:01:00,620
and they performed the worst abuses at 
night when the guards thought they 

14
00:01:00,620 --> 00:01:05,994
weren't being observed. 
In fact, the parallels were so uncanny 

15
00:01:05,994 --> 00:01:10,900
that when the Abu Ghraib scandal first 
made the news in 2004, the Stanford 

16
00:01:10,900 --> 00:01:16,240
Prison Experiment website suddenly 
started getting more than a quarter million 

17
00:01:16,240 --> 00:01:21,790
page views per day, and DVD copies 
of Quiet Rage were shown 

18
00:01:21,790 --> 00:01:24,850
to U.S. military leaders in Iraq, as well as at 

19
00:01:24,850 --> 00:01:29,510
least one member of the U.S. 
House Armed Services Committee. 

20
00:01:29,510 --> 00:01:35,790
The U.S military even began requiring all 
guards at Abu Ghraib to watch Quiet Rage 

21
00:01:35,790 --> 00:01:38,650
so that similar abuses wouldn't occur in 
the future. 

22
00:01:39,880 --> 00:01:43,500
Of course, as with Stanley Milgram's 
research on obedience, 

23
00:01:43,500 --> 00:01:48,180
the Stanford Prison Experiment raises 
significant ethical questions, 

24
00:01:48,180 --> 00:01:51,710
and I very much hope that you'll share 
your thoughts about that in the class 

25
00:01:51,710 --> 00:01:55,745
discussion forums. 
For example, what do you think about the 

26
00:01:55,745 --> 00:02:00,430
trade-off between the information gained 
from the research and the stress 

27
00:02:00,430 --> 00:02:04,700
experienced by the participants? 
How do we weigh those two different things? 

28
00:02:05,360 --> 00:02:08,100
Just to put a human face on the question, 

29
00:02:08,100 --> 00:02:13,130
consider the case of Richard Yacco, 
Prisoner 1037, 

30
00:02:13,130 --> 00:02:18,210
whose parents saw him during visitor's 
night on Day 4 of the experiment, and who, 

31
00:02:18,210 --> 00:02:23,350
the next day, was released early from 
the study because Professor Zimbardo 

32
00:02:23,350 --> 00:02:28,220
noticed symptoms of depression. 
Here's a letter that Richard Yacco's mother 

33
00:02:28,220 --> 00:02:33,130
wrote to Professor Zimbardo after seeing 
her son during visitor hours. 

34
00:02:34,380 --> 00:02:38,840
"My husband and I visited our son at 
'Stanford County Prison.' 

35
00:02:38,840 --> 00:02:43,180
It seemed very real to me. 
I had not expected anything quite so 

36
00:02:43,180 --> 00:02:47,740
severe nor had my son when he volunteered, 
I am sure. 

37
00:02:47,740 --> 00:02:52,830
It gave me a depressed feeling and I was 
somewhat upset when I saw him. 

38
00:02:52,830 --> 00:02:57,480
He looked very haggard and his chief 
complaint seemed to be that he had not 

39
00:02:57,480 --> 00:03:02,780
seen the sun for so long. 
I asked if he was sorry he volunteered and 

40
00:03:02,780 --> 00:03:07,400
he answered that at first he had been. 
However, he had gone through several 

41
00:03:07,400 --> 00:03:12,790
different moods and he was more resigned. 
This will be the hardest earned money he 

42
00:03:12,790 --> 00:03:18,700
will ever earn in his life, I am sure. 
Mother of '1037.'" 

43
00:03:18,700 --> 00:03:23,460
So you can see that the power of the role 
even spilled over to the parents, 

44
00:03:23,460 --> 00:03:28,300
one of whom referred to herself as 
"Mother of 1037." 

45
00:03:29,200 --> 00:03:33,800
Was this level of stress unethical? 
Reasonable people can reach 

46
00:03:33,800 --> 00:03:38,300
different conclusions. 
My own view is that a high level of stress 

47
00:03:38,300 --> 00:03:43,420
is not unethical in and of itself. 
After all, there are many medical and 

48
00:03:43,420 --> 00:03:48,400
psychological studies that involve a high 
level of stress, and Professor Zimbardo 

49
00:03:48,400 --> 00:03:53,200
did remove Richard Yacco as soon as he 
detected signs of depression. 

50
00:03:53,300 --> 00:03:55,990
In fact, the father thought that the son 

51
00:03:55,990 --> 00:03:58,410
should continue participating in the 
study, 

52
00:03:58,410 --> 00:04:02,100
and Richard Yacco later told a reporter 
that he didn't think he was going through 

53
00:04:02,100 --> 00:04:07,300
any kind of depression. 
The more serious ethical challenge, I think, 

54
00:04:07,300 --> 00:04:11,520
and one that's shared with Stanley 
Milgram's research on obedience, is that 

55
00:04:11,520 --> 00:04:17,110
the study didn't follow two bedrock 
principles that now govern research with 

56
00:04:17,110 --> 00:04:22,130
human participants. 
The first of these is informed consent. 

57
00:04:22,130 --> 00:04:26,900
Researchers need to let people know of 
any reasonably foreseeable factors that 

58
00:04:26,900 --> 00:04:30,670
might influence their decision whether to 
participate. 

59
00:04:30,670 --> 00:04:35,270
You might reasonably expect, for example, 
that imprisoning people could lead them 

60
00:04:35,270 --> 00:04:39,730
to feel stress, or helplessness, or 
isolation, 

61
00:04:39,730 --> 00:04:44,470
which means that for the consent to be 
truly informed, participants would need 

62
00:04:44,470 --> 00:04:49,490
to be warned in advance that they might 
experience strong negative emotions 

63
00:04:49,490 --> 00:04:55,040
if they choose to participate. 
Second, participants need to be informed 

64
00:04:55,040 --> 00:05:00,900
that they have a right to withdraw from 
the research once participation has begun. 

65
00:05:00,900 --> 00:05:02,900
The wording of these two principles is from the 

66
00:05:02,900 --> 00:05:06,900
American Psychological 
Association, but the basic ideas are 

67
00:05:06,900 --> 00:05:11,990
widely shared around the world. 
In the case of the Stanford Prison Experiment, 

68
00:05:11,990 --> 00:05:16,370
these standards had not yet 
been adopted by the research community, 

69
00:05:16,370 --> 00:05:20,200
so the consent form that participants 
signed didn't obtain 

70
00:05:20,200 --> 00:05:24,760
fully informed consent, and it contained 
a provision that 

71
00:05:24,760 --> 00:05:29,490
Stanford would never approve today. 
Specifically, the form said 

72
00:05:29,490 --> 00:05:32,990
the following: 
"I will only be released from participation 

73
00:05:32,990 --> 00:05:37,240
for reasons of health 
deemed adequate by the medical advisers 

74
00:05:37,240 --> 00:05:41,230
to the research project 
or for other reasons deemed appropriate 

75
00:05:41,230 --> 00:05:45,500
by Dr. Philip Zimbardo, Principal Investigator 
of the project." 

76
00:05:45,500 --> 00:05:49,700
In other words, the consent form asked 
participants to sign away 

77
00:05:49,700 --> 00:05:55,500
their right to withdraw -- a definite no-no 
by modern standards. 

78
00:05:55,500 --> 00:05:57,260
If you'd like to read more about the 

79
00:05:57,260 --> 00:06:02,030
Stanford Prison Experiment, the ethics 
involved, and Professor Zimbardo's 

80
00:06:02,030 --> 00:06:05,865
perspective looking back on the study, 
the most comprehensive source of 

81
00:06:05,865 --> 00:06:11,200
information is "The Lucifer Effect," 
Professor Zimbardo's bestseller, 

82
00:06:11,200 --> 00:06:15,509
which devotes over 200 pages to the Stanford 
Prison Experiment. 

83
00:06:16,780 --> 00:06:21,630
I'm also very pleased to say that Random 
House has made Chapter 11 of the book 

84
00:06:21,630 --> 00:06:26,220
available for free to anyone in our 
class, so thank you Random House! 

85
00:06:26,220 --> 00:06:32,020
This particular chapter, entitled "The 
Stanford Prison Experiment: Ethics and 

86
00:06:32,020 --> 00:06:35,760
Extensions," is one of the most 
fascinating chapters of the book. 

87
00:06:36,800 --> 00:06:41,405
Another good source of information is the 
Stanford Prison Experiment website, 

88
00:06:41,405 --> 00:06:46,380
PrisonExp.org, which is a Social Psychology Network 

89
00:06:46,380 --> 00:06:51,460
partner site that has hundreds of pages 
and links related to the research, 

90
00:06:51,460 --> 00:06:55,310
including archival materials from the 
experiment. 

91
00:06:55,310 --> 00:07:01,500
Professor Zimbardo and Social Psychology 
Network codeveloped the site in 1999, 

92
00:07:01,500 --> 00:07:05,000
which means it's about a century old in 
web years. 

93
00:07:05,000 --> 00:07:12,700
Since the '90s, it's received 130 million 
page views, and you're warmly invited to visit. 

94
00:07:12,700 --> 00:07:15,200
And speaking of visiting, for those of 
you who can make it 

95
00:07:15,200 --> 00:07:19,410
to Akron, Ohio, one 
other place you might consider visiting 

96
00:07:19,410 --> 00:07:22,550
is the Center for the History of 
Psychology, 

97
00:07:22,550 --> 00:07:27,990
which has an entire exhibit on the 
Stanford Prison Experiment, including one 

98
00:07:27,990 --> 00:07:33,300
of the original cell doors. 
These are just a few photos I took in 2012. 

99
00:07:34,800 --> 00:07:37,190
So in the final analysis, what's the 

100
00:07:37,190 --> 00:07:40,960
main lesson of the Stanford Prison 
Experiment? 

101
00:07:40,960 --> 00:07:44,360
Here's what Professor Zimbardo wrote in 
the Lucifer Effect: 

102
00:07:45,430 --> 00:07:52,530
"Good people can be induced, seduced, and 
initiated into behaving in evil ways... 

103
00:07:52,530 --> 00:07:56,790
The primary simple lesson the Stanford 
Prison Experiment teaches is that 

104
00:07:56,790 --> 00:08:01,580
situations matter. Social situations can 
have more profound effects 

105
00:08:01,580 --> 00:08:06,480
on the behavior and mental 
functioning of individuals, groups, 

106
00:08:06,480 --> 00:08:09,450
and national leaders than we might believe 
possible." 

107
00:08:10,550 --> 00:08:16,000
This lesson, that situations matter, is 
very consistent with results from Stanley 

108
00:08:16,000 --> 00:08:21,400
Milgram's research on obedience and Solomon 
Asch's research on conformity, 

109
00:08:21,400 --> 00:08:27,020
but it's important not to misinterpret 
that lesson as saying that the situation 

110
00:08:27,020 --> 00:08:32,600
is the only thing that matters, 
or individual characteristics never matter, 

111
00:08:32,600 --> 00:08:36,900
or situations matter more than 
individual characteristics. 

112
00:08:38,490 --> 00:08:43,320
The Stanford Prison Experiment was never 
intended to show that prisoner and guard 

113
00:08:43,320 --> 00:08:47,540
roles always, or even usually, lead to 
abuse. 

114
00:08:47,540 --> 00:08:52,035
Rather, it demonstrated that when people 
in positions of authority become 

115
00:08:52,035 --> 00:08:57,020
deindividuated -- when they lose sight of 
their personal identity and become 

116
00:08:57,020 --> 00:09:02,200
absorbed in a role -- 
the situation can spin out of control. 

117
00:09:02,200 --> 00:09:06,630
Here's one way to think about it. 
The main point of Milgram's research 

118
00:09:06,630 --> 00:09:12,100
wasn't that 65% of people in the baseline 
condition deliver the highest electric shock; 

119
00:09:12,100 --> 00:09:14,800
it was that under certain circumstances, 

120
00:09:14,800 --> 00:09:19,300
people will obey a stranger's command to 
harm another person. 

121
00:09:19,760 --> 00:09:24,830
The main point of Asch's research wasn't 
that people will conform on 32% of all 

122
00:09:24,830 --> 00:09:29,300
critical trials; it's that under certain 
circumstances, people will go along 

123
00:09:29,300 --> 00:09:34,500
with the group, even if it means contradicting 
the evidence of their senses. 

124
00:09:34,700 --> 00:09:36,980
And in the Stanford Prison Experiment, 

125
00:09:36,980 --> 00:09:39,800
the main point wasn't that guards will become 
abusive 

126
00:09:39,800 --> 00:09:45,870
65% of the time, or 32% of the time, or 10% 
of the time. 

127
00:09:45,870 --> 00:09:50,950
In fact, the main lesson isn't even about 
prisoners or guards. 

128
00:09:50,950 --> 00:09:55,980
It's that we need to guard ourselves 
against situational factors that can lead 

129
00:09:55,980 --> 00:10:02,370
us to behave in destructive ways. 
You know, just as climate change is often 

130
00:10:02,370 --> 00:10:08,060
referred to an inconvenient truth by 
climate scientists and environmentalists, 

131
00:10:08,060 --> 00:10:11,520
social psychology has its own 
inconvenient truth, 

132
00:10:11,520 --> 00:10:16,600
and that truth is that situational 
factors sometimes override 

133
00:10:16,600 --> 00:10:21,060
the best of intentions and lead good 
people to do bad things. 

134
00:10:22,540 --> 00:10:28,210
If we ignore that inconvenient truth in 
favor of a simplistic view of the world 

135
00:10:28,210 --> 00:10:34,400
as basically made up of good guys and bad 
guys, we risk getting blindsided by 

136
00:10:34,400 --> 00:10:40,600
powerful situational factors that, in some 
cases, can get us into deep trouble: 

137
00:10:40,600 --> 00:10:44,280
group pressures, authority figures, 
assigned roles, and so on. 

138
00:10:45,420 --> 00:10:49,690
That's the enduring lesson of the 
experiment. 

139
00:10:49,690 --> 00:10:50,620
I'll see you next time. 

