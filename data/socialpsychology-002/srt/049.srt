
1
00:00:06,500 --> 00:00:09,350
Welcome back to Social Psychology!

2
00:00:09,350 --> 00:00:13,216
In our last video, I introduced our wonderful teaching staff, 

3
00:00:13,216 --> 00:00:18,083
discussed a few unique aspects of the course, defined social psychology, 

4
00:00:18,083 --> 00:00:21,617
and ended with a brief demonstration of hindsight bias, 

5
00:00:21,617 --> 00:00:24,617
or the "I-knew-it-all-along effect."

6
00:00:24,617 --> 00:00:28,883
In this video, I want to talk a little bit about how the course is structured 

7
00:00:28,883 --> 00:00:33,083
and offer some rules of the road, just to make sure that our journey together 

8
00:00:33,083 --> 00:00:37,350
is a positive one, like "Don't park on the yellow brick road!" 

9
00:00:37,350 --> 00:00:40,150
And in just a few minutes, I'll share some exciting news

10
00:00:40,150 --> 00:00:42,950
about a feature of our course that I think you'll like.

11
00:00:42,950 --> 00:00:46,683
But first, let's talk about how the course is structured.

12
00:00:46,883 --> 00:00:50,817
Each week, we'll have a few lectures and related videos, along with some 

13
00:00:50,817 --> 00:00:55,617
assigned reading from one of David Myers' textbooks or from other sources. 

14
00:00:55,617 --> 00:00:59,350
Sometimes the readings will focus on the same topic as the lectures, 

15
00:00:59,350 --> 00:01:02,083
but more often, they'll cover material beyond the lectures 

16
00:01:02,083 --> 00:01:04,683
so that we get the broadest possible coverage 

17
00:01:04,683 --> 00:01:07,750
in the short amount of time that we have together.

18
00:01:07,750 --> 00:01:11,000
When the lecture and readings do focus on the same topic, 

19
00:01:11,000 --> 00:01:14,750
the readings will typically be assigned after the lecture.

20
00:01:14,750 --> 00:01:17,750
So, it's important to follow the order of lectures and readings

21
00:01:17,750 --> 00:01:21,216
given in the course syllabus, and of course, to keep up.

22
00:01:21,216 --> 00:01:25,216
You'll get much, much more out of the course than if you skip around 

23
00:01:25,216 --> 00:01:29,417
or if you do a reading before the lecture it's supposed to follow.

24
00:01:29,417 --> 00:01:31,750
The course will have five different assignments

25
00:01:31,750 --> 00:01:36,283
worth a combined total of 50 points. The first, called 

26
00:01:36,283 --> 00:01:39,750
the "Random Assignment Assignment," will step you through 

27
00:01:39,750 --> 00:01:43,817
a brief tutorial and random sampling and random assignment, 

28
00:01:43,817 --> 00:01:48,683
and then ask you to use the randomizer form from Research Randomizer, 

29
00:01:48,683 --> 00:01:52,617
a Social Psychology Network partner site, to generate your own 

30
00:01:52,617 --> 00:01:55,817
random sample and to randomly assign individuals 

31
00:01:55,817 --> 00:01:59,883
in the sample to one of several experimental conditions.

32
00:02:00,417 --> 00:02:03,150
In the second assignment, those of you who join 

33
00:02:03,150 --> 00:02:06,417
Social Psychology Network will create your own page 

34
00:02:06,417 --> 00:02:10,083
in the Network and receive feedback from other class members 

35
00:02:10,083 --> 00:02:12,550
on the social impression you make.

36
00:02:12,550 --> 00:02:16,000
What do other people think of you when they first see your page?

37
00:02:16,000 --> 00:02:20,083
And how can you use psychology to improve the impression you make, 

38
00:02:20,083 --> 00:02:25,683
whether it's on employers, friends, romantic interests, and so forth?

39
00:02:25,683 --> 00:02:28,083
For those of you who don't join the Network, 

40
00:02:28,083 --> 00:02:31,417
I'll offer a variation of the assignment in which you create a 

41
00:02:31,417 --> 00:02:35,483
résumé or curriculum vitae and get social feedback 

42
00:02:35,483 --> 00:02:38,216
from other class members on that.

43
00:02:38,216 --> 00:02:42,150
For the third assignment, all you need to do is post five times in the 

44
00:02:42,150 --> 00:02:46,550
class discussion forums -- an assignment some of you have already begun! 

45
00:02:46,550 --> 00:02:51,000
These posts can be questions, comments, responses to other students, 

46
00:02:51,000 --> 00:02:54,817
links to outside resources, really, anything that contributes 

47
00:02:54,817 --> 00:02:58,617
positively to your learning or the learning of others.

48
00:02:58,617 --> 00:03:02,000
In the fourth assignment, you'll take a web-based interview 

49
00:03:02,000 --> 00:03:05,807
in which the system remembers every answer you give, and asks you 

50
00:03:05,807 --> 00:03:10,000
personalized questions about your diet and lifestyle choices.

51
00:03:10,000 --> 00:03:14,683
The interactivity of this interview goes way beyond a regular web survey

52
00:03:14,683 --> 00:03:19,417
to let you see what interview technology might look like in the future.

53
00:03:19,417 --> 00:03:22,750
Finally, the Day of Compassion Assignment challenges you

54
00:03:22,750 --> 00:03:27,083
to use social psychology for the greater good by living 24 hours 

55
00:03:27,083 --> 00:03:30,950
as compassionately as possible, and asks you to reflect 

56
00:03:30,950 --> 00:03:33,817
on certain aspects of the experience.

57
00:03:33,817 --> 00:03:37,617
The assignment will be peer graded, and the highest scoring essays 

58
00:03:37,617 --> 00:03:40,483
will then be shared so that members of the class can learn

59
00:03:40,483 --> 00:03:43,683
from each other and vote on their favorites.

60
00:03:44,417 --> 00:03:46,617
And here's where it gets interesting!

61
00:03:46,617 --> 00:03:50,216
Students who complete at least four out of the five assignments, 

62
00:03:50,216 --> 00:03:52,417
who score well enough in the course to receive a 

63
00:03:52,417 --> 00:03:55,817
Statement of Accomplishment, and whose Day of Compassion 

64
00:03:55,817 --> 00:03:59,417
Assignment receives a large number of votes from other students 

65
00:03:59,417 --> 00:04:03,083
will become eligible for a Day of Compassion Award 

66
00:04:03,083 --> 00:04:05,950
as a kind of incentive to complete the course, 

67
00:04:05,950 --> 00:04:11,083
learn about social psychology, and use it for the good of society.

68
00:04:11,083 --> 00:04:13,817
I'll describe the award in detail later in the course,

69
00:04:13,817 --> 00:04:16,950
but let me say just a few words about it now.

70
00:04:16,950 --> 00:04:21,083
The first time that the award was given, the winner was Balesh Jindal, 

71
00:04:21,083 --> 00:04:25,017
a wonderful soul from New Delhi, India, whose Day of Compassion

72
00:04:25,017 --> 00:04:28,817
focused on preventing sexual abuse in her community.

73
00:04:28,817 --> 00:04:33,350
And part of the award was an expense paid trip to Northern California, 

74
00:04:33,350 --> 00:04:37,617
where she had the opportunity to personally meet the Dalai Lama -- 

75
00:04:37,617 --> 00:04:40,350
a symbol of compassion to many people.

76
00:04:40,350 --> 00:04:43,350
The meeting was sponsored by Stanford University's

77
00:04:43,350 --> 00:04:47,750
Center for Compassion and Altruism Research and Education.

78
00:04:47,750 --> 00:04:51,883
And the work was later immortalized when Coursera decorated 

79
00:04:51,883 --> 00:04:55,817
its building with a drawing they made of Balesh and a quote from her

81
00:04:55,817 --> 00:04:58,617
about the Day of Compassion assignment.

82
00:04:58,617 --> 00:05:02,883
This year, the award will include free airplane tickets, food, 

83
00:05:02,883 --> 00:05:08,283
and accommodations to meet another world famous symbol of compassion: 

84
00:05:08,283 --> 00:05:12,550
anthropologist Jane Goodall, who will join me in presenting the award

85
00:05:12,550 --> 00:05:16,883
sometime this year or next in a location to be determined.

86
00:05:16,883 --> 00:05:19,683
We'll make every effort to choose a date and place that works 

87
00:05:19,683 --> 00:05:23,417
for the winner, or if that proves impossible, one of the ten students 

88
00:05:23,417 --> 00:05:28,216
receiving Honorable Mention will have the chance to meet Dr. Goodall.

89
00:05:28,216 --> 00:05:32,750
In addition, Social Psychology Network will donate $1,000 to 

90
00:05:32,750 --> 00:05:37,017
the Jane Goodall Institute in honor of the winning student, 

91
00:05:37,017 --> 00:05:41,950
$100 in the name of each student receiving Honorable Mention, and 

92
00:05:41,950 --> 00:05:46,617
$10 in the name of the first 800 other students submitting the 

93
00:05:46,617 --> 00:05:50,683
Day of Compassion Assignment. In other words, our class will donate 

94
00:05:50,683 --> 00:05:55,617
a combined total of $10,000 to the Jane Goodall Institute, which is 

95
00:05:55,617 --> 00:05:59,683
dedicated not only to protecting great apes, but to teaching young people 

96
00:05:59,683 --> 00:06:04,990
to care for their human community, for all animals, and for the environment.

97
00:06:05,900 --> 00:06:09,600
So you have a great opportunity to learn about social psychology 

98
00:06:09,600 --> 00:06:11,617
and use it for the good of others.

99
00:06:11,617 --> 00:06:15,417
I'll say more about each of the assignments when the time comes.

100
00:06:15,417 --> 00:06:19,683
The other 50 points for the course will be based on a final exam, 

101
00:06:19,683 --> 00:06:25,083
with 50 items covering the readings, lectures, and other assigned videos.

102
00:06:25,083 --> 00:06:29,300
The exam will include multiple choice and true-false questions, 

103
00:06:29,300 --> 00:06:34,100
worth one point each, and the exam will be machine-graded.

104
00:06:34,100 --> 00:06:37,417
If your goal is to earn a signed Statement of Accomplishment, 

105
00:06:37,417 --> 00:06:42,283
all that's required is to earn a score of 70 points or better.

106
00:06:42,283 --> 00:06:47,000
For example, if you average a score of eight on each of the five assignments, 

107
00:06:47,000 --> 00:06:52,150
that would total 40 points, and if you get 30 out of 50 exam items correct,

108
00:06:52,150 --> 00:06:57,950
your course total would be 70 points, enough for a Statement of Accomplishment.

109
00:06:57,950 --> 00:07:00,683
And if you end the course with 90 points or more, 

110
00:07:00,683 --> 00:07:04,883
you'll receive a Statement of Accomplishment with Distinction.

111
00:07:04,883 --> 00:07:09,750
Let's pause for a pop-up question, just to make sure this information is clear.

112
00:07:12,617 --> 00:07:16,150
I'd also like to say a few words about communication.

113
00:07:16,150 --> 00:07:19,550
Even though the course is too large for me or the teaching assistants 

114
00:07:19,550 --> 00:07:23,483
to answer email, we'll all be monitoring the discussion forums, 

115
00:07:23,483 --> 00:07:27,883
which is the best way to communicate with us and with each other.

116
00:07:27,883 --> 00:07:29,550
Here's one way to think about it.

117
00:07:29,550 --> 00:07:35,283
If even only one tenth of 1% of the class were to email me on any given day, 

118
00:07:35,283 --> 00:07:40,000
that would be over 100 email messages a day, and I would soon be 

119
00:07:40,000 --> 00:07:43,000
drowning in email. You'd never see me again.

120
00:07:43,570 --> 00:07:46,350
So please be good to the teaching staff, and don't send us 

121
00:07:46,350 --> 00:07:48,750
to the bottom of the sea -- all right?

122
00:07:48,750 --> 00:07:52,700
If you're tempted to email us, here's the image I want you to remember.

123
00:07:57,950 --> 00:08:02,000
Okay, maybe that's a little silly, but use the discussion forums.

124
00:08:02,000 --> 00:08:06,483
They are as important to the course as the videos and the readings.

125
00:08:06,483 --> 00:08:10,400
All I ask is that when you use the forums, you treat people considerately.

126
00:08:10,400 --> 00:08:14,083
Whether you're posting or you're responding to somebody else's post, 

127
00:08:14,083 --> 00:08:19,950
please, no nastiness, no personal attacks, no profanity or ugly language, 

128
00:08:19,950 --> 00:08:22,350
and certainly, no hate speech,

129
00:08:22,350 --> 00:08:25,417
which would be grounds for removal from the course.

130
00:08:25,417 --> 00:08:28,950
If someone writes something that's insensitive or off-base,

131
00:08:28,950 --> 00:08:33,283
don't take the easy way out and open fire. Instead, use the forums 

132
00:08:33,283 --> 00:08:38,950
as an opportunity to help and teach each other -- to elevate the discussion.

133
00:08:38,950 --> 00:08:42,950
In a letter to one of his students, Korean Zen master, Seung Sahn, 

134
00:08:42,950 --> 00:08:46,683
wrote about the power of what he called "together action," 

135
00:08:46,683 --> 00:08:49,683
and I think it fits very well with this course.

136
00:08:49,683 --> 00:08:53,417
He wrote: "Together action is like washing potatoes.

137
00:08:53,417 --> 00:08:56,483
When people wash potatoes in Korea, instead of washing them 

138
00:08:56,483 --> 00:09:00,000
one at a time, they put them all in a tub full of water.

139
00:09:00,000 --> 00:09:04,950
Then someone puts a stick in the tub and pushes it up and down, up and down.

140
00:09:04,950 --> 00:09:07,617
This makes the potatoes rub against each other; 

141
00:09:07,617 --> 00:09:11,617
as they bump into each other, the hard crusty dirt falls off.

142
00:09:11,617 --> 00:09:16,216
If you wash potatoes one at a time, it takes a long time to clean each one, 

143
00:09:16,216 --> 00:09:19,617
and only one potato gets clean at a time.

144
00:09:19,617 --> 00:09:23,817
If they are all together, the potatoes clean each other."

145
00:09:23,817 --> 00:09:27,300
I very much hope that in this course, we all become potatoes 

146
00:09:27,300 --> 00:09:29,283
and learn from each other.

147
00:09:29,283 --> 00:09:32,500
Two final notes before I end this video: 

148
00:09:32,500 --> 00:09:37,000
First, I want to emphasize that this course is not intended to be 

149
00:09:37,000 --> 00:09:40,216
a comprehensive or even representative introduction 

150
00:09:40,216 --> 00:09:42,900
to the field of social psychology.

151
00:09:42,900 --> 00:09:45,883
Rather, it's one professor's perspective on the field,

152
00:09:45,883 --> 00:09:50,950
focusing mainly on material that's useful, fun, and interesting, 

153
00:09:50,950 --> 00:09:56,283
and emphasizing classic studies that have laid a foundation for later research.

154
00:09:56,283 --> 00:09:59,900
In other words, it's sort of like meeting a new person.

155
00:09:59,900 --> 00:10:02,750
If you spend a few hours a week for a month or two, you certainly 

156
00:10:02,750 --> 00:10:05,817
get to know a lot about the person, but you probably won't

157
00:10:05,817 --> 00:10:10,200
end up knowing every important thing about that person.

158
00:10:10,200 --> 00:10:13,750
If you want a comprehensive introduction to social psychology, 

159
00:10:13,750 --> 00:10:17,883
my best advice would be to get a full-length copy of David Myers' 

160
00:10:17,883 --> 00:10:22,400
Social Psychology textbook, which can be purchased as a new book, 

161
00:10:22,400 --> 00:10:26,500
a used book, an ebook, or even rented.

162
00:10:26,500 --> 00:10:30,500
Or, take a full-length college or university course in social psychology, 

163
00:10:30,500 --> 00:10:33,483
if you're able. And throughout this course, 

164
00:10:33,483 --> 00:10:37,200
I'll post suggestions for further reading and viewing, 

165
00:10:37,200 --> 00:10:40,900
and I hope that you'll do the same in our discussion forums.

166
00:10:41,400 --> 00:10:44,600
The last thing that I wanted to mention is that social psychology 

167
00:10:44,600 --> 00:10:48,216
is not the same thing as clinical psychology, 

168
00:10:48,216 --> 00:10:53,300
which is the branch of psychology most directly focused on understanding, preventing, 

169
00:10:53,300 --> 00:10:57,300
and treating mental illness and other psychological problems.

170
00:10:57,300 --> 00:11:01,000
So, if you have depression or other psychological difficulties, 

171
00:11:01,000 --> 00:11:05,600
my best recommendation would be to talk with a mental health professional -- 

172
00:11:05,600 --> 00:11:11,900
for example, a clinical psychologist, a psychiatrist, a counselor, a social worker -- 

173
00:11:11,900 --> 00:11:15,600
or if that's not possible, someone else whom you trust,

174
00:11:15,600 --> 00:11:20,500
someone you feel is safe, supportive, and whose opinion you respect.

175
00:11:20,500 --> 00:11:23,200
Social psychologists are not generally certified 

176
00:11:23,200 --> 00:11:27,500
to provide therapy or mental health advice.

177
00:11:27,500 --> 00:11:31,300
But all's not lost. Social psychology can open the door 

178
00:11:31,300 --> 00:11:35,750
to greater self-understanding and help improve social relationships, 

179
00:11:35,750 --> 00:11:40,500
intergroup relations, group performance, decision making, 

180
00:11:40,500 --> 00:11:45,150
life satisfaction, and many other social aspects of life.

181
00:11:45,150 --> 00:11:48,400
That's what we'll spend much of this course on.

182
00:11:48,400 --> 00:11:52,400
But first, I want to give you a tour of Social Psychology Network, 

183
00:11:52,400 --> 00:11:55,400
and invite you to become a member.
