



1
00:00:38,750 --> 00:00:41,688
It is all around us. 

2
00:00:41,688 --> 00:00:45,900
It is an illusion and yet profoundly real. 

3
00:00:46,160 --> 00:00:52,000
What we perceive as race is one of the first things we notice about each other. 

4
00:00:52,000 --> 00:01:01,400
Skin: darker or lighter. Eyes: round or almond, blue, black, brown. 

5
00:01:01,400 --> 00:01:08,190
Hair: curly, straight, blond, or dark. 

6
00:01:08,190 --> 00:01:13,750
And attached to these characteristics is a mosaic of values, assumptions, and 

7
00:01:13,750 --> 00:01:22,100
historical meanings.  We can see differences among populations, 

8
00:01:22,100 --> 00:01:27,400
but can populations be bundled into what we call races? 

9
00:01:28,200 --> 00:01:31,872
How many races would there be? Five? 

10
00:01:31,872 --> 00:01:37,640
Fifty five? Who decides? 

11
00:01:37,640 --> 00:01:42,500
And how different would they really be from one another? 

12
00:01:44,100 --> 00:01:49,280
 >> The measured amount of genetic variation in the human population is 

13
00:01:49,280 --> 00:01:53,200
extremely small. That's something that people need to wrap 

14
00:01:53,200 --> 00:01:57,600
themselves around, that genetically we really aren't very different. 

15
00:01:59,400 --> 00:02:06,800
>> In fact, genetically, we are among the most similar of all species. 

16
00:02:07,000 --> 00:02:09,600
Only one out of every thousand nucleotides 

17
00:02:09,600 --> 00:02:12,700
that make up our genetic code is different, 

18
00:02:12,700 --> 00:02:21,030
one individual from another. These look-alike penguins have 

19
00:02:21,030 --> 00:02:26,000
twice the amount of genetic difference, one from the other, than humans. 

20
00:02:28,100 --> 00:02:32,860
And these fruit flies, ten times more difference. 

21
00:02:32,860 --> 00:02:37,000
Any two fruit flies may be as different genetically from each other 

22
00:02:37,000 --> 00:02:43,000
as a human is from a chimpanzee. 

22
00:02:49,900 --> 00:02:54,100
as a human is from a chimpanzee. 

24
00:02:54,100 --> 00:02:58,294
Brooklyn Dodgers once hit 40 home runs in a season. No one ever gets it right, 

25
00:02:58,294 --> 00:03:02,370
because the answer is Roy Campanella, who is as Italian as he was black. 

26
00:03:02,370 --> 00:03:04,320
He had an Italian father and a black mother. 

27
00:03:04,320 --> 00:03:08,940
He is always classified as black. You see, American racial classification 

28
00:03:08,940 --> 00:03:11,770
is totally cultural. Who's Tiger Woods? 

29
00:03:11,770 --> 00:03:16,000
Who's Colin Powell? Colin Powell is as Irish as he is African. 

30
00:03:16,400 --> 00:03:19,300
Being black has been defined as just looking 

31
00:03:19,300 --> 00:03:22,900
dark enough that anyone can see you are. 

32
00:03:24,700 --> 00:03:29,010
 >> Now the stars fight a path through the Celtic defense. 

33
00:03:29,010 --> 00:03:32,500
 >> In the urban environment of the 1930s, Jewish teams dominated 

34
00:03:32,500 --> 00:03:37,000
American basketball.  >> Bud Castleman shoots from outside. It's good! 

35
00:03:38,700 --> 00:03:43,500
 >> Sons of immigrants -- theirs were the hoop dreams of the day. 

36
00:03:49,100 --> 00:03:52,500
>> And it was said that the reason that they were so good 

37
00:03:52,500 --> 00:03:56,400
at basketball was because the artful dodger characteristic 

38
00:03:56,400 --> 00:04:01,350
of the Jewish culture made them good at this sport. 

39
00:04:01,350 --> 00:04:07,200
There are strong cultural aspects of what sports individuals choose to play. 

40
00:04:07,200 --> 00:04:12,000
It has to do with the interaction of individual genetic background, 

41
00:04:12,000 --> 00:04:19,360
of opportunity, and training. History shows us that as 

42
00:04:19,360 --> 00:04:26,000
opportunities change in society, different groups get drawn into sporting arenas. 

43
00:04:27,600 --> 00:04:31,200
 >> By 1992, America's Olympic Dream Team 

44
00:04:31,200 --> 00:04:36,700
was almost completely African-American. 

45
00:04:37,100 --> 00:04:42,800
Ten years later, 20% of NBA starters would be foreign born; 

46
00:04:42,800 --> 00:04:51,290
the top NBA draft pick, Chinese. In the 1960s, Richard Lewington decided to 

47
00:04:51,290 --> 00:04:56,300
find out just how much genetic variation fell within and how much between  

48
00:04:56,300 --> 00:05:01,960
the groups we regard as races. A new technology enabled him to do 

49
00:05:01,960 --> 00:05:07,000
pioneering work. >> And that method, which is called gel electrophoresis, 

50
00:05:07,000 --> 00:05:11,410
a very fancy name, we were able to use on any organism at all. 

51
00:05:11,410 --> 00:05:13,800
If you could grind it up, you could do it. 

52
00:05:14,000 --> 00:05:15,690
That included people. I mean, you don't have to grind the whole person, 

53
00:05:15,690 --> 00:05:18,900
but you could take a little bit of tissue or blood. 

54
00:05:19,690 --> 00:05:24,100
Over the years a lot of data were gathered by anthropologists and 

55
00:05:24,100 --> 00:05:29,260
geneticists, looking at blood group genes and protein genes and other kinds of 

56
00:05:29,260 --> 00:05:32,530
genes from all over the world. I mean, anthropologists just went around 

57
00:05:32,530 --> 00:05:37,980
taking blood out of everybody. I must say, if I were a South American 

58
00:05:37,980 --> 00:05:41,630
Indian, I wouldn't have let them take my blood, but they did. 

59
00:05:41,630 --> 00:05:44,400
And so I thought, "Well, we've got enough of these data -- let's see 

60
00:05:44,400 --> 00:05:48,800
what it tells us about the differences between human groups." 

61
00:05:49,300 --> 00:05:54,100
 >> Lewington's findings were a milestone in the study of race and biology. 

62
00:05:54,400 --> 00:06:00,870
 >> If you put it all together, we've now got that for proteins, for blood groups, and 

63
00:06:00,870 --> 00:06:06,000
now with DNA sequencing, we have it for DNA sequence differences, 

64
00:06:06,000 --> 00:06:12,600
it always comes out the same: 85% of all the variation among human beings 

65
00:06:12,600 --> 00:06:18,000
is between any two individuals within any local population -- between 

66
00:06:18,000 --> 00:06:26,000
individuals within Sweden or within the Chinese or the Kikuyu or the Icelanders. 

67
00:06:26,200 --> 00:06:32,030
>> There just hasn't been time for the development of much genetic variation, 

68
00:06:32,030 --> 00:06:37,500
except that which regulates some very superficial features like skin color and hair. 

69
00:06:37,500 --> 00:06:42,810
For once the old cliche is true: under the skin we really are effectively 

70
00:06:42,810 --> 00:06:45,140
the same, and we get fooled because 

71
00:06:45,140 --> 00:06:49,300
some of the visual differences are quite noticeable. 

72
00:06:51,300 --> 00:06:56,685
 >> The superficial traits we use to construct race are recent variations. 

73
00:06:58,200 --> 00:07:04,200
By the time they arose, important and complicated traits like speech, 

74
00:07:04,200 --> 00:07:11,200
abstract thinking, even physical prowess, had already evolved. 

75
00:07:13,500 --> 00:07:20,400
 >> As geneticists, we now have the opportunity to investigate, using proper 

76
00:07:20,400 --> 00:07:26,720
genomic analysis, complex human traits -- athletic ability, musical ability, 

77
00:07:26,720 --> 00:07:31,300
intelligence, all these wonderful traits that we wish we understood better, 

78
00:07:31,300 --> 00:07:36,000
and for which we'd very much like to know if there are genes that are involved, 

79
00:07:36,000 --> 00:07:42,800
how they interact, how they play out. Those traits are old. 

80
00:07:42,800 --> 00:07:46,800
We spent most of our history as a species together in Africa, 

81
00:07:46,800 --> 00:07:50,000
in small populations, before anyone left.  

82
00:07:50,000 --> 00:07:55,300
>> Physical differences don't make race. What makes race are the laws and 

83
00:07:55,300 --> 00:08:02,400
practices that affect life chances and opportunities based on those differences. 

84
00:08:05,800 --> 00:08:10,700
Courts and legislators had long been in the business of conferring racial identities. 

85
00:08:10,700 --> 00:08:17,200
In the South, to enforce Jim Crow segregation and laws against mixed marriages 

86
00:08:17,200 --> 00:08:22,300
courts had to first determine who was black under law. 

87
00:08:22,300 --> 00:08:24,500
>> And here is where it really gets interesting. 

88
00:08:24,500 --> 00:08:30,000
You got some places, for example, Virginia -- Virginia law defined 

89
00:08:30,000 --> 00:08:35,100
a black person as a person with 1/16th African ancestry. 

90
00:08:35,100 --> 00:08:42,000
Now, Florida defined a black person as a person with 1/8th African ancestry. 

91
00:08:42,000 --> 00:08:45,000
Alabama said you're black if you have any black ancestry -- 

92
00:08:45,000 --> 00:08:49,100
any African ancestry at all. But you know what this means? 

93
00:08:49,100 --> 00:08:54,700
You can walk across a state line and literally, legally change race. 

94
00:08:54,700 --> 00:08:58,300
Now, what does race mean under those circumstances? 

95
00:08:58,300 --> 00:09:01,600
You give me the power, I can make you any race I want you to be 

96
00:09:01,600 --> 00:09:07,000
because it is a social, political construction. 

