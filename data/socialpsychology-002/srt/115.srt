1
00:00:08,150 --> 00:00:11,550
The difference between these two men seems obvious: 

2
00:00:11,550 --> 00:00:15,883
their faces are different, their hair is different -- even their shirts are

3
00:00:15,883 --> 00:00:21,017
a different color. And yet an experiment by psychologists Dan Simons and Chris Chabris

4
00:00:21,017 --> 00:00:24,350
at Harvard reveals that our brains actually process 

5
00:00:24,350 --> 00:00:26,500
very little of what comes in through the eyes.

6
00:00:27,750 --> 00:00:32,683
>> In this experiment, the subject comes up to a counter 

7
00:00:32,683 --> 00:00:37,000
and a first experimenter hands them a consent form.

8
00:00:38,683 --> 00:00:41,750
As soon as they finish signing the consent form, they hand it back 

9
00:00:41,750 --> 00:00:45,483
to the first experimenter, who then takes the consent form, 

10
00:00:45,483 --> 00:00:49,617
ducks down behind the counter to put it away, and a different experimenter 

11
00:00:49,617 --> 00:00:53,683
then stands up and hands the subject a packet of information 

12
00:00:53,683 --> 00:00:57,550
and sends them into a hallway, where we ask them questions about it.

14
00:00:57,550 --> 00:01:00,000
>> This wonderful experiment uncovers an aspect of 

15
00:01:00,000 --> 00:01:03,550
the brain's attention system known as "change blindness."

16
00:01:03,550 --> 00:01:07,550
>> Change blindness is the idea that we often miss large changes 

17
00:01:07,550 --> 00:01:10,417
to our visual world from one view to the next.

18
00:01:10,417 --> 00:01:14,216
We're often not able to see large changes that would appear to be 

19
00:01:14,216 --> 00:01:17,550
perfectly obvious to somebody who knows they're going to happen.

21
00:01:18,883 --> 00:01:26,460
>> And incredibly, in 75% of cases the subjects don't notice a thing.

22
00:01:27,460 --> 00:01:30,350
>> The lady who took me up here, she opened the door for me, 

23
00:01:30,350 --> 00:01:32,050
and told me to walk over to the desk.

24
00:01:32,050 --> 00:01:34,216
>> Fill out this form. >> Alright.

26
00:01:34,216 --> 00:01:36,483
I think there was a sign that said "Experiment." >> Um-hmm.

28
00:01:36,483 --> 00:01:42,017
>> And a man there gave me a form to sign. >> Um-hmm.

30
00:01:42,017 --> 00:01:46,483
>> There was a guy standing under a big sign that said "Experiment." >> Um-hmm. 

31
00:01:46,483 --> 00:01:48,550
>> And to my left there was, like, a pot 

33
00:01:48,550 --> 00:01:52,000
with some dirt in it and some plastic containers.

34
00:01:52,000 --> 00:01:56,350
>> Is it the 5th? >> Ah, yes, it's the 5th. 

39
00:01:56,350 --> 00:02:00,550
>> Oh, I filled out a form. >> Alright, this form. >> How long would it be about 

40
00:02:00,550 --> 00:02:03,617
before it -- And then I asked how long it would take, and he said 

41
00:02:03,617 --> 00:02:09,000
about 10, 15 minutes or so. I guess I have the time to do that.

42
00:02:10,550 --> 00:02:14,550
>> Did you notice anything unusual at all after you signed the consent form?

44
00:02:14,550 --> 00:02:16,417
>> I just signed it, and I didn't even pay attention 

45
00:02:16,417 --> 00:02:18,500
to anything that was written on it. >> Okay. 

48
00:02:18,500 --> 00:02:20,750
Um, after, after you handed it back to him, 

49
00:02:20,750 --> 00:02:25,000
did you notice anything unusual happen at all?

50
00:02:25,000 --> 00:02:26,550
>> If you can just take this into the next room.

51
00:02:26,550 --> 00:02:30,350
No, no I, I probably wasn't even looking that direction. >> Okay.

53
00:02:30,350 --> 00:02:34,417
>> I turned and looked towards the clear door and saw some people there. >> Okay.

54
00:02:34,417 --> 00:02:38,817
>> And then I turned back and looked at him for a second. >> Okay.

57
00:02:38,817 --> 00:02:45,083
Did you notice anything unusual at all after you signed the consent form? >> No.

58
00:02:45,083 --> 00:02:47,617
>> This is only going to take five or ten minutes? >> Yeah. >> Okay.

62
00:02:47,617 --> 00:02:49,617
>> It'll take, it'll be real short. >> Okay.

63
00:02:49,617 --> 00:02:52,150
>> The person who stood up was actually a different person. >> Okay.

64
00:02:52,150 --> 00:02:55,083
>> Um, I gather you didn't notice that that was different? >> No. 

66
00:02:55,083 --> 00:02:58,283
>> A different person actually stood up and handed you this form, um,

68
00:02:58,283 --> 00:03:00,083
and sent you out toward me.

69
00:03:00,083 --> 00:03:03,350
Um, I gather from your reaction, you didn't notice that? >> No!

71
00:03:03,350 --> 00:03:05,950
>> Okay, um, don't, don't feel bad about that.

73
00:03:05,950 --> 00:03:09,750
Actually, most of the time, we find about 75% of people don't notice.

74
00:03:09,750 --> 00:03:11,883
>> You serious? >> Yeah. >> The person who stood up 

77
00:03:11,883 --> 00:03:13,500
with the packet was actually a different person --

78
00:03:13,500 --> 00:03:17,617
>> Wow! >> than, ah, than the first person. >> That is incredible!

80
00:03:17,617 --> 00:03:21,090
>> So, I gather, I gather you didn't see it? >> I didn't catch that, no. >> Okay.

82
00:03:21,090 --> 00:03:23,883
>> What's really interesting is that some people notice these changes, 

83
00:03:23,883 --> 00:03:28,950
and other people don't notice these changes.

84
00:03:28,950 --> 00:03:30,483
>> Okay. If you could just take this --

86
00:03:30,483 --> 00:03:34,083
>> And we really don't yet have a good idea what separates 

87
00:03:34,083 --> 00:03:38,683
those people who don't from those people who do.

88
00:03:38,683 --> 00:03:41,300
It might be that there are individual differences, and some people 

89
00:03:41,300 --> 00:03:44,417
are better able to detect these sorts of changes.

90
00:03:44,417 --> 00:03:48,550
But it's also possible that it's just coincidence,

91
00:03:48,550 --> 00:03:52,000
that the people who noticed it, just happened to be focusing 

92
00:03:52,000 --> 00:03:55,217
on a feature that changed -- they just happened to be paying attention 

93
00:03:55,217 --> 00:04:01,750
to the color of the person's shirt -- and the people who failed to notice it,

96
00:04:01,750 --> 00:04:04,400
just happened to be paying attention to something else.

