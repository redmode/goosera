



1
00:00:06,760 --> 00:00:11,860
In the last few lectures, we've broadened 
our focus from individuals to groups, but 

2
00:00:11,860 --> 00:00:16,420
we've concentrated mainly on the effect 
that groups have on a lone individual, 

3
00:00:16,420 --> 00:00:20,850
a single person -- 
for example, group pressure, or conformity, 

4
00:00:20,850 --> 00:00:24,550
or the road from individuation to 
deindividuation. 

5
00:00:24,550 --> 00:00:30,120
In the next part of the course, we'll 
take a look at group psychology itself -- 

6
00:00:30,120 --> 00:00:36,020
what's sometimes called "group dynamics." 
Group dynamics is just a fancy term for 

7
00:00:36,020 --> 00:00:41,070
psychological processes and behaviors 
that occur either within a group 

8
00:00:41,070 --> 00:00:47,800
(that is, intragroup dynamics) or between 
groups (intergroup dynamics). 

9
00:00:47,800 --> 00:00:51,825
For example, one of the dynamics covered 
in this week's assigned reading is 

10
00:00:51,825 --> 00:00:57,310
groupthink, a well known group dynamic in 
which the quality of group decision 

11
00:00:57,310 --> 00:01:02,930
making suffers when a cohesive group 
becomes insulated from dissenting 

12
00:01:02,930 --> 00:01:06,400
viewpoints, 
especially when a group leader promotes a 

13
00:01:06,400 --> 00:01:11,430
particular solution or a particular 
course of action, and others become 

14
00:01:11,430 --> 00:01:16,410
reluctant to disagree. 
The person who conducted most of the 

15
00:01:16,410 --> 00:01:20,900
initial research on groupthink and who 
did more than anyone else to popularize 

16
00:01:20,900 --> 00:01:26,840
the term was Irving Janis, a Yale 
University social psychologist, whose work 

17
00:01:26,840 --> 00:01:31,590
became so well known that when Barack 
Obama was elected President of the United 

18
00:01:31,590 --> 00:01:37,680
States in 2008 and announced his national 
security team, he explicitly went on 

19
00:01:37,680 --> 00:01:42,800
record as saying that he wanted to 
appoint a team that would avoid groupthink. 

20
00:01:43,500 --> 00:01:45,730
Here's what he said in a press conference 

21
00:01:45,730 --> 00:01:53,570
within weeks of being elected. 
 >> I assembled this team because I'm a 

22
00:01:53,570 --> 00:01:58,710
strong believer in strong personalities 
and strong opinions. 

23
00:01:58,710 --> 00:02:01,620
I think that's how the best decisions are 
made. 

24
00:02:01,620 --> 00:02:07,550
One of the dangers in a White House, based 
on my reading of history, is that you get 

25
00:02:07,550 --> 00:02:13,440
wrapped up in groupthink, and everybody 
agrees with everything, and there is no 

26
00:02:13,440 --> 00:02:16,315
discussion, and there, there are no 
dissenting views. 

27
00:02:16,315 --> 00:02:20,900
So, I'm going to be welcoming a vigorous 
debate inside the White House. 

28
00:02:22,330 --> 00:02:26,400
 >> Isn't that something? 
It's a rare treat to see social psychology 

29
00:02:26,400 --> 00:02:30,200
used at the highest levels of 
government, and of course, 

30
00:02:30,200 --> 00:02:34,140
welcoming debate is just one way to avoid 
groupthink. 

31
00:02:34,140 --> 00:02:38,980
Another technique, one that's sometimes 
practiced in the Japanese corporate world, 

32
00:02:38,980 --> 00:02:43,230
is to have the lowest ranking members of 
a group speak first, 

33
00:02:43,230 --> 00:02:48,540
then have the people right above that go 
next, and so on, up the status hierarchy, 

34
00:02:48,540 --> 00:02:52,660
so that employees never have to 
contradict their boss -- 

35
00:02:52,660 --> 00:02:58,000
a very clever management technique for 
soliciting a wide range of opinions. 

36
00:02:58,620 --> 00:03:03,750
Beyond groupthink, here are a few topics 
covered in three brief chapters I've 

37
00:03:03,750 --> 00:03:06,900
assigned this week from David Myers' 
wonderful book 

38
00:03:06,900 --> 00:03:11,310
"Exploring Social Psychology." 
All of these topics involve group 

39
00:03:11,310 --> 00:03:15,410
dynamics that are very useful to know 
about. 

40
00:03:15,410 --> 00:03:19,460
First, you'll read about social loafing 
and free riders. 

41
00:03:19,460 --> 00:03:24,480
Here we have a couch potato. 
In the chapter on social loafing, 

42
00:03:24,480 --> 00:03:29,940
you'll also get the answer to this true/false 
item from the Snapshot Quiz: 

43
00:03:29,940 --> 00:03:34,330
"In a tug-of-war game with the same number 
of people on each side of the rope, 

44
00:03:34,330 --> 00:03:38,610
people pull harder when they're part of 
an eight-person team than when they're 

45
00:03:38,610 --> 00:03:42,940
pulling alone." 
And even more importantly, you'll find out 

46
00:03:42,940 --> 00:03:48,900
why this matters. 
Here's the answer that you gave on the Snapshot Quiz.

47
00:03:51,200 --> 00:03:54,230
After the chapter on social loafing, 

48
00:03:54,230 --> 00:03:58,600
you'll read about group polarization and 
the risky shift -- 

49
00:03:58,600 --> 00:04:03,500
a research detective story that began 
with some unexpected findings in a 

50
00:04:03,500 --> 00:04:07,979
masters thesis. 
And you'll read about some research findings 

51
00:04:07,979 --> 00:04:12,300
on deindividuation that extend 
our earlier discussion. 

52
00:04:13,124 --> 00:04:17,092
Of course, at this point it might seem 
like maybe we should just get rid of groups in 

53
00:04:17,092 --> 00:04:21,636
light of all the problems: conformity  
and groupthink, deindividuation, 

54
00:04:21,636 --> 00:04:26,801
group polarization, social loafing, and the list 
goes on. 

55
00:04:26,801 --> 00:04:31,289
But the news isn't all bad. 
For example, when it comes to solving difficult 

56
00:04:31,289 --> 00:04:36,500
problems of logic or problems 
that require a wide base of knowledge, 

57
00:04:36,500 --> 00:04:44,700
groups typically outperform individuals. 
That is, many heads are often better than one. 

58
00:04:44,700 --> 00:04:47,500
This is just one reason why cultures 
around the world 

59
00:04:47,500 --> 00:04:51,506
rely so heavily on working groups and 
committees, 

60
00:04:51,506 --> 00:04:58,810
panels, boards, task forces, and, of course, 
juries. But why do groups outperform 

61
00:04:58,810 --> 00:05:02,340
individuals? 
Is it because group members interact with 

62
00:05:02,340 --> 00:05:04,850
one another and build on each other's 
ideas, 

63
00:05:04,850 --> 00:05:09,620
or would the group do just as well if 
its members worked alone 

64
00:05:09,620 --> 00:05:13,770
without interacting? 
This was essentially the question raised 

65
00:05:13,770 --> 00:05:17,700
by the following true/false item from the 
Snapshot Quiz: 

66
00:05:17,700 --> 00:05:23,400
"Five people will usually generate more 
solutions to a difficult problem if they 

67
00:05:23,400 --> 00:05:29,350
work together in person than if they work 
alone during the same period of time." 

68
00:05:29,350 --> 00:05:32,170
You can see your own answer to this 
question here. 

69
00:05:35,750 --> 00:05:41,960
Alex Osborne, who popularized the term 
"brainstorming" over 60 years ago, proposed 

70
00:05:41,960 --> 00:05:46,040
that there's a sort of synergy that takes 
place when group members interact with 

71
00:05:46,040 --> 00:05:49,400
each other, that the whole is greater 
than the sum of the parts -- 

72
00:05:49,400 --> 00:05:54,000
a synergy that would make this Snapshot 
Quiz item true. 

73
00:05:54,200 --> 00:05:56,810
According to Osborne, groups that 

74
00:05:56,810 --> 00:06:01,800
followed his guidelines for brainstorming 
were twice as productive as groups whose 

75
00:06:01,800 --> 00:06:06,000
members worked alone. 
What does the research record show? 

76
00:06:06,000 --> 00:06:11,820
There is by now a mountain of evidence 
suggesting that this quiz item and 

77
00:06:11,820 --> 00:06:18,450
Osborne's assertion are both false. 
In fact, Professor Paul Paulus, who's a 

78
00:06:18,450 --> 00:06:22,430
world renowned authority on 
brainstorming, estimates that small 

79
00:06:22,430 --> 00:06:28,200
interacting groups (typically, meaning 
four or more people) generate only about 

80
00:06:28,200 --> 00:06:32,820
half as many solutions as the same number of 
people working alone -- 

81
00:06:32,820 --> 00:06:39,140
what's known as a "nominal group" (a group 
in name only) that pools its ideas and 

82
00:06:39,140 --> 00:06:43,600
removes duplicate ideas suggested by 
more than one person. 

83
00:06:44,000 --> 00:06:49,000
Why do nominal groups tend to outperform 
interactive brainstorming groups? 

84
00:06:49,000 --> 00:06:55,000
Well, the most obvious reason is probably 
what psychologists call "production blocking," 

85
00:06:55,000 --> 00:06:57,490
the loss in productivity that comes when 

86
00:06:57,490 --> 00:07:01,470
one person's talking 
and everyone else is largely blocked 

87
00:07:01,470 --> 00:07:05,040
from talking or developing their own 
ideas. 

88
00:07:05,040 --> 00:07:10,190
To overcome, or at least minimize, that 
problem, Professor Paulus suggests 

89
00:07:10,190 --> 00:07:14,600
alternating between interactive and 
nominal group approaches. 

90
00:07:14,600 --> 00:07:19,000
For example, spend 20 minutes working 
together to get the benefit of diverse 

91
00:07:19,000 --> 00:07:24,858
perspectives and then have group members 
spend time working independently, 

92
00:07:24,858 --> 00:07:29,890
or in some cases, you might ask group 
members to work alone first, 

93
00:07:29,890 --> 00:07:33,380
so that they don't settle on a particular solution 
prematurely. 

94
00:07:33,380 --> 00:07:37,660
Then, hold a group brainstorming session 
to share different viewpoints, 

95
00:07:37,660 --> 00:07:42,150
and finally, spend the rest of the time 
working independently without any 

96
00:07:42,150 --> 00:07:46,240
production blocking. 
Whatever the mix, the important thing is to 

97
00:07:46,240 --> 00:07:51,900
build in time for people to work on 
their own -- not just time in a group setting. 

98
00:07:52,900 --> 00:07:56,000
So these are just a few ways to avoid 
production blocking 

99
00:07:56,000 --> 00:07:60,300
while still allowing 
group members to share information, and of course, 

100
00:07:60,300 --> 00:08:04,590
if you want a group to function 
effectively, whether it's a campus group or 

101
00:08:04,590 --> 00:08:09,700
a committee or even a family, group members 
have to have a way to share their opinions, 

102
00:08:09,700 --> 00:08:12,970
and their preferences, and their 
perspectives, 

103
00:08:12,970 --> 00:08:16,670
and so forth. 
Sounds easy, but in fact, groups are 

104
00:08:16,670 --> 00:08:22,090
notoriously ineffective at sharing 
information among their members unless a 

105
00:08:22,090 --> 00:08:27,200
deliberate effort is made to do so. 
And even then it's not always so easy. 

106
00:08:27,450 --> 00:08:30,970
For example, some group members might be 
shy. 

107
00:08:30,970 --> 00:08:34,950
Others might fear looking incompetent or 
foolish. 

108
00:08:34,950 --> 00:08:37,670
Still others might want to avoid 
conflict. 

109
00:08:37,670 --> 00:08:41,000
And of course, there is always peer 
pressure and conformity. 

110
00:08:42,380 --> 00:08:47,260
Whatever the factors, groups often try to 
move in one coordinated direction, 

111
00:08:47,260 --> 00:08:51,200
while their group members are moving in all 
sorts of other directions, 

112
00:08:51,200 --> 00:08:55,500
which can lead to some complicated and 
very messy group dynamics. 

113
00:08:55,500 --> 00:09:00,150
To illustrate what I mean, I recently 
visited my university's campus center 

114
00:09:00,150 --> 00:09:05,400
during lunch and asked the students there 
to help with a strange little demonstration. 

115
00:09:06,200 --> 00:09:08,900
What I asked them to do was to stand up 

116
00:09:08,900 --> 00:09:12,930
wherever they were seated, to put their 
books down or to stop eating for a 

117
00:09:12,930 --> 00:09:18,130
moment, just to stand up and to close 
their eyes. 

118
00:09:18,130 --> 00:09:22,140
Then I asked them to turn in the 
direction they believed was north and to 

119
00:09:22,140 --> 00:09:26,190
simply take their best guess if they 
weren't sure, so that everyone was facing 

120
00:09:26,190 --> 00:09:31,890
north to the best of their knowledge. 
Finally, once everyone had turned north, 

121
00:09:31,890 --> 00:09:37,820
I asked students to point directly ahead 
so that we can see which way north is. 

122
00:09:37,820 --> 00:09:43,540
Here was the end result. 
You can see from some of the smiles, 

123
00:09:43,540 --> 00:09:46,675
that students were being very good sports 
about the demonstration, 

124
00:09:46,675 --> 00:09:50,350
and afterward, they even recorded a 
greeting to our class. 

125
00:09:51,300 --> 00:09:57,470
 >> Hello, Coursera! 
 >> So there you have it. 

126
00:09:57,470 --> 00:10:01,690
But now, getting back to group dynamics, 
what if instead of pointing north, 

127
00:10:01,690 --> 00:10:06,490
you were in a group that had a certain 
direction, a certain group objective? 

128
00:10:06,490 --> 00:10:11,230
Think about a group that you've been in -- 
maybe it's a group of friends or students 

129
00:10:11,230 --> 00:10:16,200
or coworkers or family members. 
This is what your group would look like. 

130
00:10:17,680 --> 00:10:22,400
How efficient is your group going to be 
if everyone defines north in their own way -- 

131
00:10:22,400 --> 00:10:24,700
if everyone's moving in a different direction? 

132
00:10:25,280 --> 00:10:27,380
Not very efficient. 

133
00:10:27,380 --> 00:10:31,170
Yet, this kind of group dynamic is 
actually quite common. 

134
00:10:31,170 --> 00:10:34,790
In fact, some groups are even more 
extreme. 

135
00:10:34,790 --> 00:10:37,962
Their group dynamic actually looks like 
this. 

136
00:10:38,800 --> 00:10:43,500
That is, even though every member of the 
group prefers to go in the same direction, 

137
00:10:43,500 --> 00:10:46,530
the group actually goes in the 
opposite direction. 

138
00:10:48,580 --> 00:10:53,100
How is this possible? 
Well, this particular paradox of group behavior, 

139
00:10:53,100 --> 00:10:58,310
known as the Abilene Paradox, is 
the subject of a video that I'm very pleased 

140
00:10:58,310 --> 00:11:03,100
to share with you thanks to the 
generosity of Jerry Harvey, Peter Jordan, 

141
00:11:03,100 --> 00:11:07,800
and CRM Learning. 
The video is shot with a very light touch, 

142
00:11:07,800 --> 00:11:10,250
so it's a lot of fun to watch,

143
00:11:10,250 --> 00:11:15,100
yet it's very perceptive, and it's filled 
with great social psychology. 

144
00:11:15,100 --> 00:11:21,300
In a nutshell, the thesis is that group 
dynamics don't have to involve conflict 

145
00:11:21,300 --> 00:11:25,800
in order for there to be a problem. 
You can have a problem even when group 

146
00:11:25,800 --> 00:11:32,860
members are 100% in agreement. 
To find out why and what to do about it, 

147
00:11:32,860 --> 00:11:38,590
please watch the Abilene Paradox before 
you continue watching more lectures. Okay? 

148
00:11:38,590 --> 00:11:42,000
And to finish this video, here's a pop-up 
question. 

