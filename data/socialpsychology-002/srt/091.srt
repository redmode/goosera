1
00:00:06,600 --> 00:00:08,800
I'd like to begin this lecture with a question about

2
00:00:08,800 --> 00:00:12,800
your friends and, if you have one, your romantic partner.

3
00:00:12,800 --> 00:00:16,876
When you think about the people you've chosen as a friend or a partner, what's

4
00:00:16,876 --> 00:00:22,200
the single most important factor that led you to be with one person and not another?

5
00:00:22,200 --> 00:00:27,530
Let's consider some possibilities. The person is kind or generous,

6
00:00:27,530 --> 00:00:32,640
smart or skilled, fun to be with, physically attractive,

7
00:00:32,640 --> 00:00:37,000
shares your values and attitudes, likes to do the same things you do. 

8
00:00:37,000 --> 00:00:39,960
Or maybe, it's none of the above.

9
00:00:41,230 --> 00:00:46,207
What researchers have found is that in many cases, it is none of the above.

10
00:00:46,207 --> 00:00:50,167
Surprisingly, there's a very powerful factor that we rarely think of as

11
00:00:50,167 --> 00:00:55,108
important but is essential when it comes to friendship and romantic choices.

12
00:00:55,108 --> 00:00:59,000
That factor is known as proximity or closeness -- 

13
00:00:59,000 --> 00:01:03,070
not just physical closeness, but social closeness -- 

14
00:01:03,070 --> 00:01:05,490
that is, opportunities to interact.

15
00:01:05,490 --> 00:01:08,190
This is one of the reasons why people so often end up becoming

16
00:01:08,190 --> 00:01:11,500
friends with -- or even marrying -- their classmates,

17
00:01:11,500 --> 00:01:14,050
their co-workers, their neighbors, and so on.

18
00:01:15,120 --> 00:01:18,588
The classic study on proximity and attraction was published

19
00:01:18,588 --> 00:01:23,870
in 1950 by Leon Festinger, Stanley Schachter, and Kurt Back.

20
00:01:23,870 --> 00:01:26,000
These researchers looked at friendship formation 

21
00:01:26,000 --> 00:01:29,800
in a new housing project for married students.

22
00:01:29,800 --> 00:01:33,000
The project was made up of a series of small houses 

23
00:01:33,000 --> 00:01:36,396
arranged like horseshoes around a courtyard area.

24
00:01:36,396 --> 00:01:41,000
So, the researchers could examine factors such as how many houses away 


25
00:01:41,000 --> 00:01:45,000
students were from their closest friends, whether the students 

26
00:01:45,000 --> 00:01:50,170
lived on an end unit or in between two other houses, and so on.

27
00:01:50,170 --> 00:01:53,000
Summarizing the research, Festinger had this to say:

28
00:01:53,950 --> 00:01:57,423
"The two major factors affecting the friendships which developed were 

29
00:01:57,423 --> 00:02:05,440
(1) sheer distance between houses and (2) the direction in which a house faced.

30
00:02:05,440 --> 00:02:09,210
Friendships developed more frequently between next-door neighbors,

31
00:02:09,210 --> 00:02:11,604
less frequently between people whose houses were

32
00:02:11,604 --> 00:02:14,800
separated by another house, and so on.

33
00:02:14,800 --> 00:02:18,646
As the distance between houses increased, the number of friendships 

34
00:02:18,646 --> 00:02:22,000
fell off so rapidly that it was rare to find a friendship 

35
00:02:22,000 --> 00:02:24,054
between persons who lived in houses that

36
00:02:24,054 --> 00:02:28,600
were separated by more than four or five other houses."

37
00:02:29,610 --> 00:02:30,840
Interesting, huh?

38
00:02:30,840 --> 00:02:34,130
I mean, these houses are not very far apart from each other --

39
00:02:34,130 --> 00:02:37,000
I think only about 20 feet from door to door.

40
00:02:37,000 --> 00:02:38,737
So to see that kind of sharp drop off

41
00:02:38,737 --> 00:02:42,770
after only four or five houses is really pretty dramatic.

42
00:02:42,770 --> 00:02:46,000
But the results get even more remarkable.

43
00:02:46,000 --> 00:02:50,000
I went to my university library and picked up the Journal of Social Issues, 

44
00:02:50,000 --> 00:02:53,723
where Leon Festinger published another account of the research,

45
00:02:53,723 --> 00:02:57,000
because I want to read a particular passage to you -- 

46
00:02:57,000 --> 00:02:58,810
a particularly noteworthy passage.

47
00:02:58,810 --> 00:03:01,130
So bear with me. He wrote:

48
00:03:01,130 --> 00:03:05,000
"In order to have the street appear lived on, ten of the houses 


49
00:03:05,000 --> 00:03:08,300
near the street had been turned so that they faced the street 


50
00:03:08,300 --> 00:03:11,920
rather than the court area like the other houses.

51
00:03:11,920 --> 00:03:15,000
This apparently small change in the direction in which a house 

52
00:03:15,000 --> 00:03:18,700
faced had a considerable effect on the lives of the people 

53
00:03:18,700 --> 00:03:23,040
who by accident happened to occupy these end houses.

54
00:03:23,040 --> 00:03:27,000
They had less than half as many friends in the project, 

55
00:03:27,000 --> 00:03:30,500
as did those whose houses faced the court area.

56
00:03:30,500 --> 00:03:33,859
The consistency of this finding left no doubt that the turning of 

57
00:03:33,859 --> 00:03:37,800
these houses toward the street had made involuntary social

58
00:03:37,800 --> 00:03:41,200
isolates out of the persons who lived in them."

59
00:03:41,200 --> 00:03:43,500
I mean, really quite amazing to think that

60
00:03:43,500 --> 00:03:45,900
such a small change in the direction of the

61
00:03:45,900 --> 00:03:48,000
house would have such a large effect on

62
00:03:48,000 --> 00:03:50,930
the social lives of the people who live there.

63
00:03:50,930 --> 00:03:53,790
I also happened to check out the book

64
00:03:53,790 --> 00:03:57,670
that Festinger and his colleagues published about the study.

65
00:03:57,670 --> 00:04:01,199
And here you can see a picture of what the housing project looked like.

66
00:04:02,470 --> 00:04:04,370
There, you see that there are streets running

67
00:04:04,370 --> 00:04:07,600
through it and courtyard areas, and so on.

68
00:04:07,600 --> 00:04:10,390
And Festinger and his colleagues also replicated

69
00:04:10,390 --> 00:04:14,220
the results in ways that supported these conclusions.

70
00:04:14,220 --> 00:04:16,840
For example, they found that the most popular residents

71
00:04:16,840 --> 00:04:19,405
in an apartment complex tended to be the people whose

72
00:04:19,405 --> 00:04:24,920
apartments were located near stairwells, which have a lot of traffic.

73
00:04:24,920 --> 00:04:27,944
And people who live near mailboxes -- again, where there's a lot of

74
00:04:27,944 --> 00:04:32,370
foot traffic -- tend to have more active social lives than do others. 

75
00:04:33,700 --> 00:04:36,910
Something to keep in mind if you're ever apartment hunting! 

76
00:04:36,910 --> 00:04:39,000
Well, in addition to proximity, another factor

77
00:04:39,000 --> 00:04:43,182
that's very important in interpersonal attraction is similarity.

78
00:04:43,182 --> 00:04:45,784
Research has shown that people tend to be more attracted

79
00:04:45,784 --> 00:04:49,888
to others who are similar to them in age and education level, 

80
00:04:49,888 --> 00:04:56,000
race and ethnicity, personality and attitudes, economic status, and even 

81
00:04:56,000 --> 00:05:02,500
certain physical characteristics, such as height, wrist size, and blood type.

82
00:05:02,500 --> 00:05:04,264
The last few correlations tend to be small,

83
00:05:04,264 --> 00:05:07,040
and it's not always clear why they occur.

84
00:05:07,040 --> 00:05:08,952
It might be a function of common ethnicity, 

85
00:05:08,952 --> 00:05:13,170
shared ancestry, migration patterns, and so on.

86
00:05:13,170 --> 00:05:15,200
But regardless, similarity is important for 

87
00:05:15,200 --> 00:05:19,730
both friendship formation and the choice of romantic partners.

88
00:05:19,730 --> 00:05:23,370
So birds of a feather really do flock together, 

89
00:05:23,370 --> 00:05:27,690
and the idea that opposites attract, like two pieces of a jigsaw puzzle --

90
00:05:27,690 --> 00:05:31,000
something known formally as the "complementarity hypothesis" -- 

91
00:05:31,000 --> 00:05:34,920
has received relatively little research support.

92
00:05:34,920 --> 00:05:38,610
Even though it's a romantic idea and there are always exceptions,

93
00:05:38,610 --> 00:05:40,870
people don't tend to choose friends or partners 

94
00:05:40,870 --> 00:05:45,430
who have backgrounds, attitudes, or values that complement theirs.

95
00:05:45,430 --> 00:05:47,266
They're much more likely to choose friends

96
00:05:47,266 --> 00:05:50,930
and partners who are similar to themselves.

97
00:05:50,930 --> 00:05:54,580
Here, you can see your answer to a Snapshot Quiz item on this topic.

98
00:05:58,140 --> 00:05:59,879
Another interesting line of research has to do with 

99
00:05:59,879 --> 00:06:03,110
how much physical attractiveness matters.

100
00:06:03,110 --> 00:06:07,030
This is a factor that doesn't seem like it could be studied scientifically,

101
00:06:07,030 --> 00:06:10,000
but in fact, hundreds of carefully controlled studies 

102
00:06:10,000 --> 00:06:13,040
have been published on physical attraction.

103
00:06:13,040 --> 00:06:16,399
Here, for example, are a couple of questions that researchers have asked:

104
00:06:17,530 --> 00:06:19,885
After people go out on a date with someone, 

105
00:06:19,885 --> 00:06:22,400
how much does physical attractiveness determine whether 


106
00:06:22,400 --> 00:06:25,500
they'll want to continue dating the person?

107
00:06:25,500 --> 00:06:30,179
And does the answer differ for females and males? 


108
00:06:30,179 --> 00:06:33,800
The answer is that physical attractiveness matters quite a bit, 


109
00:06:33,800 --> 00:06:38,000
for females as well as males. Studies have found that 

110
00:06:38,000 --> 00:06:40,700
the physical attractiveness of a date correlates 

111
00:06:40,700 --> 00:06:43,228
more strongly with a desire to continue dating that

112
00:06:43,228 --> 00:06:47,490
person than almost any other factors studied,

113
00:06:47,490 --> 00:06:49,200
including judgments of character 

114
00:06:49,200 --> 00:06:52,840
and the perception of common interests.

115
00:06:52,840 --> 00:06:55,462
In general, cross-cultural research suggests that 

116
00:06:55,462 --> 00:07:00,140
men express more concern about physical attractiveness than do women,

117
00:07:00,140 --> 00:07:04,364
but when it comes to actual partners -- that is, ratings of 


118
00:07:04,364 --> 00:07:07,872
people rather than ratings of general attributes -- men and women

119
00:07:07,872 --> 00:07:12,500
value physical attractiveness to nearly the same degree.

120
00:07:13,660 --> 00:07:16,787
In the very first lecture of the course, I raised a question given in the

121
00:07:16,787 --> 00:07:20,000
Snapshot Quiz about whether physically attractive people

122
00:07:20,000 --> 00:07:24,300
are seen as less intelligent than other people. The answer, 

123
00:07:24,300 --> 00:07:28,028
as covered in this week's assigned reading, is just the opposite.

124
00:07:28,028 --> 00:07:31,000
They're seen as more intelligent. Let's pause for a minute

125
00:07:31,000 --> 00:07:34,230
so that you can see your answer in the Snapshot Quiz.

126
00:07:37,740 --> 00:07:40,270
As covered in this week's assigned reading, we learn

127
00:07:40,270 --> 00:07:44,240
from childhood on that beautiful people are good people --

128
00:07:44,240 --> 00:07:47,490
the princesses, the movie stars, and so on -- 

129
00:07:47,490 --> 00:07:50,920
and that leads to a kind of physical attractiveness stereotype.

130
00:07:51,760 --> 00:07:54,014
And as long as we're talking about the Snapshot Quiz, 

131
00:07:54,014 --> 00:07:57,550
let's examine one other item that has to do with attraction:

132
00:07:57,550 --> 00:08:00,310
The question of whether women are faster than men

133
00:08:00,310 --> 00:08:03,869
at falling in love and slower to end a relationship.

134
00:08:04,920 --> 00:08:08,000
The answer, according to the research record, is once again 


135
00:08:08,000 --> 00:08:12,200
just the opposite. It's men who tend to be faster at falling in love 


136
00:08:12,200 --> 00:08:15,000
and slower at falling out of love. 


137
00:08:15,000 --> 00:08:19,000
This is a finding that surprises many people, and again, illustrates 


138
00:08:19,000 --> 00:08:23,200
why research on relationships is so interesting and worth doing.

139
00:08:23,200 --> 00:08:27,480
Let's pause for a moment so that you can see your answer in the Snapshot Quiz.

140
00:08:31,090 --> 00:08:34,700
Now, so far we've been focused on the factors that bring people together, 

141
00:08:34,700 --> 00:08:37,300
but what about after two people are together?

142
00:08:37,300 --> 00:08:41,042
What are the key predictors of success in a relationship?

143
00:08:41,042 --> 00:08:46,000
And how accurate are people at predicting whether their relationships will last?

144
00:08:46,000 --> 00:08:48,400
For example, imagine that you're a college student,

145
00:08:48,400 --> 00:08:51,310
and you've been dating somebody for a few months.

146
00:08:51,310 --> 00:08:53,686
If you want to know whether you and your partner will still be

147
00:08:53,686 --> 00:08:58,610
together after a year, who would make the most accurate prediction:

148
00:08:58,610 --> 00:09:03,000
You, a roommate who knows the relationship, or a parent who 

149
00:09:03,000 --> 00:09:08,340
knows the relationship? Well, a study was conducted on this very question,

150
00:09:08,340 --> 00:09:11,000
and it turns out that if you ask students to predict

151
00:09:11,000 --> 00:09:15,210
whether their dating relationships will still be around after a year,

152
00:09:15,210 --> 00:09:19,790
they're less accurate than their dormitory roommates or their parents.

153
00:09:19,790 --> 00:09:21,200
Why?

154
00:09:21,200 --> 00:09:25,146
Because they're motivated to believe that the relationship will continue,

155
00:09:25,146 --> 00:09:28,478
so they neglect disconfirming evidence, whereas parents

156
00:09:28,478 --> 00:09:31,300
and roommates weigh the good and the bad.

157
00:09:31,805 --> 00:09:33,988
But let's return for a moment to the question of 

158
00:09:33,988 --> 00:09:37,799
what the key predictors are of relationship success.

159
00:09:37,799 --> 00:09:39,725
The answer for men, as we all know, is to 

160
00:09:40,130 --> 00:09:44,397
be able to balance a seated person using only your teeth.

161
00:09:44,397 --> 00:09:47,637
And the men who can do this while walking up and down stairs

162
00:09:48,057 --> 00:09:51,111
are especially successful in their relationships.

163
00:09:51,111 --> 00:09:54,311
This finding was first discovered by circus entertainers nearly

164
00:09:54,311 --> 00:09:59,500
a century ago and has since been replicated several times.

165
00:10:00,380 --> 00:10:02,887
Of course, for women, the story is very different.

166
00:10:02,887 --> 00:10:04,500
What matters most is to be able to

167
00:10:04,500 --> 00:10:07,900
grip a chair in your teeth while dancing.

168
00:10:07,900 --> 00:10:10,216
And of course, the women who can do this while lying down 

169
00:10:10,216 --> 00:10:15,190
or twirling are even more likely to have relationship success.

170
00:10:15,190 --> 00:10:17,800
Researchers have been trying to figure out why this is the case, 

171
00:10:17,800 --> 00:10:19,840
and even though no one's quite sure, 

172
00:10:19,840 --> 00:10:24,020
most experts agree that the research topic is highly entertaining.

173
00:10:26,070 --> 00:10:29,356
Okay, I'm joking around, of course, but there is a point, 

174
00:10:29,356 --> 00:10:31,720
which is that it's very hard to make generalizations 

175
00:10:31,720 --> 00:10:33,808
about the predictors of relationship success,

176
00:10:33,808 --> 00:10:39,094
or relationship satisfaction, because relationships vary so much

177
00:10:39,094 --> 00:10:42,924
over time, across cultures, even within cultures.

178
00:10:42,924 --> 00:10:46,470
So we have to be very careful not to overgeneralize.

179
00:10:47,260 --> 00:10:50,000
Having said that, there are some findings that do seem to have 

180
00:10:50,000 --> 00:10:54,023
some generality, one of which is that similarity is important 

181
00:10:54,023 --> 00:10:59,700
not only in attraction but in relationship satisfaction. For example, 

182
00:10:59,700 --> 00:11:03,090
when wives and husbands have similar personalities and interests,

183
00:11:03,090 --> 00:11:07,520
they tend to report greater marital satisfaction and happiness.

184
00:11:07,520 --> 00:11:11,744
And demographic similarities such as age, education, and religion, 

185
00:11:11,744 --> 00:11:15,300
are also significant predictors of satisfaction and stability 

186
00:11:15,300 --> 00:11:19,904
in friendships, marriages, and same-sex romantic relationships.

187
00:11:21,490 --> 00:11:23,170
Another factor that I'd like to discuss is

188
00:11:23,170 --> 00:11:26,050
one that's been studied mostly in the United States,

189
00:11:26,050 --> 00:11:27,900
so it may not apply cross-culturally

190
00:11:27,900 --> 00:11:32,630
but it would still be very interesting to discuss in the class forums.

191
00:11:32,630 --> 00:11:36,100
At least in the United States, marital satisfaction is much higher when 

192
00:11:36,100 --> 00:11:42,040
people believe that household work and childcare have been divided fairly.

193
00:11:42,040 --> 00:11:45,000
In fact, the Pew Research Center published a national study 

194
00:11:45,000 --> 00:11:49,780
in the U.S. on this topic, subtitled, "I Like Hugs. I Like Kisses. 

195
00:11:49,780 --> 00:11:53,480
But What I Really Love is Help with the Dishes."

196
00:11:53,480 --> 00:11:57,000
What the study found was that 62 percent of those surveyed

197
00:11:57,000 --> 00:12:02,660
saw shared household chores as very important for a successful marriage -- 

198
00:12:02,660 --> 00:12:06,016
almost as many as the number who saw sex as very important, 

199
00:12:06,016 --> 00:12:11,370
and ahead of adequate income, good housing and shared religious beliefs.

200
00:12:12,840 --> 00:12:16,000
One last topic that I'd like to discuss is whether couples who rarely fight 

201
00:12:16,000 --> 00:12:20,250
with each other are more likely than other couples to stay together.

202
00:12:20,250 --> 00:12:23,100
And the answer here might surprise you.

203
00:12:23,100 --> 00:12:25,250
Although the research record is somewhat mixed, 

204
00:12:25,250 --> 00:12:28,740
it appears that the amount of conflict in a relationship -- for example, 

205
00:12:28,740 --> 00:12:33,229
the number of arguments -- does not do a very good job of predicting 


206
00:12:33,229 --> 00:12:37,467
the level of marital happiness or whether a couple will stay together.

207
00:12:37,467 --> 00:12:42,150
What does seem to matter is the overall ratio of positive 

208
00:12:42,150 --> 00:12:44,696
to negative interactions -- on the positive side,

209
00:12:44,696 --> 00:12:50,353
things like touching, smiling, complimenting, laughing together,

210
00:12:50,353 --> 00:12:55,500
and on the negative side, arguing, insulting, blaming, and so on.

211
00:12:55,500 --> 00:12:58,200
In fact, John Gottman, who's one of the leading researchers 

212
00:12:58,200 --> 00:13:01,500
in this area, has been able to predict marital success 

213
00:13:01,500 --> 00:13:05,020
surprisingly well based on whether a couple's ratio of

214
00:13:05,020 --> 00:13:10,360
positive to negative interactions is at least five-to-one.

215
00:13:10,360 --> 00:13:12,010
That is, he finds that if a couple has

216
00:13:12,010 --> 00:13:15,815
at least five positive interactions for every negative one,

217
00:13:15,815 --> 00:13:19,180
regardless of how many negative interactions take place, 

218
00:13:19,180 --> 00:13:22,240
the couple will probably stay together.

219
00:13:22,240 --> 00:13:24,000
On the other hand, marriages that fall below 

220
00:13:24,000 --> 00:13:29,100
that magic figure of five-to-one are at high risk for divorce.

221
00:13:29,100 --> 00:13:33,014
Dr. Gottman was kind enough to let me share a one-minute clip 

222
00:13:33,014 --> 00:13:35,825
in which he discusses the ratio, so let's watch.

223
00:13:36,825 --> 00:13:39,594
>> Let me start by talking about what it is we learned

224
00:13:39,594 --> 00:13:45,090
that allows us to predict divorce or stability with very high accuracy.

225
00:13:45,090 --> 00:13:49,200
The first thing we found was that if you take a look at 

226
00:13:49,200 --> 00:13:53,340
the ratio of positive stuff during conflict --

227
00:13:53,340 --> 00:13:57,100
things like interest, asking questions, being nice to one another, 

228
00:13:57,100 --> 00:14:00,283
being kind, being affectionate, being empathetic --

229
00:14:00,283 --> 00:14:04,288
and you look at all the negative stuff like criticism, hostility,

230
00:14:04,288 --> 00:14:10,486
anger, hurt feelings -- and you take the ratio of positive to negative,

231
00:14:10,486 --> 00:14:16,530
in relationships that stay together, that ratio turns out to be five-to-one.

232
00:14:16,530 --> 00:14:20,000
There's five times as many positive things going on in relationships 

233
00:14:20,000 --> 00:14:26,000
that work as negative. That's an interesting equation, and it sort of suggests that 

234
00:14:26,000 --> 00:14:30,000
if you do something negative to hurt your partner's feelings,

235
00:14:30,000 --> 00:14:35,030
you know, that you have to make up for it with five positive things.

236
00:14:35,030 --> 00:14:38,610
So, the equation is not balanced in terms of positive and negative.

237
00:14:38,610 --> 00:14:42,300
Negative has a lot more ability to inflict pain and damage 

238
00:14:42,300 --> 00:14:46,430
than positive things have to heal and bring you closer.

239
00:14:47,630 --> 00:14:50,200
>> Of all the relationship research and advice that I've read 

240
00:14:50,200 --> 00:14:54,900
over the years, this particular finding, I think, is one of the most useful.

241
00:14:54,900 --> 00:14:57,900
I think about that five-to-one ratio not only in my marriage, 

242
00:14:57,900 --> 00:15:01,000
but in my relationship with my daughter, my parents, 

243
00:15:01,000 --> 00:15:03,780
friends, colleagues, students, and so forth.

244
00:15:03,780 --> 00:15:06,432
That is, when there's a problem, when there's a conflict, 

245
00:15:06,432 --> 00:15:10,000
I try to make sure that there are at least five positive interactions 

246
00:15:10,000 --> 00:15:13,930
to keep the relationship strong -- to keep it healthy.

247
00:15:13,930 --> 00:15:15,854
And I hope that the information in this video

248
00:15:15,854 --> 00:15:20,210
and in the assigned reading also benefits your relationships

249
00:15:20,210 --> 00:15:22,370
and keeps the romance and the fireworks going.

250
00:15:22,370 --> 00:15:25,238
In fact, what better way to end this video

251
00:15:25,238 --> 00:15:27,260
than with some fireworks. Enjoy!