﻿1
00:00:25,500 --> 00:00:28,800
According to the PsycInfo database, thousands of studies have been

2
00:00:28,800 --> 00:00:32,740
published on the topic of happiness since the 1800s.

3
00:00:32,740 --> 00:00:36,000
But of these articles, over 60% have appeared in just the last

4
00:00:36,000 --> 00:00:41,300
ten years, and many of these new findings tend to be very surprising.

5
00:00:41,300 --> 00:00:44,600
I included an item on happiness in the Snapshot Quiz

6
00:00:44,600 --> 00:00:47,300
when I asked you to rank four different activities in terms of

7
00:00:47,300 --> 00:00:50,630
the amount of happiness that people report experiencing

8
00:00:50,630 --> 00:00:52,370
when they do each one.

9
00:00:52,370 --> 00:00:57,590
They were watching TV, shopping, preparing food, and exercising.

10
00:00:57,590 --> 00:00:59,690
If you completed the Snapshot Quiz, you should be

11
00:00:59,690 --> 00:01:02,300
able to see your answer in the next screen.

12
00:01:05,220 --> 00:01:08,000
What researchers have found, which surprises many people,

13
00:01:08,000 --> 00:01:11,000
is that the ranking runs in the exact opposite order

14
00:01:11,000 --> 00:01:14,460
that the items are listed in the Snapshot Quiz.

15
00:01:14,460 --> 00:01:18,130
In other words, people report experiencing the most happiness

16
00:01:18,130 --> 00:01:22,300
when they're exercising and the least happiness when they're watching TV,

17
00:01:22,300 --> 00:01:24,710
which is ironic because most people consider

18
00:01:24,710 --> 00:01:28,000
watching TV to be a recreational activity.

19
00:01:28,410 --> 00:01:30,800
These results were reported in the journal Science

20
00:01:30,800 --> 00:01:34,000
in 2010 by two Harvard researchers,

21
00:01:34,000 --> 00:01:36,210
and the results were based on a sample

22
00:01:36,210 --> 00:01:40,480
of roughly 5,000 people drawn from 83 countries,

23
00:01:40,480 --> 00:01:45,200
making it one of the largest and most international studies of its kind.

24
00:01:46,240 --> 00:01:48,690
Here's another example of research that at least

25
00:01:48,690 --> 00:01:51,710
I found surprising when I first read about it.

26
00:01:51,710 --> 00:01:55,390
It turns out that even though people oftentimes chase youth and

27
00:01:55,390 --> 00:01:59,530
beauty in an effort to achieve happiness, physically beautiful people

28
00:01:59,530 --> 00:02:04,540
do not report any greater life satisfaction than do other people.

29
00:02:04,540 --> 00:02:09,800
And happiness usually increases as people age, at least through their 70s.

30
00:02:10,400 --> 00:02:15,240
And here's one last example of research that again, I find surprising.

31
00:02:15,240 --> 00:02:17,990
If you compare different countries around the world,

32
00:02:17,990 --> 00:02:21,040
it turns out that people who report being the happiest

33
00:02:21,040 --> 00:02:24,230
are not those living in a tropical paradise

34
00:02:24,230 --> 00:02:27,700
with the white sand beach and palm trees swaying gently

35
00:02:27,700 --> 00:02:32,740
in the breeze, but rather, people living in Iceland, Scandinavia,

36
00:02:32,740 --> 00:02:35,550
and their neighbors, where for much of the year it's

37
00:02:35,550 --> 00:02:40,800
relatively cold, dark, and people have to cooperate to survive.

38
00:02:41,440 --> 00:02:45,000
As Eric Weiner put it in his book, "The Geography of Bliss,"

39
00:02:45,000 --> 00:02:48,560
necessity may be the mother of invention,

40
00:02:48,560 --> 00:02:52,200
but interdependence is the mother of affection.

41
00:02:53,230 --> 00:02:55,830
Again and again what research on happiness shows

42
00:02:55,830 --> 00:02:59,500
is the central importance of our social bonds,

43
00:02:59,500 --> 00:03:04,860
a point that was superbly illustrated in a 2011 TED talk by Michael Norton,

44
00:03:04,860 --> 00:03:07,660
a Harvard professor whose research has explored

45
00:03:07,660 --> 00:03:12,000
the psychological difference between spending money on oneself

46
00:03:12,000 --> 00:03:14,200
and spending money on others.

47
00:03:14,200 --> 00:03:18,990
He's also co-author of a great book that was just published a few months ago,

48
00:03:18,990 --> 00:03:22,000
"Happy Money: The Science of Smarter Spending."

49
00:03:23,000 --> 00:03:26,200
I'm very pleased to say that Professor Norton and

50
00:03:26,200 --> 00:03:29,880
the good folks at TED have made this talk available to our class,

51
00:03:29,880 --> 00:03:33,410
so for the remainder of this video, you are in for a treat.

52
00:03:33,410 --> 00:03:36,118
The title of the talk is "How to Buy Happiness."

53
00:03:38,400 --> 00:03:40,090
>> So I want to talk today about money

54
00:03:40,090 --> 00:03:41,950
and happiness, which are two things that a lot of us

55
00:03:41,950 --> 00:03:45,000
spend a lot of our time thinking about, either trying to earn them

56
00:03:45,000 --> 00:03:49,970
or trying to increase them, and a lot of us resonate with this phrase.

57
00:03:49,970 --> 00:03:53,940
So we see it in religions and self-help books, that money can't buy happiness.

58
00:03:53,940 --> 00:03:55,860
And I want to suggest today that, in fact, that's wrong,

59
00:03:56,550 --> 00:04:00,640
and that -- I'm at a business school, so that's what we do.

60
00:04:00,700 --> 00:04:02,720
So that -- that's wrong, and in fact, if you think that,

61
00:04:02,720 --> 00:04:04,980
you're actually just not spending it right,

62
00:04:04,980 --> 00:04:08,200
so that instead of spending it the way you usually spend it,

63
00:04:08,200 --> 00:04:11,500
maybe if you spent it differently, that might work a little bit better.

64
00:04:11,500 --> 00:04:15,140
And before I tell you the ways that you can spend it that will make you happier,

65
00:04:15,140 --> 00:04:17,700
let's think about the ways we usually spend it that don't, in fact,

66
00:04:17,700 --> 00:04:20,312
make us happier. We had a little natural experiment.

67
00:04:20,312 --> 00:04:23,300
So CNN a little while ago wrote this interesting article

68
00:04:23,300 --> 00:04:26,050
on what happens to people when they win the lottery.

69
00:04:26,050 --> 00:04:27,800
It turns out people think when they win the lottery,

70
00:04:27,800 --> 00:04:29,270
their lives are going to be amazing.

71
00:04:29,270 --> 00:04:31,980
This article is about how their lives get ruined.

72
00:04:31,980 --> 00:04:33,780
So what happens when people win the lottery is, number one,

73
00:04:33,780 --> 00:04:36,250
they spend all the money and go into debt.

74
00:04:36,250 --> 00:04:40,170
And number two, all of their friends and everyone they've ever met find them

75
00:04:40,170 --> 00:04:43,750
and bug them for money, and it ruins their social relationships, in fact.

76
00:04:43,750 --> 00:04:45,910
So they have more debt and worse friendships

77
00:04:45,910 --> 00:04:47,940
than they had before they won the lottery.

78
00:04:47,940 --> 00:04:50,000
What was interesting about the article was people started

79
00:04:50,000 --> 00:04:53,590
commenting on the article, readers of the thing, and instead of

80
00:04:53,590 --> 00:04:55,575
talking about how it had made them realize that money

81
00:04:55,575 --> 00:04:58,160
doesn't lead to happiness, everyone instantly started saying,

82
00:04:58,160 --> 00:04:59,855
"You know what I would do if I won the lottery?"

83
00:04:59,880 --> 00:05:01,850
and fantasizing about what they'd do.

84
00:05:01,850 --> 00:05:03,130
And here is a, just two of the ones that,

85
00:05:03,130 --> 00:05:05,830
that we saw, that are just really interesting to think about.

86
00:05:05,830 --> 00:05:08,000
One person wrote in, "When I win, I am going to buy

87
00:05:08,000 --> 00:05:11,500
my own little mountain and have a little house on top."

88
00:05:12,360 --> 00:05:15,200
And another person wrote, "I would fill a big bathtub with money and

89
00:05:15,200 --> 00:05:19,660
get in the tub while smoking a big fat cigar and sipping a glass of champagne."

90
00:05:19,660 --> 00:05:20,510
This is even worse, now...

91
00:05:20,510 --> 00:05:23,440
"Then I'd have a picture taken and dozens of glossies made.

92
00:05:23,440 --> 00:05:25,230
Anyone begging for money or trying to extort from me

93
00:05:25,230 --> 00:05:29,000
would receive a copy of the picture and nothing else."

94
00:05:31,000 --> 00:05:33,580
And so many of the comments were exactly of this type,

95
00:05:33,580 --> 00:05:37,260
where people got money, and in fact it made them antisocial.

96
00:05:37,260 --> 00:05:40,460
So I told you that it ruins people's lives, and that their friends bug them.

97
00:05:40,460 --> 00:05:42,630
It also, money often makes us feel very selfish and

98
00:05:42,630 --> 00:05:44,930
we do things only for our self. And we said,

99
00:05:44,930 --> 00:05:46,730
well, maybe the reason that money doesn't make us happy

100
00:05:46,730 --> 00:05:49,050
is that we're always spending it on the wrong things,

101
00:05:49,050 --> 00:05:51,810
and in particular, that we're always spending it on ourselves.

102
00:05:51,810 --> 00:05:53,250
And we thought, I wonder what would happen if we

103
00:05:53,250 --> 00:05:55,850
made people spend more of their money on other people.

104
00:05:55,850 --> 00:05:58,530
So, instead of being antisocial with your money, what if you were

105
00:05:58,530 --> 00:06:00,260
a little bit more prosocial with your money?

106
00:06:00,260 --> 00:06:03,210
And we thought, let's make people do it and see what happens.

107
00:06:03,210 --> 00:06:05,500
So let's have some people do what they usually do and spend money

108
00:06:05,500 --> 00:06:08,300
on themselves, and let's make some people give money away

109
00:06:08,300 --> 00:06:11,520
and measure their happiness and see if in fact, they get happier.

110
00:06:11,520 --> 00:06:14,000
So the first way that we did this on  one Vancouver morning,

111
00:06:14,000 --> 00:06:17,000
we went out on the campus at University of British Columbia,

112
00:06:17,000 --> 00:06:19,570
and we approached people and said, "Do you want to be in an experiment?"

113
00:06:19,570 --> 00:06:22,720
If they said yes, we gave, we asked them how happy they were,

114
00:06:22,720 --> 00:06:25,510
and then we gave them an envelope, and one of the envelopes had

115
00:06:25,510 --> 00:06:30,320
things in it that said by 5:00 pm today, spend this money on yourself.

116
00:06:30,320 --> 00:06:32,650
So, we gave some examples of what you could spend it on.

117
00:06:32,650 --> 00:06:34,560
Other people in the morning got a slip of paper that said

118
00:06:34,560 --> 00:06:38,130
by 5:00 pm today, spend this money on somebody else.

119
00:06:38,130 --> 00:06:41,000
Also inside the envelope was money, and we manipulated

120
00:06:41,000 --> 00:06:43,900
how much money we gave them. So some people got this slip of paper

121
00:06:43,900 --> 00:06:47,880
and $5.00. Some people got this slip of paper, and $20.00.

122
00:06:47,880 --> 00:06:50,860
We let them go about their day; they did whatever they wanted to do.

123
00:06:50,860 --> 00:06:53,910
We found out that they did, in fact, spend it the way that we asked them to.

124
00:06:53,910 --> 00:06:55,510
We called them up at night, and asked them,

125
00:06:55,510 --> 00:06:58,100
"What did you spend it on and how happy do you feel now?"

126
00:06:58,100 --> 00:06:59,500
What did they spend it on? Well, these are college undergrads,

127
00:06:59,500 --> 00:07:02,590
so a lot of what they spent it on for themselves was things like earrings

128
00:07:02,590 --> 00:07:06,474
and makeup. One woman said she bought a stuffed animal for her niece.

129
00:07:06,474 --> 00:07:08,260
People gave money to homeless people.

130
00:07:08,260 --> 00:07:11,320
Huge effect here of Starbucks.

131
00:07:11,320 --> 00:07:16,480
So, if you give undergraduates $5.00, it looks like coffee to them,

132
00:07:16,480 --> 00:07:18,900
and they run over to Starbucks and spend it as fast as they can.

133
00:07:19,650 --> 00:07:22,210
But some people bought a coffee for themselves, the way they usually would,

134
00:07:22,210 --> 00:07:25,090
but other people said that they bought a coffee for somebody else --

135
00:07:25,090 --> 00:07:27,400
so the very same purchase, just targeted

136
00:07:27,400 --> 00:07:30,250
toward yourself or targeted toward somebody else.

137
00:07:30,250 --> 00:07:32,020
What did we find when we called them back at the end of the day?

138
00:07:32,020 --> 00:07:34,000
People who spent money on other people got happier.

139
00:07:34,000 --> 00:07:36,500
People who spent money on themselves nothing happened.

140
00:07:36,500 --> 00:07:39,610
It didn't make them less happy -- it just didn't do much for them.

141
00:07:39,610 --> 00:07:41,000
And the other thing we saw is that the amount of money

142
00:07:41,000 --> 00:07:44,300
doesn't matter that much. So, people thought that $20.00 would be

143
00:07:44,300 --> 00:07:48,460
way better than $5.00. In fact, it doesn't matter how much money you spent.

144
00:07:48,460 --> 00:07:52,670
What really matters is that you spent it on somebody else rather than on yourself.

145
00:07:52,670 --> 00:07:54,290
We see this again and again when we give people money

146
00:07:54,290 --> 00:07:57,140
to spend on other people instead of on themselves.

147
00:07:57,140 --> 00:07:59,000
Of course, these are undergraduates in Canada --

148
00:07:59,000 --> 00:08:01,820
not the world's most representative population.

149
00:08:01,820 --> 00:08:04,930
They're also fairly wealthy and affluent and all these other sorts of things.

150
00:08:04,930 --> 00:08:07,000
We wanted to see if this holds true everywhere in the world,

151
00:08:07,000 --> 00:08:09,000
or just among wealthy countries, so we went, in fact,

152
00:08:09,000 --> 00:08:12,150
to Uganda and ran a very similar experiment.

153
00:08:12,150 --> 00:08:14,720
So imagine, instead of just people in Canada, we say,

154
00:08:14,720 --> 00:08:16,900
"Name the last time you spent money on yourself or other people.

155
00:08:16,900 --> 00:08:19,000
Describe it. How happy did it make you?"

156
00:08:19,000 --> 00:08:21,400
Or in Uganda, "Name the last time you spent money on yourself

157
00:08:21,400 --> 00:08:23,830
or other people, and describe that."

158
00:08:23,830 --> 00:08:26,040
And then we ask them how happy they are, again.

159
00:08:26,040 --> 00:08:28,550
And what we see is sort of amazing because there's human universals

160
00:08:28,550 --> 00:08:30,100
on what you do with your money,

161
00:08:30,100 --> 00:08:33,090
and then real cultural differences on what you do as well.

162
00:08:33,090 --> 00:08:36,810
So, for example, one guy from Uganda says this.

163
00:08:36,810 --> 00:08:40,160
He says, "I called a girl I wished to love. We basically went out on a date."

164
00:08:40,160 --> 00:08:43,800
And he says at the end that he didn't "achieve her" up 'til now.

165
00:08:43,800 --> 00:08:48,848
Here's a guy from Canada -- very similar thing.

166
00:08:48,848 --> 00:08:51,300
"I took my girlfriend out for dinner. We went to a movie.

167
00:08:51,300 --> 00:08:55,000
We left early, and then went back to her room for...

168
00:08:55,000 --> 00:08:57,792
only cake." Just -- just cake.

169
00:08:57,810 --> 00:09:00,310
Human universal. So you spend money on other people; you're being

170
00:09:00,310 --> 00:09:02,690
nice to them -- maybe you have something in mind, maybe not.

171
00:09:02,690 --> 00:09:04,680
But then we see extraordinary differences.

172
00:09:04,680 --> 00:09:06,200
So, look at these two.

173
00:09:06,200 --> 00:09:09,000
This is a woman from Canada. We say, âName a time you spent money

174
00:09:09,000 --> 00:09:12,160
on somebody else.â She says, you know, "I bought a present for my mom.

175
00:09:12,160 --> 00:09:15,000
I drove to the mall in my car, bought a present, gave it to my mom."

176
00:09:15,000 --> 00:09:16,390
Perfectly nice thing to do.

177
00:09:16,390 --> 00:09:18,430
It's good to get gifts for people that you know.

178
00:09:18,430 --> 00:09:21,200
Compare that to this woman from Uganda:

179
00:09:21,200 --> 00:09:22,870
"I was walking and met a long-time friend

180
00:09:22,870 --> 00:09:25,490
whose son was sick with malaria. They had no money.

181
00:09:25,490 --> 00:09:28,000
They went to a clinic, and I gave her this money."

182
00:09:28,000 --> 00:09:29,160
This isn't $10,000.

183
00:09:29,160 --> 00:09:31,950
It's the local currency, so it's a very small amount of money,

184
00:09:31,950 --> 00:09:34,730
in fact, but enormously different motivations here.

185
00:09:34,730 --> 00:09:38,450
This is a real medical need, literally a life-saving donation.

186
00:09:38,450 --> 00:09:41,110
Above, it's just kind of I got, bought a gift for my mother.

187
00:09:41,110 --> 00:09:44,260
What we see again, though, is that the specific way that you spend

188
00:09:44,260 --> 00:09:47,950
on other people isn't nearly as important as the fact that you spend on

189
00:09:47,950 --> 00:09:51,470
other people in order to make yourself happy, which is really quite important.

190
00:09:51,470 --> 00:09:55,480
So, you don't have to do amazing things with your money to make yourself happy.

191
00:09:55,480 --> 00:09:59,170
You can do small trivial things and yet still get these benefits from doing this.

192
00:09:59,170 --> 00:10:00,140
These are only two countries.

193
00:10:00,140 --> 00:10:02,340
We also wanted to go even broader and look at every country in the world

194
00:10:02,340 --> 00:10:06,670
if we could, to see what the relationship is between money and happiness.

195
00:10:06,670 --> 00:10:09,040
We got data from the Gallup Organization, which you know

196
00:10:09,040 --> 00:10:11,400
from all the political polls that have been happening lately.

197
00:10:11,400 --> 00:10:13,998
They asked people, "Did you donate money to charity recently?"

198
00:10:13,998 --> 00:10:16,680
And they asked them, "How happy are you with your life in general?"

199
00:10:16,680 --> 00:10:19,110
And we can see what the relationship is between those two things.

200
00:10:19,110 --> 00:10:21,300
Are they positively correlated -- giving money makes you happy --

201
00:10:21,300 --> 00:10:23,550
or are they negatively correlated?

202
00:10:23,550 --> 00:10:25,900
On this map, green will mean they're positively correlated,

203
00:10:25,900 --> 00:10:28,430
and red means they're negatively correlated,

204
00:10:28,430 --> 00:10:31,090
and you can see, the world is crazily green.

205
00:10:31,090 --> 00:10:34,400
So in every, almost every country in the world where we have this data,

206
00:10:34,400 --> 00:10:36,890
people who give money to charity are happier people

207
00:10:36,890 --> 00:10:38,960
than people who don't give money to charity.

208
00:10:38,960 --> 00:10:41,580
I know you're all looking at that red country in the middle.

209
00:10:41,580 --> 00:10:42,860
I would be a jerk and not tell you what it is, but it's,

210
00:10:42,860 --> 00:10:46,000
in fact, it's Central African Republic. You can make up stories

211
00:10:46,000 --> 00:10:48,210
how maybe it's different there for some reason or another.

212
00:10:48,210 --> 00:10:51,730
Just below that to the right is Rwanda, though, which is amazingly green.

213
00:10:51,730 --> 00:10:53,840
So almost everywhere we look, we see that giving

214
00:10:53,840 --> 00:10:57,640
money away makes you happier than keeping it for yourself.

215
00:10:57,640 --> 00:10:59,500
What about your work life, which is where we spend all the

216
00:10:59,500 --> 00:11:02,210
rest of our time when we're not with the people we know?

217
00:11:02,210 --> 00:11:04,970
We decided to infiltrate some companies and do a very similar thing.

218
00:11:04,970 --> 00:11:08,650
So, these are sales teams in Belgium. They work in teams, they go

219
00:11:08,650 --> 00:11:11,720
out and sell basically to doctors and try to get them to buy drugs.

220
00:11:11,720 --> 00:11:14,000
So, we can look to see how well they sell things

221
00:11:14,000 --> 00:11:16,720
as a function of being a member of a team.

222
00:11:16,720 --> 00:11:18,630
Some teams, we give people on the team some money

223
00:11:18,630 --> 00:11:20,970
for themselves and say, "Spend it however you want on yourself,â

224
00:11:20,970 --> 00:11:23,000
just like we did with the undergrads in Canada.

225
00:11:23,000 --> 00:11:25,510
But other teams, we say, "Here's 15 Euro.

226
00:11:25,510 --> 00:11:27,110
Spend it on one of your teammates this week.

227
00:11:27,110 --> 00:11:29,980
Buy them something as a gift or a present and give it to them."

228
00:11:29,980 --> 00:11:32,700
And then we can see, now we've got teams that spend on themselves

229
00:11:32,700 --> 00:11:35,700
and we've got these prosocial teams, so we give money to make the team

230
00:11:35,700 --> 00:11:38,660
a little better. The reason I have a ridiculous piÃ±ata there

231
00:11:38,660 --> 00:11:40,780
is one of the teams pooled their money and bought a piÃ±ata,

232
00:11:40,780 --> 00:11:41,830
and they all got around and smashed the piÃ±ata,

233
00:11:41,830 --> 00:11:43,980
and all the candy fell out and things like that --

234
00:11:43,980 --> 00:11:46,300
a very silly trivial thing to do, but think of the difference

235
00:11:46,300 --> 00:11:50,930
on a team that didn't do that all, that got 15 Euro, put it in their pocket,

236
00:11:50,930 --> 00:11:54,210
maybe bought themselves a coffee, or teams that had this prosocial experience

237
00:11:54,210 --> 00:11:57,600
where they all bonded together to buy something and do a group activity.

238
00:11:57,600 --> 00:11:59,730
What we see is that, in fact, the teams that are prosocial

239
00:11:59,730 --> 00:12:03,330
sell more stuff than the teams that only got money for themselves.

240
00:12:03,330 --> 00:12:05,036
And one way to think about it is, for every 15 Euro

241
00:12:05,036 --> 00:12:07,930
you give people for themselves, they put it in their pocket --

242
00:12:07,930 --> 00:12:10,010
they don't do anything different than they did before.

243
00:12:10,010 --> 00:12:12,100
You don't get any money from that. You actually lose money

244
00:12:12,100 --> 00:12:14,600
because it doesn't motivate them to perform any better.

245
00:12:14,600 --> 00:12:17,000
But when you give them 15 Euro to spend on their teammates,

246
00:12:17,000 --> 00:12:19,000
they do so much better on their teams that you actually get

247
00:12:19,000 --> 00:12:21,840
a huge win on investing this kind of money.

248
00:12:21,840 --> 00:12:24,090
And I realize that you're probably thinking to yourselves, "This is all fine,

249
00:12:24,090 --> 00:12:27,930
but there's a context that's incredibly important for public policy,

250
00:12:27,930 --> 00:12:30,320
and I can't imagine it would work there, and if, basically, if he

251
00:12:30,320 --> 00:12:34,060
doesn't show me that it works here, I don't believe anything he said.

252
00:12:34,060 --> 00:12:38,642
And I know that what you're all thinking about are dodgeball teams.

253
00:12:38,650 --> 00:12:41,220
This was a huge criticism that we got, you know, to say if you

254
00:12:41,220 --> 00:12:44,900
can't show it with dodgeball teams, this is all stupid, so we went out and

255
00:12:44,900 --> 00:12:47,960
found these dodgeball teams and infiltrated them,

256
00:12:47,960 --> 00:12:49,900
and we did the exact same thing as before. So some teams,

257
00:12:49,900 --> 00:12:52,848
we give people in the team money; they spend it on themselves.

258
00:12:52,848 --> 00:12:56,550
Other teams, we give them money to spend on their dodgeball teammates.

259
00:12:56,550 --> 00:12:58,210
The teams that spent money on themselves,

260
00:12:58,210 --> 00:13:00,710
they're just the same winning percentage as they were before.

261
00:13:00,710 --> 00:13:02,000
The teams that we give them money to spend on each other,

262
00:13:02,000 --> 00:13:05,500
they become different teams, and in fact, they dominate the league

263
00:13:05,500 --> 00:13:08,380
by the time they're done. Across all of these different contexts,

264
00:13:08,380 --> 00:13:12,000
your personal life, your work life, even silly things like intramural sports,

265
00:13:12,000 --> 00:13:14,500
we see spending on other people has a bigger return for you

266
00:13:14,500 --> 00:13:17,488
than spending on yourself.

267
00:13:17,488 --> 00:13:20,000
And so I'll just say, I think if you think money can't buy happiness,

268
00:13:20,000 --> 00:13:22,110
you're not spending it right. The implication is not you know,

269
00:13:22,110 --> 00:13:24,890
you should buy this product instead of that product,

270
00:13:24,890 --> 00:13:27,025
and that's the way to make yourself happier.

271
00:13:27,025 --> 00:13:29,130
It's in fact that you should stop thinking about which product

272
00:13:29,130 --> 00:13:33,000
to buy for yourself and try giving some of it to other people instead.

273
00:13:33,000 --> 00:13:36,072
And we luckily have an opportunity for you.

274
00:13:36,072 --> 00:13:38,800
DonorsChoose.org is a nonprofit for mainly

275
00:13:38,800 --> 00:13:41,640
public school teachers in low-income schools.

276
00:13:41,640 --> 00:13:44,270
They post projects, so they say, "I want to teach Huckleberry Finn

277
00:13:44,270 --> 00:13:45,950
to my class, and we don't have the books."

278
00:13:45,950 --> 00:13:48,000
Or "I want a microscope to show my, teach my students science,

279
00:13:48,000 --> 00:13:49,870
and we don't have a microscope."

280
00:13:49,870 --> 00:13:52,280
You and I can go on and buy it for them.

281
00:13:52,280 --> 00:13:53,000
The teacher writes you a thank-you note.

282
00:13:53,000 --> 00:13:54,500
The kids write you a thank-you note.

283
00:13:54,500 --> 00:13:56,990
Sometimes they send you pictures of them using the microscope.

284
00:13:56,990 --> 00:13:58,530
It's an extraordinary thing.

285
00:13:58,530 --> 00:14:00,740
Go to the website and start yourself on the process of thinking,

286
00:14:00,740 --> 00:14:04,000
again, less about how can I spend money on myself,

287
00:14:04,000 --> 00:14:06,512
and more about, "If I've got $5.00 or $15.00,

288
00:14:06,512 --> 00:14:09,050
what can I do to benefit other people?"

289
00:14:09,050 --> 00:14:10,300
Because ultimately when you do that,

290
00:14:10,300 --> 00:14:12,180
you'll find out you'll benefit yourself much more.

291
00:14:12,180 --> 00:14:13,420
Thank you.

