




1
00:00:06,740 --> 00:00:10,700
Over the past week or two, the lectures 
and readings have focused on the interplay 

2
00:00:10,700 --> 00:00:16,000
between groups and individuals -- how 
groups influence individuals and vice versa --

3
00:00:16,000 --> 00:00:19,970
as well as intergroup dynamics. 
So, for example, we've covered obedience 

4
00:00:19,970 --> 00:00:23,160
and conformity, 
social loafing and social facilitation, 

5
00:00:23,160 --> 00:00:27,990
group polarization and groupthink, 
deindividuation and prejudice. 

6
00:00:27,990 --> 00:00:30,300
All of these things involve that 
interplay. 

7
00:00:30,300 --> 00:00:34,890
But one important area that we haven't 
examined yet is bystander intervention 

8
00:00:34,890 --> 00:00:38,695
in emergency situations -- for instance, 
the question of whether 

9
00:00:38,695 --> 00:00:42,700
bystanders are more likely or less likely 
to intervene when 

10
00:00:42,700 --> 00:00:46,800
other bystanders  are present (an 
important form of social influence). 

11
00:00:46,800 --> 00:00:51,430
This is a research question whose origin 
dates back to a tragedy that took place 

12
00:00:51,430 --> 00:00:57,250
on March 13, 1964. 
At 3:20 in the morning that day, near the 

13
00:00:57,250 --> 00:01:02,700
Kew Gardens railroad station in New York, 
Kitty Genovese, a 28-year-old bar manager, 

14
00:01:02,700 --> 00:01:06,720
returned home from work. 
After parking her car, she started to 

15
00:01:06,720 --> 00:01:10,160
walk toward her apartment, which was 
about a hundred feet away, 

16
00:01:10,160 --> 00:01:14,390
and then suddenly, she noticed a man on 
the other side of the parking lot. 

17
00:01:14,390 --> 00:01:19,720
She started to run, but the man, 29-year-old 
Winston Moseley, caught up to her,

18
00:01:19,720 --> 00:01:24,350
and under a streetlight, stabbed her 
multiple times in the back. 

19
00:01:24,350 --> 00:01:27,410
She screamed, lights went on in the 
ten-story apartment building 

20
00:01:27,410 --> 00:01:31,560
across the street, 
windows slid open, and Kitty Genovese 

21
00:01:31,560 --> 00:01:33,670
cried out, "Oh, my God! 
He stabbed me!

22
00:01:33,670 --> 00:01:37,940
Please help me! 
Please help me!" A man in one of the 

23
00:01:37,940 --> 00:01:42,800
apartments called down, "Let that girl 
alone!" And Winston ran off, 

24
00:01:42,800 --> 00:01:47,180
leaving her there, bleeding. 
The apartment lights went out. 

25
00:01:47,180 --> 00:01:51,740
Kitty eventually got back on her feet, and 
she walked slowly and unsteadily toward 

26
00:01:51,740 --> 00:01:55,140
the entrance of her apartment, which was in 
the back of the building, 

27
00:01:55,140 --> 00:01:59,650
but eventually she fell over near the 
foot of some stairs in a lobby area. 

28
00:01:59,650 --> 00:02:03,020
Then about 10 minutes later, Winston 
returned, 

29
00:02:03,020 --> 00:02:07,290
sexually assaulted Kitty, and when she 
tried to resist, he kept her quiet by 

30
00:02:07,290 --> 00:02:13,030
stabbing her in the throat, fatally. 
Now let me add an epilogue. 

31
00:02:13,030 --> 00:02:16,590
When the murder first occurred, its 
coverage in the New York Times was only 

32
00:02:16,590 --> 00:02:21,950
five sentences buried on page 26. 
Two weeks after the murder, though, 

33
00:02:21,950 --> 00:02:26,000
the story appeared in a large front page 
article with a big diagram and 

34
00:02:26,000 --> 00:02:30,600
a huge story continued on a second page. 
Why? 

35
00:02:30,990 --> 00:02:34,300
Because according to the article, a 
police investigation concluded that 

36
00:02:34,300 --> 00:02:40,210
dozens of "respectable" law-abiding 
citizens had witnessed the killing, 

37
00:02:40,210 --> 00:02:44,600
but not one person telephoned the police 
during the assault. 

38
00:02:44,600 --> 00:02:49,800
Later investigations, including a 2007 
American Psychologist article, 

39
00:02:49,800 --> 00:02:54,070
have challenged the accuracy of this account.
But at the time, the murder of Kitty 

40
00:02:54,070 --> 00:02:58,400
Genovese rocked the United States, and it 
led two social psychologists, 

41
00:02:58,400 --> 00:03:02,280
Bibb LatanÃ© and John Darley, 
to conduct a pioneering set of 

42
00:03:02,280 --> 00:03:06,600
experiments on bystander intervention in 
emergency situations. 

43
00:03:06,600 --> 00:03:11,080
What this research found is that people 
are often less likely to intervene when 

44
00:03:11,080 --> 00:03:16,000
other bystanders are present. 
For an overview and a real-life example 

45
00:03:16,000 --> 00:03:20,400
that took place more than 30 years after 
the death of Kitty Genovese, let's watch 

46
00:03:20,400 --> 00:03:25,600
a 13-minute segment that Dateline NBC was 
kind enough to share with our class.

47
00:03:34,100 --> 00:03:39,600
 >> From Studio 3B and Rockefeller 
Center, here is Jane Paulley. 

48
00:03:39,600 --> 00:03:40,930
 >> Good evening. 

49
00:03:40,930 --> 00:03:45,380
If you saw a brutal crime being committed 
on a crowded street, what would you do? 

50
00:03:45,380 --> 00:03:48,700
Rush to help? 
Wait for someone else to make the first move? 

51
00:03:48,700 --> 00:03:50,990
Thankfully, most of us will never have to face

52
00:03:50,990 --> 00:03:56,560
such a terrifying test, but 
unfortunately, many who have, have failed. 

53
00:03:56,560 --> 00:04:00,600
Here's Dennis Murphy. 
 >> She hit this guy's car, just a 

54
00:04:00,600 --> 00:04:05,010
fender bender, and he started, went to 
his truck and got a 

55
00:04:05,010 --> 00:04:09,500
crowbar and started beating her car and 
beating her car. 

56
00:04:09,800 --> 00:04:13,210
 >> It happened to Dortha Word's 
daughter, Deletha, at 2:00 in the morning 

57
00:04:13,210 --> 00:04:16,890
on a weekend last August. 
She was headed home after a summer night 

58
00:04:16,890 --> 00:04:20,340
spin through Belle Isle, Detroit's 
popular island park, 

59
00:04:20,340 --> 00:04:23,370
when she sideswiped a car and didn't stop. 

60
00:04:23,370 --> 00:04:26,960
Moments later, she got stuck in traffic 
coming across the bridge that connects 

61
00:04:26,960 --> 00:04:31,900
Bell Isle with Detroit. 
The man whose car she hit caught up with her. 

62
00:04:31,900 --> 00:04:35,860
 >> He started beating her and telling her look 

63
00:04:35,860 --> 00:04:40,502
what she did to his car. 
 >> The young man, a 6-foot-four, 

64
00:04:40,502 --> 00:04:45,110
270-pound former high school football player, was in 
a rage. 

65
00:04:45,110 --> 00:04:49,400
According to police accounts, he pulled 
Deletha from her car, stripped her nearly 

66
00:04:49,400 --> 00:04:52,936
naked, and began beating her savagely on 
this bridge. 

67
00:04:52,936 --> 00:04:59,500
Deletha, not 5 feet tall, escaped her 
attacker twice, and twice he caught her. 

68
00:04:59,500 --> 00:05:03,490
She broke away once more and climbed over 
the side of the bridge clinging to the 

69
00:05:03,490 --> 00:05:08,120
railing for her life, but when her 
attacker came at her again, this time 

70
00:05:08,120 --> 00:05:13,890
swinging a car jack, Deletha let go, 
falling 30 feet into the Detroit River. 

71
00:05:13,890 --> 00:05:17,410
 >> She couldn't swim, no. 
 >> Two men drove up after Deletha 

72
00:05:17,410 --> 00:05:20,024
jumped and dove into the water to try to 
save her. 

73
00:05:20,024 --> 00:05:25,070
 >> We've got a female in the water that 
hasn't come up yet. 

74
00:05:25,070 --> 00:05:27,510
 >> She disappeared under the swift 
current. 

75
00:05:27,510 --> 00:05:32,310
Her body was found the next morning. 
And what grieves and angers Dortha Word, 

76
00:05:32,310 --> 00:05:36,350
even as much as her daughter's death, is 
that dozens of people stood by and 

77
00:05:36,350 --> 00:05:41,100
watched as the attack went on for 25 
minutes, and no one tried to help, 

78
00:05:41,100 --> 00:05:44,830
or even to call police until Deletha was in 
the river. 

79
00:05:44,830 --> 00:05:48,410
Some of the bystanders had cell phones, 
and there was a police station just at 

80
00:05:48,410 --> 00:05:52,110
the end of the bridge. 
 >> The reason we know, Ms. Word, 

81
00:05:52,110 --> 00:05:55,140
what happened on that bridge, 
because there were people there watching, 

82
00:05:55,140 --> 00:05:56,200
weren't there? 
 >> Yes. 

83
00:05:56,200 --> 00:05:59,002
 >> People saw every bit of it. 
 >> Yes. 

84
00:05:59,002 --> 00:06:00,935
 >> Cars were stopped. 
People got out of their cars. 

85
00:06:00,935 --> 00:06:05,500
 >> Yes. I can't believe it. 

86
00:06:05,500 --> 00:06:08,300
 >> It's something I have to live with for 
the rest of my life. 

87
00:06:08,300 --> 00:06:12,340
Harvey Mayberry was one of the dozens of 
bystanders that terrible night on the 

88
00:06:12,340 --> 00:06:15,000
Belle Isle Bridge. 
 >> What was going on? 

89
00:06:15,000 --> 00:06:19,490
 >> He body-slammed her to the concrete, 
he choked her, he hit her, you name it, 

90
00:06:19,490 --> 00:06:22,800
he practically did it. 
 >> Mayberry and the others watched as the attacker 

91
00:06:22,800 --> 00:06:27,600
held Deletha out to the crowd 
and asked if any of them wanted a piece of her. 

92
00:06:27,600 --> 00:06:29,600
 >> Did you feel like you were watching a movie, maybe? 

93
00:06:29,600 --> 00:06:31,300
 >> A horror one. 

94
00:06:31,300 --> 00:06:35,390
 >> That crowd would become notorious 
nationwide as "the bystanders." 

95
00:06:35,390 --> 00:06:40,000
The incomprehensible bystanders, who 
watched and did nothing as a young woman 

96
00:06:40,000 --> 00:06:44,220
suffered a horrific death. 
 >> How could you just stand by, idly by, 

97
00:06:44,220 --> 00:06:50,700
and watch somebody beat somebody like 
this and beat 'em, and you don't do anything? 

98
00:06:50,700 --> 00:06:53,700
You don't even try to stop them? I mean, 

99
00:06:53,700 --> 00:06:59,000
35 or 40 peoples could've rushed him. 
They didn't even try to do that. 

100
00:06:59,000 --> 00:07:04,760
 >> I would never forget that night. 
I mean, and I'm always wondering what if, 

101
00:07:04,760 --> 00:07:07,740
if I'd have done this, if I'd have done that. 
 >> But he did nothing. 

102
00:07:07,740 --> 00:07:11,621
And neither did bystanders in a 
frightening litany of recent incidents. 

103
00:07:11,621 --> 00:07:16,580
In Washington, D.C., four construction 
workers watched as a woman was mugged. 

104
00:07:16,580 --> 00:07:22,000
In Los Angeles, 30 bus passengers just sat 
in their seats as their driver was beaten senseless. 

105
00:07:22,000 --> 00:07:26,000
And in Virginia, six people stood by as 
a store clerk was 

106
00:07:26,000 --> 00:07:30,300
badly beaten and left bleeding in a corner. 
No one even called police. 

107
00:07:30,300 --> 00:07:32,910
When you hear tragic stories like Deletha Word's 

108
00:07:32,910 --> 00:07:36,980
and countless others like it, you 
might ask yourself: how could those 

109
00:07:36,980 --> 00:07:40,200
people be so inhuman? 
And you might say to yourself:

110
00:07:40,200 --> 00:07:44,390
well, if I'd been there, surely I would have 
stepped in and done something. 

111
00:07:44,390 --> 00:07:47,650
But would you? 
University researchers have been asking 

112
00:07:47,650 --> 00:07:51,762
exactly that question now for decades, 
and what they found out about bystanders, 

113
00:07:51,762 --> 00:07:56,600
like those people on the bridge, could 
cause you to disregard forever that old adage: 

114
00:07:56,600 --> 00:08:00,300
there's safety in numbers. 
 >> There's trouble in numbers. 

115
00:08:00,300 --> 00:08:04,200
People tend to freeze each other and 
cause each other not to respond. 

116
00:08:04,200 --> 00:08:07,800
 >> Princeton psychology professor John 
Darley was inspired to run a series of 

117
00:08:07,800 --> 00:08:12,270
experiments on bystanders by something 
that happened nearly 30 years ago: 

118
00:08:12,270 --> 00:08:16,600
another notorious case of inaction -- the 
murder of a young New York woman, 

119
00:08:16,600 --> 00:08:20,040
Kitty Genovese. 
Genovese screamed for help repeatedly 

120
00:08:20,040 --> 00:08:24,920
during the 35-minute stabbing, but no 
help came, even though more than three 

121
00:08:24,920 --> 00:08:27,420
dozen neighbors heard her cries from 
their apartments. 

122
00:08:27,420 --> 00:08:31,070
The public was horrified, 
but Professor Darley says they shouldn't 

123
00:08:31,070 --> 00:08:33,800
have been surprised. 
 >> We see these incidents on television, 

124
00:08:33,800 --> 00:08:36,155
John, and we say we're not that spineless person. 

125
00:08:36,155 --> 00:08:38,840
 >> You're exactly right. That's what we say. 

126
00:08:38,840 --> 00:08:42,510
We mustn't keep coming to that conclusion. 

127
00:08:42,510 --> 00:08:47,500
We have to think about the uncomfortable 
fact, the uncomfortable truth,  

128
00:08:47,500 --> 00:08:52,279
that I might not help. >> Darley says there is 
something else, 

129
00:08:52,279 --> 00:08:56,600
something more than fear of getting hurt 
or involved that governs our responses. 

130
00:08:56,600 --> 00:09:00,500
 >> There's inaction in numbers. 
The more people who hear or see someone 

131
00:09:00,500 --> 00:09:04,200
in trouble, the less likely that any 
individual will help. 

132
00:09:04,200 --> 00:09:07,300
 >> The person who crumbles to the 
sidewalk with a heart attack, 

133
00:09:07,300 --> 00:09:11,400
are they better off having one person there, or 
three or four people seeing them? 

134
00:09:11,400 --> 00:09:16,110
 >> I'm afraid they're better off having 
one person there, because if one person 

135
00:09:16,110 --> 00:09:21,370
is there, that one person knows 
if this person is going to get help, 

136
00:09:21,370 --> 00:09:24,960
it has to come from me. 
This is all pretty standard stuff. 

137
00:09:24,960 --> 00:09:28,560
 >> To demonstrate his findings 
that people react differently in groups 

138
00:09:28,560 --> 00:09:32,110
than alone, Dr. Darley and his colleague 
Jeff Stone put some 

139
00:09:32,110 --> 00:09:36,260
students to the bystander test. 
They were brought in to a video lab 

140
00:09:36,260 --> 00:09:39,500
room under the guise of watching a tape 
and commenting on it. 

141
00:09:39,500 --> 00:09:42,020
They'd be paid $10 for evaluating the tape. 

142
00:09:42,020 --> 00:09:43,645
 >> So that's sort of how this whole 
thing's set up. 

143
00:09:43,645 --> 00:09:48,600
 >> As the students arrived, they were 
walked past a man posing as a maintenance worker. 

144
00:09:48,600 --> 00:09:50,700
Five minutes into the videotape, the students would hear 

145
00:09:50,700 --> 00:09:54,235
what seemed to be the 
man falling from his ladder and moaning 

146
00:09:54,235 --> 00:09:59,300
out of view around the corner. Our hidden 
cameras watch to see how they'd respond. 

147
00:09:59,300 --> 00:10:01,380
At first, there's just one test subject 

148
00:10:01,380 --> 00:10:07,500
behind our one-way mirror. 
 >> Whoa! [SOUND OF CRASH]. 

149
00:10:07,500 --> 00:10:13,260
 >> I heard this sort of cry or a groan, 
and I decided it was definitely something 

150
00:10:13,260 --> 00:10:17,770
that needed to be investigated. 
 >> Very common single person reaction. 

151
00:10:17,770 --> 00:10:22,700
 >> Single person reaction. 
Nobody else: I, I gotta move. 

152
00:10:23,000 --> 00:10:25,360
 >> Another student, another crash. 

153
00:10:25,360 --> 00:10:29,600
The student has to evaluate what 
he's hearing and decide to help or not. 

154
00:10:29,600 --> 00:10:32,000
 >> Notice the poker face. 

155
00:10:32,000 --> 00:10:34,210
Isn't that marvelous? He's helping me. 

156
00:10:34,210 --> 00:10:37,840
He turned off the tape so he could go 
back and start the tape where it was 

157
00:10:37,840 --> 00:10:42,100
while he's helping. 
 >> What made you decide to act, to intervene? 

158
00:10:42,100 --> 00:10:43,210
 >> The fact that I was the only one 

159
00:10:43,210 --> 00:10:48,230
there, I had to do something. 
 >> [CRASH] Uhh! 

160
00:10:49,240 --> 00:10:53,000
 >> Again and again in his research, Darley 
found that the overwhelming majority 

161
00:10:53,000 --> 00:10:56,900
of people, when alone, help. 
 >> Are you okay? 

162
00:10:56,900 --> 00:10:59,580
He didn't answer when I asked him if he 
was okay. 

163
00:10:59,580 --> 00:11:03,100
So, I ran out, and at that point I was 
really scared. 

164
00:11:03,100 --> 00:11:08,304
I thought about what I'd do, and I thought I'd scream "911!" 

165
00:11:08,304 --> 00:11:12,780
 >> But adding passive bystanders to the 
mix drastically altered the results. 

166
00:11:12,780 --> 00:11:15,660
Watch what happens when Dr. 
Darley plants two people in the room, 

167
00:11:15,660 --> 00:11:18,735
people who have been instructed not to 
react when they hear the crash. 

168
00:11:18,735 --> 00:11:22,580
 >> I thought, man, no matter how many 
people are in here, somebody's got to get 

169
00:11:22,580 --> 00:11:25,980
up and go and check. 
 >> The two men instructed not to react 

170
00:11:25,980 --> 00:11:29,430
are on the left and right of the picture. 
So what happens to the man in the middle? 

171
00:11:29,430 --> 00:11:33,600
 >> Whoa! [CRASH] Uhh! 

172
00:11:33,600 --> 00:11:36,590
 >> As instructed, the confederates look up, 

173
00:11:36,590 --> 00:11:41,400
then go back to watching the videotape. 
The subject glances around, 

174
00:11:41,400 --> 00:11:46,000
then goes back to his work, as the moans continue 
for 14 seconds. 

175
00:11:46,000 --> 00:11:48,682
 >> Emmanuel, did your brain tell you, 
here's a guy in trouble? 

176
00:11:48,682 --> 00:11:50,710
 >> Yes. 
 >> But you didn't get up. 

177
00:11:50,710 --> 00:11:53,800
 >> No, I didn't get up. 
 >> Why didn't you intervene, do you think? 

178
00:11:53,800 --> 00:11:57,600
 >> Because of the two people on my sides. 

179
00:11:57,600 --> 00:12:00,880
They did not react at all, so I figured 

180
00:12:00,880 --> 00:12:03,500
they must know something that I don't know. 

181
00:12:03,500 --> 00:12:05,910
 >> The group dynamic kicked in. 

182
00:12:05,910 --> 00:12:09,040
Even though there was no danger if a 
person answered the call for help. 

183
00:12:09,040 --> 00:12:14,400
Time after time, the subject sat 
frozen when flanked by others who did not react. 

184
00:12:14,400 --> 00:12:17,700
 >> To go against the group is to say all of 

185
00:12:17,700 --> 00:12:24,000
you are wrong and I am right, 
and that's a hard thing for people to do. 

186
00:12:26,200 --> 00:12:28,760
 >> Watch his reaction. He seems to look 

187
00:12:28,760 --> 00:12:31,600
to the others to help in evaluate what's 
going on. 

188
00:12:31,600 --> 00:12:35,270
He doesn't budge. 
 >> Now I know that I would be one 

189
00:12:35,270 --> 00:12:39,080
of those people who you watch 
disgustedly on the news and say, 

190
00:12:39,080 --> 00:12:42,590
"Why didn't he do anything?" 
Thinking about it afterward kind of makes 

191
00:12:42,590 --> 00:12:46,320
you feel like a sheep. 
You just feel like you're controlled by 

192
00:12:46,320 --> 00:12:50,100
what other people do, and you don't really 
too have much control over what you 

193
00:12:50,100 --> 00:12:53,910
think or what you do. 
 >> And that, according to Darley, is exactly what 

194
00:12:53,910 --> 00:12:57,640
happens in most cases. 
People take their cues from those around them 

195
00:12:57,640 --> 00:13:01,380
to decide how to act. 
If they're surrounded by poker faces, 

196
00:13:01,380 --> 00:13:05,780
they are more likely not to respond. 
 >> What makes some of us heroes and 

197
00:13:05,780 --> 00:13:09,635
some of us sheep? 
More than we would like to think, 

198
00:13:09,635 --> 00:13:14,100
coincidence, I'm afraid. 
If many other people are not acting, 

199
00:13:14,100 --> 00:13:20,500
if my confusion, my puzzlement, overwhelm 
me, then I am less likely to act. 

200
00:13:20,500 --> 00:13:22,900
Within me, there's the hero. 

201
00:13:22,900 --> 00:13:26,530
Within me, there's the non-responding bystander. 

202
00:13:26,530 --> 00:13:28,340
 >> Let go. Let go. Let go. 

203
00:13:28,340 --> 00:13:33,400
 >> Also in play is a natural response 
psychologist call "diffusion of responsibility." 

204
00:13:33,400 --> 00:13:35,260
We tell ourselves that someone else will 
probably help, 

205
00:13:35,260 --> 00:13:38,080
will probably handle it better then we would. 

206
00:13:38,080 --> 00:13:40,560
And not the least of it is the fear of 

207
00:13:40,560 --> 00:13:42,700
making a fool of ourselves. 

208
00:13:42,700 --> 00:13:45,580
 >> We're stepping on stage, not knowing 
our lines. 

209
00:13:45,580 --> 00:13:51,290
We may embarrass ourselves, 
and we are intruding in a situation in 

210
00:13:51,290 --> 00:13:53,870
which we may embarrass the people we 
intrude in. 

211
00:13:53,870 --> 00:13:55,861
 >> Are you okay? 

212
00:13:55,861 --> 00:13:59,600
 >> Dr. Darley says people alone help 
80% of the time. 

213
00:13:59,600 --> 00:14:03,600
But people in groups only 20% of the time, 

214
00:14:03,600 --> 00:14:05,460
which means only about one in five of us would 

215
00:14:05,460 --> 00:14:10,370
react as David Brown did, as he 
breaks the force field of the strangers around him. 

216
00:14:10,370 --> 00:14:13,800
 >> Whoa! [CRASH] 
 >> The crash. 

217
00:14:13,800 --> 00:14:15,560
The two confederates notice. 

218
00:14:15,560 --> 00:14:18,000
You can't help but notice. 

219
00:14:18,000 --> 00:14:23,600
He hears it, looks, and it's almost as if he's 
looking at a wall. 

220
00:14:23,600 --> 00:14:28,100
And he was clearly coping with: they're not acting. 

221
00:14:28,100 --> 00:14:32,200
They're telling me there's no trouble 
here, and dealing with that. 

222
00:14:32,200 --> 00:14:37,000
 >> Sometimes what everybody else is doing 
is not necessarily the right thing, so, 

223
00:14:37,000 --> 00:14:41,000
so you have to just kind of ignore it 
and do whatever you think is right. 

224
00:14:41,000 --> 00:14:43,700
 >> Good advice, but a highly unusual reaction. 

225
00:14:43,700 --> 00:14:45,910
Dead opposite of what most of us do 

226
00:14:45,910 --> 00:14:48,390
when we're just another face in the crowd. 

227
00:14:48,390 --> 00:14:50,810
So what should we do if we get in trouble?

228
00:14:50,810 --> 00:14:54,570
If we find ourselves the victim, if 
we are the person on the sidewalk with a 

229
00:14:54,570 --> 00:14:58,040
heart attack, and we see the people out 
there who might help but their faces are 

230
00:14:58,040 --> 00:15:00,854
just frozen like pottery, what do we do? 

231
00:15:00,854 --> 00:15:05,290
 >> Let's make clear that there's no 
ambiguity about what's happening. 

232
00:15:05,290 --> 00:15:08,360
I'm saying: I'm the victim. I really need help. 

233
00:15:08,360 --> 00:15:14,080
Everybody, I really need help. 
And you, Mister, you -- I'm looking you in the eyes. 

234
00:15:15,000 --> 00:15:18,500
Will you please do, whatever. 

235
00:15:18,500 --> 00:15:21,400
 >> Pick a face out of the crowd? 
 >> Pick a face out of the crowd. 

236
00:15:21,400 --> 00:15:26,330
That person knows the responsibility 
rests with him, with her. 

237
00:15:26,330 --> 00:15:30,860
 >> Dr. Darley says that just one person 
stepping forward in a crowd can transform 

238
00:15:30,860 --> 00:15:35,310
the group from passive to active. 
 >> The thing that haunts us, I suspect, 

239
00:15:35,310 --> 00:15:38,190
is that might have happened on the 
Detroit Bridge. 

240
00:15:38,190 --> 00:15:41,180
 >> It is a question that haunts Harvey 
Mayberry every day. 

241
00:15:41,180 --> 00:15:43,899
 >> For the rest of my life, I'm going to 
have to deal with that. 

242
00:15:43,899 --> 00:15:46,800
If I had done this, then maybe she still be here. 

243
00:15:46,800 --> 00:15:50,600
 >> But it took one person to step 
forward still, and nobody did. 

244
00:15:50,600 --> 00:15:53,530
 >> Nobody did. 
But I honestly believe if one person had 

245
00:15:53,530 --> 00:15:55,392
stepped forward, I know I would have. 

246
00:15:55,392 --> 00:16:00,612
But I, to be the first one, no.

247
00:16:00,612 --> 00:16:04,685
 >> We could all find ourselves on that 
bridge, and we could all find ourselves 

248
00:16:04,685 --> 00:16:09,900
not responding, but if we understand 
why that might be so, 

249
00:16:09,900 --> 00:16:12,750
we are more likely to be free to respond. 

250
00:16:14,850 --> 00:16:18,350
The best available evidence suggests that 
Professor Darley is right. 

251
00:16:18,350 --> 00:16:22,050
When people learn about the bystander 
effect, they become more likely to 

252
00:16:22,050 --> 00:16:26,450
intervene in emergencies. 
I should also add that LatanÃ© and Darley 

253
00:16:26,450 --> 00:16:30,130
studied bystander intervention in a wide 
variety of situations 

254
00:16:30,130 --> 00:16:33,360
beyond those in which someone seemed to 
be injured. 

255
00:16:33,360 --> 00:16:38,010
For example, in one study they invited 
students in for an interview on life at 

256
00:16:38,010 --> 00:16:43,060
an urban university, and while students 
sat in the waiting room, a stream of 

257
00:16:43,060 --> 00:16:46,540
white smoke began to pour into the room 
through a vent in the wall. 

258
00:16:46,540 --> 00:16:51,055
At first the smoke was barely 
noticeable, but after awhile, 

259
00:16:51,055 --> 00:16:54,040
it was unmistakable, 
and the question was whether students 

260
00:16:54,040 --> 00:16:58,320
would be less likely to report the smoke 
in the presence of other bystanders 

261
00:16:58,320 --> 00:17:01,880
than when they waited alone. 
Here's what Latane and Darley found. 

262
00:17:03,860 --> 00:17:07,300
When students waited alone, three quarters of 
them reported the smoke, 

263
00:17:07,300 --> 00:17:12,600
and half of them reported it within two minutes. 
It was a very quick response. 

264
00:17:12,600 --> 00:17:16,710
But in another experimental condition, in 
which students sat in the waiting room 

265
00:17:16,710 --> 00:17:20,120
with two confederates who were 
instructed to remain passive 

266
00:17:20,120 --> 00:17:26,900
(just sitting there like human furniture), 
only 1 student out of 10 ever reported the smoke. 

267
00:17:26,900 --> 00:17:28,300
What did they do? 

268
00:17:28,300 --> 00:17:31,200
They coughed. They rubbed their eyes. 
They opened the window. 

269
00:17:31,200 --> 00:17:36,200
But they didn't report the smoke. 
One other noteworthy finding is that 

270
00:17:36,200 --> 00:17:40,940
after the experiment, students who hadn't 
reported the smoke gave an incredible 

271
00:17:40,940 --> 00:17:44,760
variety of explanations. 
For example, some people said that they 

272
00:17:44,760 --> 00:17:48,820
thought that the smoke was actually steam 
or a vapor coming from an air conditioner. 

273
00:17:48,820 --> 00:17:50,870
I've never seen an air conditioner do that, 

274
00:17:50,870 --> 00:17:54,070
but that was an explanation that was offered. 

275
00:17:54,070 --> 00:18:00,000
Others thought that it was smog purposely 
introduced to simulate an urban environment. 

276
00:18:00,000 --> 00:18:01,860
And still others thought that the smoke 

277
00:18:01,860 --> 00:18:05,500
was a truth gas piped in by the 
experimenters! 

278
00:18:06,610 --> 00:18:11,000
What's interesting here is that people's 
perception of the situation as an emergency -- 

279
00:18:11,000 --> 00:18:15,500
that is, their perception of the smoke as 
potentially dangerous -- 

280
00:18:15,500 --> 00:18:18,200
was altered by the number of bystanders present, 

281
00:18:18,200 --> 00:18:22,260
and yet oftentimes students 
weren't aware of being influenced. 

282
00:18:22,260 --> 00:18:26,300
This is similar in some ways to research 
on obedience to authority in which people 

283
00:18:26,300 --> 00:18:31,770
obey an authority but don't realize the 
power of the authority. Or research on 

284
00:18:31,770 --> 00:18:34,780
cognitive dissonance, in which people 
change their perception 

285
00:18:34,780 --> 00:18:39,630
of a situation to say it's not really 
that bad without realizing that they've 

286
00:18:39,630 --> 00:18:44,110
redefined their circumstances. 
Have the results on bystander 

287
00:18:44,110 --> 00:18:48,600
intervention stood the test of time since 
LatanÃ© and Darley published their results? 

288
00:18:48,600 --> 00:18:52,100
With a few specific exceptions, the answer's yes. 

289
00:18:52,100 --> 00:18:56,100
Dozens of researchers have now replicated 
the bystander effect. 

290
00:18:56,100 --> 00:19:00,200
In fact, in 2011 Peter Fischer and an international team 

291
00:19:00,200 --> 00:19:04,000
of eight other researchers published a huge meta-analysis 

292
00:19:04,000 --> 00:19:08,420
of more than 100 independent 
assessments of the bystander effect, 

293
00:19:08,420 --> 00:19:12,370
ranging from the 1960s all the way 
through 2010 

294
00:19:12,370 --> 00:19:16,800
based on data for more than 7,700 
participants. 

295
00:19:16,800 --> 00:19:22,070
According to the report, the results 
showed "clear support... that passive 

296
00:19:22,070 --> 00:19:26,700
bystanders in critical situations reduce 
helping responses." 

297
00:19:27,850 --> 00:19:31,230
Does the bystander effect occur around 
the world? 

298
00:19:31,230 --> 00:19:34,170
It's hard to say. 
The effect is very well documented in 

299
00:19:34,170 --> 00:19:38,000
North America and Europe, but it hasn't 
really been studied enough in other 

300
00:19:38,000 --> 00:19:41,700
countries to know how universal or 
prevalent it is. 

301
00:19:41,700 --> 00:19:46,100
What we do know anecdotally is that many 
countries do report cases 

302
00:19:46,100 --> 00:19:50,500
from time to time in which bystanders fail 
to intervene in emergencies. 

303
00:19:50,500 --> 00:19:56,400
For example, on October 13, 2011, a van 
ran over a 2-year-old girl 

304
00:19:56,400 --> 00:20:00,470
in a Chinese market, and at least 
17 people walked past 

305
00:20:00,470 --> 00:20:06,000
the crushed girl over a period of seven 
minutes. The girl later died. 

306
00:20:06,000 --> 00:20:10,500
On April 14, 2013, a man in India cried for help 

307
00:20:10,500 --> 00:20:14,750
after a speeding truck killed 
his wife and infant, who lay in a pool of 

308
00:20:14,750 --> 00:20:19,000
blood, but no one stopped to help for roughly 
ten minutes. 

309
00:20:19,000 --> 00:20:23,900
On July 9, 2012, a teenage girl in Guwahati, India, 

310
00:20:23,900 --> 00:20:29,100
was sexually molested for 30 minutes by 
approximately 20 men on a busy street, 

311
00:20:29,100 --> 00:20:32,600
and no one ever intervened or 
called the police. 

312
00:20:33,140 --> 00:20:37,230
And of course, you're welcome to use the 
discussion forum to post and discuss 

313
00:20:37,230 --> 00:20:41,100
additional examples of bystander 
intervention or lack of intervention. 

314
00:20:41,800 --> 00:20:46,620
But now let's pause for a pop-up question 
before I make a few concluding remarks. 

315
00:20:50,380 --> 00:20:54,710
Now of course, overall percentages don't 
tell us anything about the type of people 

316
00:20:54,710 --> 00:20:58,560
most likely to intervene, and you might 
be wondering whether LatanÃ© and Darley 

317
00:20:58,560 --> 00:21:02,200
found, for example, that certain 
personality characteristics were 

318
00:21:02,200 --> 00:21:05,200
associated with helping people in an 
emergency. 

319
00:21:05,200 --> 00:21:09,830
LatanÃ© and Darley did investigate the 
role of personality, but they didn't find 

320
00:21:09,830 --> 00:21:12,566
anything substantial. 
Here's what they wrote: 

321
00:21:12,566 --> 00:21:18,360
"Anybody can be led either to help or not 
to help in a specific situation. 

322
00:21:18,360 --> 00:21:22,130
Characteristics of the immediate 
situation may have a more important 

323
00:21:22,130 --> 00:21:27,930
influence on what the bystander does than 
[the individual's] personality or life history. 

324
00:21:27,930 --> 00:21:30,160
These findings further suggest that 

325
00:21:30,160 --> 00:21:35,200
motivational deficiencies may not account 
for the unresponsive bystander."

326
00:21:36,130 --> 00:21:40,150
What they're saying is that it's a 
mistake to think of nonintervention as 

327
00:21:40,150 --> 00:21:44,750
bystander "apathy" -- as a lack of motivation, 
or a lack of caring. 

328
00:21:44,750 --> 00:21:49,810
In fact, many people want to do good, but 
that desire is overridden by situational 

329
00:21:49,810 --> 00:21:54,130
factors like how many other bystanders 
happen to be present, or whether 

330
00:21:54,130 --> 00:21:58,615
an emergency is clear or not. 
Now, more recent research 

331
00:21:58,615 --> 00:22:04,090
has found some links between bystander intervention and 
personality characteristics,

332
00:22:04,090 --> 00:22:06,620
even though the situational factors remain 
strong. 

333
00:22:06,620 --> 00:22:12,300
For example, people who score high in 
masculinity -- whether they're male or female -- 

334
00:22:12,300 --> 00:22:15,570
are less likely to help in emergency situations, 

335
00:22:15,570 --> 00:22:19,940
apparently, because they fear looking 
foolish or being embarrassed if they do 

336
00:22:19,940 --> 00:22:24,000
something wrong, or it turns out that 
their help wasn't needed after all. 

337
00:22:24,000 --> 00:22:26,800
On the other hand, femininity doesn't seem 

338
00:22:26,800 --> 00:22:31,840
to be significantly related to bystander 
intervention one way or the other. 

339
00:22:31,840 --> 00:22:35,420
And research on sex differences shows 
mixed results, 

340
00:22:35,420 --> 00:22:39,810
with some studies finding no sex 
differences, but others finding that men 

341
00:22:39,810 --> 00:22:43,980
are more likely to help women or help in 
dangerous situations, 

342
00:22:43,980 --> 00:22:47,400
and women are more likely than men to 
help children. 

343
00:22:48,630 --> 00:22:53,260
Well, this video has grown long with 
research findings, so let me conclude 

344
00:22:53,260 --> 00:22:57,900
on a personal note. 
As it turns out, Kitty Genovese, her parents, 

345
00:22:57,900 --> 00:23:01,500
and several members of the 
Genovese family are buried in a cemetery 

346
00:23:01,500 --> 00:23:04,510
only in the hour from Wesleyan 
University, 

347
00:23:04,510 --> 00:23:08,650
so I recently visited and took a photo 
for the class. 

348
00:23:08,650 --> 00:23:12,650
The next time that you see something that
might be an emergency, and you're not sure 

349
00:23:12,650 --> 00:23:16,600
whether to call someone for help, here's 
an image to keep in mind. 

350
00:23:16,600 --> 00:23:20,950
If everyone in the class remembers this 
image, we have a good chance, 

351
00:23:20,950 --> 00:23:25,270
collectively, of preventing at least a 
few tragedies like Kitty's. 

352
00:23:25,270 --> 00:23:30,400
And that would be a great way to honor 
the memory of Kitty Genovese, Deletha Word, 

353
00:23:30,400 --> 00:23:36,400
and others who could have been saved 
if it weren't for the bystander effect. 

