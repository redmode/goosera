

1
00:00:07,000 --> 00:00:12,100
In the last video, I mentioned how group 
dynamics can involve intragroup dynamics 

2
00:00:12,100 --> 00:00:16,900
(that is, psychological processes 
and behaviors taking place within a group) 

3
00:00:16,900 --> 00:00:20,100
or intergroup dynamics (the same thing between 

4
00:00:20,100 --> 00:00:25,500
or among different groups). 
Starting with this video, we'll begin looking at 

5
00:00:25,500 --> 00:00:31,000
intergroup dynamics that 
involve categorical judgments of us and them, 

6
00:00:31,000 --> 00:00:34,960
judgments that can give rise to 
prejudice and discrimination. 

7
00:00:35,830 --> 00:00:39,700
Probably the most famous passage ever 
written by a social psychologist on the 

8
00:00:39,700 --> 00:00:45,938
topic of prejudice comes from a classic 
1954 book by Gordon Allport, entitled 

9
00:00:45,938 --> 00:00:51,300
The Nature of Prejudice. 
Allport, who was a professor of psychology 

10
00:00:51,300 --> 00:00:56,800
at Harvard University for most 
of his career, wrote, "The human mind must 

11
00:00:56,800 --> 00:01:01,580
think with the aid of categories... 
Once formed, categories are the basis 

12
00:01:01,580 --> 00:01:06,600
for normal prejudgment. 
We cannot possibly avoid this process. 

13
00:01:06,600 --> 00:01:12,670
Orderly living depends upon it." 
What Allport was saying, and later studies 

14
00:01:12,670 --> 00:01:17,100
have found, is that prejudice and 
intergroup conflict emerge in part  

15
00:01:17,100 --> 00:01:21,690
from the natural human tendency to think 
categorically. 

16
00:01:21,690 --> 00:01:27,150
Not just categories like table, or chair, 
or computer, but social categories, like 

17
00:01:27,150 --> 00:01:33,000
female, or South American, or professor. 
So in order to understand prejudice, 

18
00:01:33,000 --> 00:01:36,800
we first need to understand categorical 
thinking. 

19
00:01:36,800 --> 00:01:40,800
And to begin that examination, I want to 
ask you two very strange questions 

20
00:01:40,800 --> 00:01:45,900
about this apple. 
First, would you say that this apple is 

21
00:01:45,900 --> 00:01:49,380
part of my body? 
Not a trick question. 

22
00:01:49,380 --> 00:01:51,800
Let's pause so that you can answer that 
question. 

23
00:01:55,400 --> 00:02:01,600
Most people consider an apple and a human 
body as 100% separate from each other, 

24
00:02:01,600 --> 00:02:05,600
but let's try an experiment. 
I'm going to take a bite of this apple, 

25
00:02:05,600 --> 00:02:09,300
and you pause the video at the exact 
moment when you think that the 

26
00:02:09,300 --> 00:02:14,900
bite of apple is no longer a bite of apple â when 
it's become part of my body. 

27
00:02:14,900 --> 00:02:20,390
I'll include a timer so that you have an 
exact time when you pause the video. 

28
00:02:20,390 --> 00:02:23,640
And once it's paused, please write down 
the time code that you see here 

29
00:02:23,640 --> 00:02:27,850
because in just a minute, I'll ask you to enter 
that into a pop-up question. 

30
00:02:27,850 --> 00:02:31,928
Then you can just press play and continue 
watching the video as usual. 

31
00:02:31,928 --> 00:02:34,500
Okay? 
You ready? 

32
00:02:34,500 --> 00:02:39,600
Remember, please pause the video when the 
bite of apple is no longer a bite of apple -- 

33
00:02:39,600 --> 00:02:44,330
when it's become part of my body. 
Write down the time code, 

34
00:02:44,330 --> 00:02:47,360
and please participate â there's no right or wrong 
answer. 

35
00:02:47,360 --> 00:02:50,560
What's important is that everyone 
participates. 

36
00:02:50,560 --> 00:02:53,000
Okay? 
Here we go. 

37
00:02:56,700 --> 00:03:01,600
Oh, that is a good apple -- wow! 
Now, I'm not swallowing anything yet. 

38
00:03:02,700 --> 00:03:05,230
I'm just chewing, although some of the 
apple juice 

39
00:03:05,230 --> 00:03:08,550
just trickled down my throat. 
I don't know if that matters, but 

40
00:03:08,550 --> 00:03:13,260
I'm just chewing. 
Now, most of it is chewed up, 

41
00:03:13,260 --> 00:03:15,880
and I'm going to swallow half of the 
bite. 

42
00:03:15,880 --> 00:03:18,045
Here we go. 
Half, but I'll keep the rest in my mouth. 

43
00:03:18,045 --> 00:03:20,530
Oop! 
Half went down. 

44
00:03:20,530 --> 00:03:22,720
I'm imagining that some of you have 
paused the video, 

45
00:03:22,720 --> 00:03:26,650
but I'm going to continue chewing. 
And now I'm going to swallow 

46
00:03:26,650 --> 00:03:28,030
the rest of the bite. 
Ready? 

47
00:03:29,050 --> 00:03:32,490
Here we go. 
Just went down. 

48
00:03:32,490 --> 00:03:37,500
Oop! No. 
I think I might have some apple between my teeth.

49
00:03:38,600 --> 00:03:41,300
Okay. Now, if you haven't already paused 

50
00:03:41,300 --> 00:03:46,000
the video and written down a time code, 
let me give you just another ten seconds 

51
00:03:46,000 --> 00:03:50,450
to rewind the video and do that before we 
reach a pop-up question for you to type in 

52
00:03:50,450 --> 00:03:55,900
your number. Please, everyone participate even if 
you're not sure what the best answer is.

53
00:04:12,000 --> 00:04:16,080
I thought you might like to see 
how other members of our class answered, 

54
00:04:16,080 --> 00:04:20,200
so I'll replay the video. But this time 
I'll show the percentage of students who 

55
00:04:20,200 --> 00:04:25,300
say that the bite of apple has become 
part of my body at any given moment -- 

56
00:04:25,700 --> 00:04:30,200
that is, the percentage of students who have 
paused the video by that point. 

57
00:04:34,035 --> 00:04:37,400
Oh, that is a good apple -- wow! 

58
00:04:37,400 --> 00:04:41,400
Now, I'm not swallowing anything yet. 
I'm just chewing, although 

59
00:04:41,400 --> 00:04:44,470
some of the apple juice just trickled 
down my throat. 

60
00:04:44,470 --> 00:04:48,027
I don't know if that matters, but I'm 
just chewing. 

61
00:04:48,027 --> 00:04:53,460
Now, most of it is chewed up, and I'm 
going to swallow half of the bite. 

62
00:04:53,460 --> 00:04:55,445
Here we go. 
Half, but I'll keep the rest in my mouth. 

63
00:04:55,445 --> 00:04:59,400
Oop! Half went down. 
I'm imagining that some of you have 

64
00:04:59,400 --> 00:05:02,277
paused the video. 
But I'm going to continue chewing. 

65
00:05:02,277 --> 00:05:05,610
And now I'm going to swallow the rest of the 
bite. Ready? 

66
00:05:06,630 --> 00:05:11,500
Here we go. 
Just went down. Oop! No. 

67
00:05:11,500 --> 00:05:15,100
I think I might have some apple between 
my teeth. 

68
00:05:16,135 --> 00:05:20,200
Do you see what's interesting here? 
It's very hard to say exactly 

69
00:05:20,200 --> 00:05:24,000
when the apple stops being an apple and starts 
being me. 

70
00:05:24,000 --> 00:05:27,386
There's no single answer. 
Reasonable people can draw the line 

71
00:05:27,386 --> 00:05:31,800
in different places. 
Yet even so, society distinguishes 

72
00:05:31,800 --> 00:05:35,770
between an apple and a person because 
it's useful to do so. 

73
00:05:35,770 --> 00:05:39,280
We don't go out into an apple orchard and 
pick people. 

74
00:05:39,280 --> 00:05:44,200
We pick apples. 
The trick is to avoid confusing useful 

75
00:05:44,200 --> 00:05:46,888
with real. 
That is, to avoid being fooled into 

76
00:05:46,888 --> 00:05:51,510
thinking that there's a fixed boundary 
between an apple and a person, 

77
00:05:51,510 --> 00:05:55,700
or between any two people, 
or between people and the environment. 

78
00:05:56,630 --> 00:06:00,600
After all, rainwater from a thunderstorm 
in Brazil months ago may have been 

79
00:06:00,600 --> 00:06:05,600
soaked up by oranges that were made into the 
orange juice that you drank this past week. 

80
00:06:05,800 --> 00:06:10,600
And now, that Brazilian rainwater may be 
circulating in your body. 

81
00:06:11,600 --> 00:06:14,100
So our connection with the environment is 
not simply a matter 

82
00:06:13,790 --> 00:06:18,300
of ashes to ashes and 
dust to dust in which we'll eventually 

83
00:06:18,300 --> 00:06:22,700
return to the environment. 
We're already made up of the environment. 

84
00:06:22,700 --> 00:06:27,660
Our bodies contain countless molecules 
that lived as trees and caterpillars, 

85
00:06:27,660 --> 00:06:31,342
rocks and rainwater. 
In fact, researchers have found that 

86
00:06:31,342 --> 00:06:36,490
90% of the cells in and on the human body 
aren't even human. 

87
00:06:36,490 --> 00:06:40,492
They're microbial cells -- nonhuman 
microorganisms that live 

88
00:06:40,492 --> 00:06:47,200
in our intestines, on our skin, and so forth. 
The problem is that sometimes we forget 

89
00:06:47,200 --> 00:06:51,263
that the labels we give things are simply 
useful linguistic devices, 

90
00:06:51,263 --> 00:06:55,900
and that these devices are at best, 
only a rough approximation. 

91
00:06:56,200 --> 00:07:00,100
So, for example, we talk about ourselves 
as though we're the same person that our 

92
00:07:00,100 --> 00:07:05,370
mothers gave birth to, 
but the reality is somewhat different. 

93
00:07:05,370 --> 00:07:09,710
The skin on our body is not the same skin 
that covered us seven years ago. 

94
00:07:09,710 --> 00:07:15,170
It's made up of entirely new cells. 
The fat under our skin is not 

95
00:07:15,170 --> 00:07:20,658
the same fat that was there a year ago. 
The oldest red blood cell that any of us 

96
00:07:20,658 --> 00:07:24,270
have came on the scene only 120 days ago 
or so. 

97
00:07:24,270 --> 00:07:29,088
And the entire lining of our digestive 
tract has been replaced in the last 

98
00:07:29,088 --> 00:07:33,157
three days. 
Even your skeleton gets replaced every 

99
00:07:33,157 --> 00:07:38,078
10 years or so, your muscles every 15, 
and if you were to average 

100
00:07:38,078 --> 00:07:44,360
the age of all cells in your body, those of you who are 
college-aged wouldn't be college-aged. 

101
00:07:44,360 --> 00:07:48,730
You'd be somewhere around 7 to 10 
years old. 

102
00:07:48,730 --> 00:07:53,280
What endures and is uniquely identifiable 
isn't any fixed set of atoms 

103
00:07:53,280 --> 00:07:57,536
or molecules or cells. 
Those change over time. 

104
00:07:57,536 --> 00:08:05,210
What endures is simply a pattern -- a way 
of organizing a face, a body, behavior. 

105
00:08:05,210 --> 00:08:10,500
Likewise, we talk about day and night, 
plant and animal, female and male, 

106
00:08:10,500 --> 00:08:14,240
alive and dead, all as though they're separate 
categories. 

107
00:08:14,240 --> 00:08:19,150
But this is linguistic. 
When you look closely, the lines blur, 

108
00:08:19,150 --> 00:08:22,000
and you can see that they're all really 
gradients. 

109
00:08:22,000 --> 00:08:24,998
For instance, we talk about being alive 
or dead as though 

110
00:08:24,998 --> 00:08:29,260
they're discrete categories. 
You're either one or the other. 

111
00:08:29,260 --> 00:08:33,400
But at what point do people die? 
When their brainwaves are flat? 

112
00:08:33,400 --> 00:08:37,304
After their last breath? Their last 
heartbeat? When the blood in their 

113
00:08:37,304 --> 00:08:41,260
veins stops moving? Or their reflexes no longer 
respond? 

114
00:08:41,260 --> 00:08:45,500
Any definition you come up with will be 
somewhat arbitrary, 

115
00:08:45,500 --> 00:08:50,203
like when an apple stops being an apple, 
because those events all 

116
00:08:50,203 --> 00:08:55,818
take place at different times. 
In fact, if you were to define death as 

117
00:08:55,818 --> 00:08:59,590
taking place when skin cells no longer 
divide, 

118
00:08:59,590 --> 00:09:04,860
that time might not come for a day or two 
after the heart has stopped pumping. 

119
00:09:04,860 --> 00:09:08,300
So even life and death fall along a 
continuum. 

120
00:09:08,300 --> 00:09:12,800
You can be alive in certain respects and 
dead in others -- 

121
00:09:12,800 --> 00:09:17,208
what you might call "partially dead." 
But in our language we find it useful 

122
00:09:17,208 --> 00:09:20,900
to treat life and death as categorically 
different. 

123
00:09:20,900 --> 00:09:25,750
When a killer is wanted dead or alive, we 
understand what's meant. 

124
00:09:25,750 --> 00:09:29,904
The problem when it comes to social 
categories is that we sometimes forget 

125
00:09:29,904 --> 00:09:35,480
that these categories are psychologically 
and culturally constructed. 

126
00:09:35,480 --> 00:09:39,560
We stop thinking of Black people and 
White people or females and males as 

127
00:09:39,560 --> 00:09:44,700
useful fictions and start regarding them 
as biological facts. 

128
00:09:45,600 --> 00:09:49,600
In the United States the biological truth 
is that millions of White people 

129
00:09:49,600 --> 00:09:55,144
have genes from Black ancestors. 
Among White Americans roughly 1 out of 

130
00:09:55,144 --> 00:09:59,700
every 100 genes comes from a Black 
ancestor. 

131
00:09:59,900 --> 00:10:05,610
Conversely, 75 to 90% of Black Americans 
have White ancestors, 

132
00:10:05,610 --> 00:10:09,965
including most obviously Barack Obama, 
whose mother was White and 

133
00:10:09,965 --> 00:10:14,384
father was Kenyan. 
On average, Black Americans have gotten 

134
00:10:14,384 --> 00:10:19,590
one out of every four or five of their 
genes from a White ancestor. 

135
00:10:19,590 --> 00:10:23,616
So, if White Americans discriminate 
against Black Americans, 

136
00:10:23,616 --> 00:10:28,450
they're usually discriminating against 
the descendant of a White person. 

137
00:10:28,450 --> 00:10:31,760
This is the blurry nature of social 
categories. 

138
00:10:33,070 --> 00:10:36,300
Now, it might seem obvious that racial 
lines are blurry, 

139
00:10:36,300 --> 00:10:40,900
but what about other social categories -- 
are they all that way? 

140
00:10:40,900 --> 00:10:46,000
The answer is yes. 
Social categories are, by their very nature, 

141
00:10:46,000 --> 00:10:50,530
socially constructed. 
Even categories like "female" and "male"  

142
00:10:50,530 --> 00:10:56,500
have a fuzzy line and fall along continua. 
To see why that is, consider the case 

143
00:10:56,500 --> 00:11:01,100
of transgender people -- that is, people whose 
gender is different than the 

144
00:11:01,100 --> 00:11:04,900
gender assigned to them at birth or who 
otherwise don't fit into 

145
00:11:04,900 --> 00:11:10,850
conventional categories of female or male. 
I'm not talking about sexual orientation 

146
00:11:10,850 --> 00:11:15,222
or sexual attraction to others. 
I'm talking about the perception people have 

147
00:11:15,222 --> 00:11:21,200
of themselves -- of their self-identity and 
the expression of that identity. 

148
00:11:21,200 --> 00:11:24,700
So, to take just one example, someone may be born

149
00:11:24,700 --> 00:11:30,300
intersexual with genital organs 
that aren't clearly female or male. 

150
00:11:30,730 --> 00:11:35,700
There are also cases in which someone's 
born with XY male chromosomes 

151
00:11:35,700 --> 00:11:41,098
but is anatomically closer to female, or XX 
female chromosomes 

152
00:11:41,098 --> 00:11:46,700
but is anatomically closer to male. 
In fact, it's possible for someone to be 

153
00:11:46,700 --> 00:11:52,431
genetically female, anatomically male, 
hormonally female, but more behaviorally 

154
00:11:52,431 --> 00:11:56,800
male, and self-identify as neither female 
nor male. 

155
00:11:56,800 --> 00:12:02,096
Social categories are complex. 
To illustrate the point, I'm pleased to share 

156
00:12:02,096 --> 00:12:06,000
two videos that have been 
generously made available to our class. 

157
00:12:06,000 --> 00:12:11,900
The first video is a 9-minute excerpt 
from "Race: The Power of an Illusion," 

158
00:12:11,900 --> 00:12:17,300
a documentary that discusses the social and 
cultural construction of race. 

159
00:12:17,300 --> 00:12:21,697
Even though the video focuses somewhat on 
the American case, the general  

160
00:12:21,697 --> 00:12:26,674
principles apply to all countries. 
The video was broadcast on PBS, and 

161
00:12:26,674 --> 00:12:33,204
comes to us courtesy of California Newsreel. 
The other video is a 23-minute excerpt 

162
00:12:33,204 --> 00:12:40,000
from an HBO documentary called "Middle 
Sexes: Redefining He and She," written and 

163
00:12:40,000 --> 00:12:46,400
directed by Emmy Award winning filmmaker 
Antony Thomas and narrated by Gore Vidal. 

164
00:12:46,400 --> 00:12:51,200
In that video the topic is gender 
identity and intersexuality,  

165
00:12:51,200 --> 00:12:55,610
including cross-cultural differences in how gender 
is defined. 

166
00:12:56,930 --> 00:13:01,687
I'm hoping that these two videos will 
stimulate some great forum discussions on 

167
00:13:01,687 --> 00:13:06,160
race and gender that move beyond simple 
categories of Black, White, 

168
00:13:06,160 --> 00:13:12,040
Asian, Indian, female, male, and so forth. 
If you do participate, all I would ask 

169
00:13:12,040 --> 00:13:17,000
is that you keep a few things in mind. 
First, please make sure to watch 

170
00:13:17,000 --> 00:13:21,300
the videos on race and gender before 
commenting on those topics 

171
00:13:21,300 --> 00:13:24,852
in the discussion forums. 
That'll lay the foundation for a much more 

172
00:13:24,852 --> 00:13:30,400
informed and interesting commentary. 
Second, many of us have strong views 

173
00:13:30,400 --> 00:13:35,600
about gender, sexuality, religion, and so 
forth, so I just want to remind everyone 

174
00:13:35,600 --> 00:13:41,700
that this is a psychology class -- not a 
place for politicking or religious debate. 

175
00:13:41,700 --> 00:13:44,500
Third, please respect cultural differences. 

176
00:13:44,500 --> 00:13:47,300
There's no cultural majority in this class -- 

177
00:13:47,300 --> 00:13:51,700
everyone's a minority, which 
offers a great chance for cultural exchange, 

178
00:13:51,700 --> 00:13:57,000
as long as there's respect. 
Fourth, in a class this size you're bound 

179
00:13:57,000 --> 00:14:02,600
to have fellow classmates who identify 
themselves as transgender or intersexual. 

180
00:14:02,600 --> 00:14:06,814
It's fine to ask them questions or share 
points of view, but again, please 

181
00:14:06,814 --> 00:14:11,263
do so respectfully. 
And finally, if you're someone who has 

182
00:14:11,263 --> 00:14:15,730
a moral or religious objection to certain 
gender identities, 

183
00:14:15,730 --> 00:14:20,146
it's important to understand that people 
who are born intersex didn't choose 

184
00:14:20,146 --> 00:14:25,500
to be intersex, any more than someone else chose to 
be born a particular race, 

185
00:14:25,500 --> 00:14:30,520
or for that matter, born interracial. 
In fact, there are undoubtedly 

186
00:14:30,520 --> 00:14:35,180
people in our class who are intersex without 
realizing it, simply because 

187
00:14:35,180 --> 00:14:39,471
they've never been genetically tested, 
just as most of us have never been tested 

188
00:14:39,471 --> 00:14:45,100
to see what our racial composition is. 
Of course, that's not to say that there's 

189
00:14:45,100 --> 00:14:50,200
anything wrong with social identities 
that are chosen or that develop later in life. 

190
00:14:50,200 --> 00:14:53,100
I simply mention these things as a kind 
of starting point 

191
00:14:53,100 --> 00:14:57,300
for what I hope will be 
some very valuable class discussions. 

192
00:14:57,300 --> 00:14:59,000
Enjoy the videos! 

