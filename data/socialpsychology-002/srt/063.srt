





1
00:00:06,680 --> 00:00:11,358
I'm very pleased and honored to introduce 
an animated guest lecture, first given in 

2
00:00:11,358 --> 00:00:16,730
2012, entitled "Secrets from the Science 
of Persuasion." 

3
00:00:16,730 --> 00:00:21,230
It's a lecture given by Robert Cialdini 
and Steve Martin, who are, respectively, 

4
00:00:21,230 --> 00:00:27,800
the founding president and head of the UK 
branch of Influence at Work, a company 

5
00:00:27,800 --> 00:00:33,050
that uses persuasion research to improve 
organizational and personal performance. 

6
00:00:34,050 --> 00:00:37,300
Dr. Cialdini is also Regents' Professor Emeritus 

7
00:00:36,400 --> 00:00:41,010
of Psychology and 
Marketing at Arizona State University 

8
00:00:41,010 --> 00:00:46,360
and is author of bestsellers that have 
sold over two million copies and have 

9
00:00:46,360 --> 00:00:50,465
been translated into dozens of languages, 
including the bestseller, Yes, 

10
00:00:50,465 --> 00:00:54,700
coauthored with Noah Goldstein and Steve 
Martin. 

11
00:00:54,700 --> 00:00:58,720
From an academic perspective, I should 
also mention that Professor Cialdini is 

12
00:00:58,720 --> 00:01:03,700
the most frequently cited social 
psychologist in the field of social influence 

13
00:01:03,700 --> 00:01:06,300
and is a recipient of the Distinguished Scientist Award 

14
00:01:06,300 --> 00:01:09,900
from the Society of 
Experimental Social Psychology. 

15
00:01:10,700 --> 00:01:14,370
It's hard to imagine anyone better 
qualified to give the lecture that 

16
00:01:14,370 --> 00:01:16,600
you're about to hear. 
I hope you enjoy it. 

17
00:01:30,930 --> 00:01:35,210
 >> Researchers have been studying the 
factors that influence us to say yes 

18
00:01:35,210 --> 00:01:40,485
to the requests of others for over 60 years, 
and there can be no doubt that there's a 

19
00:01:40,485 --> 00:01:46,150
science to how we are persuaded, and a 
lot of this science is surprising. 

20
00:01:46,150 --> 00:01:50,110
When making a decision, it would be nice 
to think that people consider all the 

21
00:01:50,110 --> 00:01:55,010
available information in order to guide 
their thinking, but the reality is very 

22
00:01:55,010 --> 00:01:59,560
often different. 
In the increasingly overloaded lives we lead, 

23
00:01:59,560 --> 00:02:05,050
more than ever, we need shortcuts 
or rules of thumb to guide our decision making. 

24
00:02:05,050 --> 00:02:08,964
My own research has identified just six of these 

25
00:02:08,964 --> 00:02:13,600
shortcuts as universals that 
guide human behavior. 

26
00:02:13,600 --> 00:02:22,600
They are: reciprocity, scarcity, 
authority, consistency, liking, and consensus. 

27
00:02:22,600 --> 00:02:25,500
Understanding these shortcuts and employing them 

28
00:02:25,500 --> 00:02:30,430
in an ethical manner can 
significantly increase the chances 

29
00:02:30,430 --> 00:02:34,250
that someone will be persuaded by your 
request. 

30
00:02:34,250 --> 00:02:40,174
Let's take a closer look at each in turn. 
So, the first universal principal of 

31
00:02:40,174 --> 00:02:44,480
influence is reciprocity. 
Simply put, people are obliged to give 

32
00:02:44,480 --> 00:02:49,200
back to others the form of behavior, gift, 
or service that they have received first. 

33
00:02:49,400 --> 00:02:51,500
If a friend invites you to their party, 

34
00:02:51,500 --> 00:02:55,720
there's an obligation for you to invite them to a 
future party you are hosting. 

35
00:02:55,720 --> 00:02:59,210
If a colleague does you a favor, then you 
owe that colleague a favor. 

36
00:02:59,210 --> 00:03:03,700
And in the context of a social 
obligation, people are more likely to say 

37
00:03:03,700 --> 00:03:08,800
yes to those that they owe. 
One of the best demonstrations of the 

38
00:03:08,800 --> 00:03:13,500
principle of reciprocation comes from a 
series of studies conducted in restaurants. 

39
00:03:13,500 --> 00:03:14,970
So, the last time you visited a restaurant, 

40
00:03:14,970 --> 00:03:18,340
there's a good chance that a 
waiter or waitress will have 

41
00:03:18,340 --> 00:03:20,960
given you a gift, 
probably about the same time that they 

42
00:03:20,960 --> 00:03:27,190
bring your bill -- a liqueur perhaps, or a 
fortune cookie, or perhaps a simple mint. 

43
00:03:27,190 --> 00:03:31,320
So, here's the question: Does the giving 
of a mint have any influence over how 

44
00:03:31,320 --> 00:03:34,780
much tip you're going to leave them? 
Most people will say no, 

45
00:03:34,780 --> 00:03:38,380
but that mint can make a surprising 
difference. 

46
00:03:38,380 --> 00:03:42,200
In the study, giving diners a single mint 
at the end of their meal typically 

47
00:03:42,200 --> 00:03:48,335
increased tips by around 3%. 
Interestingly, if the gift is doubled and 

48
00:03:48,335 --> 00:03:52,400
two mints are provided, tips don't 
double -- they quadruple, 

49
00:03:52,400 --> 00:03:58,320
a 14% increase in tips. 
But perhaps most interestingly of all is 

50
00:03:58,320 --> 00:04:02,590
the fact that if the waiter provides one 
mint, starts to walk away from the table, 

51
00:04:02,590 --> 00:04:08,170
but pauses, turns back, and says, "For you 
nice people, here's an extra mint," 

52
00:04:08,170 --> 00:04:13,310
tips go through the roof -- a 23% increase, 
influenced not by what 

53
00:04:13,310 --> 00:04:17,795
was given, but how it was given. 
So, the key to using the principle of 

54
00:04:17,795 --> 00:04:21,930
reciprocation is to be the first to give, 
and to ensure that what you give is 

55
00:04:21,930 --> 00:04:26,210
personalized and unexpected. 
The second universal principle of 

56
00:04:26,210 --> 00:04:30,009
persuasion is scarcity. 
Simply put, people want more of those 

57
00:04:30,009 --> 00:04:35,280
things they can have less of. 
When British Airways announced in 2003 

58
00:04:35,280 --> 00:04:39,930
that they would no longer be operating 
the twice-daily London-New York Concord 

59
00:04:39,930 --> 00:04:44,400
flight because it had become 
uneconomical to run, 

60
00:04:44,400 --> 00:04:50,300
sales the very next day took off. 
Notice that nothing had changed about 

61
00:04:50,300 --> 00:04:54,910
the Concord itself. 
It certainly didn't fly any faster, 

62
00:04:54,910 --> 00:04:59,150
the service didn't suddenly get better, 
and the airfare didn't drop; 

63
00:04:59,150 --> 00:05:05,500
it had simply become a scarce resource, 
and as a result, people wanted it more. 

64
00:05:05,500 --> 00:05:08,830
So, when it comes to effectively 
persuading others using the scarcity 

65
00:05:08,830 --> 00:05:13,570
principle, the science is clear. 
It's not enough simply to tell people 

66
00:05:13,570 --> 00:05:18,650
about the benefits they'll gain if they 
choose your products and services; 

67
00:05:18,650 --> 00:05:22,690
you'll also need to point out what is unique 
about your proposition 

68
00:05:22,690 --> 00:05:27,720
and what they stand to lose if they fail 
to consider your proposal. 

69
00:05:28,880 --> 00:05:31,760
Our third principle of influence is the 
principle of authority -- 

70
00:05:31,760 --> 00:05:36,900
the idea that people follow the lead of 
credible, knowledgeable experts. 

71
00:05:36,900 --> 00:05:41,400
Physiotherapists, for example, are able to 
persuade more of their patients to comply 

72
00:05:41,400 --> 00:05:45,790
with recommended exercise programs if 
they display their medical diplomas on 

73
00:05:45,790 --> 00:05:50,160
the walls of their consulting rooms. 
People are more likely to give change for 

74
00:05:50,160 --> 00:05:55,400
a parking meter to a complete stranger if 
that requester wears a uniform rather 

75
00:05:55,400 --> 00:05:59,480
than casual clothes. 
What the science is telling us is that 

76
00:05:59,480 --> 00:06:03,100
it's important to signal to others what 
makes you a credible, 

77
00:06:03,100 --> 00:06:08,000
knowledgeable authority before you make 
your influence attempt. 

78
00:06:08,000 --> 00:06:11,690
Of course, this can present problems; 
you can hardly go around telling 

79
00:06:11,690 --> 00:06:15,560
potential customers how brilliant you 
are, but you can certainly arrange for 

80
00:06:15,560 --> 00:06:19,660
someone to do it for you. 
And surprisingly, the science tells us 

81
00:06:19,660 --> 00:06:23,790
that it doesn't seem to matter if the 
person who introduces you is not only 

82
00:06:23,790 --> 00:06:28,960
connected to you, but also likely to 
prosper from the introduction themselves. 

83
00:06:28,960 --> 00:06:33,120
One group of real estate agents were able 
to increase both the number of property 

84
00:06:33,120 --> 00:06:37,930
appraisals and the number of subsequent 
contracts that they wrote by arranging 

85
00:06:37,930 --> 00:06:42,705
for reception staff that answered 
customer inquiries to first mention their 

86
00:06:42,705 --> 00:06:47,720
colleagues' credentials and expertise. 
So customers interested in letting a 

87
00:06:47,720 --> 00:06:52,260
property were told, "Lettings? Let me 
connect you with Sandra, who has over 15 

88
00:06:52,260 --> 00:06:55,380
years experience letting properties in 
this area." 

89
00:06:55,380 --> 00:06:59,270
Customers who wanted more information 
about selling properties were told, 

90
00:06:59,270 --> 00:07:03,390
"Speak to Peter, our head of sales. He has over 
20 years experience selling properties. 

91
00:07:03,390 --> 00:07:07,820
I'll put you through now." 
The impact of this expert introduction 

92
00:07:07,820 --> 00:07:12,370
led to a 20% rise in the number of 
appointments and a 15% increase in the 

93
00:07:12,370 --> 00:07:17,460
number of signed contracts -- 
not bad for a small change informed from 

94
00:07:17,460 --> 00:07:22,710
persuasion science that was both ethical 
and costless to implement. 

95
00:07:22,710 --> 00:07:27,480
The next principle is consistency. 
People like to be consistent with the things 

96
00:07:27,480 --> 00:07:33,570
they have previously said or done. 
Consistency is activated by looking for 

97
00:07:33,570 --> 00:07:38,340
and asking for small initial commitments 
that can be made. 

98
00:07:38,340 --> 00:07:43,380
In one famous set of studies, researchers 
found, rather unsurprisingly, that very 

99
00:07:43,380 --> 00:07:48,440
few people would be willing to erect an 
unsightly wooden board on their front 

100
00:07:48,440 --> 00:07:53,500
lawn to support a drive safely campaign 
in their neighborhood; 

101
00:07:53,500 --> 00:07:58,990
however, in a similar neighborhood close 
by, four times as many homeowners 

102
00:07:58,990 --> 00:08:02,915
indicated that they would be willing to 
erect this unsightly billboard. 

103
00:08:02,915 --> 00:08:07,350
Why? 
Because ten days previously they had 

104
00:08:07,350 --> 00:08:13,450
agreed to place a small postcard in the 
front window of their home that signaled 

105
00:08:13,450 --> 00:08:17,020
their support for a drive safely 
campaign. 

106
00:08:17,020 --> 00:08:23,910
That small card was the initial 
commitment that led to a 400% increase 

107
00:08:23,910 --> 00:08:27,255
in a much bigger but still consistent 
change. 

108
00:08:27,255 --> 00:08:33,240
So, when seeking to influence using the 
consistency principle, the detective of 

109
00:08:33,240 --> 00:08:39,500
influence looks for voluntary, active, 
and public commitments and ideally gets 

110
00:08:39,500 --> 00:08:45,040
those commitments in writing. 
For example, one recent study reduced 

111
00:08:45,040 --> 00:08:52,220
missed appointments at health centers by 
18% simply by asking the patients, rather 

112
00:08:52,220 --> 00:08:57,580
than the staff, to write down appointment 
details on the future appointment card. 

113
00:08:58,720 --> 00:09:00,850
The fifth principal is the principal of 
liking. 

114
00:09:00,850 --> 00:09:04,600
People prefer to say yes to those that 
they like. 

115
00:09:04,600 --> 00:09:07,520
But what causes one person to like 
another? 

116
00:09:07,520 --> 00:09:12,030
Persuasion science tells us that there 
are three important factors. 

117
00:09:12,030 --> 00:09:17,070
We like people who are similar to us. We 
like people who pay us compliments. 

118
00:09:17,070 --> 00:09:20,400
And we like people who cooperate with us towards 
mutual goals. 

119
00:09:21,510 --> 00:09:25,650
As more and more of the interactions that 
we are having take place online, it might 

120
00:09:25,650 --> 00:09:30,660
be worth asking whether these factors can 
be employed effectively in, let's say, 

121
00:09:30,660 --> 00:09:35,360
online negotiations. 
In a series of negotiations studies 

122
00:09:35,360 --> 00:09:39,300
carried out between MBA students at two 
well-known business schools,

123
00:09:39,300 --> 00:09:43,880
some groups were told, "Time is money, get 
straight down to business." 

124
00:09:43,880 --> 00:09:47,870
In this group, around 55% were able to 
come to an agreement. 

125
00:09:47,870 --> 00:09:53,020
A second group, however, were told, "Before 
you begin negotiating, exchange some 

126
00:09:53,020 --> 00:09:57,566
personal information with each other, 
identify a similarity you share in common, 

127
00:09:57,566 --> 00:10:04,320
then begin negotiating." 
In this group, 90% of them were able to come 

128
00:10:04,320 --> 00:10:10,050
to successful and agreeable outcomes 
that were typically worth 18% more to 

129
00:10:10,050 --> 00:10:13,870
both parties. 
So, to harness this powerful principle of liking, 

130
00:10:13,870 --> 00:10:18,430
be sure to look for areas of 
similarity that you share with others, 

131
00:10:18,430 --> 00:10:22,800
and genuine compliments you could give 
before you get down to business. 

132
00:10:23,920 --> 00:10:28,480
The final principle is consensus. 
Especially when they are uncertain, 

133
00:10:28,480 --> 00:10:33,100
people will look to the actions and 
behaviors of others to determine their own. 

134
00:10:33,200 --> 00:10:36,100
You may have noticed that hotels often 

135
00:10:36,100 --> 00:10:41,080
place a small card in bathrooms that 
attempt to persuade guests to reuse 

136
00:10:41,080 --> 00:10:45,600
their towels and linen. 
Most do this by drawing a guest's 

137
00:10:45,600 --> 00:10:50,860
attention to the benefits that reuse can 
have on environmental protection. 

138
00:10:50,860 --> 00:10:56,090
It turns out that this is a pretty 
effective strategy, leading to around 35% 

139
00:10:56,090 --> 00:10:59,900
compliance. But could there be an even 
more effective way? 

140
00:10:59,900 --> 00:11:03,420
Well, it turns out that about 75% of people 

141
00:11:03,420 --> 00:11:08,140
who check into a hotel for four 
nights or longer will reuse their towels 

142
00:11:08,140 --> 00:11:12,430
at some point during their stay. 
So, what would happen if we took a lesson 

143
00:11:12,430 --> 00:11:17,340
from the principle of consensus and 
simply included that information on the cards, 

144
00:11:17,340 --> 00:11:23,250
and said that "75% of our guests 
reuse their towels at some time during 

145
00:11:23,250 --> 00:11:26,660
their stay, 
so please do so as well." 

146
00:11:26,660 --> 00:11:33,060
It turns out that when we do this, towel 
reuse rises by 26%. 

147
00:11:33,060 --> 00:11:37,560
Now, imagine the next time you stay in a 
hotel, you saw one of these signs, 

148
00:11:37,560 --> 00:11:40,375
you picked it up, and you read the 
following message, 

149
00:11:40,375 --> 00:11:48,120
"75% of people who have stayed in this 
room have reused their towel." 

150
00:11:48,120 --> 00:11:52,220
What would you think? 
Well, here's what you might think: 

151
00:11:52,220 --> 00:11:57,080
"I hope they're not the same towels." 
And like most people, you probably think 

152
00:11:57,080 --> 00:12:01,600
that this sign will have no influence on 
your behavior whatsoever, 

153
00:12:01,600 --> 00:12:06,930
but it turns out that changing just a few 
words on a sign to honestly point out 

154
00:12:06,930 --> 00:12:11,980
what comparable previous guests have 
done was the single most effective 

155
00:12:11,980 --> 00:12:18,000
message, leading to a 33% increase in 
reuse. 

156
00:12:18,000 --> 00:12:23,070
So the science is telling us that rather 
than relying on our own ability to 

157
00:12:23,070 --> 00:12:28,600
persuade others, we can point to what 
many others are already doing, 

158
00:12:28,600 --> 00:12:34,730
especially many similar others. 
So, there we have it: six scientifically 

159
00:12:34,730 --> 00:12:39,145
validated principles of persuasion that 
provide for small, practical, often 

160
00:12:39,145 --> 00:12:43,700
costless changes that can lead to big 
differences in your ability 

161
00:12:43,700 --> 00:12:47,970
to influence and persuade others, 
in an entirely ethical way. 

162
00:12:47,970 --> 00:12:53,000
They are the secrets from the science of 
persuasion. 

