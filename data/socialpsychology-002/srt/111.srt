1
00:00:06,520 --> 00:00:10,100
As we come to the end of our journey together, I hope you can look back and

2
00:00:10,100 --> 00:00:14,700
see some main course themes and ideas that social psychology offers us,

3
00:00:14,700 --> 00:00:18,190
such as the psychological construction of reality,

4
00:00:18,190 --> 00:00:22,150
the surprising speed with which people form social impressions,

5
00:00:22,150 --> 00:00:25,450
the complex link between attitudes and behavior,

6
00:00:25,450 --> 00:00:29,550
the power of the situation (which sometimes goes unappreciated when 

7
00:00:29,550 --> 00:00:34,000
we rush to make dispositional, or character judgments, of other people),

8
00:00:34,000 --> 00:00:39,300
and the value of empathy as an antidote to many of the problems we face.

9
00:00:39,300 --> 00:00:42,000
I hope that you've also seen that social psychology is a 

10
00:00:42,000 --> 00:00:46,350
branch of psychology and science that isn't just common sense.

11
00:00:46,350 --> 00:00:49,500
In fact, this would be a great time to review all of your answers in the

12
00:00:49,500 --> 00:00:55,000
Snapshot Quiz, which you can do by clicking on a button from our main course page.

13
00:00:55,000 --> 00:00:58,680
If you didn't get all of the items correct with high confidence, that suggests

14
00:00:58,680 --> 00:01:03,100
the value of doing research on the topics that were covered on the quiz.

15
00:01:03,100 --> 00:01:06,470
My hope is that you've left the course with some growth in

16
00:01:06,470 --> 00:01:10,570
your psychology expertise and some information that will enrich your life,

17
00:01:10,570 --> 00:01:13,290
your relationships, and your work.

18
00:01:13,290 --> 00:01:17,230
And don't forget that those of you who earn a Statement of Accomplishment 

19
00:01:17,230 --> 00:01:21,700
and have a page in Social Psychology Network, will be able to upload the Statement 

20
00:01:21,700 --> 00:01:26,920
after the course, just as you'd upload a photo, and it'll appear on your page.

21
00:01:26,920 --> 00:01:30,900
This new feature of the Network was added by two brilliant programmers,

22
00:01:30,900 --> 00:01:34,930
David Jensenius and Steve Farthing, and I'd like to take this opportunity 

23
00:01:34,930 --> 00:01:38,200
to thank them again for their work, and to thank the TAs 

24
00:01:38,200 --> 00:01:42,860
and McGraw Hill, Dave Myers, and many other contributors to the course.

25
00:01:42,860 --> 00:01:46,900
I'd also like to thank several colleagues at Wesleyan, especially those

26
00:01:46,900 --> 00:01:51,400
in Information Technology Services and University Communications --

27
00:01:51,400 --> 00:01:55,100
people who offered technical support, hardware advice, 

28
00:01:55,100 --> 00:01:58,270
and even a few images and video clips.

29
00:01:58,270 --> 00:02:01,340
One of the most important contributors to the course was Mike Conte 

30
00:02:01,340 --> 00:02:05,800
of Wesleyan's Physical Plant. Mike single-handedly made it his mission 

31
00:02:05,800 --> 00:02:09,900
to make sure that I had a quiet comfortable place to record videos.

32
00:02:09,900 --> 00:02:13,420
Mike, thank you! Thank you, thank you, thank you!

33
00:02:13,420 --> 00:02:15,950
And as you can imagine, in a course of this size,

34
00:02:15,950 --> 00:02:19,300
there are several Coursera staff members who were involved.

35
00:02:19,300 --> 00:02:22,600
Here are just a few who made valuable contributions.

36
00:02:22,600 --> 00:02:26,380
These are some of the nicest, smartest people you can imagine.

37
00:02:26,380 --> 00:02:29,800
And of all the good folks at Coursera, special thanks go to 

38
00:02:29,800 --> 00:02:33,400
Eli Bildner and Andreina Parisi-Amon.

39
00:02:33,400 --> 00:02:37,750
Words fail to describe how much they've done for this course.

40
00:02:37,750 --> 00:02:41,500
As you can see, whether the sky is sunny or whether it's stormy, 

41
00:02:41,500 --> 00:02:45,100
they continue smiling. I don't know how they do it!

42
00:02:45,100 --> 00:02:49,910
It's been just an incredible honor working with Andreina and Eli.

43
00:02:49,910 --> 00:02:54,800
Please bear with me for one minute longer; I have just a few more words of thanks.

44
00:02:54,800 --> 00:02:59,620
First, to the two loves of my life: my wife Allison and daughter Fijare.

45
00:02:59,620 --> 00:03:02,190
I not only want to thank you, but to say publicly that 

46
00:03:02,190 --> 00:03:06,340
this course would never have been given without you.

47
00:03:06,340 --> 00:03:09,200
And finally, it never would have been given without the students 

48
00:03:09,200 --> 00:03:12,100
who enrolled in it -- students from roughly 200 countries 

49
00:03:12,100 --> 00:03:15,800
who said, "Sure, I'll sign up for Social Psychology."

50
00:03:15,800 --> 00:03:18,800
I want to thank you for spending your valuable time with me 

51
00:03:18,800 --> 00:03:21,700
and for your many contributions to the course.

52
00:03:21,700 --> 00:03:24,700
Some of you joined Social Psychology Network and helped cover

53
00:03:24,700 --> 00:03:28,200
the costs of the course. Some of you provided assistance to 

54
00:03:28,200 --> 00:03:32,080
other students who needed a helping hand. Some of you offered 

55
00:03:32,080 --> 00:03:35,800
corrections or suggestions on how the course might be improved.

56
00:03:35,800 --> 00:03:39,640
And some of you posted notes of appreciation in the discussion forums,

57
00:03:39,640 --> 00:03:43,000
which I personally appreciated quite a bit.

58
00:03:43,000 --> 00:03:47,400
So, thank you. It's been an honor serving as your instructor.

59
00:03:47,400 --> 00:03:50,720
And now I'd like to turn to one last topic -- 

60
00:03:50,720 --> 00:03:54,000
a question that's been appearing lately in the discussion forums.

61
00:03:54,000 --> 00:03:55,840
Where do we go from here?

62
00:03:55,840 --> 00:03:59,700
In other words, for those interested in learning more about social psychology,

63
00:03:59,700 --> 00:04:05,150
what's a good way forward? Let me offer three suggestions.

64
00:04:05,150 --> 00:04:08,130
First, you might consider reading a textbook on the topic -- 

65
00:04:08,130 --> 00:04:13,000
either a complete introductory textbook, like the ones authored by David Myers, 

66
00:04:13,000 --> 00:04:17,610
or a more specialized text on a topic of interest. As you can see here,

67
00:04:17,610 --> 00:04:23,570
Social Psychology Network has a page that lists over 200 textbooks to choose from --

68
00:04:23,570 --> 00:04:27,120
a page generously sponsored by Psychology Press Rutledge, 

69
00:04:27,120 --> 00:04:31,200
and linked directly from the navigation bar on the left.

70
00:04:31,200 --> 00:04:35,510
And if you're interested in the psychology of prejudice and social justice,

71
00:04:35,510 --> 00:04:40,000
the Reading Room of UnderstandingPrejuice.org has hundreds of 

72
00:04:40,000 --> 00:04:45,600
other textbooks, journals, magazines, newsletters, and so on.

73
00:04:45,600 --> 00:04:49,100
A second suggestion would be to re-watch the video from Week 1

74
00:04:49,100 --> 00:04:52,580
of the course, in which I give a tour of Social Psychology Network,

75
00:04:52,580 --> 00:04:56,450
and then, after you finish watching, explore some of the resources,

76
00:04:56,450 --> 00:05:00,000
such as the Online Psychology Career Center, all of which are

77
00:05:00,000 --> 00:05:05,500
free and open access. One last suggestion would be to join or 

78
00:05:05,500 --> 00:05:09,700
learn about professional organizations related to social psychology.

79
00:05:09,700 --> 00:05:14,380
And to help you do that, I invited all of the organizations that formally endorsed 

80
00:05:14,380 --> 00:05:18,750
Social Psychology Network to contribute a one-minute videotape 

81
00:05:18,750 --> 00:05:23,900
describing their organization and ways for interested students to join.

82
00:05:23,900 --> 00:05:27,470
Several of them kindly sent me a clip, so I'll end this video 

83
00:05:27,470 --> 00:05:31,100
by simply presenting what they sent. In some cases 

84
00:05:31,100 --> 00:05:35,280
the videos are informal, and the sound quality isn't always great, 

85
00:05:35,280 --> 00:05:39,220
so you may need to turn on  subtitles while you watch.

86
00:05:39,220 --> 00:05:44,100
But the organizations are really great. They're worth knowing about.

87
00:05:44,100 --> 00:05:47,500
All of them are becoming more international in various ways, 

88
00:05:47,500 --> 00:05:52,470
and I hope that they give you a taste of psychology beyond this course.

89
00:05:52,470 --> 00:05:57,600
Again, thanks so much for being part of it! Take care.

90
00:06:10,700 --> 00:06:13,637
In 1936, at the height of the Great Depression,

91
00:06:13,637 --> 00:06:16,650
a group of prominent psychologists came together to establish 

92
00:06:16,650 --> 00:06:21,550
the Society for Psychological Study of Social Issues, or SPSSI.

93
00:06:21,550 --> 00:06:24,570
Its goal: to help the public and its representatives craft

94
00:06:24,570 --> 00:06:28,790
social policies based on a scientific understanding of human behavior.

95
00:06:28,790 --> 00:06:32,440
From the calamities of the Depression, to groundbreaking work in the 1950s

96
00:06:32,440 --> 00:06:37,000
that helped end legal segregation in the United States, to the 1990s 

97
00:06:37,000 --> 00:06:40,490
when it first gained consultative status at the United Nations,

98
00:06:40,490 --> 00:06:44,615
to present-day research on gun violence and same-sex parenting, SPSSI has been 

99
00:06:44,615 --> 00:06:49,900
a powerful voice for social change based on sound psychological science.

100
00:06:49,900 --> 00:06:53,110
We provide briefings and special reports to Congress,

101
00:06:53,110 --> 00:06:57,500
sponsor policy workshops and advocacy days for our members on Capitol Hill,

102
00:06:57,500 --> 00:07:03,020
and create and fund opportunities for psychologists to engage the political process.

103
00:07:03,020 --> 00:07:05,860
SPSSI also provides dozens of awards and grants each year 

104
00:07:05,860 --> 00:07:10,470
to everyone from undergraduates to late career academics, and publishes 

105
00:07:10,470 --> 00:07:15,990
research in three journals, including the flagship Journal of Social Issues.

106
00:07:15,990 --> 00:07:21,670
We encourage you to explore SPSSI at our website -- www.spssi.org --

107
00:07:21,670 --> 00:07:25,600
and through our Facebook and Twitter accounts.

108
00:07:42,100 --> 00:07:44,500
Community psychologists work to increase the capacity of

109
00:07:44,500 --> 00:07:47,236
communities to address critical problems and support 

110
00:07:47,236 --> 00:07:50,700
the empowerment of marginalized people as partners in research.

111
00:07:50,700 --> 00:07:54,100
They use scientific methods to study how programs and policies 

112
00:07:54,100 --> 00:07:58,576
are implemented, and to evaluate their impacts.

113
00:07:58,576 --> 00:08:01,000
>> They work to improve systems and develop programs 

114
00:08:01,000 --> 00:08:05,000
that build on strength and cultural values. They work in universities, 

115
00:08:05,000 --> 00:08:11,160
government agencies, nonprofits, and consulting firms.

116
00:08:11,160 --> 00:08:14,500
>> Community psychology seeks to reduce oppression through 

117
00:08:14,500 --> 00:08:18,730
research, advocacy, organizing, and policy change.

118
00:08:18,730 --> 00:08:26,200
Learn more about it at scra27.org and discover how you can make a difference.

119
00:08:33,192 --> 00:08:36,091
Industrial-Organizational Psychology helps take care of business 

120
00:08:36,091 --> 00:08:40,000
by using scientific methods to improve organizational performance 

121
00:08:40,000 --> 00:08:44,000
and employee well-being. Here are a few examples...

122
00:08:46,444 --> 00:08:50,300
>> Hi! I am Alexandra Whitmire with Wylie here at NASA Johnson Space Center,

123
00:08:50,300 --> 00:08:53,850
and I help manage research to support astronauts' behavior, health, 

124
00:08:53,850 --> 00:08:59,200
and performance for NASA's next generation space flight missions.

125
00:08:59,666 --> 00:09:04,100
>> I develop tests and simulations of jobs that companies use to hire  

126
00:09:04,100 --> 00:09:09,000
and promote people. Scores from these tests can help a company consistently

127
00:09:09,000 --> 00:09:15,100
differentiate between people who'd perform a job well and those who wouldn't.

128
00:09:15,100 --> 00:09:20,100
>> I'm Ed Sabin. I do research with pilots and air traffic controllers to make aviation safer.

129
00:09:20,100 --> 00:09:27,000
We try to understand the causes of accidents to prevent them, because safety is no accident.

130
00:09:27,400 --> 00:09:33,700
>> To learn more about I-O psychology, please visit SIOP.org.

131
00:09:39,621 --> 00:09:41,220
>> This is Mercedes McCormick of Psi Chi

132
00:09:41,220 --> 00:09:46,220
and the incoming President of APA Division of International Psychology. 

133
00:09:46,220 --> 00:09:50,120
>> And I'm Harold Takooshian, the Past-President of the APA International Division, 

134
00:09:50,120 --> 00:09:54,510
and of Psi Chi, the International Honors Society in Psychology.

135
00:09:54,510 --> 00:09:58,500
>> If you are a psychology student or professor working in a school 

136
00:09:58,500 --> 00:10:02,400
outside the United States, we have some wonderful news for you.

137
00:10:02,400 --> 00:10:07,000
>> First, since Psi Chi began at Yale University in 1929,

138
00:10:07,000 --> 00:10:10,000
it has grown into the world's largest honor society, 

139
00:10:10,000 --> 00:10:15,000
with 600,000 members at 11,000 schools across the USA.  

140
00:10:15,000 --> 00:10:19,000
Psi Chi promotes student excellence through a rich program of awards, 

141
00:10:19,000 --> 00:10:23,400 
conferences, publications, local campus activities, 

142
00:10:23,400 --> 00:10:28,290
and $300,000 each year in grants to students and their professors.

143
00:10:28,290 --> 00:10:31,800
>> Second, since 2009, students and professors

144
00:10:31,800 --> 00:10:35,850
outside the United States can now join the Psi Chi family. 

145
00:10:35,850 --> 00:10:41,000
Not only do students benefit, but their professor's work becomes easier 

146
00:10:41,000 --> 00:10:45,740
when their students become more active in their own education.

147
00:10:45,740 --> 00:10:50,130
Instructions to start a chapter appear at the website.

148
00:10:50,130 --> 00:10:54,090
>> You can also find more details on YouTube in at least a few languages: 

149
00:10:54,090 --> 00:10:57,770
Spanish, Russian, English, and others coming soon.

150
00:10:57,770 --> 00:11:01,200
Contact us soon. Thank you.

151
00:11:05,900 --> 00:11:07,800
>> Hello, budding social psychologists!

152
00:11:07,800 --> 00:11:10,080
My name is Nick Haslam, and I'm the President-Elect of

153
00:11:10,080 --> 00:11:14,400
the Society of Australasian Social Psychologists, or "SASP" for short.

154
00:11:14,400 --> 00:11:17,120
SASP is the body that represents the thriving community of

155
00:11:17,120 --> 00:11:20,400
social psychologists down under in Australia and New Zealand.

156
00:11:20,400 --> 00:11:23,440
What we try to do is promote the field in our area and also 

157
00:11:23,440 --> 00:11:27,810
build international collaborations. SASP runs a great conference.

158
00:11:27,810 --> 00:11:32,488
We run a variety of prizes. We publish a newsletter.

159
00:11:32,488 --> 00:11:35,680
We're particularly proud of how we support student work.

160
00:11:35,680 --> 00:11:39,590
So, we give research grants to graduate student projects.

161
00:11:39,590 --> 00:11:42,970
We subsidize grad students' travel to our conference.

162
00:11:42,970 --> 00:11:46,610
And the grad students are a very, very active part of our organization.

163
00:11:46,610 --> 00:11:49,470
The best part of all is we run a summer school every two years, 

164
00:11:49,470 --> 00:11:52,850
where students from the region -- and also from around the world --

165
00:11:52,850 --> 00:11:56,140
get together to do work with eminent social psychologists,

166
00:11:56,140 --> 00:11:59,100
usually close to a terrific beach -- it's not all work.

167
00:11:59,100 --> 00:12:04,000
So, if you're in the vicinity and would like to continue your social psychology education, 

168
00:12:04,000 --> 00:12:08,320
or if you'd like to come down here and join us, think about joining SASP.

169
00:12:08,320 --> 00:12:15,100
At least check us out -- our website is www.sasp.org.au.

170
00:12:15,100 --> 00:12:18,000
Enjoy your course!

