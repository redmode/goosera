






1
00:00:07,050 --> 00:00:11,740
In this video, I'd like to comment on 
Stanley Milgram's documentary, Obedience, 

2
00:00:11,740 --> 00:00:15,660
and update the research a little bit. 
So, if you haven't seen the documentary 

3
00:00:15,660 --> 00:00:21,290
yet, please do that first and return to 
this video afterwards. 

4
00:00:21,290 --> 00:00:22,770
Really. 
Okay? 

5
00:00:22,770 --> 00:00:25,630
Alright. 
So, here are the topics that I'd like to 

6
00:00:25,630 --> 00:00:29,430
discuss. 
First, the context in which Milgram's 

7
00:00:29,430 --> 00:00:34,189
studies took place. 
Second, ethical issues related to the 

8
00:00:34,189 --> 00:00:38,090
experiments. 
Third, contemporary research on 

9
00:00:38,090 --> 00:00:42,210
obedience. 
Fourth, some resources for learning more 

10
00:00:42,210 --> 00:00:44,990
about Milgram's research -- books, 
websites, and so on. 

11
00:00:44,990 --> 00:00:49,340
And finally, a funny example of obedience 
to authority. 

12
00:00:51,050 --> 00:00:55,020
So, about context: 
Milgram's research on obedience took 

13
00:00:55,020 --> 00:01:00,090
place in the 1960's, a decade that was 
famous in the United States and many 

14
00:01:00,090 --> 00:01:04,280
other countries for people questioning 
authority, 

15
00:01:04,280 --> 00:01:08,460
but there was also a historical event 
that Milgram was particularly interested 

16
00:01:08,460 --> 00:01:15,175
in: the Holocaust in Nazi Germany. 
Stanley Milgram was the son of Jewish 

17
00:01:15,175 --> 00:01:20,330
immigrants, and he explicitly designed 
these experiments in order to understand 

18
00:01:20,330 --> 00:01:23,930
the kind of obedience that was prevalent 
during the days of Hitler. 

19
00:01:25,350 --> 00:01:29,550
Before the Milgram studies, most people 
regarded the Holocaust as something 

20
00:01:29,550 --> 00:01:35,070
uniquely German, that there was something 
about the German character that led to 

21
00:01:35,070 --> 00:01:38,800
these atrocities. 
In essence, what Milgram showed was that 

22
00:01:38,800 --> 00:01:42,640
people might be committing the 
fundamental attribution error, 

23
00:01:42,640 --> 00:01:46,770
that they were making dispositional 
attributions when they should have at 

24
00:01:46,770 --> 00:01:51,440
least considered situational or 
environmental factors. 

25
00:01:51,440 --> 00:01:56,450
Milgram showed, to almost everyone's 
astonishment, including his own, that even 

26
00:01:56,450 --> 00:02:01,170
the most decent and kindhearted of 
Americans were capable of killing or 

27
00:02:01,170 --> 00:02:07,410
injuring another person on command. 
In other words, he showed that the 

28
00:02:07,410 --> 00:02:12,715
psychology of the Holocaust might not be 
uniquely German, and that given the right 

29
00:02:12,715 --> 00:02:16,330
situation, another Holocaust could happen 
elsewhere. 

30
00:02:17,400 --> 00:02:22,220
In fact, there was a replication in Germany 
of Milgram's research, and the levels of 

31
00:02:22,220 --> 00:02:26,860
obedience found were not significantly 
different than those that Milgram himself 

32
00:02:26,860 --> 00:02:31,840
found in the United States. 
Now, of course, the Holocaust is very 

33
00:02:31,840 --> 00:02:35,320
different than a laboratory experiment, 
and Milgram was mindful of this, 

34
00:02:35,320 --> 00:02:40,680
but there are features of the experiment 
that do resemble situations which an 

35
00:02:40,680 --> 00:02:46,030
authority figure asks somebody to violate 
someone else's rights, or even ask them 

36
00:02:46,030 --> 00:02:50,780
to commit an atrocity. 
The metaphor that Milgram used in making 

37
00:02:50,780 --> 00:02:55,900
this comparison is the difference between 
a burning match, which is what took place 

38
00:02:55,900 --> 00:03:02,680
in his laboratory, and the Chicago fire of 
1871, which was a citywide fire of 

39
00:03:02,680 --> 00:03:07,120
historic dimensions. 
The equivalence between the two is only 

40
00:03:07,120 --> 00:03:10,260
in that they involve the same basic 
process: 

41
00:03:10,260 --> 00:03:14,910
combustion in the case of the match, or 
obedience to authority in the case of 

42
00:03:14,910 --> 00:03:19,470
Milgram's research. 
Milgram was never trying to say that the 

43
00:03:19,470 --> 00:03:23,888
Holocaust and the laboratory experiment 
are somehow interchangeable or 

44
00:03:23,888 --> 00:03:27,469
indistinguishable -- that was certainly not 
his point. 

45
00:03:27,469 --> 00:03:32,510
And as I mentioned in an earlier video, 
it's also important to understand that 

46
00:03:32,510 --> 00:03:36,230
people did try to resist the 
experimenter; it's just that the 

47
00:03:36,230 --> 00:03:39,840
objections that they voiced were often 
ineffective. 

48
00:03:39,840 --> 00:03:45,620
And even when they did disobey and stop 
the experiment, it didn't happen in the 

49
00:03:45,620 --> 00:03:50,990
way we might expect. 
A 2008 meta-analysis found that 

50
00:03:50,990 --> 00:03:55,870
disobedience did not increase as the 
electric shocks appeared to become more 

51
00:03:55,870 --> 00:03:59,130
painful. 
Instead, the most common point at which 

52
00:03:59,130 --> 00:04:04,670
people stopped was at a 150 volts, when 
the learner first asked to be released. 

53
00:04:04,670 --> 00:04:10,410
If participants didn't stop at that 
point, chances are they went all the way. 

54
00:04:10,410 --> 00:04:15,020
This was true for both women and men, for 
people in the United States as well as 

55
00:04:15,020 --> 00:04:19,500
other countries, for people of different 
ages and personalities. 

56
00:04:19,500 --> 00:04:23,960
And in fact, out of hundreds of people 
who participated in Milgram's research 

57
00:04:23,960 --> 00:04:28,460
from start to finish, the number who 
looked in on the learner in the next room 

58
00:04:28,460 --> 00:04:32,955
after he stopped responding, just to see 
if he had a heart attack or needed help, 

59
00:04:32,955 --> 00:04:37,230
was zero. 
It simply never happened. 

60
00:04:38,710 --> 00:04:42,700
The question many people have asked since 
the time of Milgram's research is whether 

61
00:04:42,700 --> 00:04:47,150
the knowledge gained was worth the price 
that Milgram's participants paid, 

62
00:04:47,150 --> 00:04:52,420
whether it was ethical to put innocent 
people in such a stressful situation -- 

63
00:04:52,420 --> 00:04:55,900
a situation where they might learn things 
about themselves that they would rather 

64
00:04:55,900 --> 00:04:59,510
not know. 
In an online course, it's not possible to 

65
00:04:59,510 --> 00:05:04,340
have a classroom discussion about this 
topic, but it's certainly possible to 

66
00:05:04,340 --> 00:05:08,440
participate in the discussion forums. 
So, I very much hope that you'll take 

67
00:05:08,440 --> 00:05:14,140
this opportunity to post a comment, 
comment on somebody else's post, ask a 

68
00:05:14,140 --> 00:05:16,990
question, and so forth, 
so that you can learn from each other, 

69
00:05:16,990 --> 00:05:20,580
and so that the course isn't a spectator 
sport. 

70
00:05:20,580 --> 00:05:24,540
All I ask is that you observe the ground 
rules that we discussed earlier -- 

71
00:05:24,540 --> 00:05:28,830
that is, don't flame people when you 
disagree with their point of view. 

72
00:05:28,830 --> 00:05:32,690
If you see comments that violate our 
course guidelines, simply flag the 

73
00:05:32,690 --> 00:05:36,790
comment as inappropriate, but for everything else, try to find a 

74
00:05:36,790 --> 00:05:40,990
respectful way to share your thoughts so 
that we can create a supportive learning 

75
00:05:40,990 --> 00:05:44,090
community. 
The other thing that I wanted to mention 

76
00:05:44,090 --> 00:05:48,880
about ethics is that, these days, 
university research involving human 

77
00:05:48,880 --> 00:05:53,920
participants typically has to receive 
approval from an Institutional Review 

78
00:05:53,920 --> 00:05:59,420
Board -- an IRB -- or an ethics committee, 
although the details differ from country 

79
00:05:59,420 --> 00:06:03,490
to country. 
Researchers also have to obtain informed 

80
00:06:03,490 --> 00:06:07,750
consent from participants (or their 
guardians if they are not of age or not 

81
00:06:07,750 --> 00:06:11,610
capable), which doesn't mean that you need 
to tell participants what the 

82
00:06:11,610 --> 00:06:15,490
experimental hypothesis is. 
You just need to give them enough 

83
00:06:15,490 --> 00:06:20,410
information to make an informed judgment 
whether to participate or not. 

84
00:06:20,410 --> 00:06:25,330
So, for example, the consent form 
might say, "I understand that in this 

85
00:06:25,330 --> 00:06:28,850
study, I may be asked to punish another 
person. 

86
00:06:28,850 --> 00:06:32,295
I may experience a high level of 
emotional stress. 

87
00:06:32,295 --> 00:06:36,510
I may learn things about myself that I 
would rather not know." 

88
00:06:36,510 --> 00:06:40,990
Safeguards like informed concern were 
implemented in the mid-1970's in the 

89
00:06:40,990 --> 00:06:44,660
United States, 
and by the mid-80's obedience research 

90
00:06:44,660 --> 00:06:49,712
using Milgram's experimental procedure 
had pretty much ended worldwide, leaving 

91
00:06:49,712 --> 00:06:54,140
open the question whether obedience 
levels are any different today than they were 

92
00:06:54,140 --> 00:07:00,110
when Milgram conducted his experiments. 
In his biography of Stanley Milgram, 

93
00:07:00,110 --> 00:07:04,440
Professor Tom Blass compiled a list of 
all obedience levels recorded in 

94
00:07:04,440 --> 00:07:11,120
experiments published between 1963 and 
1985, and there was no evidence of a 

95
00:07:11,120 --> 00:07:16,160
decline. 
But what about between 1985 and now? 

96
00:07:16,160 --> 00:07:20,570
Is there still no change? 
Let's pause for a pop-up question where 

97
00:07:20,570 --> 00:07:25,140
you can take a guess, and then I'll share 
the results of a very clever experiment 

98
00:07:25,140 --> 00:07:26
that offers an answer. 


99
00:07:29,50 --> 00:07:36,430
The best available evidence suggests that if Milgram's research could be conducted 
today, the results  

100
00:07:36,430 --> 00:07:39,970
would be essentially the same as what Milgram found over a 
half century ago. 

101
00:07:39,970 --> 00:07:45,330
I say this not only because obedience 
levels didn't change for 25 years after 

102
00:07:45,330 --> 00:07:49,650
he published his work, but because 
another social psychologist, Jerry 

103
00:07:49,650 --> 00:07:56,460
Burger, published a replication in 2009, 
and his results were remarkably similar 

104
00:07:56,460 --> 00:08:00,050
to what Milgram found. 
Now, how did Jerry Burger manage to 

105
00:08:00,050 --> 00:08:05,542
conduct a replication given all the 
regulations that currently limit this 

106
00:08:05,542 --> 00:08:10,410
sort of research? 
Well, he realized that the 150 volt level 

107
00:08:10,410 --> 00:08:16,110
was something of a cliff, because nearly 
80% of participants who went past that 

108
00:08:16,110 --> 00:08:20,600
point continued all the way to the 
highest shock level. 

109
00:08:20,600 --> 00:08:25,680
So, by ending the experiment after the 
150 volt level, Professor Burger could 

110
00:08:25,680 --> 00:08:30,430
estimate what the results probably would 
be without actually putting people 

111
00:08:30,430 --> 00:08:32,610
through the most stressful part of the 
procedure, 

112
00:08:32,610 --> 00:08:38,950
and that modification, along with a few 
other changes, permitted the study to go 

113
00:08:38,950 --> 00:08:42,700
forward. 
The details are given in one of this 

114
00:08:42,700 --> 00:08:46,310
week's assigned readings, but I thought 
you still might like to watch some 

115
00:08:46,310 --> 00:08:51,950
excerpts from a very good ABC Primetime 
TV program describing the replication. 

116
00:08:51,950 --> 00:08:56,790
In fact, ABC News generously custom-edited 
the following segments 

117
00:08:56,790 --> 00:08:59,050
specifically for our class. 
Let's watch. 

118
00:09:11,310 --> 00:09:14,780
 >> Imagine this scenario: 
You go to a prestigious university to 

119
00:09:14,780 --> 00:09:17,600
participate in a learning and memory 
experiment. 

120
00:09:17,600 --> 00:09:21,320
When you arrive, you discover that the 
teaching instrument is this machine, 

121
00:09:21,320 --> 00:09:25,640
which seems to give electroshocks to a 
man on the other side of the wall. 

122
00:09:25,640 --> 00:09:28,060
 >> As you move up the scale, he begins 

123
00:09:28,060 --> 00:09:31,020
to scream out in pain. 
 >> The experiment requires that you 

124
00:09:31,020 --> 00:09:34,450
continue. 
 >> The experimenter pressures you to go on. 

125
00:09:34,450 --> 00:09:37,150
Would you agree to continue with the 
experiment? 

126
00:09:37,150 --> 00:09:38,330
 >> 
That's all. 

127
00:09:38,330 --> 00:09:41,740
 >> To find out, we teamed up with 
Dr. Jerry Burger, a social 

128
00:09:41,740 --> 00:09:45,140
psychologist at Santa Clara University in 
California. 

129
00:09:45,140 --> 00:09:48,970
 >> People have often asked this 
question about would we find these kinds 

130
00:09:48,970 --> 00:09:51,640
of results today? 
And some people try to dismiss the 

131
00:09:51,640 --> 00:09:55,210
Milgram findings by saying, "That's 
something that happened back in the 60's; 

132
00:09:55,210 --> 00:10:00,110
people aren't like that anymore." [MUSIC] 
 >> We're about to find out if people 

133
00:10:00,110 --> 00:10:02,990
have really changed. 
 >> I'm a teacher. Oh, man! 

134
00:10:02,990 --> 00:10:05,960
[LAUGH] Okay. 
 >> 39-year-old Troy Shasker is an 

135
00:10:05,960 --> 00:10:09,550
electrician. 
He's been paid $50 to participate, and 

136
00:10:09,550 --> 00:10:14,540
told that the money is his to keep even 
if he quits the experiment early. 

137
00:10:14,540 --> 00:10:17,192
He's worried about the dangers of the 
electroshock machine. 

138
00:10:17,192 --> 00:10:20,700
 >> Wow! I don't think, 
I don't think I should shock 

139
00:10:20,700 --> 00:10:22
him that hard if he really does screw up.

140
00:10:21,917 --> 00:10:23,665
 >> It's, it's severe shock there. 
 >> Yeah, there are 25. 

141
00:10:23,665 --> 00:10:29,30
 >> I mean, I could just go get my shotgun. 
 >> In the room next door, Troy watches 

142
00:10:29,30 --> 00:10:32,510
as the learner gets strapped into his 
chair. 

143
00:10:32,510 --> 00:10:36,130
His anxiety rises when he hears the next 
scripted line. 

144
00:10:36,130 --> 00:10:38,640
 >> I should probably bring up that 
couple of years ago at Kaiser, they 

145
00:10:38,640 --> 00:10:42,450
diagnosed a mild heart condition. 
I'm really not too worried about it, it's 

146
00:10:42,450 --> 00:10:45,580
not that serious, but...
 >> Well, you should know that while the 

147
00:10:45,580 --> 00:10:49,591
shocks that we'll be using today may be 
painful, they're not dangerous. 

148
00:10:49,591 --> 00:10:52,200
 >> Okay. 
 >> Milgram intended that exchange to 

149
00:10:52,200 --> 00:10:56,330
set up a conflict in the subject's mind: 
a choice between the health of the 

150
00:10:56,330 --> 00:10:59,750
learner and the authority of the 
experimenter. 

151
00:10:59,750 --> 00:11:04,083
Now, Troy starts reading Ken the word 
pairs he's supposed to memorize. 

152
00:11:04,083 --> 00:11:07,920
 >> Blue, girl. 
Nice, day. 

153
00:11:07,920 --> 00:11:12,020
Fat, neck. 
Number one. 

154
00:11:12,020 --> 00:11:20,570
 >> Then, the test begins. 
 >> Blue, boy, girl, grass, hat. 

155
00:11:20,570 --> 00:11:24,240
 >> The learner must decide which of the 
four words is the correct match. 

156
00:11:24,240 --> 00:11:28,610
At first, everything goes smoothly. 
 >> Correct. 

157
00:11:28,610 --> 00:11:33,080
I was confident that he was doing really 
good at first, and then it started 

158
00:11:33,080 --> 00:11:37,830
looking bad [LAUGH]. 
 >> At 75 volts, Troy hears the first 

159
00:11:37,830 --> 00:11:44,404
sign of trouble. 
 >> Soft, rug, pillow, hair, grass. 

160
00:11:44,404 --> 00:11:49,126
[SOUND] Incorrect. 
75 volts. 

161
00:11:49,126 --> 00:11:54,390
[SOUND]. 
I could actually hear him next door going, 

162
00:11:54,390 --> 00:11:57,186
"Oww! Ugh!" And he kept getting things wrong. 
[SOUND] Incorrect. 

163
00:11:57,186 --> 00:12:07,830
90 volts. 
 >> At 105 volts, he's clearly uneasy. 

164
00:12:07,830 --> 00:12:10,730
 >> I got a little moist on my forehead 
there. 

165
00:12:10,730 --> 00:12:12,950
I wasn't comfortable. 
 >> Mm-hm. 

166
00:12:12,950 --> 00:12:16,780
 >> I do not, I can't tell you why I, 
why I listened to him and kept going. 

167
00:12:16,780 --> 00:12:22,360
I should have just said no. 
 >> [SOUND] The correct word was duck. 

168
00:12:22,360 --> 00:12:26,820
 >> Near the end, he's almost willing 
Ken to learn the right word pairs through 

169
00:12:26,820 --> 00:12:29,042
the wall. 
 >> Horse [SOUND]. 

170
00:12:29,042 --> 00:12:36,174
Incorrect. 
135 volts. 

171
00:12:36,174 --> 00:12:38,305
[SOUND] 
 >> Troy starts laughing nervously. 

172
00:12:38,305 --> 00:12:42,191
 >> The correct word was woman. 
Rock, 

173
00:12:42,191 --> 00:12:47,327
house. 
 >> At 150 volts... 

174
00:12:47,327 --> 00:12:49,896
[SOUND] 
 >> Incorrect. 

175
00:12:49,896 --> 00:12:53,340
150 volts. 
 
176
00:12:53,340 --> 00:12:55,760
 >> Ah! That's all! 
Get me out of here! 

177
00:12:55,760 --> 00:12:59,490
I told you I have heart troubles! 
 >> Will Troy listen as the learner begs 

178
00:12:59,490 --> 00:13:02,840
him to stop, or will he follow Brian's 
orders? 

179
00:13:02,840 --> 00:13:04,900
 >> Tell him the correct word pair, 
white. 

180
00:13:04,900 --> 00:13:07,315
 >> The correct word was white horse. 
 >> And the next item? Please continue.  

181
00:13:07,315 --> 00:13:13,975
 >> The next item is sad. 
 >> He obeys the orders. 

182
00:13:13,975 --> 00:13:16,512
 >> Face. 
 >> Why didn't you stop? 

183
00:13:16,512 --> 00:13:21,190
 >> The strap, I saw him getting strapped 
in, and they were just like little, 

184
00:13:22,190 --> 00:13:24,950
I mean he could've just -- if he was in 
that much pain, he could have just tore 

185
00:13:24,950 --> 00:13:27,330
himself off. 
 >> Why are you putting it on him and 

186
00:13:27,330 --> 00:13:32,210
not you or the experimenter? 
 >> I was just doing my job [LAUGH]. 

187
00:13:32,210 --> 00:13:36,010
I was doing what I was supposed to do. 
75 volts. 

188
00:13:36,010 --> 00:13:40,250
So, I guess the influence of having the 
conductor of the experiment right 

189
00:13:40,250 --> 00:13:44,200
there next to me and telling me to keep 
going had a lot to do with it. 

190
00:13:44,200 --> 00:13:49,640
 >> Cool, day. 
 >> We tested 18 men and 22 women. 

191
00:13:49,640 --> 00:13:54,279
 >> Very often the first time they hear a 
noise from the other room, 

192
00:13:54,279 --> 00:13:55,731
 >> [SOUND] 
 >> Wrong. 

193
00:13:55,731 --> 00:13:58,920
90 volts. 
 >> Ah! 

194
00:13:58,920 --> 00:14:00,270
 >> the typical response 
 >> Ah! Get me out of here, please! 

195
00:14:00,270 --> 00:14:05,050
 >> is to turn toward the experimenter,
and if not say something, at least 

196
00:14:05,050 --> 00:14:10,589
give a look that says, "What should I do?" 
And of course, when an expert tells them, 

197
00:14:10,589 --> 00:14:14,960
"Not a problem. This is nothing to worry 
about. Continue." 

198
00:14:14,960 --> 00:14:18,350
the rational thing to do in that 
situation is to continue. 

199
00:14:18,350 --> 00:14:20,880
 >> He's not a finger in the face, he's 
not a drill sergeant. 

200
00:14:20,880 --> 00:14:25,080
 >> No, you don't have to be threatening. 
The power that he has in this situation 

201
00:14:25,080 --> 00:14:29,950
comes in part because he's an authority 
figure, and we're all trained a little 

202
00:14:29,950 --> 00:14:33,460
bit to obey authority figures, 
but also he's the expert in the 

203
00:14:33,460 --> 00:14:36,110
situation. 
He's the one that knows about this 

204
00:14:36,110 --> 00:14:40,500
machine. 
 >> [SOUND] Incorrect. 105 volts. 

205
00:14:40,500 --> 00:14:46,220
In the end, almost two-thirds of the men 
agreed to administer the highest shock. 

206
00:14:46,220 --> 00:14:50,380
For the past 30 years, there've been 
severe restrictions on using humans in 

207
00:14:50,380 --> 00:14:54,160
social psychology research. 
To avoid putting subjects under too much 

208
00:14:54,160 --> 00:14:58,150
stress, Dr. Burger made a significant 
change to our experiment. 

209
00:14:58,150 --> 00:15:01,930
 >> In this experiment, you stopped at 
150 make-believe volts. 

210
00:15:01,930 --> 00:15:05,280
In Milgram, they went much higher. 
 >> We stopped for ethical reasons. 

211
00:15:05,280 --> 00:15:09,360
We couldn't put people through the agony 
that Milgram's participants went through. 

212
00:15:09,360 --> 00:15:14,650
When we look back at Milgram's data, what 
we find is that point, that 150-volt 

213
00:15:14,650 --> 00:15:18,744
point that we stopped at, is something of 
a point of no return. 

214
00:15:18,744 --> 00:15:24,040
 >> This says 150 volts. Deep breath. 

215
00:15:24,040 --> 00:15:28,279
 >> That's all! Get me out of here! 
I told you I had heart trouble. My 

216
00:15:28,279 --> 00:15:32,100
heart's starting to bother me now. 
Get me out of here, please! 

217
00:15:32,100 --> 00:15:35,920
My heart's starting to bother me. 
I refuse to go on. 

218
00:15:35,920 --> 00:15:39,350
Let me out! 
 >> Well... 

219
00:15:39,350 --> 00:15:40,830
 >> Please go on. The next item is sad. 

220
00:15:42,650 --> 00:15:45,620
 >> Okay. 
Now, he said that he had a heart problem. 

221
00:15:47,100 --> 00:15:49,990
 >> Remember, while the shock may be 
painful to him, they're not dangerous. 

222
00:15:49,990 --> 00:15:53,490
We know that. 
 >> Okay, we're aware of that, and there's 

223
00:15:53,490 --> 00:15:57,330
not going to be any lawsuit from 
this medical facility, right? 

224
00:15:57,330 --> 00:15:59,514
 >> Well, if anything happens to him, I am 
responsible. 

225
00:15:59,514 --> 00:16:01,854
 >> That's what I needed to know. 
 >> Okay. 

226
00:16:01,854 --> 00:16:05,930
 >> We began to notice a pattern: the 
majority of people who continued to 

227
00:16:05,930 --> 00:16:10,580
follow orders refused to take 
responsibility for the learner's safety. 

228
00:16:10,580 --> 00:16:15,680
 >> It's not my responsibility. You all, 
you know, brought this. We're volunteers. 

229
00:16:15,680 --> 00:16:19,210
 >> It's your controlled lab experiment. 
 >> I see. 

230
00:16:19,210 --> 00:16:23,000
Okay... 
 >> I'm just a conduit for you. 

231
00:16:23,000 --> 00:16:27,420
 >> Well, I just flipped the switch. 
I mean, he chose to be there himself 

232
00:16:27,420 --> 00:16:29,140
to take the shocks. 
 >> Mm-hmm. 

233
00:16:29,140 --> 00:16:37,600
 >> And that was his choice. 
 >> So the bottom line 

234
00:16:37,600 --> 00:16:42,005
is that statements like, "I was just 
following orders," or "It's not my 

235
00:16:42,005 --> 00:16:45,150
responsibility" are still very much with 
us. 

236
00:16:45,150 --> 00:16:50,210
They're not historical relics from the 
Holocaust or from Milgram's research in 

237
00:16:50,210 --> 00:16:54,860
the 1960's. 
Here's how Milgram himself summed up the 

238
00:16:54,860 --> 00:17:00,260
main lesson from his research. 
He wrote, "Perhaps the most fundamental 

239
00:17:00,260 --> 00:17:05,970
lesson of our study: ordinary people, 
simply doing their jobs, and without any 

240
00:17:05,970 --> 00:17:11,390
particular hostility on their part, can 
become agents in a terribly destructive 

241
00:17:11,390 --> 00:17:16,310
process." 
If he's right that ordinary people can 

242
00:17:16,310 --> 00:17:20,870
kill or injure another person on command, 
just doing their job, they don't know the 

243
00:17:20,870 --> 00:17:24,100
other person, they don't know the 
authority figure, 

244
00:17:24,100 --> 00:17:28,670
what this lesson implies is that a future 
Holocaust could happen if we're not 

245
00:17:28,670 --> 00:17:32,695
careful. 
Stanley Milgram died of a heart attack in 

246
00:17:32,695 --> 00:17:37,580
1984 at the age of 51, 
but the legacy of his research lives on, 

247
00:17:37,580 --> 00:17:41,320
and if you are interested in learning 
more about it, a very good place to start 

248
00:17:41,320 --> 00:17:44,999
is the StanleyMilgram.com website. 
You might also get a copy of the Stanley 

249
00:17:44,999 --> 00:17:52,430
Milgram biography mentioned in an earlier 
video, as well as these books by Stanley 

250
00:17:52,430 --> 00:17:56,260
Milgram and Tom Blass. 
All of these books are terrific. 

251
00:17:56,260 --> 00:18:00,970
Another good source of information is 
Wikipedia, which has detailed entries on 

252
00:18:00,970 --> 00:18:05,690
both Stanley Milgram and Milgram 
experiment. 

253
00:18:05,690 --> 00:18:10,680
And there's also a two minute bonus video 
accessible to members of our class, in 

254
00:18:10,680 --> 00:18:14,869
which Stanley Milgram talks about the 
famous electric shock generator. 

255
00:18:16,300 --> 00:18:20,710
Whatever became of the shock generator? 
It's on display at the Center for the 

256
00:18:20,710 --> 00:18:25,980
History of Psychology in Akron, Ohio. 
For those of you who live too far away 

257
00:18:25,980 --> 00:18:30,400
from Ohio to visit the Center (which, by 
the way, is a great place), 

258
00:18:30,400 --> 00:18:33,550
I recently traveled there and took a few 
photos. 

259
00:18:33,550 --> 00:18:36,680
Here's what the Center looks like as you 
enter. 

260
00:18:36,680 --> 00:18:40,910
Here's the shock generator along with 
some of the straps and wires that made it 

261
00:18:40,910 --> 00:18:44,370
so real. 
And here's a close up of the switches. 

262
00:18:44,370 --> 00:18:48,890
Just imagine if it were your finger on 
those switches. 

263
00:18:48,890 --> 00:18:53,300
What would you do? 
It's a difficult question, and one that I 

264
00:18:53,300 --> 00:18:58,250
hope you'll discuss in the class forums. 
Meanwhile, let's end this video on a 

265
00:18:58,250 --> 00:19:02,800
lighter note with an entertaining example 
of people obeying the instructions of an 

266
00:19:02,800 --> 00:19:05,954
authority figure, 
but instead of instructions to harm a 

267
00:19:05,954 --> 00:19:11,680
stranger, instructions to marry a 
stranger. Take a look. 

268
00:19:22,950 --> 00:19:24,950
>> We asked Charlie to help us make a 

269
00:19:24,950 --> 00:19:28,875
wedding video with a difference. 
The happy couple are actors. 

270
00:19:28,875 --> 00:19:34,710
Charlie is a registrar. 
All we need now is a witness, and here 

271
00:19:34,710 --> 00:19:37,400
she comes,
an unsuspecting member of the public 

272
00:19:37,400 --> 00:19:40,294
fetched off the street. 
 >> Could I have your full name please? 

273
00:19:40,294 --> 00:19:42,252
 >> Yeah, Margaret Richardson. 
 >> Margaret Richardson. 

274
00:19:42,252 --> 00:19:45,630
 >> Margaret's offered to bail out the 
bride and groom, who've supposedly been 

275
00:19:45,630 --> 00:19:49,300
let down by their friend. 
What she doesn't know is that Charlie's 

276
00:19:49,300 --> 00:19:52,260
going to make a mistake -- 
quite a bad one. 

277
00:19:52,260 --> 00:19:56,540
 >> It is my duty to remind you of the 
solemn and binding character of the 

278
00:19:56,540 --> 00:20:01,315
ceremony of marriage. 
Now, could I ask you to repeat after me? 

279
00:20:01,315 --> 00:20:05,340
 >> I do solemnly declare... 
 >> I do solemnly declare... 

280
00:20:05,340 --> 00:20:07,390
 >> that I know notâ¦ 
 >> that I know notâ¦ 

281
00:20:07,390 --> 00:20:10,842
 >> of any lawful impediment... 
 >> of any lawful impediment... 

282
00:20:10,842 --> 00:20:11,938
 >> why I... 
 >> why Iâ¦ 

283
00:20:11,938 --> 00:20:16,842
 >> Margaret Richardson. 
 >> Margaret Richardson... 

284
00:20:16,842 --> 00:20:18,578
 >> Yes. 
 >> Repeat the name please. 

285
00:20:18,578 --> 00:20:22,640
 >> Margaret Richardson... 
 >> may not be joined... 

286
00:20:22,640 --> 00:20:25,069
 >> may not be joined... 
 >> in matrimony... 

287
00:20:25,069 --> 00:20:28,075
 >> in matrimony... 
 >> to Peter Roy Edmonds. 

288
00:20:28,075 --> 00:20:31,650
 >> to Peter Roy Edmonds. 
 >> Probably, initially I thought 

289
00:20:31,650 --> 00:20:37,890
something was wrong when, when I started 
to actually say the vows, and then I 

290
00:20:37,890 --> 00:20:41,252
really realized something was wrong when 
I began to place the ring on the finger. 

291
00:20:41,252 --> 00:20:45,520
 >> Place the ring on his finger. 
 >> I suppose, I was just standing there 

292
00:20:45,520 --> 00:20:48,140
and everybody seemed to be in agreement 
with what was happening, 

293
00:20:48,140 --> 00:20:51,510
and I was just told to do it. And for 
some reason I just did. 

294
00:20:52,710 --> 00:20:55,300
 >> So did Eric Taylor. 
 >> Could you repeat after me? 

295
00:20:55,300 --> 00:20:59,610
I call upon these persons here present... 
 >> I call upon these persons here 

296
00:20:59,610 --> 00:21:01,465
present... 
 >> to witness that I... 

297
00:21:01,465 --> 00:21:03,548
 >> to witness that I... 
 >> Eric Taylor... 

298
00:21:03,548 --> 00:21:05,550
 >> Eric Taylor... 
 >> do take thee... 

299
00:21:05,550 --> 00:21:07,668
 >> do take thee... 
 >> Julianne Gillet... 

300
00:21:07,668 --> 00:21:10,480
 >> Julianne Gillet... 
 >> to be my lawful wedded wife. 

301
00:21:10,480 --> 00:21:13,631
 >> to be my lawful wedded wife. 
He was putting the words into my mouth, 

302
00:21:13,631 --> 00:21:16,379
you know? 
He was saying that I was to say this, and 

303
00:21:16,379 --> 00:21:19,670
I was to say that. 
I was thinking that I'm the witness, and 

304
00:21:19,670 --> 00:21:21,184
the groom ain't saying nothing! 
 >> do take theeâ¦ 

305
00:21:21,184 --> 00:21:22,322
 >> Sorry? 
 >> do take thee... 

306
00:21:22,322 --> 00:21:27,109
 >> do take thee... 
 >> Peter Roy Edmonds... 

307
00:21:27,109 --> 00:21:31,875
 >> Peter Roy Edmonds... 
 >> to be my lawful wedded husband. 

308
00:21:31,875 --> 00:21:35,735
 >> At one point Margaret protests. 
 >> Sorry, I'm confused. 

309
00:21:35,735 --> 00:21:37,150
 >> But Charlie's authority is 

310
00:21:37,150 --> 00:21:38,605
irresistible. 
 >> Just repeat the phrase. 

311
00:21:38,605 --> 00:21:40,552
 >> But I'm not getting married. 
 >> No, no, just repeat the phrase. 

312
00:21:40,552 --> 00:21:43,033
 >> To be my lawful wedded husband. 

313
00:21:43,033 --> 00:21:49,460
 >> When I looked confused, the response 
was that I was confused and not 

314
00:21:49,460 --> 00:21:52,315
the situation. 
 >> I now pronounce you man and wife. 

315
00:21:52,315 --> 00:21:56,550
You may kiss the bride. 
 >> I was just aware that this man was 

316
00:21:56,550 --> 00:22:02,130
telling me to do this, in a very sort of 
definite way, 

317
00:22:02,130 --> 00:22:04,050
and there didn't seem to be an 
option. 

318
00:22:04,050 --> 00:22:07,540
I'm not quite sure why. 
 >> Julianne Gillet and Eric Taylor, you've 

319
00:22:07,540 --> 00:22:10,375
both made the declarations prescribed by 
law

320
00:22:10,375 --> 00:22:14,130
and have made a solemn and binding 
contract with each other in the presence 

321
00:22:14,130 --> 00:22:19,170
of the witnesses here assembled. 
I now pronounce you man and wife. 

322
00:22:19,170 --> 00:22:22,115
 >> When he said that we are actually 
married, I was scared. 

323
00:22:22,115 --> 00:22:26,629
I was heading out the door. 
 >> Congratulations, sir. 

324
00:22:26,629 --> 00:22:29,175
Congratulations, you may kiss the 
bride. 

325
00:22:29,175 --> 00:22:30,980
 >> Yep. 
 >> Congratulations. 

326
00:22:30,980 --> 00:22:33,274
 >> [LAUGH] Thank you. 
 >> Alright, is that it? 

327
00:22:33,274 --> 00:22:35,313
 >> Yes, you're now married. 
Well done, Mr. and Mrs. Taylor. 

328
00:22:35,313 --> 00:22:37,493
 >> Yeah. 
 >> You two married, there? 

329
00:22:37,493 --> 00:22:38,456
>> Oh. 
 >> I'm the witness. 

330
00:22:40,456 --> 00:22:41,795
 >> I thought there was something 
kind of wrong here. 

331
00:22:41,795 --> 00:22:44,443
 >> You did put the ring on the... 
 >> Yeah, but I thought 

332
00:22:44,443 --> 00:22:45,671
 >> Right. 
 >> I'm the witness. 

333
00:22:45,671 --> 00:22:48,398
 >> Oh. 
 >> I have married you, together. 

334
00:22:48,398 --> 00:22:53,795
 >> Is this going to be a problem? 
 >> Oh, yeah. 

