


1
00:00:06,900 --> 00:00:10,300
In previous videos, we've been talking about reducing conflict by 

2
00:00:10,100 --> 00:00:14,500
taking the third side. One way to do this is by serving as a mediator.

3
00:00:15,200 --> 00:00:17,700
In mediation, a third party works to resolve

4
00:00:17,700 --> 00:00:23,310
a conflict by facilitating communication and offering suggestions.

5
00:00:23,310 --> 00:00:26,780
Another way to take the third side is to arbitrate.

6
00:00:26,780 --> 00:00:29,470
In arbitration, a third party studies both

7
00:00:29,470 --> 00:00:32,830
sides of a conflict and imposes a settlement -- 

8
00:00:32,830 --> 00:00:35,100
very different from mediation.

9
00:00:35,100 --> 00:00:39,590
So, for example, in binding arbitration, the two sides agree, 

10
00:00:39,590 --> 00:00:45,950
or bind themselves, in advance, to accept whatever solution the arbitrator decides.

11
00:00:45,950 --> 00:00:50,910
And in final offer arbitration, each side submits its very best offer,

12
00:00:50,910 --> 00:00:54,800
and the arbitrator then chooses which one will be accepted.

13
00:00:54,800 --> 00:00:57,850
The idea here is to motivate each side to come up with

14
00:00:57,850 --> 00:01:01,410
the most reasonable offer it can because the arbitrator will be

15
00:01:01,410 --> 00:01:05,660
choosing one of the offers rather than crafting a compromise position.

16
00:01:07,120 --> 00:01:09,850
These are just a few ways to help reduce conflict.

17
00:01:09,850 --> 00:01:13,070
Sometimes taking the third side means listening.

18
00:01:13,070 --> 00:01:16,000
Sometimes it means simply being there, bearing witness, 

19
00:01:16,000 --> 00:01:19,660
or offering support by merely being present.

20
00:01:19,660 --> 00:01:23,280
Sometimes it means offering encouragement and the hope that yes,

21
00:01:23,280 --> 00:01:25,870
in fact, this conflict can be resolved.

22
00:01:26,870 --> 00:01:31,770
In his TED talk, William Ury offered some great examples of taking the third side -- 

23
00:01:31,770 --> 00:01:34,680
for instance, the story of the San Bushmen,

24
00:01:34,680 --> 00:01:37,620
who hide the poison arrows when there's a conflict, 

25
00:01:37,620 --> 00:01:40,630
or the wise old woman and the 18th camel that helped

26
00:01:40,630 --> 00:01:44,700
three brothers reach an agreement on how to divide 17 camels.

27
00:01:45,690 --> 00:01:48,320
But given that this is an Internet-based course with

28
00:01:48,320 --> 00:01:50,680
students from a wide variety of countries,

29
00:01:50,680 --> 00:01:53,580
I thought it would be good to add at least one example

30
00:01:53,580 --> 00:01:58,330
of how the Internet can be used to take the third side internationally.

31
00:01:58,330 --> 00:02:01,100
The role of Facebook and Twitter is well documented in the

32
00:02:01,100 --> 00:02:07,500
Arab Spring, including anti-government protests in the Egyptian revolution of 2011.

33
00:02:07,500 --> 00:02:12,300
But the use of social media to take the third side is much less known.

34
00:02:12,300 --> 00:02:15,600
So the 15-minute clip that I'm about to share with you, 

35
00:02:15,600 --> 00:02:20,100
which is another terrific TED talk, focuses on that topic.

36
00:02:21,360 --> 00:02:25,960
The speaker is a graphic designer and former Israeli soldier who not only 

37
00:02:25,960 --> 00:02:31,100
found a way for Israeli and Iranian citizens to communicate peacefully,

38
00:02:31,100 --> 00:02:34,060
but for people in many other countries to do so as well.

39
00:02:35,350 --> 00:02:39,440
In our class we have over 1,000 students from Israel and

40
00:02:39,440 --> 00:02:42,520
Iran in almost equal numbers, and of course, we have

41
00:02:42,520 --> 00:02:46,570
students from many other countries that have a history of conflict.

42
00:02:46,570 --> 00:02:49,700
So as you interact with each other in this course, 

43
00:02:49,700 --> 00:02:53,100
I hope that you'll draw from the ideas in this TED talk to think of 

44
00:02:53,100 --> 00:02:58,920
other creative ways to replace conflict with understanding and goodwill.

45
00:02:58,920 --> 00:03:03,100
If you do that -- if you succeed -- you'll be turning this MOOC 

46
00:03:03,100 --> 00:03:06,500
into an example of taking the third side. 

47
00:03:09,048 --> 00:03:15,100
>> On March 14 this year, I post this poster on Facebook.

48
00:03:15,100 --> 00:03:19,700
This is an image of me and my daughter holding the Israeli flag.

49
00:03:19,700 --> 00:03:25,310
I will try to explain you about the context of why and when I post it.

50
00:03:25,310 --> 00:03:30,510
A few days ago, I was sitting, waiting, on the line at the grocery store,

51
00:03:30,510 --> 00:03:33,800
and the owner and one of the clients 

52
00:03:33,800 --> 00:03:36,600
were talking to each other, and the owner was explaining 

53
00:03:36,600 --> 00:03:41,260
the client that we're going to get 10,000 missiles on Israel.

54
00:03:41,260 --> 00:03:47,500
And the client will say no, it's 10,000 a day.

55
00:03:48,600 --> 00:03:51,250
This is the context.

56
00:03:51,250 --> 00:03:53,440
This is where we are now in Israel.

57
00:03:53,440 --> 00:03:57,300
We have this war with Iran coming for ten years now,

58
00:03:57,300 --> 00:04:00,630
and we have people, you know, afraid.

59
00:04:00,630 --> 00:04:04,080
It's like every year, it's the last minute that

60
00:04:04,080 --> 00:04:06,550
we can do something about the war with Iran.

61
00:04:06,550 --> 00:04:10,330
It's like, if we don't act now, it's too late forever.

62
00:04:10,330 --> 00:04:12,200
For ten years now.

63
00:04:12,200 --> 00:04:15,000
So at some point it became, you know, to me, 

64
00:04:15,000 --> 00:04:17,700
I'm a graphic designer, so I made poster about it, 

65
00:04:17,700 --> 00:04:21,830
and I posted the one I just showed you before.

66
00:04:21,830 --> 00:04:24,950
Most of the time I make posters, I post them on Facebook,

67
00:04:24,950 --> 00:04:26,470
my friends like it, don't like it,

68
00:04:26,470 --> 00:04:30,490
most of the time don't like it, don't share it, don't nothing, 

69
00:04:30,490 --> 00:04:32,570
and it's another day.

70
00:04:32,570 --> 00:04:36,670
So I went to sleep, and that was it for me.

71
00:04:36,670 --> 00:04:41,560
And later on the night I woke up, because I always waking up in the night, 

72
00:04:41,560 --> 00:04:43,400
and I went by the computer, and I see all these red dots, 

73
00:04:43,400 --> 00:04:47,900
you know, on Facebook, which I've never seen before.

74
00:04:47,900 --> 00:04:50,600
And I was like, what's going on?

75
00:04:50,600 --> 00:04:54,230
So I come to the computer, and I start looking on,

76
00:04:54,230 --> 00:04:56,990
and suddenly I see many people talking to me,

77
00:04:56,990 --> 00:04:59,480
most of them I don't know,

78
00:04:59,480 --> 00:05:05,200
and few of them from Iran. Which is... What?

79
00:05:05,200 --> 00:05:10,320
Because you have to understand, in Israel we don't talk

80
00:05:10,320 --> 00:05:11,480
with people from Iran.

81
00:05:11,480 --> 00:05:13,500
We don't know people from Iran.

82
00:05:13,500 --> 00:05:18,230
It's like, on Facebook you are friends only from, 

83
00:05:18,230 --> 00:05:21,100
it's like your neighbors are your friends on Facebook.

84
00:05:21,100 --> 00:05:24,150
And now people from Iran are, like, talking to me,

85
00:05:24,150 --> 00:05:30,600
so I start answering this girl, and she's telling me she saw the poster, and 

86
00:05:30,600 --> 00:05:33,350
she asked her family to come, because they don't have a computer, 

87
00:05:33,350 --> 00:05:35,140
she asked her family to come to see the poster, 

88
00:05:35,140 --> 00:05:38,740
and they're all sitting in the living room, crying.

89
00:05:38,740 --> 00:05:40,552
So I'm like... Whoa!

90
00:05:40,552 --> 00:05:44,150
I asking my wife to come, and I tell her, you have to see that.

91
00:05:44,150 --> 00:05:45,280
People are crying.

92
00:05:45,280 --> 00:05:49,420
And she came, she read the text, and she start to cry.

93
00:05:49,420 --> 00:05:52,580
And everybody's crying now!

94
00:05:52,580 --> 00:05:53,580
So I don't know what to do,

95
00:05:53,580 --> 00:05:57,010
so my first reflex as a graphic designer, is, you know,

96
00:05:57,010 --> 00:06:00,240
to show everybody what I just seen,

97
00:06:00,240 --> 00:06:03,070
and people start to see them and to share them,

98
00:06:03,070 --> 00:06:04,660
and that's how it started.

99
00:06:04,660 --> 00:06:08,530
The day after, when really it become a lot of talking,

100
00:06:08,530 --> 00:06:11,790
I said to myself, and my wife said to me, I also want a poster.

101
00:06:11,790 --> 00:06:14,800
So, this is her.

102
00:06:15,600 --> 00:06:17,290
Because it's working.

103
00:06:17,290 --> 00:06:19,500
Put me in a poster now.

104
00:06:20,000 --> 00:06:23,100
But more seriously, I was like, okay, these ones work, 

105
00:06:23,100 --> 00:06:26,200
but it's not just about me. It's about people from Israel want to 

106
00:06:26,200 --> 00:06:30,320
say something, so I'm going to shoot all the people I know if they want, 

107
00:06:30,320 --> 00:06:32,610
and I'm going to put them in a poster, and I'm going to share them.

108
00:06:32,610 --> 00:06:38,300
So, I went to my neighbors and friends and students, and I just asked them,

109
00:06:38,300 --> 00:06:40,600
"Give me a picture -- I will make you a poster."

110
00:06:40,600 --> 00:06:45,030
And that's how it started. And that's how, really, it's unleashed,

111
00:06:45,030 --> 00:06:48,970
because suddenly, people from Facebook, friends and others,

112
00:06:48,970 --> 00:06:51,710
just understand that they can be part of it.

113
00:06:51,710 --> 00:06:53,690
It's not just one dude making one poster.

114
00:06:53,690 --> 00:06:57,040
It's we can be part of it. So they start sending me pictures 

115
00:06:57,040 --> 00:06:59,780
and ask me make me a poster, post it.

116
00:06:59,780 --> 00:07:03,100
Tell the Iranians, we from Israel love you, too. 

117
00:07:03,960 --> 00:07:08,000
It become, you know, at some point it was really, really intense, 

118
00:07:08,000 --> 00:07:13,400
I think, so many pictures, so I ask friends to come, 

119
00:07:13,400 --> 00:07:16,100
graphic designer most of them, to make poster with me 

120
00:07:16,100 --> 00:07:17,430
because I didn't have the time.

121
00:07:17,430 --> 00:07:20,180
It was a huge amount of picture,

122
00:07:20,180 --> 00:07:23,500
so for few days, that's how my living room was.

123
00:07:24,190 --> 00:07:28,270
And we receive Israeli posters, Israeli image, 

124
00:07:28,270 --> 00:07:32,900
but also lot of comments, lot of messages from Iran.

125
00:07:32,900 --> 00:07:36,270
And we took these messages and we made poster out of it 

126
00:07:36,270 --> 00:07:40,090
because I know people they don't read -- they see images.

127
00:07:40,090 --> 00:07:43,050
If it's an image, they may read it.

128
00:07:43,050 --> 00:07:46,100
So, here's a few of them.

129
00:07:48,300 --> 00:07:52,600
This one is really moving for me because it's the story of a girl that has been  

130
00:07:52,600 --> 00:08:01,000
raised in Iran to walk on an Israeli flag to enter her school every morning.

131
00:08:01,000 --> 00:08:03,250
And now that she see the posters that we're sending, 

132
00:08:03,250 --> 00:08:06,750
she start, she said that she changed her mind.

133
00:08:06,750 --> 00:08:10,090
And now she loves that blue, she loves that star, and

134
00:08:10,090 --> 00:08:14,310
she loves that flag, talking about the Israeli flag, and she wished that we meet 

135
00:08:14,310 --> 00:08:20,100
and we come to visit one each other in just few day after I posted the first poster. 

136
00:08:21,390 --> 00:08:26,500
The day after, Iranians start to respond with their own posters.

137
00:08:26,500 --> 00:08:27,800
They have graphic designers.

138
00:08:27,800 --> 00:08:30,200
What?!

139
00:08:30,200 --> 00:08:32,940
That is crazy.

140
00:08:32,940 --> 00:08:35,690
So you can see they're still shy -- they don't want to show their faces,

141
00:08:35,690 --> 00:08:38,300
but they want to spread the message. They want to respond. 

142
00:08:38,300 --> 00:08:40,280
They want to say the same thing, so, 

143
00:08:40,280 --> 00:08:44,050
and now it's communication. It's a two-way story. 

144
00:08:44,050 --> 00:08:48,360
It's Israeli and Iranian sending the same message one to each other.

145
00:08:50,270 --> 00:08:55,690
This never happened before, and this is two people supposed to be enemy.

146
00:08:55,690 --> 00:08:58,550
We're on the verge of war and suddenly,

147
00:08:58,550 --> 00:09:03,700
people on Facebook starting to say, I like this guy. I love those guys. 

148
00:09:04,680 --> 00:09:08,560
And it become really big at some point.

149
00:09:08,560 --> 00:09:11,910
And then it become news.

150
00:09:11,910 --> 00:09:16,200
Because when you seeing the Middle East, you see only the bad news, 

151
00:09:16,200 --> 00:09:20,340
and suddenly, there is something that was happening that was good news.

152
00:09:20,340 --> 00:09:23,000
So the guys on the news, they say, okay, let's talk about this.

153
00:09:23,000 --> 00:09:24,350
And it just came.

154
00:09:24,350 --> 00:09:26,350
And it, it was so much.

155
00:09:26,350 --> 00:09:30,000
I remember one day, Michal, she was talking with the journalist, 

156
00:09:30,000 --> 00:09:33,230
and she was asking him, "Who's going to see the show?"

157
00:09:33,230 --> 00:09:35,870
And he say, "Everybody."

158
00:09:35,870 --> 00:09:41,095
So she said, "Everybody in the, in Palestine? In where? Israel? 

159
00:09:41,095 --> 00:09:42,930
Who is everybody?"

160
00:09:42,930 --> 00:09:45,540
"Everybody." She say, "Syria?" "Syria."

161
00:09:45,540 --> 00:09:47,100
"Lebanon?" "Lebanon."

162
00:09:47,100 --> 00:09:49,390
At some point, you just say 40 million people

163
00:09:49,390 --> 00:09:52,300
are going to see you today. It's everybody.

164
00:09:52,300 --> 00:09:59,000
The Chinese. And we were just at the beginning of the story.

165
00:09:59,830 --> 00:10:05,500
Something crazy also happened every time a country start talking about it, 

166
00:10:05,500 --> 00:10:12,250
like Germany, America, wherever, a page on Facebook pop up with

167
00:10:12,250 --> 00:10:14,470
the same logo, with the same stories.

168
00:10:14,470 --> 00:10:19,400
So at the beginning we had Iran loves Israel, which is an Irani sitting 

169
00:10:19,400 --> 00:10:25,807
in Tehran, say, "Okay, Israel loves Iran? I give you Iran Loves Israel."

170
00:10:25,807 --> 00:10:27,690
You have Palestine Loves Israel.

171
00:10:27,690 --> 00:10:33,890
You have Lebanon that just few days ago, and these all lists of pages on Facebook 

172
00:10:33,890 --> 00:10:39,000
dedicated to the same message, to people sending their love one to each other.

173
00:10:42,510 --> 00:10:46,310
The moment I really understand that something was happening, a friend of mine 

174
00:10:46,310 --> 00:10:52,620
tell me, "Google the word Israel." And that, those were the first images

175
00:10:52,620 --> 00:10:59,750
on those day that pop ups from Google when you were typing "Israel" or "Iran."

176
00:10:59,750 --> 00:11:03,860
We really changed how people sees the Middle East, 

177
00:11:03,860 --> 00:11:06,370
because you're not in the Middle East -- you're somewhere over there. 

178
00:11:06,370 --> 00:11:07,730
And then you want to see the Middle East,

179
00:11:07,730 --> 00:11:10,840
so you go on Google and you say "Israel," and they give you the bad stuff.

180
00:11:10,840 --> 00:11:14,500
And for few day, you got those images.

181
00:11:15,830 --> 00:11:20,400
Today the Israel Loves Iran page is this number: 80,000.

182
00:11:20,400 --> 00:11:24,700
And 2 million people last week went on the page

183
00:11:24,700 --> 00:11:29,010
and shared, like, I don't know, comment one of the photos.

184
00:11:29,010 --> 00:11:33,110
So for five months now, that's what we're doing: 

185
00:11:33,110 --> 00:11:38,470
me, Michal, few of my friends that just making images.

186
00:11:38,470 --> 00:11:42,800
We showing a new reality by just making images, 

187
00:11:42,800 --> 00:11:45,370
because that's how the world perceives us.

188
00:11:45,370 --> 00:11:48,100
They see images of us, and they see bad images.

189
00:11:48,100 --> 00:11:51,400
So we're working on making good images.

190
00:11:51,400 --> 00:11:54,770
End of story. Look at this one.

191
00:11:54,770 --> 00:11:59,860
This is the Iran Love Israel page. This is not the Israel Love Iran.

192
00:11:59,860 --> 00:12:00,720
This is not my page.

193
00:12:00,720 --> 00:12:06,130
This is a guy in Tehran on the Day of Remembrance of the Israeli 

194
00:12:06,130 --> 00:12:10,900
fallen soldier putting an image of an Israeli soldier on his page.

195
00:12:10,900 --> 00:12:14,480
This is a, this is the enemy! 

196
00:12:14,480 --> 00:12:17,970
What?

197
00:12:17,970 --> 00:12:19,670
And it's going both way.

198
00:12:19,670 --> 00:12:25,290
It's like we're showing respect one to each other. And we're understanding.

199
00:12:27,270 --> 00:12:31,500
And you show compassion, and you become friends.

200
00:12:34,190 --> 00:12:39,000
And at some point you become friend on Facebook and you become friend in life.

201
00:12:39,000 --> 00:12:42,110
You can go and travel and meet people.

202
00:12:42,110 --> 00:12:48,940
And I was in Munich a few weeks ago. I went there to open an exposition about Iran,

203
00:12:48,940 --> 00:12:52,100
and I meet there with people from the page that told me, 

204
00:12:52,100 --> 00:12:55,600
"Okay, you going to be in Europe -- I'm coming." I'm coming from France, 

205
00:12:55,600 --> 00:12:57,550
from Holland, and from Germany, of course. 

206
00:12:57,550 --> 00:12:59,230
And from Israel people came.

207
00:12:59,230 --> 00:13:03,420
And we just met there for the first time in real life.

208
00:13:03,420 --> 00:13:09,380
I met with people that are supposed to be my enemies for the first time.

209
00:13:09,380 --> 00:13:12,490
And we just shake hands and have a coffee and a nice discussion,

210
00:13:12,490 --> 00:13:16,299
and we talk about food and basketball, and that was the end of it.

211
00:13:17,330 --> 00:13:21,080
Remember that image from the beginning? At some point 

212
00:13:21,080 --> 00:13:26,000
we meet in real life and we become friends.

213
00:13:28,200 --> 00:13:32,560
So, you may ask yourself, "Who is this dude?" 

214
00:13:35,770 --> 00:13:39,470
My name is Ronny Edry, and I'm 41. I'm an Israeli.

215
00:13:39,470 --> 00:13:44,810
I'm father of two. I'm a husband, and

216
00:13:44,810 --> 00:13:47,900
I'm a graphic designer -- I'm teaching graphic design.

217
00:13:49,200 --> 00:13:53,700
And I'm not that naive because lot of the time I've been asked, 

218
00:13:53,700 --> 00:13:55,890
many time I've been asked, yeah but, 

220
00:13:55,890 --> 00:13:58,080
this is really naive sending flowers over.

221
00:13:58,080 --> 00:14:02,000
I mean, I'm -- I was in the army. I was in the paratroopers 

222
00:14:02,000 --> 00:14:06,720
for three years, and I know how it looks from the ground.

223
00:14:06,720 --> 00:14:12,900
I know how it can look really bad. So to me,

224
00:14:12,900 --> 00:14:17,690
this is the courageous thing to do, to try to reach the other side 

225
00:14:17,690 --> 00:14:20,490
before it's too late, because when it's going to be too late, it's going to be too late, 

226
00:14:20,490 --> 00:14:24,420
and sometimes, war is inevitable.

227
00:14:24,420 --> 00:14:28,920
Sometimes, but maybe doing an effort, we can avoid it.

228
00:14:28,920 --> 00:14:34,450
Maybe as people, because especially in Israel, we're in a democracy.

229
00:14:34,450 --> 00:14:36,110
We have the freedom of speech.

230
00:14:36,110 --> 00:14:39,770
And maybe that little thing can change something.

231
00:14:39,770 --> 00:14:43,430
And really, we can be our own ambassadors.

232
00:14:43,430 --> 00:14:48,000
We can just send a message and hope for the best.

233
00:14:50,100 --> 00:14:53,410
So I want to ask Michal, my wife, to come with me on the stage

234
00:14:53,410 --> 00:14:57,970
just to make with you one image.

235
00:14:57,970 --> 00:14:59,800
Because it's all about images.

236
00:14:59,800 --> 00:15:04,480
And maybe that image will help us change something.

237
00:15:04,480 --> 00:15:07,570
Just to raise that. Exactly.

238
00:15:07,570 --> 00:15:10,460
And I'm just going to take a picture of it.

239
00:15:10,460 --> 00:15:13,500
And I'm just going to post it on Facebook with kind of 

240
00:15:13,500 --> 00:15:17,000
"Israeli for Peace" or something. 

241
00:15:20,800 --> 00:15:23,000
Oh, my God.

242
00:15:23,700 --> 00:15:26,000
Don't cry.

243
00:15:26,000 --> 00:15:29,000
Thank you, guys!

