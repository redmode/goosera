




1
00:00:07,170 --> 00:00:11,330
In the last video, I ended by asking 
whether the level of conformity that 

2
00:00:11,330 --> 00:00:18,800
Solomon Asch found seemed very high, or 
fairly high, or fairly low, or very low. 

3
00:00:18,800 --> 00:00:23,910
For over 20 years after he published his 
findings, most psychologists thought that 

4
00:00:23,910 --> 00:00:29,140
it was surprisingly high given how mild 
the situation was. 

5
00:00:29,140 --> 00:00:33,940
After all, the experimental participants 
weren't members of a cohesive group in 

6
00:00:33,940 --> 00:00:38,920
which they had to, for example, face their 
friends afterward, or justify their answers, 

7
00:00:38,920 --> 00:00:44,120
or even interact with members of 
their group once the study was over. 

8
00:00:44,120 --> 00:00:49,420
Second, there weren't penalties or 
financial losses or physical punishments, 

9
00:00:49,420 --> 00:00:52,690
like electric shock for guessing 
incorrectly. 

10
00:00:52,690 --> 00:00:57,130
And third, the correct answers were 
embarrassingly obvious -- 

11
00:00:57,130 --> 00:01:01,550
so obvious, you'd almost feel weird 
giving a different answer. 

12
00:01:01,550 --> 00:01:08,010
That was the prevailing view until 1976, 
when Lee Ross, Gunter Bierbrauer, and 

13
00:01:08,010 --> 00:01:12,900
Susan Hoffman published an article in the 
American Psychologist arguing 

14
00:01:12,900 --> 00:01:16,750
just the opposite. 
That is, they argued that the Asch 

15
00:01:16,750 --> 00:01:22,480
experiment created a unique situation in 
which "the assault on the 

16
00:01:22,480 --> 00:01:27,730
potential dissenter's judgment reaches 
an intensity virtually unparalleled 

17
00:01:27,730 --> 00:01:29,300
outside the laboratory." 

18
00:01:30,000 --> 00:01:36,000
Why? Well, consider: In daily life the things 
that we tend to disagree about include 

19
00:01:36,000 --> 00:01:43,100
politics, religion, books, movies -- not 
the nature of physical reality. 

20
00:01:43,100 --> 00:01:48,540
Few of us have ever faced a majority who 
steadfastly contradict the evidence of 

21
00:01:48,540 --> 00:01:53,800
our senses, where, for example, we say 
an orange is orange, 

22
00:01:53,800 --> 00:01:57,300
and the majority says, no, it's blue. 

23
00:01:57,300 --> 00:02:02,800
And we say, "Orange." 
And they say, "Blue." 

24
00:02:02,500 --> 00:02:04,030
"Orange." 
"Blue." 

25
00:02:04,030 --> 00:02:06,800
"Orange." 
"Blue." 
"Orange!" 

26
00:02:06,800 --> 00:02:08,800
Simply never happens. 

27
00:02:08,800 --> 00:02:14,450
Also, the experimental participants were 
at a loss to explain the majority opinion 

28
00:02:14,450 --> 00:02:19,140
on the basis of either situational forces 
or dispositional differences. 

29
00:02:19,140 --> 00:02:24,589
Unless they saw through the conspiracy, 
they simply had no good explanation for 

30
00:02:24,589 --> 00:02:29,370
what was going on. 
Ross, Bierbrauer, and Hoffman argued 

31
00:02:29,370 --> 00:02:35,710
that, in this sense, Asch had created a 
thoroughly unique "attribution crisis" for 

32
00:02:35,710 --> 00:02:39,350
the participants. 
People had no good explanation for their 

33
00:02:39,350 --> 00:02:44,220
group's behavior, and they feared that 
the group would be equally at a loss to 

34
00:02:44,220 --> 00:02:49,500
explain any dissent on their part. 
Here's how one of Asch's participants 

35
00:02:49,500 --> 00:02:55,600
described it: "This is unlike any 
experience I have had in my life. 

36
00:02:55,600 --> 00:03:03,125
I'll never forget it as long as I live." 
Interestingly, this revisionist view of 

37
00:03:03,125 --> 00:03:08,810
Asch's research is very consistent with 
how Asch himself saw the situation. 

38
00:03:08,810 --> 00:03:13,750
In December of 2012, I looked through 
some of his papers at the Center for the 

39
00:03:13,750 --> 00:03:17,880
History of Psychology, 
and by his own hand, he described the 

40
00:03:17,880 --> 00:03:23,860
situation as "Extreme when a 
majority contradicted the evidence of a 

41
00:03:23,860 --> 00:03:29,850
person's senses about a virtually 
undeniable matter of fact." 

42
00:03:29,850 --> 00:03:34,490
He also noted that the conflict created 
severe tension and concern,

43
00:03:34,490 --> 00:03:39,540
and in another note, he said that it was 
"Frightening to have one's 

44
00:03:39,540 --> 00:03:44,990
perceptions appear strange." 
This might seem obvious in hindsight, 

45
00:03:44,990 --> 00:03:48,740
but remember the hindsight bias that we 
discussed earlier. 

46
00:03:48,740 --> 00:03:53,200
One of the most revealing tidbits I found 
in the archive is that in the beginning, 

47
00:03:53,200 --> 00:03:57,000
Asch himself didn't realize the power of 
the situation. 

48
00:03:57,000 --> 00:04:01,390
Here's what he wrote. 
"While planning the study, I was quite 

49
00:04:01,390 --> 00:04:05,790
uncertain as to whether the investigation 
could at all be done. 

50
00:04:05,790 --> 00:04:11,070
It seemed to me that the action of the 
majority would appear so unreasonable and 

51
00:04:11,070 --> 00:04:16,080
bizarre that the subjects would quickly 
see through, laugh the majority out of 

52
00:04:16,080 --> 00:04:20,340
court and dismiss them as unworthy of 
respect. 

53
00:04:20,340 --> 00:04:25,190
This did not happen. 
In fact, it was chastening to observe how 

54
00:04:25,190 --> 00:04:30,570
easy it was to entangle individuals in 
the difficulty, and thus to bring them 

55
00:04:30,570 --> 00:04:35,900
under the sway of the forces. 
The means were absurdly simple." 

56
00:04:38,000 --> 00:04:42,500
As you might imagine, there have been 
literally hundreds of conformity studies 

57
00:04:42,500 --> 00:04:47,000
and dozens of spin-offs since Asch first 
published his experiments. 

58
00:04:47,000 --> 00:04:52,800
In fact, in the 1990s, Rod Bond and Peter 
Smith published a meta-analysis of  

59
00:04:52,800 --> 00:04:59,600
133 different studies from 17 countries in 
an effort to take stock of the 

60
00:04:59,600 --> 00:05:03,300
research literature, and the analysis 
was particularly interesting 

61
00:05:03,300 --> 00:05:07,650
because Bond and Smith were 
able to simultaneously examine different 

62
00:05:07,650 --> 00:05:13,070
factors to see which ones had a 
significant effect on conformity levels. 

63
00:05:13,070 --> 00:05:18,930
So, for example, they found that when you 
put women and men in the Asch situation, 

64
00:05:18,930 --> 00:05:24,880
either in single-sex groups or mixed-sex 
groups, women tend to conform somewhat 

65
00:05:24,880 --> 00:05:31,300
more than do men -- a gender gap that has 
not closed significantly over the decades. 

66
00:05:31,500 --> 00:05:34,060
Overall, however, conformity levels have 

67
00:05:34,060 --> 00:05:39,600
steadily declined since the 1950s. And the 
meta-analysis found that 

68
00:05:39,600 --> 00:05:45,170
conformity increases as majority size 
increases, from 2 people all the way up 

69
00:05:45,170 --> 00:05:49,460
to 13 people. 
Another thing that Bond and Smith found 

70
00:05:49,460 --> 00:05:54,000
was that conformity is more likely when 
the majority is made up of ingroup 

71
00:05:54,000 --> 00:05:56,890
members (that is, members of your own 
group -- 

72
00:05:56,890 --> 00:06:01,000
for example, friends or family members) 
than when it's made up of 

73
00:06:01,000 --> 00:06:05,080
outgroup members. 
And finally, one last factor worth 

74
00:06:05,080 --> 00:06:10,220
noting -- and they studied others as 
well -- is the role of culture. 

75
00:06:10,220 --> 00:06:15,800
In general, collectivist countries like 
Japan, that tend to put group goals ahead 

76
00:06:15,800 --> 00:06:20,640
of personal goals, show higher rates of 
conformity than do individualist 

77
00:06:20,640 --> 00:06:25,790
countries like the United States. 
Now, which of these various factors is 

78
00:06:25,790 --> 00:06:28,800
most important when it comes to 
conformity? 

79
00:06:28,800 --> 00:06:31,200
Let's pause so that you can take a guess, 

80
00:06:31,200 --> 00:06:34,000
and then I'll share what Bond and Smith 
concluded. 

81
00:06:37,700 --> 00:06:42,800
The correct answer is culture. According to 
Bond and Smith, there's a huge difference 

82
00:06:42,800 --> 00:06:47,370
between individualist and collectivist cultures 
when it comes to conformity, 

83
00:06:47,370 --> 00:06:52,380
probably in part because, while we in the 
United States and in other individualist 

84
00:06:52,380 --> 00:06:56,940
countries tend to view conformity 
somewhat negatively, people in 

85
00:06:56,940 --> 00:07:02,300
collectivist cultures often see it in 
terms of harmony, or cooperativeness, 

86
00:07:02,300 --> 00:07:06,500
or even tactfulness. And this may also help 
to explain some of the 

87
00:07:06,500 --> 00:07:11,400
gender differences in conformity. 
So, for example, American males often see 

88
00:07:11,400 --> 00:07:17,420
nonconformity and uniqueness as positive 
characteristics, but others may see the 

89
00:07:17,420 --> 00:07:21,190
very same behavior as selfish or as deviant. 

90
00:07:21,190 --> 00:07:24,740
So, we have to be very careful about the 
labels that we use. 

91
00:07:24,740 --> 00:07:30,030
One person's conformity may be another 
person's effort to be accommodating 

92
00:07:30,030 --> 00:07:34,800
or to be supportive of others. 
Now, before this video ends there's just 

93
00:07:34,800 --> 00:07:40,450
one more topic that I would like to address, 
so hang on for just a few more minutes. 

94
00:07:40,450 --> 00:07:45,320
The research we've discussed so far 
focuses on the influence a majority has 

95
00:07:45,320 --> 00:07:50,330
on a minority, often one or two people, 
but an equally important question 

96
00:07:50,330 --> 00:07:54,500
concerns the influence of a minority on 
the majority. 

97
00:07:54,500 --> 00:08:00,600
In 1969, Serge Moscovici and two 
colleagues published a landmark study 

98
00:08:00,600 --> 00:08:05,900
that examined minority influence using a 
variant of the Asch procedure. 

99
00:08:05,900 --> 00:08:11,400
Moscovici was born in Romania and spent 
time in a forced labor camp during 

100
00:08:11,400 --> 00:08:14,980
World War II, and later became a friend of 
Milgram's. 

101
00:08:14,980 --> 00:08:19,870
So, like Asch and Milgram, he was 
personally interested in the Holocaust 

102
00:08:19,870 --> 00:08:23,419
and the interplay between individual and 
group behavior. 

103
00:08:24,460 --> 00:08:29,190
What Moscovici and his colleagues showed 
is that a consistent minority can have a 

104
00:08:29,190 --> 00:08:34,300
significant influence even when it's not 
particularly powerful or prestigious. 

105
00:08:34,300 --> 00:08:39,800
In Moscovici's experiments, the group 
consisted of two confederates and 

106
00:08:39,800 --> 00:08:44,100
four experimental participants. 
In other words, it turned the Asch 

107
00:08:44,100 --> 00:08:48,860
procedure on its head. 
Everyone was seated together and shown 

108
00:08:48,860 --> 00:08:54,390
a series of slides in what appeared to be 
an experiment on color perception. 

109
00:08:54,390 --> 00:08:59,350
On each trial, group members judged the 
color and light intensity of the slide. 

110
00:08:59,350 --> 00:09:04,020
The confederates were seated either in 
the first two positions or in the first 

111
00:09:04,020 --> 00:09:09,400
and fourth positions (by the way, this 
variable turned out not to have an effect). 

112
00:09:09,400 --> 00:09:12,600
All the slides were various shades of blue, 

113
00:09:12,600 --> 00:09:17,600
but the confederates were 
instructed to judge them as green. 

114
00:09:17,600 --> 00:09:22,040
What Moscovici found was that, even 
though a control group only mistook the 

115
00:09:22,040 --> 00:09:29,200
slides for green twice in 792 judgments -- 
that is, the slides were very clearly blue -- 

116
00:09:29,200 --> 00:09:32,300
and even though all the participants had 
been previously  

117
00:09:32,300 --> 00:09:36,890
checked to ensure that they had no serious visual 
impairments, 

118
00:09:36,890 --> 00:09:42,800
majority group members judged the slides 
as green on 8% of the trials, 

119
00:09:42,800 --> 00:09:49,120
and 32% of majority group members judged 
them as green at least once. 

120
00:09:49,120 --> 00:09:54,320
In other words, a minority was able to 
exert a substantial degree of influence 

121
00:09:54,320 --> 00:09:58,860
on the majority. 
There was just one key requirement: 

122
00:09:58,860 --> 00:10:03,680
the minority had to be consistent. 
If the pair of confederates labeled some slides

123
00:10:03,680 --> 00:10:08,200
blue and some green, majority 
group members were only swayed by the 

124
00:10:08,200 --> 00:10:12,790
minority 1% of the time. 
The minority only had a significant 

125
00:10:12,790 --> 00:10:17,360
influence when it consistently judged all 
the slides as green. 

126
00:10:18,830 --> 00:10:23,700
Still, this is an empowering result. 
With Asch, we've already seen that just 

127
00:10:23,700 --> 00:10:28,780
one person dissenting from the majority 
can dramatically reduce the pressure that 

128
00:10:28,780 --> 00:10:32,850
people feel to conform. 
And now with Moscovici, we've seen that a consistent 

129
00:10:32,850 --> 00:10:39,100
minority can get majority 
group members to judge the color blue as green. 

130
00:10:39,100 --> 00:10:42,310
So, the message is "individuals count." 

131
00:10:42,310 --> 00:10:48,040
Minority perspectives have an effect -- not 
always and maybe not immediately -- 

132
00:10:48,040 --> 00:10:52,635
but more often that it might seem. 
In fact, when I first learned about 

133
00:10:52,635 --> 00:10:57,600
Moscovici's results, it inspired me to 
join with a couple of friends and create 

134
00:10:57,600 --> 00:11:02,580
a performance group that you may have 
heard of: Green Man Group. 

135
00:11:02,580 --> 00:11:06,695
I'll let you figure out which one is me. 
Just kidding! 

136
00:11:06,695 --> 00:11:10,080
Seriously, I hope that you've enjoyed 
this video. 

137
00:11:10,080 --> 00:11:14,370
The next video is going to continue our 
exploration of the tension between 

138
00:11:14,370 --> 00:11:19,710
individuals and groups, with a discussion 
of deindividuation and the Stanford 

139
00:11:19,710 --> 00:11:23,389
Prison Experiment. 
Also, if you haven't finished the 

140
00:11:23,389 --> 00:11:26,240
next assignment, this is a very good time to 
do it,

141
00:11:26,240 --> 00:11:29,938
because it brings together many of the 
topics that we've been exploring: 

142
00:11:29,938 --> 00:11:35,810
the psychology of persuasion, social 
influence, and of course, conformity. 

143
00:11:35,810 --> 00:11:37,900
It's packed with social psychology. 

144
00:11:37,900 --> 00:11:40,000
Does it seem green in here? 
