



1
00:00:06,814 --> 00:00:08,891
One of the most basic questions we can 

2
00:00:08,891 --> 00:00:13,476
ask about two people is whether they 
share the same reality... 

3
00:00:13,476 --> 00:00:17,400
whether they see the same thing when 
they're both looking at it... 

4
00:00:17,400 --> 00:00:21,100
whether they hear the same thing when 
they're both listening to it. 

5
00:00:22,290 --> 00:00:26,170
In daily life, we typically assume that 
other people share our reality, and to a 

6
00:00:26,170 --> 00:00:31,390
great extent they do, but not always and not completely. 

7
00:00:31,390 --> 00:00:36,970
Our perceptions are powerfully influenced 
by where our attention happens to be, by 

8
00:00:36,970 --> 00:00:43,270
context, by past experience, expectations, 
motivations, and many other factors.

9
00:00:43,270 --> 00:00:47,010
factors. 
In other words, our experience of reality 

10
00:00:47,010 --> 00:00:53,150
is psychologically constructed. 
I spoke about this principle in the video 

11
00:00:53,150 --> 00:00:58,160
on our course sign-up page, when I pointed 
out that the cards in our course image 

12
00:00:58,160 --> 00:01:01,175
contain a surprise that most people don't 
notice: 

13
00:01:01,175 --> 00:01:07,950
a black ten of diamonds instead of the 
normal red. 

14
00:01:07,950 --> 00:01:11,830
You can see the same sort of thing in 
this photo of a drive-through sign that 

15
00:01:11,830 --> 00:01:16,470
says over 99 billion served. 
See anything strange? 

16
00:01:16,470 --> 00:01:21,150
Most people don't see anything wrong with 
this photo until it's pointed out that 

17
00:01:21,150 --> 00:01:26,760
the restaurant's name says McDonlad's 
rather than McDonald's. 

18
00:01:26,760 --> 00:01:31,500
When we look at restaurant signs, we don't 
usually spell check them any more than we 

19
00:01:31,500 --> 00:01:36,930
need to check the color of playing cards.
Or take a look at this photo. 

20
00:01:36,930 --> 00:01:41,054
Most people see this image as showing an 
ordinary handshake and leave it at that. 

21
00:01:41,054 --> 00:01:44,150
Why? 
We don't usually need to count the number 

22
00:01:44,150 --> 00:01:46,740
of fingers in a handshake. 

23
00:01:46,740 --> 00:01:51,810
We often see what we expect to see, and 
don't see what we don't expect to see. 

24
00:01:51,810 --> 00:01:56,875
But perception isn't just a matter of 
expectations -- it's also a matter of 

25
00:01:56,875 --> 00:02:01,060
motivations. 
That is, we often see what we want to see 

26
00:02:01,060 --> 00:02:07,025
and don't see what we don't want to see. 
This dynamic was illustrated in a classic 

27
00:02:07,025 --> 00:02:13,430
1954 study of two American football 
rivals, Princeton and Dartmouth. 

28
00:02:13,430 --> 00:02:18,000
After an especially rough football game 
with lots of penalties, the researchers 

29
00:02:18,000 --> 00:02:22,450
asked students in each school to watch 
the very same film of the game 

30
00:02:22,450 --> 00:02:25,220
and record each rule violation that they 
saw. 

31
00:02:25,220 --> 00:02:29,350
The results? 
Princeton students saw Dartmouth break 

32
00:02:29,350 --> 00:02:33,060
the rules over twice as often as 
Dartmouth students did. 

33
00:02:33,060 --> 00:02:38,700
That is, they saw a different reality. 
According to the researchers: 

34
00:02:38,700 --> 00:02:43,460
"It's inaccurate and misleading to say that 
different people have different attitudes 

35
00:02:43,460 --> 00:02:48,350
concerning the same thing, 
for the thing simply is not the same for 

36
00:02:48,350 --> 00:02:53,576
different people, whether the thing is a 
football game, a presidential candidate, 

37
00:02:53,576 --> 00:02:58,650
Communism, or spinach." 
That's a colorful way of putting it, but 

38
00:02:58,650 --> 00:03:03,070
again we come back to the psychological 
construction of reality, 

39
00:03:03,070 --> 00:03:07,430
the idea that our perceptions are 
affected by what we expect to see, 

40
00:03:07,430 --> 00:03:12,630
by what we want to see, by what we're paying 
attention to, and so forth. 

41
00:03:12,630 --> 00:03:17,230
One of my all-time favorite examples 
illustrating this point comes from 

42
00:03:17,230 --> 00:03:21,960
British psychologist and professional 
magician Richard Wiseman, who has 

43
00:03:21,960 --> 00:03:27,960
generously made available to our class a 
video of his color changing card trick. 

44
00:03:27,960 --> 00:03:33,000
So let's watch a master magician both 
entertain and educate us 

45
00:03:33,000 --> 00:03:35,300
at the same time. 
Hope you enjoy! 

46
00:03:43,200 --> 00:03:44,410
 >> Hi, I'm Richard. 

47
00:03:44,410 --> 00:03:48,500
This is Sarah, and we're going to perform 
the amazing color changing card trick 

48
00:03:48,500 --> 00:03:52,650
with this blue-backed deck of cards. 
Now, the idea is very simple. 

49
00:03:52,650 --> 00:03:56,470
I'm just going to spread the cards in 
front of Sarah and ask her to push any 

50
00:03:56,470 --> 00:03:59,384
card towards the camera. 
 >> All right. 

51
00:03:59,384 --> 00:04:01,548
Okay. 
Let's see. 

52
00:04:01,548 --> 00:04:06,920
I'm going to go for this card here. 
 >> Okay. 

53
00:04:06,920 --> 00:04:11,120
Now, Sarah could've selected any card at 
all from the deck, 

54
00:04:11,120 --> 00:04:15,280
but she selected the card, which is now 
face down on the table, and what I'm going 

55
00:04:15,280 --> 00:04:18,475
to ask her to do is show us which card 
she selected. 

56
00:04:18,475 --> 00:04:22,964
 >> Right. 
So the card that I chose was in fact this 

57
00:04:22,964 --> 00:04:25,880
three of diamonds. 
 >> The three of diamonds. 

58
00:04:25,880 --> 00:04:27,510
Okay. 
Excellent choice. 

59
00:04:27,510 --> 00:04:31,390
That card goes back into the deck. 
Now, I'm just going to spread the cards 

60
00:04:31,390 --> 00:04:36,300
face up on the table, do a little click 
of the fingers, and you'll see 

61
00:04:36,300 --> 00:04:40,350
that Sarah's card here has now got a blue 
back. 

62
00:04:40,350 --> 00:04:44,360
Not particularly surprising. 
What's slightly more surprising is all of 

63
00:04:44,360 --> 00:04:51,850
the other cards have got red backs -- 
and that is the amazing color changing card trick! 

64
00:05:12,070 --> 00:05:14,390
Hi, I'm Richard. 
This is Sarah, 

65
00:05:14,390 --> 00:05:18,220
and we are going to perform the amazing 
color changing card trick with this  

66
00:05:18,220 --> 00:05:22,020
blue-backed deck of cards. 
Now, the idea is very simple. 

67
00:05:22,020 --> 00:05:25,600
I'm just going to spread the cards in 
front of Sarah and ask her to push  

68
00:05:25,600 --> 00:05:28,667
any card towards the camera. 
 >> All right. 

69
00:05:28,667 --> 00:05:30,760
Okay. 
Let's see. 

70
00:05:30,760 --> 00:05:36,259
I'm going to go for this card here. 
 >> Okay. 

71
00:05:36,259 --> 00:05:40,011
Now, Sarah could have selected any card 
at all from the deck,

72
00:05:40,011 --> 00:05:44,580
but she selected the card, which is now 
face down on the table and what I'm going 

73
00:05:44,580 --> 00:05:47,762
to ask her to do is show us which card 
she selected. 

74
00:05:47,762 --> 00:05:52,173
 >> Right. 
So the card that I chose was in fact this 

75
00:05:52,173 --> 00:05:55,190
three of diamonds. 
 >> The three of diamonds. 

76
00:05:55,190 --> 00:05:56,820
Okay. 
Excellent choice. 

77
00:05:56,820 --> 00:06:00,699
That card goes back into the deck. 
Now, I'm just going to spread the cards 

78
00:06:00,699 --> 00:06:05,610
face up on the table, do a little click 
of the fingers, and you'll see 

79
00:06:05,610 --> 00:06:09,660
that Sarah's card here has now got a blue 
back. 

80
00:06:09,660 --> 00:06:13,670
Not particularly surprising. 
What's slightly more surprising is all of 

81
00:06:13,670 --> 00:06:20,910
the other cards have got red backs -- 
and that is the amazing color changing card trick! 

82
00:06:21,910 --> 00:06:23,940
 >> Isn't that terrific? 

83
00:06:23,940 --> 00:06:28,000
The color changing card trick is an 
example of what psychologists call 

84
00:06:28,000 --> 00:06:32,290
"change blindness." 
In this case, it was blindness to changes 

85
00:06:32,290 --> 00:06:35,480
in color made while our attention was 
focused elsewhere. 

86
00:06:35,480 --> 00:06:40,100
But perceptions can also be influenced 
even when our attention 

87
00:06:40,100 --> 00:06:42,000
is focused directly on the item of interest. 

88
00:06:42,000 --> 00:06:46,200
Here's an example that I created 
specifically for this class. 

89
00:06:47,300 --> 00:06:51,400
This is just a black and white field with 
a tree and a fence. 

90
00:06:51,400 --> 00:06:54,610
Not very interesting or colorful, so I'll 

91
00:06:54,610 --> 00:06:59,060
add an apple to the tree. 
Well, that's a little large. 

92
00:06:59,060 --> 00:07:02,920
There, that's better. 
Now, there's an old expression about the 

93
00:07:02,920 --> 00:07:06,280
grass being greener on the other side of 
the fence, 

94
00:07:06,280 --> 00:07:09,650
meaning that the things we don't have 
often seem a little better, 

95
00:07:09,650 --> 00:07:14,720
and that's what I want you to focus on. 
I'm going to cover up the grass on the 

96
00:07:14,720 --> 00:07:17,870
other side of the fence with a nice 
bright color 

97
00:07:17,870 --> 00:07:23,130
and I want you to keep your eyes focused 
on the apple and your thoughts focused on 

98
00:07:23,130 --> 00:07:28,090
turning the grass green. 
And let me be clear. 

99
00:07:28,090 --> 00:07:30,780
I'm not going to change the black and 
white photo in any way. 

100
00:07:30,780 --> 00:07:35,290
I'm not going to add more apples to the 
tree or do anything to the fence. 

101
00:07:35,290 --> 00:07:40,110
I just want you to look at the apple and 
concentrate on the part of the photo 

102
00:07:40,110 --> 00:07:44,000
that's covered up. 
In a few seconds, I'll remove the cover 

103
00:07:44,000 --> 00:07:48,310
and when I do, please continue looking at 
the apple in the tree 

104
00:07:48,310 --> 00:07:51,740
and you should see something interesting. 
Ready? Here we go, 

105
00:07:51,740 --> 00:07:56,400
and again, please continue looking at the 
apple even after I've removed the cover. 

106
00:07:57,000 --> 00:08:00,200
One, two, three! 

107
00:08:01,175 --> 00:08:05,426
If you have color vision, the grass should 
indeed be greener on the other side 

108
00:08:05,426 --> 00:08:09,820
of the fence, at least for a little while. 
If you look away from the apple for a few 

109
00:08:09,820 --> 00:08:14,070
seconds, the effect will start to fade 
and you'll see that the photo is just the 

110
00:08:14,070 --> 00:08:18,360
same one that I showed you at first. 
What happened is that staring at the 

111
00:08:18,360 --> 00:08:23,240
magenta colored background temporarily 
fatigued your eyes' photoreceptors for 

112
00:08:23,240 --> 00:08:27,719
that color, which knocked your color 
perception a little bit off base. 

113
00:08:29,530 --> 00:08:33,860
If before this demonstration I'd asked 
you whether there was any circumstance 

114
00:08:33,860 --> 00:08:39,520
under which your eyes would have seen 
half of a black-and-white photo as green, 

115
00:08:39,520 --> 00:08:42,660
you probably would have said no. 
It sounds kind of crazy. 

116
00:08:42,660 --> 00:08:45,830
Why? 
Because we generally regard photographic 

117
00:08:45,830 --> 00:08:52,050
images as generating fixed perceptions. 
We don't think of our visual perceptions 

118
00:08:52,050 --> 00:08:56,880
as a combination of something out there 
with what's going on in our visual system 

119
00:08:56,880 --> 00:09:00,540
at the time of perception, 
but in fact, that's exactly what vision 

120
00:09:00,540 --> 00:09:06,280
is, and it's not just a matter of color. 
Here's a black and white example of how 

121
00:09:06,280 --> 00:09:10,349
our view of the world can be warped in 
under 30 seconds. 

122
00:09:11,390 --> 00:09:17,200
This example comes from Professor Michael 
Bach at the University of Freiburg in Germany.

123
00:09:17,200 --> 00:09:20,050
All you need to do is stare at the center 

124
00:09:20,050 --> 00:09:23,840
of the spirals and don't look away. 
Keep looking even though it's a little 

125
00:09:23,840 --> 00:09:26,880
bit hard to do. You don't need to 
concentrate. 

126
00:09:26,880 --> 00:09:30,680
Just let your eyes comfortably rest on 
the center of the spirals. 

127
00:09:30,680 --> 00:09:35,490
As you do this, your neural motion 
detectors will adapt to the movement, 

128
00:09:35,490 --> 00:09:39,300
and in just a few seconds, when you look 
away, your visual system will be 

129
00:09:39,300 --> 00:09:44,030
basically thrown into reverse, and you'll 
temporarily experience what's called a 

130
00:09:44,030 --> 00:09:46,800
"motion aftereffect." 
Okay? 

131
00:09:46,800 --> 00:09:48,840
You can look away now. 

132
00:09:50,400 --> 00:09:51,840
Pretty amazing, huh? 

133
00:09:51,840 --> 00:09:54,020
Let's pause for a wavy pop-up question. 

134
00:09:57,600 --> 00:10:00,020
So to summarize, our perceptions are a 

135
00:10:00,020 --> 00:10:04,600
joint function of what's going on out 
there and what's going on over here. 

136
00:10:04,600 --> 00:10:10,030
An optical illusion that offers, I think, 
a very nice metaphor for this interplay 

137
00:10:10,030 --> 00:10:17,140
is the rotating snakes illusion that was 
created by Akiyoshi Kitaoka of Japan. 

138
00:10:17,140 --> 00:10:21,820
The coils that seem to be moving are 
supposed to be snakes, but I actually 

139
00:10:21,820 --> 00:10:26,430
think of them as gear wheels. 
If you focus on just one corner of the 

140
00:10:26,430 --> 00:10:30,300
image, you can see that the gears aren't 
actually moving, 

141
00:10:30,300 --> 00:10:34,046
but our mental gears turn when we look at 
the image as a whole. 

142
00:10:34,046 --> 00:10:39,880
Even something as basic as line length 
can be distorted, as the famous Ponzo 

143
00:10:39,880 --> 00:10:43,400
illusion shows. 
Here you can see that the two lines are 

144
00:10:43,400 --> 00:10:47,620
identical in length, 
but when you change the context, most 

145
00:10:47,620 --> 00:10:52,699
people see the top line as slightly wider 
than the bottom line. 

146
00:10:52,699 --> 00:10:57,410
Illusions like this work in part because 
our visual system is evolved to see a 

147
00:10:57,410 --> 00:11:01,150
particular world -- 
a world in which, for example, converging 

148
00:11:01,150 --> 00:11:06,100
lines indicate distance. 
Our visual system isn't neutral, 

149
00:11:06,100 --> 00:11:10,930
and when we give it a world in which 
converging lines don't indicate distance, 

150
00:11:10,930 --> 00:11:14,850
it still makes the correction that the 
lines aren't of equal length even when, 

151
00:11:14,850 --> 00:11:18,650
in fact, they are. 
And this too is a good metaphor for how 

152
00:11:18,650 --> 00:11:21,870
people function with one another in daily 
life. 

153
00:11:21,870 --> 00:11:25,544
That is, people aren't neutral. 
Instead they come in with certain 

154
00:11:25,544 --> 00:11:31,260
predispositions, certain tendencies that 
lead them to process social information 

155
00:11:31,260 --> 00:11:35,720
in very particular ways. 
When you show people a photo of children, 

156
00:11:35,720 --> 00:11:41,500
they naturally process facial information 
to see whether the children are happy or sad. 

157
00:11:41,500 --> 00:11:44,350
When we see a close up of someone's face, 

158
00:11:44,350 --> 00:11:48,070
we wonder automatically what the person 
might be thinking. 

159
00:11:48,070 --> 00:11:50,955
Whether the person seems dangerous or 
friendly, and so on. 

160
00:11:50,955 --> 00:11:56,160
Usually, these processes are automatic and 
beneficial. 

161
00:11:56,160 --> 00:12:00,300
Certainly, there's an evolutionary benefit 
to knowing as rapidly as possible whether 

162
00:12:00,300 --> 00:12:04,600
your kids are okay, or whether a stranger 
poses a threat. 

163
00:12:04,600 --> 00:12:09,240
But when we're focused on one thing, that 
means we're not focused on another. 

164
00:12:09,240 --> 00:12:14,350
It means we can miss things, even fairly 
large things. 

165
00:12:14,350 --> 00:12:19,200
If we don't notice that someone's mouth 
is upside down or that someone's eyes 

166
00:12:19,200 --> 00:12:23,260
have been flipped around, what else are 
we missing when we try to read each 

167
00:12:23,260 --> 00:12:27,320
other's faces for much more subtle things 
and interact with each other 

168
00:12:27,320 --> 00:12:32,380
on a day-to-day basis? 
It's a question we'll explore in the next 

169
00:12:32,380 --> 00:12:36,825
two videos when we discuss confirmation 
biases and a close cousin, 

170
00:12:36,825 --> 00:12:38,690
self-fulfilling prophecies. 

