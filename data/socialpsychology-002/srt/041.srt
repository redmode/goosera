


1
00:00:06,750 --> 00:00:10,390
In the last lecture, we discussed 
categorical thinking, which is an 

2
00:00:10,390 --> 00:00:14,020
extremely valuable aspect of human 
thought. 

3
00:00:14,020 --> 00:00:17,650
When it comes to social groups, however, 
categorical thinking can give rise to 

4
00:00:17,650 --> 00:00:23,090
certain problems if people see sharp 
divisions between themselves and others 

5
00:00:23,090 --> 00:00:27,988
or between ingroups and outgroups. 
In social psychology, the term "ingroup" 

6
00:00:27,988 --> 00:00:33,000
simply refers to a group that you're a 
member of, and the term "outgroup" refers 

7
00:00:33,000 --> 00:00:37,452
to a group that you're not a member of. 
So, one person's ingroup may be another 

8
00:00:37,452 --> 00:00:42,385
person's outgroup, and vice versa. 
At any given moment, we're all members of 

9
00:00:42,385 --> 00:00:47,670
many, many ingroups and outgroups, 
and in some cases, our membership changes 

10
00:00:47,670 --> 00:00:51,581
over time. 
For example, a few years ago I joined the 

11
00:00:51,581 --> 00:00:56,950
"over 50 years old group," which had been 
an outgroup up until that point. 

12
00:00:56,950 --> 00:01:00,844
But here's the interesting thing: 
as soon as people start categorizing and 

13
00:01:00,844 --> 00:01:04,800
focusing on groups -- whether they're a 
member of the group or not -- certain 

14
00:01:04,800 --> 00:01:10,500
psychological processes come into play. 
First, focusing on a group can change 

15
00:01:10,500 --> 00:01:14,730
how we see the individuals or elements within 
that group. 

16
00:01:14,730 --> 00:01:19,800
And second, we often end up seeing one 
group as better or worse than another, 

17
00:01:19,800 --> 00:01:23,450
which is one reason why the U.S. 
Supreme Court ultimately gave up 

18
00:01:23,450 --> 00:01:27,530
on the idea of separate but equal treatment 
under the law. 

19
00:01:27,530 --> 00:01:31,542
As soon as you've got two or more groups -- 
especially social groups -- it's 

20
00:01:31,542 --> 00:01:37,200
very hard to treat them as equal. 
So let's examine each of the last two points, 

21
00:01:37,200 --> 00:01:43,000
beginning with the first one. 
In 1963, Henri Tajfel and Alan Wilkes 

22
00:01:43,000 --> 00:01:48,500
published a very thought-provoking, and 
now classic, study in which people were 

23
00:01:48,500 --> 00:01:54,500
shown eight lines and asked to estimate, 
in centimeters, how long the lines were. 

24
00:01:54,500 --> 00:01:58,110
The lines were all between 16 and 23 
centimeters -- 

25
00:01:58,110 --> 00:02:02,698
that is, 6 to 9 inches long. 
Here's a reproduction of the lines 

26
00:02:02,698 --> 00:02:07,492
in the exact proportions used. 
In some variations of the experiment the 

27
00:02:07,492 --> 00:02:12,700
lines were shown individually, and in others 
they were shown at the same time. 

28
00:02:12,700 --> 00:02:15,000
That turned out not to make a big 
difference. 

29
00:02:15,000 --> 00:02:18,300
What did make a big difference was 
grouping the lines. 

30
00:02:18,300 --> 00:02:20,712
In some experimental conditions, the four 

31
00:02:20,712 --> 00:02:24,000
shortest lines were each shown with the 
letter A 

32
00:02:24,000 --> 00:02:28,020
and the four longest lines were each 
shown with the letter B. 

33
00:02:28,020 --> 00:02:32,168
In other words, they were grouped by 
letter; the A group had shorter 

34
00:02:32,168 --> 00:02:36,386
lines than the B group. 
And when the lines were shown with letters, 

35
00:02:36,386 --> 00:02:39,500
even though they were the very 
same lines, 

36
00:02:39,500 --> 00:02:44,225
people tended to overestimate by 100% or 
more the difference in length 

37
00:02:44,225 --> 00:02:49,520
between the longest line in group A and the 
shortest line in group B --

38
00:02:49,520 --> 00:02:53,540
that is, they came to regard the lines in 
B as long lines, 

39
00:02:53,540 --> 00:02:58,600
and they rated the shortest line in that 
group a little bit longer than it really was. 

40
00:02:59,700 --> 00:03:02,660
Now, why should we care? 

41
00:03:02,660 --> 00:03:07,315
Well, just as Solomon Asch used a line 
judgement task to study conformity, 

42
00:03:07,315 --> 00:03:12,100
Tajfel used a line judgement task to 
study stereotyping. 

43
00:03:12,100 --> 00:03:18,300
Stereotyping? Really? 
What do line lengths have to do with stereotyping? 

44
00:03:18,500 --> 00:03:21,500
Here's what Tajfel and Wilkes wrote in 
their original report, 

45
00:03:21,500 --> 00:03:26,000
and I think it's interesting enough that 
I'd like to read you just a few lines. 

46
00:03:26,000 --> 00:03:29,300
They wrote: "These findings may possibly have 

47
00:03:29,300 --> 00:03:34,180
some fairly wide implications for a 
variety of judgment situations. 

48
00:03:34,180 --> 00:03:39,910
They represent, in a sense, a simplified 
exercise in stereotyping. 

49
00:03:39,910 --> 00:03:43,796
An essential feature of stereotyping is 
that of exaggerating 

50
00:03:43,796 --> 00:03:47,430
some differences between groups classified 
in a certain way 

51
00:03:47,430 --> 00:03:51,720
and of minimizing the same differences 
within such groups." 

52
00:03:51,720 --> 00:03:55,425
In other words, what Tajfel is saying is 
that the same dynamic that takes place 

53
00:03:55,425 --> 00:04:01,060
with respect to line judgments also 
applies to judgments of people. 

54
00:04:01,060 --> 00:04:06,000
And even though there are certain 
exceptions, to a great extent he was right. 

55
00:04:06,000 --> 00:04:08,600
According to social psychologist David Wilder, 

56
00:04:08,600 --> 00:04:11,400
who published a major review on 
the topic, 

57
00:04:11,400 --> 00:04:17,000
people "often assume similarities within 
groups and differences between groups 

58
00:04:17,000 --> 00:04:20,600
to a greater degree and across a broader 
range of characteristics 

59
00:04:20,600 --> 00:04:26,220
than is warranted by objective evidence." 
Once people are grouped, we tend to 

60
00:04:26,220 --> 00:04:31,594
exaggerate differences between groups and 
similarities within groups. 

61
00:04:31,594 --> 00:04:37,800
And I should add one historical footnote: 
Tajfel, who was a world renowned social 

62
00:04:37,800 --> 00:04:42,300
psychologist, became interested in 
stereotyping in large part because he 

63
00:04:42,300 --> 00:04:47,640
was a Polish-born Jew who the Germans 
took prisoner during World War II,

64
00:04:47,640 --> 00:04:52,720
and who lost every member of his 
immediate family to the Holocaust. 

65
00:04:52,720 --> 00:04:57,450
So once again, there's a link between 
social psychology and the Holocaust. 

66
00:04:57,450 --> 00:05:03,738
In the span of just 15 or 20 years, we 
have Tajfel, Milgram, Ash, Moscovici, and 

67
00:05:03,738 --> 00:05:07,740
others all trying to make sense of what 
happened in the Holocaust, 

68
00:05:07,740 --> 00:05:11,200
and find ways to prevent similar 
tragedies in the future. 

69
00:05:12,600 --> 00:05:17,100
Now, remember, I said that the formation of 
groups not only alters how people 

70
00:05:17,100 --> 00:05:22,300
think about the members, but that it also leads 
us to see one group as better or worse 

71
00:05:22,300 --> 00:05:27,180
than another, making it hard to have 
groups that are seen as separate but equal. 

72
00:05:28,310 --> 00:05:32,405
Well, in addition to his research on the 
first topic, Henri Tajfel made 

73
00:05:32,405 --> 00:05:36,920
major contributions to our understanding of the 
second topic. 

74
00:05:36,920 --> 00:05:41,198
And the way he did this was by developing 
a very creative research procedure 

75
00:05:41,198 --> 00:05:46,480
that's now known as the "minimal group paradigm." 
To see how it works, 

76
00:05:46,480 --> 00:05:51,260
I'd like you to take five seconds and estimate the number of 
dots on the following screen. 

77
00:05:51,260 --> 00:05:55,835
Don't count them -- just estimate what the 
number feels like. 

78
00:06:00,200 --> 00:06:03,500
Congratulations! Based on your answer, 
it looks like you're 

79
00:06:03,500 --> 00:06:06,540
an overestimator, and we all know it's better to be an 

80
00:06:06,540 --> 00:06:09,359
overestimator than an underestimator, 
right? 

81
00:06:10,770 --> 00:06:14,393
Well, no, we don't. 
To the best of my knowledge, there's no difference 

82
00:06:14,393 --> 00:06:18,900
in life outcome between people 
who overestimate or underestimate 

83
00:06:18,900 --> 00:06:23,400
the number of dots on a screen, but what
Tajfel found is that when 

84
00:06:23,400 --> 00:06:28,027
people were randomly assigned to receive feedback 
that they were overestimators 

85
00:06:28,027 --> 00:06:32,050
or underestimators, regardless of the estimates 
that they gave, 

86
00:06:32,050 --> 00:06:37,008
people tended to show "ingroup bias." 
That is, they tended to favor members of 

87
00:06:37,008 --> 00:06:40,460
their own group over members of the 
outgroup. 

88
00:06:40,460 --> 00:06:44,400
For example, when asked to allocate money 
to other overestimators 

89
00:06:44,400 --> 00:06:49,198
and underestimators, excluding themselves, 
Tajfel found that they tended to 

90
00:06:49,198 --> 00:06:54,240
give more money to members of their own group 
than members of the outgroup. 

91
00:06:54,240 --> 00:06:58,600
Likewise, a Canadian study found that if 
you ask people to go through a photo album 

92
00:06:58,600 --> 00:07:01,680
and pick five strangers who look like 
they're supporters of 

93
00:07:01,680 --> 00:07:06,370
each major political party, 
liberals and conservatives both guess 

94
00:07:06,370 --> 00:07:11,300
that relatively attractive people are 
supporters of their party. 

95
00:07:11,300 --> 00:07:14,795
That's ingroup bias. 
And by the way, the Wizard of Oz 

96
00:07:14,795 --> 00:07:18,500
replicated this study -- got the exact same 
results. 

97
00:07:19,750 --> 00:07:24,106
Joking aside, what makes Tajfel's results 
so striking is how little 

98
00:07:24,106 --> 00:07:29,250
it takes to trigger ingroup bias. 
What the minimal group procedure reveals 

99
00:07:29,250 --> 00:07:33,030
is that even when groups have no history 
of conflict and don't even 

100
00:07:33,030 --> 00:07:37,840
know each other, they still show ingroup 
favoritism. 

101
00:07:37,840 --> 00:07:41,470
In the words of social psychologist 
Marilyn Brewer, "Many forms of 

102
00:07:41,470 --> 00:07:46,300
discrimination and bias may develop not 
because outgroups are hated, but because 

103
00:07:46,300 --> 00:07:52,400
positive emotions such as admiration, 
sympathy, and trust are reserved for the 

104
00:07:52,400 --> 00:07:58,623
ingroup and withheld from outgroups." 
Even a chance event, such as 

105
00:07:58,623 --> 00:08:02,800
one group getting something that another doesn't as 
a result of a coin toss, 

106
00:08:02,800 --> 00:08:07,365
is enough to trigger ingroup bias. 
This result was found in a wonderful 

107
00:08:07,365 --> 00:08:12,100
little study published many years ago by 
Jacob Rabbie and Murray Horwitz, 

108
00:08:12,100 --> 00:08:18,066
the last study that I'll mention in this video. 
In the study, the experimenter divided 

109
00:08:18,066 --> 00:08:22,161
junior high school students in the 
Netherlands into four-person 

110
00:08:22,161 --> 00:08:27,322
"green groups" and four-person "blue groups" 
for what he said were administrative 

111
00:08:27,322 --> 00:08:30,800
reasons only. 
And even though the students didn't interact 

112
00:08:30,800 --> 00:08:34,500
with each other, he referred to 
the students as greens or blues 

113
00:08:34,500 --> 00:08:40,400
so that they had a group label. 
Then, after students completed several surveys, 

114
00:08:40,400 --> 00:08:43,600
the researcher told students in the experimental conditions 

115
00:08:43,600 --> 00:08:47,100
that he wanted to give them each a transistor radio 

116
00:08:47,100 --> 00:08:51,000
as a token of appreciation for participating 
in the study, 

117
00:08:51,000 --> 00:08:56,750
but unfortunately, that he only had enough 
radios for members of one group. 

118
00:08:56,750 --> 00:09:01,000
There were three experimental conditions. 
In the chance condition, he said: 

119
00:09:01,000 --> 00:09:06,100
"Perhaps the best thing we can do is flip a coin 
to decide which group gets the radios 

120
00:09:06,100 --> 00:09:10,770
and which does not. Okay? 
You want heads or tails?"

121
00:09:10,770 --> 00:09:14,900
The experimenter then tossed a coin, 
gave radios to the winning group, 

122
00:09:14,900 --> 00:09:19,870
and apologized to the losing group. 
In the experimenter condition, 

123
00:09:19,870 --> 00:09:25,260
the experimenter simply said: "Let's see -- 
I'll give the radios to this group." 

124
00:09:25,260 --> 00:09:29,180
He then gave the radios to a randomly 
selected group. 

125
00:09:29,180 --> 00:09:32,800
And in the group condition, the 
experimenter allowed a randomly chosen 

126
00:09:32,800 --> 00:09:37,990
group to vote by secret ballot to decide 
who would get the radios, 

127
00:09:37,990 --> 00:09:42,100
and regardless of the actual vote, he 
announced that they had voted to receive 

128
00:09:42,100 --> 00:09:46,312
the radios themselves. 
You can imagine how this would make the 

129
00:09:46,312 --> 00:09:49,692
other group feel! 
And the experimenter then delivered the 

130
00:09:49,692 --> 00:09:53,282
radios as before. 
In addition to these three conditions, 

131
00:09:53,282 --> 00:09:57,377
there was also a control condition in 
which students were divided 

132
00:09:57,377 --> 00:10:01,800
into groups but no mention was made of 
transistor radios. 

133
00:10:01,800 --> 00:10:05,933
What Rabbie and Horwitz found is that 
students in the experimental conditions 

134
00:10:05,933 --> 00:10:11,900
(who either received or were denied 
transistor radios) displayed ingroup bias, 

135
00:10:11,900 --> 00:10:15,800
whereas students in the control 
condition did not. 

136
00:10:15,800 --> 00:10:21,200
Students in the experimental conditions -- 
even in the chance condition -- rated the ingroup 

137
00:10:21,200 --> 00:10:26,800
as less likely to be hostile and 
more desirable to belong to than the outgroup. 

138
00:10:26,800 --> 00:10:30,400
And they also saw members of their own 
group as more responsible 

139
00:10:30,400 --> 00:10:34,800
and desirable as friends than members of the outgroup. 

140
00:10:34,800 --> 00:10:39,210
So even a coin toss was enough to trigger 
ingroup bias -- 

141
00:10:39,210 --> 00:10:44,890
a finding that suggests we need to be 
careful when we create groups or teams. 

142
00:10:44,890 --> 00:10:49,000
But we also need to be careful not to 
overinterpret or misinterpret 

143
00:10:49,000 --> 00:10:54,078
the lesson from minimal group research. 
The results do not mean that prejudice 

144
00:10:54,078 --> 00:10:58,990
and discrimination are merely a matter of 
superficial group dynamics. 

145
00:10:58,990 --> 00:11:05,900
Clearly, prejudice is also a function of 
culture, of politics, history, and economics. 

146
00:11:05,900 --> 00:11:10,000
And second, the fact that ingroup bias is 
so easy to trigger 

147
00:11:10,000 --> 00:11:13,457
doesn't mean that it's unavoidable or inevitable. 

148
00:11:13,457 --> 00:11:19,500
There are very effective techniques to 
reduce prejudice, stereotyping, and discrimination. 

149
00:11:19,500 --> 00:11:22,000
This week's reading assignment on prejudice suggests 

150
00:11:22,000 --> 00:11:26,603
some ways to do this, and thanks to the 
American Psychological Association, 

151
00:11:26,603 --> 00:11:30,500
there are also some 
translations of this particular reading, 

152
00:11:30,500 --> 00:11:39,000
in slightly shortened form, posted on the 
web at understandingprejudice.org/APA. 

153
00:11:39,000 --> 00:11:43,477
For this week's assigned reading, you're 
welcome to read any of these translations 

154
00:11:43,477 --> 00:11:47,908
or the original in English. 
Before you do that reading though, 

155
00:11:47,908 --> 00:11:52,036
let me suggest that you watch the next video, 
which will set up some of the topics 

156
00:11:52,036 --> 00:11:57,300
in that reading, because there's more to 
the story than ingroup bias. 

