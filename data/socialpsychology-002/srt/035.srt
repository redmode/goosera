



1
00:00:06,430 --> 00:00:08,194
Last time, we focused on thin slice

2
00:00:08,194 --> 00:00:12,352
research, showing that social judgments 
can be made with surprising speed and 

3
00:00:12,352 --> 00:00:17,500
accuracy even when they're based on just 
a single photo or the sound of somebody's 

4
00:00:17,500 --> 00:00:22,861
voice, or a few brief video clips of behavior. 

5
00:00:22,861 --> 00:00:27,922
But what about judgments made in person? 
For example, suppose you were applying 

6
00:00:27,922 --> 00:00:31,200
for a job. 
Person's a total stranger to you. 

7
00:00:31,200 --> 00:00:35,459
How long would it take the job 
interviewer to form an impression of you? 

8
00:00:36,510 --> 00:00:40,450
To help answer that question, I want to 
share an excerpt from a wonderful British 

9
00:00:40,450 --> 00:00:44,214
program called "The Human Zoo."

10
00:00:50 --> 00:00:51
 >> We want to learn more about first 

11
00:00:50,386 --> 00:00:54,450
impressions. 
How are they formed and how long 

12
00:00:54,450 --> 00:00:58,069
does it take? 
It's a big day for these three women. 

13
00:00:58,069 --> 00:01:02,440
They're competing for a secretarial job 
starting Monday morning,

14
00:01:02,440 --> 00:01:04,628
and we've been allowed in to film their 
interviews. 

15
00:01:04,628 --> 00:01:09,994
They've got 15 minutes each to prove 
their worth, or have they? 

16
00:01:09,994 --> 00:01:14,189
We're about to find out how long it 
really takes to make a lasting 

17
00:01:14,189 --> 00:01:17,710
impression. 
The candidates will be facing Judy Fisher, 

18
00:01:17,710 --> 00:01:21,910
a recruitment consultant 
with 30 years experience. 

19
00:01:21,910 --> 00:01:26,069
We've given her this dial, which is 
connected to our control room hidden away 

20
00:01:26,069 --> 00:01:31,030
in an adjoining office. 
There, we're effectively going to read 

21
00:01:31,030 --> 00:01:34,800
Judy's mind. 
This machine will make a trace of 

22
00:01:34,800 --> 00:01:39,160
her thought processes. 
If Judy's impressed, she'll turn her dial up. 

23
00:01:39,160 --> 00:01:41,840
That will produce a trace high on the screen. 

24
00:01:41,840 --> 00:01:44,790
If she's underwhelmed, she'll turn it down, 

25
00:01:44,790 --> 00:01:48,850
and the trace will sink. 
Everything will be scrutinized by 

26
00:01:48,850 --> 00:01:53,358
occupational psychologist Terry Kellard. 

27
00:01:56,160 --> 00:01:57,592
 >> Bridget. 
 >> Hi. 

28
00:01:57,592 --> 00:01:59,080
 >> Hello. 
I'm Judy Fisher. 

29
00:01:59,080 --> 00:02:01,740
Thanks for coming along, for being 
prepared to do this. 

30
00:02:01,740 --> 00:02:04,240
We're going to talk first, well, Bridget, 
a little bit about what you've done... 

31
00:02:04,240 --> 00:02:05,740
 >> Mm-hmm. 
 >> ...and about what you're looking for. 

32
00:02:05,740 --> 00:02:07,840
Then I'm going to tell you a little bit 
more about the job, and we can take it 

33
00:02:07,840 --> 00:02:08,617
from there. 
 >> Great. 

34
00:02:08,617 --> 00:02:09,610
 >> Okay? 
 >> Yeah. 

35
00:02:09,610 --> 00:02:13,910
 >> Bridget's been sitting down for just 
12 seconds and said all of five words, 

36
00:02:13,910 --> 00:02:18,360
but her trace has immediately gone up 
from an average reading of zero to an 

37
00:02:18,360 --> 00:02:22,058
impressive 56. 
 >> Let's just start with, I've got your 

38
00:02:22,058 --> 00:02:24,350
CV here. 
 >> Clearly she's got a positive 

39
00:02:24,350 --> 00:02:27,058
impression. 
That is the most important bit of the interview.

40
00:02:28,970 --> 00:02:32,790
 >> That would have been based on what 
she looks like. 

41
00:02:32,790 --> 00:02:34,385
She looks like a P.A. [Personal Assistant]

42
00:02:34,385 --> 00:02:35,640
 >> I just, I like that. 

43
00:02:35,640 --> 00:02:36,290
So, I'm good at that. 
 >> Mm-hmm. 

44
00:02:36,290 --> 00:02:40,660
 >> I'm efficient and fast, and I do as 
much of making sure everything's in 

45
00:02:40,660 --> 00:02:41,605
order as possible. 

46
00:02:41,605 --> 00:02:45,215
 >> The trace remains high right till 
the end, finishing up nearly a hundred. 

47
00:02:45,215 --> 00:02:48,590
Bridget's prospects look good. 
 >> Well, you seem very focused. 

48
00:02:48,590 --> 00:02:49,920
I'm sure you will. 
You've done really well. 

49
00:02:49,920 --> 00:02:50,842
 >> Thank you very much. 

50
00:02:50,842 --> 00:02:52,518
 >> Thank you so much. 
 >> That's great. 

51
00:02:52,518 --> 00:02:53,226
 >> Bye. 
 >> Bye-bye. 

52
00:02:53,226 --> 00:02:57,890
 >> When anybody meets anybody, anywhere, 
for the first time, in order to process 

53
00:02:57,890 --> 00:03:03,290
information, it is absolutely essential 
to make a judgment about this person. 

54
00:03:03,290 --> 00:03:07,920
So, essentially, what people do is make 
up their mind about a person and what 

55
00:03:07,920 --> 00:03:12,240
that person's like, generally within the 
first 15 seconds. 

56
00:03:12,240 --> 00:03:17,980
 >> Sometimes you could make your mind 
up so quickly that I just didn't give 

57
00:03:17,980 --> 00:03:22,510
them the benefit of any further doubt. 
 >> Well, I have to say that strong 

58
00:03:22,510 --> 00:03:27,250
aftershave and strong perfume, for that 
matter, are one of my bete noirs. 

59
00:03:27,250 --> 00:03:31,578
 >> I would be very reluctant to employ 
a man with fair eyelashes. 

60
00:03:31,578 --> 00:03:34,710
 >> I can think of instances where I 
can't remember anything about what 

61
00:03:34,710 --> 00:03:38,310
happened, or the person. 
All I can remember is this smell coming 

62
00:03:38,310 --> 00:03:41,480
towards me and wanting to get away from 
it badly. 

63
00:03:41,480 --> 00:03:44,890
 >> â¦and all I could see through the 
glass table is the fact that she hadn't 

64
00:03:44,890 --> 00:03:50,960
shaved her legs and there were these 
black hairs sort of just like sprouting 

65
00:03:50,960 --> 00:03:53,280
through the tights, and I couldn't get 
beyond that. 

66
00:03:53,280 --> 00:03:56,090
 >> Not crazy about gray shoes, I'm 
afraid. 

67
00:03:56,090 --> 00:03:58,460
I don't quite know why that I, I feel 
that way. 

68
00:03:58,460 --> 00:04:00,585
 >> I mean, she couldn't have shaved her 
legs for weeks. 

69
00:04:00,585 --> 00:04:03,180
 >> Lucy, hello. 
I'm Judy Fisher. 

70
00:04:03,180 --> 00:04:04,900
Thanks so much for coming along to see 
me. 

71
00:04:04,900 --> 00:04:06,620
We're going to just talk a little bit 
about... 

72
00:04:06,620 --> 00:04:07,790
 >> Lucy looks fine. 

73
00:04:07,790 --> 00:04:12,872
So her trace starts slightly above 
average, but only just, and it doesn't 

74
00:04:12,872 --> 00:04:16,000
last. 
 >> ...it was my favorite subject at 

75
00:04:16,000 --> 00:04:17,715
school. 
 >> Listen to her voice. 

76
00:04:17,715 --> 00:04:22,938
It's beginning to lack expression. 
She's licking her lips, signs of tension. 

77
00:04:22,938 --> 00:04:24,113
 >> ...long-term. 
Something... 

78
00:04:24,113 --> 00:04:27,650
 >> At best, the trace is mediocre. 
At worst, it drops. 

79
00:04:27,650 --> 00:04:30,570
 >> â¦interested in alternative health and 

80
00:04:30,570 --> 00:04:34,710
nutrition,
and one day I'd like to make my career 

81
00:04:34,710 --> 00:04:38,430
out of that. 
 >> In what do you want? In what side of it? 

82
00:04:38,430 --> 00:04:41,880
 >> Well, umm, possibly working from, working 

83
00:04:41,880 --> 00:04:47,230
from home, and perhaps in a clinic as well 
as an alternative health therapist and 

84
00:04:47,230 --> 00:04:50,460
adviser. 
 >> Lucy's lifestyle ambitions don't 

85
00:04:50,460 --> 00:04:53,940
impress. 
Her trace sinks from plus 30 to minus 30 

86
00:04:53,940 --> 00:04:57,930
in 20 seconds. 
But the next candidate's trace will sink 

87
00:04:57,930 --> 00:04:58,710
like a stone. 

88
00:05:01,240 --> 00:05:02,820
 >> Now this should be very interesting. 

89
00:05:05,050 --> 00:05:09,570
 >> Shopping bags, an obvious mistake. 
But how quickly and dramatically will 

90
00:05:09,570 --> 00:05:11,570
they affect her trace? 

91
00:05:14,920 --> 00:05:16,590
 >> Valeria, hello. I'm Judy Fischer. Good to meet you. 

92
00:05:16,590 --> 00:05:18,010
 >> Hi.  >> Thanks for coming in. 

93
00:05:18,010 --> 00:05:22,108
 >> In three seconds, it's plummeted to 
a feeble minus 50. 

94
00:05:22,108 --> 00:05:25,070
Neither Lucy nor Bridget ever sank that 
low. 

95
00:05:25,070 --> 00:05:31,130
 >> Carrying shopping bags, low levels 
of eye contact, and nothing positive has 

96
00:05:31,130 --> 00:05:33,070
happened yet. 
 >> And on the creative side, 

97
00:05:33,070 --> 00:05:34,750
you're a keen 
cinema and theatre goer? 

98
00:05:34,750 --> 00:05:37,151
 >> Mm-hmm. Yeah, very much. 
 >> You go a lot? 

99
00:05:37,151 --> 00:05:39,150
 >> Very much. 
 >> What was the last thing you saw in 

100
00:05:39,150 --> 00:05:43,878
the cinema? 
 >> I haven't been for ages, actually. 

101
00:05:43,878 --> 00:05:48,270
What was the last thing I saw? 
 >> And what about your sort of 

102
00:05:48,270 --> 00:05:50,242
attention to detail? 
You're quite thorough? 

103
00:05:50,242 --> 00:05:52,750
 >> Yeah, yeah. 
 >> Because you have got three spelling 

104
00:05:52,750 --> 00:05:54,454
mistakes in your CV. 
 >> Do I? 

105
00:05:54,454 --> 00:05:57,875
 >> Yes. 
 >> If you convey a bad first 

106
00:05:57,875 --> 00:06:02,404
impression, you have to work very, very, 
very hard indeed. 

107
00:06:02,404 --> 00:06:08,660
There was a little bit of research that 
showed that it takes eight positive 

108
00:06:08,660 --> 00:06:14,320
pieces of positive information before 
you can overcome a first, bad first 

109
00:06:14,320 --> 00:06:18,040
impression. 
My view would be that even that's very, 

110
00:06:18,040 --> 00:06:21,485
very difficult. 
 >> Unlike the other two candidates, 

111
00:06:21,485 --> 00:06:26,114
Valeria was, in fact, an actress planted 
by us without Judy's knowledge. 

112
00:06:26,114 --> 00:06:34,470
I'd actually been told by Terry to fidget, 
to not look her in the eye, to not smile 

113
00:06:34,470 --> 00:06:40,210
and to move around in my chair quite a 
bit, and just be evasive, in general. 

114
00:06:40,210 --> 00:06:44,860
Looking at the three graphs 
together Valeria came a miserable third 

115
00:06:44,860 --> 00:06:47,252
behind Bridget first and Lucy second, 

116
00:06:47,252 --> 00:06:51,083
but look closely at the first five 
seconds. 

117
00:06:51,083 --> 00:06:55,544
The three women have already been ranked 
in order, and the order never alters. 

118
00:06:55,544 --> 00:07:00,550
 >> I didn't find it very easy to get 
back on a, on a snap judgment that I had 

119
00:07:00,550 --> 00:07:03,050
made, and possibly I had lost some very 
fine candidates. 

120
00:07:03,050 --> 00:07:07,520
 >> It sounds very unfair, but the 
moment somebody walks in the room for an 

121
00:07:07,520 --> 00:07:11,780
interview, I know or I can guess quite a 
lot about them just from 

122
00:07:11,780 --> 00:07:12,900
the way they're dressed. 

123
00:07:14,385 --> 00:07:16,370
 >> Bridget, hello. 
Well done. 

124
00:07:16,370 --> 00:07:19,490
You did really well in your interview. 
You were absolutely excellent, and we're 

125
00:07:19,490 --> 00:07:20,836
delighted to offer you the job. 

126
00:07:20,836 --> 00:07:23,129
So, many congratulations. 
 >> Thanks very much. 

127
00:07:23,129 --> 00:07:24,100
 >> And the very best of luck. 

128
00:07:24,100 --> 00:07:25,550
 >> That's great. Thank you. 

129
00:07:26,000 --> 00:07:30,430
 >> Bridget's been in her new job for 
ten months now and is doing well. 

130
00:07:30,430 --> 00:07:34,500
 >> I was quite surprised that it showed the 
results so quickly, the first five 

131
00:07:34,500 --> 00:07:36,370
seconds, though 
some of them were very obvious. 

132
00:07:36,370 --> 00:07:40,150
Valeria was, within the first five 
seconds, was going to have to fight very 

133
00:07:40,150 --> 00:07:43,450
hard to get better. 
But first impressions are very, very important. 

134
00:07:44,150 --> 00:07:47,620
 >> What this video suggests and what 

135
00:07:47,620 --> 00:07:51,760
research on impression formation has 
found, is that social impressions are 

136
00:07:51,760 --> 00:07:56,290
formed with amazing speed, even when 
people have plenty of time to ask each 

137
00:07:56,290 --> 00:07:59,970
other questions. 
In other words, the first few seconds of 

138
00:07:59,970 --> 00:08:05,000
an interaction between strangers is often 
the most important time period their 

139
00:08:05,000 --> 00:08:10,100
relationship will ever have. 
It may, for example, determine whether 

140
00:08:10,100 --> 00:08:13,750
they'll work together, 
whether they'll become friends, date each 

141
00:08:13,750 --> 00:08:18,100
other, or even get married. 
Yet, there's a problem if we want to 

142
00:08:18,100 --> 00:08:22,815
understand what other people think of us 
during that important first impression. 

143
00:08:22,815 --> 00:08:27,220
Precisely because we don't have a close 
relationship with people we haven't met 

144
00:08:27,220 --> 00:08:29,640
before, 
they're probably not going to openly 

145
00:08:29,640 --> 00:08:33,790
share what they think of us, and they're 
certainly not going to offer us tips on 

146
00:08:33,790 --> 00:08:38,970
how we can make a better impression, 
but in this class, we can overcome that 

147
00:08:38,970 --> 00:08:42,580
obstacle because we're a learning 
community set up to help each other, 

148
00:08:42,580 --> 00:08:47,690
and that's exactly what the next 
assignment is designed to do. 

149
00:08:47,690 --> 00:08:52,360
The assignment, which I call the Social 
Challenge, is to create a Social 

150
00:08:52,360 --> 00:08:57,480
Psychology Network page that leads 
strangers, in this case classmates, to 

151
00:08:57,480 --> 00:09:02,440
form a positive impression of you when 
they first encounter your page. 

152
00:09:02,440 --> 00:09:06,290
The assignment will be peer-assessed, 
which means not only that you'll receive 

153
00:09:06,290 --> 00:09:10,400
a score, but that you'll receive specific 
suggestions on how to improve the 

154
00:09:10,400 --> 00:09:14,520
impression you're making. 
If you post photos of yourself, which 

155
00:09:14,520 --> 00:09:18,610
ones do people like? 
If you write a biographical description, 

156
00:09:18,610 --> 00:09:22,686
what do people think of it? 
Should you try to be funny, polite, 

157
00:09:22,686 --> 00:09:28,311
artistic, knowledgeable? 
The reading for next week, which focuses 

158
00:09:28,311 --> 00:09:32,630
on the self in the social world, 
fits perfectly with this assignment. 

159
00:09:32,630 --> 00:09:36,770
So, once the next set of lectures and 
reading materials are released to the 

160
00:09:36,770 --> 00:09:41,370
class, please use the social psychology 
you're learning when you create a page 

161
00:09:41,370 --> 00:09:44,850
that presents yourself to the social 
world. 

162
00:09:44,850 --> 00:09:48,650
When I release the second week of 
lectures and reading material, I'll also 

163
00:09:48,650 --> 00:09:52,750
post an FAQ page with details on the 
assignment. 

164
00:09:52,750 --> 00:09:56,530
For example, explaining how the 
assignment will work if you're not a 

165
00:09:56,530 --> 00:10:01,020
member of Social Psychology Network. 
And now that the course is underway, the 

166
00:10:01,020 --> 00:10:05,580
weekly release of materials will follow a 
regular timetable so that the discussion 

167
00:10:05,580 --> 00:10:09,240
forums can include whatever the topics 
are for that week. 

168
00:10:10,640 --> 00:10:15,170
So to sum up the second assignment offers 
a really unique opportunity to learn 

169
00:10:15,170 --> 00:10:18,980
about social psychology, 
to learn about the social impressions 

170
00:10:18,980 --> 00:10:22,250
that you make, 
and end up, after the course is over, 

171
00:10:22,250 --> 00:10:26,140
with a web page that other people who 
also know something about social 

172
00:10:26,140 --> 00:10:31,820
psychology have helped you to perfect. 
But now let me just end by congratulating 

173
00:10:31,820 --> 00:10:35,470
you for completing the last lecture of 
week number one. 

174
00:10:35,470 --> 00:10:39,420
I'm assuming, of course, that you're 
watching the lectures in order, that's 

175
00:10:39,420 --> 00:10:42,940
important. 
Also, let me remind you to complete the 

176
00:10:42,940 --> 00:10:48,280
first reading assignment and the Random 
Assignment Assignment by the deadline 

177
00:10:48,280 --> 00:10:53,920
that's given in the web course page. 
And here is a pop-up question to finish 

178
00:10:53,920 --> 00:10:57,790
out the lecture. 
I will see you next week. 



