




1
00:00:06,719 --> 00:00:08,765
I'd like to begin this video with an item 

2
00:00:08,765 --> 00:00:13,064
from the Snapshot Quiz: the 
question about four playing cards. 

3
00:00:13,064 --> 00:00:17,630
So if you haven't completed the quiz yet, 
please stop this video. 

4
00:00:17,630 --> 00:00:19,180
Truly, stop the video. 

5
00:00:19,200 --> 00:00:21,700
Stop! Stop the video!

6
00:00:21,800 --> 00:00:25,812
It's very important that you complete the 
quiz because it sets up not just 

7
00:00:25,812 --> 00:00:29,345
the rest of this video, but the rest of the 
course. 

8
00:00:29,345 --> 00:00:35,470
The Snapshot Quiz is designed to be fun 
and interesting, and it's there for you, 

9
00:00:35,470 --> 00:00:37,340
not for me. 
I'm not selling anything. 

10
00:00:37,340 --> 00:00:42,340
I'm not going to share anybody's answers 
or identity or anything of that nature. 

11
00:00:42,340 --> 00:00:47,150
It's there to make the course interactive 
and engaging and educational. 

12
00:00:47,150 --> 00:00:52,320
So, my advice, friendly advice: Stop, 
stop, stop, stop, 

13
00:00:52,320 --> 00:00:56,580
and complete the quiz if you want the 
full experience of this course. 

14
00:00:56,580 --> 00:01:02,260
I really think you'll get more out of it. 
Anyway, here is the quiz item that I 

15
00:01:02,260 --> 00:01:07,240
wanted to consider. 
Suppose that each of these playing cards, 

16
00:01:07,240 --> 00:01:13,810
showing A, D, 4, and 7 has a letter on 
one side and a number on the other. 

17
00:01:13,810 --> 00:01:19,800
And someone tells you, "If a card has a 
vowel on one side, it has an even number 

18
00:01:19,800 --> 00:01:22,730
on the other side." 
Question: 

19
00:01:22,730 --> 00:01:26,850
Which of the cards would you need to flip 
over if you wanted to find out whether 

20
00:01:26,850 --> 00:01:32,170
the person's lying? 
Seems like a fairly easy question, but 

21
00:01:32,170 --> 00:01:36,540
for most of us, it's not. 
If you completed the Snapshot Quiz, you 

22
00:01:36,540 --> 00:01:40,730
should now be able to see your answer in 
the following screen. 

23
00:01:43,660 --> 00:01:49,590
The correct answer is A and 7, because 
the only way to falsify a statement of 

24
00:01:49,590 --> 00:01:57,550
the form, "If X, then Y" (if vowel, then even 
number) is to find an instance of X and 

25
00:01:57,550 --> 00:02:04,040
not Y (that is, vowel and odd number). 
Cards without either a vowel or an odd number 

26
00:02:04,040 --> 00:02:07,860
aren't relevant. 
For example, the card with a D can't tell 

27
00:02:07,860 --> 00:02:12,830
you anything about cards with a vowel, so 
there's no need to turn it over. 

28
00:02:12,830 --> 00:02:15,550
If this seems a little confusing, you're 
not alone. 

29
00:02:15,550 --> 00:02:20,570
When Peter Wason and Phil Johnson-Laird 
put this type of question to 128 

30
00:02:20,570 --> 00:02:25,880
university students, they found that 
answers like A and 4, or even just A, 

31
00:02:25,880 --> 00:02:33,490
were the most common responses, given by 
59 students and 42 students, respectively. 

32
00:02:33,490 --> 00:02:39,310
In other words, cards capable of 
confirming the rule were those most often 

33
00:02:39,310 --> 00:02:41,880
seen as the cards needed to test the 
rule, 

34
00:02:41,880 --> 00:02:46,515
which isn't the same thing, and which 
shows what psychologists call a 

35
00:02:46,515 --> 00:02:52,700
"confirmation bias." 
A confirmation bias is a preference for 

36
00:02:52,700 --> 00:02:57,280
information that's consistent with a 
preconception, rather than information 

37
00:02:57,280 --> 00:03:00,880
that challenges it. 
So let me give you another example. 

38
00:03:00,880 --> 00:03:06,040
This is a problem that I adapted from the 
book Human Inference, by Dick Nisbett and 

39
00:03:06,040 --> 00:03:10,150
Lee Ross. 
Suppose that some researchers are 

40
00:03:10,150 --> 00:03:14,250
interested in whether a particular 
symptom happens to be associated with a 

41
00:03:14,250 --> 00:03:19,370
very common disease. 
So they do a year-long study of 150 

42
00:03:19,370 --> 00:03:21,908
people and they find the following 
results. 

43
00:03:21,908 --> 00:03:27,920
Two questions: 
First, in a table like this, which cells 

44
00:03:27,920 --> 00:03:31,660
do you need to examine in order to tell 
whether the symptom is associated with 

45
00:03:31,660 --> 00:03:36,970
the disease? 
And second, in this particular case, is 

46
00:03:36,970 --> 00:03:40,850
the symptom positively associated with 
the disease? 

47
00:03:40,850 --> 00:03:44,990
Let's pause to see how you answered these 
questions in the Snapshot Quiz. 

48
00:03:48,519 --> 00:03:52,610
Many people answer that the present-present cell in the upper left area of 

49
00:03:52,610 --> 00:03:57,440
the table, or maybe that cell combined 
with the absent-absent cell in the lower 

50
00:03:57,440 --> 00:04:02,790
right, are what you would need to examine, 
and they say that in this particular 

51
00:04:02,790 --> 00:04:07,390
instance, the symptom is positively 
associated with the disease, because in 

52
00:04:07,390 --> 00:04:13,640
most cases (80 out of 150 times), the 
symptom and disease occur together, 

53
00:04:13,640 --> 00:04:18,800
and in another 10 cases, the disease is 
absent when the symptom's absent. 

54
00:04:18,800 --> 00:04:24,240
So, the presence or absence of the symptom 
usually matches the presence or absence 

55
00:04:24,240 --> 00:04:28,400
of the disease. 
All told, there's a match 60% of the 

56
00:04:28,400 --> 00:04:33,930
time (90 out of 150 cases). 
So it seems like the disease and symptom 

57
00:04:33,930 --> 00:04:37,800
are related. 
But that's not actually a dependable way 

58
00:04:37,800 --> 00:04:42,310
to solve a problem like this. 
In truth, all four cells of the table are 

59
00:04:42,310 --> 00:04:45,090
needed to know whether the symptom and 
disease are related, 

60
00:04:45,090 --> 00:04:49,990
even though it feels like the present-present cell is what's most important. 

61
00:04:49,990 --> 00:04:55,130
To tell whether the symptom and disease 
are associated, you need to compare the 

62
00:04:55,130 --> 00:04:59,000
chances of people having the disease when 
the symptom's present (which is 80 out of 

63
00:04:59,000 --> 00:05:05,000
120 cases, or 67%) and the chances of 
people having the disease when the 

64
00:05:05,000 --> 00:05:12,130
symptom is absent (which is 20 out of 30 
cases -- still 67%). 

65
00:05:13,130 --> 00:05:16,200
And, of course, this gives us the answer 
to the second question. 

66
00:05:16,200 --> 00:05:20,660
In this particular case, the disease and 
the symptom are entirely unrelated 

67
00:05:20,660 --> 00:05:23,990
because the chances of having the disease 
are the same whether you've got the 

68
00:05:23,990 --> 00:05:27,490
symptom or not. 
It's 67% in both cases. 

69
00:05:27,490 --> 00:05:31,860
This sort of thinking does not come 
naturally to most people. 

70
00:05:31,860 --> 00:05:37,190
For instance, consider another example 
adapted from the book by Dick Nisbett and 

71
00:05:37,190 --> 00:05:41,770
Lee Ross: the question of whether God 
answers prayers. 

72
00:05:41,770 --> 00:05:45,820
Many people would say yes, because 
they've prayed for something (for example, 

73
00:05:45,820 --> 00:05:50,245
the survival of a loved one who's on the 
edge of death), and sure enough, it's come 

74
00:05:50,245 --> 00:05:54,220
about. But that's only the present-present cell 

75
00:05:54,220 --> 00:05:57,700
of the table. 
To fully answer the question of whether 

76
00:05:57,700 --> 00:06:01,450
people are more likely to survive when 
they're prayed for, you would need to 

77
00:06:01,450 --> 00:06:07,150
know how often people on the edge of 
death die despite being prayed for, 

78
00:06:07,150 --> 00:06:11,650
how often people on the edge of death 
survive when no one prays for them, and 

79
00:06:11,650 --> 00:06:17,120
believe it or not, how many people on the 
edge of death die when no one prays for them 

80
00:06:17,120 --> 00:06:19,530
(controlling, of course, for all sorts of other factors

81
00:06:19,530 --> 00:06:24,780
that might have an effect). 
Obviously, this kind of approach is very 

82
00:06:24,780 --> 00:06:28,150
different than how most of us operate in 
daily life. 

83
00:06:28,150 --> 00:06:32,230
And please understand this is not a case 
against the value of prayer. 

84
00:06:32,230 --> 00:06:36,820
The same principle would operate if 
somebody were to lose faith in prayer 

85
00:06:36,820 --> 00:06:39,930
because they prayed for something that 
didn't come about. 

86
00:06:39,930 --> 00:06:42,440
That would also be just one cell in the 
table. 

87
00:06:42,440 --> 00:06:46,390
The point is that all four cells are 
relevant, 

88
00:06:46,390 --> 00:06:49,800
and yet, when we have a theory about 
something and we get some supporting 

89
00:06:49,800 --> 00:06:53,650
evidence, we typically conclude that the 
theory was correct. 

90
00:06:53,650 --> 00:06:58,860
We don't go out of our way to seek 
disconfirming evidence and examine all 

91
00:06:58,860 --> 00:07:04,070
four cells of the table. 
So let's pause for a quick check to make 

92
00:07:04,070 --> 00:07:07,590
sure you can tell whether two variables, 
like the presence of a symptom and 

93
00:07:07,590 --> 00:07:10,690
disease, are statistically related to each 
other. 

94
00:07:14,150 --> 00:07:18,090
Now that we've talked about confirmation 
biases, you might be wondering, "Why are 

95
00:07:18,090 --> 00:07:22,820
they of concern to social psychologists?" 
Well, there are many reasons, but here's 

96
00:07:22,820 --> 00:07:26,280
just one: 
When individuals and groups interact with 

97
00:07:26,280 --> 00:07:29,590
each other, they usually have 
expectations, 

98
00:07:29,590 --> 00:07:34,860
and there's plenty of research that suggests that those 
expectations do not always receive a fair 

99
00:07:34,860 --> 00:07:38,590
test. 
Instead, people tend to seek out evidence that 

100
00:07:38,590 --> 00:07:44,030
confirms their expectations, and they 
give greater weight to that evidence than 

101
00:07:44,030 --> 00:07:46,718
evidence that would disconfirm their 
expectations. 

102
00:07:46,718 --> 00:07:51,870
Counter-evidence, if it's even noticed at 
all, is usually very easy to explain away. 

103
00:07:51,870 --> 00:07:55,280
So the very same mechanism that operates 

104
00:07:55,280 --> 00:08:01,040
in Wason's research also operates when 
people have stereotypes about women or 

105
00:08:01,040 --> 00:08:04,340
about racial minorities or about 
professors. 

106
00:08:04,340 --> 00:08:09,415
People focus mainly on confirming 
evidence and end up perpetuating the 

107
00:08:09,415 --> 00:08:14,880
stereotypes, or the preconceptions, or 
social expectations that they have, 

108
00:08:14,880 --> 00:08:19,109
especially when they're not highly 
motivated to question those beliefs. 

109
00:08:20,410 --> 00:08:24,520
So confirmation biases can have important 
consequences, 

110
00:08:24,520 --> 00:08:28,930
but they're only half the equation. 
Social expectations not only lead us to 

111
00:08:28,930 --> 00:08:34,595
seek out confirming evidence -- they can 
have an effect on the person about whom we hold the 

112
00:08:34,595 --> 00:08:38,280
expectation. 
In other words, social expectations 

113
00:08:38,280 --> 00:08:42,920
affect not only the person who holds 
them but the other side as well. 

114
00:08:42,920 --> 00:08:47,550
And it's that side of the equation that 
we'll focus on in the next video. 

