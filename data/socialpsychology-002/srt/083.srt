

1
00:00:14,100 --> 00:00:15,700
Welcome back.

2
00:00:15,700 --> 00:00:17,650
When you ask people what the most important 

3
00:00:17,650 --> 00:00:21,200
social problems are facing society and the world, some of 

4
00:00:21,200 --> 00:00:25,830
the most common answers you get are war and terrorism, poverty, 

5
00:00:25,830 --> 00:00:31,666
prejudice and social injustice, violations of human rights, civil rights, 

6
00:00:31,666 --> 00:00:37,330
women's rights, human trafficking, partner violence, child abuse.

7
00:00:37,330 --> 00:00:40,800
What's interesting about this list is that every single problem

8
00:00:40,800 --> 00:00:44,066
on it arises, in part, from a lack of empathy.

9
00:00:44,066 --> 00:00:46,166
And every problem would be greatly reduced

10
00:00:46,166 --> 00:00:49,726
if people felt more empathy toward one another.

11
00:00:49,726 --> 00:00:52,132
Now, what do I mean by empathy?

12
00:00:52,132 --> 00:00:56,165
Well, as you'll see later in this video, in an animated guest lecture

13
00:00:56,165 --> 00:00:59,792
by author Roman Krznaric, psychologists generally think

14
00:00:59,792 --> 00:01:02,732
about empathy in one of two ways.

15
00:01:02,732 --> 00:01:06,800
The first, which is sometimes called affective empathy, 

16
00:01:06,800 --> 00:01:10,500
has to do with feeling the emotions that someone else feels -- 

17
00:01:10,500 --> 00:01:14,166
what's sometimes called emotional matching.

18
00:01:14,166 --> 00:01:16,599
If you feel happy and I empathize, 

19
00:01:16,599 --> 00:01:21,733
I feel happy. If you feel upset, I feel upset, too.

20
00:01:21,733 --> 00:01:26,133
The other type of empathy, sometimes called cognitive empathy, 

21
00:01:26,133 --> 00:01:30,166
has to do with imagining how someone else thinks or feels,

22
00:01:30,166 --> 00:01:32,330
or imagining what you would think or feel 

23
00:01:32,330 --> 00:01:35,133
if you were in that person's position.

24
00:01:35,133 --> 00:01:37,666
With cognitive empathy, you're taking the perspective

25
00:01:37,666 --> 00:01:39,933
of someone else regardless of whether 

26
00:01:39,933 --> 00:01:43,300
you're experiencing the same emotions or not.

27
00:01:43,300 --> 00:01:45,160
And of course, thinking and feeling

28
00:01:45,160 --> 00:01:48,000
usually go together, so some psychologists talk

29
00:01:48,000 --> 00:01:50,533
about empathy as one general trait

30
00:01:50,533 --> 00:01:54,090
that has both affective and cognitive components.

31
00:01:55,433 --> 00:01:58,033
Do these distinctions really matter?

32
00:01:58,033 --> 00:01:59,867
Well, in some cases they do.

33
00:01:59,867 --> 00:02:03,733
For instance, a 2010 brain imaging study found that 

34
00:02:03,733 --> 00:02:07,033
when people saw racial ingroup and outgroup members 

35
00:02:07,033 --> 00:02:11,199
who were suffering -- for example, from a natural disaster -- 

36
00:02:11,199 --> 00:02:14,533
affective empathy was common across the board, 

37
00:02:13,830 --> 00:02:19,080
but people's level of cognitive empathy depended on their group identity.

38
00:02:20,167 --> 00:02:24,150
When people lack empathy -- whether cognitive or affective empathy -- 

39
00:02:24,150 --> 00:02:26,933
all sorts of things can go wrong. 

40
00:02:26,933 --> 00:02:32,000
A lack of empathy is associated with prejudice, aggression, bullying, 

41
00:02:32,000 --> 00:02:37,599
child molestation, and abusive parenting, just to name a few examples.

42
00:02:37,599 --> 00:02:40,533
For instance, in a study that compared abusive and

43
00:02:40,533 --> 00:02:44,000
non-abusive mothers, empathy scores accurately

44
00:02:44,000 --> 00:02:48,300
classified 80% of abusive mothers.

45
00:02:48,300 --> 00:02:53,367
In fact, empathy scores predicted child abuse even better than life stress, 

46
00:02:53,367 --> 00:02:56,433
contrary to the stereotype that abusive parents are so 

47
00:02:56,433 --> 00:03:00,767
stressed out that at some point they snap and lose control.

48
00:03:00,767 --> 00:03:02,833
What's more important than stress is

49
00:03:02,833 --> 00:03:05,733
whether parents can empathize with children,

50
00:03:05,733 --> 00:03:10,680
see the world from a kid's perspective, and feel what children feel.

51
00:03:11,833 --> 00:03:15,167
Interestingly, Barack Obama has, for many years,

52
00:03:15,167 --> 00:03:18,400
commented on the societal importance of empathy.

53
00:03:18,400 --> 00:03:21,233
I'll play for you two brief excerpts of commencement

54
00:03:21,233 --> 00:03:23,900
speeches in which he talks about this.

55
00:03:23,900 --> 00:03:28,600
The first is from a speech in 2006, when he addressed graduating students from

56
00:03:28,600 --> 00:03:35,567
Northwestern University and spoke about the need to overcome deficits in empathy.

57
00:03:35,567 --> 00:03:41,699
>> You know, there's a lot of talk in this country about the federal deficit.

58
00:03:41,699 --> 00:03:44,200
And I think it's important for us to talk about that, 

59
00:03:44,200 --> 00:03:48,167
but I think we should talk more about another deficit,

60
00:03:48,167 --> 00:03:51,233
what I call the "empathy deficit" -- 

61
00:03:51,233 --> 00:03:54,833
the ability to put ourselves in somebody else's shoes, 

62
00:03:54,833 --> 00:03:58,833
to see the world through those who are different from us: 

63
00:03:58,833 --> 00:04:02,900
the child who's hungry, the laid-off steel worker, 

64
00:04:02,900 --> 00:04:06,400
the immigrant woman who's cleaning up your dorm room. 

65
00:04:07,267 --> 00:04:09,900
>> The next clip comes from a 2008 commencement

66
00:04:09,900 --> 00:04:12,167
speech that he gave to students at the school

67
00:04:12,167 --> 00:04:15,167
where I teach: Wesleyan University.

68
00:04:15,167 --> 00:04:16,867
At one point, you can even see me in

69
00:04:16,867 --> 00:04:20,600
the faculty area about six meters away from the podium.

70
00:04:20,600 --> 00:04:22,533
And I've included in the video a little

71
00:04:22,533 --> 00:04:25,500
hint about what I was thinking at the time.

72
00:04:25,500 --> 00:04:28,833
One thing that I especially like about this clip, is that he focuses

73
00:04:28,833 --> 00:04:31,300
on the positive consequences of empathy 

74
00:04:31,300 --> 00:04:35,280
in terms of a vehicle for self-improvement. Take a look.

75
00:04:36,400 --> 00:04:37,367
>> There were many times where

76
00:04:37,367 --> 00:04:42,300
I wasn't sure where I was going, or what I was going to do with my life.

77
00:04:43,133 --> 00:04:45,700
But during my first two years of college,

78
00:04:45,700 --> 00:04:49,600
perhaps because the values my mother had taught me,

79
00:04:49,600 --> 00:04:52,733
values of hard work, and honesty and empathy,

80
00:04:52,733 --> 00:04:57,500
and compassion had finally resurfaced after a long hibernation.

81
00:04:57,500 --> 00:05:02,467
Or perhaps because of the example of wonderful teachers and

82
00:05:02,467 --> 00:05:08,533
lasting friends, I began to notice a world beyond myself.

83
00:05:08,533 --> 00:05:12,867
Our individual salvation depends on collective salvation.

84
00:05:12,867 --> 00:05:17,133
Because thinking only about yourself, fulfilling your immediate

85
00:05:17,133 --> 00:05:21,867
wants and needs, betrays a poverty of ambition.

86
00:05:21,867 --> 00:05:23,933
Because it's only when you hitch your wagon to something 

87
00:05:23,933 --> 00:05:27,575
larger than yourself that you realize your true potential.

88
00:05:28,233 --> 00:05:31,467
>> Okay, maybe that wasn't exactly what I was thinking at the time, 

89
00:05:31,467 --> 00:05:34,000
but I do remember being amazed to hear a

90
00:05:34,000 --> 00:05:38,367
presidential candidate emphasizing the importance of empathy.

91
00:05:38,367 --> 00:05:40,433
Now of course, having empathy doesn't mean

92
00:05:40,433 --> 00:05:43,433
that you'll never become involved in a conflict.

93
00:05:43,970 --> 00:05:46,567
For example, a political leader might empathize with one side in

94
00:05:46,567 --> 00:05:50,000
a conflict, only to make enemies out of the other side.

95
00:05:50,000 --> 00:05:51,533
Life is messy.

96
00:05:51,533 --> 00:05:53,699
But the research record does suggest that 

97
00:05:52,810 --> 00:05:57,533
empathic people enjoy a number of advantages.

98
00:05:57,533 --> 00:06:01,300
People high in empathy not only tend to be less prejudiced 

99
00:06:01,300 --> 00:06:04,633
and aggressive, but they and their life partners 

100
00:06:04,633 --> 00:06:09,167
tend to report higher levels of relationship satisfaction.

101
00:06:09,167 --> 00:06:13,417
They're also more likely to intervene in bystander emergency situations.

102
00:06:13,417 --> 00:06:17,333
They imagine what that victim might be going through.

103
00:06:17,333 --> 00:06:20,917
And people high in cognitive empathy, or perspective taking,

104
00:06:20,917 --> 00:06:24,292
tend to reach much better negotiated outcomes,

105
00:06:24,292 --> 00:06:26,083
in part because they're less likely to

106
00:06:26,083 --> 00:06:29,583
deadlock when bargaining with the other side.

107
00:06:29,583 --> 00:06:32,625
So empathy is something of a magic bullet, 

108
00:06:32,625 --> 00:06:36,833
meaning that it works across a wide variety of situations.

109
00:06:36,833 --> 00:06:40,750
And in this regard, it's interesting that so many cultures, religions, 

110
00:06:40,750 --> 00:06:45,125
and philosophies, recognize the central importance of empathy. 

111
00:06:45,125 --> 00:06:47,667
For example, there's the Native American saying about 

112
00:06:47,667 --> 00:06:51,875
not judging other people until you've walked a mile in their moccasins,

113
00:06:51,875 --> 00:06:54,960
advice that kind of reminds me of research on the fundamental

114
00:06:54,960 --> 00:07:00,840
attribution error and the need to consider situational explanations for behavior.

115
00:07:00,840 --> 00:07:05,200
There's the importance of oneness in eastern religions and philosophies.

116
00:07:05,200 --> 00:07:07,440
And of course, there's the Golden Rule in

117
00:07:07,440 --> 00:07:11,440
Judeo-Christian, Islamic and Asian traditions.

118
00:07:11,440 --> 00:07:15,760
So if empathy really is that important, are there ways to develop it?

119
00:07:15,760 --> 00:07:16,880
Absolutely.

120
00:07:16,880 --> 00:07:20,400
Even though empathy is in part genetic, there's no question 

121
00:07:20,400 --> 00:07:22,800
that it can be increased through training, through

122
00:07:22,800 --> 00:07:26,000
practice, and through certain lifestyle habits.

123
00:07:26,000 --> 00:07:28,720
For example, neuroimaging studies have found that

124
00:07:28,720 --> 00:07:32,520
meditation enhances the activation of brain areas that

125
00:07:32,520 --> 00:07:36,400
are involved in emotional processing and empathy.

126
00:07:36,400 --> 00:07:40,320
And there's strong evidence that children can be taught to be more empathic, 

127
00:07:40,320 --> 00:07:43,040
and that school-based empathy training programs

128
00:07:43,040 --> 00:07:47,400
reduce children's level of aggression and prejudice.

129
00:07:47,400 --> 00:07:52,400
In fact, simply taking the perspective of other people can have immediate effects,

130
00:07:52,400 --> 00:07:56,960
such as the elimination of actor observer differences in attribution. 

131
00:07:56,960 --> 00:08:01,720
For observers to take the perspective of actors, all it requires is a little bit 

132
00:08:01,720 --> 00:08:06,200
of imagination and the desire to see things from another person's point of view.

133
00:08:06,200 --> 00:08:09,480
The magic bullet is available.

134
00:08:09,480 --> 00:08:12,120
Speaking personally, this is how I teach.

135
00:08:12,120 --> 00:08:16,640
I try to imagine being a student in one of my courses, and I ask myself, 

136
00:08:16,640 --> 00:08:22,640
"What would that student want?" In most cases, the answer is cartoons.

137
00:08:22,640 --> 00:08:28,680
And so, I'm very pleased to introduce an animated guest lecture by Roman Krznaric.

138
00:08:28,680 --> 00:08:33,720
Seriously, it is animated, but in fact, it's packed with very interesting ideas.

139
00:08:33,720 --> 00:08:38,160
It's not only about empathy, but it's about the idea of outrospection.

140
00:08:38,160 --> 00:08:41,200
And it'll take us the rest of the way through this video.

141
00:08:41,200 --> 00:08:43,000
I hope you enjoy.

142
00:08:54,640 --> 00:08:59,520
The 20th century I see as the age of introspection.

143
00:08:59,520 --> 00:09:02,000
That was the era in which the self-help industry 

144
00:09:02,000 --> 00:09:05,120
and therapy culture told us that the best way 

145
00:09:05,120 --> 00:09:06,800
to discover who we are and what to

146
00:09:06,800 --> 00:09:09,520
do with our lives was to look inside ourselves -- 

147
00:09:09,520 --> 00:09:11,400
to gaze at our own navels.

148
00:09:11,400 --> 00:09:13,040
What we've discovered of course is that 

149
00:09:13,040 --> 00:09:16,360
that has not delivered the good life. So the 21st century 

150
00:09:16,360 --> 00:09:19,600
needs to be different. Instead of the age of introspection,

151
00:09:19,600 --> 00:09:23,280
we need to shift to the age of outrospection.

152
00:09:23,280 --> 00:09:25,720
And by "outrospection" I mean the idea of discovering who 

153
00:09:25,720 --> 00:09:28,960
you are and what to do with your life by stepping outside yourself,

154
00:09:28,960 --> 00:09:32,720
discovering the lives of other people, other civilizations.

155
00:09:32,720 --> 00:09:36,840
And the ultimate art form to the age of outrospection is empathy.

156
00:09:36,840 --> 00:09:40,560
I want to talk about what empathy is, why it matters, 

157
00:09:40,560 --> 00:09:44,840
and ultimately, how we can expand our empathic potential.

158
00:09:44,840 --> 00:09:47,480
Of course, empathy is more popular today as

159
00:09:47,480 --> 00:09:50,080
a concept than at any point in its history.

160
00:09:50,080 --> 00:09:52,840
Barack Obama's been talking for several years now about America's 

161
00:09:52,840 --> 00:09:56,400
empathy deficit. You've got business people talking about empathy marketing.

162
00:09:56,400 --> 00:10:00,240
The neuroscientists are measuring the empathy parts of our brains.

163
00:10:00,240 --> 00:10:03,120
But I think what we need to do is focus more on two things.

164
00:10:03,120 --> 00:10:07,680
First, the way that empathy can be part of the art of living -- 

165
00:10:07,680 --> 00:10:10,000
a philosophy of life. Empathy isn't just something

166
00:10:10,000 --> 00:10:12,560
that expands your moral universe.

167
00:10:12,560 --> 00:10:15,600
Empathy is something that can make you a more creative thinker, 

168
00:10:15,600 --> 00:10:20,080
improve your relationships, can create the human bonds that make life worth living.

169
00:10:20,080 --> 00:10:24,960
But more than that, empathy is also about social change, radical social change.

170
00:10:24,960 --> 00:10:26,480
A lot of people think of empathy as sort of a nice, 

171
00:10:26,480 --> 00:10:29,000
soft, fluffy concept. I think it's anything but that.

172
00:10:29,000 --> 00:10:33,120
I think it's actually quite dangerous, because empathy can create revolution.

173
00:10:33,120 --> 00:10:35,800
Not one of those old fashion revolutions of new states, 

174
00:10:35,800 --> 00:10:40,000
policies, governments, laws, but something much more fiery and dangerous, 

175
00:10:40,000 --> 00:10:43,280
which is a revolution of human relationships.

176
00:10:43,280 --> 00:10:45,320
Now, if you open a standard psychology

177
00:10:45,320 --> 00:10:48,400
textbook, you'll see two definitions of empathy.

178
00:10:48,400 --> 00:10:49,440
One of them is this:

179
00:10:49,440 --> 00:10:52,480
Affective empathy, empathy as a shared

180
00:10:52,480 --> 00:10:55,320
emotional response, a sort of mirrored response.

181
00:10:55,320 --> 00:10:57,800
So if you look at the face of this child in anguish, 

182
00:10:57,800 --> 00:11:00,720
and you too feel anguish, that's affective empathy.

183
00:11:00,720 --> 00:11:03,360
You're mirroring their emotions.

184
00:11:03,360 --> 00:11:04,600
The second kind you'll find

185
00:11:04,600 --> 00:11:08,960
when you open your psychology textbook is this: cognitive empathy,

186
00:11:08,960 --> 00:11:14,080
which is about perspective-taking, about stepping into somebody else's world,

187
00:11:14,080 --> 00:11:16,800
almost like an actor looking through the eyes of their character.

188
00:11:16,800 --> 00:11:19,400
It's about understanding somebody else's world view, 

189
00:11:19,400 --> 00:11:22,760
their beliefs, the fears, the experiences

190
00:11:22,760 --> 00:11:26,920
that shape how they look at the world and how they look at themselves.

191
00:11:26,920 --> 00:11:30,880
We make assumptions about people. We have prejudices about people,

192
00:11:30,880 --> 00:11:35,520
which block us from seeing their uniqueness, their individuality.

193
00:11:35,520 --> 00:11:40,440
We use labels, and highly empathic people get beyond that, 

194
00:11:40,440 --> 00:11:45,560
or get beyond those labels, by nurturing their curiosity about others.

195
00:11:45,560 --> 00:11:50,280
So, how might we nurture our curiosity? Where can we find inspiration?

196
00:11:50,280 --> 00:11:52,960
I think we can find inspiration in George Orwell, 

197
00:11:52,960 --> 00:11:56,680
who you might think of as, you know, the author of "1984" and "Animal Farm."

198
00:11:56,680 --> 00:12:00,800
But he was also one of the great empathic adventurers of the 20th century.

199
00:12:00,800 --> 00:12:04,600
You might remember, or might know, that he came from a very privileged background.

200
00:12:04,600 --> 00:12:05,400
He went to Eton.

201
00:12:05,400 --> 00:12:09,320
He was as a colonial police officer in Burma.

202
00:12:09,320 --> 00:12:12,240
But what he realized in his twenties was that he knew very little 

203
00:12:12,240 --> 00:12:15,440
about his own country -- particularly about the way that

204
00:12:15,440 --> 00:12:19,000
those people living on the social margins really experience life.

205
00:12:19,000 --> 00:12:22,880
So he decided to do something about it and conduct one of the most brilliant 

206
00:12:22,880 --> 00:12:27,640
empathy experiments, which was to go tramping on the streets of East London.

207
00:12:27,640 --> 00:12:31,040
He wrote about this famously in his book, "Down and Out in Paris and London."

208
00:12:31,040 --> 00:12:34,640
But the important thing about Orwell's experience was that it not only 

209
00:12:34,640 --> 00:12:38,160
expanded his moral universe (he became a more compassionate person),

210
00:12:38,160 --> 00:12:41,920
but it also cultivated his curiosity about strangers. 

211
00:12:41,920 --> 00:12:44,560
He developed new friendships, he gathered a whole load 

212
00:12:44,560 --> 00:12:47,960
of literary materials he used for the rest of his life.

213
00:12:47,960 --> 00:12:53,480
In a way this empathy adventure, it made him good, but it was also good for him.

214
00:12:53,480 --> 00:12:56,680
Highly empathic people tend to be very sensitive listeners.

215
00:12:56,680 --> 00:13:01,200
They're very good at understanding what somebody else's needs are.

216
00:13:01,200 --> 00:13:02,720
They tend to be also people who

217
00:13:02,720 --> 00:13:04,960
in conversations share part of their own lives,

218
00:13:04,960 --> 00:13:09,440
make conversations two-way dialogues, make themselves vulnerable.

219
00:13:09,440 --> 00:13:13,200
Worth thinking about as well is to think about political conversations.

220
00:13:13,200 --> 00:13:16,080
"It won't stop until we talk."

221
00:13:16,080 --> 00:13:19,980
This is the motto of a grass roots peace-building organization 

222
00:13:19,980 --> 00:13:23,800
in Israel and the Palestinian territories called the "Parents Circle."

223
00:13:23,800 --> 00:13:26,280
What it does is bring together Palestinian

224
00:13:26,280 --> 00:13:29,120
and Israeli families who share something very special.

225
00:13:29,120 --> 00:13:33,520
These families have all lost members of their own families in the conflict.

226
00:13:33,520 --> 00:13:36,680
And the parents' circle brings them together for

227
00:13:36,680 --> 00:13:41,500
conversations, picnics, meetings where they share each other's stories.

228
00:13:41,500 --> 00:13:44,440
They discover that they share the same pain, the same blood.

229
00:13:44,440 --> 00:13:47,760
They make that empathic bond. They also have other fantastic projects.

230
00:13:47,760 --> 00:13:52,680
My favorite one is called "Hello Peace." It's a free phone telephone line.

231
00:13:52,680 --> 00:13:54,800
So anybody can pick up and call that number.

232
00:13:54,800 --> 00:13:56,680
If you are a Palestinian and call it, you're immediately

233
00:13:56,680 --> 00:13:59,360
put through to an Israeli. You can have a half hour conversation.

234
00:13:59,360 --> 00:14:02,040
If you are an Israeli and pick it up, you're put through to a Palestinian.

235
00:14:02,040 --> 00:14:07,800
Since 2002 over a million calls have been logged on the Hello Peace free phone line.

236
00:14:07,800 --> 00:14:12,640
That's the kind of project which is trying to create grassroots empathy.

237
00:14:12,640 --> 00:14:13,720
Now, we normally think of empathy

238
00:14:13,720 --> 00:14:15,840
as something that happens between individuals.

239
00:14:15,840 --> 00:14:21,160
But I also believe it can be a collective force, it can happen on a mass scale.

240
00:14:21,160 --> 00:14:22,400
When I think of history,

241
00:14:22,400 --> 00:14:25,680
I think not of the rise and fall of civilizations and religions

242
00:14:25,680 --> 00:14:29,080
or political systems; I think of the rise and fall of empathy,

243
00:14:29,080 --> 00:14:34,500
moments of mass empathic flowering, and also of course, of empathic collapse.

244
00:14:35,200 --> 00:14:37,560
As you probably know, in the 1780s in Britain, 

245
00:14:37,560 --> 00:14:41,760
slavery was an accepted part of society.

246
00:14:41,760 --> 00:14:44,160
People felt that the economy was as dependent

247
00:14:44,160 --> 00:14:46,920
on slavery as our economy is on oil today.

248
00:14:46,920 --> 00:14:51,600
Half a million African slaves were being worked to death on British plantations 

249
00:14:51,600 --> 00:14:57,440
in the Caribbean, and nobody thought this could ever be eroded.

250
00:14:57,440 --> 00:15:01,280
But in the late 1780s, there was the rise of the world's first great 

251
00:15:01,280 --> 00:15:05,400
human rights movement, and it was a movement powered by empathy.

252
00:15:05,400 --> 00:15:08,680
Its leaders developed a very empathic campaign.

253
00:15:08,680 --> 00:15:10,320
The idea they had was to try and

254
00:15:10,320 --> 00:15:14,080
get people in Britain to experience or understand, at least, 

255
00:15:14,080 --> 00:15:18,980
what it was like to be a slave on a slave ship, on a slave plantation.

256
00:15:18,980 --> 00:15:23,840
They published oral stories of former slaves talking about what it

257
00:15:23,840 --> 00:15:27,840
was like to be whipped until they were lying on the ground.

258
00:15:27,840 --> 00:15:34,120
They also ran public meetings where they showed these little instruments,

259
00:15:34,120 --> 00:15:37,840
which were used to keep slaves' jaws open to force feed them.

260
00:15:37,840 --> 00:15:40,680
They organized for former slaves to give talks around Britain 

261
00:15:40,680 --> 00:15:45,840
about their experiences, and this led to a sort of

262
00:15:45,840 --> 00:15:50,160
revolutionary social movement really. It led to petitions. It led to 

263
00:15:49,160 --> 00:15:54,360
public protests. It led to the first great fair trade boycott of sugar.

264
00:15:54,360 --> 00:16:00,720
Eventually, it led to the abolition of the slave trade in 1807 and later slavery itself.

265
00:16:00,720 --> 00:16:04,000
What this all led to, or what it really showed, was that empathy

266
00:16:04,000 --> 00:16:06,920
could be a collective force.

267
00:16:06,920 --> 00:16:10,760
We normally think of empathy as empathizing with the down and out,

268
00:16:10,760 --> 00:16:13,760
the poor and marginalized, those on the edges of society.

269
00:16:13,760 --> 00:16:19,720
I think we need to be more adventurous in who we try to empathize with.

270
00:16:19,720 --> 00:16:22,640
I think we need to empathize with those in power.

271
00:16:22,640 --> 00:16:26,240
We need to understand how those in power, in whatever realm it is, 

272
00:16:26,240 --> 00:16:29,000
think about the world and their lives and their ambitions.

273
00:16:29,000 --> 00:16:31,960
We need to understand their values.

274
00:16:31,960 --> 00:16:34,600
Only then are we going to be able to develop effective 

275
00:16:34,600 --> 00:16:39,480
strategies for social, political, and economic transformation.

276
00:16:39,480 --> 00:16:43,000
Equally, I think we need to apply our more ambitious thinking 

277
00:16:43,000 --> 00:16:45,750
in policy realms, such as thinking about climate change.

278
00:16:45,750 --> 00:16:49,080
We all know there's a huge gap between what we know about climate change 

279
00:16:49,080 --> 00:16:52,800
and the amount of action that people are taking (i.e., not very much).

280
00:16:52,800 --> 00:16:56,000
I think that gap is explained by empathy in two forms.

281
00:16:56,000 --> 00:16:58,160
I think there's an empathic gap in terms of

282
00:16:58,160 --> 00:17:01,580
we're not empathizing across space with people in developing

283
00:17:01,580 --> 00:17:04,560
countries, like in India who, people who have been

284
00:17:04,560 --> 00:17:08,640
hit by climate change induced floods or droughts in Kenya.

285
00:17:08,640 --> 00:17:10,960
And almost more importantly perhaps, we are

286
00:17:10,960 --> 00:17:14,680
failing to empathize through time with future generations.

287
00:17:14,680 --> 00:17:19,440
I think we need to learn to expand our empathic imaginations forwards

288
00:17:19,440 --> 00:17:22,520
through time, as well as across space. How're we going to do it?

289
00:17:22,520 --> 00:17:25,000
I think we need new social institutions.

290
00:17:25,000 --> 00:17:30,120
We need, for example, empathy museums. A place which is not about dusty exhibits

291
00:17:30,120 --> 00:17:31,800
(you know, like an old Victorian museum) but an

292
00:17:31,800 --> 00:17:35,960
experiential and conversational public space, where you might walk

293
00:17:35,960 --> 00:17:38,200
in and in the first room there is a human library, 

294
00:17:38,200 --> 00:17:40,200
where you can borrow people for conversations.

295
00:17:40,200 --> 00:17:43,400
You walk into the next room, and there are 20 sewing machines,

296
00:17:43,400 --> 00:17:46,320
and there are former Vietnamese sweatshop workers there 

297
00:17:46,320 --> 00:17:50,840
who will teach you how to make a t-shirt like the one you're probably wearing, 

298
00:17:50,840 --> 00:17:55,280
under sweatshop labor conditions, and you'll be paid five pence at the end of it,

299
00:17:55,280 --> 00:17:57,760
so you understand the labor behind the label.

300
00:17:57,760 --> 00:18:02,200
You may well go into the cafe and scan in your food and discover 

301
00:18:02,200 --> 00:18:04,960
the working conditions of those who picked the coffee beans, 

302
00:18:04,960 --> 00:18:06,560
the drink that you're drinking.

303
00:18:06,560 --> 00:18:10,840
You may see a video of them talking about their lives, trying to make

304
00:18:10,840 --> 00:18:14,800
a connection across space and into realms that you don't know about.

305
00:18:14,800 --> 00:18:18,320
I think we need to think about bringing empathy into

306
00:18:18,320 --> 00:18:22,240
our everyday lives in a very sort of habitual way.

307
00:18:22,240 --> 00:18:27,480
Socrates said that the way to live a wise and good life was to know thyself.

308
00:18:27,480 --> 00:18:31,080
And we've generally thought of that as being about being self-reflective,

309
00:18:30,080 --> 00:18:34,000
looking in at ourselves. It's been about introspection.

310
00:18:34,000 --> 00:18:35,880
But I think in the 21st century,

311
00:18:35,880 --> 00:18:39,120
we need to recognize that to know thyself is something that can also 

312
00:18:39,120 --> 00:18:44,200
be achieved by stepping outside yourself, by discovering other people's lives, 

313
00:18:44,200 --> 00:18:49,440
and I think empathy is the way to revolutionize our own philosophies of life,

314
00:18:49,440 --> 00:18:52,200
to become more outrospective and to create the revolution

315
00:18:52,200 --> 00:18:56,500
of human relationships that I think we so desperately need.

