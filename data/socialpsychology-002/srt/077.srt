



1
00:00:06,530 --> 00:00:10,920
Welcome to a bonus video generously 
contributed by the Association for 

2
00:00:10,920 --> 00:00:14,360
Psychological Science. 
What you're about to see is a 

3
00:00:14,360 --> 00:00:19,130
conversation with Elliot Aronson, 
Professor Emeritus of Psychology at the 

4
00:00:19,130 --> 00:00:22,370
University of California, Santa Cruz â a 
wonderful school. 

5
00:00:23,680 --> 00:00:27,960
Professor Aronson is a world renowned 
social psychologist and cognitive 

6
00:00:27,960 --> 00:00:32,060
dissonance researcher who was once a 
graduate student of Leon Festinger's, 

7
00:00:32,060 --> 00:00:36,690
and who went on to become the only 
psychologist who's ever received the 

8
00:00:36,690 --> 00:00:42,570
American Psychological Association's top 
awards in all three major academic 

9
00:00:42,570 --> 00:00:46,660
categories: teaching, research, and 
writing. 

10
00:00:46,660 --> 00:00:50,930
In the video, Professor Aronson is 
interviewed by two other well known 

11
00:00:50,930 --> 00:00:55,278
social psychologists: his son Josh, who is 
also a professor, 

12
00:00:55,278 --> 00:01:00,200
and Carol Tavris, with whom he 
coauthored the award-winning book 

13
00:01:00,200 --> 00:01:05,110
Mistakes Were Made (But Not by Me). 
Among other things, you'll hear Elliot 

14
00:01:05,110 --> 00:01:09,620
Aronson describe how he first got 
interested in social psychology, what it 

15
00:01:09,620 --> 00:01:15,100
was like to work with Leon Festinger, and 
how the famous $1/$20 cognitive dissonance 

16
00:01:15,100 --> 00:01:20,200
experiment by Leon Festinger 
and Merrill Carlsmith almost ended up failing. 

17
00:01:20,500 --> 00:01:23,860
I hope you enjoy this piece of social 
psychology history. 

18
00:01:46,560 --> 00:01:50,800
>> I grew up in a town called Revere, Massachusetts, 

19
00:01:50,800 --> 00:01:54,290
about 20  miles northeast of Boston. 

20
00:01:54,290 --> 00:01:57,200
We are Jewish, and my family was really 
Orthodox, 

21
00:01:57,200 --> 00:02:02,820
and we lived in a neighborhood that was 
virulently anti-Semitic. 

22
00:02:03,200 --> 00:02:07,956
And in order to get to the Hebrew school, 
I would have to walk through 

23
00:02:07,956 --> 00:02:15,810
neighborhoods clustered with teenagers 
who would frequently shout anti-Semitic 

24
00:02:15,810 --> 00:02:20,180
slogans at me. 
And sometimes they'd push me around, and 

25
00:02:20,180 --> 00:02:28,270
sometimes they'd even rough me up. 
And one of my earliest vivid memories is 

26
00:02:28,270 --> 00:02:33,630
around the time I was 9 years old after 
one of these beatings, when I was sitting 

27
00:02:33,630 --> 00:02:38,299
on a curb stone nursing a bloody nose and 
a split lip, 

28
00:02:39,360 --> 00:02:43,950
and wondering why these people hated Jews 
so much. 

29
00:02:45,200 --> 00:02:50,500
Were they born hating Jews, or did 
somebody teach them to hate Jews? 

30
00:02:50,500 --> 00:02:56,700
And wondering why they hated me so much 
when they didn't even know me, 

31
00:02:56,700 --> 00:03:02,470
and wondering, if they got to know me 
better and discovered what a sweet and 

32
00:03:02,470 --> 00:03:04,700
charming little boy I was, 

33
00:03:04,700 --> 00:03:13,110
would they like me more? And if they liked 
me more, would they 

34
00:03:13,110 --> 00:03:19,290
hate other Jews less? 
Now, I didn't realize it at the time, 

35
00:03:19,290 --> 00:03:23,500
of course, but these are profound social 
psychological questions. 

36
00:03:23,700 --> 00:03:29,920
And ten years later, when I was a 
sophomore at Brandeis University, 

37
00:03:29,920 --> 00:03:35,900
I wandered quite by accident into a class 
being taught by Abraham Maslow. 

38
00:03:36,200 --> 00:03:41,380
Now, I didn't, at that time, I didn't 
know he was Abraham Maslow; 

39
00:03:41,380 --> 00:03:43,900
I thought he was, you know, Professor 
Maslow. 

40
00:03:43,900 --> 00:03:50,210
But suddenly Maslow started to talk 
about the psychology of prejudice, 

41
00:03:50,210 --> 00:03:55,820
and he was raising some of the very same 
questions that I had raised when I was 

42
00:03:55,820 --> 00:04:01,430
9 years old sitting on that curb in 
Revere, Massachusetts. 

43
00:04:01,430 --> 00:04:07,470
I immediately began taking notes, and the 
next day I switched my major from 

44
00:04:07,470 --> 00:04:11,480
economics to psychology because I 
discovered, oh my goodness, there is a 

45
00:04:11,480 --> 00:04:17,100
whole area of study dedicated to these 
very important questions. 

46
00:04:17,100 --> 00:04:24,700
I hadn't known that before, and I switched. 
I became a protege of Maslow's. 

47
00:04:25,300 --> 00:04:32,420
He was an inspirational figure, and he 
encouraged me to go on to graduate school. 

48
00:04:32,420 --> 00:04:35,185
 >> Well, well except that it's interesting, 

49
00:04:35,185 --> 00:04:40,050
Brandeis' psychology department was not 
exactly typical of what mainstream 

50
00:04:40,050 --> 00:04:43,910
psychology was doing. 
And there you go from studying with 

51
00:04:43,910 --> 00:04:48,510
Maslow at Brandeis to working with Leon 
Festinger at Stanford. 

52
00:04:48,510 --> 00:04:51,190
Would you like to talk about that 
transition? 

53
00:04:51,190 --> 00:04:55,380
 >> Oh, it was quite a transition. 
I didn't go to Stanford in order to work 

54
00:04:55,380 --> 00:05:00,400
with Leon Festinger. 
I, I had never heard of him, and I 

55
00:05:00,400 --> 00:05:05,490
assumed he had never heard of me either. 
We both arrived at Stanford at the same 

56
00:05:05,490 --> 00:05:11,300
time in 1956, 
Leon as a brilliant young professor  

57
00:05:11,300 --> 00:05:17,700
with a reputation for being a genius and me as 
a very insecure 

58
00:05:17,700 --> 00:05:22,060
first-year graduate student. 
Now, Leon came with a reputation for being a 

59
00:05:22,060 --> 00:05:28,004
genius, but he also had a reputation for 
being a very harsh, even cruel, 

60
00:05:28,004 --> 00:05:31,770
taskmaster. 
And he assigned a term paper, and I wrote 

61
00:05:31,770 --> 00:05:36,770
the term paper and handed it in, and then 
a few days later, he called me. 

62
00:05:36,770 --> 00:05:41,602
He said, "Aronson!" He had a stack of papers 
on his desk, and he pulls one out. 

63
00:05:41,602 --> 00:05:52,664
And he, he held it up like this, like, by 
the, his thumb and forefinger. 

64
00:05:52,664 --> 00:05:54,900
 >> [LAUGH] 
 >> Turning his head away, and said, 

65
00:05:54,900 --> 00:05:56,830
"I believe this is yours." 
 >> [LAUGH] 

66
00:05:57,930 --> 00:06:02,700
 >> And, as if he was holding a 
particularly smelly piece of garbage. 

67
00:06:02,700 --> 00:06:07,600
So, I said, "I guess you didn't like it 
very much." 

68
00:06:08,500 --> 00:06:14,259
And then there was a long silence. 
And then he gave me a look that was 

69
00:06:14,259 --> 00:06:17,590
unique to him. I've never seen that look 
on anyone else. 

70
00:06:17,590 --> 00:06:21,600
It was a mixture of 
 contempt and pity. 

71
00:06:21,600 --> 00:06:25,570
 >> [LAUGH] 
 >> It was sort of like, like a look 

72
00:06:25,570 --> 00:06:30,040
like that, and the contempt was obvious, 
the reason for the contempt. 

73
00:06:30,040 --> 00:06:34,400
The pity was, it looked like he was 
feeling sorry for me because I had been 

74
00:06:34,400 --> 00:06:35,485
born brain damaged, 
 >> [LAUGH] 

75
00:06:35,485 --> 00:06:40,970
 >> and, and he said, "Yeah, that's 
right; 

76
00:06:40,970 --> 00:06:48,950
I didn't like it very much." 
So I took the paper, and I slunk down 

77
00:06:48,950 --> 00:06:52,540
the corridor to my own desk, and then 
I just, you know, didn't know if 

78
00:06:52,540 --> 00:06:55,630
I could take it. 
And finally, I opened the paper, and 

79
00:06:55,630 --> 00:07:02,230
there wasn't a mark on it. 
And so, I gathered up all my courage, 

80
00:07:02,230 --> 00:07:06,625
and I took my paper, and I walked back into 
Leon's office, and I said, 

81
00:07:06,625 --> 00:07:11,010
"Dr. Festinger, there must be some mistake; 
you forgot to tell me what I did wrong. 

82
00:07:11,010 --> 00:07:13,404
How am I supposed to know what I did 
wrong?" 

83
00:07:13,404 --> 00:07:19,600
And he went, "What?" 
 >> [LAUGH]. 

84
00:07:20,100 --> 00:07:23,160
 >> He said, "You don't have enough respect 

85
00:07:23,160 --> 00:07:28,860
for your own thinking, for your own 
ideas, to follow them through to their 

86
00:07:28,860 --> 00:07:33,770
logical conclusion, 
and you expect me to do that? 

87
00:07:33,770 --> 00:07:38,600
This is graduate school -- this isn't 
kindergarten." 

88
00:07:39,090 --> 00:07:41,940
So I took the paper, and I walked back to 
my office,

89
00:07:41,940 --> 00:07:46,780
and I sat there for awhile. 
And it was an incredible moment of 

90
00:07:46,780 --> 00:07:51,860
choice. My first impulse was to say, screw him. 

91
00:07:51,860 --> 00:07:54,660
Who needs him? 
I'll drop this course, 

92
00:07:54,660 --> 00:07:57,320
and I can work with someone -- you know, and 
I don't need him. 

93
00:07:58,640 --> 00:08:04,740
And, my other impulse was to say, you 
know, he is a very bright guy, and maybe 

94
00:08:04,740 --> 00:08:10,820
he has something that he can teach me. And 
then I reread the paper, and I tried to 

95
00:08:10,820 --> 00:08:17,680
reread it through his eyes, and it was a 
sloppy, incomplete rather 

96
00:08:17,680 --> 00:08:23,280
dumb piece of work. 
So, for the next three days -- it seemed 

97
00:08:23,280 --> 00:08:30,500
like 72 consecutive hours -- I really 
reworked that paper. And then I brought it 

98
00:08:30,500 --> 00:08:34,810
to him, put it on his desk, and I said, 
"Maybe you'll like this one better." 

99
00:08:34,810 --> 00:08:39,635
And to his great credit, 20 minutes 
later (he must have read it immediately), 

100
00:08:39,635 --> 00:08:46,080
20 minutes later, he came into the room 
where I was sitting, put the paper in 

101
00:08:46,080 --> 00:08:52,120
front of me, sat on the edge of my desk, 
put his hand on my shoulder, and said, 

102
00:08:52,120 --> 00:08:58,500
"Now, this is worth criticizing." 
 >> [LAUGH] 

103
00:09:05,800 --> 00:09:11,700
 >> So, I mean Elliot, I mean, you have 
this guy, this tough guy 

104
00:09:11,700 --> 00:09:17,710
and that humanist Abe Maslow, both 
of them as your mentors -- 

105
00:09:17,710 --> 00:09:21,368
how did they see each other? 
 >> It's amazing. 

106
00:09:21,368 --> 00:09:24,600
They hated each other. 
They couldn't stand each other. 

107
00:09:25,500 --> 00:09:30,220
When Leon found out, he asked me, well, 
you know, how did you get into 

108
00:09:30,220 --> 00:09:32,160
psychology? 
And I told him about Abe Maslow, 

109
00:09:32,160 --> 00:09:42,700
and he said, "Maslow?" He said, "That guy's 
ideas are so bad they're not even wrong." 

110
00:09:43,300 --> 00:09:48,125
Which means that you can't really test 
them. You can't really see whether 

111
00:09:48,125 --> 00:09:52,670
they're wrong or not. 
And so for Leon as the really hard,  

112
00:09:52,670 --> 00:09:56,330
hard-nosed scientist, 
then they're useless if you can't put 

113
00:09:56,330 --> 00:10:01,430
them to the test. 
That summer I went back east to visit my 

114
00:10:01,430 --> 00:10:07,390
mother, and I dropped into Maslow's, 
and we were having lunch together, 

115
00:10:07,390 --> 00:10:10,450
and he said, "Oh, by the way, who are you 
working with at Stanford?" 

116
00:10:10,450 --> 00:10:13,164
And I said, "Oh, Leon Festinger." 

117
00:10:13,164 --> 00:10:16,890
He said, "Festinger?! That bastard. 
How can you stand him?" 

118
00:10:16,890 --> 00:10:20,790
>> [LAUGH] >> So, there you are. 

119
00:10:20,790 --> 00:10:25,300
I mean I, they couldn't have been two 
more different people, and I loved them both, 

120
00:10:25,300 --> 00:10:28,800
and loving them both created a great deal 

121
00:10:28,800 --> 00:10:31,605
of dissonance because they hated each 
other so much. 

122
00:10:31,605 --> 00:10:33,400
 >> What did you take from each of those guys? 

123
00:10:35,200 --> 00:10:37,530
>> Well, Maslow was inspirational. 

124
00:10:37,530 --> 00:10:41,980
What I got from Maslow was excitement 
about the promise of psychology, 

125
00:10:41,980 --> 00:10:46,600
especially the promise that psychology 
could do good in the world, could, 

126
00:10:46,600 --> 00:10:50,380
could change the world, could change people in 
a better way, could make them less 

127
00:10:50,380 --> 00:10:53,830
prejudiced, less aggressive, more loving, etc.

128
00:10:53,830 --> 00:10:57,172
There was an excitement about those 
ideas. 

129
00:10:57,172 --> 00:11:05,330
From Festinger, not only did I learn to 
do research on an interesting theory -- 

130
00:11:05,330 --> 00:11:09,320
the theory of cognitive dissonance was 
extremely provocative, especially in the 

131
00:11:09,320 --> 00:11:13,540
early days when everything was wide open, 
and they were, the hypotheses were 

132
00:11:13,540 --> 00:11:15,770
dropping out of the trees, right in my 
lap. 

133
00:11:15,770 --> 00:11:20,518
It was so easy to generate hypotheses. 
But more than that, what I learned from 

134
00:11:20,518 --> 00:11:25,860
Festinger was how to do it. 
How to do experiments. 

135
00:11:25,860 --> 00:11:29,520
It required very special skills and very 
special training. 

136
00:11:29,520 --> 00:11:33,770
You had to be a playwright. 
You had to be a director. 

137
00:11:33,770 --> 00:11:38,740
You had to be an actor, because you had 
to sell the procedure, 

138
00:11:38,740 --> 00:11:43,930
because the laboratory, as everybody 
knows, is a very sterile place, 

139
00:11:43,930 --> 00:11:48,990
and a person comes in to be the 
subject in an experiment, he's in a 

140
00:11:48,990 --> 00:11:55,430
sterile environment. 
What our job was, to embed that person in 

141
00:11:55,430 --> 00:12:01,670
a scenario where he's not stepping back 
and making decisions about what would a 

142
00:12:01,670 --> 00:12:06,810
person normally do in this situation, 
but where he's so embedded in the 

143
00:12:06,810 --> 00:12:13,985
scenario that we constructed that he's 
behaving the way he or she would behave 

144
00:12:13,985 --> 00:12:18,500
in his or her real environment if it were 
happening.  

145
00:12:18,500 --> 00:12:23,980
And for that, you needed those skills: 
actor, director, playwright. 

146
00:12:23,980 --> 00:12:26,024
So you wrote a scenario that was 
powerful. 

147
00:12:26,024 --> 00:12:32,390
Now, I remember the very famous 
Festinger-Carlsmith experiment that most 

148
00:12:32,390 --> 00:12:37,430
of you probably know about. 
That's the one where if you lie about, 

149
00:12:37,430 --> 00:12:40,670
if you do a task that's really boring and 
then you tell somebody else it's 

150
00:12:40,670 --> 00:12:46,000
interesting, and you tell him it's interesting for $1, 

151
00:12:46,000 --> 00:12:51,090
you believe, you come to believe that 
it's interesting, where if you tell him 

152
00:12:51,090 --> 00:12:55,565
it's interesting for $20, you don't come 
to believe that it's interesting. 

153
00:12:55,565 --> 00:13:00,700
Classic dissonance experiment, one of the 
two or three earliest experiments 

154
00:13:00,700 --> 00:13:04,110
done on dissonance theory. 
Now, Carlsmith, Merrill Carlsmith, was an 

155
00:13:04,110 --> 00:13:08,650
undergraduate at Stanford, and Leon had 
him working on this experiment, and he 

156
00:13:08,650 --> 00:13:12,150
was running pilot subjects. 
And he came to me one day after h ran 

157
00:13:12,150 --> 00:13:15,790
seven or eight subjects, and says, you 
know, this is never going to work. 

158
00:13:15,790 --> 00:13:18,800
The subjects don't believe it. 
It's really boring. 

159
00:13:18,800 --> 00:13:27,200
And so Leon said to me, "Train him." 
Now, Carlsmith was a genius. 

160
00:13:27,200 --> 00:13:30,970
He was a really smart guy, but he was really 
wooden. 

161
00:13:30,970 --> 00:13:36,025
And what I had to train him to do was to 
sell that experiment to the participants. 

162
00:13:36,025 --> 00:13:40,340
So, the scenario is, 
you're the, 

163
00:13:40,340 --> 00:13:47,560
if I'm the experimenter, and a person goes 
through the experiment doing these dull things 

164
00:13:47,560 --> 00:13:50,970
like packing spools and turning screws 
for an hour 

165
00:13:50,970 --> 00:13:55,120
(it's really awful -- it's like an industrial 
assembly line type thing), 

166
00:13:55,120 --> 00:14:01,990
and then he is supposed to tell the next 
person waiting to be in the experiment 

167
00:14:01,990 --> 00:14:05,660
(who's really a stooge) how interesting it 
is, okay, 

168
00:14:05,660 --> 00:14:10,800
for either $1 dollar or $20. 
Now, you don't just say that 

169
00:14:10,800 --> 00:14:14,490
to the participant. 
You sweat, you strain, 

170
00:14:14,490 --> 00:14:19,830
you say, "Oh, my God! I, 
I have hired a person to tell the next 

171
00:14:19,830 --> 00:14:25,720
person in line that the experiment was 
interesting, and he hasn't shown up, and 

172
00:14:25,720 --> 00:14:28,980
that person's been waiting out there for 
10 minutes. I don't know what to do." 

173
00:14:28,980 --> 00:14:32,950
And you pace up and down, and then you 
finally say to the guy, "Would you mind 

174
00:14:32,950 --> 00:14:40,940
doing that for me, and I'll pay you $1/ 
$20 for doing it?" And you have to sell it 

175
00:14:40,940 --> 00:14:44,475
by being the actor, by sweating and 
straining. 

176
00:14:44,475 --> 00:14:49,600
And it took me about a week -- it was like 
actor's studio to get -- 

177
00:14:49,600 --> 00:14:52,080
 >> [LAUGH] 
 >> to get Merrill up to speed. And as 

178
00:14:52,080 --> 00:14:57,170
soon as he got it, then he ran a few pilot subjects, 
and it was working and then did 

179
00:14:57,170 --> 00:15:01,600
the experiment. 
And the difference between simply sleepwalking 

180
00:15:01,600 --> 00:15:05,230
through the instruction and 
doing it in a dramatic way is the 

181
00:15:05,230 --> 00:15:10,150
difference between the hypothesis 
coming out and not coming out. 

