



1
00:00:06,760 --> 00:00:11,670
Thanks to the generosity of Alexandra 
Milgram and Alexander Street Press, we're 

2
00:00:11,670 --> 00:00:16,379
fortunate to have a little bonus video 
in which Stanley Milgram discusses his 

3
00:00:16,379 --> 00:00:20,310
electric shock generator, how he thought of the idea, and what it's 

4
00:00:20,310 --> 00:00:24,350
meant for psychology. 
It is, I would venture to guess, the 

5
00:00:24,350 --> 00:00:28,480
single most famous experimental apparatus 
in social psychology. 

6
00:00:30,600 --> 00:00:33,492
 >> It's not hard to find instances of 
aggression in the real world, 

7
00:00:33,492 --> 00:00:37,840
but the problem for the psychologist is 
to find a way to study aggression, 

8
00:00:37,840 --> 00:00:43,020
aggressive behavior, in a precise, exact 
way in laboratory situations, where he 

9
00:00:43,020 --> 00:00:46,450
has some control, and where the person 
who's the object of aggression, the 

10
00:00:46,450 --> 00:00:49,470
target of aggression, does not actually 
get hurt. 

11
00:00:49,470 --> 00:00:51,308
I wrestled with this problem for a long 
time, 

12
00:00:51,308 --> 00:00:55,400
and in 1960 a simple solution occurred to 
me, 

13
00:00:55,400 --> 00:00:59,980
and that is, a subject could be allowed 
to administer a graded series of shocks 

14
00:00:59,980 --> 00:01:03,720
to another person. 
I drew a set of sketches of a shock machine. 

15
00:01:03,720 --> 00:01:08,300
It was later to be called an 
aggression machine. 

16
00:01:08,300 --> 00:01:12,320
I moved on to use this machine for the 
study of obedience, 

17
00:01:12,320 --> 00:01:17,550
but other investigators -- Arnold Buss, for 
example -- used the machine to study 

18
00:01:17,550 --> 00:01:21,280
aggression per se. 
He showed us that men will administer 

19
00:01:21,280 --> 00:01:25,925
higher levels of shock than women. 
Leonard Berkowitz studied the effects of 

20
00:01:25,925 --> 00:01:31,300
frustration and the effects of arousal, 
of imitation of aggressive action, using 

21
00:01:31,300 --> 00:01:34,070
this machine. 
I think the machine was quite important 

22
00:01:34,070 --> 00:01:37,970
for psychology because it allowed us to 
study aggressive behavior in the 

23
00:01:37,970 --> 00:01:40,950
laboratory. 
Of course, there are many shortcomings of 

24
00:01:40,950 --> 00:01:43,330
this kind of study. It's somewhat sterile; it's a little bit 

25
00:01:43,330 --> 00:01:46,350
artificial. And so, we also have to turn to the real 

26
00:01:46,350 --> 00:01:48,800
world to study the dynamics of 
aggression. 

