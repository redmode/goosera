


1
00:00:06,850 --> 00:00:10,150
When it comes to intergroup bias, you 
might be wondering: why are we 

2
00:00:10,150 --> 00:00:15,690
still dealing with prejudice, stereotyping, and 
discrimination in this day and age?

3
00:00:15,690 --> 00:00:18,690
Haven't we had enough time to work these 
things out? 

4
00:00:18,690 --> 00:00:23,000
In the United States, it was roughly half 
a century ago that President Johnson 

5
00:00:23,000 --> 00:00:30,400
signed the Civil Rights Act of 1964 and 
spoke of closing the springs of racial poison. 

6
00:00:30,300 --> 00:00:31,900
Here's a clip. 

7
00:00:32,135 --> 00:00:42,200
>> I urge every American to join in 
this effort, to bring justice and hope 

8
00:00:42,200 --> 00:00:48,700
to all our people, and to bring peace to our 
land. 

9
00:00:48,700 --> 00:00:52,000
My fellow citizens, we have come now to a 
time of testing. 

10
00:00:53,740 --> 00:01:01,000
We must not fail. 
Let us close the springs of racial poison. 

11
00:01:01,000 --> 00:01:06,700
Let us pray for wise and understanding hearts. 

12
00:01:06,700 --> 00:01:11,000
Let us lay aside irrelevant differences 

13
00:01:11,000 --> 00:01:16,671
and make our nation whole. 
 >> Clearly, in many ways, the United States 

14
00:01:16,671 --> 00:01:21,300
is more whole than it was back 
then, as the election and reelection 

15
00:01:21,300 --> 00:01:26,400
of Barack Obama suggests. 
But in the United States and many other countries, 

16
00:01:26,400 --> 00:01:29,940
the springs of racial poison 
are far from closed, 

17
00:01:29,940 --> 00:01:33,000
and other intergroup biases persist as 
well. 

18
00:01:33,000 --> 00:01:36,100
Why should that be, after all these years? 

19
00:01:37,060 --> 00:01:41,300
Well, one reason is that intergroup 
biases are attached to some relatively 

20
00:01:41,300 --> 00:01:44,700
slow moving institutions and larger 
systems, 

21
00:01:44,700 --> 00:01:49,510
like culture, law, and economics. 
For example, to the extent that 

22
00:01:49,510 --> 00:01:52,010
neighborhoods and schools remain 
segregated, 

23
00:01:52,010 --> 00:01:56,250
it's hard to reduce prejudice and 
stereotyping beyond a certain point. 

24
00:01:56,250 --> 00:02:00,300
You really need positive intergroup 
contact. 

25
00:02:00,430 --> 00:02:04,316
But there's another reason, and that is 
that intergroup biases don't always 

26
00:02:04,316 --> 00:02:09,300
feel like they're biases, either because 
they operate outside our awareness 

27
00:02:09,300 --> 00:02:14,300
or they don't seem like they're 
harming anyone, so they are very slow to change. 

28
00:02:14,300 --> 00:02:18,000
In this video, I'll mention just three examples. 

29
00:02:18,000 --> 00:02:22,020
First, the outgroup homogeneity bias. 

30
00:02:22,020 --> 00:02:26,800
Second, positive stereotypes and 
benevolent forms of prejudice. 

31
00:02:26,800 --> 00:02:30,600
And finally, ingroup favoritism and 
affinity. 

32
00:02:30,960 --> 00:02:34,810
Let's begin with a discussion of outgroup 
homogeneity bias, 

33
00:02:34,810 --> 00:02:38,905
which is a well documented tendency for 
people to see outgroup members 

34
00:02:38,905 --> 00:02:45,200
as more alike from one to the next, 
more homogeneous than ingroup members. 

35
00:02:45,200 --> 00:02:48,725
For example, in the United States 
Democrats might see more 

36
00:02:48,725 --> 00:02:53,250
variation within their political party 
than within the Republican party. 

37
00:02:53,250 --> 00:02:58,400
They'd say, "Democrats come from all walks 
of life, whereas Republicans tend to be 

38
00:02:58,400 --> 00:03:04,000
more similar from one to the next." 
But Republicans might say just the opposite. 

39
00:03:05,100 --> 00:03:06,906
Now, you could be thinking, "Well, of course 

40
00:03:06,906 --> 00:03:11,048
they see more variation in their own group -- 
they know more members of the ingroup 

41
00:03:11,048 --> 00:03:14,760
than the outgroup." And certainly, that 
factor can play a role, 

42
00:03:14,760 --> 00:03:19,154
but there's more to it than that. 
For one thing, some studies have found 

43
00:03:19,154 --> 00:03:22,916
that the outgroup homogeneity effect is 
not related to the number of people 

44
00:03:22,916 --> 00:03:27,124
you know in each group. 
Also, the bias takes place with groups 

45
00:03:27,124 --> 00:03:32,400
that have quite a bit of contact with 
each other, like females and males. 

46
00:03:32,400 --> 00:03:36,190
How many times have you heard women say, 
 >> "Oh, you men are all alike." 

47
00:03:36,190 --> 00:03:38,000
 >> And how many times have you heard 
men say, 

48
00:03:38,000 --> 00:03:42,500
 >> "You women are all alike." 
 >> That's the outgroup homogeneity bias. 

49
00:03:42,500 --> 00:03:44,350
Now, why does it matter? 

50
00:03:44,350 --> 00:03:47,878
Well, in large part, it matters because 
reducing members of an outgroup to a 

51
00:03:47,878 --> 00:03:52,840
single identity is just one step away 
from stereotyping them. 

52
00:03:52,840 --> 00:03:57,256
So, perceiving an outgroup as homogeneous 
might not feel like prejudice, 

53
00:03:57,256 --> 00:04:02,858
but it can lead to stereotyping and discrimination. 
The good news is that if you flip things 

54
00:04:02,858 --> 00:04:06,826
around and get people to think about 
differences among outgroup members, 

55
00:04:06,826 --> 00:04:11,650
prejudice and discrimination can actually 
be reduced. 

56
00:04:11,650 --> 00:04:16,600
This finding was reported in 2011 by 
researchers in France who conducted 

57
00:04:16,600 --> 00:04:19,522
a study in which students were told that 
they'd be participating 

58
00:04:19,522 --> 00:04:26
in a pair of unrelated experiments, one on 
memory, and the other on hiring decisions, 

59
00:04:26 --> 00:04:31,122
when in reality, they were participating in a 
two-phase experiment on prejudice. 

60
00:04:31,122 --> 00:04:36,600
In the memory phase, students were 
randomly assigned to one of three conditions. 

61
00:04:36,600 --> 00:04:38,692
In the first condition, students were 

62
00:04:38,692 --> 00:04:42,520
shown eight photos of Arabs, and asked to 
write sentences 

63
00:04:42,520 --> 00:04:45,125
about how the individuals were similar to each 
other, 

64
00:04:45,125 --> 00:04:51,100
supposedly to prepare for a later memory 
test on the faces that were shown. 

65
00:04:51,100 --> 00:04:56,700
Students were also asked to do the same 
thing with eight abstract paintings. 

66
00:04:56,700 --> 00:05:00,400
In a second condition, students were 
shown the same photos and asked to write 

67
00:05:00,400 --> 00:05:05
sentences about how the individuals 
differed from each other, and after that 

68
00:05:05 --> 00:05:10
to again do the same thing with eight 
abstract paintings. And in the control 

69
00:05:10 --> 00:05:13,830
condition, students weren't asked to 
write any sentences; 

70
00:05:13,830 --> 00:05:18,400
they were simply shown the Arab 
individuals and the abstract paintings. 

71
00:05:18,400 --> 00:05:21,631
The researchers were kind enough to send 
me photos so that 

72
00:05:21,631 --> 00:05:26,000
our class can see the sort of images that 
were used in the study. 

73
00:05:26,000 --> 00:05:30,000
As you can see, the images present Arab 
people who differed in gender, 

74
00:05:30,000 --> 00:05:36,500
age, and style of clothing. 
Then, in what seemed to be a second experiment, 

75
00:05:36,500 --> 00:05:40
students were asked by a 
different researcher to evaluate 

76
00:05:40 --> 00:05:45,849
four job candidates for a sales position. 
Roughly half the time, the strongest candidate's 

77
00:05:45,849 --> 00:05:51,674
CV had a male French name,
and roughly half the time, the same CV 

78
00:05:51,674 --> 00:05:56,761
had a male Arab name. 
The results showed that students who wrote about 

79
00:05:56,761 --> 00:06:01,031
Arab homogeneity or students 
in the control condition who didn't write 

80
00:06:01,031 --> 00:06:07
any sentences later discriminated against 
the Arab candidate, whereas students 

81
00:06:07 --> 00:06:12,230
who wrote about the variability of Arabs 
showed no job discrimination at all -- 

82
00:06:12,230 --> 00:06:16,900
that is, it didn't matter whether the 
candidate had a French name or an Arab name. 

83
00:06:18,000 --> 00:06:20,518
So even though perceptions of variability 

84
00:06:20,518 --> 00:06:24,361
might not seem like much of a bias, we 
can use our knowledge of the 

85
00:06:24,361 --> 00:06:29,900
outgroup homogeneity bias to reduce 
discrimination, by deliberately focusing 

86
00:06:29,900 --> 00:06:35,310
on the variability of outgroup members. 
Now, you might ask, is the outgroup 

87
00:06:35,310 --> 00:06:39,500
always seen as more homogeneous? 
And the answer is no. 

88
00:06:39,500 --> 00:06:45,200
Meta-analyses suggest that the effect is 
strongest when the ingroup is relatively large, 

89
00:06:45,200 --> 00:06:47,481
and when the ingroup and outgroup are enduring, 

90
00:06:47,481 --> 00:06:54,300
real-life groups, not simply 
temporary groups created artificially in a laboratory. 

91
00:06:54,300 --> 00:06:57,200
If the ingroup is fairly small and the attributes 

92
00:06:57,200 --> 00:07:00,711
in question are important to 
its identity or stereotypically 

93
00:07:00,711 --> 00:07:05,200
associated with the group, then the 
outgroup homogeneity effect 

94
00:07:05,200 --> 00:07:13,000
may disappear or even reverse. 
For example, in 2011 a team of British researchers 

95
00:07:13,000 --> 00:07:15,890
found that female nurses, who 
are in the majority, 

96
00:07:15,890 --> 00:07:20,720
tend to see male nurses as more 
homogeneous than female nurses, 

97
00:07:20,720 --> 00:07:26,000
but male nurses, on the other hand, show 
an ingroup homogeneity effect, 

98
00:07:26,000 --> 00:07:30,500
meaning that they see 
members of their own group as more homogeneous. 

99
00:07:30,500 --> 00:07:33,900
Likewise, the study found that male police officers 

100
00:07:33,900 --> 00:07:38,000
show the traditional 
outgroup homogeneity effect towards female officers, 

101
00:07:38,000 --> 00:07:43,190
but female officers show 
an ingroup homogeneity effect. 

102
00:07:43,190 --> 00:07:47,300
Let's pause for a pop-up question, just to 
make sure that these findings are clear. 

103
00:07:50,640 --> 00:07:56,800
So, the bottom line is that in some cases 
there's actually an ingroup homogeneous bias, 

104
00:07:56,800 --> 00:07:58,029
but more often people tend to see outgroups 

105
00:07:58,029 --> 00:08:03,312
as relatively homogeneous, 
a bias that can lead to stereotyping 

106
00:08:03,312 --> 00:08:08,060
even though it doesn't always seem that we're 
behaving in a biased way. 

107
00:08:08,060 --> 00:08:11,756
As I mentioned earlier, a second way that 
intergroup biases can occur 

108
00:08:11,756 --> 00:08:16,000
without it seeming like anything is wrong is through 
positive stereotypes and 

109
00:08:16,000 --> 00:08:22,380
benevolent forms of prejudice. 
What do I mean by "positive" stereotypes? 

110
00:08:22,380 --> 00:08:26,200
Well, for example, in the the United 
States, Asian people are often 

111
00:08:26,200 --> 00:08:30,400
stereotyped as being good at math. 
Hispanic people are stereotyped 

112
00:08:30,400 --> 00:08:35,000
as being family-oriented. 
African-American men are often 

113
00:08:35,000 --> 00:08:39,600
stereotyped as having athletic ability. 
And women are stereotyped 

114
00:08:39,600 --> 00:08:45,076
as being more nurturing than men. 
All of these stereotypes feel like they're 

115
00:08:45,076 --> 00:08:50,000
compliments, and all have a kernel of 
truth in which the stereotype accurately 

116
00:08:50,000 --> 00:08:55
describes some members of the group, but 
they are all overgeneralizations 

117
00:08:55 --> 00:09:00,990
that reduce a diverse collection of people to 
a single type, to a stereotype. 

118
00:09:00,990 --> 00:09:06,200
One of the most interesting ways that 
this dynamic plays out is with respect to sexism. 

119
00:09:06,300 --> 00:09:10,000
Social psychologists Peter Glick and Susan Fiske 

120
00:09:10,000 --> 00:09:13,866
have proposed that, in addition to 
hostile forms of sexism that show 

121
00:09:13,866 --> 00:09:19,490
contempt toward women, there's also such a thing 
as benevolent sexism. 

122
00:09:19,490 --> 00:09:23,355
Here's what they say: 
"Some forms of sexism are, 

123
00:09:23,355 --> 00:09:27,840
for the perpetrator, subjectively benevolent, 
characterizing women as 

124
00:09:27,840 --> 00:09:32,600
pure creatures who ought to be protected, 
supported, and adoredâ¦ 

125
00:09:32,600 --> 00:09:37,234
This idealization of women simultaneously 
implies that they are weak and 

126
00:09:37,234 --> 00:09:43,340
best suited for conventional gender roles; 
being put on a pedestal is confining, 

127
00:09:43,340 --> 00:09:47,200
yet the man who places a woman there is 
likely to interpret this as cherishing, 

128
00:09:47,200 --> 00:09:51,380
rather than restricting, her (and many 
women may agree)." 

129
00:09:52,740 --> 00:09:57,000
In other words, unlike forms of prejudice 
in which the target group is hated, 

130
00:09:57,000 --> 00:10:02,020
benevolent sexism can take place even 
when the target of prejudice is loved, 

131
00:10:02,020 --> 00:10:06,644
and because of that, women themselves may 
embrace, or at least tolerate, 

132
00:10:06,644 --> 00:10:11,000
benevolent sexism. 
To illustrate what benevolent sexism looks like, 

133
00:10:11,000 --> 00:10:14,500
I'd like to share with you two 
vintage TV commercials that aired 

134
00:10:14,500 --> 00:10:19,600
sometime around the 1960s. 
The first is a Goodyear tire advertisement 

135
00:10:19,600 --> 00:10:24,500
that presents women as 
vulnerable and in need of a man's protection. 

136
00:10:29,600 --> 00:10:36,160
 >> This flat tire needs a man. 
But, when there's no man around, 

137
00:10:48,500 --> 00:10:54,825
when there's no man around, 
Goodyear should be. 

138
00:10:54,825 --> 00:10:58,000
Why? 
Watch this. 

139
00:10:58,000 --> 00:11:03,400
New Goodyear Double Eagle, 
carries its own spare inside. 

140
00:11:03,400 --> 00:11:08,800
Lifeguard safety spare. 
A tire in a tire. 

141
00:11:08,800 --> 00:11:10,800
Keeps on going. 

142
00:11:10,800 --> 00:11:14,600
Next time, give her a second chance. 

143
00:11:25,990 --> 00:11:27,820
 >> So the message is that women need the 

144
00:11:27,820 --> 00:11:31,910
protection of either a man, or a 
lifeguard safety feature -- 

145
00:11:31,910 --> 00:11:35,000
that women need to be guarded and 
protected. 

146
00:11:35,000 --> 00:11:39
In the next TV commercial, one woman 
gives another advice on how to 

147
00:11:39 --> 00:11:42,450
make good coffee for her husband -- a traditional 
gender role. 

148
00:11:42,450 --> 00:11:49,000
And when the husband compliments his wife 
on the coffee, she looks like she's just won the lottery. 

149
00:11:49,500 --> 00:11:52,000
>> Folgers is different. They blend it special. 

150
00:11:52,000 --> 00:11:54,300
And Folgers is mountain grown coffee. 

151
00:11:54,300 --> 00:11:56,900
 >> Mountain grown? 
 >> That's the richest kind. 

152
00:11:56,900 --> 00:12:01,000
You try it. 
 >> Your coffee, Sir. 

153
00:12:01,000 --> 00:12:04,500
 >> Oh, thanks Honey. 
 >> You're welcome. 

154
00:12:04,500 --> 00:12:10,500
 >> It's great, Honey! 
How can such a pretty wife make such great coffee? 

155
00:12:11,000 --> 00:12:13,500
 >> I heard that. 

156
00:12:13,500 --> 00:12:16,640
 >> Try Folger's, the mountain grown 
coffee -- 

157
00:12:16,640 --> 00:12:22,905
mountain grown for better flavor. 
 >> Of course, things have improved since 

158
00:12:22,905 --> 00:12:28,000
the time these commercials were made, but 
benevolent sexism is still very much with us. 

159
00:12:28,000 --> 00:12:30,514
Here, for example, is an advertisement 

160
00:12:30,514 --> 00:12:35,760
that ran in the February 2013 issue of 
Woman's Day magazine 

161
00:12:35,760 --> 00:12:39,536
in which a woman is not only shown in a 
traditional gender role, 

162
00:12:39,536 --> 00:12:45,190
but in fact, her apron is more like a costume 
or a uniform than a functional accessory, 

163
00:12:45,190 --> 00:12:49,100
because she's simply boiling a plastic 
bag for ten minutes -- not something 

164
00:12:49,100 --> 00:12:53,710
that you need an apron for. 
What these examples suggest is that you don't 

165
00:12:53,710 --> 00:12:58,600
need to dislike a target group -- in 
this case women -- in order for one group 

166
00:12:58,600 --> 00:13:02,200
to be treated as lower in status than 
another group. 

167
00:13:02,200 --> 00:13:06,700
And this point fits perfectly with one 
last example of how intergroup bias 

168
00:13:06,700 --> 00:13:11,570
can fly under the radar without being 
detected. 

169
00:13:11,570 --> 00:13:15,514
As I mentioned in the previous lecture, 
discrimination and bias often 

170
00:13:15,514 --> 00:13:20,500
have less to do with attitudes toward the outgroup 
than with ingroup favoritism -- 

171
00:13:20,500 --> 00:13:24,428
that is, preferring the ingroup over the 
outgroup, which doesn't always 

172
00:13:24,428 --> 00:13:28,641
feel like intergroup bias. 
After all, we might treat our friends 

173
00:13:28,641 --> 00:13:32,970
and family members with more consideration 
than we would strangers, 

174
00:13:32,970 --> 00:13:36,400
but does that mean that we're biased 
against strangers? 

175
00:13:36,400 --> 00:13:41,100
I think most of us would say no, but the 
net effect can still disadvantage 

176
00:13:41,100 --> 00:13:45,900
the outgroup. 
In fact, the Social Psychology Network partner site 

177
00:13:45,900 --> 00:13:51,500
UnderstandingPrejudice.org, has an 
interactive demonstration that shows how 

178
00:13:51,500 --> 00:13:54,570
small preferences to be with ingroup 
members 

179
00:13:54,570 --> 00:13:58,800
can lead to surprisingly strong patterns 
of segregation. 

180
00:13:58,800 --> 00:14:02,590
To take just one example, here's a board 
with two groups, 

181
00:14:02,590 --> 00:14:08,800
represented by green and blue tokens, 
randomly distributed on a 10-by-10 board. 

182
00:14:08,800 --> 00:14:11,200
Suppose that each green token wants to 

183
00:14:11,200 --> 00:14:15,020
have at least 3 green neighbors in 
adjacent squares, 

184
00:14:15,020 --> 00:14:18,500
and each blue token wants to have at 
least 3 blue neighbors -- 

185
00:14:18,500 --> 00:14:22,240
a very modest preference to be with 
ingroup members. 

186
00:14:22,240 --> 00:14:25,957
No one's asking here to be in an all-green 
neighborhood or 

187
00:14:25,957 --> 00:14:30,157
even in a majority green neighborhood. 
Each token that doesn't have 

188
00:14:30,157 --> 00:14:35,602
its preference satisfied has a red X. 
And when you place your cursor over the 

189
00:14:35,602 --> 00:14:39,600
tokens with the red X, gold squares show 
all the places where the token 

190
00:14:39,600 --> 00:14:45,600
might be moved to become happy -- 
that is, to have its preference satisfied. 

191
00:14:45,600 --> 00:14:50,200
I'll move just a few pieces and you can 
see the red Xs disappear. 

192
00:14:50,200 --> 00:14:53,980
But what's the end result if we keep 
doing this? 

193
00:14:53,980 --> 00:14:57,945
To save time, I'll click "AutoComplete," 
and you can see how segregated 

194
00:14:57,945 --> 00:15:03,500
the community becomes. 
So, very minor preferences at the individual level 

195
00:15:03,500 --> 00:15:07,400
can have powerful 
consequences at the group level. 

196
00:15:08,200 --> 00:15:12,105
Fortunately, it's also the case that a 
preference to be near even 

197
00:15:12,105 --> 00:15:16,900
one outgroup member is enough to reverse 
the effect and reduce segregation. 

198
00:15:16,900 --> 00:15:20,493
For those of you interested in seeing how 
that dynamic works, let me invite you to 

199
00:15:20,493 --> 00:15:26,300
visit UnderstandingPrejudice.org and take 
just five or ten minutes to go through 

200
00:15:26,300 --> 00:15:31,015
the segregation demonstration. 
Meanwhile, I'd like to share one other 

201
00:15:31,015 --> 00:15:36,000
example of how ingroup favoritism can 
have much the same effect as prejudice 

202
00:15:36,000 --> 00:15:40,400
even when there are no negative feelings 
toward the outgroup. 

203
00:15:40,400 --> 00:15:45,700
In 2005, a team of British psychologists 
lead by Mark Levine studied prejudice 

204
00:15:45,700 --> 00:15:49,200
in an experiment modeled after the Good 
Samaritan study 

205
00:15:49,200 --> 00:15:54,300
that we discussed earlier in the course. 
In Levine's study, British fans of a 

206
00:15:54,300 --> 00:15:57,370
particular soccer team answered a couple 
of surveys that  

207
00:15:57,370 --> 00:16:01,326
asked them about their home team, 
and then they had to walk between the 

208
00:16:01,326 --> 00:16:05,840
psychology department and another 
building to view a videotape. 

209
00:16:05,840 --> 00:16:10,220
Here's an aerial view of Lancaster 
University, where the study took place. 

210
00:16:10,220 --> 00:16:15,205
The psychology department was here, and 
each participant followed this path, 

211
00:16:15,205 --> 00:16:20,900
at which point a jogger cut across this 
grassy area, tripped about 15 feet away 

212
00:16:20,900 --> 00:16:24,800
from the participant, landed on the 
ground, and shouted in pain 

213
00:16:24,800 --> 00:16:29,700
while holding his ankle. 
In reality, the jogger was a confederate,

214
00:16:29,700 --> 00:16:31,850
and the main dependent variable was whether 

215
00:16:31,850 --> 00:16:36,010
people would help. 
The answer is that they did help, 

216
00:16:36,010 --> 00:16:41,920
12 out of 13 times, when the jogger happened to 
be wearing a home team shirt. 

217
00:16:41,920 --> 00:16:47,400
On the other hand, joggers wearing a rival 
team shirt were helped only 3 times out of 10, 

218
00:16:47,400 --> 00:16:49,600
and joggers who wore a sport shirt with 

219
00:16:49,600 --> 00:16:54,470
no team logo were helped only 4 times out of 12. 

220
00:16:54,470 --> 00:16:57,590
So, it wasn't that people treated an 
outgroup member as worse 

221
00:16:57,590 --> 00:17:01,100
than anyone else -- it was that they treated 
an ingroup member 

222
00:17:01,100 --> 00:17:06,090
better than anyone else. 
Of course, the million dollar question 

223
00:17:06,090 --> 00:17:09,700
is whether there's an effective way to 
reduce intergroup biases, 

224
00:17:09,700 --> 00:17:15,167
and I'm pleased to say that the answer is yes. 
This week's assigned reading covers 

225
00:17:15,167 --> 00:17:18,700
several effective techniques, but if we 
return to the soccer study, the 

226
00:17:18,700 --> 00:17:25,200
researchers found a very simple way to 
increase helping and reduce intergroup biases. 

227
00:17:25,500 --> 00:17:28,000
In a second experiment that was part of 
the same study, 

228
00:17:28,000 --> 00:17:32,800
they changed the initial 
surveys to ask about being a soccer fan 

229
00:17:32,800 --> 00:17:37,000
rather than asking people about being a 
fan of a particular team. 

230
00:17:37,000 --> 00:17:40,900
And when they did this, people helped 
8 out of 10 times when the jogger 

231
00:17:40,900 --> 00:17:45,170
wore a home team shirt, 
7 out of 10 times when the jogger 

232
00:17:45,170 --> 00:17:51,000
wore the rival team's shirt, but only 2 
out of 9 times when the jogger didn't 

233
00:17:51,000 --> 00:17:55,400
wear a soccer team shirt of any kind. 
So, by getting people to think about their 

234
00:17:55,400 --> 00:17:59,841
shared identity of being a soccer fan, 
rather than their identity of favoring a 

235
00:17:59,841 --> 00:18:06,000
particular team, they got people to help 
joggers who supported the rival team. 

236
00:18:07,370 --> 00:18:11,450
Well, this gives you a general overview 
of intergroup biases 

237
00:18:11,450 --> 00:18:15,200
beyond traditional forms of prejudice in 
which hatred, fear, and other 

238
00:18:15,200 --> 00:18:21,100
powerful emotions play a central role. 
One question that I've explored in my own research 

239
00:18:21,100 --> 00:18:25
is whether some of these biases 
operate not only when the outgroup 

240
00:18:25 --> 00:18:29,200
is of a different race, religion, gender, or 
sexual orientation,

241
00:18:29,200 --> 00:18:33,900
but of another species. 
For instance, do we see members of 

242
00:18:33,900 --> 00:18:38,000
another species as more alike from one to 
the next than they really are -- 

243
00:18:38,000 --> 00:18:42,500
a sort of outgroup homogeneity effect in 
which animals are seen as relatively 

244
00:18:42,500 --> 00:18:49,200
interchangeable with one another 
(cows are cows, pigs are pigs, and so forth)? 

245
00:18:49,200 --> 00:18:53,800
Second, do humans show species-based ingroup bias, 

246
00:18:53,800 --> 00:18:56,200
and if so, what are the consequences for how 

247
00:18:56,200 --> 00:19:00,100
we think about animals? 
For example, if you eat meat, 

248
00:19:00,100 --> 00:19:05,060
do you think of it as coming from something or 
someone? 

249
00:19:05,060 --> 00:19:09,010
If you eat dairy products, do you 
associate them with a lactating animal? 

250
00:19:09,010 --> 00:19:13,882
And if not, why? 
Does it make sense to talk about being 

251
00:19:13,882 --> 00:19:20,000
prejudiced to certain animals, or does 
prejudice really require a human outgroup? 

252
00:19:20,000 --> 00:19:24,000
And even if your view is that prejudice can 
only run from human to human, 

253
00:19:24,000 --> 00:19:28,200
what can we learn about prejudice from 
how we think about animals? 

254
00:19:28,200 --> 00:19:30,900
Here, for example, are some questions that I posed 

255
00:19:30,900 --> 00:19:34,560
in a 2003 book on prejudice and 
discrimination. 

256
00:19:35,900 --> 00:19:41,300
Is it significant that the word "mulatto" 
(often used as a synonym for half-breed) 

257
00:19:41,300 --> 00:19:47,200
shares its etymology with "mule"? Or that 
race emerged from terms for animal breeding? 

258
00:19:47,200 --> 00:19:49,800
Does it mean anything that the word "husband" 

259
00:19:49,800 --> 00:19:54
shares a common origin with 
animal husbandry, or that rape 

260
00:19:54 --> 00:19:57,200
was originally classified as a property crime? 

261
00:19:58,440 --> 00:20:02,300
I hope that you'll use the discussion 
forums to consider these questions, 

262
00:20:02,300 --> 00:20:06,050
as well as your own questions about the 
psychology of prejudice. 

263
00:20:06,050 --> 00:20:10,980
You have a rare opportunity to interact 
with people from all over the world.

264
00:20:10,980 --> 00:20:14,700
All I ask, as before, is that you respect 
cultural differences 

265
00:20:14,700 --> 00:20:19,500
and keep the conversation positive and focused on 
psychology. 

266
00:20:19,500 --> 00:20:23,075
To help promote dialogue, the teaching 
assistants and I have posted a starter 

267
00:20:23,075 --> 00:20:29,170
set of "springboard" questions designed to 
help everybody dive into the discussion. 

268
00:20:29,170 --> 00:20:33,324
What you'll see are a whole host of 
sub-forums and discussion threads 

269
00:20:33,324 --> 00:20:36,070
just waiting for you to jump in! 

