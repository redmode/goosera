


1
00:00:30,043 --> 00:00:33,000
In the heartland of the American Midwest, 

2
00:00:33,000 --> 00:00:39,500
eight-year-old Noah is already a boy apart. 

3
00:00:43,300 --> 00:00:51,000
Midwest America is a real difficult place for a child like Noah. 

4
00:00:58,500 --> 00:01:03,119
We have a lot of farmers -- you know, hardworking people that have 

5
00:01:03,119 --> 00:01:10,400
always grown up that a boy's a boy, and a girl's a girl, and it's also 

6
00:01:10,400 --> 00:01:15,950
a strong religious belief that Noah shouldn't be like that 'cause 

7
00:01:15,950 --> 00:01:21,490
God didn't make him that way. It's hard to make people understand. 

8
00:01:21,490 --> 00:01:26,700
 >> I like to make stuff outta, like, ribbons and stuff like shirts and skirts 

9
00:01:26,700 --> 00:01:28,400
and stuff like that.  

10
00:01:28,400 --> 00:01:29,880
>> I know it's nothing that we've done. 

11
00:01:29,880 --> 00:01:33,840
Noah has been that way since he's been old enough to express himself.

 12
00:01:33,840 --> 00:01:37,300
We've done many things to try and steer Noah away from the way he is now.

13
00:01:37,300 --> 00:01:41,526
Nothing's been encouraging. I think with a child like Noah, if you 

14
00:01:41,526 --> 00:01:46,190
accept him unconditionally, you're teaching him love, you're teaching him 

15
00:01:46,190 --> 00:01:50,000
that his parents love him and they support him no matter 

16
00:01:50,000 --> 00:01:54,790
if he's different than the other boys, and I do know one thing: 

17
00:01:54,790 --> 00:01:57,100
he didn't choose to be like this.  

18
00:01:57,100 --> 00:02:03,212
>> You put it this, put in the back and you twist it in the back. 

19
00:02:03,212 --> 00:02:07,929
Pull on it like that, put it up here and tie it. 

20
00:02:07,929 --> 00:02:13,000
 >> Noah's mother, Michelle, and her present husband, Mike. 

21
00:02:13,000 --> 00:02:14,320
 >> Did I not give you two? 

22
00:02:14,320 --> 00:02:19,370
 >> Noah's older brother, Luke -- a boy like any other boy. 

23
00:02:19,370 --> 00:02:22,900
>> For the sake of the children, the adults agreed to put 

24
00:02:22,900 --> 00:02:27,350
their differences behind them and build a relationship that 

25
00:02:27,350 --> 00:02:30,822
would make the boys feel equally at home in either household, 

26
00:02:30,822 --> 00:02:35,870
their father's or their mother's -- not the only decision that makes 

27
00:02:35,870 --> 00:02:38,000
this family special. 

28
00:02:38,000 --> 00:02:42,800
>> We don't know a single other family that deals with these issues.

29
00:02:42,800 --> 00:02:48,000 
We're working it all out ourselves, and then when I have my family 

30
00:02:48,000 --> 00:02:50,600
that's saying -- the family I was raised in -- 

31
00:02:50,600 --> 00:02:54,800
that's saying you're doing it wrong, and I feel so much in my heart 

32
00:02:54,800 --> 00:02:58,150
that I'm doing it right, sometimes it's overwhelming. 

33
00:02:58,150 --> 00:03:01,260
 >> Why do your family think you're doing the wrong thing? 

34
00:03:01,260 --> 00:03:04,770
>> We're a Christian family, and that's what the Bible teaches. 

35
00:03:04,770 --> 00:03:09,120
Literally, specifically, I think in Deuteronomy it says that men 

36
00:03:09,120 --> 00:03:15,600
are not to wear women's clothing. In Genesis, it says, God made a man 

36
00:03:15,600 --> 00:03:20,900
and a woman so that they might procreate. This is exactly what it says. 

37
00:03:21,880 --> 00:03:25,000
No matter what was written in the Bible or any historical document, 

38
00:03:25,000 --> 00:03:31,340
I can't believe in a God who would not love my Noah for exactly the way he is. 

39
00:03:31,340 --> 00:03:36,900
He gets teased, boys and even girls come up and ask him, "Are you a girl?"

41
00:03:36,900 --> 00:03:40,000
I would just like to tell people that

42
00:03:40,000 --> 00:03:47,900
I don't feel like answering all those questions, so, don't ask me. 

43
00:03:47,900 --> 00:03:52,570
 >> What are your greatest fears? >>Just that someone will hurt him. 

44
00:03:52,570 --> 00:03:57,800
I've read it more than once that there's about a 50% chance that these 

45
00:03:57,800 --> 00:04:08,536
children will be killed or kill themselves. That would be my biggest concern. 

46
00:04:15,536 --> 00:04:19,100
>>The violence against our community of transgender women 

47
00:04:19,100 --> 00:04:27,000
tends to be really just over the top and, and angry and, and horrible. 

48
00:04:27,000 --> 00:04:31,000
Usually a transgender woman is not just punched or, 

49
00:04:31,000 --> 00:04:35,490
or beaten up or, or even just murdered with a gunshot. 

50
00:04:35,490 --> 00:04:40,500
There are transgender women who are, are shot with machine guns 

51
00:04:40,500 --> 00:04:45,244
until they're, they're just pieces of meat -- until they're unrecognizable. 

52
00:04:45,244 --> 00:04:49,000
There, there are women who are stabbed 30, 40, 50 times 

53
00:04:49,000 --> 00:04:54,800
and then set on fire. If you can just imagine physically, 

54
00:04:54,800 --> 00:05:01,000
the time it would take to stab someone 40 times. 

55
00:05:01,000 --> 00:05:05,120
 >> An everyday scene: four friends out for lunch. 

56
00:05:05,120 --> 00:05:08,700
On the surface, nothing remarkable. Nothing to suggest that two 

57
00:05:08,700 --> 00:05:14,800
of these women are at risk, and never more so, should they fall in love. 

58
00:05:14,800 --> 00:05:21,500
 >> In 1999, I met a soldier named Barry who was able to see me as 

59
00:05:21,500 --> 00:05:26,170
the woman I am and who loved me for the person that I am. 

60
00:05:26,170 --> 00:05:32,000
At a certain point, his fellow soldiers discovered that the girlfriend 

61
00:05:32,000 --> 00:05:36,000
that he talked about so much was a transsexual. You know, 

62
00:05:36,000 --> 00:05:43,000
they discovered that I was transsexual. This all culminated in, 

63
00:05:43,000 --> 00:05:49,000
in his murder on July 4th in 1999. They beat him to death 

64
00:05:49,000 --> 00:05:52,540
with a baseball bat in his sleep. 

65
00:05:55,420 --> 00:06:00,400
 >> We bring out a certain fear and a certain anger in some people 

66
00:06:00,400 --> 00:06:05,490
who interact with us, because suddenly they find themselves, 

67
00:06:05,490 --> 00:06:12,000
questioning themselves or worrying about how their response to us 

68
00:06:12,000 --> 00:06:15,600
affects their identity. Suddenly, something that they thought 

69
00:06:15,600 --> 00:06:19,446
was a bedrock part of their identity has been challenged. 

70
00:06:19,446 --> 00:06:21,522
Hey, Cal, looking at your board...

71
00:06:21,522 --> 00:06:24,870
 >> Andrea and Calpernia run a website serving the 

72
00:06:24,870 --> 00:06:28,400
transgender community, offering advice on everything from medical 

73
00:06:28,400 --> 00:06:33,600
and legal issues to fashion and make-up. Their personal histories 

74
00:06:33,600 --> 00:06:40,294
defy simple classification: male, female, hetero, homo. 

75
00:06:40,294 --> 00:06:44,000
 >> People always assume that this is all about sex, 

76
00:06:44,000 --> 00:06:49,000
and certainly we are all sexual beings in some way or another, 

77
00:06:49,000 --> 00:06:56,700
but that's not the entire story. People transition for a lot of reasons. 

78
00:06:56,700 --> 00:06:59,200
One of the confusing things in my own case 

79
00:06:59,200 --> 00:07:02,670
for some people is that I'm attracted to women, 

80
00:07:02,670 --> 00:07:06,300
and so they think, oh, you know, why would you do all this 

81
00:07:06,300 --> 00:07:09,680
if you're attracted to women? And I think it really points out that 

82
00:07:09,680 --> 00:07:14,100
this isn't about who you, you know, want to be with -- it's about 

83
00:07:14,100 --> 00:07:18,300
who you are. People want to have this nice, simple, black and white, 

84
00:07:18,300 --> 00:07:22,000
binary world where everything's easily compartmentalized, but 

85
00:07:22,000 --> 00:07:25,000
there's always going to be people who challenge those categories, 

86
00:07:25,000 --> 00:07:29,450
and that's what we do with gender and sex and sexuality. 

86
00:07:29,450 --> 00:07:31,000
>> Haaa. 

87
00:07:31,000 --> 00:07:35,839
 >> So, who are these people who blur the lines between man and woman? 

88
00:07:35,839 --> 00:07:39,020
 >> Haaa. What I'd like to do with my voice, 

89
00:07:39,020 --> 00:07:43,195
get it to the point where it is sounding like a woman's. 

90
00:07:43,195 --> 00:07:49,000
>> What are the forces that shape them or shape any of us sexually? 

91
00:07:50,555 --> 00:07:59,164
 >> The answers to these questions begin before we are born. 

92
00:07:59,164 --> 00:08:02,860
In the early stages of life's journey, we all develop 

93
00:08:02,860 --> 00:08:10,000
the same basic set of organs, male and female, exactly alike. 

94
00:08:10,000 --> 00:08:16,750
Six-to-seven weeks after conception, we reach the first fork in the road. 

95
00:08:16,750 --> 00:08:22,000
The route we take from here is largely determined by our chromosomes:  

96
00:08:22,000 --> 00:08:29,550
male (XY), female (XX). We take the male direction when a protein on the 

97
00:08:29,550 --> 00:08:34,930
Y chromosome activates the testes, which produce the male hormone 

98
00:08:34,930 --> 00:08:43,700
androgen. The female parts wither away. 

99
00:08:43,700 --> 00:08:46,800
But what else can happen in those first three months 

100
00:08:46,800 --> 00:08:52,800
when males (XY) and females (XX) take their separate paths? 

101
00:08:52,800 --> 00:08:59,600
There are other combinations -- XXY, XYY, XO -- that can send the developing 

102
00:08:59,600 --> 00:09:05,800
child into territory in between, neither male nor female: intersex. 

103
00:09:05,800 --> 00:09:07,996
>> With intersex, one of the things that can happen is that 

104
00:09:07,996 --> 00:09:11,000
the phallus can be halfway in between. You can have something 

105
00:09:11,000 --> 00:09:13,500
that looks like a penis, but the opening is on the side. 

106
00:09:13,500 --> 00:09:17,550
Or you can have something that looks like a clitoris but is quite large. 

107
00:09:17,50 --> 00:09:22,510
 >> One person in a hundred has a body that differs from what doctors consider 

108
00:09:22,510 --> 00:09:30,948
standard male or female. That's over 4,000 people in a city this size.

109
00:09:30,948 --> 00:09:35,850
In our culture the idea of an intermediate sex seems threatening 

110
00:09:35,850 --> 00:09:38,900
and the whole issue has been shrouded in secrecy. 

111
00:09:38,900 --> 00:09:47,000
Recently, though, people with these conditions have stepped out of the shadows. 

113
00:09:47,000 --> 00:09:53,000
>> When I was born, doctors couldn't determine if I was a boy or girl. 

114
00:09:53,000 --> 00:09:58,200
I had what are described as ambiguous genitalia. 

115
00:09:58,200 --> 00:10:04,800
My parents were confused, scared. They weren't able to tell 

116
00:10:04,800 --> 00:10:08,500
anyone who knew they'd had a child if it was a boy or a girl. 

117
00:10:08,500 --> 00:10:10,600
The challenge of people who used to be called hermaphroditic -- 

118
00:10:10,600 --> 00:10:14,200
what we now call intersex, have intersex conditions -- is that they

119
00:10:14,200 --> 00:10:18,350
really do threaten the idea that there's this clear natural boundary between 

120
00:10:18,350 --> 00:10:21,970
the male and the female, because there isn't a clear natural boundary. 

121
00:10:21,970 --> 00:10:26,900
So, intersex begins to raise for us the question of what gender really means 

122
00:10:26,900 --> 00:10:31,134
and to what extent it really matters, and that's very uncomfortable. 

123
00:10:31,134 --> 00:10:34,900
>> And, until very recently, the medical establishment was 

124
00:10:34,900 --> 00:10:38,620
determined that there should be no ambiguity. 

125
00:10:38,620 --> 00:10:42,000
>> The finding of ambiguous genitalia in the newborn is a medical 

126
00:10:42,000 --> 00:10:47,450
and social emergency. When the cause is established and gender 

127
00:10:47,450 --> 00:10:53,024
assignment is made, the abnormal genitalia must be corrected. 

128
00:10:53,024 --> 00:10:57,200
>> Max was just a year old when his phallus was surgically reduced. 

129
00:10:57,200 --> 00:11:01,900
 >> He was brought up as a girl, Judy, who underwent a whole series of 

130
00:11:01,900 --> 00:11:08,150
operations until the age of 15, never once being told what they were for.

131
00:11:08,150 --> 00:11:12,000
 >> For 20-odd years, I wasn't allowed to talk to anyone about this 

132
00:11:12,000 --> 00:11:17,240
and didn't tell -- just like my mother didn't tell her best friend, 

133
00:11:17,240 --> 00:11:19,830
I, I never even told my best friend. 

134
00:11:19,830 --> 00:11:22,850
 >> By her late teens, Judy felt confused. 

135
00:11:22,850 --> 00:11:26,400
She tried a relationship with a boy and a girl. 

136
00:11:26,400 --> 00:11:31,120
Whereas my male partner, boyfriend, had not commented on 

137
00:11:31,120 --> 00:11:34,220
the differences in my genital anatomy -- which, incidentally, I wasn't 

138
00:11:34,220 --> 00:11:40,350
even aware of at the time -- my female partner did. She said something. 

139
00:11:40,350 --> 00:11:44,000
She said, "Boy, Jude, you sure are weird." I came away from that 

140
00:11:44,000 --> 00:11:50,550
thinking of myself as a monster or a freak, and so I decided that I would 

141
00:11:50,550 --> 00:11:58,000
avoid that upset by being with men. So, I quite literally settled down with 

142
00:11:58,000 --> 00:12:01,000
the next guy to come along. 

142
00:12:01,000 --> 00:12:04,800
>> Judy simply married a male friend from college,

143
00:12:04,800 --> 00:12:07,190
but the relationship was short-lived. 

144
00:12:07,190 --> 00:12:10,030
Judy had met Tamara.

145
00:12:10,030 --> 00:12:14,915
I was really excited. It was definitely a case of love at first sight. 

146
00:12:14,915 --> 00:12:18,000
Sparks just flew -- it was magic.  

147
00:12:18,000 --> 00:12:20,350
>> I looked at her and fell in love with her. 

147
00:12:20,350 --> 00:12:24,100
It was love at first sight. She was breathtakingly beautiful. 

148
00:12:24,100 --> 00:12:27,000
 >> Then a bombshell hit. 

149
00:12:27,000 --> 00:12:29,990
I needed my childhood immunization records, so I contacted my 

150
00:12:29,990 --> 00:12:34,110
childhood pediatrician and found out he had retired, and the woman 

151
00:12:34,110 --> 00:12:38,900
who had taken over his practice, had someone photocopy 

152
00:12:38,900 --> 00:12:43,370
my records and mail them. And I opened my mail in a diner in,

153
00:12:43,370 --> 00:12:47,350
in Center City Philadelphia, and right after my name -- 

154
00:12:47,350 --> 00:12:50,100
which at the time was Judy Elizabeth Beck -- were the words 

155
00:12:50,100 --> 00:12:57,200
"male pseudohermaphrodite," and I was devastated and dumbfounded. 

156
00:12:57,200 --> 00:13:01,000
At the same time, it was, it was almost a relief, because I had a label. 

157
00:13:01,000 --> 00:13:04,700
Not only did I know that I was a monster, but I, I could tell, I could point 

158
00:13:04,700 --> 00:13:11,000
in the textbook exactly what kind of monster I was. 

159
00:13:16,000 --> 00:13:19,700
>> We started corresponding and just had this flurry of letters, 

160
00:13:19,700 --> 00:13:26,500
and gradually just started little bitty disclosures, and, you know, "Boy, when I first met you, 

161
00:13:26,500 --> 00:13:30,200
you really turned my head," and I would write something back and, 

162
00:13:30,200 --> 00:13:33,700
you know, we eventually just called it peeling the onion because it was just 

163
00:13:33,700 --> 00:13:36,800
little tiny layer after little tiny layer to get to the heart of it. 

164
00:13:36,800 --> 00:13:40,670
I think it was a, it was a Friday evening. 

165
00:13:40,670 --> 00:13:48,000
We, we talked on the phone, and, and, and talked right through dawn, 

166
00:13:48,000 --> 00:13:52,502
and at some point, during the night, I, I told her. 

167
00:13:52,502 --> 00:13:58,500
I just remember being so sad that she thought that this was 

168
00:13:58,500 --> 00:14:03,310
going to make such a big difference in the way that I felt about her, 

169
00:14:03,310 --> 00:14:06,700
and that she hadn't had anybody to help carry the weight of that 

170
00:14:06,700 --> 00:14:12,600
her whole life, that she'd gone through, essentially, twenty-something years 

171
00:14:12,600 --> 00:14:17,800
thinking that if anyone knew this, they would just not want to be with her. 

172
00:14:17,800 --> 00:14:23,300
And I said, you know, that doesn't make any difference to me whatsoever. 

173
00:14:23,300 --> 00:14:27,300
 >> The couple lived an open lesbian relationship, but 

174
00:14:27,300 --> 00:14:32,100
Judy's knowledge of her own medical history was gnawing at her. 

175
00:14:32,100 --> 00:14:37,300
 >> I began to question how valid the lesbian identity was. 

176
00:14:37,300 --> 00:14:44,000
If I'm not female, can I be a lesbian? And thinking in those vicious circles 

177
00:14:44,000 --> 00:14:49,000
and undermining this precious shred of identity that I had finally

178
00:14:49,000 --> 00:14:54,100
obtained threw me into, yeah, a very bad depression. 

179
00:14:54,100 --> 00:14:56,900
I was hospitalized. 

179
00:14:56,900 --> 00:15:00,000
>> What emerged from this turmoil was a man. 

180
00:15:00,000 --> 00:15:03,500
Judy became Max. 

181
00:15:03,500 --> 00:15:06,900
The full transition took four years, and incredibly, 

182
00:15:06,900 --> 00:15:14,000
the loving bond with Tamara survived. They live as man and wife 

183
00:15:14,000 --> 00:15:17,665
with a child conceived by Tamara. 

184
00:15:17,665 --> 00:15:20,900
 >> At first I was resistant. I was like, no, you know, I, 

185
00:15:20,900 --> 00:15:24,260
I finally got this lesbian relationship thing lined up. 

186
00:15:24,260 --> 00:15:26,900
You know, I've got a community and I know who I am, and I 

187
00:15:26,900 --> 00:15:30,400
understand the terms of my relationship, and you can't go changing it 

188
00:15:30,400 --> 00:15:35,880
on me in the middle. But, on the other hand, I had a partner 

189
00:15:35,880 --> 00:15:39,080
that I loved desperately who was miserable most of the time and 

190
00:15:39,080 --> 00:15:42,880
uncomfortable in himself and uncomfortable in the world. 

191
00:15:42,880 --> 00:15:48,000
He's a much more balanced, doing-okay-psychologically guy. 

192
00:15:48,000 --> 00:15:55,688
And, I guess I sort of chalked that up as a fair bargain. 

193
00:15:55,688 --> 00:16:02,900
 >> This old man. >> This old man? 

195
00:16:02,900 --> 00:16:06,250
 >> I don't have a male identity, and I don't know that I ever had a 

196
00:16:06,250 --> 00:16:09,700
female identity, but I certainly don't have one now, and if pressed, 

197
00:16:09,700 --> 00:16:14,700
I suppose I would say I have intersexed identity. I think of myself as 

198
00:16:14,700 --> 00:16:22,800
intersexed, and I think I have a great deal more compassion than I ever did for men. 

199
00:16:22,800 --> 00:16:25,800
Goofy's wife ran into the house. There was Goofy. 

200
00:16:25,800 --> 00:16:28,000
 >> We've been together more than 10 years now, and we're 

201
00:16:28,000 --> 00:16:35,400
still together. This is the same person that I fell in love with. 

203
00:16:35,400 --> 00:16:38,470
We all change over time. You get married at 19. 

204
00:16:38,470 --> 00:16:42,700
By the time you're 69, you're not that girl of 19. 

205
00:16:42,700 --> 00:16:45,440
Your body will change. You'll thicken in some places. 

206
00:16:45,440 --> 00:16:50,110
Maybe you'll lose weight. What we've had is just a more quick 

207
00:16:50,110 --> 00:16:53,500
condensed radical transformation in one of us. 

208
00:16:53,500 --> 00:16:58,350
And yes, his body has changed. It's going to keep on changing. 

209
00:16:58,350 --> 00:17:01,540
You know, for the next 50 years, his hair's going to fall out, 

210
00:17:01,540 --> 00:17:06,420
and he's going to get wrinkles, and he's still going to be my Max. 

211
00:17:06,420 --> 00:17:11,160
 >> Unlike Max Beck, most of us are recognizably male or female 

212
00:17:11,160 --> 00:17:14,700
within three months of conception. Over the next six months, 

213
00:17:14,700 --> 00:17:18,900
hormones sculpt the brain -- male or female -- but the genitals 

214
00:17:18,900 --> 00:17:25,055
can develop in one direction, the brain in another. 

215
00:17:30,000 --> 00:17:34,700
 >> One thing we have to remember from Darwin to Kinsey to 

216
00:17:34,700 --> 00:17:41,100
any great thinker about sexuality is, variation is the norm. 

217
00:17:41,100 --> 00:17:46,000
Biology loves variation. Biology loves differences. 

218
00:17:46,000 --> 00:17:49,000
Society hates it. 

219
00:17:49,000 --> 00:17:52,300
 >> All creatures are united through descent. 

220
00:17:52,300 --> 00:18:02,000
We share a common ancestry with birds, fishes, trees, and every living thing. 

221
00:18:02,000 --> 00:18:07,000
So, what can nature tell us about ourselves? 

223
00:18:07,000 --> 00:18:13,000
 >> There's no precedent in nature for the rigid assertion of a 

224
00:18:13,000 --> 00:18:20,460
male-female binary of the sort that people are supposed to be obeying. 

225
00:18:23,300 --> 00:18:27,200
 >> Nature consists of rainbows within rainbows within rainbows, 

226
00:18:27,200 --> 00:18:32,200
so that even if one does manufacture some category, 

227
00:18:32,200 --> 00:18:36,000
once you start looking within the category, you find variation 

228
00:18:36,000 --> 00:18:38,830
within that, and then you could make a subcategory, 

229
00:18:38,830 --> 00:18:42,100
and you find variations still within that. 

230
00:18:42,100 --> 00:18:47,200
 >> In the natural world, diversity is everywhere, from sex-changing fish 

231
00:18:47,200 --> 00:18:51,800
to male seahorses that actually give birth. In some species, 

232
00:18:51,800 --> 00:18:55,900
male and female are indistinguishable. In others there are two or more 

233
00:18:55,900 --> 00:19:00,000
distinct male or female genders. There are even species 

234
00:19:00,000 --> 00:19:05,230
where intersex bodies are in the majority. 

235
00:19:05,230 --> 00:19:09,072
Some animals are promiscuous, others bond for life, 

236
00:19:09,072 --> 00:19:15,228
and over 300 vertebrate species engage in homosexual behavior. 

237
00:19:15,228 --> 00:19:22,000
But exclusive homosexuality is a human construct. 

238
00:19:28,500 --> 00:19:35,700
Suriname -- a small South American country with a vicious history. 

239
00:19:35,700 --> 00:19:39,800
The Creole people here are the descendants of African slaves 

240
00:19:39,800 --> 00:19:43,800
who suffered under one of the cruelest regimes ever imposed 

241
00:19:43,800 --> 00:19:48,000
on a subject people. The Dutch plantation owners literally 

242
00:19:48,000 --> 00:19:53,000
worked their slaves to death, continually replenishing stocks 

243
00:19:53,000 --> 00:19:58,400
with fresh blood from Africa. Punishments were grotesque. 

244
00:19:58,400 --> 00:20:00,900
Slaves had no rights to their own children, 

245
00:20:00,900 --> 00:20:05,000
and were forbidden to learn to read or write. 

246
00:20:05,000 --> 00:20:07,300
>> Under these conditions, one of the few things 

247
00:20:07,300 --> 00:20:13,800
that enabled them to survive was their African culture. 

248
00:20:13,800 --> 00:20:17,750
The Creole people believe that we're all visited by spirits 

249
00:20:17,750 --> 00:20:23,527
sent by God to help us through life's trials. 

250
00:20:23,527 --> 00:20:28,531
Sexuality is shaped by the spirits. When a woman is possessed 

251
00:20:28,531 --> 00:20:32,820
by a strong male spirit she will desire other women, 

252
00:20:32,820 --> 00:20:40,530
and a man will desire other men when visited by a female spirit. 

253
00:20:57,000 --> 00:21:02,650
>>The result is a society where sexuality is fluid and carries no stigma. 

254
00:21:02,650 --> 00:21:08,014
It's the quality of the relationship that counts.

256
00:21:15,500 --> 00:21:19,500
>> The spirits teach you whether you live as man and woman, 

257
00:21:19,500 --> 00:21:22,700
woman and woman, or man and man; 

258
00:21:22,700 --> 00:21:32,950
it's okay, as long as you handle the relationship decently. 

259
00:21:32,950 --> 00:21:37,775
Feelings don't come from the individual -- they come from God. 

260
00:21:37,775 --> 00:21:41,010
You may live with a man for perhaps ten years, 

261
00:21:41,010 --> 00:21:45,660
and then in the eleventh year, you get feelings for a woman. 

262
00:21:45,660 --> 00:21:49,600
It's not your doing -- it's God's doing. 

263
00:21:49,600 --> 00:21:56,300
>> Iris and Ellie came together over 20 years ago, when Iris's marriage was breaking down. 

264
00:21:56,300 --> 00:21:59,935
It was the first relationship either had had with another woman, 

265
00:21:59,935 --> 00:22:07,200
and they both have children and grandchildren of their own. 

267
00:22:07,200 --> 00:22:15,600
 >> I was unlucky with men, and then I found love with a woman. 

268
00:22:15,600 --> 00:22:19,100
Where you find happiness, that's where you should stay, 

269
00:22:19,100 --> 00:22:24,000
and I found happiness with Iris. 

270
00:22:24,000 --> 00:22:29,900
And so I asked my family to accept us, and they did -- 

271
00:22:29,900 --> 00:22:35,100
from the oldest to the youngest -- and that pleases me very much. 

272
00:22:35,100 --> 00:22:40,300
>> God made everybody. We don't make ourself.

273
00:22:40,300 --> 00:22:46,700
God made us, and he made homos, he made lesbians, 

274
00:22:46,700 --> 00:22:55,670
he made bisexuals, and every one of them has a purpose. 

275
00:22:55,670 --> 00:23:02,300
>> This way of thinking about same-sex desire, we find elsewhere 

276
00:23:02,300 --> 00:23:07,920
in the black diaspora. We find traces of it in Africa itself. 

277
00:23:07,920 --> 00:23:13,700
So, contrary to a very dominant discourse at this moment that 

278
00:23:13,700 --> 00:23:22,000
homosexuality is un-African, that the colonialist brought it to Africa, 

279
00:23:22,000 --> 00:23:27,900
I say that there is ample evidence that the principles are 

280
00:23:27,900 --> 00:23:33,200
and have been present in Africa for a long time. It is African. 

281
00:23:33,200 --> 00:23:35,720
It is human. It is everywhere. 
