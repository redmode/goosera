﻿1
00:00:12,690 --> 00:00:15,935
In this video, I'll give you a tour of Social Psychology Network

2
00:00:15,935 --> 00:00:19,720
with an emphasis on resources of interest to students,

3
00:00:19,720 --> 00:00:24,000
which isn't difficult because the Network began as a resource for students.

4
00:00:24,000 --> 00:00:31,000
SPN was born in 1996 as an ugly duckling web syllabus for my Social Psychology

5
00:00:31,000 --> 00:00:34,644
course at Wesleyan. And it didn't really become a

6
00:00:34,644 --> 00:00:40,000
professional network until 1999, when the U.S. National Science Foundation

7
00:00:40,000 --> 00:00:44,100
began funding it. So, thank you NSF!

8
00:00:44,100 --> 00:00:50,000
In 2004, two leading professional societies, the Society for Personality

9
00:00:50,000 --> 00:00:55,200
and Social Psychology and the Society of Experimental Social Psychology,

10
00:00:55,200 --> 00:01:02,500
both became sponsors of the Network, and in 2005, SPN became a member organization.

11
00:01:02,500 --> 00:01:07,000
It also has wonderful staff members and student interns who work

12
00:01:07,000 --> 00:01:10,800
around the clock -- literally nights, weekends, holidays --

13
00:01:10,800 --> 00:01:15,800
to keep the Network running and available 24 hours a day.

14
00:01:16,400 --> 00:01:20,000
So, what exactly is Social Psychology Network,

15
00:01:20,000 --> 00:01:24,000
and what are its goals? Well, as you can see on the home page of

16
00:01:24,000 --> 00:01:30,000
SocialPsychology.org, the mission is to promote peace, social justice,

17
00:01:30,000 --> 00:01:33,800
and sustainable living through public education, research, and the

18
00:01:33,800 --> 00:01:38,000
advancement of psychology. So, to take just one example,

19
00:01:38,000 --> 00:01:41,000
suppose that you were interested in either learning or teaching

20
00:01:41,000 --> 00:01:45,000
about the topic of racism. One thing you could do would be to

21
00:01:45,000 --> 00:01:50,000
search SPN's database of more than 20,000 classified links,

22
00:01:50,000 --> 00:01:54,000
by typing the keyword "racism" into the search field here.

23
00:01:54,000 --> 00:02:00,000
Let's do that, and when we hit Return, we'll get all manner of results.

24
00:02:00,000 --> 00:02:05,300
It might be organizations, researchers, events, publications, and so on.

25
00:02:05,300 --> 00:02:11,300
Now, let's suppose that we're particularly interested in, say, books on racism.

26
00:02:11,300 --> 00:02:15,200
We can drill down by clicking on the Publications tab.

27
00:02:15,200 --> 00:02:20,000
And then under that, we can see Books. And we'll click on those,

28
00:02:20,000 --> 00:02:24,000
and there we have a number of books on racism. And not only that,

29
00:02:24,000 --> 00:02:29,800
but we have links to book reviews. SPN also has an advanced search in which

30
00:02:29,800 --> 00:02:35,000
you can specify the type of item you're looking for-- say, a college course.

31
00:02:35,000 --> 00:02:39,400
And we can pop open that category to further specify whether we're

32
00:02:39,400 --> 00:02:44,000
looking for a distance-learning course or a regular course.

33
00:02:44,000 --> 00:02:48,000
And by the way, if you're interested in distance-learning resources,

34
00:02:48,000 --> 00:02:54,000
SPN has a page specifically devoted to that in its navigation menu on the left,

35
00:02:54,000 --> 00:02:59,000
and on that page, you'll find degree programs in psychology, MOOCs,

36
00:02:59,000 --> 00:03:04,000
and much more. For instance, if we click on MOOCs, we're taken to

37
00:03:04,000 --> 00:03:10,000
that part of the page, and we can scroll through a list of free online courses.

38
00:03:10,000 --> 00:03:14,000
You might have noticed that SPN also has partner sites.

39
00:03:14,000 --> 00:03:17,000
These are sites that the Social Psychology Network team

40
00:03:17,000 --> 00:03:20,400
designed and manages on our web server (with the exception

41
00:03:20,400 --> 00:03:26,000
of the SPSP website, which moved to a different service in 2012).

42
00:03:26,000 --> 00:03:29,200
During our course, we'll use several of these partner sites,

43
00:03:29,200 --> 00:03:33,000
beginning with the randomizer form on Research Randomizer

44
00:03:33,000 --> 00:03:39,000
in Assignment #1. Another point of interest: SPN maintains

45
00:03:39,000 --> 00:03:42,500
topical pages that go way beyond social psychology --

46
00:03:42,500 --> 00:03:47,300
everything from clinical psychology, to cultural psychology, to neuroscience,

47
00:03:47,300 --> 00:03:52,200
to peace psychology. In fact, let's click on peace psychology,

48
00:03:52,200 --> 00:03:56,000
and what you'll see is that these pages begin with a table of contents

49
00:03:56,000 --> 00:04:00,000
on specific topics. So in this case you see Mahatma Gandhi,

50
00:04:00,000 --> 00:04:06,000
Martin Luther King Jr., and graduate studies in peace psychology.

51
00:04:06,000 --> 00:04:10,000
If we zoom back to the top of the page, you'll see that graduate programs

52
00:04:10,000 --> 00:04:15,200
in social and personality psychology can also be searched geographically.

53
00:04:15,200 --> 00:04:19,000
For example, let's choose Full Screen Interactive mode.

54
00:04:19,000 --> 00:04:24,000
Click on, let's see, Asia, and maybe click on one of the programs in India,

55
00:04:24,000 --> 00:04:28,000
which pops open a bubble with a link that will in turn take us right to that

56
00:04:28,000 --> 00:04:34,000
graduate program's website. So, there it is! Or, if I close this window and

57
00:04:34,000 --> 00:04:39,000
type in a location like CA (short for California), we're taken to that

58
00:04:39,000 --> 00:04:44,000
geographic area, and we can click on specific programs in the side panel.

59
00:04:44,000 --> 00:04:48,000
So, let's see. I'll click on University of California at Berkeley --

60
00:04:48,000 --> 00:04:52,000
wonderful school -- there's the bubble, and if I click on the link,

61
00:04:52,000 --> 00:04:55,000
I'm again taken to the program.

62
00:04:55,000 --> 00:04:59,000
For anyone contemplating a career in psychology, the

63
00:04:59,000 --> 00:05:03,400
Online Psychology Career Center is another resource that might interest you.

64
00:05:03,400 --> 00:05:07,000
There's a direct link listed a few lines above the distance-learning page

65
00:05:07,000 --> 00:05:11,000
in the navigation menu on the left. And the Career Center includes

66
00:05:11,000 --> 00:05:15,000
everything from searchable job databases, to tips on graduate school,

67
00:05:15,000 --> 00:05:20,800
internships, and financial aid. Let me just mention a few other resources

68
00:05:20,800 --> 00:05:24,000
that were developed specifically with students in mind.

69
00:05:24,000 --> 00:05:27,000
I'll go fast because you can always rewind the video.

70
00:05:27,000 --> 00:05:31,700
SPN has a student discussion forum where you can post questions and

71
00:05:31,700 --> 00:05:36,000
requests for assistance. And likewise, there's a job forum that

72
00:05:36,000 --> 00:05:40,000
allows you to receive email alerts whenever a particular type of job opens

73
00:05:40,000 --> 00:05:44,000
up that might interest you. For example, you could request alerts if

74
00:05:44,000 --> 00:05:50,000
a postdoctoral position opens up in Australia -- one of my favorite countries.

75
00:05:50,000 --> 00:05:54,000
Another key element of the Network is that it contains a searchable directory

76
00:05:54,000 --> 00:06:00,000
of approximately 2,000 social psychology experts from 50 different countries.

77
00:06:00,000 --> 00:06:04,000
One way to search for experts is through a map, similar to the Graduate Program

78
00:06:04,000 --> 00:06:08,000
map that I showed earlier. Or, you can search the Directory through

79
00:06:08,000 --> 00:06:14,000
the Directories menu here. If you're interested in a particular topic --

80
00:06:14,000 --> 00:06:19,000
say, obedience -- you can search all SPN profiles by typing a keyword

81
00:06:19,000 --> 00:06:23,000
or phrase into this field. So we'll just type "obedience," and then

82
00:06:23,000 --> 00:06:27,000
once the results come in, I'll click on the top result so that you can see

83
00:06:27,000 --> 00:06:31,000
what a profile page looks like. There we go.

84
00:06:31,000 --> 00:06:36,000
As you can see, SPN profiles have tabs with an Overview statement,

85
00:06:36,000 --> 00:06:41,000
Publications, Contact information, and anything else the profile holder's

86
00:06:41,000 --> 00:06:46,700
decided to add, just as you might create a profile in Facebook or LinkedIn.

87
00:06:46,700 --> 00:06:51,000
For example, we can click on Publications by Professor Blass and flip through

88
00:06:51,000 --> 00:06:55,000
his books, some of which will actually be mentioned later in the course

89
00:06:55,000 --> 00:07:00,000
when we discuss research on obedience. We can also search the directory

90
00:07:00,000 --> 00:07:05,000
by research interests. So, let's look for someone with expertise

91
00:07:05,000 --> 00:07:13,000
on attitudes, prejudice, intergroup relations, neuroscience,

92
00:07:13,000 --> 00:07:16,000
and let's throw in the Internet for good measure.

93
00:07:16,000 --> 00:07:20,700
Because we combined so many different areas of expertise, there's currently

94
00:07:20,700 --> 00:07:27,000
only one person who covers them all: Mahzarin Banaji of Harvard University.

95
00:07:27,000 --> 00:07:31,200
And if we click on her profile, there are several things worth noting.

96
00:07:31,200 --> 00:07:36,000
First, there are links to social media, to her curriculum vitae,

97
00:07:36,000 --> 00:07:40,000
and an article about her in Wikipedia. You can see that there's also

98
00:07:40,000 --> 00:07:44,000
a link to her research laboratory, under the Research tab.

99
00:07:44,000 --> 00:07:47,000
And if we click on the Files tab, there are some nice photos

100
00:07:47,000 --> 00:07:53,000
in her Image Gallery. But the profile feature that I especially want to

101
00:07:53,000 --> 00:07:57,500
highlight is the SPN Mentor designation under the Teaching tab.

102
00:07:57,500 --> 00:08:02,000
This means that the expert is willing to offer free career advice and assistance

103
00:08:02,000 --> 00:08:07,500
to students from underrepresented groups as defined in this box.

104
00:08:07,500 --> 00:08:12,500
To receive that assistance, you just visit the SPN Mentorship Program page

105
00:08:12,500 --> 00:08:18,300
and follow the instructions. There are over 500 mentors to choose from

106
00:08:18,300 --> 00:08:21,000
worldwide, and we've set it up so that students can

107
00:08:21,000 --> 00:08:26,000
even GeoSearch the database. For example, if we go to the map of

108
00:08:26,000 --> 00:08:34,000
Profiled Experts, and we choose Europe, we get a map of SPN profile holders,

109
00:08:34,000 --> 00:08:40,000
which we can then limit to mentors with expertise in, for example, let's choose

110
00:08:40,000 --> 00:08:46,000
Personality Psychology, and there you go! With respect to the mentorship program,

111
00:08:46,000 --> 00:08:50,200
all I ask is that you please don't contact Professor Banaji,

112
00:08:50,200 --> 00:08:54,000
because I don't want to flood her with requests as a result of showing

113
00:08:54,000 --> 00:08:59,000
her profile in this video. Well, that's a lot of ground that we've covered,

114
00:08:59,000 --> 00:09:03,000
geographically and otherwise, so let's pause for a pop-up question.

115
00:09:06,000 --> 00:09:11,000
Hello again! In an earlier lecture video I mentioned inviting you to join

116
00:09:11,000 --> 00:09:15,000
Social Psychology Network, which I very much hope that you'll do.

117
00:09:15,000 --> 00:09:20,000
But let me say upfront, that unlike the course, joining the Network is not free.

118
00:09:20,000 --> 00:09:23,500
And I understand that some of you may not have the resources to join,

119
00:09:23,500 --> 00:09:28,400
or may not have the desire. That's okay. Everyone is welcome to take this course

120
00:09:28,400 --> 00:09:33,000
and use the resources in Social Psychology Network for free, regardless

121
00:09:33,000 --> 00:09:38,000
of whether you're a member or not. But for those of you who can afford to join,

122
00:09:38,000 --> 00:09:43,000
I want to strongly ask that you do, for two reasons. First, it helps us cover

123
00:09:43,000 --> 00:09:48,000
the cost of offering all the resources and services shown in this video.

124
00:09:48,000 --> 00:09:52,000
Neither Wesleyan nor I receive any funds from SPN memberships.

125
00:09:52,000 --> 00:09:56,700
100% of the money goes toward resources for students and other users

126
00:09:56,700 --> 00:10:01,000
of the Network. Second, Coursera students who join the Network

127
00:10:01,000 --> 00:10:05,000
will be given their own SPN Page -- very similar, actually,

128
00:10:05,000 --> 00:10:08,000
to the professional profiles I showed you earlier,

129
00:10:08,000 --> 00:10:11,300
but with an extra feature. Through a special arrangement,

130
00:10:11,300 --> 00:10:15,000
Coursera students with an SPN member page will have the option of

131
00:10:15,000 --> 00:10:20,000
displaying their Statement of Accomplishment directly on their Network page,

132
00:10:20,000 --> 00:10:24,200
which can then be used when applying for jobs or internships,

133
00:10:24,200 --> 00:10:29,000
or really any situation in which you want to display credentials and experience

134
00:10:29,000 --> 00:10:34,000
that might relate to psychology, education, business, health, and so on.

135
00:10:34,000 --> 00:10:36,300
How much does it cost to join the Network?

136
00:10:36,300 --> 00:10:42,000
A basic membership is $25 per calendar year, but for Coursera students

137
00:10:42,000 --> 00:10:47,000
we've dropped the membership rate to $10 per year or $20 for a two-year

138
00:10:47,000 --> 00:10:51,000
membership, which is actually lower than our regular student discount.

139
00:10:51,000 --> 00:10:55,300
In fact, it's the lowest membership rate we've ever offered.

140
00:10:56,000 --> 00:10:59,300
So,I hope that you join the Network. We would love, love, love to

141
00:10:59,300 --> 00:11:03,300
have you as a member if it doesn't present a financial burden.

142
00:11:03,300 --> 00:11:06,300
There's a "Join the Network" button right on our course page, and when

143
00:11:06,300 --> 00:11:10,000
you complete the Snapshot Quiz, you'll also be given an opportunity

144
00:11:10,000 --> 00:11:14,000
to join the Network, or just complete the Quiz. And either way,

145
00:11:14,000 --> 00:11:18,000
all the instructions are right there. Please complete the Snapshot Quiz

146
00:11:18,000 --> 00:11:22,000
before watching any further videos. And then once you're done,

147
00:11:22,000 --> 00:11:25,000
you're most welcome to watch the next video, which is on the

148
00:11:25,000 --> 00:11:30,000
psychological construction of reality. It should be a fun video!

