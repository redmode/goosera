





1
00:00:06,491 --> 00:00:08,701
I hope that you enjoyed the guest lecture 

2
00:00:08,701 --> 00:00:13,197
by Bob Cialdini and Steve Martin. 
In this video, we'll talk further about 

3
00:00:13,197 --> 00:00:17,640
Professor Cialdini's research, 
as well as other studies on the ins and outs 

4
00:00:17,640 --> 00:00:22,700
of social influence -- 
that is, efforts by an individual or a group 

5
00:00:22,700 --> 00:00:27,010
to have an influence on another 
individual or group, whether it might be 

6
00:00:27,010 --> 00:00:33,370
to donate money, or buy a product, support 
a political cause or a candidate, 

7
00:00:33,370 --> 00:00:38,400
comply with a request, and so forth. 
To take just one example, several years ago, 

8
00:00:38,400 --> 00:00:44,340
Petrified Forest National Park in 
Arizona tried to discourage theft 

9
00:00:44,340 --> 00:00:48,190
by posting a sign intended to have a social 
influence. 

10
00:00:48,190 --> 00:00:54,030
The sign said, "Your heritage is being 
vandalized every day by theft losses of 

11
00:00:54,030 --> 00:01:00,490
petrified wood of 14 tons a year, mostly 
a small piece at a time." 

12
00:01:01,500 --> 00:01:06,780
The park thought that by helping visitors 
understand how small thefts add up to a 

13
00:01:06,780 --> 00:01:12,740
large problem, stealing would be reduced, 
but the problem continued, and eventually 

14
00:01:12,740 --> 00:01:16,870
they turned to Professor Cialdini and 
asked if he could help. 

15
00:01:16,870 --> 00:01:21,610
Professor Cialdini's solution is described 
in a short article that I've assigned as 

16
00:01:21,610 --> 00:01:24,990
part of this week's reading, 
so I'm not going to give away the full story, 

17
00:01:24,990 --> 00:01:30,275
but one problem Professor Cialdini 
pointed out is that the park was 

18
00:01:30,275 --> 00:01:36,460
unintentionally signaling that theft is 
normative, or normal behavior. 

19
00:01:36,460 --> 00:01:42,990
If 14 tons of petrified wood are removed 
each year, a tiny piece at a time, 

20
00:01:42,990 --> 00:01:48,400
that means thousands of visitors are doing it, 
and in fact, when Professor Cialdini 

21
00:01:48,400 --> 00:01:56,900
had the park remove the anti-theft message, 
stealing actually fell by almost 80%. 

22
00:01:58,430 --> 00:02:02,300
So you have to be careful. 
Yes, it's important to convey the 

23
00:02:02,300 --> 00:02:06,500
seriousness of a problem, 
but if you announce, for example, 

24
00:02:06,500 --> 00:02:11,030
that in a particular country a woman 
is raped every few minutes, 

25
00:02:11,030 --> 00:02:16,420
or most people cheat or steal, 
you're normalizing the very behavior 

26
00:02:16,420 --> 00:02:19,200
that you want to change. 
And even though we know that consensus 

27
00:02:19,200 --> 00:02:24,410
information doesn't always have an effect 
on causal attributions, it often has a 

28
00:02:24,410 --> 00:02:29,600
large effect when it comes to persuasion 
and social influence. 

29
00:02:29,600 --> 00:02:34,620
Psychologists have studied a wide variety 
of social influence techniques. 

30
00:02:34,620 --> 00:02:38,840
For instance, research suggests that 
people are more likely to do something 

31
00:02:38,840 --> 00:02:43,300
when you get them to imagine doing it or 
to predict that they'll do it 

32
00:02:43,300 --> 00:02:47,390
in the future (say, voting in an upcoming 
election). 

33
00:02:47,390 --> 00:02:51,720
Other research has found that mentioning 
your name before making a request 

34
00:02:51,720 --> 00:02:57,750
can increase the chances that the other 
person will say yes by 50 to 100% -- 

35
00:02:57,750 --> 00:03:02,200
something as simple as "Hi, my name is 
Scott, and I'm wondering whether you might 

36
00:03:02,200 --> 00:03:07,600
do me a small favor." 
Engaging people in a dialogue also leads 

37
00:03:07,600 --> 00:03:12,220
to greater compliance with a request, 
presumably because dialogue resembles 

38
00:03:12,220 --> 00:03:17,358
friendship more than a monologue does. 
The idea is to talk with people 

39
00:03:17,358 --> 00:03:22,910
rather than at people. 
One very effective technique pioneered 

40
00:03:22,910 --> 00:03:28,244
by Professor Cialdini is the "Even a penny 
will help" technique. 

41
00:03:28,244 --> 00:03:31,620
The original study on this 
technique was very simple. 

42
00:03:31,620 --> 00:03:36,030
Students went door-to-door raising money 
for the American Cancer Society, 

43
00:03:36,030 --> 00:03:40,600
and they said one of two things. 
Half the time, they said: "I'm collecting 

44
00:03:40,600 --> 00:03:45,000
money for the American Cancer Society. 
Would you be willing to help 

45
00:03:45,000 --> 00:03:49,010
by giving a donation?" 
And the other half of the time, they said 

46
00:03:49,010 --> 00:03:54,270
exactly the same thing but added, "Even a 
penny will help." 

47
00:03:54,270 --> 00:03:59,100
The idea here was not only to make it 
affordable to comply but to make it hard 

48
00:03:59,100 --> 00:04:04,890
to say no to such a minimal request. 
And lo and behold, by tacking on this 

49
00:04:04,890 --> 00:04:09,920
extra sentence (just five words: 
"Even a penny will help"), compliance 

50
00:04:09,920 --> 00:04:16,430
jumped from 29% to 50% with no 
significant difference in the average 

51
00:04:16,430 --> 00:04:21,400
amount people gave -- a very simple 
technique to keep in mind 

52
00:04:21,400 --> 00:04:25,630
the next time that you're fundraising. 
Anyway, these are just a few findings 

53
00:04:25,630 --> 00:04:30,630
related to social influence, which, like 
persuasion, has received just a huge 

54
00:04:30,630 --> 00:04:34,980
amount of research attention. 
So, to avoid spreading ourselves too thin, 

55
00:04:34,980 --> 00:04:39,660
I'd like to focus on three of the 
most famous techniques: 

56
00:04:39,660 --> 00:04:45,850
first, the foot-in-the-door technique; 
second, the door-in-the-face technique; 

57
00:04:45,850 --> 00:04:51,390
and third, the low-ball technique, which 
car dealers are notorious for using. 

58
00:04:51,390 --> 00:04:55,570
Let's begin with the foot-in-the-door 
technique, which was briefly mentioned 

59
00:04:55,570 --> 00:05:00,040
in the guest lecture and was first studied 
experimentally by Jonathan Freedman 

60
00:05:00,040 --> 00:05:05,260
and Scott Fraser in the 1960s. 
The premise is that people are more 

61
00:05:05,260 --> 00:05:10,140
likely to comply with a large request 
once they've already complied with a 

62
00:05:10,140 --> 00:05:14,890
smaller one -- a foot in the door of the 
larger request. 

63
00:05:14,890 --> 00:05:17,580
Freedman and Fraser conducted two 
experiments. 

64
00:05:17,580 --> 00:05:23,100
The one I'd like to walk through -- and the 
one that's most famous -- is Experiment 2. 

65
00:05:23,100 --> 00:05:28,350
The participants were 112 innocent 
residents of Palo Alto, California, who 

66
00:05:28,350 --> 00:05:33,900
just happened to be home between 1:30 and 
4:30 one weekday afternoon when a member 

67
00:05:33,900 --> 00:05:37,095
of Freedman and Fraser's research team 
knocked on their door. 

68
00:05:37,095 --> 00:05:41,980
Every third or fourth home on certain 
blocks was randomly assigned to one of 

69
00:05:41,980 --> 00:05:46,750
five conditions: 
four in which a relatively small request 

70
00:05:46,750 --> 00:05:52,100
preceded a larger request, and one in 
which the larger request was made 

71
00:05:52,100 --> 00:05:57,340
without any prior contact. 
Four different small requests were used. 

72
00:05:57,340 --> 00:06:02,500
In one condition, the person at the door 
introduced herself or himself as a member 

73
00:06:02,500 --> 00:06:05,610
of the Community Committee for Traffic 
Safety 

74
00:06:05,610 --> 00:06:10,720
and asked people whether they'd post a 
small sign, three inches square, 

75
00:06:10,720 --> 00:06:15,400
in their window or car that simply said "Be a Safe 
Driver." 

76
00:06:15,400 --> 00:06:19,510
Professor Freedman was kind enough to 
loan me some materials from the original  

77
00:06:19,510 --> 00:06:24,210
study to share with our class, 
and as you can see, the sign was printed 

78
00:06:24,210 --> 00:06:28,100
in black block letters on a white 
background and generally looked 

79
00:06:28,100 --> 00:06:33,210
mildly unattractive. 
If people agreed to the request, 

80
00:06:33,210 --> 00:06:36,200
they were given a sign and thanked; 
otherwise, they were simply 

81
00:06:36,200 --> 00:06:40,600
thanked for their time. 
In another condition, residents were asked by a 

82
00:06:40,600 --> 00:06:44,670
member of the Keep California 
Beautiful Committee whether they would 

83
00:06:44,670 --> 00:06:50,860
post an equally unattractive sign that 
said "Keep California Beautiful." 

84
00:06:50,860 --> 00:06:55,165
In the third condition, residents were 
asked to sign a petition that supported 

85
00:06:55,165 --> 00:06:59,960
legislation on safe driving. 
And in a fourth condition, people were asked 

86
00:06:59,960 --> 00:07:05,460
to sign a petition in support of 
legislation to keep California beautiful. 

87
00:07:05,460 --> 00:07:10,500
So, these were the four small requests: two 
involving a three-inch sign, 

88
00:07:10,500 --> 00:07:16,090
and two involving a petition. 
Then, about two weeks later, a different member 

89
00:07:16,090 --> 00:07:22,400
of the research team revisited 
these households to make a much larger request. 

90
00:07:23,400 --> 00:07:26,300
This time, residents were approached by a member of 

91
00:07:26,300 --> 00:07:31,200
Citizens for Safe Driving to 
see whether they would post a very large sign 

92
00:07:31,200 --> 00:07:36,580
on safe driving in their front yard 
for a week or a week and a half. 

93
00:07:36,580 --> 00:07:40,840
This photo, from Professor Freedman, 
shows a sign smaller than the one 

94
00:07:40,840 --> 00:07:45,130
eventually used, which completely 
concealed the front door. 

95
00:07:45,130 --> 00:07:49,900
And once again, the sign was 
deliberately designed to look a bit ugly. 

96
00:07:49,900 --> 00:07:54,020
If people agreed to the request, they 
were told that more names than necessary 

97
00:07:54,020 --> 00:07:56,900
were being gathered, 
and that they'd be contacted in a few weeks if

98
00:07:56,900 --> 00:08:01,650
their home were to be used. 
That way, people didn't actually have to 

99
00:08:01,650 --> 00:08:04,930
have a sign installed in their front 
yard. 

100
00:08:04,930 --> 00:08:10,300
So, in two conditions, the small request 
concerned the same issue as the 

101
00:08:10,300 --> 00:08:15,300
large request: safe driving as opposed to 
keeping California beautiful. 

102
00:08:15,300 --> 00:08:19,100
And in two conditions, the small request 
was of the same kind as the 

103
00:08:19,100 --> 00:08:24,400
large request: posting a sign as opposed to 
signing a petition. 

104
00:08:25,440 --> 00:08:30,280
Freedman and Fraser wanted to see whether 
the small request had to resemble the 

105
00:08:30,280 --> 00:08:34,980
large request in order to be effective, 
and if so, whether it was more important 

106
00:08:34,980 --> 00:08:40,400
to match the large request by issue or by 
kind of request. 

107
00:08:40,400 --> 00:08:45,020
What do you think they found? 
Before I share the results, take a guess. 

108
00:08:45,020 --> 00:08:47,500
Many people are surprised by the answer.

109
00:08:51 --> 00:08:56,600
The correct answer is that compliance with 
a small request increased compliance with a 

110
00:08:56,600 --> 00:09:02,800
large request, regardless of whether the 
requests were of the same type or on the same topic. 

111
00:09:02,800 --> 00:09:07,430
When people were asked to post a large 
sign without first being asked to comply 

112
00:09:07,430 --> 00:09:14,820
with a small request, only 17% said yes, 
but if we average across all four of the 

113
00:09:14,820 --> 00:09:19,000
other conditions, including everyone who 
received a small request, whether or not 

114
00:09:19,000 --> 00:09:26,670
they said yes, compliance jumped to 56% -- 
over three times higher. 

115
00:09:26,670 --> 00:09:33,020
And what's especially interesting is 
that compliance was nearly as high, 47%, 

116
00:09:33,020 --> 00:09:38,200
even when the small request was of a 
different type and on a different issue. 

117
00:09:39,400 --> 00:09:43,470
This is fairly dramatic. 
What we have is more than double the rate of 

118
00:09:43,470 --> 00:09:47,900
compliance found in the large request 
only condition, even though the two requests 

119
00:09:47,900 --> 00:09:52,950
are of a different type (signing 
in a petition versus posting a sign) 

120
00:09:52,950 --> 00:09:58,320
on a different topic (keeping California 
beautiful versus safe driving) asked by 

121
00:09:58,320 --> 00:10:04,320
two different people, and separated in 
time by roughly two weeks. 

122
00:10:04,320 --> 00:10:09,080
How would you explain this? 
Freedman and Fraser concluded that once 

123
00:10:09,080 --> 00:10:15,130
people agree to a request they become, in 
their own eyes, "the kind of person who 

124
00:10:15,130 --> 00:10:19,910
does this sort of thing, who agrees 
to requests made by strangers, 

125
00:10:19,910 --> 00:10:25,050
who cooperates with good causes." In other 
words, their explanation was 

126
00:10:25,050 --> 00:10:29,310
very consistent with Daryl Bem's 
self-perception theory. 

127
00:10:29,310 --> 00:10:33,340
People watch themselves behave a 
certain way and conclude that they 

128
00:10:33,340 --> 00:10:36,700
hold values and attitudes that match their 
behavior. 

129
00:10:37,430 --> 00:10:41,950
Since the time of Freedman and Fraser's 
study, more than a hundred experiments 

130
00:10:41,950 --> 00:10:44,350
have examined the foot-in-the-door 
technique, 

131
00:10:44,350 --> 00:10:48,980
and the bottom line is that its 
effectiveness really depends on 

132
00:10:48,980 --> 00:10:53,100
the details of how it's used. 
For example, the technique is most 

133
00:10:53,100 --> 00:10:59,910
effective when you explicitly label the 
person as helpful or as a supporter, 

134
00:10:59,910 --> 00:11:04,400
saying something like "I really appreciate 
you supporting this effort," 

135
00:11:04,400 --> 00:11:08,200
which strengthens the person's self-perception 
as a supporter. 

136
00:11:08,890 --> 00:11:12,455
And the effect is strongest when the 
large request is presented as a 

137
00:11:12,455 --> 00:11:17,020
continuation of the smaller request, 
something that builds on the person's 

138
00:11:17,020 --> 00:11:22,500
prior commitment --  
"You did X last month; can you do a little bit more?" 

139
00:11:22,500 --> 00:11:26,400
But what happens when you ask people for 
something large 

140
00:11:26,400 --> 00:11:30,620
without a small request 
first, and they say no? 

141
00:11:30,620 --> 00:11:34,550
Does that make them more likely to 
continue saying no if you come back 

142
00:11:34,550 --> 00:11:39,700
with a smaller request? 
This question was posed in the Snapshot Quiz, 

143
00:11:39,700 --> 00:11:42,600
and you can see your answer here. 

144
00:11:45,400 --> 00:11:49,300
The correct answer, based on the best 
available research evidence, 

145
00:11:49,300 --> 00:11:55,800
is that when people reject a large request, 
they often become more likely to comply 

146
00:11:55,800 --> 00:12:02,800
with smaller requests down the road -- 
smaller, not equally large. That's key here. 

147
00:12:02,800 --> 00:12:07,820
When a large request is scaled back, it 
has the appearance of a concession, 

148
00:12:07,820 --> 00:12:12,050
and because there are strong social norms 
encouraging reciprocity, 

149
00:12:12,050 --> 00:12:17,100
people often feel obligated to make a 
concession of their own to reciprocate 

150
00:12:17,100 --> 00:12:20,530
and meet the other person somewhere in 
the middle. 

151
00:12:20,530 --> 00:12:25,700
In fact, this rejection-then-moderation 
procedure is so effective that 

152
00:12:25,700 --> 00:12:30,420
Professor Cialdini and his colleagues, who 
conducted pioneering research 

153
00:12:30,420 --> 00:12:36,100
on the topic, gave it a name: the door-in-the-face technique. 

154
00:12:36,100 --> 00:12:40,670
In one experiment, for example, a member 
of Professor Cialdini's research team 

155
00:12:40,670 --> 00:12:45,490
would walk up to students on campus 
and ask them if they'd like to work two hours 

156
00:12:45,490 --> 00:12:52,000
per week as an unpaid volunteer at 
a juvenile detention center 

157
00:12:52,000 --> 00:12:55,400
for at least two years! 
Can you imagine? 

158
00:12:55,400 --> 00:13:00,600
None of the students ever agreed to this, but 
then, after students had refused 

159
00:13:00,600 --> 00:13:05,510
the large request, 
the researcher made a smaller request, 

160
00:13:05,510 --> 00:13:09,400
like volunteering only once for about two 
hours. 

161
00:13:09,400 --> 00:13:14,700
What Professor Cialdini found is that 
compliance with a small request is much 

162
00:13:14,700 --> 00:13:20,740
higher if you can first get someone to 
slam a door in your face with a larger 

163
00:13:20,740 --> 00:13:24,800
request. In Professor Cialdini's research, 
this technique  

164
00:13:24,800 --> 00:13:31,266
doubled the rate of compliance, and a later 
meta-analysis of 87 different paired 

165
00:13:31,266 --> 00:13:37,200
requests found that the door-in-the-face 
technique is especially effective when 

166
00:13:37,200 --> 00:13:42,550
the same person makes both requests, 
the two requests are made 

167
00:13:42,550 --> 00:13:48,270
face-to-face with no delay between them, 
and the requests are prosocial 

168
00:13:48,270 --> 00:13:52,220
with the same beneficiary -- 
that is, the requests are for the good of 

169
00:13:52,220 --> 00:13:56,900
others, and the ones who benefit are the 
same in both requests. 

170
00:13:56,900 --> 00:14:03,500
Under these conditions, norms of 
reciprocity and generosity are quite strong, 

171
00:14:03,500 --> 00:14:08,000
and as long as the initial request 
isn't so large as to seem unreasonable or 

172
00:14:08,000 --> 00:14:15,170
greedy, people find it hard to say no. 
One last social influence technique that 

173
00:14:15,170 --> 00:14:20,200
I'd like to discuss is known as the low-ball 
technique, and once again, 

174
00:14:20,200 --> 00:14:24,200
Professor Cialdini and his colleagues 
conducted ground-breaking studies 

175
00:14:24,200 --> 00:14:28,200
on the topic. 
Let me describe just one typical experiment. 

176
00:14:28,200 --> 00:14:31,200
I think you'll find it very useful to know about. 

177
00:14:31,750 --> 00:14:35,400
The participants were 63 college students 
enrolled in 

178
00:14:35,400 --> 00:14:39,440
a psychology course, 
and they were randomly assigned to either 

179
00:14:39,440 --> 00:14:43,290
a control condition or an experimental 
condition. 

180
00:14:43,290 --> 00:14:47,310
In both conditions, a research 
assistant telephoned students 

181
00:14:47,310 --> 00:14:52,810
and introduced herself as follows: 
"I'm calling for the Psychology Department 

182
00:14:52,810 --> 00:14:59,050
to schedule Psychology 100 students for 
an experiment on thinking processes. 

183
00:14:59,050 --> 00:15:03,510
The experiment concerns the way people 
organize facts. 

184
00:15:03,510 --> 00:15:07,940
We can give you one hour of credit for 
your participation in this experiment." 

185
00:15:09,060 --> 00:15:12,490
If students were in the control condition, 
the assistant told them that the 

186
00:15:12,490 --> 00:15:18,230
experiment was being run at 7:00 am 
Wednesday and Friday morning 

187
00:15:18,230 --> 00:15:21,080
and asked whether they might be able to 
make it then. 

188
00:15:21,080 --> 00:15:27,650
Not surprisingly, only 9 of 29 students, 
or 31%, were willing to make an 

189
00:15:27,650 --> 00:15:33,810
appointment that early in the morning, 
and only 24% showed up. 

190
00:15:33,810 --> 00:15:39,100
In the experimental condition (the low-ball 
condition), students were informed of 

191
00:15:39,100 --> 00:15:45,590
the 7:00 am time only after they had already 
agreed to participate in the experiment; 

192
00:15:45,590 --> 00:15:49,640
this was the low-ball, thrown at the last 
moment. 

193
00:15:49,640 --> 00:15:55,140
When all was said and done, 56% of these 
students made an appointment, 

194
00:15:55,140 --> 00:16:01,258
and 53% actually showed up for the 7:00 am 
experiment. 

195
00:16:01,258 --> 00:16:05,500
So, although there are obviously limits, 
it seems that once people commit 

196
00:16:05,500 --> 00:16:10,650
themselves to honoring a request, 
the request can often be increased 

197
00:16:10,650 --> 00:16:13,270
without them withdrawing from the 
commitment, 

198
00:16:13,270 --> 00:16:19,010
just as car dealers often throw a low-ball 
by adding conveyance fees and other 

199
00:16:19,010 --> 00:16:24,149
charges after people have agreed to buy a 
car for a particular price. 

200
00:16:25,180 --> 00:16:29,510
Let's pause for a pop-up question on 
social influence techniques, 

201
00:16:29,510 --> 00:16:32,150
and then I'll make a few concluding remarks. 
 

202
00:16:35,050 --> 00:16:39,610
Now, what's the value of knowing about 
these techniques? Well, it's not so that you can 

203
00:16:39,610 --> 00:16:45,200
go out and low-ball other people, 
I hope, but rather, that you can protect yourself 

204
00:16:45,200 --> 00:16:50,610
when other people try to low-ball you. 
The social influence techniques discussed 

205
00:16:50,610 --> 00:16:55,850
in this video are very powerful tools, 
but once you realize that a low-ball has 

206
00:16:55,850 --> 00:16:59,900
been thrown, 
or that someone stuck a foot in your door, 

207
00:16:59,900 --> 00:17:02,940
or gotten you to slam a door in their face, 

208
00:17:02,940 --> 00:17:06,000
you'll be in a better position to step back and

209
00:17:06,000 --> 00:17:10,050
decide whether or not you want 
to comply with a request. 

210
00:17:10,050 --> 00:17:12,900
Maybe you'll say yes, or maybe you'll say no,

211
00:17:12,900 --> 00:17:17,010
but at the very least you'll understand 
the psychological dynamics 

212
00:17:17,010 --> 00:17:22,000
of the situation and be less likely to be 
manipulated. 

213
00:17:22,920 --> 00:17:26,760
Well, with this video you've completed two 
weeks of the course. 

214
00:17:26,760 --> 00:17:31,570
Bravo! I hope that you've enjoyed it as much as I have. 

215
00:17:31,570 --> 00:17:35,680
Before we end, let me just offer a friendly 
reminder to complete this week's 

216
00:17:35,680 --> 00:17:40,600
assignment, entitled "What Social 
Impression Do You Make?" 

217
00:17:40,600 --> 00:17:44,300
For the assignment, you can either join 
Social Psychology Network 

218
00:17:44,300 --> 00:17:48,400
and create your own page if you haven't 
already, or you can 

219
00:17:48,400 --> 00:17:52,920
complete the assignment by submitting 
a CV or resume. 

220
00:17:52,920 --> 00:17:57,530
Personally, I would love to have you as a 
member of the Network, so I very much 

221
00:17:57,530 --> 00:18:02,240
hope that you will join If it doesn't 
present a financial hardship, 

222
00:18:02,240 --> 00:18:05,540
but either way will work just fine for 
the assignment. 

223
00:18:05,540 --> 00:18:09,950
What's most important is that you 
actively engage the course material. 

224
00:18:09,950 --> 00:18:14,819
If you can spare the time, don't just 
watch the videos -- do the readings and 

225
00:18:14,819 --> 00:18:18,910
complete the assignments. 
We're heading into very interesting 

226
00:18:18,910 --> 00:18:23,400
waters next week, so if you can complete 
the readings and the assignments 

227
00:18:23,400 --> 00:18:26,000
for each week, you'll get 
more out of the course, 

228
00:18:26,000 --> 00:18:30,892
because it will set up the next week. 
Until then! 

