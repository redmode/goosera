


1
00:00:07,560 --> 00:00:11,650
In the last video, we discussed the link 
between attitudes and behavior, but we 

2
00:00:11,650 --> 00:00:17,420
didn't discuss the issue of causation. 
Specifically, when attitudes and behavior 

3
00:00:17,420 --> 00:00:21,370
are related to each other, is it more 
often the case that attitudes shape 

4
00:00:21,370 --> 00:00:24,638
behavior, 
or that behavior shapes attitudes? 

5
00:00:24,638 --> 00:00:30,480
Until Leon Festinger developed the theory 
of cognitive dissonance, most people 

6
00:00:30,480 --> 00:00:34,260
assumed that attitudes shaped behavior 
much more than the other way around. 

7
00:00:35,590 --> 00:00:40,070
But with the arrival of dissonance 
theory, psychologists began to question 

8
00:00:40,070 --> 00:00:42,970
that assumption. 
Now, instead of me giving you a dry 

9
00:00:42,970 --> 00:00:48,660
definition of exactly what the theory is, 
let me first illustrate how it works with 

10
00:00:48,660 --> 00:00:54,240
a story that captures its essence. 
This story comes from a book on Jewish 

11
00:00:54,240 --> 00:00:56,910
folklore, 
and it's a very well known story in 

12
00:00:56,910 --> 00:01:01,499
social psychology. 
Once upon a time, there was a Jewish 

13
00:01:01,499 --> 00:01:05,725
tailor who set up his business on the 
main street of a town that had an 

14
00:01:05,725 --> 00:01:09,300
anti-Semitic gang -- a bunch of 
kids who would visit the shop 

15
00:01:09,300 --> 00:01:12,660
each day shouting "Jew! 
Jew!" in an effort 

16
00:01:12,660 --> 00:01:15,890
to basically bother him and 
drive him out of town. 

17
00:01:15,890 --> 00:01:20,720
Well, the tailor was bothered, 
but after several sleepless nights 

18
00:01:20,720 --> 00:01:25,190
he finally devised a plan. 
[SOUND] The next time that the kids came 

19
00:01:25,190 --> 00:01:29,830
to threaten him, the tailor announced 
that anyone who called him a Jew would 

20
00:01:29,830 --> 00:01:34,210
get a dime. 
He then handed out dimes to each member 

21
00:01:34,210 --> 00:01:37,150
of the gang. 
Pleased with their new incentive, the 

22
00:01:37,150 --> 00:01:40,900
kids come back the next day, they're 
shouting "Jew! Jew!" 

23
00:01:40,900 --> 00:01:46,590
and the tailor, smiling, gave each one a 
nickel, explaining that he could only 

24
00:01:46,590 --> 00:01:51,030
afford five cents that day. 
Well, the gang left satisfied because 

25
00:01:51,030 --> 00:01:56,070
after all, a nickel's a nickel. 
Then in the following day, the tailor 

26
00:01:56,070 --> 00:02:00,350
gave out only pennies to each gang 
member, again, explaining that he 

27
00:02:00,350 --> 00:02:03,920
couldn't afford more than that. 
Well, a penny wasn't much of an 

28
00:02:03,920 --> 00:02:09,290
incentive, and the kids began to protest. 
When the tailor replied that they could 

29
00:02:09,290 --> 00:02:13,980
take it or leave it, they decided to 
leave it, refusing to call him a Jew  

30
00:02:13,980 --> 00:02:18,710
for only one penny. 
Problem solved! 

31
00:02:18,710 --> 00:02:23,788
Now why would members of the gang harass 
the tailor for free, but not for a penny? 

32
00:02:23,788 --> 00:02:28,470
According to Festinger's theory of 
cognitive dissonance, people are 

33
00:02:28,470 --> 00:02:33,990
generally motivated to reduce or avoid 
psychological inconsistencies, 

34
00:02:33,990 --> 00:02:37,850
so when the tailor announced that he was 
happy to be called a Jew, and when he 

35
00:02:37,850 --> 00:02:43,410
changed the gang's motivation 
from anti-Semitism to monetary reward, 

36
00:02:43,410 --> 00:02:48,170
he made it inconsistent, or 
"dissonance-arousing" for the gang to 

37
00:02:48,170 --> 00:02:50,670
please him without financial 
compensation. 

38
00:02:50,670 --> 00:02:56,150
Without a sufficiently large payment, the 
kids could no longer justify behaving at 

39
00:02:56,150 --> 00:03:00,700
variance with their attitudes, which 
were, of course, to upset the tailor, 

40
00:03:00,700 --> 00:03:06,615
not to make him happy. 
Well, in 1959 the same principle was 

41
00:03:06,615 --> 00:03:11,770
demonstrated in what may well be the best 
known cognitive dissonance experiment 

42
00:03:11,770 --> 00:03:16,080
ever conducted -- 
a senior thesis, as it turns out, carried 

43
00:03:16,080 --> 00:03:20,520
out by Leon Festinger's student, Merrill 
Carlsmith. 

44
00:03:20,520 --> 00:03:24,660
A personal aside: that talented 
undergraduate eventually became the 

45
00:03:24,660 --> 00:03:28,910
professor who taught me statistics when I 
was a graduate student over 20 years 

46
00:03:28,910 --> 00:03:31,325
later. 
He was an absolutely brilliant psychologist, 

47
00:03:31,325 --> 00:03:37,120
and this photo comes to us 
courtesy his son, Chris, who is now 

48
00:03:37,120 --> 00:03:42,480
himself a professor. 
Anyway, in the study 60 undergraduate 

49
00:03:42,480 --> 00:03:46,680
students at Stanford were randomly 
assigned to one of three experimental 

50
00:03:46,680 --> 00:03:51,180
conditions. 
In the $20 condition, participants were 

51
00:03:51,180 --> 00:03:56,900
required to perform boring laboratory 
tasks for an hour, after which they were 

52
00:03:56,900 --> 00:04:00,640
paid $20 
to tell a waiting student that the tasks 

53
00:04:00,640 --> 00:04:04,720
were interesting and enjoyable. 
The experimenter explained that a paid 

54
00:04:04,720 --> 00:04:08,700
assistant normally performed that duty as 
part of the experiment, but that the 

55
00:04:08,700 --> 00:04:13,945
assistant couldn't do it that day. 
In the $1 condition, students were paid 

56
00:04:13,945 --> 00:04:19,260
$1 to do the very same thing. 
And there was also a control condition in 

57
00:04:19,260 --> 00:04:23,220
which students simply engaged in the 
boring tasks. 

58
00:04:23,220 --> 00:04:27,250
What were the tasks? 
Well first, students spent half an hour 

59
00:04:27,250 --> 00:04:32,530
[SOUND] using one hand to put 12 spools 
onto a tray. 

60
00:04:32,530 --> 00:04:38,750
Unload the tray, load the tray again, 
unload the tray, refill the tray one more 

61
00:04:38,750 --> 00:04:42,110
time, and so on, with 
the experimenter simply sitting 

62
00:04:42,110 --> 00:04:45,515
there with a stop watch carefully taking 
notes. 

63
00:04:45,515 --> 00:04:50,376
Then, just when the students were about 
to lose their minds, they spent another 

64
00:04:50,376 --> 00:04:55,890
30 minutes using one hand to turn 48 pegs 
on a peg board 

65
00:04:55,890 --> 00:05:01,380
one-quarter turn at a time -- in other 
words, Boring with a capital B. 

66
00:05:02,610 --> 00:05:06,848
Once the poor student had finished his 
tasks, here's what the experimenter said: 

67
00:05:06,848 --> 00:05:11,950
 >> Okay, that's fine. 
Let me tell you now what we're actually 

68
00:05:11,950 --> 00:05:14,890
studying here. 
It's the effect of preparatory mental set 

69
00:05:14,890 --> 00:05:18,030
on performance. 
The rest of the subjects are prepared by 

70
00:05:18,030 --> 00:05:21,570
being told that the experiment will be 
very interesting and enjoyable; 

71
00:05:21,570 --> 00:05:26,300
in fact, it's lots of fun. 
Now I have a somewhat unusual request to 

72
00:05:26,300 --> 00:05:28,770
make of you. 
The next subject is waiting right 

73
00:05:28,770 --> 00:05:32,575
outside, but the fellow who ordinarily 
gives the spiel isn't here. 

74
00:05:32,575 --> 00:05:35,830
I wonder if you could possibly take his 
place. 

75
00:05:35,830 --> 00:05:39,020
As a matter of fact, we figure we'll be 
needing someone in the future, 

76
00:05:39,020 --> 00:05:43,835
so I'd like to offer you a $20 retainer 
and have you remain on call for us. 

77
00:05:43,835 --> 00:05:46,380
Would that be alright? 
 >> $20? 

78
00:05:46,380 --> 00:05:49,600
That'd be fine. 
 >> After the student agreed to be 

79
00:05:49,600 --> 00:05:54,628
hired -- and by the way all of the students 
did -- the experimenter led the student to 

80
00:05:54,628 --> 00:05:59,460
a room where another person appeared to 
be waiting for the experiment to begin. 

81
00:05:59,460 --> 00:06:04,730
And that person, who was in fact a member 
of the research team, a confederate, said 

82
00:06:04,730 --> 00:06:08,530
that a friend who had earlier 
participated in the experiment described 

83
00:06:08,530 --> 00:06:13,210
it as really miserable, as boring and 
tedious, and so on. 

84
00:06:13,210 --> 00:06:17,450
Let's listen in. 
 >> He said it was pretty miserable 

85
00:06:17,450 --> 00:06:21,625
and that I should do everything I could 
to get out of it. 

86
00:06:21,625 --> 00:06:23,280
 >> Well, I think maybe your friend was 
wrong. 

87
00:06:23,280 --> 00:06:26,330
Perhaps there's a different experiment, 
because this was a lot of fun. 

88
00:06:26,330 --> 00:06:29,430
It appeared to me as if it were, as if 
it were a puzzle. 

89
00:06:29,430 --> 00:06:32,330
We had to turn these knobs, and 
I tried to figure out what we were doing 

90
00:06:32,330 --> 00:06:35,059
it for, but I really couldn't figure it 
out. 

91
00:06:35,059 --> 00:06:38,063
Perhaps you'll have better luck. 
 >> What were the results? 

92
00:06:38,063 --> 00:06:43,980
Participants in the $20 condition and in 
the control condition later evaluated the 

93
00:06:43,980 --> 00:06:49,670
experimental task as slightly negative, but participants in the $1 condition 

94
00:06:49,670 --> 00:06:51,948
actually thought the tasks were fairly 
positive. 

95
00:06:51,948 --> 00:06:57,500
Festinger and Carlsmith argued that 
students who were paid only a dollar to 

96
00:06:57,500 --> 00:07:01,400
lie to another person had experienced 
cognitive dissonance. 

97
00:07:01,400 --> 00:07:05,765
According to Festinger, people experience 
cognitive dissonance when they 

98
00:07:05,765 --> 00:07:11,140
simultaneously hold two thoughts that are 
psychologically incompatible, that are 

99
00:07:11,140 --> 00:07:13,170
inconsistent 
in some way. 

100
00:07:13,170 --> 00:07:18,880
In this particular instance, the 
dissonant cognitions were the following: 

101
00:07:18,880 --> 00:07:25,660
First, the tasks were extremely boring. And second, for only a dollar,  

102
00:07:25,660 --> 00:07:30,790
I (an honest person), just told someone the 
tasks were interesting and enjoyable. 

103
00:07:30,790 --> 00:07:35,620
When taken together these statements 
imply that people in the $1 condition had 

104
00:07:35,620 --> 00:07:40,150
lied for no good reason. 
People in the $20 condition, on the other hand, 

105
00:07:40,150 --> 00:07:44,330
had agreed to be hired for what 
they apparently considered to be a very 

106
00:07:44,330 --> 00:07:49,500
good reason: $20 (which 
was, in those days, a small fortune -- 

107
00:07:49,500 --> 00:07:54,880
something like $150 today). 
Festinger proposed that people try 

108
00:07:54,880 --> 00:07:58,790
whenever possible to reduce cognitive 
dissonance. 

109
00:07:58,790 --> 00:08:04,610
He regarded dissonance as a negative 
drive state, as an unpleasant feeling or 

110
00:08:04,610 --> 00:08:07,935
condition, 
and he hypothesized that people in that 

111
00:08:07,935 --> 00:08:14,040
$1 condition would be motivated to reduce 
their feelings of dissonance. 

112
00:08:14,040 --> 00:08:17,580
Now, of course, there wasn't much the 
students could do about that second 

113
00:08:17,580 --> 00:08:20,670
thought. 
The fact is, they did tell another person 

114
00:08:20,670 --> 00:08:24,540
that the task was enjoyable, and they did it for only a dollar. 

115
00:08:24,540 --> 00:08:28,380
And they probably weren't going to change 
their view of themselves as honest 

116
00:08:28,380 --> 00:08:32,480
decent people. 
On the other hand, the dullness of the 

117
00:08:32,480 --> 00:08:37,670
task afforded students some wiggle room. 
Dullness, you might say, is in the eye of 

118
00:08:37,670 --> 00:08:41,620
the beholder. So, according to Festinger and Carlsmith, 

119
00:08:41,620 --> 00:08:47,510
people in the $1 condition later came to 
believe that the task was kind of fun 

120
00:08:47,510 --> 00:08:52,890
in order to reduce the dissonance caused 
by lying to another person for only a 

121
00:08:52,890 --> 00:08:57,450
dollar. 
In contrast, people in the $20 condition 

122
00:08:57,450 --> 00:09:00,365
saw the experimental tasks for exactly 
what they were: 

123
00:09:00,365 --> 00:09:04,810
crushingly dull. 
People in that experimental condition had 

124
00:09:04,810 --> 00:09:08,940
no need to reduce dissonance, because 
they already had a good explanation for 

125
00:09:08,940 --> 00:09:14,485
their behavior: they were paid $20. 
Here's some rare footage of Leon 

126
00:09:14,485 --> 00:09:18,160
Festinger himself explaining cognitive 
dissonance theory. 

127
00:09:20,440 --> 00:09:25,360
 >> Other theories might predict that 
the man who is paid most would have the 

128
00:09:25,360 --> 00:09:30,870
highest motivation for enthusing over the 
dull task and would be most sold on it 

129
00:09:30,870 --> 00:09:34,708
himself. 
Cognitive dissonance theory leads to an 

130
00:09:34,708 --> 00:09:41,368
exactly opposite prediction. 
The man who is paid $20 knows that the 

131
00:09:41,368 --> 00:09:47,160
task is dull, but he also knows that he 
had sufficient justification for saying 

132
00:09:47,160 --> 00:09:49,800
that it wasn't. 
 >> Did you enjoy working on the manual 

133
00:09:49,800 --> 00:09:52,300
test. 
 >> Well, it really wasn't too enjoyable. 

134
00:09:52,300 --> 00:09:59,760
In fact, it was rather boring. 
 >> How about the man who is paid $1? 

135
00:09:59,760 --> 00:10:04,770
He knows the task is dull, but he has two 
discrepant thoughts. 

136
00:10:04,770 --> 00:10:09,200
He also knows that he did not have 
sufficient justification for saying that 

137
00:10:09,200 --> 00:10:12,900
it wasn't. 
For him, there is dissonance. 

138
00:10:14,250 --> 00:10:17,165
Time after time, we have seen what 
follows. 

139
00:10:17,165 --> 00:10:22,675
He reduces the dissonance by changing his 
opinion about the dullness of the task. 

140
00:10:25,250 --> 00:10:29,942
 >> The bottom line is that cognitive 
dissonance theory has two main prongs. 

141
00:10:29,942 --> 00:10:35,060
First, the act of holding two 
incompatible thoughts creates a sense of 

142
00:10:35,060 --> 00:10:40,600
internal discomfort, or "dissonance." 
And second, people try to reduce or 

143
00:10:40,600 --> 00:10:43,900
avoid these feelings of tension whenever 
possible. 

144
00:10:43,900 --> 00:10:47,280
But it turns out that the story doesn't 
end here, because 

145
00:10:47,280 --> 00:10:52,100
there's a whole other way to 
account for what Festinger and Carlsmith found. 

146
00:10:52,100 --> 00:10:55,580
In the mid-1960's, social psychologist 

147
00:10:55,580 --> 00:10:59,620
Daryl Bem proposed that cognitive 
dissonance findings could be explained by 

148
00:10:59,620 --> 00:11:04,621
what he called "self-perception theory." 
According to self-perception theory, 

149
00:11:04,621 --> 00:11:08,975
dissonance findings have nothing to do 
with a negative drive state called 

150
00:11:08,975 --> 00:11:12,150
dissonance. 
They have only to do with how people 

151
00:11:12,150 --> 00:11:14,995
infer their beliefs from their own 
behavior. 

152
00:11:14,995 --> 00:11:20,394
Like dissonance theory, Bem's 
self-perception theory has two main prongs: 

153
00:11:20,394 --> 00:11:26,590
First, individuals come to know their own 
attitudes and emotions and other internal 

154
00:11:26,590 --> 00:11:31,490
states partially by inferring them from 
observations of their own behavior and 

155
00:11:31,490 --> 00:11:34,930
the circumstances in which their behavior 
occurs. 

156
00:11:34,930 --> 00:11:39,367
And second, to the extent that internal 
cues are weak, 

157
00:11:39,367 --> 00:11:44,120
ambiguous, or uninterpretable, the 
individual is functionally in the same 

158
00:11:44,120 --> 00:11:48,860
position as an outside observer. 
It just so happens that the person you're 

159
00:11:48,860 --> 00:11:53,839
observing is yourself. 
The way Bem would explained Festinger and 

160
00:11:53,839 --> 00:11:59,230
Carlsmith's results is to argue that 
students who saw themselves tell another 

161
00:11:59,230 --> 00:12:03,870
person that the task was enjoyable for 
only a dollar inferred that they must 

162
00:12:03,870 --> 00:12:08,480
have enjoyed the task, 
just as an outside observer would infer. 

163
00:12:08,480 --> 00:12:12,080
On the other hand, students who saw 
themselves tell another person that the 

164
00:12:12,080 --> 00:12:16,840
task was enjoyable 
for $20 explained their behavior to 

165
00:12:16,840 --> 00:12:22,670
themselves as a result of being paid a 
large sum of money -- again, as an outside 

166
00:12:22,670 --> 00:12:26,360
observer would. 
The difference between self-perception 

167
00:12:26,360 --> 00:12:31,240
theory and cognitive dissonance theory is 
that self-perception theory explains 

168
00:12:31,240 --> 00:12:35,950
classic dissonance findings in terms of 
how people infer 

169
00:12:35,950 --> 00:12:41,960
the causes of their behavior. 
It is essentially attributional in nature, 

170
00:12:41,960 --> 00:12:46,640
whereas cognitive dissonance theory 
explains findings in terms of a natural 

171
00:12:46,640 --> 00:12:51,212
tendency to reduce inner conflict, 
tension, dissonance. 

172
00:12:51,212 --> 00:12:56,240
According to Bem, participants in the 
Festinger and Carlsmith study could have 

173
00:12:56,240 --> 00:13:01,320
been cool as cucumbers and still given 
the same pattern of results. 

174
00:13:01,320 --> 00:13:06,530
A feeling of dissonance, according to Bem, 
isn't necessary. 

175
00:13:06,530 --> 00:13:08,810
Which of these two theories is more 
accurate 

176
00:13:08,810 --> 00:13:12,550
or more useful when it comes to 
explaining dissonance "phenomena"? 

177
00:13:12,550 --> 00:13:17,700
Well, for many years, researchers on each 
side of the issue attempted to design 

178
00:13:17,700 --> 00:13:21,140
a definitive experiment in support of their 
theory,  

179
00:13:21,140 --> 00:13:24,900
in effect dealing a friendly death blow 
to the other side. 

180
00:13:24,900 --> 00:13:29,400
But in fact, each round of 
experimentation served only to provoke 

181
00:13:29,400 --> 00:13:34,750
more experiments by the other side and, 
after several years of this, Bem himself 

182
00:13:34,750 --> 00:13:38,650
withdrew from the debate. 
In the final analysis, I think it makes 

183
00:13:38,650 --> 00:13:42,804
sense to say that both theories operate 
in a wide variety of situations. 

184
00:13:42,804 --> 00:13:47,490
Clearly, there are many situations in which 
we do feel tension, and there are also 

185
00:13:47,490 --> 00:13:53,260
situations in which we closely watch our 
behavior and draw inferences from it. 

186
00:13:53,260 --> 00:13:57,790
Following psychology tradition, however, 
I'm going to use dissonance terminology 

187
00:13:57,790 --> 00:14:01,926
when discussing research findings that 
could be explained equally well 

188
00:14:01,926 --> 00:14:06,600
by self-perception theory. 
But let's pause now just briefly for a 

189
00:14:06,600 --> 00:14:10,990
quick comprehension check, and then I'll 
make a few closing remarks. 

190
00:14:14,640 --> 00:14:18,199
So we've now covered the basics of 
cognitive dissonance theory and 

191
00:14:18,199 --> 00:14:22,610
self-perception theory, which sets you up 
for the most interesting part: 

192
00:14:22,610 --> 00:14:27,775
classic research findings that are filled 
with surprises, useful applications, and 

193
00:14:27,775 --> 00:14:34,595
thought provoking consequences. 
That will be our topic for next time. 

