





1
00:00:06,970 --> 00:00:11,390
As we've seen in earlier videos, our 
experience of reality isn't simply a 

2
00:00:11,390 --> 00:00:15,540
function of what's out there. 
Our perceptions are also influenced by 

3
00:00:15,540 --> 00:00:21,120
context, by previous experience, and by 
our tendency to seek out evidence that 

4
00:00:21,120 --> 00:00:25,970
confirms our expectations. 
But social perceptions and social 

5
00:00:25,970 --> 00:00:30,610
expectations affect more than the person 
who holds them. They can also affect the 

6
00:00:30,610 --> 00:00:34,560
person about whom the expectations are 
held. 

7
00:00:34,560 --> 00:00:39,505
In fact, in some cases, our predictions 
and our expectations can lead to 

8
00:00:39,505 --> 00:00:44,120
self-fulfilling prophecies. 
The term "self-fulfilling prophecy" was 

9
00:00:44,120 --> 00:00:50,970
coined in a classic 1948 journal article 
by a sociologist named Robert Merton. 

10
00:00:50,970 --> 00:00:55,240
Here's what Merton wrote: 
"The self-fulfilling prophecy is, in the 

11
00:00:55,240 --> 00:01:00,720
beginning, a false definition of the 
situation, evoking a new behavior, which 

12
00:01:00,720 --> 00:01:06,460
makes the originally false conception 
come true [thereby perpetuating] a reign 

13
00:01:06,460 --> 00:01:09,520
of error. For the prophet will cite the actual 

14
00:01:09,520 --> 00:01:13,570
course of events, as proof [of being] right 
from the beginning. 

15
00:01:13,570 --> 00:01:16,510
Such are the perversities of social 
logic." 

16
00:01:18,110 --> 00:01:21,885
What Merton is saying is that a 
self-fulfilling prophecy is a 

17
00:01:21,885 --> 00:01:24,780
misconception, 
but it's a misconception that later 

18
00:01:24,780 --> 00:01:28,210
becomes true. 
In social psychology there have been 

19
00:01:28,210 --> 00:01:31,990
hundreds of investigations of 
self-fulfilling prophecies, 

20
00:01:31,990 --> 00:01:37,890
but by far the most famous is a book-length 
study published in 1968 by 

21
00:01:37,890 --> 00:01:43,450
psychologist Robert Rosenthal and school 
principal Lenore Jacobson. 

22
00:01:43,450 --> 00:01:46,639
I wasn't able to get a photo of Lenore 
Jacobson, 

23
00:01:46,639 --> 00:01:50,560
but here are photos of Professor 
Rosenthal and the school where the study 

24
00:01:50,560 --> 00:01:54,730
was conducted. 
In the experiment, grade school teachers 

25
00:01:54,730 --> 00:01:59,700
were given diagnostic information 
indicating that roughly 1 out of every 5 

26
00:01:59,700 --> 00:02:03,365
students would "bloom" academically in the 
coming year. 

27
00:02:03,365 --> 00:02:09,020
And as measured by intelligence tests 
eight months later, these students did 

28
00:02:09,020 --> 00:02:13,790
improve more than other students. 
What makes the study by Rosenthal and 

29
00:02:13,790 --> 00:02:19,090
Jacobson interesting is that these 
academic bloomers were selected at 

30
00:02:19,090 --> 00:02:22,390
random. 
The diagnostic information teachers were 

31
00:02:22,390 --> 00:02:27,270
given had been assigned using a table of 
random numbers. 

32
00:02:27,270 --> 00:02:31,190
Apparently, when teachers were told that 
certain randomly selected students would 

33
00:02:31,190 --> 00:02:36,960
do well, they gave these students more 
attention and more praise than they gave 

34
00:02:36,960 --> 00:02:41,100
the other students, and as a result, these students actually 

35
00:02:41,100 --> 00:02:43,770
improved more than the other students 
did. 

36
00:02:43,770 --> 00:02:48,450
Their IQ scores increased within eight 
months. 

37
00:02:48,450 --> 00:02:53,270
Not all students, and not by a huge 
amount, but for first and second grade 

38
00:02:53,270 --> 00:02:58,350
students, the increase was statistically 
significant, meaning that the difference 

39
00:02:58,350 --> 00:03:03,150
probably wasn't by chance alone. 
Rosenthal and Jacobson called this 

40
00:03:03,150 --> 00:03:07,845
phenomenon the "Pygmalion effect," after 
the George Bernard Shaw play 

41
00:03:07,845 --> 00:03:13,809
Pygmalion, in which Professor Higgins 
turns an uneducated flower girl into a 

42
00:03:13,809 --> 00:03:18,980
lady by teaching her to dress and speak 
in such a way that people expect her to 

43
00:03:18,980 --> 00:03:24,350
be a lady. 
Since 1968, the Pygmalion effect has been 

44
00:03:24,350 --> 00:03:29,380
replicated in all sorts of settings, from 
elementary schools, to high schools, to 

45
00:03:29,380 --> 00:03:33,250
universities, job settings, 
and although there are plenty of 

46
00:03:33,250 --> 00:03:37,695
exceptions and the effect isn't always 
strong, there's no doubt that 

47
00:03:37,695 --> 00:03:43,914
self-fulfilling prophecies do take place. 
In fact, studies suggest not only that 

48
00:03:43,914 --> 00:03:48,230
teacher expectations can influence 
student performance, but student 

49
00:03:48,230 --> 00:03:51,210
expectations about teachers can have an 
effect. 

50
00:03:52,240 --> 00:03:56,500
One other thing to note about 
self-fulfilling prophecies in daily life 

51
00:03:56,500 --> 00:04:01,420
is that their eventual truth can make it 
very hard to distinguish them from 

52
00:04:01,420 --> 00:04:05,830
regular prophecies. 
For example, consider the case in which 

53
00:04:05,830 --> 00:04:10,130
North Korea suspects that South Korea is 
aggressive 

54
00:04:10,130 --> 00:04:14,060
and, to protect itself, North Korea 
begins arming. 

55
00:04:14,060 --> 00:04:19,080
South Korea perceives the armament by 
North Korea as aggressive, and South 

56
00:04:19,080 --> 00:04:25,020
Korea responds by arming defensively. 
North Korea interprets the defensive 

57
00:04:25,020 --> 00:04:30,920
armament by South Korea as confirmation 
that South Korea is, in fact, aggressive, 

58
00:04:30,920 --> 00:04:36,470
so North Korea arms even more. 
This situation is known in political 

59
00:04:36,470 --> 00:04:40,630
science as a security dilemma, 
and one of the most frequently cited 

60
00:04:40,630 --> 00:04:44,300
results of such a dilemma is World War 
I. 

61
00:04:44,300 --> 00:04:48,660
Although we can never know for sure 
whether this was the case or not, many 

62
00:04:48,660 --> 00:04:54,600
historians have argued that World War I 
was caused in part by misperceptions of 

63
00:04:54,600 --> 00:05:00,890
aggression -- mistaken expectations that 
ultimately proved true. 

64
00:05:00,890 --> 00:05:06,100
What's interesting is that without a 
control group in which, say, North Korea 

65
00:05:06,100 --> 00:05:11,510
doesn't begin by expecting South Korea to 
arm (and of course, life doesn't come with 

66
00:05:11,510 --> 00:05:15,290
a control group), 
we can never be absolutely sure whether 

67
00:05:15,290 --> 00:05:20,710
North Korea's prophecy was 
self-fulfilling or simply prophetic. 

68
00:05:20,710 --> 00:05:25,270
It's always possible that a country arms 
as an act of aggression, but it's also 

69
00:05:25,270 --> 00:05:30,620
possible that the country would never 
have armed if it weren't expected to be 

70
00:05:30,620 --> 00:05:33,920
aggressive. 
The only way to know for sure would be 

71
00:05:33,920 --> 00:05:38,670
to see whether the country arms when the 
other side has no suspicions at the 

72
00:05:38,670 --> 00:05:43,450
outset, when there are no expectations to 
become fulfilled. 

73
00:05:44,980 --> 00:05:49,270
Well, fortunately, in the laboratory it is 
possible to design control groups, 

74
00:05:49,500 --> 00:05:54,810
and in the 1970s, Mark Snyder of the 
University of Minnesota conducted a 

75
00:05:54,810 --> 00:05:59,540
series of really ingenious experiments 
on a special type of self-fulfilling 

76
00:05:59,540 --> 00:06:02,970
prophecy known as "behavioral 
confirmation." 

77
00:06:04,150 --> 00:06:09,440
Behavioral confirmation takes place when 
people's social expectations lead them to 

78
00:06:09,440 --> 00:06:14,390
act in a way that causes others to 
confirm these expectations. 

79
00:06:14,390 --> 00:06:19,467
In other words, behavioral confirmation 
is a social type of self-fulfilling 

80
00:06:19,467 --> 00:06:22,100
prophecy. 
Let me give you an example from one of 

81
00:06:22,100 --> 00:06:27,420
the first and best known studies 
documenting behavioral confirmation. 

82
00:06:27,420 --> 00:06:32,720
In this study, published by Mark Snyder 
and Bill Swann, male college students 

83
00:06:32,720 --> 00:06:37,810
competed in a reaction-time contest with 
another student to see who could respond 

84
00:06:37,810 --> 00:06:43,980
most quickly over 24 trials. 
Students were told that on an alternating 

85
00:06:43,980 --> 00:06:49,150
basis, they would have access to a noise 
weapon that could deliver a distracting 

86
00:06:49,150 --> 00:06:54,120
sound to the other person from level 1 
all the way up to level 6, 

87
00:06:54,120 --> 00:06:58,610
which was about as loud as a lawnmower. 
I'll give you a rough sense of what it 

88
00:06:58,610 --> 00:07:02,648
was like, but let me first suggest you 
cover your ears or turn down the volume -- 

89
00:07:02,648 --> 00:07:04,140
it's pretty loud. 
Ready? 

90
00:07:04,140 --> 00:07:05,833
Here we go... 

91
00:07:05,833 --> 00:07:09,730
[NOISE]. Pretty loud. 

92
00:07:09,730 --> 00:07:13,040
The participants were each given a set of 
headphones. 

93
00:07:14,140 --> 00:07:17,145
That was weird! 
Anyway, they were given a set of 

94
00:07:17,145 --> 00:07:21,099
headphones so that they could use the 
weapon against the other person without 

95
00:07:21,099 --> 00:07:26,730
being blasted by the sound themselves. 
Specifically, Student A could use the 

96
00:07:26,730 --> 00:07:32,270
weapon on trials one, two, and three. 
Then Student B had it for trials four 

97
00:07:32,270 --> 00:07:36,030
through six. 
Then back to Student A for another three 

98
00:07:36,030 --> 00:07:42,570
trials, then to Student B, and so on, 
but here's the twist: Without Student B 

99
00:07:42,570 --> 00:07:48,300
knowing it, the experimenters led Student 
A to think that Student B was either a 

100
00:07:48,300 --> 00:07:54,870
very competitive, aggressive person, or a 
very cooperative person. 

101
00:07:54,870 --> 00:07:58,900
In reality these expectations were 
assigned at random. 

102
00:07:58,900 --> 00:08:03,510
They didn't have anything to do with how 
aggressive Student B actually was, but 

103
00:08:03,510 --> 00:08:08,960
the expectations had a dramatic effect. 
When students were led to expect a 

104
00:08:08,960 --> 00:08:14,095
partner who was aggressive, they came out 
with guns blazing, using weapon 

105
00:08:14,095 --> 00:08:18,360
intensities in the high range 61% of the 
time, 

106
00:08:18,360 --> 00:08:23,865
but when students expected a cooperative 
partner, they only used high intensities 

107
00:08:23,865 --> 00:08:28,140
28% of the time. 
And in response, students who were 

108
00:08:28,140 --> 00:08:34,460
expected to be competitive returned fire 
and actually became more aggressive -- 

109
00:08:34,460 --> 00:08:38,280
a textbook example of behavioral 
confirmation 

110
00:08:38,280 --> 00:08:43,890
(in this case, a kind of Pygmalion effect 
gone bad, in which you expect the worst 

111
00:08:43,890 --> 00:08:49,850
of someone and you get the worst). 
Because the expectations held by Student A 

112
00:08:49,850 --> 00:08:55,140
were randomly assigned by the 
experimenters, similar to a coin toss, we 

113
00:08:55,140 --> 00:08:58,810
can be sure that the people Student A 
competed with didn't differ in 

114
00:08:58,810 --> 00:09:03,240
aggression before the study. 
In other words, we know that the prophecy 

115
00:09:03,240 --> 00:09:08,150
was self-fulfilling -- that it was a 
product of expectations. 

116
00:09:08,150 --> 00:09:12,359
You might think of it like a laboratory 
version of a security dilemma. 

117
00:09:13,930 --> 00:09:17,150
But there's more. 
In a second phase of the experiment, the 

118
00:09:17,150 --> 00:09:22,640
researchers removed Student A from the 
study and had Student B compete in 

119
00:09:22,640 --> 00:09:27,860
another 24 trial reaction-time contest, 
this time with someone who wasn't told 

120
00:09:27,860 --> 00:09:32,098
anything about whether Student B was 
aggressive or cooperative. 

121
00:09:32,098 --> 00:09:36,760
The experimenters wanted to see whether 
people who had earlier been expected to 

122
00:09:36,760 --> 00:09:41,540
behave aggressively would continue to 
behave aggressively once the person who 

123
00:09:41,540 --> 00:09:45,490
held that expectation had been removed 
from the environment. 

124
00:09:45,490 --> 00:09:50,120
So they replaced Student A with a new 
person who wasn't led to believe 

125
00:09:50,120 --> 00:09:54,150
anything about Student B one way or 
another. 

126
00:09:54,150 --> 00:09:58,480
What did the researchers find? 
Under certain circumstances described in 

127
00:09:58,480 --> 00:10:02,740
the research report, Student A's 
expectations of aggression or 

128
00:10:02,740 --> 00:10:08,070
cooperation continued to have an effect 
on Student B's behavior with a new 

129
00:10:08,070 --> 00:10:11,600
person. 
That is, Student B continued to behave 

130
00:10:11,600 --> 00:10:15,830
more aggressively when that behavior had 
been expected earlier than when it 

131
00:10:15,830 --> 00:10:19,640
hadn't. 
Now think about that for a moment. 

132
00:10:19,640 --> 00:10:25,140
What these results suggest is that 
beliefs can literally create reality, 

133
00:10:25,140 --> 00:10:29,410
even after the person with the original 
beliefs has left the scene entirely. 

134
00:10:30,440 --> 00:10:34,750
The implication is that, for better or 
worse, the beliefs that your friends, 

135
00:10:34,750 --> 00:10:40,160
family members, teachers or coworkers 
hold about you may, in some cases, 

136
00:10:40,160 --> 00:10:44,390
continue to have an effect, even when 
these individuals aren't physically 

137
00:10:44,390 --> 00:10:48,490
present. 
As you might imagine, this research has 

138
00:10:48,490 --> 00:10:52,700
received a lot of attention, and to this day, it remains 

139
00:10:52,700 --> 00:10:56,410
controversial. 
Robert Rosenthal himself estimates that 

140
00:10:56,410 --> 00:11:01,380
studies on self-fulfilling prophecies 
only find the effect about 40% of the 

141
00:11:01,380 --> 00:11:04,680
time. 
Also, the research underscores the 

142
00:11:04,680 --> 00:11:08,310
importance of understanding how random 
assignment works. 

143
00:11:08,310 --> 00:11:12,680
Without that, a lot of the studies that 
we discuss in this course are bound to 

144
00:11:12,680 --> 00:11:16,540
be confusing. 
So this is actually a great time for you 

145
00:11:16,540 --> 00:11:21,480
to finish the first assigned reading, and 
then right after that, the Random 

146
00:11:21,480 --> 00:11:25,460
Assignment Assignment, 
which has a brief tutorial on random 

147
00:11:25,460 --> 00:11:30,085
sampling and random assignment 
(both of which are taken from Research 

148
00:11:30,085 --> 00:11:32,950
Randomizer, a Social Psychology Network 
partner site). 

149
00:11:34,030 --> 00:11:37,570
The assignment includes all the 
instructions that you need, and students 

150
00:11:37,570 --> 00:11:41,590
tell me that it takes about 15 minutes to 
complete, start to finish, 

151
00:11:41,590 --> 00:11:47,140
so it's not a terribly long assignment. 
Meanwhile, we can't very well end this 

152
00:11:47,140 --> 00:11:50,070
video without a pop-up question, so here 
you go. 

