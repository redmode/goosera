

1
00:00:06,680 --> 00:00:10,890
In our discussion of the bystander effect, we examined the role of bystanders

2
00:00:10,890 --> 00:00:15,230
in the murder of Kitty Genovese and the fatal assault on Deletha Word,

3
00:00:15,230 --> 00:00:17,530
but I didn't say much about the role of

4
00:00:17,530 --> 00:00:21,130
the perpetrators, or more generally, about the topic of violence.

5
00:00:21,130 --> 00:00:25,960
So in this video, I'd like to talk about the triggers of aggression -- 

6
00:00:25,960 --> 00:00:28,250
that is, the factors that could lead to anything from

7
00:00:28,250 --> 00:00:30,959
very small acts of aggression, all the way up to

8
00:00:30,959 --> 00:00:34,380
murder, or terrorism, or warfare.

9
00:00:34,380 --> 00:00:37,190
As defined in this week's reading, aggression is

10
00:00:37,190 --> 00:00:41,660
"physical or verbal behavior intended to cause harm."

11
00:00:41,660 --> 00:00:44,200
The act might involve hostile aggression, 

12
00:00:44,200 --> 00:00:48,350
which springs from anger or hostility, or instrumental aggression,

13
00:00:48,350 --> 00:00:52,200
which also aims to harm, but mainly as a means to some other end.

14
00:00:52,200 --> 00:00:56,140
For instance, social psychologists would say that when the United States

15
00:00:56,140 --> 00:00:59,700
bombed Baghdad in order to overthrow Saddam Hussein, 

16
00:00:59,700 --> 00:01:03,100
it was an act of instrumental aggression.

17
00:01:03,100 --> 00:01:08,440
And, of course, some acts may involve both hostile and instrumental components.

18
00:01:08,440 --> 00:01:11,740
Most social psychology research on aggression has to do

19
00:01:11,740 --> 00:01:14,470
with things that may or may not cause it, 

20
00:01:14,470 --> 00:01:20,030
such as violent TV shows, hot weather, pornography, video games, and so on.

21
00:01:21,150 --> 00:01:24,500
For example, one of the oldest questions is whether watching violence 

22
00:01:24,500 --> 00:01:27,850
on TV causes children to become aggressive,

23
00:01:27,850 --> 00:01:30,880
or whether it's merely correlated with aggression.

24
00:01:30,880 --> 00:01:32,850
Let's pause for a pop-up question so that 

25
00:01:32,850 --> 00:01:35,320
you can predict what researchers have found,

26
00:01:35,320 --> 00:01:39,290
and then after, I'll share with you what the scientific consensus is.

27
00:01:42,260 --> 00:01:44,580
Although the debate over correlation versus

28
00:01:44,580 --> 00:01:47,610
causation ran for decades, by now there's

29
00:01:47,610 --> 00:01:50,730
overwhelming evidence that exposure to TV violence

30
00:01:50,730 --> 00:01:54,490
causes aggression, at least in some cases.

31
00:01:54,490 --> 00:01:59,970
As far back as 1972, the U.S. Surgeon General warned about 

32
00:01:59,970 --> 00:02:05,600
"a causal relation between viewing violence on television and aggressive behavior."

33
00:02:05,600 --> 00:02:08,000
Then in 1982, a report by the 

34
00:02:08,000 --> 00:02:11,128
National Institute of Mental Health supported this conclusion.

35
00:02:11,128 --> 00:02:17,310
And in 1985, the American Psychological Association passed a resolution informing

36
00:02:17,310 --> 00:02:20,020
broadcasters and the public that television

37
00:02:20,020 --> 00:02:22,630
violence poses a danger to children.

38
00:02:23,450 --> 00:02:27,720
In 2000, six of the top medical and health
organizations in the

39
00:02:27,720 --> 00:02:31,090
United States signed a joint statement on the 

40
00:02:31,090 --> 00:02:34,370
impact of entertainment violence on children,

41
00:02:34,370 --> 00:02:40,340
in which they wrote that "well over 1,000 studies point overwhelmingly

42
00:02:40,340 --> 00:02:44,725
to a causal connection between media violence and aggressive behavior."

43
00:02:44,725 --> 00:02:48,591
And in 2007, a landmark meta-analysis

44
00:02:48,591 --> 00:02:52,270
examined 24 research reports in which children

45
00:02:52,270 --> 00:02:55,080
or adults were exposed to media violence

46
00:02:55,080 --> 00:02:57,520
and the amount of naturally occurring aggression

47
00:02:57,520 --> 00:03:00,790
was later recorded or independently rated.

48
00:03:00,790 --> 00:03:05,510
In other words, these weren't artificial laboratory tasks related to aggression.

49
00:03:05,510 --> 00:03:07,500
The children simply played with each other,

50
00:03:07,500 --> 00:03:09,200
and adults interacted with one another, 

51
00:03:09,200 --> 00:03:13,200
while trained observers recorded people's behavior.

52
00:03:13,200 --> 00:03:16,822
The results? Roughly one out of every four or five 

53
00:03:16,822 --> 00:03:20,600
participants showed an increase in aggression,

54
00:03:20,600 --> 00:03:24,710
which was a highly significant result given the large number of studies, 

55
00:03:24,710 --> 00:03:28,310
and of course, these are only brief exposures to violence.

56
00:03:28,310 --> 00:03:32,840
To put these results in context, the effect of media violence on aggression 

57
00:03:32,840 --> 00:03:38,510
was stronger than the effects are of condom usage on HIV transmission.

58
00:03:38,510 --> 00:03:43,200
They were stronger than the effects of lead exposure on children's intelligence, 

59
00:03:43,200 --> 00:03:47,860
and stronger than the effects of calcium intake on bone mass.

60
00:03:49,100 --> 00:03:51,000
So the relationship between media violence 

61
00:03:51,000 --> 00:03:54,450
and aggression is substantial, it's causal, 

62
00:03:54,450 --> 00:03:58,178
and the increase in aggression has been found in interactions among friends,

63
00:03:58,178 --> 00:04:02,200
among classmates, strangers, a wide variety of situations, 

64
00:04:02,200 --> 00:04:04,810
and a wide variety of studies.

65
00:04:04,810 --> 00:04:09,100
In fact, in 2010, a study published in the Journal of Advertising 

66
00:04:09,100 --> 00:04:12,460
even found a statistically significant increase 

67
00:04:12,460 --> 00:04:14,900
in aggressive thoughts when kids watched a

68
00:04:14,900 --> 00:04:19,100
violent 30-second TV commercial for action figure toys.

69
00:04:19,100 --> 00:04:20,900
A toy commercial! 

70
00:04:20,900 --> 00:04:24,000
Yet when the researchers held focus group sessions 

71
00:04:24,000 --> 00:04:28,900
(open discussions with kids and with parents), neither the children nor the adults 

72
00:04:28,900 --> 00:04:32,810
expressed concern about the violence in TV commercials.

73
00:04:33,030 --> 00:04:35,540
In those cases when parents limited the amount of

74
00:04:35,540 --> 00:04:38,090
media violence that their kids were allowed to see,

75
00:04:38,090 --> 00:04:41,030
the limits were always on TV shows, movies,

76
00:04:41,030 --> 00:04:44,280
or video games -- never violence in the commercials.

77
00:04:45,600 --> 00:04:48,640
Now why is TV so powerful?

78
00:04:48,640 --> 00:04:52,300
Well over time, kids learn to imitate what they see on TV, 

79
00:04:52,300 --> 00:04:56,290
and research suggests that later in childhood and in adulthood, 

80
00:04:56,290 --> 00:05:00,000
those effects sometimes show up in the form of bullying other people, 

81
00:05:00,000 --> 00:05:06,480
of partner violence, copycat crimes, and so on. Kids are unbelievably good 

82
00:05:06,480 --> 00:05:11,580
at imitation learning, which I will illustrate with my own daughter, Fijare.

83
00:05:11,580 --> 00:05:13,910
By the time she was 15 months old, Fijare had seen 

84
00:05:13,910 --> 00:05:17,420
enough people using cell phones that she began imitating.

85
00:05:17,420 --> 00:05:20,890
But as you can see from the bottom photos where she's talking

86
00:05:20,890 --> 00:05:25,140
into the back of the phone, she didn't have it quite right.

87
00:05:25,140 --> 00:05:27,840
By the time she was two and a half years old, however, 

88
00:05:27,840 --> 00:05:31,570
she had not only figured it out -- she looks like she's setting up a power lunch

89
00:05:31,570 --> 00:05:34,460
before going out to play in the snow.

90
00:05:34,460 --> 00:05:37,270
The tendency for kids to learn through imitation,

91
00:05:37,270 --> 00:05:40,250
and the link between children's imitation learning and aggression, 

92
00:05:40,250 --> 00:05:44,460
was dramatically illustrated in a famous 1961

93
00:05:44,460 --> 00:05:48,180
study by Albert Bandura and two coauthors 

94
00:05:48,180 --> 00:05:52,710
in which children were mildly frustrated by the removal of some popular toys

95
00:05:52,710 --> 00:05:56,780
and then either were or were not exposed to an adult model 

96
00:05:56,780 --> 00:06:00,070
who punched, kicked, and yelled at a plastic Bobo doll.

97
00:06:01,090 --> 00:06:03,990
What this study found was that kids who had earlier seen an

98
00:06:03,990 --> 00:06:08,970
aggressive model often ended up imitating the behavior and beating up the doll,

99
00:06:08,970 --> 00:06:11,000
whereas kids who hadn't been exposed to an 

100
00:06:11,000 --> 00:06:15,450
aggressive model rarely showed aggression toward the doll.

101
00:06:15,450 --> 00:06:18,970
Now, you might be wondering, why is this research so famous?

102
00:06:18,970 --> 00:06:23,200
Isn't it obvious that kids will imitate what they see adults do?

103
00:06:23,200 --> 00:06:26,750
Well, we have to guard against hindsight bias here.

104
00:06:26,750 --> 00:06:29,980
Before Bandura's research, it was widely believed

105
00:06:29,980 --> 00:06:32,220
that a good way to reduce aggression was

106
00:06:32,220 --> 00:06:37,620
to just get it out of your system by, for example, punching a pillow or screaming.

107
00:06:37,620 --> 00:06:38,510
Something like that.

108
00:06:38,510 --> 00:06:42,380
Or watching somebody else behave aggressively on the athletic field, 

109
00:06:42,380 --> 00:06:46,390
in the boxing ring, on TV or in a movie, and so forth.

110
00:06:46,390 --> 00:06:49,000
Here's an example from the days of vaudeville. 

111
00:06:49,800 --> 00:06:54,420
The idea is to blow off steam, to vent in a kind of "catharsis" -- 

112
00:06:54,420 --> 00:06:57,500
an idea that Aristotle wrote about and that was later adapted 

113
00:06:57,500 --> 00:07:01,300
by Sigmund Freud and others, as a sort of hydraulic model 

114
00:07:01,300 --> 00:07:04,150
in which emotional pressures build and build

115
00:07:04,150 --> 00:07:07,740
until finally you release them by behaving aggressively or by

116
00:07:07,740 --> 00:07:10,480
watching other people do it for you, like we see here.

117
00:07:11,990 --> 00:07:14,060
Isn't that clip amazing?

118
00:07:14,060 --> 00:07:17,300
In the Snapshot Quiz, I asked whether you thought that 

119
00:07:17,300 --> 00:07:21,570
punching a pillow or screaming might be an effective way to reduce aggression.

120
00:07:21,570 --> 00:07:26,830
If you answered yes, it may be in part due to a cultural legacy from Sigmund Freud.

121
00:07:26,830 --> 00:07:31,060
On the other hand if you said no, it may be a legacy from Albert Bandura.

122
00:07:31,060 --> 00:07:33,000
Let's pause so that you can see your answer.

123
00:07:36,300 --> 00:07:39,650
What Albert Bandura and others after him have shown

124
00:07:39,650 --> 00:07:45,100
is that contrary to the catharsis hypothesis, aggression begets aggression.

125
00:07:45,400 --> 00:07:49,610
So for example, if I punch the video camera lens, 

126
00:07:49,610 --> 00:07:52,650
it's not going to make me feel less aggressive or improve my mood.

127
00:07:52,650 --> 00:07:55,000
In fact, here, we can try an experiment.

128
00:07:55,000 --> 00:07:55,025
[SOUND OF GLASS BREAKING]

129
00:07:59,025 --> 00:08:01,680
Nope. Don't feel less aggressive,

130
00:08:01,680 --> 00:08:04,746
don't feel any better. In fact, here, let me get rid of this.

131
00:08:04,746 --> 00:08:09,700
There we go. Returning to the Bobo doll study,

132
00:08:09,700 --> 00:08:13,760
Professor Bandura was kind enough to send along to our class a couple minutes of

133
00:08:13,760 --> 00:08:18,480
video footage from the original study, during which he makes a few remarks,

134
00:08:18,480 --> 00:08:20,320
and I'll add a few comments of my own.

135
00:08:20,500 --> 00:08:21,990
Let's watch.

136
00:08:25,980 --> 00:08:31,610
>> An adult model beat up an inflated Bobo doll in novel ways.

137
00:08:31,610 --> 00:08:39,000
She pummeled it with a mallet, flung it in the air,

138
00:08:39,000 --> 00:08:44,080
kicked it repeatedly, and threw it down and beat it.

139
00:08:44,080 --> 00:08:47,580
And these novel acts were embellished with hostile remarks.

140
00:08:48,820 --> 00:08:51,870
It was once widely believed that seeing

141
00:08:51,870 --> 00:08:55,700
others vent aggression would drain the viewer's aggressive drive.

142
00:08:55,700 --> 00:09:00,500
As you can see, exposure to aggressive modeling is hardly cathartic.

143
00:09:01,360 --> 00:09:03,416
Exposure to aggressive modeling

144
00:09:03,416 --> 00:09:07,900
increased attraction to guns, even though it was never modeled.

145
00:09:07,900 --> 00:09:13,290
Guns had less appeal to children who had no exposure to the aggressive modeling.

146
00:09:13,290 --> 00:09:17,265
The children also picked up the novel, hostile language.

147
00:09:17,265 --> 00:09:21,900
>> To see how much aggression there was, just look at the facial expression.

148
00:09:21,900 --> 00:09:24,325
And it wasn't only boys.

149
00:09:24,325 --> 00:09:28,515
Here you can see a girl using a mallet to viciously attack the doll.

150
00:09:29,820 --> 00:09:32,930
>> The room contained varied play materials, and

151
00:09:32,930 --> 00:09:36,700
children could choose to play aggressively or non-aggressively.

152
00:09:36,700 --> 00:09:40,040
>> Besides aggressive toys like a mallet and dart guns, 

153
00:09:40,040 --> 00:09:45,400
there were crayons and coloring paper, dolls, cars and trucks, lots of choices.

154
00:09:45,400 --> 00:09:50,500
>> The children devised new ways of hitting the doll.

155
00:09:50,500 --> 00:09:57,000
And here's a creative embellishment. A doll becomes a weapon of assault.

156
00:09:58,000 --> 00:10:00,900
>> By the way, two historical footnotes:

157
00:10:00,900 --> 00:10:04,260
First, a Bobo doll of the sort that Bandura used in his

158
00:10:04,260 --> 00:10:08,390
research is now on display at the Center for the History of Psychology -- 

159
00:10:08,390 --> 00:10:11,590
actually, just a few feet away from one of the doors of the Stanford

160
00:10:11,590 --> 00:10:14,250
Prison Experiment and the electric shock generator

161
00:10:14,250 --> 00:10:17,830
used by Stanley Milgram in his obedience research.

162
00:10:17,830 --> 00:10:19,940
Here are a few photos I took just to give you 

163
00:10:19,940 --> 00:10:22,850
a close-up picture of what the doll looked like.

164
00:10:24,120 --> 00:10:27,660
And second, in 2004 Albert Bandura went on to

165
00:10:27,660 --> 00:10:32,065
receive the Outstanding Lifetime Contribution to Psychology Award

166
00:10:32,065 --> 00:10:36,800
from the American Psychological Association -- not only for his research on aggression, 

167
00:10:36,800 --> 00:10:41,000
but for research suggesting the positive side of imitation learning.

168
00:10:41,890 --> 00:10:45,190
Based on his research, for example, serial TV dramas

169
00:10:45,190 --> 00:10:50,020
have harnessed the power of imitation learning to promote positive social change.

170
00:10:50,020 --> 00:10:52,000
So, it's not all about aggression.

171
00:10:52,000 --> 00:10:53,640
Here are two examples.

172
00:10:54,100 --> 00:10:57,100
In Mexico, nearly a million people enrolled 

173
00:10:57,100 --> 00:11:00,010
in a program to learn to read after watching

174
00:11:00,010 --> 00:11:03,600
a TV drama that promoted literacy by showing characters 

175
00:11:03,600 --> 00:11:07,790
struggling to read and then becoming literate.

176
00:11:07,790 --> 00:11:10,530
And in Tanzania, a series of TV dramas

177
00:11:10,530 --> 00:11:13,070
led married people to discuss the need to

178
00:11:13,070 --> 00:11:17,100
control family size and to adopt family planning methods.

179
00:11:18,000 --> 00:11:21,000
But returning to the topic of aggression,

180
00:11:21,000 --> 00:11:23,100
one of the most important questions that 

181
00:11:23,100 --> 00:11:27,500
grew out of Bandura's research is whether behaviors modeled on television 

182
00:11:27,500 --> 00:11:30,000
show the same sort of effect that he documented 

183
00:11:30,000 --> 00:11:31,900
in the laboratory. For instance, when there's 

184
00:11:31,900 --> 00:11:34,870
a widely publicized boxing match, 

185
00:11:34,870 --> 00:11:37,790
does the level of aggression in society go up?

186
00:11:37,790 --> 00:11:39,440
In one of the most famous boxing matches 

187
00:11:39,440 --> 00:11:43,650
in history, Muhammad Ali against Joe Frazier,

188
00:11:43,650 --> 00:11:46,490
Muhammad Ali actually took out a little gorilla doll 

189
00:11:46,490 --> 00:11:49,040
and punched it during a press conference,

190
00:11:49,040 --> 00:11:53,983
pretending that the doll was Joe Frazier (somewhat similar to a little Bobo doll).

191
00:11:55,000 --> 00:12:00,000
>> Come on, Gorilla. We in Manila.

192
00:12:01,295 --> 00:12:03,720
>> Now, you could hear people in the background laughing,

193
00:12:03,720 --> 00:12:06,500
but the question here is deadly serious.

194
00:12:06,500 --> 00:12:09,810
Could this sort of highly rewarded prizefighting

195
00:12:09,810 --> 00:12:13,650
and punching increase the aggression of TV viewers?

196
00:12:13,650 --> 00:12:19,200
Not only is the answer yes -- there's evidence that it may even lead to murder.

197
00:12:20,340 --> 00:12:22,900
David Philips, a sociology professor at the 

198
00:12:22,900 --> 00:12:25,800
University of California, San Diego, published 

199
00:12:25,800 --> 00:12:28,500
an award-winning study in which he examined all 

200
00:12:28,500 --> 00:12:31,000
heavyweight championship prize fights between 

201
00:12:31,000 --> 00:12:36,700
1973 and 1978, and found that U.S. homicide rates 

202
00:12:36,700 --> 00:12:39,590
jumped over 12% above average

203
00:12:39,590 --> 00:12:42,630
three days after the boxing matches took place,

204
00:12:42,630 --> 00:12:46,010
and over 6% above average four days later -- 

205
00:12:46,010 --> 00:12:49,000
increases that translated into nearly 200

206
00:12:49,000 --> 00:12:52,300
additional murders over this five-year period 

207
00:12:52,300 --> 00:12:57,000
and led Professor Phillips to conclude that "heavyweight prize fights 

208
00:12:57,000 --> 00:13:01,920
stimulate fatal, aggressive behavior in some Americans." 

209
00:13:01,920 --> 00:13:05,050
In this research, Professor Phillips also found that the more

210
00:13:05,050 --> 00:13:09,300
a fight was publicized, the larger the increase in murder afterwards.

211
00:13:09,300 --> 00:13:12,740
And he found that when a Black boxer lost the fight, 

212
00:13:12,740 --> 00:13:16,810
the murder rate increased for young Black men but not young White men.

213
00:13:16,810 --> 00:13:19,800
On the other hand, when a White boxer lost the fight, 

214
00:13:19,800 --> 00:13:21,900
the murder rate increased for young White men, 

215
00:13:21,900 --> 00:13:25,000
but not young Black men -- a pattern suggesting that 

216
00:13:25,000 --> 00:13:30,000
aggression was being imitated. So, when it comes to violence 

217
00:13:30,000 --> 00:13:34,450
on TV or in movies, there's a causal connection to aggression.

218
00:13:34,450 --> 00:13:36,660
But what about other forms of media?

219
00:13:36,660 --> 00:13:40,300
For example, print media, radio, video games, and so forth. 

220
00:13:40,300 --> 00:13:43,400
Can they also generate aggression?

221
00:13:43,400 --> 00:13:47,710
Absolutely. To take one example, exposure to pornography -- 

222
00:13:47,710 --> 00:13:50,215
whether in the form of magazines or books,

223
00:13:50,215 --> 00:13:55,010
DVDs or videos -- is causally related to sexual aggression.

224
00:13:55,010 --> 00:13:58,300
Here's what a major review concluded after looking

225
00:13:58,300 --> 00:14:01,270
at what it called "nonviolent forms of pornography" 

226
00:14:01,270 --> 00:14:05,050
(things like magazine centerfolds) and violent forms of pornography 

227
00:14:05,050 --> 00:14:08,095
(things like characters being forced to have sex).

228
00:14:09,000 --> 00:14:11,820
"Experimental research shows that exposure

229
00:14:11,820 --> 00:14:15,000
to nonviolent or violent pornography results

230
00:14:15,000 --> 00:14:17,560
in increases in both attitudes supporting

231
00:14:17,560 --> 00:14:20,660
sexual aggression and in actual aggression."

232
00:14:21,800 --> 00:14:26,000
As for music and video games, I'll give just a few quick examples of

233
00:14:26,000 --> 00:14:30,219
how they can lead to aggression and to a loss in sensitivity toward violence.

234
00:14:30,200 --> 00:14:35,140
A 2006 study found that when male college students listened to a

235
00:14:35,140 --> 00:14:38,290
couple of songs with sexually aggressive lyrics,

236
00:14:38,290 --> 00:14:42,570
the students later showed a level of aggression toward a female confederate

237
00:14:42,570 --> 00:14:47,390
that was roughly 50% higher than they showed toward a male confederate,

238
00:14:47,390 --> 00:14:50,840
or toward a female confederate when the songs hadn't been aggressive.

239
00:14:50,840 --> 00:14:54,380
When you think about how much music college students listen

240
00:14:54,380 --> 00:14:56,400
to each day, it's amazing that there was a

241
00:14:56,400 --> 00:15:00,640
measurable effect after only two songs.

242
00:15:00,640 --> 00:15:03,800
Along similar lines, a 2007 study found that 

243
00:15:03,800 --> 00:15:06,983
after only 20 minutes of playing a violent video game,

244
00:15:06,983 --> 00:15:11,500
participants showed physiological evidence of desensitization 

245
00:15:11,500 --> 00:15:14,694
when shown a videotape of real-life violence.

246
00:15:14,694 --> 00:15:18,100
Specifically, people's heart rate didn't increase, 

247
00:15:18,100 --> 00:15:22,000
and their galvanic skin response, which is a measure of sweating, 

248
00:15:22,000 --> 00:15:25,778
didn't show any sign of physical arousal. It was as though 

249
00:15:25,778 --> 00:15:30,900
they had become deadened, after only 20 minutes of video games.

250
00:15:31,600 --> 00:15:36,300
Finally, a 2011 study found a neural desensitization 

251
00:15:36,300 --> 00:15:39,620
after college students played violent video games.

252
00:15:39,620 --> 00:15:44,270
In this study, students were randomly assigned to spend 25 minutes playing

253
00:15:44,270 --> 00:15:47,000
either a violent video game like Grand Theft Auto 

254
00:15:47,000 --> 00:15:52,480
or a nonviolent video game, and then they were shown violent images.

255
00:15:52,480 --> 00:15:56,420
What the researchers found was that students who played 25 minutes

256
00:15:56,420 --> 00:16:02,080
of violent video games showed less brain activity in response to violence.

257
00:16:02,080 --> 00:16:06,890
Their brains has become deadened, or desensitized, to violence.

258
00:16:06,890 --> 00:16:10,610
And the researchers found that the more desensitization there was,

259
00:16:10,610 --> 00:16:13,000
the more aggression these students later showed 

260
00:16:13,000 --> 00:16:15,770
in another task, which makes sense.

261
00:16:15,770 --> 00:16:18,140
If your brain doesn't respond to violence,

262
00:16:18,140 --> 00:16:20,480
then hurting other people isn't a big deal.

263
00:16:21,940 --> 00:16:24,610
For those of you who haven't seen a violent video game, 

264
00:16:24,610 --> 00:16:28,760
let me show you just 20 seconds of a game that's rated as "TEEN" -- 

265
00:16:28,760 --> 00:16:32,980
that is, generally suitable for ages 13 and up.

266
00:16:32,980 --> 00:16:35,490
In the full game, people are shot and killed, 

267
00:16:35,490 --> 00:16:37,200
but in the segment that I'll share with you,

268
00:16:37,200 --> 00:16:39,780
there's just a few seconds of the characters shooting.

269
00:16:39,780 --> 00:16:41,900
There's no victims and there's no blood.

270
00:17:08,200 --> 00:17:13,000
At one point, a version of this game was the number one online action game 

271
00:17:13,000 --> 00:17:17,610
in the United States. Over nine million copies have been downloaded.

272
00:17:17,610 --> 00:17:21,880
What makes the game unique is that it was developed by the U.S. Army

273
00:17:21,880 --> 00:17:26,200
as a recruitment tool to get young people to join the Army.

274
00:17:26,200 --> 00:17:27,800
Here's the Army's website, 

275
00:17:27,800 --> 00:17:29,950
and as you can see, there's a page with games

276
00:17:29,950 --> 00:17:35,300
and an invitation to "get in on the action" and "have fun."

277
00:17:35,300 --> 00:17:37,970
If we scroll down to the video game, the description says, 

278
00:17:37,970 --> 00:17:41,880
"America's Army, the official game of the U.S Army

279
00:17:41,880 --> 00:17:46,450
delivers an authentic and entertaining army experience." 

280
00:17:46,450 --> 00:17:49,000
So, here we have a government-funded video game 

281
00:17:49,000 --> 00:17:53,200
that research suggests will lead teenagers to become more aggressive 

282
00:17:53,200 --> 00:17:56,090
and more desensitized to violence, and that 

283
00:17:56,090 --> 00:17:59,980
presents simulated killing as fun entertainment.

284
00:18:01,080 --> 00:18:02,860
Now, why do I bring this up? 

285
00:18:02,860 --> 00:18:04,640
It's not that the game is more extreme

286
00:18:04,640 --> 00:18:07,400
than other games on the market -- far from it -- 

287
00:18:07,400 --> 00:18:09,960
but rather, that its use as a recruitment tool

288
00:18:09,960 --> 00:18:13,870
shows how mainstream and accepted media violence has become,

289
00:18:13,870 --> 00:18:16,290
and this acceptance raises certain questions.

290
00:18:17,500 --> 00:18:21,310
For example, if you enjoy playing violent video games, and you feel

291
00:18:21,310 --> 00:18:25,310
that they haven't made you more aggressive or desensitized you to violence,

292
00:18:25,310 --> 00:18:27,940
how do you know you haven't been affected?

293
00:18:27,940 --> 00:18:32,270
Life doesn't offer a control group version of you without video games,

294
00:18:32,270 --> 00:18:34,500
so what's the best evidence?

295
00:18:34,500 --> 00:18:38,000
Another question: Suppose you aren't affected when you play 

296
00:18:38,000 --> 00:18:42,600
violent video games, but research suggests that other people are.

297
00:18:42,600 --> 00:18:44,410
How would you answer a critic who says that 

298
00:18:44,410 --> 00:18:48,000
you're still contributing to a general culture of violence 

299
00:18:48,000 --> 00:18:53,000
by making it normative, or normal, to shoot at other people for fun?

300
00:18:53,200 --> 00:18:56,700
Regardless of whether you play video games, what do you think about 

301
00:18:56,700 --> 00:19:00,300
the U.S. military developing a game that might promote aggression 

302
00:19:00,300 --> 00:19:03,980
or desensitize teenagers to violence?

303
00:19:03,980 --> 00:19:06,790
Are they just speaking the language of young people today,

304
00:19:06,790 --> 00:19:10,010
just as many other countries have done over the years,

305
00:19:10,010 --> 00:19:12,000
or does this cross a line?

306
00:19:12,800 --> 00:19:16,190
And finally, what are the most effective steps you can take to reduce

307
00:19:16,190 --> 00:19:21,800
the effects of media violence in your life, in your family, and in society?

308
00:19:22,140 --> 00:19:25,850
I hope that you'll visit the discussion forum to share what you think, 

309
00:19:25,850 --> 00:19:30,100
see what others think, and raise further questions for the class to consider.

310
00:19:31,120 --> 00:19:34,440
Well, that's a general overview on the effects of media violence,

311
00:19:34,440 --> 00:19:37,860
but of course, there are other triggers of aggression as well.

312
00:19:37,860 --> 00:19:41,660
Here, for example, are a few important factors that we haven't discussed yet.

313
00:19:41,660 --> 00:19:45,660
Most of these are covered in the assigned reading for this week.

314
00:19:45,660 --> 00:19:50,000
First, biological factors such as genetics and testosterone. 

315
00:19:50,000 --> 00:19:55,100
Alcohol use, which, in the United States, is associated with 40% of all 

316
00:19:55,100 --> 00:19:59,570
violent crimes and two-thirds of all intimate partner violence. 

317
00:19:59,570 --> 00:20:05,170
In fact, a 2010 study even found that exposure to alcohol-related

318
00:20:05,170 --> 00:20:09,880
images and words is enough to increase aggressive thoughts and behavior,

319
00:20:09,880 --> 00:20:15,660
presumably because alcohol is paired with aggression in movies, TV shows, and daily life.

320
00:20:15,660 --> 00:20:22,000
So it primes or activates those associations, even when people haven't had any alcohol to drink.

321
00:20:22,620 --> 00:20:27,590
Aggression also increases when people are exposed to violent words and images.

322
00:20:27,590 --> 00:20:31,200
For example, experimental participants give a higher number of electric shocks 

323
00:20:31,200 --> 00:20:36,870
to someone when there are guns visible in the room than when no guns are in view.

324
00:20:36,870 --> 00:20:42,310
In other words, the very sight of a weapon can prime people to behave aggressively. 

325
00:20:42,310 --> 00:20:46,600
This kind of priming occurs whether the weapon is a handgun, rifle, or knife, 

326
00:20:46,600 --> 00:20:50,000
whether the weapon is physically present or just a photo,

327
00:20:50,000 --> 00:20:54,600
and whether the study takes place in a laboratory or field setting.

328
00:20:54,600 --> 00:20:58,330
What this means, for example, is that you could be watching or reading 

329
00:20:58,330 --> 00:21:03,000
news coverage about a war, terrorism, or anything else that shows

330
00:21:03,000 --> 00:21:06,400
a weapon, and that weapon could prime you to behave aggressively 

331
00:21:06,400 --> 00:21:10,280
toward others without you ever knowing it.

332
00:21:10,280 --> 00:21:13,090
Another trigger of aggression is culture -- 

333
00:21:13,090 --> 00:21:15,400
for example, living in a country or a community 

334
00:21:15,400 --> 00:21:18,700
that emphasizes honor and machismo.

335
00:21:19,460 --> 00:21:24,020
And one last trigger I should mention is physical pain or discomfort.

336
00:21:24,020 --> 00:21:29,560
Things like crowding, air pollution, and heat can all increase aggression.

337
00:21:29,560 --> 00:21:33,300
For instance, studies have found that during hotter days, months, 

338
00:21:33,300 --> 00:21:37,020
seasons, and years, aggression increases in the form of 

339
00:21:37,020 --> 00:21:40,540
domestic violence, criminal assaults, and

340
00:21:40,540 --> 00:21:44,380
even major-league batters being hit by pitched baseballs,

341
00:21:44,380 --> 00:21:48,400
which is widely seen as an act of aggression by the pitcher.

342
00:21:48,770 --> 00:21:52,190
In that regard, it's interesting to speculate whether heat might

343
00:21:52,190 --> 00:21:55,200
be playing a role in certain Middle Eastern or African regions 

344
00:21:55,200 --> 00:22:00,500
that are known to be hotspots or hotbeds of violence -- 

345
00:22:00,500 --> 00:22:04,450
not as a main factor, obviously, but as a secondary factor,

346
00:22:04,450 --> 00:22:08,400
a kind of contributing factor that makes aggression more likely.

347
00:22:08,800 --> 00:22:12,700
Research by Craig Anderson and his colleagues even suggests that 

348
00:22:12,700 --> 00:22:16,000
small increases in heat due to climate change may translate 

349
00:22:16,000 --> 00:22:20,330
into tens of thousands of serious and deadly assaults.

350
00:22:20,330 --> 00:22:24,700
This finding was recently supported by a 2013 meta-analysis 

351
00:22:24,700 --> 00:22:28,400
published by other researchers in the journal Science.

352
00:22:28,400 --> 00:22:34,000
The meta-analysis examines 60 different studies and concluded not only that

353
00:22:34,000 --> 00:22:37,520
"amplified rates of human conflict could represent a large

354
00:22:37,520 --> 00:22:40,899
and critical impact of anthropogenic climate change,"

355
00:22:40,899 --> 00:22:45,430
but that the effects on group conflict would probably be even larger

356
00:22:45,430 --> 00:22:48,200
than the effects on interpersonal conflict.

357
00:22:49,410 --> 00:22:53,490
This week's assigned reading discusses aggression in more detail, including ways

358
00:22:53,490 --> 00:22:58,150
to reduce aggression, and the message is very consistent with previous topics.

359
00:22:58,150 --> 00:23:02,700
If social psychology tells us anything about aggression and media violence, 

360
00:23:02,700 --> 00:23:07,040
it's to beware of underestimating the power of situational factors -- 

361
00:23:07,040 --> 00:23:11,900
of what we see, what we hear, and what we're entertained by.

362
00:23:13,100 --> 00:23:16,310
One of the main reasons that I focus this lecture so much

363
00:23:16,310 --> 00:23:20,730
on media violence is that, out of all the different triggers of aggression,

364
00:23:20,730 --> 00:23:24,400
this is one of the few in which most of us have a large degree of control 

365
00:23:24,400 --> 00:23:27,700
because we have the power of non-participation.

366
00:23:27,700 --> 00:23:30,640
In the end, we're the ones who create the playlist.

367
00:23:30,640 --> 00:23:34,000
We don't need to listen to lyrics that promote violence against women, 

368
00:23:34,000 --> 00:23:38,260
or watch TV programs and movies that prime aggression.

369
00:23:38,260 --> 00:23:42,700
So even though aggression isn't the happiest of topics, the ultimate message 

370
00:23:42,700 --> 00:23:45,960
of this research is incredibly empowering because it shows

371
00:23:45,960 --> 00:23:49,710
that individual choices can really make a difference.

372
00:23:49,710 --> 00:23:51,600
I'll see you next time.
