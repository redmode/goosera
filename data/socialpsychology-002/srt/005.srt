





1
00:00:06,150 --> 00:00:09,300
I never had the pleasure of meeting 
Stanley Milgram, but in this video we'll 

2
00:00:09,300 --> 00:00:13,372
hear from two social psychologists who came 
to know him well. 

3
00:00:13,372 --> 00:00:17,719
The first, Harold Takooshian, was a student of Stanley Milgram's in 

4
00:00:17,719 --> 00:00:23,790
the 1970's, and the second, Phil 
Zimbardo, was, amazingly enough, a high 

5
00:00:23,790 --> 00:00:28,620
school classmate of Stanley Milgram's in 
New York years before 

6
00:00:28,620 --> 00:00:31,520
either one of them became a social psychologist. 

7
00:00:31,520 --> 00:00:35,434
This video comes to us courtesy of Fordham University.

8
00:00:36,434 --> 00:00:39,680
 >> Here are two of us in New York City who knew Stanley Milgram in different ways. 

9
00:00:39,680 --> 00:00:43,520
My name is Harold Takooshian. 
 >> My name is Phil Zimbardo. 

10
00:00:43,520 --> 00:00:48
 >> And we wish you well in your 
conference in Canada, celebrating the 50th 

11
00:00:48 --> 00:00:50,320
anniversary of Stanley Milgram's classic 
work. 

12
00:00:50,320 --> 00:00:53,717
Gina Nestar, the conferees, we wish you 
well. 

13
00:00:53,717 --> 00:00:59,180
We'd like to give you a brief perspective 
in a different way on Stanley Milgram. 

14
00:00:59,180 --> 00:01:02,960
I would say that I knew Stanley Milgram 
as a student. 

15
00:01:02,960 --> 00:01:07,437
The fact is that Stanley Milgram is 
mostly known as a larger than life 

16
00:01:07,437 --> 00:01:11,914
researcher who did the obedience 
experiment, but there are about 50 of us 

17
00:01:11,914 --> 00:01:15,168
who knew Stanley as a professor, and 
there are a couple of things I would 

18
00:01:15,168 --> 00:01:18,036
say about this. 
One is that, Stanley, like Dr. 

19
00:01:18,036 --> 00:01:22,860
Zimbardo, was a dedicated professor 
all his life, not just a researcher,

20
00:01:22,860 --> 00:01:29,345
from 1960 at Yale 'til the very day he died, 
December 20th, 1984, chairing a 

21
00:01:29,345 --> 00:01:33,448
dissertation. 
And those of us who knew him knew that 

22
00:01:33,448 --> 00:01:39,392
Stanley was even greater as a teacher 
than a researcher. His class was 

23
00:01:39,392 --> 00:01:42,122
remarkable. 
The two things I would say about it: one 

24
00:01:42,122 --> 00:01:46,480
is that he was, he made the class come 
alive; 

25
00:01:46,480 --> 00:01:53,152
social psychology was remarkably vivid 
with him, and we had small classes so 

26
00:01:53,152 --> 00:01:56,043
there weren't really many students that 
studied with him, but those of us who 

27
00:01:56,043 --> 00:02:01,062
did -- it was remarkable.
The second thing I would say about 

28
00:02:01,062 --> 00:02:04,068
Stanley is that we, he had what we call 
"wonders." 

29
00:02:04,068 --> 00:02:08,578
He would do things in the class that no 
other professor would do, and we were 

30
00:02:08,578 --> 00:02:12,676
always wondering why he did it. 
One quick example is he asked the 

31
00:02:12,676 --> 00:02:16,108
students to grade each other at the end 
of the semester. Other professors don't 

32
00:02:16,108 --> 00:02:18,016
do that, and then he would sit there and read the 

33
00:02:18,016 --> 00:02:21,596
grades out loud that each of us gave to 
the other, and we learned from that 

34
00:02:21,596 --> 00:02:23,772
experience. 
Speaking personally, I learned a lot 

35
00:02:23,772 --> 00:02:29,452
about myself just by taking his class. 
The final thing I would say about Stanley 

36
00:02:29,452 --> 00:02:33,759
as a teacher is that we had a large 
gathering for him on his 49th birthday 

37
00:02:33,759 --> 00:02:39,285
because he was very ill, and we asked 
him, "What do you tell us students is the 

38
00:02:39,285 --> 00:02:43,798
makings of an excellent researcher?" and he 
said three things: 

39
00:02:43,798 --> 00:02:49,460
courage, courage, courage, and I must say that has influenced my 

40
00:02:49,460 --> 00:02:52,245
career as a teacher. 
I'm so grateful to Stanley. 

41
00:02:52,245 --> 00:02:56,417
Thank you. 
 >> Thanks, Harold. 

42
00:02:56,417 --> 00:03:01,986
So, I knew Stanley back in 1948-1949, 
another era. 

43
00:03:01,986 --> 00:03:08,011
We were students at James Monroe High 
School in the Bronx in senior year, 

44
00:03:08,011 --> 00:03:12,604
class 12H1, if I remember rightly. 
And Stanley was the smartest kid in the 

45
00:03:12,604 --> 00:03:16,850
class, 
the envy of most of the rest of us. 

46
00:03:16,850 --> 00:03:20,798
I think I, as I remember, I think he got 
all the awards at graduation, or almost 

47
00:03:20,798 --> 00:03:22,475
all the awards. 
He was in Arista. 

48
00:03:22,475 --> 00:03:29,531
But he was also in the stage, even then 
he had an interest in, in theatrical and 

49
00:03:29,531 --> 00:03:34,666
in filming, and people forget that 
later in his life he did a lot of film 

50
00:03:34,666 --> 00:03:42,990
work. But I knew him first, 
I had -- I grew up in the Bronx. Here I am, 

51
00:03:42,990 --> 00:03:52,516
the Bronx Tale, and I went to PS 52, a 
junior high school, and then from PS 52 I 

52
00:03:52,516 --> 00:04:00,854
went to, graduated -- I first went to high 
school at Stuyvesant High for one term,  

53
00:04:00,854 --> 00:04:05,526
Peter Stuyvesant, which was one of the 
elite schools in the Bronx, and then I discovered there were no girls. I 

54
00:04:05,526 --> 00:04:10,198
just had been three years with guys, and I 
had it, so I finished my first year and I 

55
00:04:10,198 --> 00:04:14,870
went to Monroe where there were girls, 
lots and lots of girls, and I was really 

56
00:04:14,870 --> 00:04:18,603
happy. 
That was like sophomore year, and then my 

57
00:04:18,603 --> 00:04:22,640
family moved to California -- North 
Hollywood, California, because my father's 

58
00:04:22,640 --> 00:04:25,470
sisters all lived there, sisters and 
brothers, 

59
00:04:25,470 --> 00:04:29,410
and it was a disaster for me because I 
had always been a popular kid. 

60
00:04:29,410 --> 00:04:32,660
As smart as Stanley was, I was the other 
side. 

61
00:04:32,660 --> 00:04:37,810
I was the popular kid, not so smart, and I was shunned. 

62
00:04:37,810 --> 00:04:41,570
I didn't understand that, meaning 
wherever I sat down people would move 

63
00:04:41,570 --> 00:04:47,827
away, in class, in the lunchroom, and this 
happened repeatedly over the course of 

64
00:04:47,827 --> 00:04:51,390
most of the year. 
I developed psychosomatic asthma, which 

65
00:04:51,390 --> 00:04:55,949
became the excuse my family needed to go 
back to the Bronx. 

66
00:04:55,949 --> 00:05:00,981
It was just a bad time, and the reason I was shunned, I discovered 

67
00:05:00,981 --> 00:05:07,076
at the end, at the end of the year, was 
that kids thought because I -- they had 

68
00:05:07,076 --> 00:05:12,108
heard I was from New York and Italian; 
therefore, I must be part of the Mafia and 

69
00:05:12,108 --> 00:05:16,320
therefore I'm dangerous. 
So they shunned me because they were 

70
00:05:16,320 --> 00:05:22,712
afraid of me. And so this Bronx Tale, 
which is a play, a movie, says, "Is it 

71
00:05:22,712 --> 00:05:26,114
better to be feared or loved?" 
Well, if you're in the Mafia, you'd rather 

72
00:05:26,114 --> 00:05:32,698
be feared, but I would rather be loved. 
So, the important point is that I went 

73
00:05:32,698 --> 00:05:35,180
then back to James Monroe High School for 
my senior year 

74
00:05:35,180 --> 00:05:43,785
(this is 1948, '49, '49 into '50), and I 
met this smart little kid, Stanley 

75
00:05:43,785 --> 00:05:49,720
Milgram, and he was the person who wrote 
the little blurbs in the yearbook. 

76
00:05:49,720 --> 00:05:55,780
That was his job, and so he was asking, 
asking me about myself and how to 

77
00:05:55,780 --> 00:05:58,570
describe. So I described the situation. I 
said, 

78
00:05:58,570 --> 00:06:01,436
"You know, I don't understand, 
because I had always been a popular kid 

79
00:06:01,436 --> 00:06:05,405
and now, within three months of getting 
into James Monroe High School, I was 

80
00:06:05,405 --> 00:06:12,310
elected by the senior class as the most 
popular boy in the class, James Monroe. 

81
00:06:12,310 --> 00:06:16,290
My equivalent was pretty Janie Monroe. 
And how could it be that, how could I be 

82
00:06:16,290 --> 00:06:21,220
shunned, ignored, rejected, excluded, 
you know, a few months earlier, 

83
00:06:21,220 --> 00:06:25,270
and now I'm the most popular?" And Stanley said, "It's the situation -- 

84
00:06:25,270 --> 00:06:31,730
it's not you," which is curious because 
this is the start of being a situationist. 

85
00:06:31,730 --> 00:06:35,822
And then, and the other thing that was 
amazing was, even at that young age (we 

86
00:06:35,822 --> 00:06:42,100
were like 16, 17), he was really ahead of 
his time in being concerned about social 

87
00:06:42,100 --> 00:06:44,446
issues, 
and that is, in a very personal way, he 

88
00:06:44,446 --> 00:06:47,060
was concerned, 
could the Holocaust happen again in 

89
00:06:47,060 --> 00:06:48,952
America? 
Could he and his family end up in a 

90
00:06:48,952 --> 00:06:52,032
concentration camp? 
And of course, people said, "No, that's 

91
00:06:52,032 --> 00:06:54,750
Nazi Germany." 
"We're not that kind of people." 

92
00:06:54,750 --> 00:06:58,599
"Could never happen here." 
And to his credit, he'd say, he'd say, 

93
00:06:58,599 --> 00:07:01,720
"How do you know? How do you -- how could 
you be so sure? 

94
00:07:01,720 --> 00:07:06,130
Don't you think they would have said the 
same thing in 1939 as you're saying now?" 

95
00:07:06,130 --> 00:07:12,021
And, essentially, his basic message was, 
has been, how do you know what you would 

96
00:07:12,021 --> 00:07:15,570
do, how could you predict with certainty 
what you would do

97
00:07:15,570 --> 00:07:21,240
in a new situation
unless you're in the situation? 

98
00:07:21,240 --> 00:07:26,660
Because when you're outside of the situation 
looking in, it's easy to make 

99
00:07:26,660 --> 00:07:30,458
proscriptive judgments. 
"I'm not that kind of person." "We're not 

100
00:07:30,458 --> 00:07:34,128
the kind of people to do this." And so essentially, Milgrim's 

101
00:07:34,128 --> 00:07:38,919
research 
really put ordinary people in a new 

102
00:07:38,919 --> 00:07:41,690
situation, 
a situation where in the role of teacher, 

103
00:07:41,690 --> 00:07:45,665
they had this enormous power over their 
student. 

104
00:07:45,665 --> 00:07:50,567
Now, they were in a chain of command 
where authority had power over them but 

105
00:07:50,567 --> 00:07:58,129
they had, they had the execution power 
over, over their student. 

106
00:07:58,129 --> 00:08:01,080
And the Stanford prison study, which then 
followed in the next decade, 

107
00:08:01,080 --> 00:08:05,690
was a similar thing, 
where guards were given total power over 

108
00:08:05,690 --> 00:08:09,300
prisoners. 
And then the point is, 

109
00:08:09,300 --> 00:08:12,460
how did they use that power? 
Well, they abused the power. 

110
00:08:12,460 --> 00:08:17,080
And that's, that's what I call evil: 
the intentional use and abuse of power 

111
00:08:17,080 --> 00:08:22,552
to harm, to hurt other people. 
Now, the last story I want to mention is, 

112
00:08:22,552 --> 00:08:30,736
I finished the Stanford Prison Experiment 
on August 20th, 1971, and in those days, 

113
00:08:30,736 --> 00:08:33,230
the American Psychological Association [conference] was on Labor 
Day. 

114
00:08:33,230 --> 00:08:36,650
I'm not sure where it was; somehow I remember 
Canada. 

115
00:08:36,650 --> 00:08:40,426
Anyway, I was supposed to give a talk 
about something that I was doing. It 

116
00:08:40,426 --> 00:08:44,220
was -- I'm not even sure what the topic 
was -- 

117
00:08:44,220 --> 00:08:49,270
and at the end of the talk I said, I said 
I've saved five minutes because I 

118
00:08:49,270 --> 00:08:52,570
really have to share something we've just 
done. I've just done this study, the 

119
00:08:52,570 --> 00:08:57,940
Stanford Prison Experiment, 
and I had a few slides I put up, and I 

120
00:08:57,940 --> 00:09:01,224
just described, you know, we haven't even 
analyzed the data, but clearly what we're 

121
00:09:01,224 --> 00:09:07,319
seeing is in a very short time, good boys 
are abusing other boys only because 

122
00:09:07,319 --> 00:09:11,973
they're in a role, and the role is in a 
situation, and the situation is in a 

123
00:09:11,973 --> 00:09:16,180
setting, 
and, and I said, you know, I hope to be 

124
00:09:16,180 --> 00:09:20,400
able to present next year a fuller 
version of the study.

125
00:09:20,400 --> 00:09:24,210
And Stanley came over, and he was not a 
very emotional guy, and he embraced me. 

126
00:09:24,210 --> 00:09:28,445
I said, "Whoa, hey Stan." 
He said, "Oh, thank you! Now you have taken the 

127
00:09:28,445 --> 00:09:33,020
ethical heat off of my back on your back 
as having done the most unethical study 

128
00:09:33,020 --> 00:09:37,030
ever." 
[LAUGH] So, I laugh, but he, he 

129
00:09:37,030 --> 00:09:39,714
was right 
because in my experiment, people really 

130
00:09:39,714 --> 00:09:42,702
suffered, 
not for 40 minutes, as in his study, and 

131
00:09:42,702 --> 00:09:46,092
then, in his study, of course, they were 
told that it's a confederate, you really 

132
00:09:46,092 --> 00:09:48,518
didn't hurt him. 
In my study, people really hurt one 

133
00:09:48,518 --> 00:09:52,472
another over an extended period of time, 
for which I apologize. 

134
00:09:52,472 --> 00:09:54,700
 In any event, thanks so much. 

135
00:09:54,700 --> 00:09:57,745
Thanks, Stanley, for all the work you've 
done, 

136
00:09:57,745 --> 00:10:03,777
all the ways you've inspired your 
students, your colleagues. And we hope if 

137
00:10:03,777 --> 00:10:08,110
you're looking down on the good times, 
you'll enjoy. 

