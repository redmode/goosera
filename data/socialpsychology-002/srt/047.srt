
1
00:00:06,817 --> 00:00:08,883
Welcome to Social Psychology!

2
00:00:08,883 --> 00:00:11,683
I am so glad you're taking this course.

3
00:00:11,683 --> 00:00:15,750
I'm Scott Plous, Executive Director of Social Psychology Network  

4
00:00:15,750 --> 00:00:20,216
(which I'll talk about in a later video), and Professor of Psychology at

5
00:00:20,216 --> 00:00:24,950
Wesleyan University, a liberal arts university in Middletown, Connecticut, 

6
00:00:24,950 --> 00:00:30,483
almost exactly between New York and Boston -- about two hours from each city.

7
00:00:30,483 --> 00:00:32,483
Here's our corner of the world.

8
00:00:46,883 --> 00:00:53,350
Wesleyan is a private university, nonreligious, fairly selective, and expensive,

9
00:00:53,350 --> 00:00:58,150
but thanks to the magic of Coursera, and a decision by the Wesleyan administration, 

10
00:00:58,150 --> 00:01:02,550
this short course is being offered free of charge to anyone who's interested. 

11
00:01:02,550 --> 00:01:07,417
So, let's hear it for Coursera and Wesleyan!

12
00:01:07,417 --> 00:01:11,683
Seriously, Wesleyan has contributed funds for the hardware and software 

13
00:01:11,683 --> 00:01:16,350
needed to create this course as a sort of experiment in higher education,

14
00:01:16,350 --> 00:01:20,216
and of course, as a public service. And I should probably mention 

15
00:01:19,360 --> 00:01:24,216
up front that I, myself, am not making a penny from the course.

16
00:01:24,216 --> 00:01:26,617
I'm joining this effort because I genuinely find 

17
00:01:26,617 --> 00:01:30,083
the idea of free education incredibly exciting.

18
00:01:30,083 --> 00:01:34,683
So, I'm honored to be your instructor -- happy to do it.

19
00:01:34,683 --> 00:01:39,150
And to help administer the course, we have some wonderful teaching assistants.

20
00:01:39,150 --> 00:01:43,017
You can see their smiling faces and read about their backgrounds from 

21
00:01:43,017 --> 00:01:47,570
our course page by clicking on the menu item that says "Your Friendly TAs."

22
00:01:49,083 --> 00:01:53,417
As for my own background, the Coursera website already has an overview 

23
00:01:53,417 --> 00:01:58,150
and links, so I'll be brief. My undergraduate psychology degree came from the 

24
00:01:58,150 --> 00:02:04,150
University of Minnesota, where I spent many happy hours in dear old Elliott Hall.

25
00:02:04,150 --> 00:02:08,150
And my doctoral and postdoctoral work was done at Stanford University, 

26
00:02:08,150 --> 00:02:11,750
with Phil Zimbardo as my graduate advisor.

27
00:02:11,750 --> 00:02:15,000
As you can see from this photo, Professor Zimbardo

28
00:02:15,000 --> 00:02:18,216
has become something of a superhero in the field, 

29
00:02:18,216 --> 00:02:21,617
and he's done this course a good deed by generously granting 

30
00:02:21,617 --> 00:02:25,750
anyone who takes this course free access to his documentary 

31
00:02:25,750 --> 00:02:29,150
on the famous Stanford Prison Experiment.

32
00:02:29,150 --> 00:02:33,683
You'll also have free access to several other online videos: 

33
00:02:33,683 --> 00:02:38,083
a great video entitled "The Abilene Paradox"

34
00:02:38,083 --> 00:02:40,950
(in a later lecture, I'll talk about what the paradox is -- 

35
00:02:40,950 --> 00:02:43,750
it's really, really fascinating).

36
00:02:43,750 --> 00:02:50,150
Stanley Milgram's classic film Obedience and an ABC News Primetime followup.

37
00:02:50,150 --> 00:02:54,883
A PBS documentary with surprising research on what makes us happy, 

38
00:02:54,883 --> 00:02:57,817
and just as important, what doesn't.

39
00:02:57,817 --> 00:03:02,950
Two animated guest lectures. The first, called "Secrets from the Science

40
00:03:02,950 --> 00:03:08,550
of Persuasion," is a joint lecture by two world-class experts on the topic.

41
00:03:08,550 --> 00:03:13,417
And the second is on empathy and the power of "outrospection" -- 

42
00:03:13,417 --> 00:03:17,483
of getting to know yourself not just through introspection, but through 

43
00:03:17,483 --> 00:03:22,617
the lives of other people. And of course, much, much more.

44
00:03:23,483 --> 00:03:27,150
One unique aspect of the course is the large number of individuals 

45
00:03:27,150 --> 00:03:31,617
and organizations contributing free materials, beginning with 

46
00:03:31,617 --> 00:03:36,017
David Myers of Hope College and Mike Sugarman of McGraw Hill, 

47
00:03:36,017 --> 00:03:40,350
who are allowing us to use two outstanding textbooks authored by 

48
00:03:40,350 --> 00:03:45,000
Professor Myers. Here are just some of the contributors -- please see 

49
00:03:45,000 --> 00:03:48,817
our course sign-up page for a more complete list, and I'll acknowledge 

50
00:03:48,817 --> 00:03:55,000
others as the course unfolds. These groups have been just incredibly generous.

51
00:03:55,483 --> 00:03:59,550
Another unique aspect of the course is that we'll be using resources from 

52
00:03:59,550 --> 00:04:04,483
Social Psychology Network -- the world's largest online community devoted 

53
00:04:04,483 --> 00:04:09,417
to social psychology. In a later video, I'll give you a tour of the Network, 

54
00:04:09,417 --> 00:04:15,283
which I founded in 1996 -- long time ago now -- and which has had over

55
00:04:15,283 --> 00:04:21,283
a quarter billion page views since then. In that video, I'll describe some features

56
00:04:21,283 --> 00:04:24,817
that I think would be of special interest to members of the class, 

57
00:04:24,817 --> 00:04:29,550
and I'll invite you to join the Network. Coursera and Social Psychology Network

58
00:04:29,550 --> 00:04:35,683
have teamed up to create some wonderful new resources that I think that you'll like.

59
00:04:35,683 --> 00:04:39,000
You can see that the Network is very excited to meet you. 

60
00:04:39,750 --> 00:04:44,150
One other aspect of the course that makes it a bit unique is that we'll begin 

61
00:04:44,150 --> 00:04:47,487
with a "Snapshot Quiz" to take a picture of your thinking 

62
00:04:47,487 --> 00:04:51,483
before being exposed to the lectures and assigned readings.

63
00:04:51,483 --> 00:04:54,150
That way, you'll be able to compare your answers with 

64
00:04:54,150 --> 00:04:57,483
the results of research that we discuss later.

65
00:04:57,483 --> 00:05:01,417
In other words, you'll be able to privately view your own quiz answers 

66
00:05:01,417 --> 00:05:07,216
right in the lecture videos -- a new feature that Coursera created just for our class, 

67
00:05:07,216 --> 00:05:12,216
and, as far as I know, something that's never been done before in an online course.

68
00:05:12,216 --> 00:05:15,817
And when the course ends, the Snapshot Quiz will also help you see 

69
00:05:15,817 --> 00:05:19,617
how much your psychology expertise has changed.

70
00:05:19,617 --> 00:05:23,483
You know, it's easy to say that psychology is just common sense, 

71
00:05:23,483 --> 00:05:26,150
but when you dig deeper, there are all sorts of questions 

72
00:05:26,150 --> 00:05:30,000
in which you can think of good reasons why the answer might be yes, 

73
00:05:30,000 --> 00:05:33,000
and equally good reasons why it might be no.

74
00:05:33,000 --> 00:05:36,617
In dating and marriage, do opposites attract?

75
00:05:36,617 --> 00:05:40,150
Does punching a pillow reduce aggression?

76
00:05:40,150 --> 00:05:43,350
Do women fall in love faster than men?

77
00:05:43,350 --> 00:05:47,400
Is it true that money can't buy happiness?

78
00:05:47,883 --> 00:05:50,550
That's where social psychology comes in.

79
00:05:50,550 --> 00:05:55,083
It's the scientific study of how people think about, influence, 

80
00:05:55,083 --> 00:05:58,083
and relate to one another. It uses many of the same 

81
00:05:58,083 --> 00:06:01,550
experimental methods found in other branches of science.

82
00:06:01,550 --> 00:06:05,617
It uses many of the same techniques for data analysis.

83
00:06:05,617 --> 00:06:08,216
And the results of these investigations are typically 

84
00:06:08,216 --> 00:06:12,000
published in peer-reviewed research journals.

85
00:06:12,000 --> 00:06:14,750
And as long as we're talking about research, I should also mention 

86
00:06:14,750 --> 00:06:18,017
that from time to time in these videotape lectures, 

87
00:06:18,017 --> 00:06:21,417
I'm going to pause the action to ask you a question.

88
00:06:21,417 --> 00:06:24,216
It might be to predict research findings before 

89
00:06:24,216 --> 00:06:26,883
I share them with you -- just to take a guess.

90
00:06:26,883 --> 00:06:30,083
Or it might be to ask you about something covered in the lecture, 

91
00:06:30,083 --> 00:06:32,483
just to make sure that you're following the material.

92
00:06:32,483 --> 00:06:35,483
It might even be to ask you a question that doesn't have 

93
00:06:35,483 --> 00:06:38,683
a right or wrong answer, just to stimulate your thinking, 

94
00:06:38,683 --> 00:06:45,216
to help you think about a topic actively rather than passively watching the videos.

95
00:06:45,216 --> 00:06:49,900
Let me just turn out the lights for a moment and give you an example.

96
00:06:53,150 --> 00:06:56,483
If you're like most people, you answered "yes" to this question.

97
00:06:56,483 --> 00:06:59,417
Not everyone does, but most people do.

98
00:06:59,417 --> 00:07:03,100
What's interesting is that if you give a different set of people 

99
00:07:03,100 --> 00:07:06,883
the opposite research finding -- that is, that attractive people are 

100
00:07:06,883 --> 00:07:10,950
seen as more intelligent -- most people again say that 

101
00:07:10,950 --> 00:07:13,600
that's what they would have guessed.

102
00:07:13,600 --> 00:07:15,350
Why? What's going on here?

103
00:07:15,350 --> 00:07:18,883
Well, it's something known as "hindsight bias," or the 

104
00:07:18,883 --> 00:07:23,283
"I-knew-it-all-along effect," and it's covered in this week's assigned readings.

105
00:07:23,283 --> 00:07:28,100
It's the tendency for people to see things as more obvious in hindsight,

106
00:07:28,100 --> 00:07:30,950
after they already know what the outcome is.

107
00:07:30,950 --> 00:07:35,283
The moral of the story with respect to social psychology findings is that -- 

108
00:07:35,283 --> 00:07:39,000
whether they're about attractiveness or anything else -- they're a whole lot 

109
00:07:39,000 --> 00:07:43,500
harder to predict when you don't know already what the answer is.

110
00:07:43,500 --> 00:07:45,500
And that's what makes it interesting.

111
00:07:45,500 --> 00:07:47,817
That's what makes it worth studying.

112
00:07:47,817 --> 00:07:52,150
So, if you want to know, for example, whether attractiveness is associated 

113
00:07:52,150 --> 00:07:56,150
with people seeing you as more intelligent, or you want to know 

114
00:07:56,150 --> 00:08:00,617
whether opposites attract, stick around, and you'll learn about that

115
00:08:00,617 --> 00:08:04,417
and many other research findings throughout this course.

116
00:08:04,417 --> 00:08:06,617
That's the deal, that's the deal.

117
00:08:06,617 --> 00:08:09,950
If you commit yourself to a few hours each week for 

118
00:08:09,950 --> 00:08:13,216
the duration of the course -- rather than picking and choosing 

119
00:08:13,216 --> 00:08:17,617
particular topics or particular videos and readings -- if you take it 

120
00:08:17,617 --> 00:08:21,750
as a package, I promise that I will do everything possible 

121
00:08:21,750 --> 00:08:25,817
to make this course worth your valuable time.

122
00:08:25,817 --> 00:08:27,800
How will it work?

123
00:08:27,800 --> 00:08:30,400
That's the subject of our next video.

124
00:08:30,400 --> 00:08:31,900
I'll see you there!

