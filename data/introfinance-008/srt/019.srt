1
00:00:00,000 --> 00:00:05,046
So what I'm going to do is I'm going to
introduce the concept of compounding.

2
00:00:05,074 --> 00:00:11,031
Therefore I do another problem and then
what I would like you to do is, do a

3
00:00:11,031 --> 00:00:16,082
problem with compounding. So simple
interest rate we've talked about, simple

4
00:00:16,082 --> 00:00:22,053
interest is very simple which is if I give
a $100,000 to the bank and pays ten

5
00:00:22,053 --> 00:00:27,060
percent every year, you get $10,000 every
year but that's not how the world is.

6
00:00:27,060 --> 00:00:33,049
Let's do compound interest. Suppose you
take the same problem, right? You plan to

7
00:00:33,049 --> 00:00:39,255
attend the business school and you will be
forced to take out a $100,000 loan at ten

8
00:00:39,255 --> 00:00:44,096
percent but now the loan characteristic
changes. What are your monthly payments

9
00:00:44,096 --> 00:00:50,034
given that you'll have to payback the loan
in five years? Remember what I have

10
00:00:50,034 --> 00:00:56,000
changed. Have I changed the amount you're
borrowing? No. Have I changed the interest

11
00:00:56,000 --> 00:01:00,076
rate, you're borrowing it yearly?
Actually, yes but we'll see that in a

12
00:01:00,076 --> 00:01:06,015
second. The interest rate is written as
ten%. The thing that I've changed on you

13
00:01:06,015 --> 00:01:11,019
is another real world's twist which I
think is important. I've changed the

14
00:01:11,019 --> 00:01:16,043
annual payments to monthly payments and
the number of years are the same. So,

15
00:01:16,043 --> 00:01:22,065
quick question, what is your monthly
payment and what is the real annual

16
00:01:22,065 --> 00:01:29,004
interest rate? So these are the two thing
we will do and I will show you how to do

17
00:01:29,004 --> 00:01:36,010
this and I will expect you to understand
and do it on your own but I'll, I'll go to

18
00:01:36,010 --> 00:01:49,536
the steps, okay. So what has changed? R is
ten percent annual and, but my payments is

19
00:01:49,536 --> 00:02:02,273
monthly so five years implies, how many
months? 60 months. If the interest rate is

20
00:02:02,273 --> 00:02:07,723
t10 percent annual and by the way the good
news is, that's how interest rates are

21
00:02:07,723 --> 00:02:18,441
quoted. What is the interest rate per
month? It's .1 / twelve. And the reason is

22
00:02:18,441 --> 00:02:24,901
there are twelve months. Why is five years
60 months? Because there are twelve months

23
00:02:24,901 --> 00:02:34,605
in a year, okay. And what is Pv? 100,000.
And what are we trying to figure out? Pmt.

24
00:02:34,605 --> 00:02:41,689
But we are not doing five of them, we are
doing how many of them? 60. So a lot of

25
00:02:41,689 --> 00:02:46,660
people get very confused when the
periodicity of the interest payment

26
00:02:46,660 --> 00:02:52,085
changes so you could have monthly
interest, you could have annual interest,

27
00:02:52,085 --> 00:02:58,082
you could have daily int erest, you could
have quarterly interest. The way I think

28
00:02:58,082 --> 00:03:04,093
about this problem is to remember you are
in control. So the best way to deal with

29
00:03:04,093 --> 00:03:15,051
this problem is to change the timeline, so
do this. Start at zero, put 100,000. Then

30
00:03:15,051 --> 00:03:28,052
how many periods? One through 60. Why?
Because I'm doing the problem monthly.

31
00:03:28,052 --> 00:03:35,689
What is the interest rate per period?
Apples to apples remember take .1 /

32
00:03:35,689 --> 00:03:46,112
twelve. Does it make sense? I'm being
internally consistent. Why is this a good

33
00:03:46,112 --> 00:03:52,503
way to solve the problem? Because the
problem is now fixed to what I know

34
00:03:52,503 --> 00:04:00,226
already so what will happen? Let's go on a
spreadsheet and do this problem and then

35
00:04:00,226 --> 00:04:10,362
see so, okay. The good news is I have this
problem solved for an annual basis. So

36
00:04:10,362 --> 00:04:17,377
what do I do? I just divide .1 by twelve.
What have I done? I've converted the

37
00:04:17,377 --> 00:04:23,056
interest rate to monthly. Then what do I
do? Whatever I've divided the interest

38
00:04:23,056 --> 00:04:27,737
rate by, I have to multiply the number of
years by the same amount and it's 60,

39
00:04:27,737 --> 00:04:40,042
right? What is $100,000? Hasn't changed.
My interest rate, my payment amount is

40
00:04:40,042 --> 00:04:49,033
2124. So I'm paying about $2125 per month
to repay the loan so you see what I've

41
00:04:49,033 --> 00:04:58,070
done. I've just simply taken the fact that
I know that I can mess with the timeline.

42
00:04:58,070 --> 00:05:07,989
So I made m 60, I made R .1 / twelve, and
my Pv, the amount of loan, was 100,000 and

43
00:05:07,989 --> 00:05:14,012
I came up with I believe 2125. Let me just
double check that the numbers are

44
00:05:14,012 --> 00:05:20,144
right.Yes, it is. So, so the question now
is, this is the question number one, how

45
00:05:20,144 --> 00:05:26,146
many of these will I pay? Obviously 60. I
will encourage you to do one exercise.

46
00:05:26,146 --> 00:05:32,804
What is the present value of paying 2125
at that interest rate 60 times? Answer has

47
00:05:32,804 --> 00:05:39,032
to be this, the amount of [inaudible]
right? Let me ask you, how much will you

48
00:05:39,032 --> 00:05:50,738
owe fter thirty months. Right? How much
will you owe after thirteen months? Very

49
00:05:50,738 --> 00:06:03,076
simple, make PMT 2125 which you just
calculated, right? R is what? .1 / twelve,

50
00:06:03,079 --> 00:06:13,709
right? M is how much? Remember where we
are standing now? You're standing at point

51
00:06:13,709 --> 00:06:21,694
30 looking forward to 60. M is 30. Do the
Pv of this, you have the amount of money

52
00:06:21,694 --> 00:06:27,417
you owe the bank. It's so simple, right?
You don't even have to d o that whole

53
00:06:27,417 --> 00:06:32,352
table. The reason I went through this
problem in detail, with and without

54
00:06:32,352 --> 00:06:38,391
compounding and with the, the annual and
monthly is simply to emphasis to you is

55
00:06:38,391 --> 00:06:44,436
that it is extremely important for you to
recognize that finance is very logical and

56
00:06:44,436 --> 00:06:49,815
you take yourself to the problem and not
let the problem scare you, okay. What is

57
00:06:49,815 --> 00:07:02,020
the actually interest rate? How much is my
annual are actually? Okay. So this is a

58
00:07:02,020 --> 00:07:09,076
good question to ask, right? So here's for
you to pause and think. The stated R is

59
00:07:09,076 --> 00:07:15,524
ten percent but the actual R can't be
ten%. It has to be more and the reason is

60
00:07:15,524 --> 00:07:23,638
again pause compounding, right? So let's
just quickly do that and then I encourage

61
00:07:23,638 --> 00:07:31,631
you right after that to take another break
as I said, today is a little bit intense

62
00:07:31,631 --> 00:07:39,753
and I want to emphasize. We have done the
time and the formula very simple so what

63
00:07:39,753 --> 00:07:48,041
I'm going to do, is I'm going to just use
this formula and explain. If I put $one at

64
00:07:48,041 --> 00:08:02,504
what interest rate? R is annual ten%. This
is always annual. What is K? K is, is the

65
00:08:02,504 --> 00:08:11,012
number of periods that are within that
year so K is twelve here. Why? Because

66
00:08:11,012 --> 00:08:22,856
it's monthly. So how many periods? Twelve
month period, twelve^12 What is this? This

67
00:08:22,856 --> 00:08:30,026
number is the future value of $one after
twelve intervals so what is the interest

68
00:08:30,026 --> 00:08:36,669
rate being charged? Is that -one? And I
would encourage you to do this calculation

69
00:08:36,669 --> 00:08:43,419
and the answer is, you should know, this
number is greater than ten%. Why? Because

70
00:08:43,419 --> 00:08:49,344
you are paying ten percent annually but
actually that's not true. You're not

71
00:08:49,344 --> 00:08:54,630
paying ten percent annually, you're paying
ten percent divided by twelve monthly and

72
00:08:54,630 --> 00:09:01,314
with compounding, raised to power twelve,
this works out to actually be about

73
00:09:01,314 --> 00:09:07,865
10.47%. Now you may think that .5 percent
is not a big deal, it is especially if

74
00:09:07,865 --> 00:09:14,770
you're borrowing a lot of money, it's a
big deal. So the difference between this

75
00:09:14,770 --> 00:09:23,436
and this, these two, this is stated and
this is actually the real interest that

76
00:09:23,436 --> 00:09:31,733
you are being charged. So I hope you take
a break now. What we have done is we have

77
00:09:31,733 --> 00:09:37,262
taken a loan problem. We have dissected it
because it's, it's just reflects

78
00:09:37,262 --> 00:09:43,150
everything awesome about finance, And then
what we have done is w e have gone back

79
00:09:43,150 --> 00:09:48,945
and said to ourselves, what if we took the
loan and we make it a monthly loan? No

80
00:09:48,945 --> 00:09:54,006
problem. If you know finance and you are
thinking clearly, you're logical, you

81
00:09:54,006 --> 00:09:59,258
won't mess with anything except your
timeline. So you're month matches the

82
00:09:59,258 --> 00:10:04,462
period and if there are five years, there
are 60 months. The interest rate changes

83
00:10:04,462 --> 00:10:09,151
accordingly and changes accordingly and
then your payment is calculated based on

84
00:10:09,151 --> 00:10:14,794
the same kind of information you give.
Okay. So I would at this point again take

85
00:10:14,794 --> 00:10:20,060
a break. We have little bit left, one
problem left and little bit of concept for

86
00:10:20,060 --> 00:10:25,410
today and then we will call it quits. It's
a, it's a long day today but I definitely

87
00:10:25,410 --> 00:10:29,034
encourage you to just kind of a little bit
of break.
