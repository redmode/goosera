1
00:00:00,000 --> 00:00:08,097
Hi. Welcome back. We have had time and
opportunity to do many different problems

2
00:00:08,097 --> 00:00:14,060
and I have taken the time to go really
deep in to them and I have spent a lot of

3
00:00:14,060 --> 00:00:20,285
time today with you because I believe the
value add in online is the explanation one

4
00:00:20,285 --> 00:00:25,633
on one. And today is a lot of real world
applications which will help you further

5
00:00:25,633 --> 00:00:31,022
in the class so just bear with me for
little time. We'll do one interesting

6
00:00:31,022 --> 00:00:36,058
problem but before we go there, let's do
my favorite financial concept. And my

7
00:00:36,058 --> 00:00:42,059
favorite financial concept is called a
perpetuity. A perpetuity is simply

8
00:00:42,059 --> 00:00:52,480
something that pays you C dollars, roughly
the same amount PMT which goes on forever,

9
00:00:52,480 --> 00:00:59,522
right? It's pretty remarkable so what do
you get? You get CCCCC forever and it

10
00:00:59,522 --> 00:01:06,509
could be with or without growth. So when I
saw this the first time, I said this is

11
00:01:06,509 --> 00:01:12,375
nerdy dumb to the extreme. This is a
textbook idea. When am I going to ever see

12
00:01:12,375 --> 00:01:18,150
something like that, right? So let me just
first of all give you, just think of some

13
00:01:18,150 --> 00:01:23,990
examples of perpetuities, right? So there
are bonds out there. There is something

14
00:01:23,990 --> 00:01:30,855
that pays you say, one pound for a long
period of time so that's one example. The

15
00:01:30,855 --> 00:01:37,652
second example which is much more
complicated but much more intuitive is

16
00:01:37,652 --> 00:01:45,430
what is called a stock, a promise as
opposed to a bond. I promise we'll spend

17
00:01:45,430 --> 00:01:52,934
couple of weeks on stocks and bonds but I
wanted to raise this, what's the basic

18
00:01:52,934 --> 00:02:04,202
difference between the standard and bond?
This is limited inn majority and this

19
00:02:04,202 --> 00:02:15,989
hopefully goes on forever. So, for example
when John Ford started the company, he

20
00:02:15,989 --> 00:02:22,172
didn't say we will be there only for five
years and I'll pay you some money and then

21
00:02:22,172 --> 00:02:27,892
we are gone. That's not a company, that's
not an idea. Great ideas last forever.

22
00:02:27,892 --> 00:02:33,986
Great things last forever. What I want you
to recognize in all of this is that

23
00:02:33,986 --> 00:02:39,574
forever actually, doesn't mean forever.
What do I mean by that? So take the

24
00:02:39,574 --> 00:02:44,477
example and show you the power of
perpetuities. Now let me ask you this,

25
00:02:44,477 --> 00:02:52,026
suppose I gave you something, suppose I
gave you something that paid $ten forever.

26
00:02:52,026 --> 00:02:58,544
Guess what the power of perpetuity is. At
time zero guess what the Pv is? We won't

27
00:02:58,544 --> 00:03:04,349
derive this. I'm going to give you this
one formula. When we have time towards the

28
00:03:04,349 --> 00:03:10,938
end, I'll do stuff that is kind of nerdy
and, you know interesting for derivations

29
00:03:10,938 --> 00:03:17,115
if you have the time but I'll tell you
that the terms are to be C/R. So the

30
00:03:17,115 --> 00:03:23,645
simplest formula in the world is the
formula for perpetuity. Which is the most

31
00:03:23,645 --> 00:03:30,249
complex thing to comprehend. So what could
this perpetuity be? It's a perpetuity that

32
00:03:30,249 --> 00:03:37,393
lasts. It's like a stock that pays you
$ten every year and is expected a company

33
00:03:37,393 --> 00:03:43,535
that's expected to survive for the
reasonable future. So turns out, what will

34
00:03:43,535 --> 00:03:49,827
be the answer for this? Suppose the
interest rate is ten%. This would be

35
00:03:49,827 --> 00:03:59,062
$10/.1 is $100. Okay? So, what could be
simpler, right? And would you like this

36
00:03:59,062 --> 00:04:06,098
$ten to grow over time? Sure. If it's
growing over time at the rate of G, is the

37
00:04:06,098 --> 00:04:13,349
growth rate. So suppose the growth rate is
ten%. So in the first year, how much is

38
00:04:13,349 --> 00:04:19,816
this paying? $Ten. In the second year it's
paying 1.1 and so on by the way, these

39
00:04:19,816 --> 00:04:25,756
things in real life are called growth
stocks. So something like what Microsoft

40
00:04:25,756 --> 00:04:31,328
was in the beginning or Google, or
technology firms that are successful. They

41
00:04:31,328 --> 00:04:36,551
grow and pay out over time. Their growth
rate is extremely high. What does the

42
00:04:36,551 --> 00:04:44,820
formula become? C/r - G. Right? So because
this growth rate is ten percent I can't

43
00:04:44,820 --> 00:04:52,215
use this formula so lets make this growth
rate five percent so what is R? Ten%. What

44
00:04:52,215 --> 00:04:59,944
is G? Five percent and what is C? $Ten.
Again, very simple to calculate. The

45
00:04:59,944 --> 00:05:05,186
reason I'm introducing this now is not so
much to start doing a lot of examples. I

46
00:05:05,186 --> 00:05:10,273
just want to introduce it now because its
a linear process. We went from annuity,

47
00:05:10,273 --> 00:05:15,481
which ends after certain interval. You
take loans and you pay them off. Then you

48
00:05:15,481 --> 00:05:20,472
think of another concept that goes on
forever and we'll come back to these

49
00:05:20,472 --> 00:05:25,563
stocks. And as I showed you right now, if
the growth rate is ten percent and your,

50
00:05:25,563 --> 00:05:30,419
your interest rate is also ten%, you can't
use this formula. That doesn't mean you

51
00:05:30,419 --> 00:05:35,572
can't calculate the number. You have to
the long route, long way. That emphasizes

52
00:05:35,572 --> 00:05:41,000
one more important issue. Don't use form
ulas blindly. Formulas are at your

53
00:05:41,000 --> 00:05:46,058
disposal not the other way around, okay.
So, we'll come back to perpetuities in the

54
00:05:46,058 --> 00:05:52,029
future but for now I would say just keep
it at the back of your mind that there is

55
00:05:52,029 --> 00:05:57,086
something real world that looks like a
perpetuity and it's very common it's what

56
00:05:57,086 --> 00:06:03,068
called the share in the company or a stock
of a company because it, we will last for

57
00:06:03,068 --> 00:06:09,062
a very long period of time and in the real
world, what happens is you don't know how

58
00:06:09,062 --> 00:06:15,409
to value things beyond say ten years or 30
years, it's just too far because the world

59
00:06:15,409 --> 00:06:20,431
is too uncertain. Such formula, C/R - G
are really useful in approximating what

60
00:06:20,431 --> 00:06:26,029
you think is going to happen. There's no
point getting too refined after five or

61
00:06:26,029 --> 00:06:31,068
six years because it's about the future.
So actually these formulas, when I saw

62
00:06:31,068 --> 00:06:36,032
first time, I said what are they talking
about are the most useful ones in real

63
00:06:36,032 --> 00:06:43,039
life because you want to get a sense of
value. You don't want to get it so precise

64
00:06:43,039 --> 00:06:49,099
when you know it's wrong, right? So
approximate formulas like C/R - G are

65
00:06:49,099 --> 00:06:57,038
actually so useful in grabbing the basics
of what finance is trying to offer, okay.

66
00:06:57,038 --> 00:07:04,060
As promised I am going to spend a little
more time today and this time is not on

67
00:07:04,060 --> 00:07:09,087
the next three slides. The next three
slides that you see here, don't worry

68
00:07:09,087 --> 00:07:15,017
about them. I'm just emphasizing them to
remind myself, and to remind you, that if

69
00:07:15,017 --> 00:07:20,617
you are oriented towards formulas which I
would encourage you to be because formulas

70
00:07:20,617 --> 00:07:25,793
reflect in the end of your understanding
of what's going on, not the other way

71
00:07:25,793 --> 00:07:31,405
around. I would encourage you to walk
through these formulas and as I said I'm

72
00:07:31,405 --> 00:07:37,077
not going to spend the time for you to
read them. This is one part of this

73
00:07:37,077 --> 00:07:43,073
overhead slides that are used or visuals
that I will provide you as a resource. As

74
00:07:43,073 --> 00:07:49,018
I said, I want the main resource to be the
videos but I am providing you resources

75
00:07:49,018 --> 00:07:54,044
like the course syllabus with chapters
from various books, written by wonderful

76
00:07:54,044 --> 00:08:00,530
people. I also want you to learn from them
not just from me. I don't have the control

77
00:08:00,530 --> 00:08:06,038
on learning. Your learning is, you are in
charge but I'll give you some formulas so

78
00:08:06,038 --> 00:08:12,075
that we can go back and confirm your
knowledge. So I just wanted to remind you

79
00:08:12,075 --> 00:08:17,343
and me, we'll do that. That will put
everything together but I want to end

80
00:08:17,343 --> 00:08:23,414
today's class by doing a problem with you
and I would say I am going to read this

81
00:08:23,414 --> 00:08:29,456
problem first. Could you try to understand
the context and then I encourage you to

82
00:08:29,456 --> 00:08:36,026
take a break. And hopefully you have taken
several breaks over different days during

83
00:08:36,026 --> 00:08:41,882
this content, because I am committed in
week two to make you understand why we do

84
00:08:41,882 --> 00:08:47,868
things the way we do, and the reason is we
can learn a lot in ten weeks and I have

85
00:08:47,868 --> 00:08:53,861
taken a lot of time today, simply to make
you understand how real things are even

86
00:08:53,861 --> 00:08:59,278
though they are problems written on a
little spreadsheets or in a little

87
00:08:59,278 --> 00:09:05,232
Powerpoint so lets go through it. You are
30 years old. You believe you'll be able

88
00:09:05,232 --> 00:09:09,166
to save for the next twenty years until
you're 50 and why am I saying that?

89
00:09:09,166 --> 00:09:14,074
Typically, that's when a lot of people
earn money and save but after that, for

90
00:09:14,074 --> 00:09:19,305
ten years. You'll, until your retirement
at 60 you will have a spike in your, I

91
00:09:19,305 --> 00:09:25,321
shouldn't even call it a spike in your
expenses, many spikes in your expenses so

92
00:09:25,321 --> 00:09:31,485
that you will not be in a position to save
for the next ten years. So remember what's

93
00:09:31,485 --> 00:09:37,047
going on, starting at 30, 50, and then
under a break at 60. These are artificial

94
00:09:37,047 --> 00:09:42,566
but believe me, very useful points in your
life. After 60 what are you going to do?

95
00:09:42,566 --> 00:09:48,567
You want to retire but you want to be able
to live, right? And you want to be able to

96
00:09:48,567 --> 00:09:54,151
live at the standard of living which in
this problem is pretty high. Because most

97
00:09:54,151 --> 00:09:59,826
people in the world cannot afford even
one-tenth of this or 1/20 but that's, I

98
00:09:59,826 --> 00:10:05,486
want big round numbers so that I don't
have to deal with decimals and I'm not

99
00:10:05,486 --> 00:10:11,057
trying to make a statement about what my
expectations or yours are. So you'll

100
00:10:11,057 --> 00:10:17,156
retire at 60 and then you expect to live
until 80 and you want to take care of the

101
00:10:17,156 --> 00:10:22,701
next twenty years at the rate of $100,000,
right? You want to but now, now what is

102
00:10:22,701 --> 00:10:28,965
happening now? You were saving between 30
and 50 so that you could do this between

103
00:10:28,965 --> 00:10:34,544
60 and 80. And the fact that both are
twenty years is just an artifact of the

104
00:10:34,544 --> 00:10:40,835
example. Who controls all of this? You do.
Who controls now, the next decision? What

105
00:10:40,835 --> 00:10:46,817
interest rate will you earn on your
savings will depend on you. It'll depend

106
00:10:46,817 --> 00:10:53,615
on what type of risks you're willing to
take and for convenience or for just fun,

107
00:10:53,615 --> 00:10:59,483
I'm assuming that you are a person
inclined to invest in risky stuff. And

108
00:10:59,483 --> 00:11:05,536
therefore you will be rewarded on average
in the long run at eight%. So, this is the

109
00:11:05,536 --> 00:11:13,609
nature of the beast. This is the problem.
I'm encouraging you now to do two things.

110
00:11:13,609 --> 00:11:21,324
Think about it for about five to ten
minutes and do the most important thing in

111
00:11:21,324 --> 00:11:27,951
life which is more important than even
finance, I can't believe I just said that

112
00:11:27,951 --> 00:11:34,763
but draw a timeline and put your problem
on that timeline. If you can do that, we

113
00:11:34,763 --> 00:11:40,325
can do this problem in five minutes. If
you can't do that, remember it's not the

114
00:11:40,325 --> 00:11:46,160
problem of finance. Finance is going to
help you not hurt you. It's because common

115
00:11:46,160 --> 00:11:51,990
sense is not that common. [laugh] Is,
finance is full of common sense but the

116
00:11:51,990 --> 00:11:57,330
word common sense is a, is a wrong
expression. I've found whenever I look at

117
00:11:57,330 --> 00:12:03,046
common sense, it's pretty complicated.
That's why after a while it becomes common

118
00:12:03,046 --> 00:12:08,410
to you but finance is only going to help
you. So do that, I'll come back in about

119
00:12:08,410 --> 00:12:14,061
five minutes and we'll do this problem
together and that will be the end of today

120
00:12:14,061 --> 00:12:20,111
and I promise you this will enable you to
do the assignment and crank up the heat in

121
00:12:20,111 --> 00:12:25,520
the assignment. I want this week to be
intense for you and the purpose fully

122
00:12:25,520 --> 00:12:30,447
served. And the reason is we can go very
far, we can go very far not in the

123
00:12:30,447 --> 00:12:35,106
mechanics but in the understanding of the
world, okay. So break for five to ten

124
00:12:35,113 --> 00:12:38,052
minutes and I'll come back and do the
problem.
