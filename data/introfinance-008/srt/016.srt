1
00:00:00,000 --> 00:00:05,510
I hope you have taken a break and, you
know got familiar with what we are doing.

2
00:00:05,510 --> 00:00:11,565
As I said, today's going to be intense
because I want you very early on in this

3
00:00:11,565 --> 00:00:17,438
class to really get a feel for finance. I
mean to me the beauty of finance as I keep

4
00:00:17,438 --> 00:00:23,589
saying so is not the formula. It makes so
much sense and once you get the hang of

5
00:00:23,589 --> 00:00:30,226
why we are using an Excel and why we are
doing the formula, you will get a sense of

6
00:00:30,226 --> 00:00:36,298
that. It will be very, very easy for you
to recognize the value of each. I have

7
00:00:36,298 --> 00:00:41,043
nothing against Excel but here's a simple
reason we use Excel, is because we don't

8
00:00:41,043 --> 00:00:45,714
have time to calculate all these numbers.
We know how to do it but we don't have the

9
00:00:45,714 --> 00:00:50,605
time and it would be a pretty bizarre
thing to calculate numbers for the heck of

10
00:00:50,605 --> 00:00:55,915
it. The second reason is, these cash flows
in this problem are fixed. They could

11
00:00:55,915 --> 00:01:02,204
change. That's what life is and we'll do
that later in the class but two aspects

12
00:01:02,204 --> 00:01:08,667
about it. Most examples of loans or
retirement funds are very realistic in

13
00:01:08,667 --> 00:01:15,015
this respect too i.e. The C is fixed.
People choose to operate like that whether

14
00:01:15,015 --> 00:01:20,132
its convenience whatever and loans many
times are fixed rate loans. So, what I

15
00:01:20,132 --> 00:01:25,196
would encourage you to do is recognize
that these are simplifications at some

16
00:01:25,196 --> 00:01:30,553
level, the C being the fixed number but at
the same time C, its not a simplification

17
00:01:30,553 --> 00:01:35,458
of real life and we'll get to more
complicated things. Why am I bringing this

18
00:01:35,458 --> 00:01:41,113
up? Because if you didn't have
compounding, we will need Excel. And if we

19
00:01:41,113 --> 00:01:47,166
didn't have things changing over time, we
wouldn't need Excel. So, Excel is awesome

20
00:01:47,166 --> 00:01:52,333
but keep it where it is. It's, it's not
controlling you. You control it. Okay,

21
00:01:52,333 --> 00:01:57,899
let's move on to the second phase of an
annuity which is present value. So again

22
00:01:57,899 --> 00:02:02,972
I've, there's this chart. I would like you
to look at it for a second. Same thing,

23
00:02:02,972 --> 00:02:08,586
just to make life simple, what I'm going
to do is I'm going to stick with the same

24
00:02:08,586 --> 00:02:14,026
example of three years. And by the way as
I'm doing this, I want to thank, instead

25
00:02:14,026 --> 00:02:19,184
of just thanking oneself, I want to
multiple times thanks to my colleagues. I

26
00:02:19,184 --> 00:02:25,507
went to the University of Chicago for my
PhD and before that I've started a lot. I

27
00:02:25,507 --> 00:02:31,778
have, to say thanks to so many people for
showing me the beauty of finance. I also

28
00:02:31,778 --> 00:02:37,196
want to thank my colleagues at the
University of Michigan, Ross School of

29
00:02:37,196 --> 00:02:43,449
Business. Ronen Israel is one of them who
with me taught. This introductory class

30
00:02:43,449 --> 00:02:49,969
many, many years ago. And he has been a
big influence in how I think and then a

31
00:02:49,969 --> 00:02:57,671
lot of other colleagues like who's a great
teacher. I want to thank all these guys

32
00:02:57,671 --> 00:03:03,070
for letting me become who I am and if I'm
worth anything, it's due to other people,

33
00:03:03,070 --> 00:03:09,047
not due to me. Okay, so let's get started.
The first cash flow at year 00 and not

34
00:03:09,047 --> 00:03:14,045
because it's supposed to be, it's
convention. And in this case remember,

35
00:03:14,045 --> 00:03:20,022
because it's present value, I'm standing
today, I'm doing the opposite of future

36
00:03:20,022 --> 00:03:26,848
value. So I'm saying not years to the end,
years to discount. And the word discount

37
00:03:26,848 --> 00:03:33,530
is coming from a very simple reason and
the R > zero. Because interest rates

38
00:03:33,530 --> 00:03:38,518
are positive, future value grows but
because interest rates are positive, the

39
00:03:38,518 --> 00:03:44,288
future is discounted when you bring it
back today, right? So how much? Zero years

40
00:03:44,288 --> 00:03:50,803
of discounting because we are standing
today and present value today is zero. But

41
00:03:50,803 --> 00:03:56,005
the present value turns out to be zero.
Why? That's simply because there's no cash

42
00:03:56,005 --> 00:04:01,151
flow here. If there were, it would be
exactly the same number, because of no

43
00:04:01,151 --> 00:04:06,450
years to discounting. So in some sense,
years to discounting is a key variable.

44
00:04:06,450 --> 00:04:11,415
Now here, things are a little bit simpler.
One, the first C is one year of A, two,

45
00:04:11,415 --> 00:04:16,613
three. So because we have done this table
before, I'm not going to spend too much

46
00:04:16,613 --> 00:04:21,723
time on it. However, recognize that we are
doing the exact opposite of what we did

47
00:04:21,723 --> 00:04:26,618
last time and the reason we are doing
present value after future value is in my

48
00:04:26,618 --> 00:04:33,147
book, if you understand future value You
understand compounding. And then when you

49
00:04:33,147 --> 00:04:39,664
come backwards, you're not torn away by
why you're dividing one + R, (one + R)^2.

50
00:04:39,957 --> 00:04:47,238
So let's do it. This is C / (one + R) and
the neat thing is we have done this last

51
00:04:47,238 --> 00:04:59,590
time, only thing is we have to do it thre
e times, C. Why am I putting one + R each

52
00:04:59,590 --> 00:05:09,623
time in parentheses? Simply because it's
the one + R is the factor, not one outside

53
00:05:09,623 --> 00:05:16,363
or R outside anything like that. One + R
is the factor and the reason it's getting

54
00:05:16,363 --> 00:05:22,788
squared and cubed is because pause again,
compounding, right? So, so once you

55
00:05:22,788 --> 00:05:28,806
recognize this aspect of it, I want you to
bear with me for a second and what I'm

56
00:05:28,806 --> 00:05:34,660
going to do is I'm going to go to another
page. Where I actually write out the

57
00:05:34,660 --> 00:05:40,076
formula and again I'm not going to try to
simplify it. Simplifications can be done

58
00:05:40,076 --> 00:05:44,824
very easily by you and in some sense, it's
useful to do it. So let me write out the

59
00:05:44,824 --> 00:05:52,844
present value formula. Present value
formula for a three year annuity and I use

60
00:05:52,844 --> 00:06:03,240
this three just to remind me that it's
just three years, is what? C / (one + R

61
00:06:03,240 --> 00:06:14,383
),, + C / (one + R)^2 + C / (one + R)^3,
right? So, I've just to do three PVs. Now

62
00:06:14,383 --> 00:06:18,903
remember, these PVs are not easy to do,
right? So that's why I'll, at some point

63
00:06:18,903 --> 00:06:24,952
use a calculator and remember, I can
replace this by PMT in my head. In

64
00:06:24,952 --> 00:06:32,200
textbooks we don't use PMTs, we use C
because C is a generic word for cash. The

65
00:06:32,200 --> 00:06:37,420
other thing is Cs are fixed and that's
because of the nature of the Bs that we

66
00:06:37,420 --> 00:06:50,948
are dealing with right now, okay? Now let
me show you that actually I can take C out

67
00:06:50,948 --> 00:07:01,394
and have. Right? Pretty straightforward?
So what is this? If you think about it a

68
00:07:01,394 --> 00:07:07,855
little bit, this is a factor again. And
it's a present value factor of an annuity

69
00:07:07,855 --> 00:07:14,528
of what? What is the annuity? $One. So if
you know the present, if you know this guy

70
00:07:14,528 --> 00:07:23,835
for which what do you need to know? R and
n. How many years and what's the interest

71
00:07:23,835 --> 00:07:30,149
rate? If you know the value of $one payed
three times, you can know the value of C

72
00:07:30,149 --> 00:07:36,545
bucks whether it's half a buck or it's
$100. That's the beauty of it. How does

73
00:07:36,545 --> 00:07:48,437
this formula change if you go to Pv of n?
Simple, one thing changes. This, there's a

74
00:07:48,437 --> 00:07:58,768
bunch of dots + one / (one + R)^n. So
you'll keep going until you arrive at the

75
00:07:58,768 --> 00:08:05,177
end, right? I hope this clear. Again, why
am I doing this? I first did the concept

76
00:08:05,177 --> 00:08:10,790
then the formula but I'm really, really
interested now in doing problems. And

77
00:08:10,790 --> 00:08:17,014
again I repeat, if you need to pause now,
it's good to do it because I do not want

78
00:08:17,014 --> 00:08:26,068
you to get overwhelmed by formulas and so
on, okay? So let's go on and I'm going to

79
00:08:26,068 --> 00:08:34,227
even pause for a second to remind you, you
can, you don't have to keep watching the

80
00:08:34,227 --> 00:08:39,024
video. Take a break and let's do one
problem at a time.
