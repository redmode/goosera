1
00:00:03,710 --> 00:00:08,860
Welcome, I hope you've had a good summer.
I just wanted to talk about the class.

2
00:00:08,860 --> 00:00:13,702
Since there is about a week or so left
before it starts, believe it or not.

3
00:00:13,702 --> 00:00:20,330
I'll, I have put something on the sa, on
the web and it's a detailed syllabus

4
00:00:20,330 --> 00:00:24,590
for the class and I would encourage you to
take the opportunity to read it.

5
00:00:24,590 --> 00:00:29,030
It's a big document simply because I want
to give you a clear sense

6
00:00:29,030 --> 00:00:30,140
of what you're going to get into.

7
00:00:31,220 --> 00:00:35,060
And you want to make a decision whether
this class is good for you or not.

8
00:00:35,060 --> 00:00:38,240
And even though you have registered, I'd
encourage you to read it.

9
00:00:38,240 --> 00:00:40,430
It gives you a good sense.

10
00:00:40,430 --> 00:00:42,550
But what I'm going to do in the video is I
thought

11
00:00:42,550 --> 00:00:47,310
I'd give you a visual idea and talk to you
about the class.

12
00:00:47,310 --> 00:00:49,980
And it reflects what's in the syllabus.

13
00:00:49,980 --> 00:00:54,210
But hopefully easier to follow and
motivate you to go

14
00:00:54,210 --> 00:00:55,180
read the syllabus.

15
00:00:56,180 --> 00:01:02,450
The first main thing I'll talk about, are
the objectives of the class, and to me,

16
00:01:02,450 --> 00:01:08,450
the main objective, the first one, is to
learn how to value assets.

17
00:01:08,450 --> 00:01:13,900
Assets are what create value in the world.
And if you know which is a better asset

18
00:01:13,900 --> 00:01:19,240
than the other in terms of creating value
for the world I think you're on board.

19
00:01:19,240 --> 00:01:22,240
You're going to make good decisions.

20
00:01:22,240 --> 00:01:27,650
And that's the second, if you may,
practical purpose of this class.

21
00:01:28,700 --> 00:01:31,360
To understand value, to make good
decisions.

22
00:01:31,360 --> 00:01:33,620
And if you can make good decisions it kind
of

23
00:01:33,620 --> 00:01:37,840
empowers you to feel comfortable in a very
uncertain world.

24
00:01:37,840 --> 00:01:41,620
You're not guaranteed to make great
decisions.

25
00:01:41,620 --> 00:01:45,000
But you will probabilistically make better
decisions and

26
00:01:45,000 --> 00:01:51,770
that's what you want.
I want you to understand your environment

27
00:01:51,770 --> 00:01:55,480
and what the public policy's all about.
Why's that important?

28
00:01:55,480 --> 00:02:01,120
Because, as I said, finance's beauty is
about valuing

29
00:02:01,120 --> 00:02:07,120
assets but making real life decisions.
And all of this happens within a context.

30
00:02:07,120 --> 00:02:10,760
And in fact the context is an outcome of
our actions.

31
00:02:10,760 --> 00:02:13,890
So if you understand the interaction
between decisions

32
00:02:13,890 --> 00:02:17,300
and what's happening and public policy,
it'll help you.

33
00:02:17,300 --> 00:02:19,590
Now, will it be a perfect understanding?

34
00:02:19,590 --> 00:02:23,380
No, but I guarantee you, when you read the
popular

35
00:02:23,380 --> 00:02:27,450
press, you'll be able to understand things
a bit better.

36
00:02:27,450 --> 00:02:31,610
And I would encourage you to read
critically, not just like a consumer.

37
00:02:33,180 --> 00:02:36,500
Finally, I hope this class

38
00:02:36,500 --> 00:02:43,520
motivates you to learn.
Learning is a very powerful thing in life.

39
00:02:43,520 --> 00:02:45,680
And I, I, I'll talk about it a little bit

40
00:02:45,680 --> 00:02:51,010
later more in depth, but when you learn
more, learn finance.

41
00:02:51,010 --> 00:02:53,050
And the reason is, if you know finance,
you pretty

42
00:02:53,050 --> 00:02:56,700
much know most of anything you want to
know in life.

43
00:02:56,700 --> 00:02:59,840
And I'm kind of serious about it, you
know, that's, that's the funny part.

44
00:03:00,880 --> 00:03:02,330
So objectives,

45
00:03:02,330 --> 00:03:03,930
why you want to do.

46
00:03:03,930 --> 00:03:06,830
Think about it and I hope you're convinced
that

47
00:03:06,830 --> 00:03:08,860
this is something that you really need to
learn.

48
00:03:10,770 --> 00:03:12,650
What are the prerequisites?

49
00:03:12,650 --> 00:03:15,750
I've been receiving emails about
prerequisites.

50
00:03:15,750 --> 00:03:18,690
This is a question that we always get even
in live

51
00:03:18,690 --> 00:03:23,260
classes and this is a very tough for me
because fundamentally.

52
00:03:23,260 --> 00:03:26,370
It could be everything in life or nothing.

53
00:03:26,370 --> 00:03:27,960
Some, and prerequisites

54
00:03:27,960 --> 00:03:30,680
could be nothing or everything.
Why am I saying that?

55
00:03:30,680 --> 00:03:34,160
It's because if you go into the detail or
development of

56
00:03:34,160 --> 00:03:40,100
finance, the first thing that's at the
basis of finance is Economics.

57
00:03:40,100 --> 00:03:44,500
And economics is actually called the
mother discipline of finance.

58
00:03:44,500 --> 00:03:47,432
It is borrowed mostly from that
discipline.

59
00:03:47,432 --> 00:03:50,538
So I would encourage you to know that.

60
00:03:50,538 --> 00:03:53,490
Behavioral science is also becoming

61
00:03:53,490 --> 00:03:57,900
important over time, but economics remains
the basic paradigm.

62
00:03:59,440 --> 00:04:02,950
Accounting, I must admit, accounting
though I

63
00:04:02,950 --> 00:04:05,360
know some of it, is kind of boring.

64
00:04:05,360 --> 00:04:06,440
And.

65
00:04:06,440 --> 00:04:07,540
But it's a language.

66
00:04:07,540 --> 00:04:10,326
I mean it's like, if you didn't understand
me,

67
00:04:10,326 --> 00:04:14,200
because I'm speaking in English, there
would be a problem.

68
00:04:14,200 --> 00:04:16,100
Similarly, if you don't understand

69
00:04:16,100 --> 00:04:18,650
accounting, you cannot understand what's
going

70
00:04:18,650 --> 00:04:22,460
on, and because it's imperfect, like every

71
00:04:22,460 --> 00:04:24,520
other language, you really have to know
it.

72
00:04:24,520 --> 00:04:27,880
I promise you I will give you hints of
what to do.

73
00:04:27,880 --> 00:04:30,140
When we do accounting, because we can't do
all of it.

74
00:04:31,760 --> 00:04:36,760
One last thing about prerequisites,
algebra and statistics.

75
00:04:38,445 --> 00:04:41,870
Are other formal subject matters that we
need

76
00:04:41,870 --> 00:04:44,250
and I think you need this for everything

77
00:04:44,250 --> 00:04:48,930
with so much data available on anything
under the sun.

78
00:04:48,930 --> 00:04:55,070
Knowing algebra and statistics is very
important and I hope my treatment

79
00:04:55,070 --> 00:04:58,560
of the subject matter will motivate you to
learn a lot more.

80
00:04:58,560 --> 00:05:03,980
Rather than seeing it as a hurdle to
understanding things.

81
00:05:03,980 --> 00:05:08,330
So these are the formal things you could
think about but don't worry about it.

82
00:05:08,330 --> 00:05:10,130
As I said if

83
00:05:10,130 --> 00:05:14,380
you have curiosity and bad attitude and a
bad attitude is

84
00:05:14,380 --> 00:05:19,070
one which says, you know, give me
anything, I'll handle it.

85
00:05:19,070 --> 00:05:22,340
If you have that attitude you will do very

86
00:05:22,340 --> 00:05:24,630
well in this class, and you'll learn a
ton.

87
00:05:24,630 --> 00:05:26,890
Not just in this class, in life.

88
00:05:26,890 --> 00:05:29,300
And so bring that attitude and we'll be
fine.

89
00:05:30,530 --> 00:05:34,590
Now I'll talk about something that's very
important, and it's called pedagogy.

90
00:05:34,590 --> 00:05:35,570
How do conduct

91
00:05:35,570 --> 00:05:36,070
the class?

92
00:05:37,280 --> 00:05:40,630
This class, because of the value of
finance being

93
00:05:40,630 --> 00:05:47,090
both theory, framework and practical, is
all driven by problem-solving.

94
00:05:47,090 --> 00:05:51,280
I will always force you to do problems,
because that's how you learn.

95
00:05:52,420 --> 00:05:53,270
What does that mean?

96
00:05:54,390 --> 00:06:00,650
Everything I introduce in class will not
be proceeded by a formula,

97
00:06:00,650 --> 00:06:04,580
finance has lot of algebra, lot of
formulas, but I will always

98
00:06:04,580 --> 00:06:09,580
tell you why we are doing something, not
what it's all about.

99
00:06:09,580 --> 00:06:12,620
Why is far more important to life then the
what.

100
00:06:12,620 --> 00:06:14,280
The what you can Google, right.

101
00:06:14,280 --> 00:06:17,090
So if you can Google and get what I

102
00:06:17,090 --> 00:06:20,940
providing you in the class I'm not
providing you much.

103
00:06:20,940 --> 00:06:26,090
So, because finance is applied, we'll
emphasize problem solving, and everything

104
00:06:26,090 --> 00:06:28,750
will be introduced with an example.

105
00:06:28,750 --> 00:06:34,980
However, I want you to recognize, that you
have to do, most of the work.

106
00:06:34,980 --> 00:06:38,040
I am a coach.
You have to do the work.

107
00:06:38,040 --> 00:06:41,540
I hope I'm a good coach, and I motivate
you

108
00:06:41,540 --> 00:06:44,590
and give you the main issues you need to
worry about.

109
00:06:44,590 --> 00:06:44,940
Right?

110
00:06:44,940 --> 00:06:46,890
In life there are not that many issues.

111
00:06:46,890 --> 00:06:51,880
Four or five, one is love the rest is,
kind of follows a little bit.

112
00:06:51,880 --> 00:06:54,080
Number two is finance, in my book, but
anyway.

113
00:06:54,080 --> 00:06:58,720
So, you'll be doing assignments every
week, and I encourage you.

114
00:06:58,720 --> 00:07:03,220
Whether they are called quizzes or not,
quizzes or assignments, do them.

115
00:07:03,220 --> 00:07:05,940
You will have one per week and a final.

116
00:07:05,940 --> 00:07:11,690
You'll have deadlines for these like in a
real class, and you must do these.

117
00:07:11,690 --> 00:07:14,240
Apart from getting a certificate, you'll
learn.

118
00:07:16,840 --> 00:07:20,850
Finally, I want you to know how I've
structured the class.

119
00:07:20,850 --> 00:07:25,330
I think this is true of most Coursera
classes, it's highly

120
00:07:25,330 --> 00:07:31,590
recommended to mimic the real world, or
face to face classes on some dimension.

121
00:07:31,590 --> 00:07:37,450
So one of the dimensions is, I will
release material one week at a time.

122
00:07:37,450 --> 00:07:39,405
You will not be able to get into the ten

123
00:07:39,405 --> 00:07:41,930
week class and do the whole class in a
week.

124
00:07:41,930 --> 00:07:44,420
Why am I doing that?
For two reasons.

125
00:07:44,420 --> 00:07:50,580
One, I genuinely believe that you need
time to understand new stuff.

126
00:07:50,580 --> 00:07:53,850
And practice assignments, problems within
the videos.

127
00:07:55,310 --> 00:07:58,730
And if you understand the stuff, you're
better off.

128
00:07:58,730 --> 00:08:00,310
You can go to the next step.

129
00:08:00,310 --> 00:08:03,170
Luckily, finance builds on stuff.

130
00:08:03,170 --> 00:08:06,470
So you have to know the first week to know
the second week, and so on.

131
00:08:07,678 --> 00:08:09,560
Well, the second reason is probably more

132
00:08:09,560 --> 00:08:12,690
important of releasing one stuff one week
at

133
00:08:12,690 --> 00:08:16,940
a time, so synchronous I mean not at a
point in time but for a week.

134
00:08:16,940 --> 00:08:21,540
Second reason is, that learning happens
from each other, so when I do

135
00:08:21,540 --> 00:08:26,150
these classes live, all assignments are in
a group and they evaluate each other.

136
00:08:26,150 --> 00:08:28,090
Not everybody gets the same grade.

137
00:08:28,090 --> 00:08:32,470
So this I can't mimic online.
But you can.

138
00:08:32,470 --> 00:08:38,390
Join the forums and learn from each other.
In fact, I believe you'll learn far more

139
00:08:38,390 --> 00:08:43,510
that way than you'll learn from my videos
and I actually hope that's the case.

140
00:08:46,020 --> 00:08:51,240
Now, content and materials is important.
The main.

141
00:08:51,240 --> 00:08:54,940
Material you will get from me is the
video.

142
00:08:54,940 --> 00:08:58,410
For every week, they'll be approximately
two week,

143
00:08:58,410 --> 00:09:02,930
two hours worth of video, broken up into
pieces.

144
00:09:02,930 --> 00:09:08,440
Because finance is so applied, you'll have
many opportunities

145
00:09:08,440 --> 00:09:10,720
to pause the videos, whether I ask you to

146
00:09:10,720 --> 00:09:13,380
pause or not.
This is very important.

147
00:09:13,380 --> 00:09:17,080
Every time I put up a problem, and you're
reading it.

148
00:09:17,080 --> 00:09:22,110
Please recognize that this is an
opportunity for you to learn the stuff by

149
00:09:22,110 --> 00:09:26,960
doing the problem first, and then starting
the video and doing it together with me.

150
00:09:28,050 --> 00:09:31,340
This is my main advice to you.

151
00:09:31,340 --> 00:09:35,090
Take every opportunity to do the problems
in the videos.

152
00:09:35,090 --> 00:09:35,870
So you'll have lot

153
00:09:35,870 --> 00:09:38,790
of testing, self testing opportunities.

154
00:09:38,790 --> 00:09:42,150
Having said that, video is the main
resource.

155
00:09:42,150 --> 00:09:43,590
What do I mean by that.

156
00:09:43,590 --> 00:09:47,460
I'm not going to give you PowerPoint
versions of the video.

157
00:09:47,460 --> 00:09:53,910
To me PowerPoint is great.
But it also makes you go to sleep.

158
00:09:53,910 --> 00:09:56,910
Online learning has huge advantages.

159
00:09:56,910 --> 00:10:00,670
You can make your own class notes and I
encourage you to,

160
00:10:00,670 --> 00:10:04,190
from the video.
But be an active listener not.

161
00:10:04,190 --> 00:10:07,922
Lay, sitting back, having a coffee, and
you can do that too.

162
00:10:07,922 --> 00:10:08,242
[LAUGH]

163
00:10:08,242 --> 00:10:11,129
If you want to, of course, I can't
possibly watch.

164
00:10:11,129 --> 00:10:14,200
But I would encourage you to make, be an
active

165
00:10:14,200 --> 00:10:19,400
participants in the video and, hopefully
if I'm interesting enough, right?

166
00:10:20,600 --> 00:10:24,410
But I will provide you very few notes and
these are

167
00:10:24,410 --> 00:10:28,190
largely geared toward stuff that you need
to know at one place.

168
00:10:28,190 --> 00:10:31,270
So two or three of these, review of
statistics.

169
00:10:31,270 --> 00:10:33,430
Some notes on how to use Excel or

170
00:10:33,430 --> 00:10:38,520
a Financial Calculator, and some formulas
put together, too.

171
00:10:38,520 --> 00:10:42,590
But I must emphasize, don't go by the how
to do things.

172
00:10:42,590 --> 00:10:45,630
First understand what the heck is going on
and why.

173
00:10:48,340 --> 00:10:52,520
Another resource I'll give you, is there
are many textbooks written on finance.

174
00:10:52,520 --> 00:10:55,850
Finance took off in the late 50s and for
the

175
00:10:55,850 --> 00:11:02,390
next 30 years there were lot of really
great breakthroughs.

176
00:11:02,390 --> 00:11:05,030
And those breakthroughs formed the
fundamentals.

177
00:11:05,030 --> 00:11:08,130
And great textbooks are available and I'll
give you some.

178
00:11:08,130 --> 00:11:09,910
I give you references.

179
00:11:09,910 --> 00:11:13,330
But there's one textbook which you may
want to look at,

180
00:11:13,330 --> 00:11:16,820
which is free on the web, written by a
colleague.

181
00:11:16,820 --> 00:11:20,600
Who allows you to read the stuff free.

182
00:11:20,600 --> 00:11:22,800
Not download, and so on.

183
00:11:22,800 --> 00:11:27,140
So please, when you use this textbook,
don't try to download the stuff.

184
00:11:27,140 --> 00:11:29,550
You're violating copyright.

185
00:11:29,550 --> 00:11:31,530
I also encourage you to look at the other

186
00:11:31,530 --> 00:11:34,890
textbooks, and don't worry about which
edition it is.

187
00:11:34,890 --> 00:11:39,200
The fundamentals are the same, so choose a
reference for yourself.

188
00:11:39,200 --> 00:11:45,970
But recognize I am not following any
specific textbook page by page.

189
00:11:45,970 --> 00:11:49,410
If I were, this class wouldn't be very
useful, you know.

190
00:11:49,410 --> 00:11:53,690
So I am doing the essence of finance at an
introductory level.

191
00:11:53,690 --> 00:11:57,030
But you need to have some reference and
there are several options.

192
00:11:57,030 --> 00:11:58,150
So make a choice soon.

193
00:12:00,720 --> 00:12:04,730
Finally the material that is kind of comes
second

194
00:12:04,730 --> 00:12:07,710
in importance to the videos is,, are the
assignments.

195
00:12:07,710 --> 00:12:09,420
And I've talked enough about them.

196
00:12:09,420 --> 00:12:12,700
But I would encourage you again to do the
assignments.

197
00:12:12,700 --> 00:12:14,745
Try them even before looking at the video,

198
00:12:14,745 --> 00:12:15,250
[LAUGH]

199
00:12:15,250 --> 00:12:19,780
that's a, that's kind of a challenge to
try to understand what's going on.

200
00:12:19,780 --> 00:12:21,440
Test yourself all the time.

201
00:12:23,770 --> 00:12:25,790
I'm going to, now talking about this
thing, I'm

202
00:12:25,790 --> 00:12:28,560
going to move over to how do we evaluate.

203
00:12:28,560 --> 00:12:30,330
Why are we evaluating you?

204
00:12:30,330 --> 00:12:33,320
Because we'll give you a certificate.

205
00:12:34,450 --> 00:12:37,810
And the certificate has to have some
criteria right?

206
00:12:37,810 --> 00:12:39,390
I'm not a big fan of grading.

207
00:12:39,390 --> 00:12:44,230
In fact, I feel that's the least enjoyable
part of my life.

208
00:12:44,230 --> 00:12:49,290
And partly because, it's kind of boring.
But also because, who am I?

209
00:12:49,290 --> 00:12:50,790
You know I mean, you are learning.

210
00:12:50,790 --> 00:12:53,860
You should be able to grade yourself and
so on.

211
00:12:53,860 --> 00:12:57,500
But the world is complex so let's talk
about evaluation for a second.

212
00:12:57,500 --> 00:12:59,080
It will be pass, fail.

213
00:12:59,080 --> 00:13:01,660
So how do I determine pass or fail?

214
00:13:01,660 --> 00:13:05,570
You get a certificate if you pass, you
will not get a certificate.

215
00:13:05,570 --> 00:13:07,970
So my grading philosophy is very simple.

216
00:13:07,970 --> 00:13:15,440
It's not mine, it's borrowed from my
reading of grading in particular.

217
00:13:15,440 --> 00:13:20,370
And basically the notion here is, do you
know enough rather than do you

218
00:13:20,370 --> 00:13:25,170
get 100%.
So in each assignment you have to get 70%,

219
00:13:25,170 --> 00:13:31,060
if you don't get 70% on any one assignment
you can not get a certificate.

220
00:13:32,175 --> 00:13:35,920
Similarly you have to get 70% on the final
which is in the last week.

221
00:13:35,920 --> 00:13:38,780
You don't have an assignment in the last
week but you have a final.

222
00:13:40,030 --> 00:13:40,810
The good news

223
00:13:40,810 --> 00:13:45,850
is, that you'll get feedback immediately
from the computer as you're taking them.

224
00:13:45,850 --> 00:13:48,020
And for the assignments you have the whole
week.

225
00:13:49,500 --> 00:13:51,990
Whereas for the final I'll let you know
how much time you have.

226
00:13:53,470 --> 00:13:55,600
The real good news however, is because

227
00:13:55,600 --> 00:13:58,310
I'm interested in you learning, rather
than testing

228
00:13:58,310 --> 00:14:03,900
you just once and saying forget about it,
this guy or gal doesn't know something.

229
00:14:03,900 --> 00:14:05,860
I'm more interested in what you know

230
00:14:05,860 --> 00:14:07,650
rather than that what you don't know.

231
00:14:07,650 --> 00:14:11,650
So I'll give you two attempts at each of
one of these.

232
00:14:11,650 --> 00:14:14,340
Each assignment, two attempts, final two
attempts.

233
00:14:15,480 --> 00:14:19,600
But you have to finish within the
deadlines, both the attempts.

234
00:14:19,600 --> 00:14:22,410
Otherwise, you cannot, your score cannot
count.

235
00:14:22,410 --> 00:14:24,370
We have to have some criteria, right?

236
00:14:27,130 --> 00:14:30,930
Now dealing with me or the teaching
assistant.

237
00:14:30,930 --> 00:14:33,610
And this is one of the things that I think

238
00:14:33,610 --> 00:14:37,560
is a little bit of a disadvantage in the
online class.

239
00:14:37,560 --> 00:14:40,580
This class is really large.

240
00:14:40,580 --> 00:14:43,090
I do not want to get into the numbers
because I

241
00:14:43,090 --> 00:14:45,960
want you to think as this class as just
for you.

242
00:14:45,960 --> 00:14:48,910
I mean, I think that's the beauty of
online class.

243
00:14:48,910 --> 00:14:50,470
So it's a large class.

244
00:14:50,470 --> 00:14:52,450
What can't we do?

245
00:14:52,450 --> 00:14:54,990
I would encourage you, please don't
attempt to

246
00:14:54,990 --> 00:14:58,260
contact me, or my teaching assistant
Nathan Brown.

247
00:14:59,335 --> 00:15:03,630
And the reason is, you can easily find our
emails and shoot an email.

248
00:15:03,630 --> 00:15:07,040
It's not that, we don't love you.

249
00:15:07,040 --> 00:15:08,290
I love you all.

250
00:15:08,290 --> 00:15:11,020
There's a heart that beats in each one of
you.

251
00:15:11,020 --> 00:15:17,663
And that's my requirement, for loving, you
know, I'm, in fact, even plants have

252
00:15:17,663 --> 00:15:18,905
life, right?

253
00:15:18,905 --> 00:15:24,930
So, but in an event, coming back to the
basics, I cannot afford to answer emails

254
00:15:24,930 --> 00:15:30,740
and Nathan cannot, because it will become
unmanageable at our end.

255
00:15:30,740 --> 00:15:33,840
So I'd encourage you not to try to contact
us.

256
00:15:33,840 --> 00:15:34,950
So what will we do?

257
00:15:34,950 --> 00:15:38,290
We'll provide all the resources on a
weekly basis.

258
00:15:38,290 --> 00:15:40,260
They'll come, you'll know what you want,
you'll

259
00:15:40,260 --> 00:15:42,830
know exactly what, what's needed to be
done.

260
00:15:43,940 --> 00:15:48,170
And, the teaching assistant, Nathan, will
monitor the class

261
00:15:48,170 --> 00:15:50,950
for any errors and so on and so forth.

262
00:15:50,950 --> 00:15:54,620
But our goal is to make this class self
sufficient.

263
00:15:54,620 --> 00:15:55,780
How?

264
00:15:55,780 --> 00:15:58,510
There's only one teaching assistant and
only one me.

265
00:15:58,510 --> 00:16:03,330
But there are hundreds and thousands of
you, taking this class currently.

266
00:16:03,330 --> 00:16:08,000
Your talking to each other is going to be
more powerful, a learning tool.

267
00:16:08,000 --> 00:16:09,080
So, say for example,

268
00:16:09,080 --> 00:16:13,380
I make an error, and actually I have made
errors in the videos but I don't

269
00:16:13,380 --> 00:16:19,190
try to fix them and you know cut out that
clip, simply because we all make mistakes.

270
00:16:19,190 --> 00:16:22,410
I correct the error in real time with you,
and usually

271
00:16:22,410 --> 00:16:25,850
they are small, and I guarantee you they
will be small.

272
00:16:25,850 --> 00:16:29,110
But, if you find something, don't say, oh,
you know, your work?

273
00:16:29,110 --> 00:16:31,080
Gautam made a mistake.

274
00:16:31,080 --> 00:16:34,320
you'll make mistakes, I make mistakes,
your attitude should

275
00:16:34,320 --> 00:16:38,930
be I'll sought out what that mistake is.
Finally.

276
00:16:40,060 --> 00:16:44,490
This class is about ambiguity and
learning, and I

277
00:16:44,490 --> 00:16:47,980
would encourage you to take that attitude
towards it.

278
00:16:47,980 --> 00:16:48,480
Okay?

279
00:16:49,870 --> 00:16:52,130
One last thing which I feel compelled to

280
00:16:52,130 --> 00:16:54,510
talk about, because I have prepared most
of

281
00:16:54,510 --> 00:16:59,260
the class, and I wanted to share with you,
why did I say yes to it?

282
00:16:59,260 --> 00:17:02,170
Why did I choose to do something online?

283
00:17:02,170 --> 00:17:09,030
The first reason is I believe education
should be accessible to all.

284
00:17:09,030 --> 00:17:10,790
Why do I believe that?

285
00:17:10,790 --> 00:17:15,180
Because I believe education almost is like
a right.

286
00:17:15,180 --> 00:17:20,720
It's almost like a right to breathe.
I know I have strong views about it.

287
00:17:20,720 --> 00:17:26,680
But I generally believe that education
makes you become empowered.

288
00:17:28,460 --> 00:17:32,380
Most valuable investment you could ever
make is education.

289
00:17:32,380 --> 00:17:33,630
What do I mean by that?

290
00:17:33,630 --> 00:17:37,340
Remember, if you want electricity, which
is good.

291
00:17:38,400 --> 00:17:43,100
You are willing to burn coal, however,
burning coal leads

292
00:17:43,100 --> 00:17:46,940
to some problems, some costs on society,
and on you too.

293
00:17:48,100 --> 00:17:51,110
Education is an investment that largely
gives you

294
00:17:51,110 --> 00:17:53,430
benefits, why wouldn't you want to do
that,

295
00:17:53,430 --> 00:17:57,069
why wouldn't you want to make an
investment in yourself?

296
00:17:58,980 --> 00:18:04,460
My goal in this investment is to empower
you, not to

297
00:18:04,460 --> 00:18:07,830
be dependent on a coach like me, or any
other person.

298
00:18:07,830 --> 00:18:11,100
But to take this class, and say I can do
it too.

299
00:18:12,200 --> 00:18:14,110
And that's the beauty of education.

300
00:18:15,270 --> 00:18:18,600
I'll make a couple more points and then
we'll see each other

301
00:18:18,600 --> 00:18:19,280
in about a week.

302
00:18:21,240 --> 00:18:24,510
This has been a real challenge for me as a
teacher.

303
00:18:24,510 --> 00:18:28,130
I've always wanted to be able to teach
everybody.

304
00:18:28,130 --> 00:18:31,420
Every single person, and that's not been
easy.

305
00:18:31,420 --> 00:18:33,700
And I don't think I can teach everybody.

306
00:18:33,700 --> 00:18:36,580
But this experiment, hopefully, which
becomes an

307
00:18:36,580 --> 00:18:40,530
ongoing activity in my life, I feel like

308
00:18:40,530 --> 00:18:43,620
this gives me the ability to reach out to
you on a one to one

309
00:18:43,620 --> 00:18:47,600
basis and hopefully make you learn and be
excited.

310
00:18:49,100 --> 00:18:53,940
Finally, I love change and uncertainty,
because life would be pretty

311
00:18:53,940 --> 00:18:57,120
boring if I knew exactly what was going to
happen when.

312
00:18:57,120 --> 00:19:01,120
I know sometimes in life you want
certainty, but I'm talking

313
00:19:01,120 --> 00:19:06,530
about most of life, uncertainty is what
gives you value in life.

314
00:19:06,530 --> 00:19:09,640
So remember.
We are experimenting here.

315
00:19:09,640 --> 00:19:16,000
I want you to come with that attitude.
I'm going to learn finance in a new way.

316
00:19:16,000 --> 00:19:18,000
And I'm going to go with it.

317
00:19:18,000 --> 00:19:22,422
There will be glitches, there will be
errors made along the way from your side,

318
00:19:22,422 --> 00:19:25,194
maybe from technology, maybe from some
errors in

319
00:19:25,194 --> 00:19:28,000
the assignments, and so on and so forth.

320
00:19:28,000 --> 00:19:31,880
I'm almost sure we'll try our best not for
that to happen.

321
00:19:31,880 --> 00:19:34,770
But if something happens remember you're a
pioneer,

322
00:19:34,770 --> 00:19:37,280
I'm a pioneer and we are working on it
together.

323
00:19:38,640 --> 00:19:41,330
I'm looking forward to seeing you in a
week.

324
00:19:41,330 --> 00:19:42,980
And may the force be with you.

325
00:19:42,980 --> 00:19:44,090
See you soon.
Bye.

