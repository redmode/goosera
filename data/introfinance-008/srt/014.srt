1
00:00:00,000 --> 00:00:05,032
We are just doing future value of
annuities. And I'll show you now, why this

2
00:00:05,032 --> 00:00:10,085
is such a cool thing. And what I'm going
to do is, I'm going to do two examples,

3
00:00:10,085 --> 00:00:16,067
both for future value of an annuity. And
whats the other thing we do? Present value

4
00:00:16,067 --> 00:00:22,028
for an annuity. And remember, annuities,
the amount of the annuity when you write

5
00:00:22,028 --> 00:00:27,095
could be called C. But when you go to a
calculator or a spreadsheet we will call

6
00:00:27,095 --> 00:00:33,065
it PMT because that's what it, they call
it, right? Makes sense. Okay. I would like

7
00:00:33,065 --> 00:00:39,088
you to stare at this problem and I know
you have the ability to pause and so on,

8
00:00:39,088 --> 00:00:46,019
but I'd like to pause with you and what
I'm going to do, for every example, and if

9
00:00:46,019 --> 00:00:52,058
I don't, you should do this, is I'm going
to read out the problem for you and we'll

10
00:00:52,058 --> 00:00:58,066
talk about it a little bit. And then I
would really encourage you to try to do

11
00:00:58,066 --> 00:01:04,047
the problem, I'll do it with you but I
would encourage you to kind of think

12
00:01:04,047 --> 00:01:09,042
actively and be participating in it
because I can't, you know, I can't see

13
00:01:09,042 --> 00:01:15,005
whether you're doing it or not but I hope
you do. Okay, so what will be the value of

14
00:01:15,005 --> 00:01:20,028
your portfolio? What is the portfolio?
Portfolio and, that's a lingo in Finance.

15
00:01:20,028 --> 00:01:26,007
Portfolio means whatever your investment
is, wherever you have put your money. So,

16
00:01:26,007 --> 00:01:31,080
the word portfolio is used generically
because you'll, you'll, you'll see later,

17
00:01:31,080 --> 00:01:37,075
it's a hangover from the fact that life
has risk. And if life has risk and you do

18
00:01:37,075 --> 00:01:43,056
not like risk, which most people don't,
you tend to not put all your eggs in one

19
00:01:43,056 --> 00:01:48,327
basket. So, the fact that you hold a
basket of different things is called a

20
00:01:48,327 --> 00:01:53,745
portfolio. Okay, I'm. I try to emphasize
words which are I take for granted

21
00:01:53,745 --> 00:01:58,963
because, you know, I mean, if you're new
to Finance, which most of you probably

22
00:01:58,963 --> 00:02:04,209
are, you need to understand why certain
language is used in, in very commonly. At

23
00:02:04,209 --> 00:02:10,891
retirement, if you deposit $10,000 every
year in a pension fund. Now, if you are

24
00:02:10,891 --> 00:02:17,583
really young, say, you are fifteen and
taking this class which I hope some of you

25
00:02:17,583 --> 00:02:21,670
are, don't worry too much about
retirement, you know, have some fun.

26
00:02:21,670 --> 00:02:26,703
You're in high school, you haven't even be
gun earning hopefully, just having fun.

27
00:02:26,703 --> 00:02:31,875
So, but this is something that you will do
at some point, most people do. So, what I

28
00:02:31,875 --> 00:02:37,515
would recommend is just think of it as an
intellectual problem but actually it's a

29
00:02:37,515 --> 00:02:43,638
very rare problem. So, what will be the
value pf your portfolio over time and you

30
00:02:43,638 --> 00:02:50,328
deposit $10,000 every year in a pension
fund? What is a pension fund? Pension fund

31
00:02:50,328 --> 00:02:55,637
is a place or an account which hopefully
has multiple assets, if you're risk

32
00:02:55,637 --> 00:03:01,768
averse. Multiple kinds of investments, a
bond, a stock, and we'll talk about those.

33
00:03:01,768 --> 00:03:08,602
You put $10,000 every year, why 10,000
fixed amount? Well, nothing is forcing you

34
00:03:08,602 --> 00:03:15,552
to put 10,000 every year. It can be 11,000
one year, 9,000 the other. But oddly

35
00:03:15,552 --> 00:03:22,172
enough, to make life simple perhaps, many
people tend to put away a certain amount

36
00:03:22,172 --> 00:03:28,371
of money every year for things they need
in the future. So, the notion of a pension

37
00:03:28,371 --> 00:03:34,379
fund is at some point I'm going to retire
and I need some money. So, you put away

38
00:03:34,379 --> 00:03:40,383
10,000 every year. You plan to retire in
about 40 years, and expect to earn eight

39
00:03:40,383 --> 00:03:46,946
percent on your portfolio. So, what have I
given you? I have given you everything you

40
00:03:46,946 --> 00:03:53,666
need. I have given you PMT, RC, which is
10,000. I've given you number of years

41
00:03:53,666 --> 00:04:00,656
left for you to retire, 40, and I've given
you an interest rate that you're likely to

42
00:04:00,656 --> 00:04:05,686
earn on your portfolio, which means, where
you put your money. A bank, whatever.

43
00:04:05,686 --> 00:04:10,752
We'll talk about that in a second. But
let's just focus on this, and try to do

44
00:04:10,752 --> 00:04:16,066
this problem. I hope you've been listening
to me, and I hope you've been paying

45
00:04:16,066 --> 00:04:22,018
attention. Because if you pay attention to
a problem, it gets to be a little intense.

46
00:04:22,018 --> 00:04:27,049
And I'll do the problems with you. And I
have promised myself, today, I'll spend a

47
00:04:27,049 --> 00:04:32,067
lot of time just doing problems with you
because that's how you learn. And I,

48
00:04:32,067 --> 00:04:38,010
another piece of advise. I've given you
textbooks to read, that you can go, get

49
00:04:38,010 --> 00:04:43,079
and read. They can be second hand. They
can be, whatever. The fundamental

50
00:04:43,079 --> 00:04:48,643
principles of Finance have been known
since we were in the cave. So, it's a,

51
00:04:48,643 --> 00:04:53,860
it's a, just remember that what you're
trying to do is focus on the fundamentals.

52
00:04:53,860 --> 00:04:59,068
So, read whatever you want if the video
doesn't satisfy your curiosity. But the

53
00:04:59,068 --> 00:05:05,016
video is trying to be self-sufficient. So,
let's do this, let's now start doing the

54
00:05:05,016 --> 00:05:10,078
problem. And what I'm going to do is I'm
going to do two problems for future value,

55
00:05:10,078 --> 00:05:15,691
two problems for present value. But I'll
take breaks with you. So, I'll let you

56
00:05:15,691 --> 00:05:21,734
know that maybe it's time to take some
time off. Get some coffee, go jog around

57
00:05:21,734 --> 00:05:28,035
the apartment building where you live, or
talk to your friend, or watch a video on

58
00:05:28,035 --> 00:05:36,433
YouTube, you know, why not? Okay. So let's
get started. I'm going to draw a timeline

59
00:05:36,433 --> 00:05:45,949
right here. You can use the box I gave you
or you can use, I prefer this. And how

60
00:05:45,949 --> 00:05:53,726
many years? 40. So, please re, remember
there's always one more point in time than

61
00:05:53,726 --> 00:06:00,097
the number of periods. If you remember
that or if you recognize that, you'll be

62
00:06:00,097 --> 00:06:06,362
okay. So, how maNy points in time, 41.
Zero, one through 40. How many periods of

63
00:06:06,362 --> 00:06:12,547
time? Well, it takes two points of time to
make a period, so there one less. One,

64
00:06:12,547 --> 00:06:19,654
two. So, what we'll do to make our life
simple is we'll assume that the first

65
00:06:19,654 --> 00:06:26,222
$10,000 is at the end of the year. Why did
I do that? I could always start saving at

66
00:06:26,222 --> 00:06:31,275
this point. But I'm doing it simply so
that I can use the formula and, just

67
00:06:31,275 --> 00:06:36,193
directly, you know. Use the calculator,
and do it, and setup. We can change that,

68
00:06:36,193 --> 00:06:43,703
so don't worry. You can start a payment
today, and change it. It's just a minor

69
00:06:43,703 --> 00:06:51,759
difference. How much? Another thing that
seems a little bit odd or manufactured in

70
00:06:51,759 --> 00:06:58,508
this formula is that you are saving at
year 40 too, right, you may not be, right or

71
00:06:58,508 --> 00:07:05,234
actually messed up saving in between. So,
but for convenience, we are trying to

72
00:07:05,234 --> 00:07:12,225
understand the problem which is got 40 of
these guys, alright. So, the good news is

73
00:07:12,225 --> 00:07:18,100
even though this formula is very
complicated, right, you divide this by how

74
00:07:18,100 --> 00:07:24,504
much? I mean, you carry it forward by how
much? One period but there's no money. How

75
00:07:24,504 --> 00:07:30,817
much do you carry this forward by? 39
periods. How much do you carry this

76
00:07:30,817 --> 00:07:37,548
forward by? 38. Which is the simplest
piece of this? This guy. Why? Because I'm

77
00:07:37,548 --> 00:07:43,068
askin g you, what is the future value at
this point in time, alright. So, the

78
00:07:43,068 --> 00:07:48,418
future value of this point in time of this
guy is just itself. That's the, that's

79
00:07:48,418 --> 00:07:54,300
what I mean, if you learn how to travel in
time. If you are at 40 and you are

80
00:07:54,300 --> 00:08:00,222
imagined you are at 40, point number 40,
the last PMT or C is $10,000, so at time

81
00:08:00,222 --> 00:08:05,528
40, it's exactly ten, but when you look
back if you were to, you have to carry

82
00:08:05,528 --> 00:08:10,871
past money amounts you've invested as
earning money, which is good news for you,

83
00:08:10,871 --> 00:08:15,158
right? And it's earning how much? Eight
percent and let me, let me tell you,

84
00:08:15,158 --> 00:08:21,308
that's not bad at all, aleight. And we'll
talk about that when we talk about risk

85
00:08:21,308 --> 00:08:27,093
and return in a second. So, do you
understand the nature of the beast? The

86
00:08:27,093 --> 00:08:32,958
beast is not easy. It's not easy, it's
like doing 39 future values and adding

87
00:08:32,958 --> 00:08:44,189
them up, right? So what I'm going to do is
I'm going to now shift to using a

88
00:08:44,189 --> 00:08:58,476
calculator. Okay, Okay, so, if you notice
I am on the top panel and I am going to

89
00:08:58,476 --> 00:09:07,478
use the formula of PMT. Remember, whatever
you don't know, you type in here. So,

90
00:09:07,478 --> 00:09:15,139
that's yeah, PMT, alright. And the one
thing, you have to do before you do PMT

91
00:09:15,139 --> 00:09:21,515
and not get excited like PM, Mr. Hyper,
you have to put equal sign so that,

92
00:09:21,515 --> 00:09:28,658
otherwise, you will get all kinds of
garbage. Okay, you open it up. What is the

93
00:09:28,658 --> 00:09:35,700
first number that shows up? The first
number that shows up is the rate of return

94
00:09:35,700 --> 00:09:42,490
and we know how much are we earning. We
earning 0.08. Again, emphasizing this. The

95
00:09:42,490 --> 00:09:49,119
only reason I'm using Excel right now is
what? Simply, because the calculation is

96
00:09:49,119 --> 00:09:55,546
very difficult. But I've explained to you
what's going on. You're doing 40 going

97
00:09:55,546 --> 00:10:02,373
forwards, but actually only 39. Because
the first one is zero and the last one is

98
00:10:02,373 --> 00:10:10,016
just itself. So, so that's why, that's it,
okay? So, you put a comma, and what's the

99
00:10:10,016 --> 00:10:19,250
next one? 40. Number of periods, right?
And, actually let me just back track a

100
00:10:19,250 --> 00:10:28,168
little bit. The thing that we want to
figure out is FV, so put an FV and now I

101
00:10:28,168 --> 00:10:35,669
want rate 0.08. You see what I was doing
we will do next time. So, the number of

102
00:10:35,669 --> 00:10:43,221
periods is 40 and in this case, I know my
PMT and my PMT is $ten, 000, right? And

103
00:10:43,221 --> 00:10:51,244
what is PV, don't worry about it, it's not
in there and just hit, okay? So, what do

104
00:10:51,244 --> 00:10:58,492
you get and if you get a lot of money
basically, you get two million dollars,

105
00:10:58,492 --> 00:11:03,486
2.59 million dollars, arlight. Its two,
five, nine, zero, five, six, five. So,

106
00:11:03,486 --> 00:11:12,263
what does this tell you? This tells you
that if you invest $10,000 in a bank 40

107
00:11:12,263 --> 00:11:20,863
times, the future value of that will end
up being 2.59 million dollars. So, what

108
00:11:20,863 --> 00:11:28,532
I'm going to do, I'm going to try to talk
you through the problem again. So, just,

109
00:11:28,532 --> 00:11:36,003
so that we are working together. What did
I do? I calculated Future Value. So, in

110
00:11:36,003 --> 00:11:43,263
order to calculate Future Value of
something that I don't know, I have to use

111
00:11:43,263 --> 00:11:50,877
the Future Value function in the
calculator or in the spreadsheet. And out

112
00:11:50,877 --> 00:12:02,437
popped, I gave this information $10,000
was the PMT, 40 was n. But most

113
00:12:02,437 --> 00:12:11,961
importantly eight percent was r. So, I
gave all this information to Excel or a

114
00:12:11,961 --> 00:12:17,418
calculator, whatever you choose to be
using, simply because it's a very

115
00:12:17,418 --> 00:12:23,347
complicated calculation. Conceptually,
it's not that difficult. And we got 2.59

116
00:12:23,347 --> 00:12:30,692
mil. And I'm going to just use it
approximately because I'm not going to

117
00:12:30,692 --> 00:12:36,660
calculate, write all the digits and so on.
So, what has happened here? Let me just

118
00:12:36,660 --> 00:12:42,702
walk you through this problem. First of
all, remember yesterday, remember I asked

119
00:12:42,702 --> 00:12:48,438
you, what is the answer to a Finance
question or anybody asks you what, what

120
00:12:48,438 --> 00:12:54,089
should you say? Compounding. But you
always have to pause because you want to

121
00:12:54,089 --> 00:13:00,095
look smart, right? So, you take a pause,
and you say compounding. And so, so let me

122
00:13:00,095 --> 00:13:07,001
ask you the following question. Suppose
there was no interest rate or, in other

123
00:13:07,001 --> 00:13:12,639
words, how much of the 10,000 are you
throwing in? And suppose the interest is

124
00:13:12,639 --> 00:13:18,073
zero. This problem is very simple to do. Why?
Because you do 40 times 10,000. You have

125
00:13:18,073 --> 00:13:26,074
$400,000, right. So, the interest rate,
time value of money is zero, you will have

126
00:13:26,074 --> 00:13:34,081
a lot of money in your bank account but
how much will it be? 400,000. How much do

127
00:13:34,081 --> 00:13:42,865
you have, if the interest rate is eight%?
2.59 million. Huge difference in magnitude

128
00:13:42,865 --> 00:13:48,964
and who's the culprit? Compounding. In
this case, the culprit is helpi ng you.

129
00:13:48,964 --> 00:13:54,764
But in the case of if you are paying it,
it hurts. So, we'll do a loan later. So,

130
00:13:54,764 --> 00:14:00,696
here it's helping you. So, let's talk
through this problem a little bit. And so

131
00:14:00,696 --> 00:14:07,921
that you understand how empowered you are.
How empowered actually you are and Finance

132
00:14:07,921 --> 00:14:14,249
will make you feel liberated in some, in
the simple problem. So here, here you go.

133
00:14:14,249 --> 00:14:21,927
Let me ask you, who decided the 10,000?
Think about it. Who should decide the

134
00:14:21,927 --> 00:14:29,397
10,000 every year? You? Or a financial
adviser? Who? I hope the answer is you.

135
00:14:29,397 --> 00:14:36,678
So, 10,000 if you have, I know it's not
easy to figure out. But I would encourage

136
00:14:36,678 --> 00:14:41,753
you to think about what your needs are in
the future so that you could figure out

137
00:14:41,753 --> 00:14:46,853
how much you need to put away. And we'll
talk, do a problem quite the reverse in a

138
00:14:46,853 --> 00:14:51,717
second, say you put away $10,000, who
decides that? You decide that. Second

139
00:14:51,717 --> 00:14:58,063
question, what is the other number in this
problem? It's 40. How many years to

140
00:14:58,063 --> 00:15:04,454
retirement? I know you can say that a job
may have a retirement age or some sort of,

141
00:15:04,454 --> 00:15:11,020
but I, I challenge you in that. Hopefully,
you will have much more control on when

142
00:15:11,020 --> 00:15:15,989
you retire. Then you think you do. By
that, I mean you should keep learning in

143
00:15:15,989 --> 00:15:21,794
life so that you will always have the
opportunity to do something, right? And we

144
00:15:21,794 --> 00:15:26,867
are talking about the money problem but it
could be about anything. So, let's take

145
00:15:26,867 --> 00:15:32,680
the extreme case scenario, you are doing a
regular job and you know 40 years from

146
00:15:32,680 --> 00:15:37,368
now, you are going to retire. My point
there is, you have more control on that,

147
00:15:37,368 --> 00:15:42,458
but sometimes people don't. People have
jobs where they are dependent on their

148
00:15:42,458 --> 00:15:47,488
employer on how many years they work? But
forgiven, I mean you won't go to a

149
00:15:47,488 --> 00:15:53,825
financial advisor and say, how many years
do I retire? The person will give you an

150
00:15:53,825 --> 00:15:59,290
answer but [laugh] , you know, will charge
you a lot money for giving the answer,

151
00:15:59,290 --> 00:16:05,110
right? So the 40 is also information that
you should know, 10,000 is the information

152
00:16:05,110 --> 00:16:10,411
you should know and now the eight%. I'm
going to violate the assumption that I

153
00:16:10,411 --> 00:16:16,940
said make at the back of your mind. But to
be fair, I never said assume is not there.

154
00:16:16,940 --> 00:16:23,058
I said, I know it's there, keep it in the
back of your mind, but for convenience's

155
00:16:23,058 --> 00:16:29,127
sake, ignore it. And that is risk. So, let
me ask you this, who determines the

156
00:16:29,127 --> 00:16:34,655
eight%? And if you can answer that, you
have arrived. And the answer is simply

157
00:16:34,655 --> 00:16:39,501
nobody. If, if anybody knew what the
interest rate was in the future for the

158
00:16:39,501 --> 00:16:43,972
next 40 years or something, you know, they
would, they would be omnipresent. They

159
00:16:43,972 --> 00:16:48,533
would know the future. We are wanting to
be like that. But I think the beauty of

160
00:16:48,533 --> 00:16:53,660
life is nobody knows. And in fact, one of
the most profound developments in Finance

161
00:16:53,660 --> 00:16:57,984
in recent years, I, I should recent say
last 40, 50 years that gets challenged

162
00:16:57,984 --> 00:17:02,486
because it's a good idea. Bad ideas don't
get challenged, right? Good ideas get

163
00:17:02,486 --> 00:17:08,198
challenged. So, the notion there is that
nobody in a good market should be able to

164
00:17:08,198 --> 00:17:13,561
tell the future because everything we know
is already in the marketplace, right?

165
00:17:13,561 --> 00:17:19,371
That's why I said competitive markets at
the beginning are extremely important to

166
00:17:19,371 --> 00:17:24,336
what we do. So, quick question. Who
determines the eight%?. And the answer is

167
00:17:24,336 --> 00:17:29,547
you. And this is where I have to bring in
risk a little bit. Why? Because eight

168
00:17:29,547 --> 00:17:35,071
percent let me tell you, if you get over
the next 40 years, may the force be with

169
00:17:35,071 --> 00:17:40,913
you. [laugh]. Because it is not going to
be easy. You have to take risks to get

170
00:17:40,913 --> 00:17:46,150
high rates of return, and with risk comes
volatility. So, the eight%, the higher

171
00:17:46,150 --> 00:17:52,167
returns, the more likely it is that you
are jumping all over the place, like the

172
00:17:52,167 --> 00:17:58,621
stock market. So, if you want to be safer,
what you have to do? You have to lower the

173
00:17:58,621 --> 00:18:04,741
interest rate to say, four%. Put it in a
bond issued by the government, in the long

174
00:18:04,741 --> 00:18:10,877
run, and you'll be safer. But what will
happen to the 2.59 million? Do this

175
00:18:10,877 --> 00:18:16,246
exercise for yourself. Let's, after this
class is over, use four percent instead of

176
00:18:16,246 --> 00:18:22,149
eight%. And what will you see? A dramatic
drop in the amount of money that you have

177
00:18:22,149 --> 00:18:27,924
at the end. So, what, why am I emphasizing
so much in one little problem? Because

178
00:18:27,924 --> 00:18:33,598
that's what Finance's beauty is, you know.
If you understand these problems inside

179
00:18:33,598 --> 00:18:38,566
out, and you know how to use the Excel
spreadsheet to calculate the answer,

180
00:18:38,566 --> 00:18:43,886
you've arrived. So, if you use four%, what
happens? You kind of get rid of your

181
00:18:43,886 --> 00:18:49,467
nervousness about risk. But what happens
to the amount of money you have? It'll

182
00:18:49,467 --> 00:18:55,029
drop dramatically, right? We know that. We
know the power of compounding. It helps

183
00:18:55,029 --> 00:19:01,774
when an interest rate goes up, it hurts
when it goes down. So, having said that,

184
00:19:01,774 --> 00:19:08,286
if the interest rate is four%, you're
going to suffer. What, what can you say

185
00:19:08,286 --> 00:19:14,616
about the eight%, four percent choice?
Neither one is good or bad. Neither one is

186
00:19:14,616 --> 00:19:22,441
good or bad. What's important is you have
control over the four and the eight in the

187
00:19:22,441 --> 00:19:28,038
following sense, not that you can predict
it but you, if you choose to put four

188
00:19:28,038 --> 00:19:32,845
percent in your calculations, it has to be
matched by your investment strategy. So,

189
00:19:32,845 --> 00:19:37,558
if you are thinking you're going to earn
eight percent and put it in the bank,

190
00:19:37,558 --> 00:19:43,569
especially today, and if this low interest
rates go on, you're dreaming. You'll have

191
00:19:43,569 --> 00:19:48,711
closer to $400,000, if the bank is still
there after 40 years, right? So, so think

192
00:19:48,711 --> 00:19:54,044
like that. Everything is under your
control and the beauty of markets is for

193
00:19:54,044 --> 00:20:00,090
most of us, we do not need to second guess
what the interest rates are. All we need

194
00:20:00,090 --> 00:20:06,245
to do is match our preferences of risk
with our investment strategy and then not

195
00:20:06,245 --> 00:20:08,082
worry about it too much.
