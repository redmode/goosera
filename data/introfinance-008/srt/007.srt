1
00:00:00,000 --> 00:00:06,078
So this problem says and if you're taking
a break good for you. Let's do this

2
00:00:06,078 --> 00:00:12,092
problem. This problem says, suppose you
put 500 bucks in the bank. And the

3
00:00:12,092 --> 00:00:17,061
interest rate is seven%. If you notice,
what have I done til now? I've chosen

4
00:00:17,061 --> 00:00:22,049
interest rate of ten%. Why did I do it?
Because I can do the problem in my head,

5
00:00:22,049 --> 00:00:27,049
and so can you. I'll take, have the luxury
of doing it, the real world doesn't. In

6
00:00:27,049 --> 00:00:32,037
fact, it has three decimals in it or
something like that. Here, we'll make it a

7
00:00:32,037 --> 00:00:37,214
little bit more interesting. Let's make it
interesting, make the interest rate 70%.

8
00:00:37,214 --> 00:00:42,018
However, the interest is not coming from
the seven%. If I asked you, what is the

9
00:00:42,018 --> 00:00:47,196
seven, what is seven percent of 500 bucks,
I hope you can do the problem in your

10
00:00:47,196 --> 00:00:53,096
head. And it, multiply the two and you get
a good answer, whatever it is. It's 35.

11
00:00:53,096 --> 00:00:59,771
Right? So, however, what's complicating
things is how much will you have at the

12
00:00:59,771 --> 00:01:04,804
end of ten years? So let's go back to
basics. And I want, I'm going to force you

13
00:01:04,804 --> 00:01:10,053
to do this. And the reason is if you
don't, I'm going to do it. Whether you

14
00:01:10,053 --> 00:01:15,078
want to do it or not is obviously your
choice. I'm going to be very timeline

15
00:01:15,078 --> 00:01:21,024
oriented, especially in the beginning of
the class. So what's the time? This is

16
00:01:21,024 --> 00:01:34,099
zero. Right? How many periods? Again for
convenience ten years. What's the other

17
00:01:34,099 --> 00:01:41,094
element you know in the problem? I know
that I can put P here as 500. You can

18
00:01:41,094 --> 00:01:49,034
think of it as B, B, B sharp. The question
I'm now asking is what the heck is going

19
00:01:49,034 --> 00:01:57,083
on here? Again please remember we have no
risk. So I have gone from two years to ten

20
00:01:57,083 --> 00:02:02,085
years and you know lot can happen, and lot
has happened in the recent past. So I

21
00:02:02,085 --> 00:02:07,050
don't mean to believe to what has
happened, I just think that uncertainty

22
00:02:07,050 --> 00:02:12,091
cuts both ways and we have seen lot of bad
effects of crisis, but I'm going to ignore

23
00:02:12,091 --> 00:02:18,032
all uncertainty for the time being so that
we understand the effect of time. Now this

24
00:02:18,032 --> 00:02:23,009
is not easy. It's very simple to
understand conceptually because what would

25
00:02:23,009 --> 00:02:28,099
you do. Just carry it forward one year of
the time. And if we had all the time in

26
00:02:28,099 --> 00:02:34,080
the world, and the only problem to solve,
we cou ld do it and ten weeks would be

27
00:02:34,080 --> 00:02:40,016
over. So, but we want to write the
formula. I want to take one + r which is

28
00:02:40,016 --> 00:02:45,091
the factor, which in this case is. And
what do I want to do? How many times is

29
00:02:45,091 --> 00:02:53,031
this happening? I know it's happening over
ten years. So here is the problem. I'm

30
00:02:53,031 --> 00:03:00,005
raising it to the power of ten. And
remember every time you go forward the

31
00:03:00,005 --> 00:03:07,504
factor is one + r so after this one here
it will be times 1.07, 1.07^2, and so on.

32
00:03:07,504 --> 00:03:13,687
This thing, by the way, if you can do it
in your head, there's something seriously

33
00:03:13,687 --> 00:03:18,700
wrong with you. You need to grow up and do
more interesting things in life. But there

34
00:03:18,700 --> 00:03:23,442
are people who can do this in their head,
and I think there's something, you're

35
00:03:23,442 --> 00:03:27,715
spending your time on the wrong thing.
Just think through it. If you understood

36
00:03:27,715 --> 00:03:34,487
what's going on, this is where you ask
yourself, do I get a calculator? Or do I

37
00:03:34,487 --> 00:03:39,590
get Excel? And what I'm going to do, is,
you have notes on using either one. The

38
00:03:39,590 --> 00:03:45,234
calculator has to be financial, but not in
this problem necessarily, because you can

39
00:03:45,234 --> 00:03:51,054
raise things to the power of ten and so
on. But this is the kind of Algebra that

40
00:03:51,054 --> 00:03:56,986
you have to be comfortable. Therefore, I'm
throwing in the Algebra before actually

41
00:03:56,986 --> 00:04:01,547
solving the problem. But to solve the
problem, we'll do, have to what? We'll

42
00:04:01,547 --> 00:04:18,921
have to go to Excel. And I'll show you
very simple way of doing Excel. Okay. So.

43
00:04:18,921 --> 00:04:27,233
Let's see. If you can see what I am doing
there, I am going to the tab on top, the,

44
00:04:27,233 --> 00:04:32,584
the, the space on top that says effects.
And that is where the functions reside. So

45
00:04:32,584 --> 00:04:38,587
if you haven't got the finance functions
you can always get them from Excel, it's

46
00:04:38,587 --> 00:04:44,240
not a big deal. But the thing I like you
to know is this, it's very intuitive. So

47
00:04:44,240 --> 00:04:49,720
what are the key elements you need to know
to solve this problem. I mean the key

48
00:04:49,720 --> 00:04:54,958
elements you need to know is, you are
solving a future value problem. So the

49
00:04:54,958 --> 00:04:59,721
first thing you do is you put in something
that you don't know. You don't put in

50
00:04:59,721 --> 00:05:03,810
something that you are, already know
because the Excel will look back at and

51
00:05:03,810 --> 00:05:08,387
will say, you already know the answer, why
the heck are you asking me. So it's

52
00:05:08,387 --> 00:05:13,634
feature value I don't know. And as soon as
I press feature value, guess what pops up.

53
00:05:13,634 --> 00:05:19,470
What pops up right there is the first
thing you need to know, which is the rate.

54
00:05:19,470 --> 00:05:25,836
And the rate is the interest rate. As I
said, symbols are something are something

55
00:05:25,836 --> 00:05:31,983
that you need to familiarize yourself
with. And another thing about Excel, which

56
00:05:31,983 --> 00:05:38,167
many calculators differ on, is in Excel,
you have to rate, write the rate exactly

57
00:05:38,167 --> 00:05:44,764
as it is. So it is .07. Many calculators
would allow you to just write seven, but

58
00:05:44,764 --> 00:05:50,631
in Excel if you write seven, that means
you are assuming the interest rate as

59
00:05:50,631 --> 00:05:56,335
humongous, right. So what's the next
element it asks for? It says N P E R. The

60
00:05:56,335 --> 00:06:02,082
N is the operative word, P E R stands for
periods. So in this case I believe. We

61
00:06:02,082 --> 00:06:07,095
have to type, ten, because there was
interest rate is seven, the number of

62
00:06:07,095 --> 00:06:13,051
years past is ten, and there is a button
called, or there's a symbol called PMT.

63
00:06:13,051 --> 00:06:18,086
For now, ignore it. And the reason I'm
asking you to ignore it is it doesn't

64
00:06:18,086 --> 00:06:24,070
enter our problem. That's the next element
we'll get into next week. And PMT stands,

65
00:06:24,070 --> 00:06:30,069
basically for something called payment. So
what when does do payments happen say for

66
00:06:30,069 --> 00:06:37,035
on a loan? They happen regularly. Right
now we're just looking at $one transferred

67
00:06:37,035 --> 00:06:43,365
travel, time travel over time. So I will
put a zero there, because if you don't put

68
00:06:43,365 --> 00:06:49,440
a zero, it won't know what's going on. And
then I put PV, and I know PV. It's I who

69
00:06:49,440 --> 00:06:55,024
put money in the bank, so I better know
how much it is. Alright, so I put it in

70
00:06:55,024 --> 00:07:00,613
there and then I press return. Now what
you will notice is it, it's showing up in

71
00:07:00,613 --> 00:07:06,850
red. You are noticing $983 and some cents.
By the way I'm not interested in cents

72
00:07:06,850 --> 00:07:12,485
here, right. So I'm not even interested in
the answers so much. I'm interested in

73
00:07:12,485 --> 00:07:18,131
your understanding why I use Excel and the
reason I used Excel is because the human

74
00:07:18,131 --> 00:07:23,804
mind cannot calculate something raised to
power ten very easily. And in fact,

75
00:07:23,804 --> 00:07:29,143
there's a, there's a whole video created
and researched on this, that human are

76
00:07:29,143 --> 00:07:34,578
very good with linear stuff. And humans
are not very good with non-line ar stuff.

77
00:07:34,578 --> 00:07:40,261
So, that's why maybe, sometimes finance
looks like a challenge. But if you break

78
00:07:40,261 --> 00:07:45,308
it up into bite-size pieces, and recognize
why you're using Excel, Excel doesn't

79
00:07:45,308 --> 00:07:51,031
control you. You control Excel. Right? I,
I hope that's pretty obvious. So if you've

80
00:07:51,031 --> 00:07:57,422
seen Matrix, it's a very different world
and we are not there yet. So don't let

81
00:07:57,422 --> 00:08:03,803
Matrix enter your mind thinking that Excel
is solving your problems, one day it will

82
00:08:03,803 --> 00:08:10,290
but not for now at least. So what is 983?
$983 is the value of $500. How much, how

83
00:08:10,290 --> 00:08:16,460
many years from now? Ten. But there has to
be another element, in answering this

84
00:08:16,460 --> 00:08:23,303
problem, and that is the interest rate. So
the interest rate is seven%, and the world

85
00:08:23,303 --> 00:08:28,634
remains the same for the next ten years.
And I get the seven percent a year,

86
00:08:28,634 --> 00:08:34,647
assuming no risk right now. I'll get 983
bucks in the bank. But clearly, if the

87
00:08:34,647 --> 00:08:40,256
interest rate was lower, I would have
less. If the interest rate was higher, I

88
00:08:40,256 --> 00:08:46,321
would have more. The real core dynamic is
the interaction between time and the

89
00:08:46,321 --> 00:08:52,097
interest rate. So the interest rate is a
pretty year number, in this case seven

90
00:08:52,097 --> 00:08:58,088
percent but the real cool interaction,
which we call compounding, is between the

91
00:08:58,088 --> 00:09:05,015
seven percent interest and the number of
years, ten. And the more years that

92
00:09:05,015 --> 00:09:11,048
happen, and we'll see a problem soon, the
more the dynamic becomes very powerful. So

93
00:09:11,048 --> 00:09:17,067
here, you've almost doubled your money in
ten years at an interest of seven that

94
00:09:17,067 --> 00:09:23,531
wouldn't happen at a lower would happen
faster at a higher rate. So I hope this is

95
00:09:23,531 --> 00:09:29,799
clear to you. Now one last comment, and I
said I am not going to spent too much time

96
00:09:29,799 --> 00:09:35,745
on Excel, but I have to kind of satisfy
your curiosity. Why is the number in red?

97
00:09:35,745 --> 00:09:41,378
Why is it negative? Now think about it
this is actually a pretty cool thing. So

98
00:09:41,378 --> 00:09:49,055
Excel has been set up to make you realize.
That if you put enough 500 plus today it

99
00:09:49,055 --> 00:09:55,122
has to be negative in the future. So think
about it. Who's getting the 500 bucks

100
00:09:55,122 --> 00:10:00,865
today? You're giving it up. But who's
getting it? The bank is getting it. And

101
00:10:00,865 --> 00:10:06,113
what will they have to do ten years from
now? If they're getting it? Remember I put

102
00:10:06,113 --> 00:10:11,075
500 positive in the PD. They'll have to
give up 983. So the lesson from Excel is

103
00:10:11,075 --> 00:10:16,042
pretty cool, and that is, you can't get
something for nothing. There is no such

104
00:10:16,042 --> 00:10:20,898
thing as a free lunch. So if the bank, if
you give 500 and are willing to give

105
00:10:20,898 --> 00:10:25,760
another 983 to the bank, then your the
sucker. Not the bank. And banks would be

106
00:10:25,760 --> 00:10:30,826
pretty happy. In fact, probably will feel
like doing business with you and you go

107
00:10:30,826 --> 00:10:36,128
out of business and the bank will be in
business forever. So, just wanted to give

108
00:10:36,128 --> 00:10:42,243
you a sense of that. I'm going to come
back to this in a second because I want

109
00:10:42,243 --> 00:10:55,462
to, you to recognize that doing that doing
these problems on Excel, is simply because

110
00:10:55,462 --> 00:11:01,484
you can calculate things faster. So, let
me do some examples. By the way, and I

111
00:11:01,484 --> 00:11:08,090
want to emphasis one thing, a lot of these
examples that I am using in this class. I

112
00:11:08,090 --> 00:11:14,054
don't even remember how I thought of them.
I mean, lot of my colleagues at Michigan

113
00:11:14,054 --> 00:11:20,003
have helped me become a good teacher if
I'm a good teacher at all. Lot of the

114
00:11:20,003 --> 00:11:25,046
things we talk about. Lot of the examples
we use, lot of the notes we use. We use

115
00:11:25,046 --> 00:11:30,068
with each other. And, to be honest when
you read literature out there. Lot of

116
00:11:30,068 --> 00:11:36,038
these numbers have real world meaning. So
let's go to the next problem. And show you

117
00:11:36,038 --> 00:11:40,874
the power of compounding. What are the
future values of investing 100 bucks at

118
00:11:40,874 --> 00:11:47,205
ten%, versus five%for 100 years. Why am I
doing this? I am, I'm doing this simply to

119
00:11:47,205 --> 00:11:52,073
give you a, actually the real world
context. So which kind of person, and I

120
00:11:52,073 --> 00:11:59,007
promise I won't talk about risk but I, I'm
by implication talking about it because if

121
00:11:59,007 --> 00:12:06,032
there is no risk how many interest rates
would there be? One, and it would be the

122
00:12:06,032 --> 00:12:11,028
same. Because risk, largely, is
responsible for different interest rates.

123
00:12:11,028 --> 00:12:16,017
But for the time being, let's assume, for
whatever reason, you have two

124
00:12:16,017 --> 00:12:20,091
opportunities, five percent or ten%. In
the real world, what would this mirror?

125
00:12:20,091 --> 00:12:25,500
The five percent is kind of closer to a
bond, where the difference between a bond

126
00:12:25,500 --> 00:12:31,163
and a stock is it's less risky. The ten
percent is kind of closer to what the U.

127
00:12:31,163 --> 00:12:36,574
S. Stock market say, has given. It's given
more over the last, say, 80 years. So I'm

128
00:12:36,574 --> 00:12:42,448
just anchoring them in kind of real world
problems, but keeping the interest rate

129
00:12:42,448 --> 00:12:48,471
simple, so it's not 4.265, you know? For
the sake of time. And you can do more real

130
00:12:48,471 --> 00:12:54,612
world problems in your personal investing.
But here's, here's a cool question.

131
00:12:54,612 --> 00:12:59,040
Suppose a grandfather, a
great-grandfather, had invested 100 bucks,

132
00:12:59,040 --> 00:13:04,098
hundred years ago in the stock market
versus a bond. That's the kind of context.

133
00:13:04,098 --> 00:13:14,031
How much money would you have today?
Clearly you cannot have, you cannot do

134
00:13:14,031 --> 00:13:19,058
this problem easily. Right? In your head.
So but I'm going back to the problem we

135
00:13:19,058 --> 00:13:24,059
just had and I'm going to just modify it.
So how much was the interest rate

136
00:13:24,059 --> 00:13:32,006
possible? So let's start off with. Instead
of seven percent the new problem has

137
00:13:32,006 --> 00:13:39,017
either five or ten, so let's start with
five. What is N? It's very obvious that n

138
00:13:39,017 --> 00:13:44,072
was ten in my previous problem, but now
it's 100. And zero is PMT again, but how

139
00:13:44,072 --> 00:13:50,321
much, amount of money am I putting in? In
the previous problem, I put in 100, 500

140
00:13:50,321 --> 00:13:56,023
and now I'm putting in 100, right? So, if
my fingers are going all over the place

141
00:13:56,023 --> 00:14:01,080
and I punch the wrong number. We'll all
deal with it, right? I mean I'm, I'm

142
00:14:01,080 --> 00:14:08,009
teaching you one on one, I feel like you
are listening. Believe it or not, I feel

143
00:14:08,009 --> 00:14:14,046
like I can see you. But anyway, before you
think I'm really strange let's move on.

144
00:14:14,046 --> 00:14:20,422
Okay, so you have about $13,000, plus a
little bit. If, what do you do. If your

145
00:14:20,422 --> 00:14:26,612
grand great granddad, had put 100 bucks in
kind of a bond, and it had grown to, and

146
00:14:26,612 --> 00:14:32,960
of course this is your units of government
long term bond, and the government is

147
00:14:32,960 --> 00:14:38,372
still there, and so on. So remember this
number, 13,150, but the question asked

148
00:14:38,372 --> 00:14:44,037
you, how much could it be? At ten percent
and if you asked a lay person on the

149
00:14:44,037 --> 00:14:50,030
street, was probably smarter than me, but
if you just ask them because they haven't

150
00:14:50,030 --> 00:14:56,720
done finance. So suppose I change the
interest rate from five to ten. What do

151
00:14:56,720 --> 00:15:03,068
you think would happen? And I think what
that person will think is, think linearly.

152
00:15:03,068 --> 00:15:08,630
They'll try to say, oh, okay. Maybe it'll
double. So remember what was answer the

153
00:15:08,630 --> 00:15:14,264
first time. About 13,000. So, I think I
got this, everything right here, 100

154
00:15:14,264 --> 00:15:20,457
years, 100 bucks, that hasn't changed.
Look at the answer. And the answer is

155
00:15:20,457 --> 00:15:28,654
about 1.3 or $1.4 million. So what does
that tell you that it's a mind bogglingly

156
00:15:28,654 --> 00:15:35,603
dramatic change. And the culprit there is
what? Simple, who's the culprit,

157
00:15:35,603 --> 00:15:40,884
compounding? Or who's the beneficiary,
compounding? So I want you to just think

158
00:15:40,884 --> 00:15:47,312
about this for a second. And I'll go back
to the problem and show it to you, so that

159
00:15:47,312 --> 00:15:55,788
you feel comfortable with the question I
have asked you. What are the future values

160
00:15:55,788 --> 00:16:01,907
of investing 110 percent versus five%? So
what did we see? 30,000, 1.3 million if

161
00:16:01,907 --> 00:16:08,238
I'm, you know, reading it right. Huge
difference. So what's going on? Let me ask

162
00:16:08,238 --> 00:16:14,507
you this suppose there was no compounding.
Right? Suppose there was no compounding,

163
00:16:14,507 --> 00:16:20,386
which is what? Interest will be treated
like it's different. Interest cannot earn

164
00:16:20,386 --> 00:16:26,722
interest. The only thing that can earn
interest is the original 100 bucks. Let me

165
00:16:26,722 --> 00:16:33,347
ask you, with five%, how much will you
have after 100 years? And you should be

166
00:16:33,347 --> 00:16:40,134
able to answer that question, very easily.
And the reason is very simple. Simple

167
00:16:40,134 --> 00:16:45,630
interest rate is adjective. It's linear.
We are very good at it. So let's take,

168
00:16:45,630 --> 00:16:52,200
will the 100 bucks still be there? Sure.
But every year how much will I be getting?

169
00:16:52,200 --> 00:16:57,900
Five percent of 100 bucks is five bucks.
After 100 years of five bucks how much is

170
00:16:57,900 --> 00:17:04,033
it? 500. So you see how simple it is, that
you have 500 bucks, five bucks at a time

171
00:17:04,033 --> 00:17:09,694
for 100 years, plus the original 600,
which was our answer. Our answer was

172
00:17:09,694 --> 00:17:15,164
13,000. Alright? So what's the, where's
the difference coming from? Compounding,

173
00:17:15,164 --> 00:17:19,425
you see? [laugh]. It's very, very
unbelievable. You know, you can't

174
00:17:19,425 --> 00:17:24,761
visualize this stuff. But let's go to the
more diff, more, the second problem. So

175
00:17:24,761 --> 00:17:29,966
now I intrigue, increase the interest from
five to ten%. How much am I getting every

176
00:17:29,966 --> 00:17:34,827
year, on the 100 bucks? Well, twice as
much. I was getting five bucks first, now

177
00:17:34,827 --> 00:17:40,443
I'm getting ten bucks. What is ten bucks
times 100, 100 a year. Thousand, so how

178
00:17:40,443 --> 00:17:45,589
much do I have? I have a hundred bucks
plus another thousand bucks. Sounds pretty

179
00:17:45,589 --> 00:17:51,799
reasonable? But what was the answer at ten
%, more than a million dollar? So you see

180
00:17:51,799 --> 00:17:58,038
what's happening, two things are
happening. Compounding is very tough to

181
00:17:58,038 --> 00:18:04,031
understand but it's real. It's been
happening. People have made money with

182
00:18:04,031 --> 00:18:09,236
risk of, obviously. However. What's even
more complicated is that comp, the

183
00:18:09,236 --> 00:18:14,724
comparison with compounding between five
and ten becomes a total nightmare. It's

184
00:18:14,724 --> 00:18:21,013
very different, difficult to comprehend.
Because it just blows in your face. If you

185
00:18:21,013 --> 00:18:26,554
want to, if you want think about a really
cool example. Actually provided in I teach

186
00:18:26,554 --> 00:18:33,034
executives with a couple of my colleagues.
And this is borrowed from their example.

187
00:18:33,034 --> 00:18:39,052
And it's a real world scenario. So, read
this for a second. Peter Minuit, if I'm

188
00:18:39,052 --> 00:18:46,571
saying that right, by the way I don't
speak French, so if I've screwed up his

189
00:18:46,571 --> 00:18:54,283
name pardon me, everybody screws up my
name, so no big deal. Peter Minuit bought

190
00:18:54,283 --> 00:19:01,852
the Manhattan Island from native Americans
for 24 bucks in 1626, right. Suppose the

191
00:19:01,852 --> 00:19:08,864
native Americans decided, had decided not
to. It's just had decided to sell the

192
00:19:08,864 --> 00:19:13,535
land. And then taken the money of 24
bucks. And put it as a "financial

193
00:19:13,535 --> 00:19:18,703
investment". At about six%. Whey am I
choosing six%? Because it's not neither

194
00:19:18,703 --> 00:19:24,221
too high. It's neither too low. Though,
you don't know what interest rates will be

195
00:19:24,221 --> 00:19:29,803
like in the future. Given what's happening
now. But lets stick with six%. I Think

196
00:19:30,009 --> 00:19:36,997
just as an example. How much would the
native Americans have, in the bank today.

197
00:19:36,997 --> 00:19:45,913
So this is your problem to solve, and it
is not an easy one. So let's try and see

198
00:19:45,913 --> 00:19:48,088
how would we do this.
