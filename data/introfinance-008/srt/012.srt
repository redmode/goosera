1
00:00:02,014 --> 00:00:09,088
Hi, welcome to week two. I wanted to give
you some idea of what we did last week.

2
00:00:09,088 --> 00:00:17,019
And some idea of what we are going to do
next, this week before we jump into the

3
00:00:17,019 --> 00:00:24,005
week. And I'll tend to do that all the
time, so that we are on the same page. So,

4
00:00:24,032 --> 00:00:29,714
recap of last week. I think the main
things we spoke about are two, if it can

5
00:00:29,714 --> 00:00:37,006
fall into two chunks. The first was the
philosophy of the class or the content of

6
00:00:37,006 --> 00:00:42,059
the class. And I would like to, again,
emphasize that, although a lot of

7
00:00:42,059 --> 00:00:49,584
requirements and prerequisites could be
there, it's an applied field. It's borrows

8
00:00:49,584 --> 00:00:55,422
from so many disciplines. And it's become
almost a discipline of its own. We will

9
00:00:55,422 --> 00:01:01,335
try to do as much during the videos as
possible so that you don't feel like you

10
00:01:01,335 --> 00:01:07,020
are in anyway handicapped by not having
done one of the background courses.

11
00:01:07,020 --> 00:01:12,873
However, when I do reach a point, where I
think it's appropriate not to keep going,

12
00:01:12,873 --> 00:01:19,023
I will let you know and at that point what
you will realize is that you need to do X

13
00:01:19,023 --> 00:01:24,040
before you can do more of finance. But
this is self contained, I want to

14
00:01:24,040 --> 00:01:30,073
emphasize that. Then we moved on last, for
last week, we moved on to content, because

15
00:01:30,073 --> 00:01:37,006
we have ten weeks and we got to keep
moving. And I told you that lot of what's

16
00:01:37,006 --> 00:01:42,068
going to happen in this class, is really
work that you will do. That's my

17
00:01:42,068 --> 00:01:49,028
philosophy of teaching, is that, I know a
little bit, I can motivate you to learn, I

18
00:01:49,028 --> 00:01:54,738
can show you wild things and that's my
job. But a lot of actual practice that you

19
00:01:54,738 --> 00:02:01,591
need to do in finance with the real world
problems has to happen at your end. So we

20
00:02:01,591 --> 00:02:07,077
will introduce everything with examples,
and last week this is what we did. We

21
00:02:07,077 --> 00:02:14,522
basically covered two issues and the
first, and I'll just draw a timeline. And

22
00:02:14,522 --> 00:02:24,364
we started with something here. And as for
the sake of just reference, draw just two,

23
00:02:24,364 --> 00:02:31,375
two points. And remember, we started with
one, one period and went on to two and

24
00:02:31,375 --> 00:02:38,507
more. But we carried this forward, say, to
something called future value. And then,

25
00:02:38,507 --> 00:02:45,745
after recognizing that, we also did what
does the PV of something i n the future

26
00:02:45,745 --> 00:02:54,013
look like. And as you can see from this,
the future value turns out of this will be

27
00:02:54,013 --> 00:03:02,241
P, which is the initial value times (one +
r)^2. Right? And that two can become n.

28
00:03:02,241 --> 00:03:09,470
And we got very familiar with going back
and forth with all of this. So what is PV?

29
00:03:09,470 --> 00:03:14,635
Well, PV is pretty straightforward. It's
the future value in this case (one + r)^2

30
00:03:14,791 --> 00:03:21,136
and you can recognize how similar the two
are. One is like looking at yourself in a

31
00:03:21,136 --> 00:03:26,968
mirror. And that's what I like about
finances. It is very logical, I warn you

32
00:03:26,968 --> 00:03:32,771
again though all the answers that you are
going to get are wrong [laugh]. So, the

33
00:03:32,771 --> 00:03:38,445
process is right, the answers are wrong.
And the reason the answers are wrong, is

34
00:03:38,445 --> 00:03:44,416
because you're worrying about the future,
not the past, and for the time being we're

35
00:03:44,416 --> 00:03:49,505
assuming there's no uncertainty, but I
remember it, I reminded you many times

36
00:03:49,505 --> 00:03:55,208
that at the back of your mind, risk will
always be there, and that's what's real,

37
00:03:55,208 --> 00:04:01,601
right? So if, because of risk, in some
senses everything you calculate is wrong,

38
00:04:01,601 --> 00:04:08,190
but the process and the way of thinking
and the framework is simply awesome. So, I

39
00:04:08,190 --> 00:04:13,537
will encourage you to go back and do
couple of things even before you proceed.

40
00:04:13,537 --> 00:04:18,794
First, and this I will say every week even
though it may become repetitive, I think

41
00:04:18,794 --> 00:04:24,174
it is important. It's all the more
important because you are controlling your

42
00:04:24,174 --> 00:04:29,650
learning, I am not. In a classroom I have
a lot more control. Everybody is there and

43
00:04:29,650 --> 00:04:34,771
so on and so forth. But here, you are in
control and that's the way it should be,

44
00:04:34,771 --> 00:04:39,631
right? And I am hoping that online
education forces regular education to

45
00:04:39,631 --> 00:04:45,059
become individualized. You know, as I said
last time I feel frustrated sometimes

46
00:04:45,059 --> 00:04:50,211
that, I cannot teach everybody the way
they want to learn in a big classroom

47
00:04:50,211 --> 00:04:55,211
because I'm doing it in real time.
Hopefully online can allow me to teach

48
00:04:55,211 --> 00:05:01,183
each person at a time, but that puts some
pressure on you to perform. So, this is

49
00:05:01,183 --> 00:05:07,007
what I expect every week, and I'll repeat
it. And obviously, you can ignore me. I

50
00:05:07,007 --> 00:05:12,239
mean, this is your life, not mine. But
it'll help, especially in finance. These

51
00:05:12,239 --> 00:05:18,285
are the co uple of things you wanna do.
You want to make sure you understand the

52
00:05:18,285 --> 00:05:23,696
video. The good news is, you can go back
and forth. But I will not provide

53
00:05:23,696 --> 00:05:29,285
ressources specific to the video. Any
resources available are standard text

54
00:05:29,285 --> 00:05:36,054
books or I will give you specific formulas
and so on, I'll write up and put on the

55
00:05:36,054 --> 00:05:41,876
website. And there are several reasons for
this. And one is, fundamental reason is, I

56
00:05:41,876 --> 00:05:47,609
want you to learn actively. I think I
spoke last time. If you're sitting there

57
00:05:47,609 --> 00:05:53,369
and just consuming, which unfortunately is
most of education, it's not learning in my

58
00:05:53,369 --> 00:05:58,135
book. For learning, you want to be there.
So it's, nothing says about humanity that

59
00:05:58,135 --> 00:06:03,338
you need to get up at seven and be in a
class at eight and then after that every

60
00:06:03,338 --> 00:06:08,542
two hours the bell rings and you have to
change your mindset to learn something

61
00:06:08,542 --> 00:06:13,249
new. That's all artificial. It's like a
factory. Humans are much more complicated

62
00:06:13,249 --> 00:06:17,439
and interesting people. So, I will
encourage you to learn on your own.

63
00:06:17,439 --> 00:06:23,367
However, review the video and make your
own notes from the video. It'll really

64
00:06:23,367 --> 00:06:28,890
help. Second, every week we have
assignments. And last week's assignments

65
00:06:28,890 --> 00:06:34,277
were a little bit easy, because we didn't
do that much. However, you need to

66
00:06:34,277 --> 00:06:39,576
familiarize yourself with the assignment
because finance is logical. So, finance is

67
00:06:39,576 --> 00:06:44,323
not, one day I say one thing and the next
day I say something that's totally

68
00:06:44,323 --> 00:06:49,029
contradictory, which by the way happens in
life. People say so many things I'm, you

69
00:06:49,029 --> 00:06:53,604
know on the one hand, on the second hand,
God gave us only two hands but sometimes

70
00:06:53,604 --> 00:06:58,448
people make fifteen different scenarios
from the same issue. Finance keeps you

71
00:06:58,448 --> 00:07:03,184
disciplined, so if, if you want to use the
framework repeatedly, you've got to do the

72
00:07:03,184 --> 00:07:07,994
problems. So, last week's problems you
should have done by now. If not, I would

73
00:07:07,994 --> 00:07:13,832
encourage you right now to stop watching
the video and redo those assignments or do

74
00:07:13,832 --> 00:07:18,961
them again, if you've tried to done them
in a, you know, fast fashion or not

75
00:07:18,961 --> 00:07:24,500
completely. Or if you haven't done them at
all, do them and then watch this video

76
00:07:24,500 --> 00:07:29,720
because it'll make your life easier and
I'll be happy. And remember, I can see, I

77
00:07:29,720 --> 00:07:35,281
can feel you. So it's, it's, it's not
something I'm saying in isolation. I mean

78
00:07:35,281 --> 00:07:38,008
it and I hope you take this advice.
