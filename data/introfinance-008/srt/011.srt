1
00:00:03,118 --> 00:00:08,574
Hi. I hope you have had a nice summer
until now. And, as promised, we are

2
00:00:08,574 --> 00:00:14,125
launching the introduction to finance
class and I'm really happy to do this

3
00:00:14,125 --> 00:00:19,678
class for you. It's free, it's online, and
we'll talk a little bit more about that

4
00:00:19,678 --> 00:00:26,750
aspect of it. But first, I wanted to just
tell you a little bit about finance. So,

5
00:00:26,750 --> 00:00:35,758
why finance? Turns out finance is the
study of value, and value is what I

6
00:00:35,758 --> 00:00:42,558
believe all humans are after. There is
this notion that finance is about money,

7
00:00:42,558 --> 00:00:48,865
but you'll see today, I won't talk about
money too much. And the reason is, because

8
00:00:48,865 --> 00:00:55,279
money is just a tool. And finance is a
more, much more fundamental understanding

9
00:00:55,279 --> 00:00:59,576
of value. And I think that's an important
distinction to make. Later on I'll

10
00:00:59,576 --> 00:01:07,322
obviously, in examples that I'll use. I
will use, a lot of money language. But I

11
00:01:07,322 --> 00:01:12,893
want you to understand this class is not
about money, it's about value. And, that's

12
00:01:12,893 --> 00:01:21,573
extremely critical to what we are going to
do. Value creation has two key components

13
00:01:21,573 --> 00:01:28,341
and those are central to finance. And one
is time, and the other is uncertainty. And

14
00:01:28,341 --> 00:01:33,653
the key element of the time and
uncertainty is that, just time alone is

15
00:01:33,653 --> 00:01:38,933
tough to understand. But when you throw in
time and uncertainty, it becomes a little

16
00:01:38,933 --> 00:01:42,981
bit more complicated. So, what we are
going to do in these class is, we are

17
00:01:42,981 --> 00:01:47,837
going to try to understand time on it, by
itself a lot, what are the implications of

18
00:01:47,837 --> 00:01:54,142
time for assessing value and then we'll
later, on what are the complexities

19
00:01:54,142 --> 00:01:59,813
imposed by uncertainty. And I, I want you
do be a little patient in that respect

20
00:01:59,813 --> 00:02:04,285
because uncertainty is always there and,
so you'll need to kind of, think through

21
00:02:04,285 --> 00:02:08,403
issues a little bit carefully and not
worry too much about uncertainty until we

22
00:02:08,403 --> 00:02:15,331
get to it. The third element about this
which I really, really like, is that

23
00:02:15,331 --> 00:02:21,587
almost anything can be valued using the
framework we'll learn. And it's a way of

24
00:02:21,587 --> 00:02:28,514
thinking, it's pretty remarkable
that, give me whatever asset you want to

25
00:02:28,514 --> 00:02:35,146
value, whether it's a corporation, whether
it's a particular project, or whether it's

26
00:02:35,146 --> 00:02:41,853
even human capital.  What is our value? And
remember even though money will be used

27
00:02:41,853 --> 00:02:48,477
across as the most obvious way to value
things, value doesn't come necessary only

28
00:02:48,477 --> 00:02:54,357
from money. I think you know what I'm
talking about. It's a way of thinking and

29
00:02:54,357 --> 00:02:59,888
a set of tools. And to be honest, I've been in this profession for a long

30
00:02:59,888 --> 00:03:05,272
time and I, I've considered myself very
fortunate to have come across finance,

31
00:03:05,272 --> 00:03:11,135
because I don't think I've seen anything
else that is both the way of thinking and

32
00:03:11,135 --> 00:03:19,399
provides a set of tools. So it's like to
me, it's both art and science. And the way

33
00:03:19,399 --> 00:03:26,010
of thinking has a lot of science to it,
lot of rationality to it but a lot of art

34
00:03:26,010 --> 00:03:31,591
to it which is, what attracts me to it. On
the other hand, you'll see we'll use

35
00:03:31,591 --> 00:03:38,600
Excel, we'll use whatever, computers
technology. Those tools are extremely

36
00:03:38,600 --> 00:03:44,823
important but I will emphasize throughout
this class the way of thinking rather than

37
00:03:44,823 --> 00:03:50,315
the set of tools and we'll see that as we go along. Most importantly,

38
00:03:50,315 --> 00:03:55,166
I love this class, I love these topics,
simply because all of us are making

39
00:03:55,166 --> 00:04:01,926
decisions all the time and it is so
applicable. It's one aspect of economics,

40
00:04:01,926 --> 00:04:06,893
which is another discipline, which is
extremely applicable. So I find that

41
00:04:06,893 --> 00:04:13,534
attractive because we can make decision-making much more transparent and much more

42
00:04:13,534 --> 00:04:19,957
value based. So, I want you to think about
these issues as you think about this class

43
00:04:19,957 --> 00:04:24,093
and if these things really mean something
to you, both at a personal and a

44
00:04:24,093 --> 00:04:28,272
professional level, take this class and
use this class to motivate

45
00:04:28,272 --> 00:04:35,334
yourself to learn. A little bit about the
syllabus and the content. First of all,

46
00:04:35,334 --> 00:04:41,774
this is a ten week class and the syllabus
is on the website so, I'm not going to

47
00:04:41,774 --> 00:04:45,286
through the syllabus in detail. If there's
one thing I'll ask you to do, if you

48
00:04:45,286 --> 00:04:50,151
haven't already done it, is to get a look
at the syllabus. Just broadly speaking

49
00:04:50,151 --> 00:04:54,702
this is what we'll spend about two weeks
on what is called time value of

50
00:04:54,702 --> 00:04:59,904
money, understanding how to value benefit
and costs over time. And as I said,

51
00:04:59,904 --> 00:05:05,977
initially we will ignore uncertainty. The
next couple of weeks are decision

52
00:05:05,977 --> 00:05:12,934
criteria, which are largely based on the
first topic about how do you make value

53
00:05:12,934 --> 00:05:17,958
enhancing decisions and I will spend a lot
of time on decisions that are more

54
00:05:17,958 --> 00:05:22,470
personally motivated, because I think that
you'll relate to that stuff much better

55
00:05:22,470 --> 00:05:30,319
than making a corporate decision. However,
a lot of you work or are going to work in

56
00:05:30,319 --> 00:05:35,074
life and it's good to realize that the
decision criteria are the same. So, it

57
00:05:35,074 --> 00:05:39,078
doesn't matter what decision you're
making. Hopefully, you're not making

58
00:05:39,078 --> 00:05:44,678
drawing, you know, graphs and using Excel
to make personal decisions all the time,

59
00:05:44,678 --> 00:05:49,528
that would be a bit much. But, I would
recommend very strongly to keep the

60
00:05:49,528 --> 00:05:55,881
framework in mind and recognize you can
always use it. Two weeks on, the two

61
00:05:55,881 --> 00:06:04,057
fundamental ways of financing any idea or of
borrowing for a company or of you,

62
00:06:04,057 --> 00:06:09,120
investing in something. And the two,
securities, financial securities is what

63
00:06:09,120 --> 00:06:14,412
they call them, are bonds and stocks. And
we will spend two weeks on bonds and

64
00:06:14,412 --> 00:06:20,139
stocks to understand them, because it's
very critical to understanding finance.

65
00:06:20,139 --> 00:06:25,377
Two weeks, then entirely devoted to risk
and uncertainty. And as I said before,

66
00:06:25,377 --> 00:06:28,810
until this point, I'm going to stay away
from risk and uncertainty. Otherwise,

67
00:06:28,810 --> 00:06:33,197
you'll be drinking at a fire hydrant.
It's, it gets to be a bit much. But we

68
00:06:33,197 --> 00:06:39,463
will go linearly and introduce risk and
uncertainty, probably my favorite topic in

69
00:06:39,463 --> 00:06:44,479
Finance, because it is really cool and it
can get quite complicated but we'll keep

70
00:06:44,479 --> 00:06:51,111
it very simple and basic. One week putting
it all together and the final week is a

71
00:06:51,111 --> 00:06:56,399
review and the final exam. So, that
gives you sense of the syllabus and I hope

72
00:06:56,399 --> 00:07:01,098
that you do take the time to review it. In
fact, that's your first homework, to get to

73
00:07:01,098 --> 00:07:05,964
know the syllabus inside out. I'm now
going to move over to another aspect of

74
00:07:05,964 --> 00:07:13,114
the class, which is extremely important to
understand. This first video gives you a

75
00:07:13,114 --> 00:07:18,394
reflection of the whole course. So, please
spend some time watching it. So, what's

76
00:07:18,394 --> 00:07:23,886
the process? Some of the features: the
class will run synchronously, one week at a

77
00:07:23,886 --> 00:07:29,259
time. And the reason I'm doing that is I
wanted to be very much like a class in

78
00:07:29,259 --> 00:07:34,738
real time, as if I'm teaching you face to
face. There are a couple of reasons for

79
00:07:34,738 --> 00:07:41,992
it. One, I think what happens is, a lot of
learning online occurs among yourselves.

80
00:07:41,992 --> 00:07:46,417
So, I will talk about my role in second
year and your role. But I think it's very

81
00:07:46,417 --> 00:07:51,593
important to keep it synchronous. The
second reason I think is equally important

82
00:07:51,593 --> 00:07:56,842
and that is: you need time for things to
assimilate. This is going to be a

83
00:07:56,842 --> 00:08:02,427
challenging class simply for two reasons.
One, I'm giving you a framework. Two, the

84
00:08:02,427 --> 00:08:10,379
framework will be used over and over to
apply to real world problems.

85
00:08:10,379 --> 00:08:15,379
The real world problems will be simplified a
little bit but they'll retain more stuff,

86
00:08:15,379 --> 00:08:21,853
there will be more features. So you, you
need time to do that learning. You cannot

87
00:08:21,853 --> 00:08:25,990
just watch a video and say, I got it. If
you got it, that's pretty cool, but I

88
00:08:25,990 --> 00:08:31,931
think practice is what makes things
perfect. The main method of delivery is

89
00:08:31,931 --> 00:08:37,427
going to be videos. And I want to talk a
little bit about this. I want the videos

90
00:08:37,427 --> 00:08:45,540
to be the main resource that you will
have. I will do my best to make things

91
00:08:45,540 --> 00:08:52,020
meaningful to you, to make them exciting,
and break them up into pieces that you can

92
00:08:52,020 --> 00:08:57,480
watch one at a time, even within
one week session, so that you are at the

93
00:08:57,480 --> 00:09:03,322
comfort level to understand things in byte
size pieces. However, the good news is you

94
00:09:03,322 --> 00:09:07,848
can stop watching anytime, you can restart
anytime so you take care of that

95
00:09:07,848 --> 00:09:13,760
flexibility, use it as you go along. What
are the supporting materials? I'll give

96
00:09:13,760 --> 00:09:18,990
you a list of text books and they will be
listed in the syllabus. And the

97
00:09:18,990 --> 00:09:23,842
relevant chapters are highlighted according to
the sessions I am doing. So, there are ten

98
00:09:23,842 --> 00:09:29,562
sessions, ten weeks worth of video and
match the textbooks with the relevant

99
00:09:29,562 --> 00:09:35,346
chapters and that's how we'll manage the
supporting material. There will be some

100
00:09:35,346 --> 00:09:41,097
minimal notes I'll provide you, like the
syllabus, some formulas, some ways of

101
00:09:41,097 --> 00:09:48,984
doing Excel, and so on and so forth. But I
do not want you to focus too much on

102
00:09:48,984 --> 00:09:53,671
materials that I give you. I think that,
that can go across as spoon-feeding and so

103
00:09:53,671 --> 00:09:58,728
on. So, I want you to learn on your own
and I think that's part of what this class

104
00:09:58,728 --> 00:10:03,783
is all about. Assessments. I'll have
assignments every week and that fits in

105
00:10:03,783 --> 00:10:10,068
with the philosophy of the class. It's a really cool topic and the coolness

106
00:10:10,068 --> 00:10:13,621
comes from the fact that it's so
applicable. And so what I'm going to do,

107
00:10:13,621 --> 00:10:18,245
is I'm going to give you assignments and
you will be required to submit them and

108
00:10:18,245 --> 00:10:24,868
all grading will be done by computers.
Final point which I have been told

109
00:10:24,868 --> 00:10:29,747
repeatedly by people who have experimented
with this and have done long distance

110
00:10:29,747 --> 00:10:35,246
teaching too. What's extremely important,
given that you're not physically in the

111
00:10:35,246 --> 00:10:42,325
classroom together, is the interaction
among yourselves. That's very important,

112
00:10:42,325 --> 00:10:47,645
peer-to-peer learning and you have to
recognize that the class is so big and

113
00:10:47,645 --> 00:10:51,829
you're all over the world that the
interaction between faculty of teaching

114
00:10:51,829 --> 00:10:58,238
assistants cannot be part of the class.
Otherwise, you know, it will

115
00:10:58,238 --> 00:11:03,462
become not manageable for people. So, I
want you to recognize that last point. So,

116
00:11:03,462 --> 00:11:09,613
the process is as important as any other
aspect of understanding what this class

117
00:11:09,613 --> 00:11:16,757
is all about and I hope that helps. I'm
now going to go to my favorite topic, and

118
00:11:16,757 --> 00:11:23,837
which is what is the philosophy of this
class and why did I choose to do it, why

119
00:11:23,837 --> 00:11:30,453
do I teach? So, the first is, how does
learning happen? And I'm going to give you

120
00:11:30,453 --> 00:11:36,061
examples of what the real world thinks,
how learning happens. I think most of the

121
00:11:36,061 --> 00:11:41,717
time, learning happens in two hour blocks,
people sit quietly in the room, and one

122
00:11:41,717 --> 00:11:47,573
guy does the talking or one woman does the
talking. I really don't think that's how

123
00:11:47,573 --> 00:11:52,692
learning should, really happens in the
world. Learning happens when you're happy.

124
00:11:52,692 --> 00:11:57,973
You want to listen. You want to listen to
not just the person in front of you, like

125
00:11:57,973 --> 00:12:03,437
me. You want to listen to yourself. And I
think that's such a key important aspect

126
00:12:03,437 --> 00:12:09,935
of life. I have never learned anything
unless I had wanted to learn it, and I've,

127
00:12:09,935 --> 00:12:15,265
unless I'm happy. So the good news is,
this is one of the strengths of online, is

128
00:12:15,265 --> 00:12:20,610
that, you, when you're happy, do this video watching. When you're open, when

129
00:12:20,610 --> 00:12:24,856
your mind is ready to take it. You start
out one day, and the coffee hasn't kicked

130
00:12:24,856 --> 00:12:30,464
in, stop the video. Watch it when it's
meaningful to you. I think that's

131
00:12:30,464 --> 00:12:35,040
extremely important. You are the center of
the learning and learning happens when

132
00:12:35,040 --> 00:12:41,607
you're relaxed, you're happy and you want
to learn. The second, where does learning

133
00:12:41,607 --> 00:12:47,743
happen? And this is kind of related to the
first issue, learning happens still in

134
00:12:47,743 --> 00:12:51,894
very structured two hour time blocks. Who
the heck came up with that idea? I don't

135
00:12:51,894 --> 00:12:55,007
know, but we kind of follow it as if you
know, that's the best thing invented by

136
00:12:55,007 --> 00:13:01,780
humanity. That's a bunch of garbage.
Learning happens, hopefully, in it's own

137
00:13:01,780 --> 00:13:07,267
organic way. And this is where again I
think the online class allows you that

138
00:13:07,267 --> 00:13:11,667
flexibility. You don't have to come to
class, you don't have to sit in front of,

139
00:13:11,667 --> 00:13:16,811
with forty people. There's a downside to
that, a hundred people. You'll be learning

140
00:13:16,811 --> 00:13:23,525
by yourself, and taking advantage of the
peers in your large group of students

141
00:13:23,525 --> 00:13:27,867
that, that are learning with you. So
what's the role of the teacher and the

142
00:13:27,867 --> 00:13:35,371
faculty? And that's me in this case. I
think I have taught since 1985, I've

143
00:13:35,371 --> 00:13:40,035
taught before that too, but from 1985
until now I've been at the Ross School of

144
00:13:40,035 --> 00:13:49,219
Business. I've traveled to other places. I
see my role largely as someone who

145
00:13:49,219 --> 00:13:55,083
shows you the beauty of finance. Who shows
you and motivates you as to why you would

146
00:13:55,083 --> 00:14:01,648
want to learn this cool subject. My role
is not to spoon-feed you, because I think

147
00:14:01,648 --> 00:14:07,739
it's not empowering you. So, if you want
to consume and just want to learn

148
00:14:07,739 --> 00:14:14,165
through consumption and watching and just
input, that's not what my role is. I'll do

149
00:14:14,165 --> 00:14:18,476
my best in the videos to show you the
beauty of finance, but I won't hold your

150
00:14:18,476 --> 00:14:26,020
hand, because I feel that a faculty member
is a coach, not a person who spoon-feeds,

151
00:14:26,020 --> 00:14:29,463
but a person who actually challenges you to be the best you

152
00:14:29,463 --> 00:14:37,668
can be. Related to that, this class will
be about why things work the way they do.

153
00:14:37,668 --> 00:14:43,605
I will, as I said, It's very applied, I
will introduce formulas through

154
00:14:43,605 --> 00:14:48,221
examples rather than the other way around
most of the time. There is a lot of math,

155
00:14:48,221 --> 00:14:53,264
and so on so forth, a lot of structure. But
I'm much more interested in you

156
00:14:53,264 --> 00:14:59,388
understanding why things work the way they
do. The difference between people who are

157
00:14:59,388 --> 00:15:04,061
successful in life. And I don't mean money
again, I mean just successful and

158
00:15:04,061 --> 00:15:09,921
happy. And the difference between people
who struggle in life is the fact, is the

159
00:15:09,921 --> 00:15:16,357
difference that some of us understand the,
"why things work" idea. So it's like, if you

160
00:15:16,357 --> 00:15:21,930
understand why things work the way they do
and you recognize the beauty of finance

161
00:15:21,930 --> 00:15:28,034
that it's a framework, it's not something
that you have to memorize. It's not

162
00:15:28,034 --> 00:15:32,737
something you Google. If you know the "why"
of things and that's what finance is all

163
00:15:32,737 --> 00:15:39,622
about, you'll never have to go back to
your notes, worry about google'ing stuff and

164
00:15:39,622 --> 00:15:44,307
so on. You can start from scratch and
tackle any problem you want to.

165
00:15:45,322 --> 00:15:48,876
What is the role of the student? I think it's
becoming pretty obvious what your role is,

166
00:15:48,876 --> 00:15:54,586
but I want to reiterate it. I think if you
are in the mood of, "I'm going to sit there

167
00:15:54,586 --> 00:15:59,643
and just consume", I would say that's not
the role of the student. The student is

168
00:15:59,643 --> 00:16:04,795
the center of learning. And I think
therefore, with the, being in the center

169
00:16:04,795 --> 00:16:09,546
comes the responsibility to learn. And I
think it's extremely important that you

170
00:16:09,546 --> 00:16:14,682
grab this responsibility. And remember,
people are worried about online teaching

171
00:16:14,682 --> 00:16:21,820
and how they will be cheating, how there
is no involvement directly

172
00:16:21,820 --> 00:16:27,444
and watching of what's going on by the
faculty or the administrators, I'm not

173
00:16:27,444 --> 00:16:32,556
worried about that because I think you
have a "Jiminy Cricket", I mean if you've

174
00:16:32,556 --> 00:16:38,108
watched Pinocchio, whenever he lied, you
know Jiminy Cricket was his conscience,

175
00:16:38,108 --> 00:16:43,593
his nose grew longer. And you are your
best Jiminy Cricket, I mean I can do all

176
00:16:43,593 --> 00:16:49,970
the watching I want in my class, but I
think if you know you're crossing a line,

177
00:16:49,970 --> 00:16:53,201
that's all you need to know, right? And so
your moral compass is better than

178
00:16:53,201 --> 00:16:59,427
anything I can give you. And I tell you: learn,
learn on your own. Learning is a

179
00:16:59,427 --> 00:17:05,055
collective exercise, but you do the work
on your own. And that's how you learn.

180
00:17:05,055 --> 00:17:11,384
That's how you'll develop and I encourage
you to take a very active role, especially

181
00:17:11,384 --> 00:17:18,047
in the online class. In going and
satisfying your curiosity and trying to do

182
00:17:18,047 --> 00:17:24,115
things that you always wanted to do. Who
is ultimately responsible for learning?

183
00:17:24,115 --> 00:17:29,642
So, the answer is, I hope that you know by
now, the answer is you. And I am just a

184
00:17:29,642 --> 00:17:37,839
facilitator. And I just dream of making
learning happen for you in any way I can

185
00:17:37,839 --> 00:17:43,859
help, except I won't spoon-feed. I will
challenge you, and I will hopefully make

186
00:17:43,859 --> 00:17:50,363
you learn through that process. Devote
about five to ten hours a week. And this,

187
00:17:50,363 --> 00:17:55,316
I'm putting under philosophy, simply
because I want to emphasize that while

188
00:17:55,316 --> 00:18:00,351
online learning makes things very easy for
you, you can learn at your own time and

189
00:18:00,351 --> 00:18:05,333
you're happy and so on, it also imposes
a certain discipline on you. And I

190
00:18:05,333 --> 00:18:10,665
think if you keep five to ten hours a
week, every week, by the end of the class,

191
00:18:10,665 --> 00:18:16,057
you won't need to study for the final.
That's the cool thing about finance. So,

192
00:18:16,057 --> 00:18:22,252
is the class really free? Humm, I don't
think so. And this goes back to the point

193
00:18:22,252 --> 00:18:28,919
of money, association of money and cost or
benefit. I think you should think of the

194
00:18:28,919 --> 00:18:33,288
effort you'll put in. So, the class is for
free in the sense you're not paying money.

195
00:18:33,288 --> 00:18:38,146
But I'm going to make you work hard. So,
in fact harder than if you were sitting

196
00:18:38,146 --> 00:18:42,879
in my classroom and if you are willing to do
that, the class is obviously not free.

197
00:18:42,879 --> 00:18:49,520
Class requires effort and it will
be pretty deep and hard for you. I'm

198
00:18:49,520 --> 00:18:53,538
challenging you, I'm not discouraging you,
but I want you to know, you'll do the

199
00:18:53,538 --> 99:59:59,000
learning, and I will be the facilitator,
and we'll have fun together.
