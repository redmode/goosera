1
00:00:00,000 --> 00:00:05,074
So we've done this problem. And this
problem is a PMT problem. However, what's

2
00:00:05,074 --> 00:00:11,095
given to you here? The PMT's given to you,
you figured it out. Now, I'm going to do a

3
00:00:11,095 --> 00:00:17,092
future value problem with PMT figuring in,
but exactly the opposite. So, let's go

4
00:00:17,092 --> 00:00:27,079
there. And then I will take a natural
break. Okay so, the next problem. Let's

5
00:00:27,079 --> 00:00:35,031
just read it, and by the way, if at this
point you're feeling little tired, you had

6
00:00:35,031 --> 00:00:42,080
too much of future value of an annuity,
take a break. That's okay, because I think

7
00:00:42,080 --> 00:00:49,012
it's much more important you understand
bite size pieces. And you can always join

8
00:00:49,012 --> 00:00:55,029
me in a minute, I am not going anywhere,
I'm here, okay? Okay so, let's get started

9
00:00:55,029 --> 00:01:01,062
with example number two of an annuity. So,
I'm going to again draw a time line. And

10
00:01:01,062 --> 00:01:07,465
hopefully, we'll get so familiar with
doing this, we're going to, I promised

11
00:01:07,465 --> 00:01:13,509
myself today that I'm going to go slow.
And I'm going to squeeze every ounce of

12
00:01:13,509 --> 00:01:19,048
energy from a problem. And that's what
computative finance is. So, zero here and

13
00:01:19,048 --> 00:01:28,534
how much here? 25. Right? Now, what do I
know in this problem? I know that per year

14
00:01:28,534 --> 00:01:35,980
the interest rate again is eight percent.
Just for simplicity the same number, the

15
00:01:35,980 --> 00:01:42,756
numbers will changed, depending on who you
are. And now I say, I know the future

16
00:01:42,756 --> 00:01:50,148
value. And I'm using dollars again, just
for simplicity, right? So just, let's

17
00:01:50,148 --> 00:01:56,746
pause. Like last time, I jumped straight
into PMT, knowing that it's a PMT problem,

18
00:01:56,746 --> 00:02:03,471
but it's not the kind of problem that
dictates what you're looking for, it is

19
00:02:03,471 --> 00:02:09,756
what information you need to be answered.
So here, I know future value. So, the

20
00:02:09,756 --> 00:02:16,665
question that's being asked is suppose you
want $500,000 when you retire 25 years

21
00:02:16,665 --> 00:02:22,878
from now. How much must you invest each
year starting at the end of this year if

22
00:02:22,878 --> 00:02:28,176
the interest rate is eight%? Now I repeat
again. I'm choosing eight%, but actually

23
00:02:28,176 --> 00:02:33,898
you are choosing eight%. Not the exact
number, but the strategy. And for eight%,

24
00:02:33,898 --> 00:02:39,752
you'd better be invested in something
risky. You're aren't going to get eight

25
00:02:39,752 --> 00:02:45,510
percent from the bank, okay? So, $500,000
you need at retirement, right? And you're

26
00:02:45,510 --> 00:02:51,731
using eight%. So, let me ask you this,
the, when we go to Excel in a second, you

27
00:02:51,731 --> 00:02:58,497
will use the PMT function. Why? Because
that's the guy I don't know. And that's

28
00:02:58,497 --> 00:03:07,304
the guy I'm trying to solve for. Right?
Okay, let's do it. Okay. The good news is

29
00:03:07,304 --> 00:03:19,349
I have the same problem set up over there,
but now what do I do? And this actually

30
00:03:19,349 --> 00:03:26,713
helps. I have the last Problem. What do I
do, I change FV to PMT. Why did I do that,

31
00:03:26,713 --> 00:03:32,849
because as I said earlier, in this
particular example I do not know one of

32
00:03:32,849 --> 00:03:39,701
them, and that's PMT. What's the interest
rate, eight%. How many years? In the

33
00:03:39,701 --> 00:03:45,770
previous problem it was forty, I mean it
was forty years, right. In this problem,

34
00:03:45,770 --> 00:03:53,123
We have I believe twenty five, and if I
make a mistake. That's one of the times

35
00:03:53,123 --> 00:04:00,641
you can catch me, and fix it faster than
me. Okay? [laugh] So, okay. So, the number

36
00:04:00,641 --> 00:04:08,025
of periods, EMT. And then the next
information, this is a little bit

37
00:04:08,025 --> 00:04:15,050
important because Excel has a system which
you got to follow, otherwise you are kind

38
00:04:15,050 --> 00:04:21,306
of on your own. If after the number of
periods there is, there is a symbol called

39
00:04:21,306 --> 00:04:27,961
PV, which we know what it is. Do we know
the PV of this problem? The answer's no.

40
00:04:27,961 --> 00:04:34,859
So we've got to put zero, because we don't
have a number there and then we type FV

41
00:04:34,859 --> 00:04:42,417
500. And hopefully, when I say. If I have
all the numbers right, and I am doing it

42
00:04:42,417 --> 00:04:47,224
in real time with you, simply to make you
recognize that you can do it. You can do

43
00:04:47,224 --> 00:04:52,726
it just like I did it. The reason I'm
again, I am using the calculator is simply

44
00:04:52,726 --> 00:04:58,560
because the number that I need to
calculate has got 25 operations involved,

45
00:04:58,560 --> 00:05:04,398
right? So, the only operation that's
simple is the last one. But, in this case

46
00:05:04,398 --> 00:05:10,582
I don't know the last one either. I don't
know 25 of them, right? The PMT. So, how

47
00:05:10,582 --> 00:05:16,879
do I figure that out? I have to use a
calculator or do step wise very slowly the

48
00:05:16,879 --> 00:05:24,021
problem, and we'd be here forever, okay.
So, 6,840. Hundred and, let me call it

49
00:05:24,021 --> 00:05:32,410
6,840. So I am going to now go back to the
problem. The answer to this is in dollars,

50
00:05:32,410 --> 00:05:38,640
6840. Why am I making it 6840, why not
6839.1? Because we are family, now. I

51
00:05:38,640 --> 00:05:45,808
mean, I'm not gonna worry about decimals,
and you don't nee d to worry about them,

52
00:05:45,808 --> 00:05:52,280
at least in the classroom. In real life,
probably, yes. Okay, 6840. Let's for

53
00:05:52,280 --> 00:06:03,183
convenience assume that it's about 7,000.
Approximately, right? So, what's going on

54
00:06:03,183 --> 00:06:11,107
here? I need to put away $6,840 and I
approximate it, approximately $7000 how

55
00:06:11,107 --> 00:06:19,041
many times? 25 times to end up having
$500, all right? So, why did I approximate

56
00:06:19,041 --> 00:06:27,490
even 6,840 by 7,000? Let me ask you the
following question. Suppose the interest

57
00:06:27,490 --> 00:06:37,159
rate was zero, right? In other words,
there was no value to time. What would you

58
00:06:37,159 --> 00:06:48,200
have if you invested $7,000.00 25 times?
You just multiply seven by, 7,000 by 25,

59
00:06:48,200 --> 00:06:59,615
right. So, what do you do, you take
$7,000.00 multiply it by 25, you have 175.

60
00:06:59,615 --> 00:07:11,597
75's the. Why did I do this? Again, as in
finance, pause and say compounding, right.

61
00:07:11,597 --> 00:07:20,416
So if I didn't have eight percent rate of
return, I would make only, have only

62
00:07:20,416 --> 00:07:29,605
$175,000. That's not little. And by no
means am I saying it's throwaway money.

63
00:07:29,605 --> 00:07:36,677
But, compared to 175 to the 500. So,
what's going on? The eight percent is

64
00:07:36,677 --> 00:07:42,279
helping me and here's my little take
before we take a natural break on this, I

65
00:07:42,279 --> 00:07:47,904
hope you understand this problem.
Secondly, I hope you recognize that the

66
00:07:47,904 --> 00:07:54,084
eight percent is coming from where? The
market. And I hope you realize now why the

67
00:07:54,084 --> 00:08:00,860
market is so awesome. Because you're not
doing anything. I'm not doing anything. I

68
00:08:00,860 --> 00:08:06,059
mean, I put away my money in my
retirement. What am I giving in this

69
00:08:06,059 --> 00:08:12,095
example? I'm putting away 68,40, right? I
understand that could be my hard-earned

70
00:08:12,095 --> 00:08:19,011
money. But the fact is the externality,
the positive benefit the market provides

71
00:08:19,011 --> 00:08:25,023
to people for their ability to benefit
from the economy. At the rate of eight

72
00:08:25,026 --> 00:08:31,025
percent is phenomenal. You see what I'm
saying. So, so, what's going on here is

73
00:08:31,025 --> 00:08:36,576
that I, my money goes to somebody with
great ideas, who's able to earn some

74
00:08:36,576 --> 00:08:41,886
money, and I still can earn eight%. So, I
don't want you to ever forget the beauty

75
00:08:41,886 --> 00:08:47,547
of markets. Beauty of markets is an
ability for all of us to share, not one

76
00:08:47,547 --> 00:08:52,745
person, all of us. That's the beauty of
it. The unfortunate thing about life, as I

77
00:08:52,745 --> 00:08:57,815
said once in a while I'll go into life, is
that not everybody has this opportunity.

78
00:08:57,815 --> 00:09:02,095
And yes, it can, we can all say that
everybody's not working hard enough. But,

79
00:09:02,095 --> 00:09:10,231
sometimes it's difficult to make money,
right? It's difficult to have jobs. Lot of

80
00:09:10,231 --> 00:09:15,553
people these days don't have the ability
to even invest. So, let's do this, let's

81
00:09:15,553 --> 00:09:21,744
take a break right now. We have spent a
lot of time on two problems. I do not want

82
00:09:21,744 --> 00:09:27,437
you to exhaust yourself but I want you to
think about these issues. One last

83
00:09:27,437 --> 00:09:33,370
thought, while we're off-line, redo these
problems and double-check them. Let me

84
00:09:33,370 --> 00:09:41,079
explain what I mean. Make 6840 your
payment, make number of years 25 and make

85
00:09:41,079 --> 00:09:49,071
the interest rate eight percent and solve
the problem for what the future value will

86
00:09:49,071 --> 00:09:57,033
be. And what should your answer be?
500,000, tell me what's cooler than that.

87
00:09:57,033 --> 00:10:05,007
It's internally consistent, it's got to
be, if it's not, it's not finance. Okay?

88
00:10:05,007 --> 00:10:11,085
So, take a break, and I'll also take a
pause, and we'll come back and start off

89
00:10:11,085 --> 00:10:16,060
with present value of annuitys. Keep
smiling. Thank you.
