1
00:00:00,000 --> 00:00:05,049
In some senses as I said before, if you
know how to draw timelines, you have

2
00:00:05,049 --> 00:00:11,028
arrived. So, what I am going to do now is,
I am going to show you what a timeline

3
00:00:11,028 --> 00:00:16,077
means and you will just kinda laugh,
because it's kind of silly but I think

4
00:00:16,077 --> 00:00:22,070
it's important. So, and by the way, just
so that you know my handwriting is nowhere

5
00:00:22,070 --> 00:00:28,071
near as perfect as the title on top. So if
you expect it to be, you need to grow up.

6
00:00:28,363 --> 00:00:37,104
So, I am going to just draw a timeline and
you should be able to take a word problem

7
00:00:37,104 --> 00:00:46,590
and put it along the timeline. And I will
put dots here. Why is this important? I

8
00:00:46,590 --> 00:00:52,840
think its important because if you can
take a real life problem and put it on a

9
00:00:52,840 --> 00:00:58,743
timeline, you achieved probably the most
difficult part of taking a real world

10
00:00:58,743 --> 00:01:04,607
situation and then using financial tools.
So, in some senses, finance requires you

11
00:01:04,607 --> 00:01:10,209
to know what life is all about. What the
problem is all about before you can use

12
00:01:10,209 --> 00:01:15,032
finance [laugh] you know? So, so I, I've
said this many times you know, and it was

13
00:01:15,032 --> 00:01:19,682
an accident that I got to learn finance.
But there is love and then there's

14
00:01:19,682 --> 00:01:25,031
finance. The gap is huge, right? I mean,
love is somewhere special. But being

15
00:01:25,031 --> 00:01:30,032
number two ain't bad. So, you need to
understand life, love and so on, and then

16
00:01:30,032 --> 00:01:35,027
put in on a timeline and you can put,
pretty much anything on a timeline. So,

17
00:01:35,027 --> 00:01:40,651
here's the first thing. At this point,
we'll typically call this a PV, present

18
00:01:40,651 --> 00:01:47,639
value. Then the number of periods are
pretty obvious. So this is one, two, three

19
00:01:47,647 --> 00:01:53,953
and this is n, n reflects these time
periods. The, the important thing to

20
00:01:53,953 --> 00:02:02,314
remember is that r is the interest rate
that applies to one period and the real

21
00:02:02,314 --> 00:02:09,088
world typically that one period is a year.
By that I mean, when you see interest

22
00:02:09,088 --> 00:02:14,395
rates being quoted for various stuff like
a bank loan and so on, it will be annual,

23
00:02:14,395 --> 00:02:20,545
and that's just, so that it make sense,
you can compare things, kind of. So, in

24
00:02:20,545 --> 00:02:27,620
the, in the, in the beginning what I will
do is I'll just take PV and I'll try to

25
00:02:27,620 --> 00:02:35,073
relate it to FV. So, we'll try to
understand these two concepts. How does FV

26
00:02:35,073 --> 00:02:41,100
translate to PV, PV translate to FV, go
back and forth and become very familiar

27
00:02:41,100 --> 00:02:46,320
today. But I, I remind you of one thing,
just listening to me is, it looks easy,

28
00:02:46,320 --> 00:02:52,469
and that's the challenge of this class. I
will make it sound really easy but [laugh]

29
00:02:52,469 --> 00:02:55,652
but the challenge is to do the problems
and that's when you internalize, right?

30
00:02:55,652 --> 00:03:01,067
Because the word problem is the problem.
And if you can't figure out the word

31
00:03:01,067 --> 00:03:05,270
problem, this is, ain't going to help. So,
drawing a timeline, bringing a word

32
00:03:05,270 --> 00:03:10,200
problem to it is what it's going to be
about. I'll start off with simple problems

33
00:03:10,200 --> 00:03:16,360
and then make them more complicated. But
today, what we'll stick with is, a single

34
00:03:16,360 --> 00:03:21,533
payment, and, I'm sorry, meaning I will
transfer something from PV to FV, either

35
00:03:21,533 --> 00:03:29,539
for one year, two years, or ten years, and
vice versa. We could have stuff coming in

36
00:03:29,539 --> 00:03:34,384
here, which is also dollars. Remember,
this is dollars, and this is dollars.

37
00:03:34,384 --> 00:03:39,636
Could have dollars coming in here, that's
what is happening, actually in most

38
00:03:39,636 --> 00:03:44,575
projects, most forum. But we could do
ignore that for the time being. And the

39
00:03:44,575 --> 00:03:50,943
reason is, as I said, I want you to
understand time value of money. We'll go

40
00:03:50,943 --> 00:03:56,331
slow, in the beginning, and then we'll
take off. So, you know, when you are on a

41
00:03:56,331 --> 00:04:01,157
plane, the pilot warns you. Okay, fasten
your seat belt. I'm gonna take off. I'm

42
00:04:01,157 --> 00:04:04,572
going to warn you. I mean, when you hit
the assignments in the programs there will

43
00:04:04,572 --> 00:04:08,414
be a warning. You better have your seat
belt on. So, get on to the problems.

44
00:04:08,414 --> 00:04:14,092
That's how you learn. You don't learn by
just listening to me or anybody. Okay? So,

45
00:04:14,092 --> 00:04:20,047
please recognize the importance of
timeline, and I'm going to go back to the

46
00:04:20,047 --> 00:04:28,780
notion of how to think about time value of
money and how to take timelines and work

47
00:04:28,780 --> 00:04:34,955
them forward. We have talked about the
importance of timelines, I am now going to

48
00:04:34,955 --> 00:04:40,519
jump into what I promised I'd do. I am not
going to create a formula and, I mean,

49
00:04:40,519 --> 00:04:46,225
pick a formula and just throw it at you.
No. To the extent I can, and that's my

50
00:04:46,225 --> 00:04:51,653
challenge, is to talk about a problem and
then create the formula. Because I don't

51
00:04:51,653 --> 00:04:57,220
like formulas without understanding whats
going on. Okay. But the mai n insight we

52
00:04:57,220 --> 00:05:02,693
are going to worry about is, a dollar
today is worth more than a dollar

53
00:05:02,693 --> 00:05:09,362
tomorrow. Or in other words, that's the
essence of time value of money. The time

54
00:05:09,362 --> 00:05:15,741
by itself, the passage of time by itself
has value. And there are some reasons for

55
00:05:15,741 --> 00:05:21,151
it, as I said, you can go back and read up
on them. And we are going to assume what

56
00:05:21,151 --> 00:05:26,842
captures the value of money, is the
interest rate. The relationship between

57
00:05:26,842 --> 00:05:33,300
today and tomorrow, today and the future.
And that interest rate we'll assume is

58
00:05:33,300 --> 00:05:38,306
positive. So, let me start with an
example. Suppose a bank pays a ten percent

59
00:05:38,306 --> 00:05:44,300
interest rate per year and you are given a
choice between two plans. By the way, I'll

60
00:05:44,300 --> 00:05:49,950
be going a lot back and forth, writing and
stuff like that, but that will hopefully

61
00:05:49,950 --> 00:05:55,207
make it more engaging and as I do a
problem, you should do it with me, you

62
00:05:55,207 --> 00:06:00,799
know? And then if the problem gets
complicated, I will give you more time and

63
00:06:00,799 --> 00:06:06,368
then do it together and so on. So this,
these are your two choices. It's very

64
00:06:06,368 --> 00:06:12,021
simple. I either give you $100 dollars
today or I give you $100 one year from

65
00:06:12,021 --> 00:06:19,014
now. And for the time being, let's keep
our period one year. So the question

66
00:06:19,014 --> 00:06:25,071
really is, which one would you prefer? And
why? As I said, I just don't want to know

67
00:06:25,071 --> 00:06:31,044
what you prefer, I want to know why the
heck do you prefer it? It turns out, if

68
00:06:31,044 --> 00:06:36,758
you talked about it even for a second or
even not thinking about it, you'll choose

69
00:06:36,758 --> 00:06:43,591
one of the two. And it's probably going to
be the first one, right? So, the goal here

70
00:06:43,591 --> 00:06:52,035
is to, use the simple example to motivate
something that is fundamental that we'll

71
00:06:52,035 --> 00:06:59,055
build on. So, this is the future value
problem in an example. So, what I'm going

72
00:06:59,055 --> 00:07:06,075
to do is I'm going to try to work with
you. So, try to think through this. So, A

73
00:07:06,075 --> 00:07:25,720
is, remember, A was $100 now. This is A.
And B was $100 in the future. And that's

74
00:07:25,720 --> 00:07:30,839
what I meant. The timeline is extremely
important. Your, I'm giving you two very

75
00:07:30,839 --> 00:07:37,806
simple choices to actually recognize. Now,
this is where even popular financial press

76
00:07:37,806 --> 00:07:44,126
screws up and you wouldn't believe it, but
it's true. What we'll do in our head is we

77
00:07:44,126 --> 00:07:50,109
intuitively recog nize that just the
passage of time has an effect and will

78
00:07:50,109 --> 00:07:56,912
have an effect on the value of the money
we are talking about or whatever it is,

79
00:07:56,912 --> 00:08:03,179
but we directly compare these two. And
that's not the right thing to do. In other

80
00:08:03,179 --> 00:08:09,114
words, if you were to do this, and I say
this in my class, and I'm going to say

81
00:08:09,114 --> 00:08:15,263
this to you, if you start comparing money
across time directly with each other, it

82
00:08:15,263 --> 00:08:21,574
would be better if you stabbed me. Because
you're basically telling me, whatever

83
00:08:21,574 --> 00:08:26,495
you're teaching in finance is useless. So,
remember the first principle is, you

84
00:08:26,495 --> 00:08:31,957
cannot compare money across time. That
would only be meaningful if time had no

85
00:08:31,957 --> 00:08:38,738
value. And what captures the value of time
in this one scenario, is what? The

86
00:08:38,738 --> 00:08:44,528
interest rate. So, let's try to work it a
little bit better. At this point in time,

87
00:08:44,528 --> 00:08:50,595
let's do the future value, right? So, what
is already in the future? We know that

88
00:08:50,595 --> 00:08:57,772
this is already in the future. So, the
question is, I cannot compare this to this

89
00:08:57,772 --> 00:09:05,060
at time zero, but what can I do? I can
either bring this back to time zero, so

90
00:09:05,060 --> 00:09:12,961
take this. Or, carry this forward to the
future. And the reason I'm going to do

91
00:09:12,961 --> 00:09:20,404
carrying forward the future value first,
is I think It is easier to understand

92
00:09:20,404 --> 00:09:26,994
finance if you do that. And it also makes
you think about the future. And that's

93
00:09:26,994 --> 00:09:33,371
very important. Every decision that you
make, every value creating decision that

94
00:09:33,371 --> 00:09:40,066
you make should force yourself to look
into the future. And this is where I think

95
00:09:40,066 --> 00:09:46,098
accounting can make, can make fun of.
Accounting standing at time zero where we

96
00:09:46,098 --> 00:09:51,284
are today, is looking backwards. So
that's, it's, you know, it's, it's done.

97
00:09:51,284 --> 00:09:57,317
The, the past is over. So, while you can
derive very interesting implications from

98
00:09:57,317 --> 00:10:03,005
the past and I don't mean to demean
anything, all decisions ultimately involve

99
00:10:03,005 --> 00:10:08,498
your capability to look into the future.
And that's what's challenging about it and

100
00:10:08,498 --> 00:10:13,799
that's what's awesome. Every decision has
an impact on the future. And typically,

101
00:10:13,799 --> 00:10:19,343
the painful part happens today. The better
the idea the more the pain today. But

102
00:10:19,343 --> 00:10:24,355
benefits, lot in the future. You know? So
like Go ogle, I mean it took a lot of

103
00:10:24,355 --> 00:10:29,521
effort to create and now a lot of values
been created. So, sticking to the simple

104
00:10:29,521 --> 00:10:38,539
problem, I think you know the answer to
this. The answer to this will be $110. And

105
00:10:38,539 --> 00:10:45,804
the reason is very simple, r is ten%. So,
let me just walk you through, talk you

106
00:10:45,804 --> 00:10:50,884
through and then we'll do the formula. I
know right now many of you are saying come

107
00:10:50,884 --> 00:10:56,343
on, this is just too easy. Well, it will
build on itself and so we got to

108
00:10:56,343 --> 00:11:02,394
understand this piece. So, the $100 that
you had, you could put in a bank, right?

109
00:11:02,394 --> 00:11:08,757
And that 100, because the interest rate is
positive, will be part of this 110.

110
00:11:08,757 --> 00:11:16,187
Because the interest rate is positive you
can't lose that $100, right? And then,

111
00:11:16,187 --> 00:11:22,557
you're earning ten percent interest. So,
what is ten percent of $100? $ten. So,

112
00:11:22,557 --> 00:11:30,370
it's very obvious what's going on that
you, in the end will have $110. So, as I

113
00:11:30,370 --> 00:11:36,834
promised you, what I am not going to do is
I am not going to throw a formula at you

114
00:11:36,834 --> 00:11:43,059
until at least you have some sense of
where I am going and hopefully this simple

115
00:11:43,059 --> 00:11:49,162
example has motivated you to, motivated
you to try to understand future value a

116
00:11:49,162 --> 00:11:56,069
little bit better. So now, what I am going
to do is I am going to throw the concept

117
00:11:56,069 --> 00:12:02,242
at you. In this concept, what it says is
the following, that the future value of

118
00:12:02,242 --> 00:12:07,974
anything that's carried forward has to
have two components. One is the initial

119
00:12:07,974 --> 00:12:13,890
payment, and then our example, it's 100.
And the other is accumulated interest

120
00:12:13,890 --> 00:12:20,638
which in our example is $ten. So, the
problem becomes very straightforward. You

121
00:12:20,638 --> 00:12:28,295
put in $100, you get a $100. But then you
get ten%on the 100 which is $ten. So you

122
00:12:28,295 --> 00:12:34,072
get 110. So, this is the formula. So, if I
were to ask you, what does it related to

123
00:12:34,072 --> 00:12:44,110
the problem that we just did. So, what is
this P? P is your initial payment of $100.

124
00:12:44,110 --> 00:13:00,131
What is the r? R is the ten%. But the ten
percent is on what? Is on the P of $100.

125
00:13:00,131 --> 00:13:08,214
So, I know that ten percent of $100 is a
fraction one-tenth and this will be $ten.

126
00:13:08,217 --> 00:13:14,763
But the way we write it, which looks, is
very straightforward, is we take P out of

127
00:13:14,763 --> 00:13:21,037
the picture. So, the P is common to the
first one, therefore, the one. And rP, r

128
00:13:21,037 --> 00:13:28,772
P. So, what you put in th e brackets is
many times called Future Value Factor.

129
00:13:28,772 --> 00:13:37,104
It's a factor because, what does this one
+ r reflect? Let's do it in our case. One

130
00:13:37,104 --> 00:13:44,710
+ r in our case is 1.10. And what's cool
about this number is, it tells you the

131
00:13:44,710 --> 00:13:50,503
future value of $one. So, if you know the
future value of $one in this case is 1.1,

132
00:13:50,503 --> 00:13:55,342
which is very simple, one plus the
interest. You know the future value of any

133
00:13:55,342 --> 00:14:00,271
number. Because if the number is 100, you
multiply 1.1 by a 100, you get 110. If

134
00:14:00,271 --> 00:14:04,923
it's a million, you get 1.1 million and so
on and so forth. So, many people

135
00:14:04,923 --> 00:14:10,430
conceptually emphasize that Future Value
Factor. And I'm going to just do it this

136
00:14:10,430 --> 00:14:15,666
one time. But you can go back to the
notes, and think about it like that, you

137
00:14:15,666 --> 00:14:20,431
know? I mean, it'll be, it'll be very
helpful to you. So, right now, what I'm

138
00:14:20,431 --> 00:14:25,603
not going to do is, I'm not going to use
any tool to elaborate on this formula, by

139
00:14:25,603 --> 00:14:30,529
that I mean, you don't need Excel to do
this, right? Actually, you need Excel only

140
00:14:30,529 --> 00:14:36,038
to do when the problem becomes difficult
to compute, not think about. Okay. So, the

141
00:14:36,038 --> 00:14:42,704
initial payment is P and the accumulated
interest is r P. So, that's the way you

142
00:14:42,704 --> 00:14:46,046
want to think about this problem.
