1
00:00:00,000 --> 00:00:06,368
Hello. Welcome Back. I hope you have had
the chance of now doing full problems at

2
00:00:06,368 --> 00:00:13,609
your own pace. And today as I said, is
going to be intense because we are trying

3
00:00:13,609 --> 00:00:19,992
to pull in a lot of real world stuff. And
I'm trying to tell you the meaning of

4
00:00:19,992 --> 00:00:26,764
every bit of information to the extent I
can. So, take your time and now let's use

5
00:00:26,764 --> 00:00:32,779
this one simple example to show you the
awesomeness of finance. I really mean it.

6
00:00:32,779 --> 00:00:39,685
If you hang on to the next half hour or so
and listen carefully, we'll be in good

7
00:00:39,685 --> 00:00:45,071
shape. Okay. So what is a loan
amortization table? First of all, I told

8
00:00:45,071 --> 00:00:53,085
you last time that the, the loan example
is a classic example of what a finance

9
00:00:53,085 --> 00:01:00,001
instrument looks like or what finance is
all about. So what I'm going to do here is

10
00:01:00,001 --> 00:01:05,872
I'm going to take the same problem. How
much did we borrow, a $100,000 so I'm

11
00:01:05,872 --> 00:01:13,990
going to write $100,000 here. And the
reason I'm writing $100,000 is because

12
00:01:13,990 --> 00:01:20,797
that's the amount of money you started out
with. I want you to recognize that the

13
00:01:20,797 --> 00:01:26,780
amount here, tied to here is beginning
balance. Why did I say that? Because

14
00:01:26,780 --> 00:01:33,781
that's the amount of money you have walked
away with or you owe the bank. At the

15
00:01:33,781 --> 00:01:41,407
beginning of each year, year one so what
point in time is this? It's time zero,

16
00:01:41,407 --> 00:01:47,040
point and time zero. So, that's why the
number of beginning it said so because

17
00:01:47,040 --> 00:01:53,014
otherwise it's assumed things are
happening at the end of the year, right?

18
00:01:53,014 --> 00:02:00,585
So, please be very clear that if I draw a
timeline, the $100,000 is at time zero and

19
00:02:00,585 --> 00:02:06,750
who has paid you this? The bank so you
have it now what do you have to do? Pay.

20
00:02:06,750 --> 00:02:14,028
So, the first payment will occur when? At
the end of the year. It won't occur here.

21
00:02:14,028 --> 00:02:20,715
It will occur here and we know it's 6350.
The good news is that this, for

22
00:02:20,715 --> 00:02:26,845
convenience and very common in the real
world, is what is called a fixed interest

23
00:02:26,845 --> 00:02:36,174
rate loan. So, the interest rate was ten
percent for convenience in CY but the good

24
00:02:36,174 --> 00:02:49,621
news is. Right? So the yearly payment
starting with this year is, I'll write 26K

25
00:02:49,621 --> 00:02:57,463
for convenience. It's about 26,000, right?
So what is an amortization table and why

26
00:02:57,463 --> 00:03:02,514
am I doing it? Because it brings out the
essenc e of what's happening during the

27
00:03:02,514 --> 00:03:07,452
life of the loan and I think it's very
important for you to time travel. If you

28
00:03:07,452 --> 00:03:12,414
know how to time travel, you'll understand
finance. So right now, you are here, and

29
00:03:12,414 --> 00:03:18,548
let's begin time travel. At the end of the
first year, what did we do? We pay 26,380.

30
00:03:18,548 --> 00:03:25,250
Let's break it into two parts. The first
part is the interest and we know the

31
00:03:25,250 --> 00:03:31,565
interest is ten percent but remember the
weakness of an interest rate if there is

32
00:03:31,565 --> 00:03:38,031
any, it's not in Dollars. It's not in the
form of evaluation that you're used to

33
00:03:38,031 --> 00:03:44,585
dollars, yen and so on so how much will it
be? I'm going to pause for a second. This

34
00:03:44,585 --> 00:03:50,809
is not a difficult problem but people get
stuck with it. It has to be ten percent of

35
00:03:50,809 --> 00:04:00,879
what you borrowed and how much is that?
Sorry, 10,000. Does that, is that clear?

36
00:04:00,879 --> 00:04:08,120
This is very important. You owe interest
on what you borrowed at the beginning of

37
00:04:08,120 --> 00:04:13,929
the period and one year has passed and you
owe 100,000 at the beginning so you owe

38
00:04:13,929 --> 00:04:19,500
[inaudible]. So how much will you repay
the loan? Remember, because the goal here

39
00:04:19,500 --> 00:04:25,771
is not only to pay interest but to repay
the loan so how much is left? Very simple,

40
00:04:25,771 --> 00:04:32,002
if you are paying 26,380 and you're paying
$10,000 interest, how much is the loan

41
00:04:32,002 --> 00:04:37,808
repayment? Straight forward. Did this,
subtract this, you're left with 16,380.

42
00:04:37,808 --> 00:04:44,050
Now, you know why I took ten%. I took ten
percent because I'm doing this problem to

43
00:04:44,050 --> 00:04:50,853
do. I'm not using the calculator. In real
life, that's why Excel is great. You can

44
00:04:50,853 --> 00:04:55,120
do, instead of ten percent you can have
.2, three, four, five whatever. So now,

45
00:04:55,120 --> 00:05:01,674
the next step is a little bit important
and the question is how much will you owe

46
00:05:01,674 --> 00:05:09,403
the bank at the beginning of year two
which is which point? Remember, beginning

47
00:05:09,403 --> 00:05:17,357
of year two is .1, end of year two is .2.
So at this point how much will you oh,

48
00:05:17,357 --> 00:05:22,853
very simple, you owe 100.000. You paid the
use of money ten%. You would love to

49
00:05:22,853 --> 00:05:28,775
deduct it from how much you owe but the
hand will come out of the bank and hit you

50
00:05:28,775 --> 00:05:33,812
and say what the heck are you doing? This
is for the use of money. On the other

51
00:05:33,812 --> 00:05:39,310
hand, the bank would love for you to pay
all 26,800 as interest. Of course, if you

52
00:05:39,310 --> 00:05:44,482
are silly enou gh to do that the bank will
try it, right? So it depends who's being

53
00:05:44,482 --> 00:05:49,510
silly or stupid here. Okay so this is the
amount you repay so what do you do? You

54
00:05:49,510 --> 00:05:56,352
subtract this from this. And how much are
you left with? Just to look, make sure I'm

55
00:05:56,352 --> 00:06:04,578
getting the numbers, right. Everybody got
it? So what has happened? I have lowered

56
00:06:04,578 --> 00:06:11,395
the amount I owe because I paid 26 and
only 10,000 was the interest, right? Now

57
00:06:11,395 --> 00:06:19,052
at the end of the second year what do I
do, I again pay 26K. Now what is going to

58
00:06:19,052 --> 00:06:25,004
happen? Take a guess. Will the amount of
interest go up relative to last year that

59
00:06:25,004 --> 00:06:31,487
you pay or go down? Think about it. If the
amount went up, you're going in the wrong

60
00:06:31,487 --> 00:06:37,726
direction. The only way the amount would
go up on interest is if you're actually

61
00:06:37,726 --> 00:06:43,675
borrowed more rather than pay back some.
And there's no good or bad here. It's

62
00:06:43,675 --> 00:06:49,027
here, the assumption is that you're going
to pay back the loan, right? So how much

63
00:06:49,027 --> 00:06:56,019
will you pay in interest? Pretty
straightforward, 8362. How did I do that?

64
00:06:56,019 --> 00:07:02,023
Pretty straightforward, again. The
interest rate is ten%. I took the ten

65
00:07:02,023 --> 00:07:08,161
percent and I didn't multiply it to the
100,000. I multiplied it to 83620, why?

66
00:07:08,161 --> 00:07:15,476
Because I don't owe the bank 100,000. I
owe the bank only 83,000 at the end of the

67
00:07:15,476 --> 00:07:22,063
first year. The good news is my interest
has dropped but the reflection of that

68
00:07:22,063 --> 00:07:28,098
good news is that I'm paying back more.
So, if I paid back 16,380 how much am I

69
00:07:28,098 --> 00:07:35,033
paying back now? More or less? Answer is,
if the interest amount has dropped the

70
00:07:35,033 --> 00:07:41,084
repayment amount has to go up, assuming
that I'm paying back the same amount 26K

71
00:07:41,084 --> 00:07:50,044
or so every year. So the answer to that is
18018. For my, for the ease of saving time

72
00:07:50,044 --> 00:07:57,538
I've just done these calculations ahead of
time. And so how, how do I know that? I

73
00:07:57,538 --> 00:08:04,981
know that eighteen + eight has to add up
to 26 because is 26 is what I paid again

74
00:08:04,981 --> 00:08:10,966
at end of year two. So at the end of the
year two, what is happening? My interest

75
00:08:10,966 --> 00:08:17,443
rate is going down, but my repayment rate
is going up. And this is needed for you to

76
00:08:17,443 --> 00:08:23,224
repay the loan, right? So here's your
homework number one. Before you do

77
00:08:23,224 --> 00:08:30,197
anything else, try to fill up this box and
I will do i t quickly for you but the

78
00:08:30,197 --> 00:08:37,839
principle is the same. How do I go from
here to here? I subtract this from this so

79
00:08:37,839 --> 00:08:46,051
let me write the number for you, 65603.
How do I go from here to the interest

80
00:08:46,051 --> 00:08:53,532
column ten percent of this? So 6560 and
how do I get this column? I know that the

81
00:08:53,532 --> 00:09:01,797
number has to increase because this drop
and this amount is the same so 26 is the

82
00:09:01,797 --> 00:09:12,114
same so I subtract 6500 from 26 I get
19820, okay? Same thing, let me just write

83
00:09:12,114 --> 00:09:26,492
it out 75,783, this number drops to 4578.
This number goes up to 21802. This number

84
00:09:26,492 --> 00:09:39,348
becomes 23982. This number is 2398. And
the last number is 23982. So, see what's

85
00:09:39,348 --> 00:09:47,732
happening now. At the end of the year, how
much did I owe? 23982 but I paid 26,380.

86
00:09:47,732 --> 00:09:55,719
You see, I owed pay 23982 but I paid 2639,
380. Why did I pay more than I owed?

87
00:09:55,719 --> 00:10:03,981
Because I owed 23982 at the beginning of
the year and I have to pay interest on it

88
00:10:03,981 --> 00:10:11,793
of 2398 so I have to pay 26380 to be able
to pay back the loan. But, the good news

89
00:10:11,793 --> 00:10:18,517
is when I am done in year five, how much
do I owe the bank? Nothing. Again, I'm

90
00:10:18,517 --> 00:10:23,951
saying it's good news, consistent with
your plan to pay after five years. In

91
00:10:23,951 --> 00:10:29,814
finance, the good news is, there's no good
news, bad news. It depends on what your

92
00:10:29,814 --> 00:10:35,792
objective is so for example, if you don't
have money, you many times don't have

93
00:10:35,792 --> 00:10:41,832
money coming in, people take interest rate
only loans. That's okay because it is

94
00:10:41,832 --> 00:10:47,888
dictated by the cash flow constraints you
have. You pay less because you're only

95
00:10:47,888 --> 00:10:54,847
paying interest. But most people want to
pay off the loan, therefore this example

96
00:10:54,847 --> 00:11:00,875
is very, very valid, okay? So please
remember this, do this example one more

97
00:11:00,875 --> 00:11:07,393
time, why am I going to emphasize this and
where does the beauty of finance come in?

98
00:11:07,393 --> 00:11:13,366
And now bear with me for a second. What
are the first columns going? The year.

99
00:11:13,366 --> 00:11:19,815
What are the second columns showing?
Beginning balance. The early payment,

100
00:11:19,815 --> 00:11:26,054
interest, and principal payment. Suppose I
walked up to you, suppose I walked up to

101
00:11:26,054 --> 00:11:32,684
you and asked you, hey you're taking a
loan $100,000. You're just coming out of

102
00:11:32,684 --> 00:11:38,090
the bank and I'm your buddy, and I know
you know finance. I say how much did you

103
00:11:38,090 --> 00:11:45,351
borrow ? I said $100,000. I say look, can
you tell me how much will you pay the bank

104
00:11:45,351 --> 00:11:50,583
every year for the next five years? Will
you be able to do that? Sure. You have an

105
00:11:50,583 --> 00:11:55,768
Excel spreadsheet with you, you're sitting
in the car. You open it up and you do a

106
00:11:55,768 --> 00:12:01,072
PMT calculation and you can come up with
26380, easy. So the good news is, once you

107
00:12:01,072 --> 00:12:07,076
know how much you're borrowing, the yearly
payment column at a fixed interest rate is

108
00:12:07,076 --> 00:12:14,030
very straightforward but what is the most
difficult part of this problem? The most

109
00:12:14,030 --> 00:12:19,046
difficult part of this problem is the
following answer to the following

110
00:12:19,046 --> 00:12:25,279
question. If I were trying to figure out
whether you really know finance and

111
00:12:25,279 --> 00:12:30,099
awesomeness of it, I'll ask you the
following question. How much will you owe

112
00:12:30,099 --> 00:12:37,057
the bank? How much will you owe the bank
at the beginning of year three? At the

113
00:12:37,057 --> 00:12:46,058
beginning of year three, which is also the
end of year two, right? So how much will

114
00:12:46,058 --> 00:12:51,071
you owe the bank at the beginning of year
three? How will you do that problem? So,

115
00:12:51,071 --> 00:12:57,004
this is where if you did this problem this
way, it'll take you ages to do. Because I

116
00:12:57,004 --> 00:13:02,009
could ask you the question how much will
you owe at the beginning of year four?

117
00:13:02,011 --> 00:13:06,359
Look to get there, what will I have to do?
If I were to say, how much do you owe the

118
00:13:06,359 --> 00:13:11,612
bank at the beginning of the year four?
I'll have to go through many roles of this

119
00:13:11,612 --> 00:13:17,267
spreadsheet to be able to understand and
this is where the beauty of finance comes

120
00:13:17,267 --> 00:13:23,879
in. And I'm going to try to show you a
timeline, which is very similar to this

121
00:13:23,879 --> 00:13:31,208
one and I am going to call it. I'm going
to call it, instead of amortization I'm

122
00:13:31,208 --> 00:13:37,009
going to call it the power of the math. So
let me start off with a simple question.

123
00:13:37,009 --> 00:13:43,542
If I asked you, if I asked you to tell me
how much do you owe the bank here?

124
00:13:43,542 --> 00:13:50,859
Remember this is the beginning of each,
first year. What point in time? Zero. Now,

125
00:13:50,859 --> 00:13:57,019
it's a silly question to ask, but not
quite. Why is it a silly question to ask?

126
00:13:57,019 --> 00:14:02,997
Because you already know how much you've
borrowed. Which is what, $100,000 but let

127
00:14:02,997 --> 00:14:08,499
me ask you this, as soon as you walk out
of the bank. This is something that the

128
00:14:08,499 --> 00:14:14,832
bank will tell you, believe me it will,
that you need to pay how many times? Five

129
00:14:14,834 --> 00:14:21,556
times, right? Right? So you walk into the
bank, you know the yearly payment and I

130
00:14:21,556 --> 00:14:28,448
ask you the following apparently silly
question, how much do you owe the bank the

131
00:14:28,448 --> 00:14:36,273
moment you walk out of it? You know what
many people will say? Many people will

132
00:14:36,273 --> 00:14:42,950
multiply 26380 by five and you have just.
Destroyed me if the answer is that. You

133
00:14:42,950 --> 00:14:49,402
might as well take a big knife and stick
it in my stomach. And the reason is, you

134
00:14:49,402 --> 00:14:56,147
cannot add a multiplier over time because
of compounding and the positive interest

135
00:14:56,147 --> 00:15:02,486
rate, right? Because if you do, how much
do you owe? You owe 20,000 five times if

136
00:15:02,486 --> 00:15:10,450
interest rate was zero so your answer is
not a good one. So here is what you do,

137
00:15:10,450 --> 00:15:20,132
you make 26380, five times of PMT, right?
And what do you make m? Five. Yes, because

138
00:15:20,132 --> 00:15:29,215
you owe five of these. What is the only
other number you need to do? R, which is

139
00:15:29,215 --> 00:15:37,875
what ten%. If you do this problem, what
button do you need to or what execution in

140
00:15:37,875 --> 00:15:46,039
Excel or a calculator do you need to do?
Well, to figure this out, you have to

141
00:15:46,039 --> 00:15:56,082
figure out Pv. Please do it. I wish your
answer will work out to be 100,000, right?

142
00:15:56,082 --> 00:16:05,016
We know that. Why am I emphasizing this?
Because the awesomeness of finance comes

143
00:16:05,016 --> 00:16:11,691
from the following simple principle number
one. All value is determined by standing

144
00:16:11,691 --> 00:16:17,469
at a point in time and looking forward.
You can do value in many different ways.

145
00:16:17,469 --> 00:16:22,494
You can do it a time - five and bring it
forward, or do it in the future but the

146
00:16:22,494 --> 00:16:27,485
best way to think about decision making
is, you're standing at zero and you're

147
00:16:27,485 --> 00:16:32,492
looking forward. So when you're standing
at zero and looking forward, how many

148
00:16:32,492 --> 00:16:37,635
payments of 26385? Each one separated by
one year, the first one starting in which

149
00:16:37,635 --> 00:16:42,776
year? End of the first year and when you
do the Pv of it, you better come up with a

150
00:16:42,776 --> 00:16:47,504
100,000. And this is .one of the most
profound Nobel Prize winning points,

151
00:16:47,504 --> 00:16:52,623
please keep it in mind we'll come back to
it later which is the following. You

152
00:16:52,623 --> 00:16:58,014
cannot make money by borrowing and
lending, right? If the present value of

153
00:16:58,014 --> 00:17:03,380
26,380 was different than 100,000,
somebody's made a fool of. So suppose the

154
00:17:03,380 --> 00:17:09,172
bank gives you more than $100,000, the
bank is being an idiot and let me assure

155
00:17:09,172 --> 00:17:15,822
you that won't happen. If you walk off
with the less, somebody's shafting you. So

156
00:17:15,822 --> 00:17:21,527
the question here is, how does the bank
then make money? Well. They makes money by

157
00:17:21,527 --> 00:17:26,463
charging you little bit more on the
borrowing lending rates difference. They

158
00:17:26,463 --> 00:17:31,428
have to feed the family too, right. They
work for you. They create the market but

159
00:17:31,428 --> 00:17:36,391
that's the friction I was talking about
but value cannot be created by borrowing

160
00:17:36,391 --> 00:17:41,361
and lending other wise you and I would be
home and creating value. Yes, it grows

161
00:17:41,361 --> 00:17:46,268
over time but the present value is still
the same as the money I put it, right?

162
00:17:46,268 --> 00:17:50,531
That's the very fundamental point in
finance. Value is created not by

163
00:17:50,531 --> 00:17:56,611
exchanging money which is this, borrowing
and lending. Values creating by coming up

164
00:17:56,611 --> 00:18:03,075
with a new idea for creating value for
society, right? So that's what I'm trying

165
00:18:03,075 --> 00:18:09,525
to say. So let me ask you this, how much
will owe the bank at beginning of year

166
00:18:09,525 --> 00:18:15,624
two? What would you have to do? You would
have to calculate the interest rate, you

167
00:18:15,624 --> 00:18:20,454
would have to calculate the principal
payment, and then you subtract it from

168
00:18:20,454 --> 00:18:26,746
this. Answer is very straight forward. And
this is where I love the math. Just change

169
00:18:26,746 --> 00:18:32,809
one number, make this four. So change the
five to four and do the Pv, what will you

170
00:18:32,809 --> 00:18:39,596
come up with? Please do it. You'll come up
with 63280. So what have I done? Instead

171
00:18:39,596 --> 00:18:45,809
of sticking with time zero I've time
traveled to period one. If I'm at period

172
00:18:45,809 --> 00:18:52,583
one, I've already paid this up. How many
more left? Four left. So m is four and

173
00:18:52,583 --> 00:18:59,705
interest rate is ten percent and how many
am I paying? 26,380. So 83,620 is very

174
00:18:59,705 --> 00:19:06,643
easy to do if you recognize that. So how
would you do the next column? It's 65,

175
00:19:06,643 --> 00:19:13,749
603. How will you do it? You just make m
three so you see what I'm saying? What I'm

176
00:19:13,749 --> 00:19:21,123
saying is the simplest thing in finance is
don't get hung up on the past. Get hung,

177
00:19:21,123 --> 00:19:27,033
whenever you are asked about value of
anything, whether you owe it or you're

178
00:19:27,033 --> 00:19:32,904
getting the value, look to the future and
the problem becomes trivial. Why? Because

179
00:19:32,904 --> 00:19:38,520
if you know all these values, th ese are
just ten percent of this. So this is just

180
00:19:38,520 --> 00:19:43,922
ten percent of this row and then this is
just these two added together is this. So

181
00:19:43,922 --> 00:19:51,262
if I add these two, I get this. So I can
do this in a second as opposed to doing it

182
00:19:51,262 --> 00:19:57,128
over an amortization table. So one more
time. If I were to ask you to do this

183
00:19:57,128 --> 00:20:02,494
problem all over again, what would you do?
You wouldn't use any prop, you only Excel

184
00:20:02,494 --> 00:20:08,491
to solve the problem. So if I asked you,
how much do you owe in a particular year

185
00:20:08,491 --> 00:20:13,759
to the bank which is a very good question
to ask. You will just do what? You will

186
00:20:13,759 --> 00:20:19,415
time travel, right? You remember my
tricks? Jumps across two buildings. First

187
00:20:19,415 --> 00:20:24,844
time is not successful but that's life but
then manages to jump across, right? And if

188
00:20:24,844 --> 00:20:29,882
you haven`t seen Matrix, see Matrix. It`s
much more interesting than this problem.

189
00:20:29,882 --> 00:20:35,078
So, time travel to year, whatever forward,
look forward how many payments are left

190
00:20:35,078 --> 00:20:40,365
just to the PV. Okay? I hope you like this
because this is, if you remember this,

191
00:20:40,365 --> 00:20:45,459
this is finance. Compounding plus this is
mostly what finance is all about. So, it`s

192
00:20:45,459 --> 00:20:48,079
a mindset, you always look forward, okay?
