1
00:00:00,000 --> 00:00:08,033
Okay. The first example I'll do of a PV of
annuity is just like a future value, I

2
00:00:08,033 --> 00:00:15,001
will give you the annuity and I will ask
you to calculate the PV. So, let's read

3
00:00:15,001 --> 00:00:21,047
the problem very carefully. How much
money, and just so that we are, I can

4
00:00:21,047 --> 00:00:27,093
write on this. You see, I press a button
here to be able to write all over, and

5
00:00:27,093 --> 00:00:33,097
then go back and so I do local things that
you can't see. Okay. Anyway, so, I love

6
00:00:33,097 --> 00:00:39,030
technology and here's a person here
sitting next to me. ≫> [laugh]

7
00:00:39,030 --> 00:00:43,370
>> Who is an awesome person and, I,
without him I wouldn't be doing this,

8
00:00:43,370 --> 00:00:48,298
honest. How much money do you need to put
in the bank today so that you can spend

9
00:00:48,298 --> 00:00:53,755
$10,000 every year for the next 25 years
starting at the end of this year? So,

10
00:00:53,755 --> 00:00:58,888
think about this. What kind of problem is
this? This is a problem where you need

11
00:00:58,888 --> 00:01:03,594
money today and you are thinking about
using it and depleting it over time,

12
00:01:03,594 --> 00:01:08,640
alright? So, suppose the interest rate is
five percent and I think we've become very

13
00:01:08,640 --> 00:01:13,868
real with interest rates. We've gone from
eight to five now, yeah, that's what

14
00:01:13,868 --> 00:01:18,163
[laugh] the real world looks like right
now. So, what I'm going to do is I'm going

15
00:01:18,163 --> 00:01:25,223
to draw a timeline. And I'm sorry, this is
something I'll force you to do and another

16
00:01:25,223 --> 00:01:32,352
use of Excel is it comes with a timeline
with the ASL being zero and so on. Okay,

17
00:01:32,352 --> 00:01:39,190
so 25, zero one, so remember more points
in time by one than the number of periods,

18
00:01:39,190 --> 00:01:47,255
right? So, what do I know here? I'm
standing here. What do I know? I know that

19
00:01:47,255 --> 00:01:57,413
I need $10,000, 25 times, alright? So,
this to me is C, R, P, and T. So I know

20
00:01:57,413 --> 00:02:06,534
PMT. But what do I do, want to figure out?
Okay, I need to spend $10,000 but I need

21
00:02:06,534 --> 00:02:14,548
to have it in my bank, because maybe I
just want to retire or maybe over the next

22
00:02:14,548 --> 00:02:22,168
25 years I'm working, but I want $10,000
sets, set aside to, for some needs that I

23
00:02:22,168 --> 00:02:28,072
am planning for me. Whatever the
motivation, you've decided to do this, I

24
00:02:28,072 --> 00:02:36,038
will have to figure out the, now if I did
it the long way, what will I have to do?

25
00:02:36,038 --> 00:02:47,371
$10,000 / (one + r), right? The story is
not over. What do I have to do? $10,000 /

26
00:02:47,371 --> 00:02:56,528
(one + r)^2? And how many times? 25 times.
Now, if I had to do the same thing 25

27
00:02:56,528 --> 00:03:02,956
times, life is easy. But again whose
messing with my fun? Compounding. Because

28
00:03:02,956 --> 00:03:09,616
every time I add another one, this two
becomes a three, four, five, 25 times,

29
00:03:09,616 --> 00:03:17,334
okay? So, what do I have to do? When in
desperation for calculation, go to Excel.

30
00:03:17,334 --> 00:03:26,792
Okay, so I'm going to do, I'm going to go
to Excel. And the good news is, the

31
00:03:26,792 --> 00:03:35,446
previous problem is already there. So, I
do not want to do a PMT first, because I

32
00:03:35,446 --> 00:03:44,251
already know PMT. But I want to do now,
what? Pv, right? Interest rate is now, not

33
00:03:44,251 --> 00:03:52,328
eight%, but it's five%. How many years
left? I believe in, in, in our problem

34
00:03:52,328 --> 00:04:02,293
that we were looking at, just let me just
confirm what it is. We do have 25 years

35
00:04:02,293 --> 00:04:11,785
left and so we are okay on that. What is
the PMT? Well, I know might be empty. And

36
00:04:11,785 --> 00:04:21,701
I'm going to remove that last element
because it's not needed. So, lets say. So,

37
00:04:21,701 --> 00:04:33,859
140,939.45 cents. So, what is that tell
me? I better have $140,939 in the bank to

38
00:04:33,859 --> 00:04:45,097
satisfy the need of spending 10,000 in the
future. So, let me go back to the problem

39
00:04:45,097 --> 00:04:55,094
and tell you what's going on. So, I need
in the bank 140, 939 now let me just for

40
00:04:55,094 --> 00:05:03,135
convenience call it $141,000, alright. So,
let me ask you this. If I put, spend

41
00:05:03,135 --> 00:05:10,797
$10,000 every year and the interest rate
zero, zero, right? How many times have I

42
00:05:10,797 --> 00:05:18,397
spent 10,000, 25 times? Then it's 250,000,
right? So, the number that I'm going to

43
00:05:18,397 --> 00:05:26,481
put in the bank is nowhere close to 250,
it's off by at least 110, approximately.

44
00:05:26,481 --> 00:05:34,116
Why is that? Because when I put $141,000
in the bank, the world is helping me. The

45
00:05:34,116 --> 00:05:39,223
ingenuity of the world is helping me, in
the form of five percent rate of return,

46
00:05:39,223 --> 00:05:45,239
right? So, the, the good news here is when
you do PV in this case, you have to put

47
00:05:45,239 --> 00:05:50,778
much less than what you need in the
future, simply because, as the money is

48
00:05:50,778 --> 00:05:56,969
being withdrawn. The remaining money is
accruing in value because again, of the

49
00:05:56,969 --> 00:06:03,183
positive interest rate. So, is this, this
problem gives you a sense of how to do PV.

50
00:06:03,183 --> 00:06:09,499
And remember, the PV is discounting. So,
every $10,000 in the future is becoming

51
00:06:09,499 --> 00:06:15,205
less so today. And the last $10,000 is
being discounted by 25 years. So, w hat

52
00:06:15,205 --> 00:06:21,215
happens is, you are not multiplying 10,000
by 25 to get this answer. Because of

53
00:06:21,215 --> 00:06:25,972
compounding and a positive interest rate,
you're getting an answer of $141,000

54
00:06:25,972 --> 00:06:30,698
dollars. I would encourage you to do this
in your own time. Try to, after we are

55
00:06:30,698 --> 00:06:37,061
done with this class, see how much money
are you left with at the end of 25 years

56
00:06:37,061 --> 00:06:42,297
if you carry this money forward. And we'll
do this in a context, and why I am

57
00:06:42,297 --> 00:06:47,100
encouraging you to think like that is
simply to confirm that this number is

58
00:06:47,100 --> 00:06:52,409
right under the assumptions. One final
comment before we move on to the next

59
00:06:52,409 --> 00:06:57,242
problem. The interest rate is five percent
here. So, if the world is the same as our

60
00:06:57,242 --> 00:07:01,839
previous problem, it requires a strategy
that is less risky than the eight percent

61
00:07:01,841 --> 00:07:07,282
to follow. So, just wanted to bring that
risk thing that's at the back of your mind

62
00:07:07,282 --> 00:07:11,382
into the picture. Just to show you, you
know the ten, you know the 25, and

63
00:07:11,382 --> 00:07:17,383
basically you know the five. Of course,
the five won't be five the more risk you

64
00:07:17,383 --> 00:07:23,480
take, but that's true about anybody making
an investment, right? So, please remember

65
00:07:23,480 --> 00:07:29,331
that. This problem helps you a ton. What
I'm going to do now is, if you want to

66
00:07:29,331 --> 00:07:34,588
take a break, this is a natural time. But
I'm going to, because we've become

67
00:07:34,588 --> 00:07:39,715
familiar with doing these kinds of
problems, I'm going to take the next

68
00:07:39,715 --> 00:07:45,037
problem on right away. But as I said, I
always take pauses for you to take

69
00:07:45,037 --> 00:07:50,094
ownership. In spite of the fact that you
can pause me anytime, I think it's good

70
00:07:50,094 --> 00:07:58,028
for me to tell you what I think would be a
good time for you to pause. Okay guys, now

71
00:07:58,028 --> 00:08:04,066
I'm going to do a problem on which I will
spend a lot of time. And why am I doing

72
00:08:04,066 --> 00:08:10,096
this, spending a lot of time? You'll see
this is a classic finance problem in the

73
00:08:10,096 --> 00:08:17,047
real word sense. So, here goes the problem
and please read with me. I'm going to try

74
00:08:17,047 --> 00:08:23,092
to highlight things as I go along. You
plan to attend a business school and you

75
00:08:23,092 --> 00:08:28,744
will be forced to take out $100,000 in a
loan at ten%. And the ten percent you'll

76
00:08:28,744 --> 00:08:35,060
see is kind of artificial because I want
to make my life a little easy here.

77
00:08:35,060 --> 00:08:41,097
Hopefully, you don't need to pay ten%, you
need to pay much less. But th e $100,000

78
00:08:41,097 --> 00:08:52,323
is an fact of life. It costs about that
much for two years worth of tuition. And I

79
00:08:52,323 --> 00:09:01,931
teach at the business school and clearly,
I benefit from the value provided by

80
00:09:01,931 --> 00:09:07,019
business school education in people's
willingness to pay, but I want to

81
00:09:07,019 --> 00:09:12,072
emphasize, this is not a small number. And
remember, when you come to school, you're

82
00:09:12,072 --> 00:09:18,051
also giving up an opportunity of working.
So, when you do your calculations to go to

83
00:09:18,051 --> 00:09:23,709
school, it's not a minor thing. And being
in education, a part of me really believes

84
00:09:23,709 --> 00:09:29,773
that things like this class I am knowing
is should be a large part of the future.Of

85
00:09:29,773 --> 00:09:35,576
course, it brings up the questions, how do
people survive if everything is for free

86
00:09:35,576 --> 00:09:41,354
and so on. But I think, I personally feel,
this hundred thousand numbers is a bit too

87
00:09:41,354 --> 00:09:46,952
high, even if I benefit personally from
it. Anyway, you want to figure out your

88
00:09:46,952 --> 00:09:52,595
yearly payments given that you will have
five years to pay back the loan, right?

89
00:09:52,595 --> 00:09:59,414
So, what do I know? We call this guy n, we
call this guy what, PV, FV, PMT? Well, we

90
00:09:59,414 --> 00:10:07,266
call it PV. Why? Because when I walk out
with the bank, from the bank with $100,000

91
00:10:07,266 --> 00:10:14,760
of loan, I have the money, today. But what
do I have to do? In this case, pay a hefty

92
00:10:14,760 --> 00:10:21,912
ten%. But I emphasize again, the good news
is, the interest rate is not that high,

93
00:10:21,912 --> 00:10:29,047
and should not be that high. Let me throw
in the word should there too. Because

94
00:10:29,325 --> 00:10:34,713
simply because it's just too high, from
any standard. Anyway, so at ten percent

95
00:10:34,713 --> 00:10:40,433
simply because you will see later it will
help us with the calculations. So, the

96
00:10:40,433 --> 00:10:47,582
first question to ask ourselves is let's
draw the timeline and in this case there's

97
00:10:47,582 --> 00:10:53,186
five, zero, one, two ,three, four. Now,
I'm going to ask you, is this a real world

98
00:10:53,186 --> 00:10:59,447
problem? And if you tell me no. I'd, I'll
think they'd choose with you, not with me,

99
00:10:59,447 --> 00:11:04,032
right? This is a real world problem, the
only thing that will change is the

100
00:11:04,032 --> 00:11:09,366
numbers, right? So, the quick question to
you is, who decides $100,000? $100,000 a

101
00:11:09,366 --> 00:11:14,850
year, who decides it? Well, all of us
collectively. The fee is determined by the

102
00:11:14,850 --> 00:11:19,983
school or the university, wherever you are
going and the amount you need to borrow

103
00:11:19,983 --> 00:11:24,528
depends on your ability to finance the
education, right? So, you'll borrow, let

104
00:11:24,528 --> 00:11:29,082
say that you're going to borrow a 100,000,
which is two years worth of tuition and,

105
00:11:29,082 --> 00:11:35,521
of course, you need to spend the money on
yourself too, let's keep that aside for a

106
00:11:35,521 --> 00:11:43,327
minute. Who decides the ten%? This is a
very interesting question and goes back to

107
00:11:43,327 --> 00:11:49,441
my emphasis on markets. If it is one
person in the whole market deciding the

108
00:11:49,441 --> 00:11:54,791
interest rate, that person is called a
monopolist. And if, in finance, or

109
00:11:54,791 --> 00:12:01,451
borrowing and lending money, there is one
person you know who will get screwed, us,

110
00:12:01,451 --> 00:12:07,899
the customers or the people borrowing
money. So, that's why competitive markets

111
00:12:07,899 --> 00:12:14,055
are important. Competitive markets are
important so that the consumer benefits.

112
00:12:14,055 --> 00:12:19,521
Not the producer, necessarily. And by the
way, all of us are the same people. It's

113
00:12:19,521 --> 00:12:24,564
not us versus them. I'm just emphasizing
that markets are for people. Markets are

114
00:12:24,564 --> 00:12:29,730
not for one person or a few of us, right?
That's the beauty of markets. Anyway, so

115
00:12:29,730 --> 00:12:34,567
the ten%, hopefully, is coming out of
competition. Why? Because if there are

116
00:12:34,567 --> 00:12:38,754
three banks, you'll always go to the
lowest interest rate, right? So,

117
00:12:38,754 --> 00:12:43,701
competition among banks on the internet,
hopefully is getting better to help you

118
00:12:43,701 --> 00:12:50,033
get a reasonable interest rate. So, R per
period is ten%. And how did you decide

119
00:12:50,033 --> 00:12:55,482
five years? Well it's again, an
interaction between you and the bank. And

120
00:12:55,482 --> 00:13:00,086
interest rates will vary depending on the
periodicity or the length of the, sorry,

121
00:13:00,086 --> 00:13:04,833
the majority of the loan and so on. So,
let's keep that issue right now in the

122
00:13:04,833 --> 00:13:10,391
risk category, right? So, you have five
years to pay it back and the question I'm

123
00:13:10,391 --> 00:13:18,973
asking is what? How much will you pay
every year, per year, right? So, lets do

124
00:13:18,973 --> 00:13:28,702
this problem. And to do this problem, I
have to go to Excel. And I'm going to now

125
00:13:28,702 --> 00:13:35,081
try to do things a little quickly on
Excel. So, lets do it without screwing

126
00:13:35,081 --> 00:13:42,423
things up, obviously. So, what was our
problem now? I know my PV and I'm going to

127
00:13:42,423 --> 00:13:49,537
calculate what? Pmt, right? This is a
number I should know and the bank should

128
00:13:49,537 --> 00:13:58,179
tell me. So, the interest rate is how
much? Interes t rate is10%. And how many

129
00:13:58,179 --> 00:14:06,357
years? Not 25, five. And the next number
is PV, fortunately, if I see it right,

130
00:14:06,357 --> 00:14:16,108
yup. And you got to keep your eye on the
ball. And how much did I borrow? 100,000,

131
00:14:16,108 --> 00:14:26,043
right? So, huge number, so the answer to
this question is 26,380. And I think what

132
00:14:26,043 --> 00:14:34,294
this is telling you is, and I'm, I kind of
rounded things off again without decimals

133
00:14:34,294 --> 00:14:41,771
there. It's telling you that I, or you,
whoever is borrowing the money will get

134
00:14:41,771 --> 00:14:48,567
$100,000 today, but will have to pay
$26,000 plus 380, five times. So, just

135
00:14:48,567 --> 00:14:54,706
pause there. This looks like a huge
number. Now, the number, don't get fixated

136
00:14:54,706 --> 00:14:59,611
on the number. If you were borrowing
10,000 it will be less. If you were

137
00:14:59,611 --> 00:15:04,987
borrowing 50,000 it would be less. If you
are borrowing one million, the payment

138
00:15:04,987 --> 00:15:09,430
will be more. But there is a one to one
relationship between what you are

139
00:15:09,430 --> 00:15:14,626
borrowing and what you have to pay. So
now, let me ask you this question, suppose

140
00:15:14,626 --> 00:15:20,788
the interest rate was zero, suppose you
could go to the bank and just get $100,000

141
00:15:20,788 --> 00:15:26,948
and not pay any interest. How much would
you pay every year? Pretty simple, take

142
00:15:26,948 --> 00:15:32,988
$100,000 and divide by five. In this case,
you're paying 26, 380 more every period,

143
00:15:32,988 --> 00:15:39,454
every year. And for simplicity we've kept
the year as a fixed quantity not a month.

144
00:15:39,454 --> 00:15:46,292
We'll get to that in a second. S, o what
I'm going to do now is I'm going to take

145
00:15:46,292 --> 00:15:56,077
the 26, 380 and do this. What should be
the present value of this? With n, five, r

146
00:15:56,077 --> 00:16:07,335
of ten%. What should be the PV? If you can
answer that question, you know how to mess

147
00:16:07,335 --> 00:16:12,353
with Excel, actually know how to do
something profound. If you do this

148
00:16:12,353 --> 00:16:18,864
exercise, which I encourage you to do. It
has to be a $100,000, right? Because the,

149
00:16:18,864 --> 00:16:26,777
you're just going back and forth with the
same problem. Okay? So, $26,380 is the

150
00:16:26,777 --> 00:16:34,299
amount of money that you'll have to pay
every year on a loan. What I'm going to do

151
00:16:34,299 --> 00:16:40,317
next and I'm going to take a break now.
And I think you need to take a break, is

152
00:16:40,317 --> 00:16:45,986
you know how to do this problem. I'm going
to now use this problems to show you how

153
00:16:45,986 --> 00:16:51,467
great and awesome finance is. And after
that, I will do a couple of other problems

154
00:16:51,467 --> 00:16:57,372
a nd get you completely internalized with
the class today. As I promised, the class

155
00:16:57,372 --> 00:17:03,626
is intense because I'm doing problems. I'm
bringing in the real world. If I were just

156
00:17:03,626 --> 00:17:09,009
doing the formulas, you'd be much happier
because time would just be passing by

157
00:17:09,009 --> 00:17:14,549
quickly but the learning I believe won't
be the same, okay? So, lets take break,

158
00:17:14,549 --> 00:17:20,066
we'll come back and deal with this issue
in a second.
