1
00:00:07,500 --> 00:00:10,790
I'm here to talk a little bit about the
new syllabus that I

2
00:00:10,790 --> 00:00:16,420
put up and welcome you to the new second
version of the class.

3
00:00:16,420 --> 00:00:19,310
And talk a little bit about how the class
has changed.

4
00:00:19,310 --> 00:00:23,610
I would encourage you to go see the
original introduction to the syllabus.

5
00:00:23,610 --> 00:00:27,480
Because in the content of the class, not
much has changed.

6
00:00:27,480 --> 00:00:28,390
So let's get started.

7
00:00:29,480 --> 00:00:32,930
addition to the first offering in my book
went

8
00:00:32,930 --> 00:00:37,130
reasonably well.
Of course I don't see you in real time.

9
00:00:37,130 --> 00:00:39,170
so that makes it difficult to evaluate.

10
00:00:39,170 --> 00:00:40,240
But we have a lot of data.

11
00:00:41,370 --> 00:00:44,910
And I personally thought going in that
this

12
00:00:44,910 --> 00:00:48,230
would be very different from face to face.

13
00:00:48,230 --> 00:00:49,470
It was very different.

14
00:00:49,470 --> 00:00:52,060
But it was empowering and uplifting for
me.

15
00:00:53,430 --> 00:00:58,400
Having said that I hope that's true also.
Of students

16
00:00:58,400 --> 00:01:02,450
who took the class because it's easy for
me to feel good about

17
00:01:02,450 --> 00:01:06,410
the class, but it's the real test is if
the students feel good.

18
00:01:06,410 --> 00:01:11,010
There again the feed back seems to suggest
that more or less things went well.

19
00:01:12,090 --> 00:01:14,190
More importantly I hope you had fun.

20
00:01:14,190 --> 00:01:17,400
I mean I say this often in the videos that
we'll

21
00:01:17,400 --> 00:01:20,870
see that you don't learn because somebody
wants you to learn.

22
00:01:20,870 --> 00:01:22,940
Learning is a very personal thing.

23
00:01:22,940 --> 00:01:23,590
And you'll

24
00:01:23,590 --> 00:01:30,482
learn when your kind of happy and open, so
I hope that's true this time around to.

25
00:01:30,482 --> 00:01:36,930
Now for a brief uh,uh, cover, are
uh,covering

26
00:01:36,930 --> 00:01:39,980
officials that matter in the second
edition and

27
00:01:39,980 --> 00:01:43,180
there largely motivated by feedback and
reflection on

28
00:01:43,180 --> 00:01:46,450
the first edition, a little bit of
precaution.

29
00:01:46,450 --> 00:01:48,650
I'm not going to retape the

30
00:01:48,650 --> 00:01:51,190
whole first edition introduction.

31
00:01:51,190 --> 00:01:54,230
I call it like a textbook first in the
preface.

32
00:01:54,230 --> 00:01:56,470
And the reason is that seems a little
unnatural.

33
00:01:56,470 --> 00:02:00,690
So what I'm going to do is, I'm going to
do a very beef, brief introduction

34
00:02:00,690 --> 00:02:02,830
to the second edition, and that will be

35
00:02:02,830 --> 00:02:05,730
followed by the introduction to the first
class.

36
00:02:06,780 --> 00:02:09,130
At the same time, the syllabus and hard
copy is

37
00:02:09,130 --> 00:02:11,690
available to use, so I would encourage you
to read it.

38
00:02:11,690 --> 00:02:13,960
And actually, if you go to the syllabus,

39
00:02:13,960 --> 00:02:15,815
you've pretty much done the class.
It's a really

40
00:02:15,815 --> 00:02:15,910
[LAUGH]

41
00:02:15,910 --> 00:02:19,150
long syllabus I encourage you to read
that.

42
00:02:19,150 --> 00:02:20,450
So, what are the changes?

43
00:02:23,260 --> 00:02:25,950
It has almost, as I said.

44
00:02:25,950 --> 00:02:29,280
The syllabus has almost everything that
you need.

45
00:02:29,280 --> 00:02:34,950
So the changes are structured and, not to
deny the importance of structure.

46
00:02:34,950 --> 00:02:39,060
I want to re-emphasize that content is not
the issue

47
00:02:39,060 --> 00:02:42,590
here, it's how we manage the class that's
the issue.

48
00:02:43,630 --> 00:02:46,760
So, and another thing, and I'll talk about
the structure in a second.

49
00:02:46,760 --> 00:02:48,410
Another thing is we

50
00:02:48,410 --> 00:02:54,010
absolutely must have teaching assistants,
and I talk in the first edition video

51
00:02:54,010 --> 00:02:59,830
about Nate being the teaching assistant.
The new teaching assistant is Elizabeth.

52
00:02:59,830 --> 00:03:05,380
And so whenever you hear the word Nate
just change it, think Elizabeth.

53
00:03:05,380 --> 00:03:08,550
And that'll, that'll make a little bit of
a difference in how

54
00:03:08,550 --> 00:03:13,350
you view things But there's an important
change on the TA front,

55
00:03:13,350 --> 00:03:18,070
which has been facilitated by Coursera,
and I think it's a great idea.

56
00:03:18,070 --> 00:03:22,820
And that is, you'll have community TAs, we
are hoping.

57
00:03:22,820 --> 00:03:25,160
These are people who did well in the
previous

58
00:03:25,160 --> 00:03:29,020
class, but because I don't believe in
grades, quite honestly,

59
00:03:29,020 --> 00:03:31,840
that much, because, you know, it's, in my
book,

60
00:03:31,840 --> 00:03:34,410
that's just a partial measure, measure of
what you've learned.

61
00:03:35,420 --> 00:03:38,450
We have tried to see the participation on
the forums.

62
00:03:39,680 --> 00:03:44,900
And see, and pick people who did well on
both dimensions.

63
00:03:44,900 --> 00:03:47,560
Having said that, we don't have a kind of,
we

64
00:03:47,560 --> 00:03:51,930
are not giving these people special
certificates and certifying them.

65
00:03:51,930 --> 00:03:55,530
This is contributing to the realization I
had in the

66
00:03:55,530 --> 00:03:59,080
last class that my role is very, small
actually in learning.

67
00:04:00,080 --> 00:04:04,750
And I think that feeling, this whole
experience last time made me realize that

68
00:04:04,750 --> 00:04:10,420
this one person-centric learning model is
kind of maybe a little bit broken.

69
00:04:10,420 --> 00:04:16,190
So this provision of new TAs that can help
you learn is, is a fascinating thing.

70
00:04:16,190 --> 00:04:21,200
And I must say, the forums last time, were
one of the best things that could have

71
00:04:21,200 --> 00:04:26,925
happened, and oddly enough, you guys feel
more comfortable talking

72
00:04:26,925 --> 00:04:28,110
[UNKNOWN].

73
00:04:28,110 --> 00:04:33,100
You guys feel more comfortable talking to
each other on the forum electronically,

74
00:04:33,100 --> 00:04:37,370
maybe the anonymity and so on, helps than
students in real time do.

75
00:04:37,370 --> 00:04:40,010
Even in the business school, face-to-face.

76
00:04:40,010 --> 00:04:43,990
So, I would encourage you to be open to
the, to Elizabeth, of course.

77
00:04:43,990 --> 00:04:46,740
But also to community TAs.

78
00:04:46,740 --> 00:04:48,370
Because they are people who are trying

79
00:04:48,370 --> 00:04:51,330
to help this whole movement of online
education.

80
00:04:52,680 --> 00:04:53,250
So, that's a little

81
00:04:53,250 --> 00:04:53,900
bit about that.

82
00:04:53,900 --> 00:04:56,982
Let me go and talk about the structural
change.

83
00:04:56,982 --> 00:05:01,450
What I had envisioned in in, in, in the
first offering was something

84
00:05:01,450 --> 00:05:05,480
that I had kind of taken from other people
who have done it.

85
00:05:05,480 --> 00:05:08,182
We are early in the process, but at the
same time we are

86
00:05:08,182 --> 00:05:11,970
kind of, a little bit of doing the buying
the earth thing here.

87
00:05:11,970 --> 00:05:18,110
Especially given the large classes.
So I thought, that for a ten week class.

88
00:05:18,110 --> 00:05:18,790
To make

89
00:05:18,790 --> 00:05:20,110
it synchronous.

90
00:05:20,110 --> 00:05:23,100
By that, I don't mean exactly at the same
time.

91
00:05:23,100 --> 00:05:26,290
But to release do it similar to a
real-time class.

92
00:05:26,290 --> 00:05:29,900
So you don't do the whole course in one
week in a face-to-face class.

93
00:05:29,900 --> 00:05:31,460
You do it one week at a time.

94
00:05:32,605 --> 00:05:35,950
Or one, you meet twice a week face to
face.

95
00:05:35,950 --> 00:05:38,180
And the reason for that is pretty, pretty
good.

96
00:05:38,180 --> 00:05:39,905
For example, again, in the business school

97
00:05:39,905 --> 00:05:43,050
when I teach, people don't do individual
assignment.

98
00:05:43,050 --> 00:05:44,580
They work together.

99
00:05:44,580 --> 00:05:49,310
So, releasing one material, one block at a
time which I had

100
00:05:49,310 --> 00:05:54,360
taught would be one week works because
there you get participate on

101
00:05:54,360 --> 00:05:57,470
forums and then a half the people are just
zipping from the

102
00:05:57,470 --> 00:06:01,360
whole class and the other half are going
at their own pace.

103
00:06:01,360 --> 00:06:03,440
I think one week didn't make sense,
because

104
00:06:03,440 --> 00:06:05,780
well after all, week is an artificial
thing right?

105
00:06:05,780 --> 00:06:09,810
I don't when it was created, but seven
days in a week, and 24 hours

106
00:06:09,810 --> 00:06:15,410
a day, I mean, we work in that world, but
why learn in that world, right?

107
00:06:15,410 --> 00:06:19,750
So we realize pretty early that releasing
materials two weeks at a time.

108
00:06:19,750 --> 00:06:22,480
These are content, the videos, two weeks

109
00:06:22,480 --> 00:06:25,630
worth of videos made more sense
pedagogically.

110
00:06:25,630 --> 00:06:30,197
Because the frit, the thinking that's
going on behind this class.

111
00:06:30,197 --> 00:06:34,928
But we also realise that because you are
already busy to expect you to

112
00:06:34,928 --> 00:06:38,248
stay on top of assignments every two
weeks, and

113
00:06:38,248 --> 00:06:42,120
assignments by the way are the key to
learning.

114
00:06:42,120 --> 00:06:46,360
I mean, I can come and talk, and coach you
and emphasize the basics.

115
00:06:46,360 --> 00:06:48,480
But after that, you got to run with it.

116
00:06:48,480 --> 00:06:53,850
So what we decided to is release two weeks
worth of videos but give you

117
00:06:53,850 --> 00:06:59,900
one week after that to assimilate and to
think through,

118
00:06:59,900 --> 00:07:02,080
and therefore finish the assignments.

119
00:07:02,080 --> 00:07:06,900
So I would encourage you very strongly, to
participate on forums,

120
00:07:06,900 --> 00:07:11,790
thinking of this class as a three week
block by block class.

121
00:07:11,790 --> 00:07:17,870
And because there are five such blocks, we
have a total of 15 weeks.

122
00:07:17,870 --> 00:07:21,280
So to emphasize again, two weeks worth of
videos

123
00:07:21,280 --> 00:07:25,110
chopped up into pieces that are digestible
by you.

124
00:07:25,110 --> 00:07:30,210
Always doing problems in those videos that
you will see I talk about all the time.

125
00:07:30,210 --> 00:07:33,090
But again having a sense of assimilation
and

126
00:07:33,090 --> 00:07:37,300
appropriate block of time is about three
weeks.

127
00:07:37,300 --> 00:07:38,810
I hope that makes sense.

128
00:07:38,810 --> 00:07:42,470
I think it makes a lot of sense to me, and
I, I really appreciate

129
00:07:42,470 --> 00:07:47,400
the feedback from people on an ongoing
basis on the forums on these two issues.

130
00:07:47,400 --> 00:07:50,740
So these are the main changes, you know,
how we'll do them.

131
00:07:50,740 --> 00:07:54,450
there's a little bit change to the grading
system which I'll get into in a

132
00:07:54,450 --> 00:08:00,940
second, but before that let me tell you
changes that I will not make because.

133
00:08:00,940 --> 00:08:05,190
I mean those are just to close to my heart
issues,

134
00:08:05,190 --> 00:08:07,830
oh, and related to learning not nothing to
do with finance.

135
00:08:07,830 --> 00:08:09,020
So, let me emphasis those.

136
00:08:10,630 --> 00:08:14,470
Assignments are very difficult, especially
for people

137
00:08:14,470 --> 00:08:16,460
who haven't had much of a background

138
00:08:16,460 --> 00:08:21,200
in analytical, or mathematical way of
thinking.

139
00:08:21,200 --> 00:08:22,460
But they remain difficult.

140
00:08:22,460 --> 00:08:26,730
And the reason is I'm very concerned in
doing this

141
00:08:26,730 --> 00:08:28,430
that my learning philosophy and

142
00:08:28,430 --> 00:08:32,100
their expectations match, University of
Michigan.

143
00:08:32,100 --> 00:08:36,230
I believe University of Michigan is one of
the best universities in the world.

144
00:08:36,230 --> 00:08:41,590
I'm privileged to be here and it's my
responsibility to maintain.

145
00:08:41,590 --> 00:08:44,510
My own learning philosophy and
expectations of

146
00:08:44,510 --> 00:08:47,590
the larger community around me and the
world.

147
00:08:47,590 --> 00:08:52,140
And basically you want to learn something
that is challenging and

148
00:08:52,140 --> 00:08:56,050
actually teaches you how to think and
that's the philosophy behind it.

149
00:08:56,050 --> 00:09:00,930
So, yes assignments will be difficult, but
what will be more difficult is you'll get

150
00:09:00,930 --> 00:09:06,710
the sense like I'm doing the easy stuff.
In the videos, and you are doing the most

151
00:09:06,710 --> 00:09:11,100
difficult task well, I think that's that's
the real life is.

152
00:09:11,100 --> 00:09:16,530
I can only teach you the different leg of
pieces you mean you will get the sense

153
00:09:16,530 --> 00:09:19,270
of, the feel of each piece that each

154
00:09:19,270 --> 00:09:23,090
real life problem is just putting these
pieces differently.

155
00:09:23,090 --> 00:09:25,560
So, that's all that's going on.

156
00:09:25,560 --> 00:09:30,060
So I'll emphasize each piece and a little
bit of putting them together.

157
00:09:30,060 --> 00:09:31,830
But you'll learn, only by

158
00:09:31,830 --> 00:09:33,385
doing assignments that are real

159
00:09:33,385 --> 00:09:33,530
[UNKNOWN]

160
00:09:33,530 --> 00:09:34,710
and challenging.

161
00:09:34,710 --> 00:09:38,360
I promise you there's no artificial
question

162
00:09:38,360 --> 00:09:39,740
that you'll never see in the real world.

163
00:09:39,740 --> 00:09:42,350
That's why I just think finance is
awesome.

164
00:09:42,350 --> 00:09:44,670
I mean it's, anyway, you'll see, I
actually

165
00:09:44,670 --> 00:09:47,620
think it's the best thing created by
mankind.

166
00:09:48,680 --> 00:09:52,810
Solutions to assignments will not be
provided by me, and this was one

167
00:09:52,810 --> 00:09:57,030
request that came often, and I kept
pushing back, and let me emphasize why.

168
00:09:58,240 --> 00:09:59,190
It fits

169
00:09:59,190 --> 00:10:03,540
in with my philosophy that solutions are
the least part of life.

170
00:10:03,540 --> 00:10:07,920
Questions are far more important, and you
can figure out solutions.

171
00:10:07,920 --> 00:10:12,990
The other thing is in finance you seem to
get one answer, but that is wrong, right?

172
00:10:12,990 --> 00:10:15,710
So all answers in life are wrong when
you're thinking about

173
00:10:15,710 --> 00:10:17,620
the future because you're making
assumptions

174
00:10:17,620 --> 00:10:19,970
and you're putting up a framework.

175
00:10:19,970 --> 00:10:22,440
So, in some sense, giving you answers

176
00:10:22,440 --> 00:10:24,730
to assignment is kind of defeating the
purpose.

177
00:10:24,730 --> 00:10:26,660
But there is a deeper reason.

178
00:10:26,660 --> 00:10:30,645
One of the most important challenges we'll
face in online education

179
00:10:30,645 --> 00:10:34,420
as we take it to a deeper level is
something called copyright.

180
00:10:35,810 --> 00:10:38,520
And copyright issues have already
confronted with.

181
00:10:39,600 --> 00:10:44,370
In the sense that I can not teach as
freely.

182
00:10:44,370 --> 00:10:46,850
As I would face to face, because the law

183
00:10:46,850 --> 00:10:50,020
is much more lenient where the people are
registered,

184
00:10:50,020 --> 00:10:52,630
they are paying for the resources, and so
on.

185
00:10:52,630 --> 00:10:56,860
So for example, I cannot use some
resources at all.

186
00:10:56,860 --> 00:11:01,340
I cannot copy from a textbook, or borrow
from a textbook a question.

187
00:11:01,340 --> 00:11:03,420
It may seem a harmless thing to do.

188
00:11:03,420 --> 00:11:05,830
Unless I assign that book to you
officially,

189
00:11:05,830 --> 00:11:08,300
which kind of defeats the purpose of a
free class.

190
00:11:08,300 --> 00:11:09,180
Alright.

191
00:11:09,180 --> 00:11:15,810
So, all the assignments were created by me
and my TAs over a period of several weeks.

192
00:11:15,810 --> 00:11:16,970
They were tested.

193
00:11:16,970 --> 00:11:20,690
They were put up in the archetecture of
coursera, and

194
00:11:20,690 --> 00:11:23,280
I cannot give you solutions because, you
know the web.

195
00:11:23,280 --> 00:11:27,960
You know, once the solutions are out
there, they kind of become, a lot

196
00:11:27,960 --> 00:11:29,510
of people just look at them, and

197
00:11:29,510 --> 00:11:31,560
it defeats the purpose of doing an
assignment.

198
00:11:31,560 --> 00:11:33,238
So, this is what I'll encourage you to do.

199
00:11:33,238 --> 00:11:40,990
Collaborate among yourselves, but do not
post answers, because it really doesn't

200
00:11:40,990 --> 00:11:43,700
help, and in fact you'll be surprised,
pleasantly,

201
00:11:43,700 --> 00:11:47,130
how people in the forum themselves
discourage such behavior.

202
00:11:48,360 --> 00:11:50,120
So, what I would say is this.

203
00:11:50,120 --> 00:11:52,710
Work with each other, figure out the
framework,

204
00:11:52,710 --> 00:11:55,550
don't worry too much about the final
answer.

205
00:11:55,550 --> 00:11:58,012
When the machines grade you because I
can't,

206
00:11:58,012 --> 00:12:00,640
my TAs can't, a thousands of students we
tried

207
00:12:00,640 --> 00:12:05,960
to give you the hints of where you could
have gone wrong and I promise that I,

208
00:12:05,960 --> 00:12:11,190
not promise, I can guarantee you that if
online education have to take off

209
00:12:11,190 --> 00:12:16,280
the way we assess your assignments has to
become richer.

210
00:12:16,280 --> 00:12:18,410
You can't just get a zero for a wrong

211
00:12:18,410 --> 00:12:21,280
answer and ten out of ten for a right
answer.

212
00:12:21,280 --> 00:12:24,130
You have to be doing the steps properly.

213
00:12:24,130 --> 00:12:27,310
And I promise you, and I can almost
guarantee you

214
00:12:27,310 --> 00:12:31,170
that if you don't keep up with assessment
methodologies that can,

215
00:12:31,170 --> 00:12:35,040
are possible through machines, online
education will suffer.

216
00:12:35,040 --> 00:12:37,480
Because assessment is extremely important.

217
00:12:37,480 --> 00:12:40,560
And by that I don't mean a certificate.

218
00:12:40,560 --> 00:12:44,520
Or a degree, I mean your ability to see
whether you are on the right track or not.

219
00:12:45,740 --> 00:12:47,460
So do collaborate.

220
00:12:47,460 --> 00:12:50,350
But please remember, honor code, on accord
I mean, you

221
00:12:50,350 --> 00:12:53,260
have Jiminy Cricket, I don't know if you
saw Pinocchio.

222
00:12:53,260 --> 00:12:56,710
Each one of us has a Jiminy Cricket in us.
I can't tell you.

223
00:12:56,710 --> 00:12:58,220
How to be good or bad.

224
00:12:58,220 --> 00:13:00,640
You know when you're doing something good,

225
00:13:00,640 --> 00:13:03,260
you also know when you're crossing the
line.

226
00:13:03,260 --> 00:13:05,641
So what is crossing a line here.

227
00:13:05,641 --> 00:13:10,380
It's very tough to articulate, but I think
you'll know when you cross it, and

228
00:13:10,380 --> 00:13:13,280
try to follow honor code or working

229
00:13:13,280 --> 00:13:16,319
hard, collaborating, but still owning your
own stuff.

230
00:13:17,470 --> 00:13:19,810
It's true here, and it's true in life.

231
00:13:20,810 --> 00:13:22,160
And I hope you

232
00:13:22,160 --> 00:13:24,380
don't take it as some rule I'm telling
you.

233
00:13:24,380 --> 00:13:25,010
I'm not.

234
00:13:25,010 --> 00:13:29,020
I'm don't, I have, my honor code can't be
better than yours for you.

235
00:13:29,020 --> 00:13:30,730
That's my genuine belief.

236
00:13:30,730 --> 00:13:32,229
You just have to listen to your own voice.

237
00:13:33,400 --> 00:13:36,120
So one last thing having said that
evaluation grading is

238
00:13:36,120 --> 00:13:38,638
not important and I'm not going to talk
about it.

239
00:13:38,638 --> 00:13:38,859
And

240
00:13:38,859 --> 00:13:39,506
[LAUGH]

241
00:13:39,506 --> 00:13:42,320
the reason is very simple and that is that
there is a

242
00:13:42,320 --> 00:13:47,200
lot of desire among all of you to get some
kind of certification.

243
00:13:47,200 --> 00:13:51,370
And this is another tough issue for online
of education for various reasons.

244
00:13:51,370 --> 00:13:53,500
And you can figure out what those reasons
are.

245
00:13:53,500 --> 00:13:58,720
What I'm going to do here is give you a
sense of how we will evaluate you.

246
00:13:58,720 --> 00:14:03,310
First and foremost, you can learn at your
own pace.

247
00:14:03,310 --> 00:14:04,740
You do not have

248
00:14:04,740 --> 00:14:06,060
to have a certificate.

249
00:14:06,060 --> 00:14:08,460
And that's a fascinating thing about
online learning.

250
00:14:09,570 --> 00:14:12,840
But in order to get a certificate, we have
to have some standards.

251
00:14:12,840 --> 00:14:16,573
So we have pass, fail kind of grading
going on at the end of the class.

252
00:14:17,630 --> 00:14:19,970
And now, let me tell you specifics.

253
00:14:19,970 --> 00:14:22,550
So, we have nine assignments and one
final,

254
00:14:22,550 --> 00:14:25,810
that kind of fits the ten week learning
model.

255
00:14:25,810 --> 00:14:28,560
Of course, there's one week every two
weeks of assimilation.

256
00:14:29,680 --> 00:14:33,580
You'll have nine assignments and one
final, you have

257
00:14:33,580 --> 00:14:38,520
to get 70% on five out of the nine
assignments.

258
00:14:38,520 --> 00:14:43,210
So five, out of the nine, I said, you
know, put up ten fingers over there.

259
00:14:43,210 --> 00:14:45,475
So five out of nine, you've got to get

260
00:14:45,475 --> 00:14:48,500
70%, and you've got to get 70% on the
final.

261
00:14:48,500 --> 00:14:50,340
Now here's the good news.

262
00:14:50,340 --> 00:14:55,090
Though you will have a deadline for both
the assignments and

263
00:14:55,090 --> 00:14:59,710
the finals, shot a deadline for the final,
then the assignments.

264
00:15:01,520 --> 00:15:05,410
You have two tries on each one, and that
tells

265
00:15:05,410 --> 00:15:09,020
you how difficult it is for us to create
assessments.

266
00:15:09,020 --> 00:15:11,480
Because I can't give you the same
questions every time, so there's

267
00:15:11,480 --> 00:15:15,060
some randomness built in, and we have to
create a lot more.

268
00:15:15,060 --> 00:15:20,910
Assessment then be actually you see.
So you got two tries at each.

269
00:15:20,910 --> 00:15:25,410
And the reason is I'm not interested in
learning what you don't know.

270
00:15:25,410 --> 00:15:29,140
I'm really keen on learning what you do
know.

271
00:15:29,140 --> 00:15:34,700
And that I think this whole assimilation
time, this two attempts at everything

272
00:15:34,700 --> 00:15:38,130
I hope makes life easier for you to show
me what you know.

273
00:15:39,410 --> 00:15:45,870
Okay, so this is where I'll stop now.
But I look forward to seeing you soon.

274
00:15:45,870 --> 00:15:46,850
And

275
00:15:46,850 --> 00:15:53,250
I guarantee you that if you haven't done
online before, it's going to be different.

276
00:15:53,250 --> 00:15:57,950
It's going to be different in both good
and not so good ways.

277
00:15:57,950 --> 00:16:01,640
But I, I encourage you and I challenge
you.

278
00:16:01,640 --> 00:16:06,910
To take take this on.
Don't worry about perfection here.

279
00:16:06,910 --> 00:16:08,931
Remember, there are thousands of students
just at

280
00:16:08,931 --> 00:16:12,890
Course, Coursera, actually across 2
million, something like that.

281
00:16:12,890 --> 00:16:21,040
So you cannot expect technology, learning,
and this close, this class in particular.

282
00:16:21,040 --> 00:16:24,810
To work perfectly all the time.
You want to learn.

283
00:16:24,810 --> 00:16:30,100
Learning is messy, and learning is fun.
So with that, hope to see you soon,

284
00:16:31,150 --> 00:16:36,170
and it will be a pleasure having a new
class one more time, starting pretty soon.

285
00:16:36,170 --> 00:16:37,190
See you, bye.

