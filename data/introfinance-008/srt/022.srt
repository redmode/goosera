1
00:00:01,063 --> 00:00:08,092
Hi. Welcome. This is Week three and I
wanted to give you a sense today of what

2
00:00:08,092 --> 00:00:15,053
we did last week, like I did last time.
That's the first part of the agenda but

3
00:00:15,053 --> 00:00:22,022
the second part of the agenda is to move
on to using the stuff you used yesterday

4
00:00:22,022 --> 00:00:28,048
to something that's extremely important.
Which is, how do you make decisions using

5
00:00:28,048 --> 00:00:34,059
the tools we have acquired? Obviously,
even yesterday, or last time, we did use

6
00:00:34,059 --> 00:00:40,085
tools to make decisions. But this has a
slightly different twist, and an important

7
00:00:40,085 --> 00:00:47,019
one. And, it will give you a flavor of how
the real world works. Again, everything

8
00:00:47,019 --> 00:00:53,002
will be done, first, with an example. And
as promised, my job here is to make you

9
00:00:53,002 --> 00:00:59,027
love the maths. And because it is possible
and not because I am artificially trying

10
00:00:59,027 --> 00:01:05,001
to do so. And the only way to do that is
to show the applicability of something

11
00:01:05,001 --> 00:01:10,054
before you start throwing math or formulas
at it. So I hope you like that style. So,

12
00:01:10,054 --> 00:01:18,660
let's get started, So, today is Week
three. As always, the first thing that I

13
00:01:18,660 --> 00:01:28,206
do, oops, the first the first thing that
I'll do, as always, is to recap the

14
00:01:28,206 --> 00:01:35,454
previous week. And last time, what we did
was and I want to just give you a flavor

15
00:01:35,454 --> 00:01:43,036
with a little bit of writing on the side
is, last time, we defined and worked with

16
00:01:43,036 --> 00:01:51,748
things like PV, FV. This is present value,
future value. And if you want to draw a

17
00:01:51,748 --> 00:01:59,261
timeline, what we did was we recognized
that this was somewhere here and this was

18
00:01:59,261 --> 00:02:06,191
somewhere here. The P, the length of this
interval are number of periods, clearly

19
00:02:06,191 --> 00:02:12,298
depends on the nature of the problem. So,
I'm just picking ten, just for the heck of

20
00:02:12,298 --> 00:02:17,880
it. We recognize how to move forward and
back, using just PV, FV. And in the

21
00:02:17,880 --> 00:02:23,520
beginning, we just concentrated on one
cash flow. That was Week one. Week two, we

22
00:02:23,520 --> 00:02:30,731
introduced something called PMT which can
occur every period. The little bit of

23
00:02:30,731 --> 00:02:38,910
artificiality about PMT is that it's fixed
for every period. But as we saw yesterday,

24
00:02:38,910 --> 00:02:46,029
in most situations, or in many situations,
we, ourselves, choose to keep that number

25
00:02:46,029 --> 00:02:52,019
the same. For example, loans are taken on
fixed rate interest, we deposit m oney

26
00:02:52,019 --> 00:02:57,468
into banks or into a portfolio based on
our needs for the future and so on. But at

27
00:02:57,468 --> 00:03:02,322
the, at the reasonably constant level. But
the important point is not that this

28
00:03:02,322 --> 00:03:08,276
number is exactly fixed. Is that this
being fixed allows us a lot of flexibility

29
00:03:08,276 --> 00:03:14,149
to do problems and we can later like
today, change PMT and make it different

30
00:03:14,149 --> 00:03:19,789
every year, but the process, the way of
thinking is exactly the same. So, what I'm

31
00:03:19,789 --> 00:03:29,155
going to do now is I'm going to do a recap
of last week using something that we did

32
00:03:29,155 --> 00:03:37,774
last time. So, I'll give you a minute to
read this problem again, and you will

33
00:03:37,774 --> 00:03:42,821
recognize this problem. And, reason I'm
doing it is because it kind of captured

34
00:03:42,821 --> 00:03:48,000
everything we did last time. But, as you
read it, you should see a difference. So,

35
00:03:48,000 --> 00:03:53,623
I am throwing in something new into the
problem so that you can think creatively

36
00:03:53,623 --> 00:03:58,929
to do this problem again. So, the problem
again states that you're 30 years old. You

37
00:03:58,929 --> 00:04:04,442
believe you will save for the next twenty
years, 'til you're 50. This is exactly the

38
00:04:04,442 --> 00:04:10,901
same as last time. For ten years following
that until you retired at age 60, you have

39
00:04:11,129 --> 00:04:16,575
an inability because of your expenses,
college expenses, weddings, and so on, to

40
00:04:16,575 --> 00:04:21,921
save. And remember you're at 30, you're
trying to figure your life out in the

41
00:04:21,921 --> 00:04:28,484
future. And it's not difficult to do, this
is all for your own thinking, not somebody

42
00:04:28,484 --> 00:04:35,704
else telling you what to do. So, I'm kinda
of empowering you to think for yourself.

43
00:04:35,704 --> 00:04:44,151
Now, at 60, if you want to guarantee
yourself $8,000 per month after 'til you

44
00:04:44,151 --> 00:04:50,886
"are no more at 80". How much do you need
to save for the next twenty years starting

45
00:04:50,886 --> 00:04:58,443
at age 30? But the one wrinkle I've thrown
in is I've changed this from an annual to

46
00:04:58,443 --> 00:05:04,494
a monthly problem. And I'm going to let
that stay up there for a second, alright,

47
00:05:04,494 --> 00:05:10,173
since we have seen this problem before,
I'm creating a new twist and we shouldn't

48
00:05:10,173 --> 00:05:16,334
be able to, you know worry too much about
the fact that there's months now. On the

49
00:05:16,334 --> 00:05:22,252
other hand, I think the problem becomes
much richer, much more real world, right,

50
00:05:22,252 --> 00:05:27,979
because of compounding and you should
always pause before you say that. Okay, so

51
00:05:27,979 --> 00:05:34,429
let's get started and I'm going to, since
we are beginning this class, I'm going to

52
00:05:34,429 --> 00:05:39,947
just try to address this issue right away.
So, first thing you always do is draw a

53
00:05:39,947 --> 00:05:44,933
timeline. And I'm borrowing this timeline
from the last time. But notice that I've

54
00:05:44,933 --> 00:05:50,605
done something. And if you're looking at
the slide carefully, what you'll notice is

55
00:05:50,605 --> 00:05:56,470
that I have made my life very simple by
doing the first thing, which is under my

56
00:05:56,470 --> 00:06:03,475
control, is defining a period according to
the nature of the beast. So, the problem

57
00:06:03,475 --> 00:06:09,621
here says that I will be saving monthly
and the timeline converted to months,

58
00:06:09,621 --> 00:06:15,947
solves a lot of my problems. So, the
question now is, between years 30 and 50,

59
00:06:15,947 --> 00:06:21,518
there were twenty years, right? But
actually there are 240 months. Again,

60
00:06:21,518 --> 00:06:27,504
between the years 60 and 80, there were
twenty years that 240 months. So, the

61
00:06:27,504 --> 00:06:34,411
question is between 50 and 60, when I'm
not able to save, but I'm not dis-saving,

62
00:06:34,411 --> 00:06:41,642
right? How many years are there? Ten, but
recognize that no longer can you work with

63
00:06:41,642 --> 00:06:48,218
years. And therefore, you have to convert
that 220 months. So, that's basically the

64
00:06:48,218 --> 00:06:54,736
nature of the beast. So let's, let's get
started and do this problem a little bit a

65
00:06:54,736 --> 00:07:00,694
little bit faster than last time. So, what
I'm going to do is I'm going to show you

66
00:07:00,694 --> 00:07:06,305
months coming here. And why am I stopping
at this end? Why am I time traveling?

67
00:07:06,305 --> 00:07:12,552
Because remember, Finance forces you to
look forward, and that's one of the best

68
00:07:12,552 --> 00:07:17,928
things about it. And as I told you last
time, don't look back, look forward

69
00:07:17,928 --> 00:07:23,761
because you're making decisions. And then
decide, when you look forward, what is it

70
00:07:23,761 --> 00:07:29,875
that you're trying to solve. So, here, you
want how much per month? I believe, my

71
00:07:29,875 --> 00:07:38,111
numbers now are 8,000 per month, you need
for a long period of time. Right? So, the

72
00:07:38,111 --> 00:07:46,956
PMT you'll put in solving this problem is
8,000. And we'll do it in a second. N is

73
00:07:46,956 --> 00:07:55,308
how much? N now is not twenty years, it's
240 months. Basically, this has to match

74
00:07:55,308 --> 00:08:03,203
this. If these two are not aligned, you
have a problem. Most importantly you have

75
00:08:03,203 --> 00:08:10,922
to align this with the problem. So, if an
annual interest rate is stated at eight%,

76
00:08:10,924 --> 00:08:19,828
the monthly interest rate should be 0.08 /
twelve. If you were getting compounding on

77
00:08:19,828 --> 00:08:27,007
a quarterly basis, it would be different.
Daily basis, it will be different. So, the

78
00:08:27,007 --> 00:08:32,063
question here is now how do I solve this
problem and what would that tell me,

79
00:08:32,063 --> 00:08:37,078
because you've done this problem before,
I'm going to recognize you remember that

80
00:08:37,078 --> 00:08:42,044
the first thing I'll try to do is PV of
what I need and I'am going to put

81
00:08:42,044 --> 00:08:47,292
subscript here 60, so that it, you see the
calculator Excel doesn't know where you

82
00:08:47,292 --> 00:08:52,184
are, but you do, right. So, you know when
you solve this problem, you are not at

83
00:08:52,184 --> 00:08:56,398
point 30, you're at point 60 in the
future, you're already there in your mind

84
00:08:56,398 --> 00:09:01,466
and you're looking forward, you're not
looking back. Even though you are at 60

85
00:09:01,466 --> 00:09:06,489
and its yet to come, 30 years from now,
when you are solving this piece you're

86
00:09:06,489 --> 00:09:11,515
looking forward the same principle. So,
lets see. This is a tough one to do, right

87
00:09:11,515 --> 00:09:16,815
in your head? Because as I said, if you'll
do it, there is something wrong with you.

88
00:09:16,815 --> 00:09:22,592
So, let's presume you can't do it and lets
use Excel to do it, okay. So, what I'm

89
00:09:22,592 --> 00:09:32,061
going to do is I'm going to toggle and go
to Excel, okay. So, let's see. And, I had

90
00:09:32,061 --> 00:09:37,027
problems myself in this class that I
wouldn't do a lot of execution. But I'm

91
00:09:37,027 --> 00:09:42,011
not doing execution without, hopefully, a
need for doing it. So, hopefully this is

92
00:09:42,011 --> 00:09:47,014
helpful to you without being hand holding.
So, remember, what are we trying to solve

93
00:09:47,014 --> 00:09:51,067
for? We are trying to solve for a PV
problem. Even last time, remember, if I, I

94
00:09:51,067 --> 00:09:57,015
screwed up is I thought I was solving a
PMT problem but I was actually solving a

95
00:09:57,015 --> 00:10:02,064
PV. So, the thing that you are solving for
is the function and its PV, remember in

96
00:10:02,064 --> 00:10:10,471
your head it's PV in year six. Now, what
is the interest rate? 0.08 But you got to

97
00:10:10,471 --> 00:10:15,912
pause, you do not have annual compounding,
you have monthly compounding. So, you

98
00:10:15,912 --> 00:10:24,338
divide that by twelve. How many periods do
you have? 240, don't put twenty because if

99
00:10:24,338 --> 00:10:31,109
you put an interest rate of 0.08 / twelve
and you put twenty, your annual interest

100
00:10:31,109 --> 00:10:37,823
rate is almost non-existent, which, by the
way, matches with reality these days. But

101
00:10:37,823 --> 00:10:43,239
f or the time being, let's do this
problem, okay? So you have 240 PMT, and

102
00:10:43,239 --> 00:10:49,585
then FV is what? We don't have a future
value here. And you have, have we got all

103
00:10:49,585 --> 00:10:55,993
the numbers in there? 240, yes. And the
PMT is something I do know. So, the PMT

104
00:10:55,993 --> 00:11:03,467
comes before FV, and I press 8,000, right.
So, what do you get? You get $956,434. And

105
00:11:03,467 --> 00:11:10,960
I'm going to avoid the cents right? So,
everybody recognize this. So, it's pretty

106
00:11:10,960 --> 00:11:19,703
straightforward and I'm going to leave it
there. And I'm going to toggle back to our

107
00:11:19,703 --> 00:11:26,857
presentation. So, now, we know, by the
way, this is art form. I am going between

108
00:11:26,857 --> 00:11:34,447
different media without you even knowing.
I hope you like that. Anyway, so, so PV

109
00:11:34,447 --> 00:11:43,904
60. We saw, was, and I'm going to confirm
this with my own notes, 956,434. I'm

110
00:11:43,904 --> 00:11:54,214
writing all these details simply because
I'm as I said my philosophy is that I will

111
00:11:54,214 --> 00:12:01,043
not give you resources unless they are
absolutely necessary. Do, for you to then

112
00:12:01,043 --> 00:12:06,456
sit back and consume, I want you to work
through this problem yourself so I'm

113
00:12:06,456 --> 00:12:12,017
giving you some minimal information. Now,
the problem is I cannot stay here. I got

114
00:12:12,017 --> 00:12:17,513
to match the tools I have to the problem I
can do. Now, clearly even you become

115
00:12:17,513 --> 00:12:22,692
proficient, you can do this in an Excel
and do it all quick, faster and so on.

116
00:12:22,692 --> 00:12:28,547
But, lets do it a little bit, logically. I
want to bring it back here. Why do I want

117
00:12:28,547 --> 00:12:34,220
to do that? And the reason is, I do know
how to calculate a PMT, which I am trying

118
00:12:34,220 --> 00:12:39,714
to calculate. What is my saving monthly?
If I know its future value at this point,

119
00:12:39,714 --> 00:12:45,378
but if their is a gap it's a problem,
right? So, why not take the problem to

120
00:12:45,378 --> 00:12:51,915
something you know how to do rather than
just wait there and expect some magic to

121
00:12:51,915 --> 00:12:57,570
occur? And its not a big deal it helps
your thinking. So, now what do we do?

122
00:12:57,570 --> 00:13:04,035
Let's go back. And try to understand
what's going on and do Excel again. And

123
00:13:04,035 --> 00:13:11,356
so, what I'm going to do now is I'm going
to, as I said before, toggle back to

124
00:13:11,356 --> 00:13:18,617
Excel. Now, the good news is I already
have a number up there, 956, right? So,

125
00:13:18,617 --> 00:13:24,281
let me take equals PV. Why did I do PV?
Because now, I am bringing something at

126
00:13:24,281 --> 00:13:30,419
your 60 to your 50 to m atch what I want.
So, let's do it. You have to be a little

127
00:13:30,419 --> 00:13:36,038
careful, because I think as I said, if
you're not careful you will put in the

128
00:13:36,038 --> 00:13:41,523
wrong numbers. So, now 0.08 / twelve.
Again, please remember, not eight percent

129
00:13:41,524 --> 00:13:48,798
but twelfth of that. How many periods?
Well, between 50 and 60, there are ten

130
00:13:48,798 --> 00:13:55,884
years, about 120 months, right. So, we've
got those two numbers and now, we need to

131
00:13:55,884 --> 00:14:02,223
figure out, what do I put in next? And
remember the next item here is PMT, that's

132
00:14:02,223 --> 00:14:08,104
how its been set up in Excel. But I don't
have a PMT, right? This time, I don't, I

133
00:14:08,104 --> 00:14:13,920
put a zero there, but I do have a future
value and the future value's sitting in

134
00:14:13,920 --> 00:14:19,987
which cell, we just solved the problem,
I've retained that cell A1. Look what

135
00:14:19,987 --> 00:14:26,598
happens. I think this happened last time
too. Just because you're doing it monthly

136
00:14:26,598 --> 00:14:32,672
doesn't mean this won't happen. The value
has dropped drastically in fact, to less

137
00:14:32,672 --> 00:14:38,754
than half. And what's the reason for that?
120 months have passed, and the monthly

138
00:14:38,754 --> 00:14:44,791
interest rate is non-trivial, right? So,
so, now we are at a point where this

139
00:14:44,791 --> 00:14:51,514
again, again let me just toggle back to
the PowerPoint. Now, the good news is,

140
00:14:51,514 --> 00:14:59,854
where am I? I am now at this point and I
have a number that I can deal with. I

141
00:14:59,854 --> 00:15:10,040
think its 438.96, 438.96. So, what does
this number mean? Let's just pause for a

142
00:15:10,040 --> 00:15:16,348
second. It was more than, more than twice
here, right? So, what does this number

143
00:15:16,348 --> 00:15:24,700
mean? In English, this is the amount of
money I should have in my bank at age 50

144
00:15:24,700 --> 00:15:33,065
to support what, to support all my post
retirement, 8,000 a month expenses. So,

145
00:15:33,065 --> 00:15:38,478
that's one way of thinking about it. So,
now, however, I don't have that money,

146
00:15:38,478 --> 00:15:45,070
alright. So, I need to act today, starting
end of next month, to start saving. But

147
00:15:45,070 --> 00:15:52,008
what kind of a problem is it? It's a PMT
problem. How much money do I save a month,

148
00:15:52,008 --> 00:15:58,056
whose future value I already know. Make
sense? Okay, let's do it. Just one more

149
00:15:58,056 --> 00:16:04,629
step, and then, after that, I will take a
break because I want you to, kind of think

150
00:16:04,629 --> 00:16:11,934
about this little bit and see what's going
on. So, lets go to Excel. So, now, if you

151
00:16:11,934 --> 00:16:18,515
look at the two numbers on top, the first
numbe r is, it's showing negative simply

152
00:16:18,515 --> 00:16:25,896
because I'm putting a positive number as a
payment. So now, 430 needs to be in the

153
00:16:25,896 --> 00:16:33,409
bank, at which time? At time, age 50, that
is 956, it grows into if it stays in the

154
00:16:33,409 --> 00:16:40,559
bank or in a portfolio. So, lets do the
PMT problem because now we are solving for

155
00:16:40,559 --> 00:16:47,997
PMT. How many periods do you need? First
is you need twenty periods but you also

156
00:16:47,997 --> 00:16:55,088
need a rate of return before that, 0.08 /
twelve, right? Everybody got that? Okay.

157
00:16:55,088 --> 00:17:03,265
How many PVs do I save for twenty years,
which is 240 months? Fair enough? Got it.

158
00:17:03,265 --> 00:17:10,142
Now, the max number, and again you have to
just be paying attention because your,

159
00:17:10,142 --> 00:17:15,769
what, as to what Excel offers in v
sequence. And don't try to memorize it

160
00:17:15,769 --> 00:17:20,876
because you've got junk in your head,
doesn't help. The next item is PV. We know

161
00:17:20,876 --> 00:17:26,731
we don't know the PV. We could do the PV
but we don't know it. What is the future

162
00:17:26,731 --> 00:17:31,842
value and where is it sitting? It's not
sitting in cell A1. It's sit, sitting in

163
00:17:31,842 --> 00:17:37,921
cell A2. Remember, what's sitting in cell
A1 is the future value to your 60, and you

164
00:17:37,921 --> 00:17:46,078
want it to your 50. Okay. So, let me see
what, how, what I did? You see, I can

165
00:17:46,078 --> 00:17:53,572
guess when I'm doing something not quite
right. So, you have A2, 240. You see, I

166
00:17:53,572 --> 00:17:59,848
told you, you should put zero there. And
somehow, I put a zero, and it disappeared.

167
00:17:59,848 --> 00:18:05,601
But, you know, I have intuition. And once
you start doing this number, a number will

168
00:18:05,601 --> 00:18:11,337
jump out at you and say, that doesn't make
sense. [laugh]. So, saving $3,000 a month

169
00:18:11,337 --> 00:18:16,697
just doesn't make sense, al right? Today,
to get $8,000 in the future, look how much

170
00:18:16,697 --> 00:18:22,883
are you saving. You're saving about $732.
So, just to recap everything, let me go

171
00:18:22,883 --> 00:18:32,216
back one more time and show you what
you've done. Basically, basically, what

172
00:18:32,216 --> 00:18:42,307
you've done is, sorry, just one second.
Basically, what we have done is, we have

173
00:18:42,307 --> 00:18:48,453
solved a problem where we started off
wanting 8,000 a month 240 times. We figure

174
00:18:48,453 --> 00:18:54,069
out its present value at this stage. We
figure out its present value at this

175
00:18:54,069 --> 00:18:59,983
stage. And then we said, how much do you
need to save every month to solve this

176
00:18:59,983 --> 00:19:05,646
problem? Look, I'm not going to enter the
items here. We already did this. What I'm

177
00:19:05,646 --> 00:19:10,884
going to enter is, what did we get over
here? We got 732. And I wanted you to

178
00:19:10,884 --> 00:19:19,159
think a little bit about it. Look at the
power of time and compounding. I have to

179
00:19:19,159 --> 00:19:28,955
save only $700 a month plus 7.30, right?
But in return for that, I'm able to enjoy

180
00:19:28,955 --> 00:19:36,004
$8,000 a month. The thing that has changed
is time between the two. Why is it that I

181
00:19:36,004 --> 00:19:43,287
can save so little and enjoy so much in
the future? One, because time has passed.

182
00:19:43,287 --> 00:19:49,983
Two, because of the interest rate. I
repeat again, an interest rate of eight

183
00:19:49,983 --> 00:19:55,928
percent a year, which is eight percent
divided by12 a month is in a very high

184
00:19:55,928 --> 00:20:01,858
number, first point. Second point, yes,
you are working hard, hopefully, to get

185
00:20:01,858 --> 00:20:07,970
this saving possible of 700 plus. But
remember who's giving you the benefit. The

186
00:20:07,970 --> 00:20:13,773
world is investing your money in good
ideas, hopefully. And we'll see what good

187
00:20:13,773 --> 00:20:20,327
ideas are later today. That's the topic of
today's class. You are able to enjoy what

188
00:20:20,327 --> 00:20:26,317
the world has to offer, through the
ability to invest in other things. So,

189
00:20:26,317 --> 00:20:32,638
that other thing we called was a
portfolio. Remember, that portfolio grows

190
00:20:32,638 --> 00:20:39,296
over time, simply because of two things.
More months, more the growth. Higher the

191
00:20:39,296 --> 00:20:45,760
interest rate, higher the growth. The two
together Is the effect, profound effect of

192
00:20:45,760 --> 00:20:51,391
compounding, which even Einstein said he
couldn't fathom and turns out that's why

193
00:20:51,391 --> 00:20:57,053
you need Excel and other things to
calculate these numbers. So, I hope this

194
00:20:57,053 --> 00:21:04,006
put everything together one more time and
the small twist of going from a year to a

195
00:21:04,006 --> 00:21:10,384
month, helped. Do one last thing, answer
the following question. If compounding is

196
00:21:10,384 --> 00:21:16,541
monthly and the stated interest rate is
eight percent annually, what really is the

197
00:21:16,541 --> 00:21:23,370
annual interest rate? I can give you the
directional answer. It has to be more

198
00:21:23,370 --> 00:21:29,689
because of pause compounding and I would
take your time to solve that problem and

199
00:21:29,689 --> 00:21:35,015
you're not going to do it, you have a
formula, you have information, do it for

200
00:21:35,015 --> 00:21:41,053
yourself and it will help you understand.
The only difference is you're going from

201
00:21:41,053 --> 00:21:47,096
month to a year instead of a year to many
years. Again, just messing around with the

202
00:21:47,096 --> 00:21:52,866
timeline. So, take a break. W e are going
to start a new topic today, which by the

203
00:21:52,866 --> 00:21:59,263
way is going to take the same ideas, but
is important decision-making session. And

204
00:21:59,263 --> 00:22:05,924
in this week we'll recognize how to
evaluate good ideas and bad ideas and pick

205
00:22:05,924 --> 00:22:09,076
hopefully the good ones. See you soon.
