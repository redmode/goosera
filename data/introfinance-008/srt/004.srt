1
00:00:00,000 --> 00:00:03,128
Hi, welcome back.

2
00:00:03,128 --> 00:00:07,763
Today is the first day, or, first week,
actually, of the class. And,

3
00:00:07,763 --> 00:00:13,156
I talked about the various aspects of
the class that previous session.

4
00:00:13,156 --> 00:00:19,333
But, just to give you a sense of my style, I'm
hyper,

5
00:00:19,333 --> 00:00:23,836
I'm nuts, I love this stuff. So,

6
00:00:23,836 --> 00:00:29,935
I will tend to be myself with you as if, you know, you're in the room with me and I hope

7
00:00:29,935 --> 00:00:33,876
that's actually good for you, your
learning rather than interfering. And I

8
00:00:33,876 --> 00:00:40,286
talk about life and so on as we go along,
and the good news is you can always skip

9
00:00:40,286 --> 00:00:45,258
parts you don't enjoy. Whereas if you were
in the class you'd have to suffer me

10
00:00:45,258 --> 00:00:50,799
throughout the class. So what I'm going to
do today is I'm going to start off with

11
00:00:50,814 --> 00:00:55,546
the content. And this is what I do in
class too. The first week, if it's left

12
00:00:55,546 --> 00:01:00,446
just introductory, I don't think it's
right. If we have time. We have a lot to

13
00:01:00,446 --> 00:01:06,875
cover. And I will stop the topic. One
important aspect I want you to recognize,

14
00:01:06,875 --> 00:01:13,717
is that typically, in real time, when
you're teaching classes like this, the

15
00:01:13,717 --> 00:01:19,869
amount of exposure the students have to a
teacher, or the classroom time, if you

16
00:01:19,869 --> 00:01:25,278
may, is a lot more than on video and video
is very intense, there's very few

17
00:01:25,278 --> 00:01:30,743
interruptions. Those are the downsides I
think. But on the other hand as I said you

18
00:01:30,743 --> 00:01:35,942
can keep learning from others and you can
keep going back to the video. I want to

19
00:01:35,942 --> 00:01:41,400
emphasize this every time, is that the
good news is you have the video and I want

20
00:01:41,400 --> 00:01:46,383
the video to be self-contained. I will not
give you too many notes.

21
00:01:46,383 --> 00:01:51,612
In fact, I encourage you if you really wanted to
learn, and I've thought about this a lot.

22
00:01:51,612 --> 00:01:56,486
Encourage you to make your own notes as we
are talking. You know? I think note

23
00:01:56,486 --> 00:02:01,573
taking, thinking through, however you want
to do, scribble on a tablet, use a pen or

24
00:02:01,573 --> 00:02:06,474
a paper. I'll encourage you to do that,
because then you've created your own set

25
00:02:06,474 --> 00:02:10,478
of notes. I was inclined to give you a lot
of material. But there are a lot of

26
00:02:10,478 --> 00:02:15,674
textbooks available, and I'll give, they
are in the syllabus, I've mentioned a few

27
00:02:15,674 --> 00:02:20,908
there are others. I want you to
choose your resource, and me, to provide

28
00:02:20,908 --> 00:02:26,625
you enough to be able to be successful in
learning this class. So we'll talk, start

29
00:02:26,625 --> 00:02:33,035
talking about the time value of money
today and we'll continue this in the next

30
00:02:33,035 --> 00:02:39,974
week. And the reason is, time value of
money is central to understanding finance.

31
00:02:41,250 --> 00:02:47,154
So let's get started. I want to
pitch the beauty of finance. If you

32
00:02:47,154 --> 00:02:55,198
remember, as its ability, the structure,
and the details and the tools. The ability

33
00:02:55,598 --> 00:03:01,434
of finance to solve or, I shouldn't say
solve, but to understand decision making

34
00:03:01,434 --> 00:03:06,508
and make good decisions. So we never sure
about the decision going to be right or

35
00:03:06,508 --> 00:03:12,542
wrong, that's life, but we want to develop
a framework where the chances of you

36
00:03:12,542 --> 00:03:18,113
making good decisions given the
information you have, goes up. So I hope

37
00:03:18,113 --> 00:03:23,766
you recognize that. So what is really
critical to decision making?

38
00:03:24,489 --> 00:03:30,545
Every decision involves time and uncertainty. I
have emphasized this in the introduction

39
00:03:30,545 --> 00:03:36,760
of the class, and I will keep emphasizing
it. If you understand time, and you

40
00:03:36,760 --> 00:03:45,309
understand uncertainty at a gut level, in
a framework context, in a daily life

41
00:03:45,386 --> 00:03:49,101
context, I think you will have arrived.
And it'll, life will be so cool. You know,

42
00:03:49,101 --> 00:03:55,163
you'll be able to make decisions not using
Excel, but based on your gut, as if you've

43
00:03:55,163 --> 00:04:00,453
also used Excel. Because your framework is
so strong. So time and uncertainty, these

44
00:04:00,453 --> 00:04:05,027
are, these are common to every decision.
Please recognize that. They're common to

45
00:04:05,027 --> 00:04:10,152
every decision. Whether you want to cross
the street or whether you want to create a

46
00:04:10,152 --> 00:04:15,832
new technology to solve poverty, which I
hope you do. Okay, very important to

47
00:04:15,832 --> 00:04:21,627
understand just the impact of time on a
decision and that's what we call time

48
00:04:21,627 --> 00:04:27,156
value of money, typically. We ignore
uncertainty for some time and I'm going to

49
00:04:27,156 --> 00:04:33,116
do that in a very deliberate way. I will
talk about it, because life is full of

50
00:04:33,116 --> 00:04:38,576
uncertainty, but I'm going to try to
ignore bringing in uncertainty, simply

51
00:04:38,576 --> 00:04:43,473
because if we learn in a linear way,
building blocks will help us get there,

52
00:04:43,473 --> 00:04:48,121
rather than throw everything at you and
it's going to be difficult. So remember,

53
00:04:48,121 --> 00:04:52,483
we are going to spend a lot of time
understanding the impact of time with

54
00:04:52,483 --> 00:04:59,015
uncertainty, somewhere in the back of our
minds but not explicitly accounted for.

55
00:04:59,015 --> 00:05:06,020
And just to read, emphasize this issue, I
am putting it as a bullet point, and so

56
00:05:06,020 --> 00:05:11,963
that you can make notes on this. We got to
internalise the time value of money. And

57
00:05:11,963 --> 00:05:17,342
so what I am going to do now is give you a
sense of what some terminology is

58
00:05:17,342 --> 00:05:22,402
available to us which actually turns out
to be, except for the fact that it's

59
00:05:22,402 --> 00:05:27,526
English that I am talking, makes a lot of
sense. And I am going to spend a lot of

60
00:05:27,526 --> 00:05:32,556
time on each one of them before jumping
into actually talking about Finance. So

61
00:05:32,556 --> 00:05:37,585
you can think of it as the language of
finance that we need. And I am not going

62
00:05:37,585 --> 00:05:42,243
to throw too much language at you. I'll
throw it, throw enough so that you

63
00:05:42,243 --> 00:05:49,461
understand what''s going on at that point
in time. So the first thing that we will

64
00:05:49,461 --> 00:05:56,655
try to talk a lot about is present value
and something that's closely related to

65
00:05:56,655 --> 00:06:01,565
it, future value. I would like you to,
I'll soon start drawing timelines, which

66
00:06:01,565 --> 00:06:06,651
are very important. So basically, one of
the things that you need to know is how to

67
00:06:06,651 --> 00:06:09,541
draw a timeline. And if you know how to
draw a timeline, half the problem is

68
00:06:09,756 --> 00:06:15,449
solved. Because the world, it's problems
are awesome, because nobody knows how to

69
00:06:15,449 --> 00:06:20,660
approach them. World, the life in general
is quite complicated. What finance does is

70
00:06:20,660 --> 00:06:25,540
makes it simple. I mean that's what you
want from a frame book, not to make like

71
00:06:25,540 --> 00:06:30,941
more complicated, it's complicated enough.
So look at what present value means.

72
00:06:30,941 --> 00:06:36,462
It means the value of anything today, present
as of now. What does future value mean?

73
00:06:36,462 --> 00:06:41,590
The value in the future, and what time in
the future, we'll start off with very

74
00:06:41,590 --> 00:06:46,971
simple examples and make it infinite
future, right? So, so, so you don't need

75
00:06:46,971 --> 00:06:53,285
to worry about present versus future.
We'll structure it within a problem that

76
00:06:53,285 --> 00:06:59,640
you have. But the, the most important
aspect of this is look at the units in

77
00:06:59,640 --> 00:07:05,774
which both are measured. The units in
which both are measured are dollars or yen

78
00:07:05,774 --> 00:07:11,339
or rupee as I said wherever you are the
currency from your country. And you a re

79
00:07:11,339 --> 00:07:17,261
from all over the world taking this class
which, that alone excites me, you know.

80
00:07:17,261 --> 00:07:23,130
And so, hopefully I can reach you and help
you in learning this stuff. So whether

81
00:07:23,130 --> 00:07:28,153
it's a dollar or a yen or whatever. Both
are in dollars. And the, the, again,

82
00:07:28,153 --> 00:07:33,459
dollar is not the important thing, but the
thing is that we have a measurement of

83
00:07:33,459 --> 00:07:38,316
value. And a lot of things in life,
actually can't be measure, love, for

84
00:07:38,316 --> 00:07:44,033
example, is something that you can't put a
dollar sign. That's why it's so awesome.

85
00:07:44,033 --> 00:07:49,315
But for matters in this class, the dollar
sign is just a language of reflecting

86
00:07:49,315 --> 00:07:54,721
value. So the unit is very important
because I'm going to now go to the next

87
00:07:54,721 --> 00:08:01,170
thing which is n and many times it could
be called t, T as in Tom, symbol. It's for

88
00:08:01,170 --> 00:08:07,184
the number of periods that you're thinking
about. So, for example, if you're thinking

89
00:08:07,184 --> 00:08:12,850
only about today versus next year, then
the n is one. But notice, again, why is

90
00:08:12,850 --> 00:08:18,901
this important? Because you recognize, in
a second, that the passage of time alone

91
00:08:18,901 --> 00:08:24,026
makes decision making both interesting and
challenging. And that's why I love

92
00:08:24,026 --> 00:08:29,071
finance, is that, think about it. Just
because of the passage of time, you need

93
00:08:29,071 --> 00:08:35,032
to worry about so many things but isn't it
cool that time alone can make such a huge

94
00:08:35,032 --> 00:08:39,756
difference. So the number of periods, now,
it's critical for you to understand

95
00:08:39,756 --> 00:08:45,090
something which I'll repeat. Okay, the
problem will lend itself to the definition

96
00:08:45,090 --> 00:08:50,342
of the period. So, just remember that, the
period doesn't have to be a year, it can

97
00:08:50,342 --> 00:08:55,364
be one day depending on the nature of the
problem. Finally, the most critical

98
00:08:55,364 --> 00:09:02,804
aspects of finance is, if I were to
capture one element of finance that

99
00:09:02,804 --> 00:09:09,819
distinguishes itself from everything else,
it is the next symbol, r. And I'm using it

100
00:09:09,819 --> 00:09:14,525
in lower case, r here. Sometimes textbooks
use caps and so on, for n and so on. Don't

101
00:09:14,525 --> 00:09:20,269
worry about that. The critical element
here is this. The first aspect of an

102
00:09:20,269 --> 00:09:26,851
interest rate is, it's not in dollars, it
is in percentage terms. So it's like a

103
00:09:26,851 --> 00:09:32,552
change over time. This is very, very
important to understand. And I think,

104
00:09:32,568 --> 00:09:38,047
intuitively, you do. But practically,
we'll also try to see what's going on with

105
00:09:38,047 --> 00:09:44,049
it. The other element which is very, very
important for this class and in a way,

106
00:09:44,049 --> 00:09:50,081
this is the one aspect of the class that I
wish I could spend weeks on, but

107
00:09:50,081 --> 00:09:56,091
we don't have the time. We'll assume the
interest rate is positive and I'll say

108
00:09:56,091 --> 00:10:02,346
this is an assumption. And, let me throw
an idea to you. Where does, where do

109
00:10:02,346 --> 00:10:07,372
interest rates come from? Why are they
typically positive? Can they be negative?

110
00:10:07,372 --> 00:10:14,005
It's a fascinating topic. And I would
recommend a book. Called Theory Of

111
00:10:14,005 --> 00:10:21,005
Interest written in 1930 by Irving Fisher,
who, if you read the book, it pretty much

112
00:10:21,005 --> 00:10:27,167
blows your mind. This guy, almost a
century ago, has predicted all modern

113
00:10:27,167 --> 00:10:33,280
finance and he writes in such an awesome
way. No technical stuff, just some graphs.

114
00:10:33,357 --> 00:10:37,939
It's just awesome stuff to read. But the
critical aspect of that book is that it

115
00:10:37,939 --> 00:10:41,968
will take you off on a journey will take
you off on a journey that won't be the

116
00:10:41,968 --> 00:10:47,292
journey we'll take. At least aspects of
it. And that is we'll assume the interest

117
00:10:47,292 --> 00:10:51,713
rate is positive, because that's what we
usually see, but I want you to recognize

118
00:10:51,713 --> 00:10:57,192
that assumption right up front. And again,
repeat, again. I know you're clamoring for

119
00:10:57,192 --> 00:11:03,243
uncertainty. I love uncertainty. I think
that's what makes life interesting. But

120
00:11:03,243 --> 00:11:08,815
for the time being, we're going to stick
with no uncertainty. Of course, that's

121
00:11:08,988 --> 00:11:13,934
completely unrealistic, but it's simple so
that our building blocks of learning and

122
00:11:13,934 --> 00:11:20,441
time that it takes to learn, happens the
natural way. So I've introduced the

123
00:11:20,441 --> 00:11:24,579
terminology. I want you to stare at these
things for a little while. Please

124
00:11:24,579 --> 00:11:30,389
remember, I'll introduce more. And I want
you to get comfortable with these. So, for

125
00:11:30,389 --> 00:11:35,076
example, if you want to take a break now
and you want to go google. That's the

126
00:11:35,076 --> 00:11:40,975
beauty of Google. In fact when my son was
little, even now he asks me questions and

127
00:11:40,975 --> 00:11:46,192
I usually don't have a clue of the answer.
So, for example, "Why is the sky blue?" So

128
00:11:46,192 --> 00:11:51,566
I would say to Gabe, Gabriel, "Gabriel,
why don't you Google?" So I think by about

129
00:11:51,566 --> 00:11:56,945
five he thought Google was this really
cool person who knew everything and my

130
00:11:56,945 --> 00:12:01,172
dad is a loser. So, so, just, just so that
and I just wanted to talk about this,

131
00:12:01,172 --> 00:12:06,416
because this is cool stuff, right? So just
go Google, try to read your book, you've

132
00:12:06,416 --> 00:12:12,025
got to get familiar with these concepts, a
little bit, if you want to take a break.
