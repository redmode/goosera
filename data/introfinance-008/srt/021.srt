1
00:00:00,000 --> 00:00:05,098
Welcome back. Hopefully you've looked at
this problem, thought about it. I want to

2
00:00:05,098 --> 00:00:11,038
just emphasize one more time the
importance of timelines. So what I've done

3
00:00:11,038 --> 00:00:17,014
is actually, I've drawn one ahead of time
and I'm going to go to that now. So I'm

4
00:00:17,014 --> 00:00:22,041
starting saving at year 30 and for
convenience, to be consistent with the

5
00:00:22,041 --> 00:00:28,017
formula, we are going to assume that the
saving starts at the end of the year. So

6
00:00:28,017 --> 00:00:33,075
I'm starting at 30, I'll start saving at
31 and I'll quit saving at 50. And we

7
00:00:33,075 --> 00:00:39,476
never say 21 or 30 by definition means the
end of the year, right? That's the way we

8
00:00:39,476 --> 00:00:45,216
talk. I want to retire at 60. So, what's
going to happen between 50 and 60? Well,

9
00:00:45,216 --> 00:00:49,937
we look at the graph, nothing. So what I
am going to do is I am going to now

10
00:00:49,937 --> 00:00:58,106
annotate, and start writing on this. So,
this is the time period when I'm saving

11
00:00:58,106 --> 00:01:07,507
here nothing. In other words, by magic I'm
approximately even. I'm not saving and I'm

12
00:01:07,507 --> 00:01:13,954
not dissaving. I know you might not have
heard of that word, but I have. You are

13
00:01:13,954 --> 00:01:18,956
basically not spending more than you're
earning every year. It's a matter of

14
00:01:18,956 --> 00:01:29,609
convenience, right? And now in this period
what am I doing? I am consuming but not

15
00:01:29,609 --> 00:01:35,560
burning, alright? Again, that's in a
simplification but will give you a sense,

16
00:01:35,560 --> 00:01:41,102
I think this word problem is just awesome.
You can modify it in various ways. Now the

17
00:01:41,102 --> 00:01:46,563
question is dying with it. Of course you
don't want to die. I haven't met anybody

18
00:01:46,563 --> 00:01:52,141
who's said, you know I'm really looking
forward to dying but it's one of the most

19
00:01:52,141 --> 00:01:57,721
predictable things based on statistics are
expected. So let's assume 80 and let's

20
00:01:57,721 --> 00:02:02,720
assume for convenience that, even in the
last year, end of the last year, I am

21
00:02:02,720 --> 00:02:07,735
going to spend some money. Me, on my
behalf, or whatever or given to somebody

22
00:02:07,735 --> 00:02:13,235
else, or whatever. It can change the
problem in many ways so let's see. What is

23
00:02:13,235 --> 00:02:22,591
the question asked me? Is the question
asking me how much am I saving per year?

24
00:02:22,591 --> 00:02:31,869
Is it a PMT question? Balances? Yes but
the problem is the question I'm trying to

25
00:02:31,869 --> 00:02:37,680
answer, and this is why finance is
awesome, and you need to time travel. The

26
00:02:37,680 --> 00:02:43,007
answer to it, if you just say PM T to the
calculator or Excel and wait, the Excel is

27
00:02:43,007 --> 00:02:48,430
going to look at you and smack you around,
and rightly so. Saying you expect me to

28
00:02:48,430 --> 00:02:53,843
answer a question even I don't know the
answer to, right? You Google PMT, the

29
00:02:53,843 --> 00:02:59,054
Google is going to laugh back at you. So,
the question is you got to know something

30
00:02:59,054 --> 00:03:07,066
in the future so yes it turns out we do.
We know from 61. To 80. What do I want? I

31
00:03:07,066 --> 00:03:13,540
want PMT of how much every year? That's
what I said, we have everything set up, we

32
00:03:13,540 --> 00:03:20,058
want a payment of what? We want, if you go
back to the problem, let's go back to the

33
00:03:20,058 --> 00:03:26,067
problem for a second so that we are all on
the same page, right? I don't want to

34
00:03:26,067 --> 00:03:32,030
confuse numbers. I want to consume
$100,000 every year. So once I know that,

35
00:03:33,034 --> 00:03:42,003
I know that starting in I know that
starting year 61 I need 100,000 and how

36
00:03:42,003 --> 00:03:50,061
many of these? So this is a very simple
problem to do. It's called a PMT present

37
00:03:50,061 --> 00:03:57,044
value problem. So I do the PV at this
point. Lets do it very quickly because if

38
00:03:57,044 --> 00:04:05,679
I know the PV at this point, I will note
the PV at this point. If I know the PV at

39
00:04:05,679 --> 00:04:13,822
this point, I know the future value of my
savings. So the thing I'm trying to solve

40
00:04:13,822 --> 00:04:23,265
for is, twenty PMTs here which is money
I'm going to invest. And the answer is

41
00:04:23,265 --> 00:04:29,418
hidden in twenty PMTs in the future. The
only difference between these two is the

42
00:04:29,418 --> 00:04:36,277
difference in time and obviously, in one
case I'm giving away money to the bank or

43
00:04:36,277 --> 00:04:42,055
a fund or retirement account in the other
case I'm withdrawing, right? Okay?

44
00:04:42,055 --> 00:04:48,083
Everybody, so let's do. If I can somehow
figure this out, I'll be much better off

45
00:04:48,083 --> 00:04:55,010
than I am right now. So, so that, let's,
let's try to get to it. Okay, Excel so

46
00:04:55,010 --> 00:05:00,070
this is not a PMT problem. I know the PMT.
So what am I going to do? Please

47
00:05:00,070 --> 00:05:06,067
recognize, you've time traveled to year
60. Why have I time traveled to year 60?

48
00:05:06,067 --> 00:05:12,057
Because I know how to figure out the
present value of something starting one

49
00:05:12,057 --> 00:05:18,039
year from now. And at 60, I know at 61
I'll need 100,000. Okay, so the interest

50
00:05:18,039 --> 00:05:24,060
rate and we are doing this annual so we
don't want to do the monthly thing is .08

51
00:05:24,060 --> 00:05:34,040
I believe, right? And how many periods?
After twenty periods a nd what is PMT?

52
00:05:34,040 --> 00:05:43,077
Turns out, it's the exactly the same
number from the previous problem. So

53
00:05:43,077 --> 00:05:52,405
124,000. And there must be a number here.
Let me just try to make sure we got it. I

54
00:05:52,405 --> 00:06:01,335
could tell, there was a mistake that, then
I did it quickly I pressed .8 and .8 is 80

55
00:06:01,335 --> 00:06:08,781
percent interest rate, you don't want
that, alright? Okay, this makes a lot more

56
00:06:08,781 --> 00:06:19,504
sense. So I have 981,814.74. I'm going to
go back now. I'm going to go back now and

57
00:06:19,504 --> 00:06:33,002
say that what I need is 980 and to confirm
the number 981. 815. Write it one more

58
00:06:33,002 --> 00:06:44,170
time, so that it's legible. At what point?
This is the PV at point 60. Another way in

59
00:06:44,170 --> 00:06:51,634
English to say this is the following.
Remember we did the loan problem and we

60
00:06:51,634 --> 00:06:58,098
talked about repaying the loan, its the
same problem. Except slightly different. I

61
00:06:58,098 --> 00:07:04,224
need to have $981,000 but 982 if you may,
in the bank in which year? In year 60 to

62
00:07:04,224 --> 00:07:10,574
be able to finance what? $100,000 worth of
consumption every year withdrawal and at

63
00:07:10,574 --> 00:07:16,358
the end of the year 80 how much will I be
left with? Zero. Exactly the same problem,

64
00:07:16,358 --> 00:07:21,777
right? So that's what I want you to
understand. In the end, there is only one

65
00:07:21,777 --> 00:07:26,834
problem and If you know how to do it, you
can apply that thinking to anything.

66
00:07:26,834 --> 00:07:33,752
However, I have a problem here. There is a
gap of ten years that I don't like, why?

67
00:07:33,752 --> 00:07:40,183
Because I know how to use annuities but I
do not know how to use annuities that end

68
00:07:40,183 --> 00:07:45,443
in year 50 but actually the evaluation I
know is in year 60. So what do I need to

69
00:07:45,443 --> 00:07:51,051
do? Step two, and by the way you can do
this many ways. I'm taking the easy way

70
00:07:51,051 --> 00:07:57,303
out. So what do I have to do? Remember in
finance time value of money everything and

71
00:07:57,303 --> 00:08:02,551
the interest rate is what? Eight percent
so now what do I have to do? I have to

72
00:08:02,551 --> 00:08:08,121
bring it back PV in the year 50. What will
that do? That will become the future value

73
00:08:08,121 --> 00:08:14,903
of the saving annuity I'm trying to solve.
So let's do that and that'll help us. As I

74
00:08:14,903 --> 00:08:22,635
said this is a very, very cool problem and
I'm going to walk you through it simply

75
00:08:22,635 --> 00:08:31,308
because. I'm going to make another set.
Notice I've left, sorry. Okay. Notice I

76
00:08:31,308 --> 00:08:39,510
have left the first cell still open and I
am going to write zero, I 'm sorry. I mean

77
00:08:39,510 --> 00:08:44,791
I'm going to write equal and now I am
going to do a PV function, right. We will

78
00:08:44,791 --> 00:08:50,799
see in a second why I have left the cell
in its own place, because I can right .08,

79
00:08:50,799 --> 00:08:56,275
the interest rate is .08. Remember I
screwed up last time. I put .8 don't do

80
00:08:56,275 --> 00:09:02,367
that. Number of periods, ten. Why? Because
I am bringing a value I know in your 60

81
00:09:02,367 --> 00:09:08,594
back to your 50, right. And there is no
PMT. Remember this is a one short thing.

82
00:09:08,594 --> 00:09:14,575
So we are combining our learning's from
the past week, there is no PMT. Its a one

83
00:09:14,575 --> 00:09:20,478
short bank account that will finance a
future PMT. Right, so zero, don't forget

84
00:09:20,478 --> 00:09:28,778
that and then I know the future value of
this. Where is it residing? In cell A1. So

85
00:09:28,778 --> 00:09:40,320
what is the value of 981 year 60, in the
year 50? Well, the value is about

86
00:09:40,320 --> 00:09:51,416
$454,770. So I'm going to, I apologize
there's a little bit of going this. Let me

87
00:09:51,416 --> 00:10:00,767
just write out the numbers. So it's about
981,815 and now brought it back to about

88
00:10:00,767 --> 00:10:08,646
454. And to get the numbers right 7710.
I'm just confirming that the numbers are

89
00:10:08,646 --> 00:10:15,483
right, okay? So, so see what has happened,
the magnitude it has almost became half,

90
00:10:15,483 --> 00:10:22,698
right. Actually, less than half and the
reason is I'm discounting heavily at

91
00:10:22,698 --> 00:10:30,385
eight%. Now, I'm almost done. Why? Because
this PV is in year 50. This PV is in year

92
00:10:30,385 --> 00:10:39,241
60. But I know, if I know this PV in the
year 50, it can become the future value of

93
00:10:39,241 --> 00:10:47,036
this problem. So I know the value that I'm
saving towards. It's about $455,000.

94
00:10:47,036 --> 00:10:53,919
454,770 but now I need to ask the
following question. How much every year

95
00:10:53,919 --> 00:11:03,323
will I put in the bank so I that I'm sure
454,770 is left? Everybody got that?

96
00:11:03,323 --> 00:11:12,528
Right, ? One more step. Okay, so now
notice I'll keep that 454 where, exactly

97
00:11:12,528 --> 00:11:19,538
where it is. One it will give me sense of
what I got the right numbers but now I

98
00:11:19,538 --> 00:11:26,591
want to do what? Remember, I am trying to
save every year in amount that becomes 454

99
00:11:26,591 --> 00:11:33,175
after twenty years so I'm going to do a
PMT problem. Why? Because don't know the

100
00:11:33,175 --> 00:11:39,674
amount, okay. What is the interest rate?
.08. How many times am I saving? N period

101
00:11:39,674 --> 00:11:45,993
is twenty and what is the present value of
it? I don't know the present value because

102
00:11:45,993 --> 00:11:51,954
the presen t value would be in year 30,
right, so what do I know luckily. I know

103
00:11:51,954 --> 00:12:00,688
the future value and where is it sitting?
In cell A2, right. Bingo. 46319. And let

104
00:12:00,688 --> 00:12:10,348
me just confirm that this is okay, and
turns out, this seems to be too much. So

105
00:12:10,348 --> 00:12:23,377
what did I do wrong here? I, A1 A2, BMT.
You see what I did wrong? And, I wasn't

106
00:12:23,377 --> 00:12:31,952
paying attention, I pressed A2 as PV. It's
good to make mistakes, which I know you

107
00:12:31,952 --> 00:12:38,148
will. But it's a minor mistake in the
sense that I'm making a calculation error,

108
00:12:38,148 --> 00:12:45,037
but I could guess my answer was wrong
because it was too high so I have 9937. So

109
00:12:45,037 --> 00:12:50,251
the answer now is right. Remember, if you
look up and you do the PMT function, the

110
00:12:50,251 --> 00:12:55,214
first number you press is interest rate,
second is number of period to twenty,

111
00:12:55,219 --> 00:13:00,563
third is PV. And I said I didn't have any
PV but I didn't put zero and final is FV.

112
00:13:00,563 --> 00:13:06,400
So if I knew the PV, I would replace zero
by that and make A2, zero. Is that, so

113
00:13:06,400 --> 00:13:14,075
okay so we are cool now, and we know, and
I'll confirm that we need 9938. And we're

114
00:13:14,075 --> 00:13:21,098
almost done. So here comes the final. Hey,
I am sure you're relieved, by now but how

115
00:13:21,098 --> 00:13:30,010
much would I save, every year for the next
twenty years. Is 9938, look a the

116
00:13:30,010 --> 00:13:37,084
awesomeness of this. Look at the
awesomeness. In this case the awesomeness

117
00:13:37,084 --> 00:13:43,840
of finance is helping you. Its actually,
really not because it is time value of

118
00:13:43,840 --> 00:13:50,509
money but I am saving only $10,000 how
many times? Twenty times but I am going to

119
00:13:50,509 --> 00:13:57,453
use that money to consume how much? Twenty
times. $100,000, twenty times. How the

120
00:13:57,453 --> 00:14:04,689
heck is this possible? It's possible
because the time value of money is eight%.

121
00:14:04,690 --> 00:14:13,473
If the time value of money was zero. The
world ain't going to make me do this

122
00:14:13,473 --> 00:14:20,549
easily, right? So I say 9938 I get 9938
back, one for one. If the interest rate is

123
00:14:20,549 --> 00:14:25,215
zero. I hope you like today because I
loved talking to you today. I'm not

124
00:14:25,215 --> 00:14:29,541
kidding you which by the way kidding in
the US means making fun of you or joking

125
00:14:29,541 --> 00:14:34,082
with you or, you know because I've
traveled and I've lived in different

126
00:14:34,082 --> 00:14:39,051
countries and different words mean
different things but I've tried to tell

127
00:14:39,051 --> 00:14:43,534
you the essence and the beauty of finance
which has made it a little bit ch

128
00:14:43,534 --> 00:14:48,645
allenging for you, I am sure but I think
that's the value of this class if anything

129
00:14:48,645 --> 00:14:55,025
I have to bring to the table. Just a quick
minute. I think I feel, I wanted you to

130
00:14:55,025 --> 00:15:01,069
know, that I feel very fortunate that I'm
able to teach you anything at all and many

131
00:15:01,069 --> 00:15:07,056
times I want to be like Tuvok. I, I hope
you watch Star, Star Trek and so on, and

132
00:15:07,056 --> 00:15:13,065
do the mind meld and do this very quickly.
But that's not feasible, right? And it's

133
00:15:13,065 --> 00:15:19,082
also probably not the right thing to do.
Because if you try to gain knowledge very

134
00:15:19,082 --> 00:15:25,045
quickly like that, it tends not to stick.
So I've tried to make the problems

135
00:15:25,045 --> 00:15:31,039
available. I've tried to go slow with you
and therefore the videos will be long.

136
00:15:31,039 --> 00:15:37,061
When will the videos be short? When the
content is very crisp, but maybe you need

137
00:15:37,061 --> 00:15:44,021
to go to a textbook to reinforce or maybe
you need to go back later to accounting to

138
00:15:44,021 --> 00:15:50,035
reinforce statistics. See you next time.
It was fun talking to you this week and I

139
00:15:50,035 --> 00:15:56,033
hope you stay in touch and do the
problems, in touch with the material and

140
00:15:56,033 --> 00:16:00,098
do the assignment. See you next week.
Thank you very much, bye.
