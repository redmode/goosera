1
00:00:00,000 --> 00:00:06,003
Hi. So hopefully you've taken some time
off. I'm going to shift to present value

2
00:00:06,003 --> 00:00:11,812
but I, as I promised earlier, I'm going to
stop with present value for very similar

3
00:00:11,812 --> 00:00:17,779
problems that we did earlier. And the
reason I'm going to stop today is I think

4
00:00:17,779 --> 00:00:24,011
the first week should expose you to the
class, what it's all about, and some idea

5
00:00:24,011 --> 00:00:30,406
of time, value and money. But, we'll get
things more complicated next session. Of

6
00:00:30,406 --> 00:00:38,185
course, I also want to remind you that we
are trying to follow very strictly a week

7
00:00:38,185 --> 00:00:45,553
by week schedule, so I will let the video
length be dictated by the week. So,

8
00:00:45,553 --> 00:00:52,892
because today was introductory stuff, and
we did videoing of the content starting

9
00:00:52,892 --> 00:00:59,342
about. An hour ago. What I'm going to do
today is leave you with enough material of

10
00:00:59,342 --> 00:01:04,954
the content. You can always go watch the
introduction to the class again anytime.

11
00:01:04,954 --> 00:01:10,814
But I wanted to give you enough today this
week so that you can start, you know,

12
00:01:10,814 --> 00:01:17,163
practicing, start doing assignments and so
on. So now the next concept which probably

13
00:01:17,163 --> 00:01:22,694
is more talked about as present value
than, than, than as future value. So again

14
00:01:22,694 --> 00:01:28,260
let me start with a problem. So the
question says what is the present value of

15
00:01:28,260 --> 00:01:34,331
receiving 110 one year from now. And I
think some of you or all of you are

16
00:01:34,331 --> 00:01:40,764
smiling because you know why I am asking
you this question with these specific

17
00:01:40,764 --> 00:01:45,908
numbers. The interest rate is ten percent
and I'm giving you $110 one year from now.

18
00:01:45,908 --> 00:01:51,389
So, think about this problem as something
like this. You're going to receive $110 in

19
00:01:51,389 --> 00:01:56,558
the future one year from now, and you're
trying to figure out, what does it mean to

20
00:01:56,558 --> 00:02:01,349
you today? And this is a very important
problem to solve, right? Because most

21
00:02:01,349 --> 00:02:05,923
problems in life, you make effort today,
and you get money or pleasure in the

22
00:02:05,923 --> 00:02:10,298
future, right? So you wanna figure out,
what's the value of that today. And

23
00:02:10,298 --> 00:02:16,056
present value, therefore, is a little bit.
More important than future value for

24
00:02:16,056 --> 00:02:22,072
decision making. However, however, future
value, I think, is easier to understand,

25
00:02:22,072 --> 00:02:28,169
and it forces you to be a finance person.
Finance people look forward. Finance

26
00:02:28,169 --> 00:02:32,880
people don't lo okay back. Right? So that
hopefully that's engrained in you. I'll

27
00:02:32,880 --> 00:02:37,273
tell you a little bit about what that has
done to me [laugh], you know, because

28
00:02:37,273 --> 00:02:41,946
finance has changed me in good ways,
mostly. But there are some elements of it

29
00:02:41,946 --> 00:02:46,465
which are pretty hilarious which I'll get
to in a second. Okay so lets do this

30
00:02:46,465 --> 00:02:54,149
problem and I am going to use again, just
a simple way of doing it. And so, here you

31
00:02:54,149 --> 00:03:01,488
go. You have zero, you have one. One
period has passed. Remember always what

32
00:03:01,488 --> 00:03:10,175
connects one and two is time value, money
at ten%. I made it very easy and I have

33
00:03:10,175 --> 00:03:16,892
110 here. So that's the problem. I think
if you have paid attention and you have

34
00:03:16,892 --> 00:03:22,236
gone back, which you can do any time you
know the answer to this. Because I know

35
00:03:22,236 --> 00:03:27,292
what the future value of 100 bucks is at
ten percent its 110. So what is the value

36
00:03:27,292 --> 00:03:34,838
of 110 in future today? Answer has to be
100. Recall again, the first problem I did

37
00:03:34,838 --> 00:03:41,986
with you, I asked how much would 100
become after one year? And we realize that

38
00:03:41,986 --> 00:03:48,041
100 bucks would become 110. I'll ask you
exactly the same question, but in reverse.

39
00:03:48,041 --> 00:03:53,726
I'm saying suppose you had 110, how much
would it be worth? Obviously, to get the

40
00:03:53,726 --> 00:03:59,521
same 100 bucks, the interest rate has to
be the same right, and I've kept it the

41
00:03:59,521 --> 00:04:05,081
same. So the reason I find this problem
very interesting is it's easy to do.

42
00:04:05,081 --> 00:04:10,864
However, how do you go from 110 to 100
bucks. So, this is where what I would

43
00:04:10,864 --> 00:04:18,479
recommend very strongly is that we try to
do the concept before the formula. So if

44
00:04:18,479 --> 00:04:25,469
you look at my notes, I'll just go ahead,
one bit. Is that I never do the formula

45
00:04:25,469 --> 00:04:30,576
before the concept. So if you saw me
toggle, I went to the next page and I

46
00:04:30,576 --> 00:04:37,020
showed you the formula after I did the
concept. So, let's do it again. Simple. It

47
00:04:37,020 --> 00:04:44,909
turns out that present value and future
value, in this case, with one period

48
00:04:44,909 --> 00:04:54,629
difference. Conceptually I know what, I
know already what this is. I know future

49
00:04:54,629 --> 00:05:04,678
value is equal to present value one + R.
Right? Pretty straight forward, I know

50
00:05:04,678 --> 00:05:11,267
this. So how would be the future value of
100 bucks at ten percent after one year,

51
00:05:11,267 --> 00:05:16,994
we know the answer is 110. But know I am
asking you just the unk nown element of

52
00:05:16,994 --> 00:05:28,382
this is this. So what will P will be? P
will be FV / one + R. I'm just, in working

53
00:05:28,382 --> 00:05:35,642
this equation, it's very simple, it's
algebra. However, dividing something by

54
00:05:35,642 --> 00:05:42,248
one + our R if I said it to you the first
time, you would have said, why the heck am

55
00:05:42,248 --> 00:05:48,279
I dividing something by one + something?
And the reason is that one + something is

56
00:05:48,279 --> 00:05:54,034
a factor that anything can be multiplied
by. So the future value factor is one + R.

57
00:05:54,181 --> 00:05:59,465
What is the present value factor? One /
one + R, right? Because I'm taking the

58
00:05:59,465 --> 00:06:05,708
future value, and multiplying it by one
which is missing here, because it doesn't,

59
00:06:05,708 --> 00:06:12,284
divided by one + R. Right? So this guy is
telling me what? This guy is telling me

60
00:06:12,284 --> 00:06:19,817
what is the present value of one buck if I
got it one year from now? I have to divide

61
00:06:19,817 --> 00:06:26,660
that by one + R and if R in our examples
is greater than zero, what will the value

62
00:06:26,660 --> 00:06:33,276
become? Less. So it's very straightforward
that something in the future will become

63
00:06:33,276 --> 00:06:40,763
lesser in magnitude or value when I bring
it today and that's why this whole process

64
00:06:40,763 --> 00:06:47,048
is called discounting. So when you read
about finance, we'll say discounted cash

65
00:06:47,048 --> 00:06:53,068
flow, or discounted money. And the reason
is you're lessening it, and the key to

66
00:06:53,068 --> 00:07:01,047
that lessening is what? This R being
greater than zero creates present to

67
00:07:01,047 --> 00:07:07,076
become larger in the future. But it also
therefore implies by definition that the

68
00:07:07,076 --> 00:07:14,038
larger in the future becomes less today.
So that's the concept, right? But in doing

69
00:07:14,038 --> 00:07:22,019
the concept what I've also done. I have
told you the formula. So I've told you the

70
00:07:22,019 --> 00:07:29,027
formula which we can rewrite, and you can
write now. You created the formula.

71
00:07:29,027 --> 00:07:37,601
Present value is equal to, in our case,
100 bucks. Sorry. $110. I apologize. Which

72
00:07:37,601 --> 00:07:49,066
was the future value divided by 1.1. Why?
Because R was ten%. So 110, pardon the,

73
00:07:49,092 --> 00:07:56,027
this is a one. You know what I mean? I can
make mistakes too. 110/1.1 and this

74
00:07:56,027 --> 00:08:02,097
becomes 100 bucks. How do you double check
that? Very straightforward. That's what I

75
00:08:02,097 --> 00:08:09,025
love about finance. Ask yourself how much
would 100 become in the future? And the

76
00:08:09,025 --> 00:08:15,411
answer, you know, is 110. So future value,
present value are kind of checking each

77
00:08:15,411 --> 00:08:21,618
other. They have to be consistent with
each other, because one is simply looking

78
00:08:21,618 --> 00:08:27,687
at the other in, in reverse, if you may.
So, I hope this is useful to you and this

79
00:08:27,687 --> 00:08:34,040
itself is easy to calculate, because I'm
dividing by 1.1 and the value is, if you

80
00:08:34,040 --> 00:08:39,853
noticed, was 110. However, let's do this
problem. Suppose you will inherent

81
00:08:39,853 --> 00:08:46,233
$121,000 two years from now, and the
interest rate is ten%. How much does it

82
00:08:46,233 --> 00:08:52,078
mean to you today? In other words, ask
yourself the following question, if you

83
00:08:52,078 --> 00:09:00,035
were to put 100 bucks. Right? $100,000, in
the bank how much would it become 121,000?

84
00:09:00,035 --> 00:09:07,056
I'm giving you the answer already and the
reason is I know, because I've created

85
00:09:07,056 --> 00:09:14,014
this problem, that the answer to this
question turns out to be $100,000. So

86
00:09:14,014 --> 00:09:21,063
let's see how I got that. And I'm keeping
the problem simple because you need to

87
00:09:21,063 --> 00:09:27,026
understand the concept better than the
actual calculation. Because the

88
00:09:27,026 --> 00:09:34,914
calculations if they're simple you focus
on. So let me ask you this. Can you tell

89
00:09:34,914 --> 00:09:45,247
me the value of this 121 in year one? Can
you? And I hope you say yes. Why? Because

90
00:09:45,247 --> 00:09:51,137
we have done it. We have done a one year
problem. So that's why I said, in finance,

91
00:09:51,137 --> 00:09:56,485
you can time travel. Watch Star Trek?
Watch Star Wars? Watch Matrix? If that's

92
00:09:56,485 --> 00:10:02,955
part of who you are, finance will be easy.
So, let's time travel to year one How much

93
00:10:02,955 --> 00:10:11,407
will it be? Well, I know it will be
121,000 divided by how much? 1.10. That's

94
00:10:11,407 --> 00:10:19,778
the amount it will be after how much? One
year, but you're looking at two years from

95
00:10:19,778 --> 00:10:25,580
now 121. This is very easy to divide.
That's why I took it. So what is 1.1 into

96
00:10:25,580 --> 00:10:32,131
121? How much will I be left with? Well
one, one, 110. So this is 110,000. But

97
00:10:32,131 --> 00:10:39,634
that's not what I am asking you. I am not
asking you what is the value of $121,000

98
00:10:39,634 --> 00:10:48,173
inheritance one year from now. I am asking
you today. So what do you do. You take

99
00:10:48,173 --> 00:10:57,089
121,000 / 1.1, and then I divide it again
by 1.1. Right? So how much does that

100
00:10:57,089 --> 00:11:06,121
become? I know this guy is 110 and I know
how to divide 110 by 1.1. The answer has

101
00:11:06,121 --> 00:11:14,352
to be 100,000. So just this simple example
tells you how hurtful present value

102
00:11:14,352 --> 00:11:20,554
process can be and why do we call it
discounti ng. If you are taking the future

103
00:11:20,554 --> 00:11:25,559
of 121. And boiling it down to only 100
bucks. $100,000. And the reason is very

104
00:11:25,559 --> 00:11:31,085
simple. The interest is pretty high, but a
lot of people in business, in particular,

105
00:11:31,085 --> 00:11:36,528
it tend to use high interest rates, and I
find that, a little bizarre at times. But,

106
00:11:36,528 --> 00:11:42,302
anyways, just wanted to give you a flavor
of what's going on. And now I'm going to,

107
00:11:42,302 --> 00:11:48,375
do what I promised you, is I'm going to
tell you the concept, formula together. So

108
00:11:48,375 --> 00:11:54,576
let's do it. What's the formula of present
value? So the present value formula. Turns

109
00:11:54,576 --> 00:12:06,326
out to be future value of, in our case,
121 / one + R raised to power what? In our

110
00:12:06,326 --> 00:12:14,469
case, it was two because future value of
121 was occurring when? Two years from

111
00:12:14,469 --> 00:12:22,480
now? In this case, it will be raised to
power N. Where N, in our example, was two,

112
00:12:22,482 --> 00:12:28,742
but could be anything. Right? You could be
getting an inheritance 50 years from now.

113
00:12:28,742 --> 00:12:34,693
You could be retiring 30 years from now.
So this problem doesn't have to be two or

114
00:12:34,693 --> 00:12:40,014
three right? So, so that's why I wanted to
emphasize that and I am going to do one

115
00:12:40,014 --> 00:12:44,867
more thing before we go today. And what I
am going to do is before I get into

116
00:12:44,867 --> 00:12:50,463
multiple payments, that's what next time
is. So just to give you a sense of what,

117
00:12:50,463 --> 00:12:55,045
wherever you're are headed. We've talked
about a single payment carrying back and

118
00:12:55,045 --> 00:13:00,065
forth. Now I will do something that's much
closer to reality. Which is multiple

119
00:13:00,065 --> 00:13:09,027
payments, but before I do that, what I
want to do is. Show you Excel again. So.

120
00:13:10,070 --> 00:13:22,101
Okay, So let's show you excel again and
the problem I will do is the two year

121
00:13:22,101 --> 00:13:29,227
problem. So let's do. Now, what, what is
it that we're trying to calculate? The

122
00:13:29,227 --> 00:13:36,149
critical thing to remember is put the
function that I'm trying to solve for. So

123
00:13:36,149 --> 00:13:46,249
I'm doing PV? Right? And the rate was
what? Ten%. Number of periods was two. Pmt

124
00:13:46,249 --> 00:13:49,691
remember, is a flow every year. We don't
have any of that. And now I am supposed to

125
00:13:49,691 --> 00:13:57,629
tell you the future value. I'll tell you
in the Excel. Okay? So 121,000. I think I

126
00:13:57,629 --> 00:14:04,890
got it. If I didn't, shame on me. And
let's see. A $100,000. Right? So, I want

127
00:14:04,890 --> 00:14:11,075
you to see if, one last thing and I'll
show yo u the power of compounding in

128
00:14:11,075 --> 00:14:18,000
reverse. Right? So lets make this 121,000
stay the same. Let's keep ten percent

129
00:14:18,002 --> 00:14:25,034
interest the same. But let's just mess
with this number two. So I'm going to make

130
00:14:25,034 --> 00:14:32,058
the two ten. So what am I saying? Instead
of giving you $121,000, your inheritance,

131
00:14:32,058 --> 00:14:39,043
suddenly you realize that there was a
typo. Sorry, not two years, but ten years.

132
00:14:39,063 --> 00:14:45,610
Now, you are thinking, no big deal. Well,
you're probably wrong. By huge amount,

133
00:14:45,610 --> 00:14:51,596
your amount of money you're left with, if
I've gotten all the money right and by the

134
00:14:51,596 --> 00:14:55,601
way, part of your problem is to double
check what I'm doing. Not to, kind of

135
00:14:55,601 --> 00:15:00,490
second guess me but to get the problem
right, right? So, so hopefully we have a

136
00:15:00,490 --> 00:15:05,462
relationship now where your not waiting
for me to make a mistake. [laugh]. Because

137
00:15:05,462 --> 00:15:10,025
I will make mistakes. Your goal here is to
try to learn for yourself even if I am. So

138
00:15:10,025 --> 00:15:17,403
what's happened? I have reduced the value
of my inheritance to less than half by

139
00:15:17,403 --> 00:15:25,056
simply taking two years, and making it
ten. So, bless you. I hope you've enjoyed

140
00:15:25,056 --> 00:15:33,326
today. I hope you recognize that finance
has both technology power of thinking, and

141
00:15:33,326 --> 00:15:39,011
bringing it all together. Hope you also
recognize that we are going to go slow in

142
00:15:39,011 --> 00:15:44,336
the beginning and part of the reason is so
that you feel comfortable with time value

143
00:15:44,336 --> 00:15:48,603
of money, and to do that slowness, the
major way I am going slow is by keeping

144
00:15:48,603 --> 00:15:53,914
risk out of the picture. If I threw risk
in there and started messing with that at

145
00:15:53,914 --> 00:15:58,448
the same time, life would become quite
complicated. But just as a, so it

146
00:15:58,448 --> 00:16:03,495
satisfies your curiosity, remember high
risk, high return. High risk, high return

147
00:16:03,495 --> 00:16:08,704
tend to go together. So, even though we
are not talking about risk right now, that

148
00:16:08,704 --> 00:16:14,596
being at the back of your mind in that
simple powerful way is not a bad thing. I

149
00:16:14,596 --> 00:16:20,349
look forward to you next week and I am
really excited about this class and the

150
00:16:20,349 --> 00:16:26,911
reason is believe it or not. I feel like
I'm teaching, each one of you separately.

151
00:16:26,911 --> 00:16:33,149
And I think the power if that has power,
that's awesome because even when I teach

152
00:16:33,149 --> 00:16:39,821
classes like I cannot do that. I think, I
feel I struggle many times becaus e I feel

153
00:16:39,821 --> 00:16:46,308
like I wish I could be a perfect teacher
for everybody, but I'm not. I haven't met

154
00:16:46,308 --> 00:16:51,328
anybody who is because we all have
different ways of learning. And I am

155
00:16:51,328 --> 00:16:57,074
hoping that online, though it has
limitations obviously, online has this

156
00:16:57,074 --> 00:17:04,028
huge benefit that I feel like I am talking
to you, you are there, I can feel you. And

157
00:17:04,028 --> 00:17:09,092
remember, whatever beats here is the same
thing that beats here. So if I see you or

158
00:17:09,092 --> 00:17:14,019
I don't see you, I can feel you. Take are
and see you next week.
