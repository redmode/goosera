1
00:00:00,000 --> 00:00:05,071
So I hope all of you get a sense of this
very simple problem because I think what,

2
00:00:05,071 --> 00:00:11,022
what, what the future problems will be,
will be complicated versions of this. And

3
00:00:11,022 --> 00:00:18,017
I get very excited with simple stuff too
because it's, it's kinda cool. So, how

4
00:00:18,017 --> 00:00:23,092
much will $100 become after two years,
right? So, so now what have I done? I have

5
00:00:23,092 --> 00:00:29,060
taken the one year problem and broken it
into a, and made it into a two year

6
00:00:29,060 --> 00:00:35,058
problem. So let's see how to do that. And
I really would appreciate it if you did

7
00:00:35,058 --> 00:00:41,048
what I am doing, either now or later. And
by the way, I'll do this for relatively

8
00:00:41,048 --> 00:00:47,060
simple problems, and let you work with the
more difficult ones. So how many periods

9
00:00:47,060 --> 00:00:54,016
do I have? I have zero, one and two. And
as I said, the length of the period, the

10
00:00:54,016 --> 00:01:00,050
length of the period, is a year, but
that's artificially chosen. I'm just

11
00:01:00,050 --> 00:01:07,029
choosing it for simplicity. It can be
anything. We'll see that in a second. So,

12
00:01:07,029 --> 00:01:13,032
okay. So, one year, two years, and what is
the question asking me? So you put 100

13
00:01:13,032 --> 00:01:22,082
bucks in the bank. And you're asking
yourself, how much will you become. At

14
00:01:22,082 --> 00:01:30,026
this point, so what is the future value of
this? Right? So, so the, so the question

15
00:01:30,026 --> 00:01:36,731
is pretty straightforward, Abut it is a
little bit complicated. So, here's what

16
00:01:36,731 --> 00:01:44,068
the answer will be. And I'll tell you the
answer first. The answer will be $121.

17
00:01:50,042 --> 00:01:59,305
It's clearly more than 110, but it's 121.
And this, in all of this, that $one, this

18
00:01:59,305 --> 00:02:03,103
guy, if you understand where that's coming
from, you'll see how it'll blow your mind

19
00:02:03,103 --> 00:02:08,535
when you increase the number of periods,
okay? So here's, here's a simple way of

20
00:02:08,535 --> 00:02:13,829
understanding what's going on. So, I'm
going to do the example without a formula,

21
00:02:13,829 --> 00:02:19,525
right? Or, whatever, the formula that we
already know. So, can you tell me, how

22
00:02:19,525 --> 00:02:27,438
much will this be, at this point? Do we
know how to solve a period problem? Of

23
00:02:27,438 --> 00:02:37,859
course we do. We know this is 110. Why?
Because this was 100 x 1.1, right? So, you

24
00:02:37,859 --> 00:02:46,399
don't need the formula to do this,
hopefully. But you could do it in the

25
00:02:46,399 --> 00:02:52,892
heads. But now, noticed what has happened.
I can use the same one period concept

26
00:02:52,892 --> 00:02:59,317
conceptually to move one period forward.
So, what will this amount, which is 110,

27
00:02:59,317 --> 00:03:07,736
be after one year? And what will you do
again? You'll take 110, multiply again by

28
00:03:07,736 --> 00:03:16,140
1.1, and that should give you an answer of
121. So what's going on here? Answer is

29
00:03:16,140 --> 00:03:20,842
very simple. You're doing a one period
problem, twice. And, so, so, think of a

30
00:03:20,842 --> 00:03:26,212
bank. It's, dumb, right? Not people, the
bank. So the bank is looking in the first

31
00:03:26,212 --> 00:03:31,331
period and saying, "What's going on a
times zero? You have 100 bucks." At the

32
00:03:31,331 --> 00:03:35,828
end of one period, what does it do? Give
an interest rate of ten%. It says, "Now

33
00:03:35,828 --> 00:03:41,130
you have 110." But the bank doesn't know
the difference between the ten bucks that

34
00:03:41,130 --> 00:03:47,181
you didn't have in the first year. But now
have, so 110 bucks, bucks are the same. So

35
00:03:47,181 --> 00:03:53,288
it thinks you now have, right this so, 110
bucks and takes it forward another period,

36
00:03:53,288 --> 00:03:59,516
it has become $121. So, what's going on?
Where is that one buck coming from? So, if

37
00:03:59,516 --> 00:04:05,926
you think about it, you're getting ten
bucks here, and ten bucks here. That's one

38
00:04:05,926 --> 00:04:12,244
way to think about it. Why? Because this
ten is ten percent of this, for the first

39
00:04:12,244 --> 00:04:19,703
period, and this ten is again, a ten of
this in the first period. So, if you add

40
00:04:19,703 --> 00:04:26,959
up those, you have 100 + ten + ten, you
have 120 at the end, right? So, you have

41
00:04:26,959 --> 00:04:33,986
100 + ten + ten, so you have 120. So,
you'd be saying, "How did I go from 120 to

42
00:04:33,986 --> 00:04:40,943
121? " The answer is very simple. What we
have ignored in all this is this ten

43
00:04:40,943 --> 00:04:48,091
bucks, which was not here, is added here,
will also earn interest over the second

44
00:04:48,091 --> 00:04:53,176
period. And what is ten percent of ten
bucks? One buck. So, plus one. Is it 121.

45
00:04:53,176 --> 00:04:59,370
So, it's, it's pretty straight-forward.
I'm writing all over the graph, but I want

46
00:04:59,370 --> 00:05:04,982
you to understand that, this is not
complicated. The complication is simply

47
00:05:04,982 --> 00:05:10,415
coming because you, if you, if you're
thinking, you're not thinking about the

48
00:05:10,415 --> 00:05:16,199
ten bucks that comes as interest, will
also start earning interest in the next

49
00:05:16,199 --> 00:05:23,056
period. So, so I've, I've given you a
sense of, what is the future value of 100

50
00:05:23,056 --> 00:05:29,515
bucks, two years from now. And the concept
and formula, let me just repeat one more

51
00:05:29,515 --> 00:05:37,434
time, so that you, you can, understand. So
the formula says this. If I have P at

52
00:05:37,434 --> 00:05:49,123
times zero, after one year it will be P(1
+ r), after two years, what will it be?

53
00:05:49,123 --> 00:06:01,280
P(1 + r) (one + r). Why? Because this P in
our case was 100. But after one year, this

54
00:06:01,280 --> 00:06:12,679
whole thing has become 110, and then when
you carry it forward, again, it will

55
00:06:12,679 --> 00:06:21,071
become 121, and turns out P(1 + r)^2 is
exactly, equal to 121. Now, isn't this

56
00:06:21,071 --> 00:06:27,373
cool? The formula is, is telling you
exactly what's going on, instead of me

57
00:06:27,373 --> 00:06:34,799
throwing the formula at you. Formula makes
sense, but here's where Einstein got blown

58
00:06:34,799 --> 00:06:41,277
away too. You see Einstein said this,
Einstein's most famous equation was E =

59
00:06:41,277 --> 00:06:46,595
MC^2. Now it's square of square out here,
right? They're common to the two. But

60
00:06:46,595 --> 00:06:52,907
turns out, if I have 100 years passing by,
if two were to increase to 100, what would

61
00:06:52,907 --> 00:07:05,093
this formula would become? It would become
P(1 + r)^100. And, even Einstein saw

62
00:07:05,093 --> 00:07:11,676
compounding work that is interest on,
interest on, interest. In this case, it

63
00:07:11,676 --> 00:07:17,097
was only one buck initially over one, two
periods. Interest on, interest on,

64
00:07:17,097 --> 00:07:23,419
interest works. So, its so powerful, that
in fact I would give this advice to you.

65
00:07:23,419 --> 00:07:29,215
Anytime you're asked a finance question,
say, the answer is compounding. And you

66
00:07:29,215 --> 00:07:33,398
are likely to be right, 90 percent of the
time. The only thing you want to do, is

67
00:07:33,398 --> 00:07:38,696
you want to look intelligence. In life,
looking intelligence is far more important

68
00:07:38,696 --> 00:07:43,770
than being intelligent. So, what you want
to do is you want to say, you know, pause

69
00:07:43,770 --> 00:07:48,269
and say, "Is it compounding?". Because
what that will do, is make people think

70
00:07:48,269 --> 00:07:52,789
like you're really cool, you know,
something they don't. But seriously,

71
00:07:52,789 --> 00:07:59,918
compounding is, is really, really, tough
thing to internalize. So, what I'm going

72
00:07:59,918 --> 00:08:07,111
to do now, is I'm going to take advantage
of, Excel. And I promised you that I

73
00:08:07,111 --> 00:08:13,940
wouldn't teach Excel. But I'm going to do
a problem where I'll be forced to use

74
00:08:13,940 --> 00:08:20,313
Excel. So, let's, let's stare at this
problem. And if you want to take a break

75
00:08:20,313 --> 00:08:26,434
right now, this may be a great time to
take a break. Because we have done future

76
00:08:26,434 --> 00:08:32,192
value, where we actually could, by hand
and do the calculation. So, repeat again

77
00:08:32,192 --> 00:08:36,607
in words, I will. $100 after one year,
110. Why? I got ten percent ,ten bucks,

78
00:08:36,607 --> 00:08:41,930
over one year, I have 110. After two
years, what's happened? Well, one way to

79
00:08:41,930 --> 00:08:47,670
think about it, which is very intuitive,
is, how much do I have after one year? If

80
00:08:47,670 --> 00:08:53,383
the bank is still there, of course. It's
110, right? I told you, I won't talk about

81
00:08:53,383 --> 00:08:58,188
risks. So, I'm assuming the bank is still
there. So, 110 you still have, and after

82
00:08:58,188 --> 00:09:03,750
two years, it would have become 121. And
the real ton in your side, is that one

83
00:09:03,751 --> 00:09:09,809
buck. And if you understand that one buck
comes simply from the fact, that you're

84
00:09:09,809 --> 00:09:14,726
going, you now have ten more dollars after
one year, which is also earning ten

85
00:09:14,726 --> 00:09:19,824
percent because it didn't do any harm to
anybody, you know, it's just like the 100.

86
00:09:19,824 --> 00:09:24,616
What did it do? So ten percent of that,
that's the one buck. Now, that is what is

87
00:09:24,616 --> 00:09:29,904
compounding's power interest on interest.
But it's only one buck. Otherwise, if you

88
00:09:29,904 --> 00:09:35,337
didn't have interest on interest, you
would still have 120, right? Ten bucks

89
00:09:35,337 --> 00:09:40,592
each year on the original 100. Now, you
have 121. So, it says, what's the big deal

90
00:09:40,592 --> 00:09:45,968
here? Well, let me try another example,
and then I'll give you some examples which

91
00:09:45,968 --> 00:09:52,070
are really awesome. Just the simple idea,
and I think if you understand compounding

92
00:09:52,070 --> 00:09:57,127
as how difficult it is for a human being
to internalize, you'll understand why

93
00:09:57,127 --> 00:10:01,816
finance is so viewed as so difficult, but
if you understand the intuition, it's

94
00:10:01,816 --> 00:10:05,002
pretty straightforward, right? So, let's
do this problem.
