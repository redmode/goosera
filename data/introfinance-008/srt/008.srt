1
00:00:00,000 --> 00:00:06,188
>> I'm going to use this platform
itself and tell you what the formula will

2
00:00:06,188 --> 00:00:18,166
look like. So it's very obvious now to you
that the P here is $24. But remember when

3
00:00:18,166 --> 00:00:34,639
1626. Right? And R is six%. Remember it's
per year. So two things to remember about

4
00:00:34,639 --> 00:00:43,029
R, it always is for a period of time, in
this case a year, and always a percentage.

5
00:00:43,065 --> 00:00:55,022
I'm asking the following question. What is
the future value in 2012? Everybody on the

6
00:00:55,022 --> 00:01:00,335
same page? ≫> Yes. ≫> So I'm
going to. Now, you write, I'll talk. The

7
00:01:00,335 --> 00:01:10,044
future value will be 24 1.06 which is the
one period factor, future value factor.

8
00:01:10,044 --> 00:01:18,029
Raised to power, not multiplied. Raised to
power number of years that have gone

9
00:01:18,029 --> 00:01:25,094
between 1626 and 2012. And I think if I
have that right, it's 386 years. So now

10
00:01:25,094 --> 00:01:32,095
this, if you can do in your head, it's
pretty cool but largely useless in my

11
00:01:32,095 --> 00:01:38,076
book. Okay? So, so let's try and do it
with a calculator, now that we have a

12
00:01:38,076 --> 00:01:45,018
machine that's dumb, and we can beat it up
once in a while to give us thing that we

13
00:01:45,018 --> 00:01:50,863
don't, can't calculate too easily. So
let's do this. We'll use the same formula,

14
00:01:50,863 --> 00:01:58,894
and let's see how to change it. The first
thing that we encounter is the interest

15
00:01:58,894 --> 00:02:05,896
rate. So 0.06. Did I get that right? No.
I, I. Sorry about that. Just give me a

16
00:02:05,896 --> 00:02:12,463
second. There's always. Okay. So .06 is
the interest rate, and the number of

17
00:02:12,463 --> 00:02:22,422
periods was 386. While you're doing this
or while I'm doing this, try to guess the

18
00:02:22,422 --> 00:02:29,819
answer. If you do, you're a genius. Okay,
and how much money was put away? Remember,

19
00:02:29,819 --> 00:02:37,158
no PMT, no in flow between these 386
years. So all we are having is the initial

20
00:02:37,158 --> 00:02:45,822
good old $24. Did I get that right? Six%,
386, $24. Okay. So let's see what happens.

21
00:02:45,822 --> 00:02:52,323
This by the way has so many zeros in it
that I loose my mind right there. You

22
00:02:52,323 --> 00:02:58,436
know, I always forget how many zeros in a
million, billion. Stuff that you can get

23
00:02:58,436 --> 00:03:04,534
from Google you shouldn't put in your
head, is my philosophy. But it's about

24
00:03:04,534 --> 00:03:12,456
$140,000,000,000. 24 bucks has become
$140,000,000,000. Let me just show you how

25
00:03:12,456 --> 00:03:22,438
things change. So, for example if I just
took away two years from here. Right? I

26
00:03:22,438 --> 00:03:31,208
just made it what it would have been in
2010, which would be 384. Guess what

27
00:03:31,208 --> 00:03:38,096
happens? It's $125,000,000,000. So what
has happened in the last two years? You

28
00:03:38,096 --> 00:03:45,021
added $15,000,000,000. Why? Because you've
earned interest on interest on interest

29
00:03:45,021 --> 00:03:51,835
for a lot of years. But the pool of money
you have after 384 years is so big, that

30
00:03:51,835 --> 00:03:58,246
the next two years just add another
$15,000,000,000. So while this is very

31
00:03:58,246 --> 00:04:04,052
interesting, it is, while it is a very
interesting problem, I also wanted you to

32
00:04:04,052 --> 00:04:08,898
recognize it's not completely unheard of.
In other words, it's not an unreal

33
00:04:08,898 --> 00:04:13,731
question to ask. Is, if this money had
been put away, how much would they have?

34
00:04:13,731 --> 00:04:18,468
Of course, the much more interesting
problem is the following. How much is

35
00:04:18,468 --> 00:04:23,958
Manhattan worth today? So I'm not gonna
get into that. That's, that's an entirely

36
00:04:23,958 --> 00:04:30,202
different question to ask, but it gives
you a sense of what, what I was trying to

37
00:04:30,202 --> 00:04:36,741
get here and what I'm going to do now is
I'm going to take a little bit of a pause,

38
00:04:36,741 --> 00:04:42,590
I'm going to let you think about what we
have done. I would highly recommend, if

39
00:04:42,590 --> 00:04:49,142
you haven't taken a break till now, to
take a break right now. Do two things. Do

40
00:04:49,142 --> 00:04:54,888
yoga, If that's what makes you happy.
Drink coffee, smile, be happy. Just

41
00:04:54,888 --> 00:04:59,985
revisit, especially, the last couple of
problems, and try to familiarize yourself

42
00:04:59,985 --> 00:05:05,073
with both the concept and the technology.
Because I, I'm a big believer, if you do

43
00:05:05,073 --> 00:05:10,186
it on the fly, it just sits with you. If
you postpone it too later. And I'm not

44
00:05:10,186 --> 00:05:15,123
asking you to start the assignment right
now, right? That's a totally different

45
00:05:15,123 --> 00:05:20,042
animal. I'm asking you to just revisit
quickly. Take a break. And what I'm going

46
00:05:20,042 --> 00:05:26,029
to do is I'm going to do one more concept
today. Which has present value and then

47
00:05:26,029 --> 00:05:33,103
we'll. Stop for this week and we'll move
onto more interesting problems still

48
00:05:33,103 --> 00:05:39,990
involving time value of money next week.
Okay? So take a little break. We'll come

49
00:05:39,990 --> 00:05:41,088
back. Thank you.
