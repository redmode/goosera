1
00:00:00,000 --> 00:00:05,572
So, that was last week. Moving forward
this week, I want to emphasize one more

2
00:00:05,572 --> 00:00:12,040
thing. Last week, we had between the
introduction to the class and the present

3
00:00:12,040 --> 00:00:20,015
value, future value of stuff that you did.
We covered a lot of video. And, video

4
00:00:20,015 --> 00:00:24,097
watching is very intense. And I wanted to
be intense because that's how you're

5
00:00:24,097 --> 00:00:31,028
learning. However, again following my
philosophy that time is, the amount of

6
00:00:31,028 --> 00:00:36,038
time you spend on something cannot be the
same for every individual. Similarly, when

7
00:00:36,038 --> 00:00:42,030
I create videos, I'm going to let the
natural process of that day's session

8
00:00:42,030 --> 00:00:48,005
topic drive the length of the video. I
will not cross two hours. If I do, we'll

9
00:00:48,005 --> 00:00:52,083
edit it. But I will not force myself to
do, to do two hours. It will depend on the

10
00:00:52,083 --> 00:00:58,552
nature of the beast. So, one warning about
today. You've got to be an active listener

11
00:00:58,552 --> 00:01:02,965
because we have done some finance, you
know a little bit about what's going on.

12
00:01:02,965 --> 00:01:07,116
We are going to complicate it, but the
complication comes not just because of the

13
00:01:07,116 --> 00:01:11,419
formula, but because I'm going to make it
more real-world. So you've got to be

14
00:01:11,419 --> 00:01:17,000
engaged and you've got to pause the video,
if you want to. We'll take some breaks,

15
00:01:17,000 --> 00:01:21,925
natural breaks, and I'll try to take them
at times where I think it's more

16
00:01:21,925 --> 00:01:27,617
convenient to do so for you, rather than
for me. But I think today will be quite

17
00:01:27,617 --> 00:01:32,417
intense. You'll need to be an active
listener, and have a Excel spreadsheet

18
00:01:32,417 --> 00:01:38,360
with you, or a piece of paper with you to
follow as we go along because I'll do a

19
00:01:38,360 --> 00:01:44,931
lot of problems. So just a little bit,
it's, it's more likely to be much more

20
00:01:44,931 --> 00:01:58,894
intense than last time, okay? So let's get
started. Okay. Today, the main issue we'll

21
00:01:58,894 --> 00:02:04,630
deal with, put very simply, is the notion
of multiple payments. And, what do I mean

22
00:02:04,630 --> 00:02:08,663
by that? Last time, if you remember, we
did do one period, but when then we

23
00:02:08,663 --> 00:02:14,331
crossed over to multiple periods. But one
thing was common to everything we did,

24
00:02:14,331 --> 00:02:19,649
whether it was present value or future
value, we had one thing to carry forward

25
00:02:19,649 --> 00:02:25,330
or back. One payment, one amount of money,
one piece of happiness, whatever. Just

26
00:02:25,330 --> 00:02:32,020
happening, and you're trying to get a hold
of how to mess around with time. As I

27
00:02:32,020 --> 00:02:37,095
said, time travel, if you like it, is what
finance is all about. Today, we'll go into

28
00:02:37,095 --> 00:02:41,691
multiple payments. Why are we doing this?
As I said, there's no necessity to do it

29
00:02:41,691 --> 00:02:47,218
except, life is like that. Very seldom
will you find that you have to make a

30
00:02:47,218 --> 00:02:53,043
decision based on a single payment at a
single point in time. Or single input or

31
00:02:53,043 --> 00:02:58,145
output at a single point in time. In fact,
life's more interesting issues have

32
00:02:58,145 --> 00:03:04,670
something happening today and a lot
happening in the future. So for example,

33
00:03:04,670 --> 00:03:10,412
when Google was started, I doubt if they
said, we'll put in so much effort into

34
00:03:10,412 --> 00:03:16,924
creating something fantastic which will
last only for one period or two periods at

35
00:03:16,924 --> 00:03:22,547
best. But, and things will happen only
once. No, they were hoping and reality

36
00:03:22,547 --> 00:03:27,485
proved them right, is that things will
keep happening. It will expand and time

37
00:03:27,485 --> 00:03:34,065
will play a major role for hopefully
forever. So, the first element of multiple

38
00:03:34,065 --> 00:03:39,471
payments which I saw in text books is
called an Annuity. And this, this is a

39
00:03:39,471 --> 00:03:45,187
special case and annuities we will call
either C. C stands by the way for Cash

40
00:03:45,187 --> 00:03:52,900
Flow or PMT, which stands for Payment and
you'll see why I'm using PMT. Pmt is the

41
00:03:52,900 --> 00:03:58,980
symbol used in calculators and PMT is the
symbol used in textbooks and PMT is the

42
00:03:58,980 --> 00:04:05,374
symbol used for an annuity in a
spreadsheet. And it derives from the fact

43
00:04:05,374 --> 00:04:11,033
that payments are made back for an
obligation, you'll understand in a second.

44
00:04:11,033 --> 00:04:17,070
So let me just first show you some
terminology. So cash flow, which by the

45
00:04:17,070 --> 00:04:23,622
way is being used here simply because I'm
going to use lot of examples where cash is

46
00:04:23,622 --> 00:04:28,796
involved. And today, another aspect of
whatever examples I do after introducing

47
00:04:28,796 --> 00:04:33,972
the concept, is that they are personal
examples. They apply to you personally. We

48
00:04:33,972 --> 00:04:41,604
will do a lot of corporate applications,
applications of when you start project in

49
00:04:41,604 --> 00:04:46,253
a firm and so on. But this class will
start off doing a lot of applications that

50
00:04:46,253 --> 00:04:50,391
means something to you. So if you look
over here, this is a timeline, another way

51
00:04:50,391 --> 00:04:55,238
of drawing a timeline and as I said,
timelines are everything. If you take the

52
00:04:55,238 --> 00:05:00,436
word problem and put it on a timeline,
that what we will do in a second actually,

53
00:05:00,436 --> 00:05:05,540
you have arrived. Finance will just make
your life so easy after that. So here's

54
00:05:05,540 --> 00:05:14,753
what an annuity is. An annuity pays C
dollars three times in this chart. And

55
00:05:14,753 --> 00:05:20,043
something is very important for you to
recognize. If you stare at this chart,

56
00:05:20,043 --> 00:05:26,510
you'll recognize that nothing is happening
at time zero. And when I say time zero, I

57
00:05:26,510 --> 00:05:34,344
mean a specific point in time. What is
one? End of period one, going from zero to

58
00:05:34,344 --> 00:05:39,836
one. So, what is two? End of period two,
and the period lasts from one to two. What

59
00:05:39,836 --> 00:05:46,050
is three? End of period three. So, one
aspect of finance which you have to

60
00:05:46,050 --> 00:05:51,760
remember is when you, there's some
assumptions built into the formulas that

61
00:05:51,760 --> 00:05:58,330
you use. And here the assumption is the
first payment of the annuity occurs one

62
00:05:58,330 --> 00:06:02,863
period from now. And the reason for this
is very simple. You'll see an example that

63
00:06:02,863 --> 00:06:09,201
the classic annuity if you think of, think
of an example is what is it and a classic

64
00:06:09,201 --> 00:06:14,634
annuity is alone. Why? Because you take
out some money, your bank gives you some

65
00:06:14,634 --> 00:06:18,552
money and then you pay back. And
typically, although your not required to

66
00:06:18,552 --> 00:06:23,136
pay back the fixed amount, you do tend to
pay back a fixed amount because the

67
00:06:23,136 --> 00:06:28,298
interest rate is fixed and so on and so
forth. And we can change all that, but

68
00:06:28,298 --> 00:06:32,688
it's very easy to try to understand
something that is fixed for three periods

69
00:06:32,688 --> 00:06:36,896
of time and then change it to the C
changing over three periods. That's

70
00:06:36,896 --> 00:06:41,356
becomes easier to do. So what I'm going to
do is I'm going to first explain this

71
00:06:41,356 --> 00:06:46,658
concept, and I'm going to, as I said, go
slow initially. And in all the problems

72
00:06:46,658 --> 00:06:53,448
too. So let's fill int he cash flow here.
I left it open. So this is zero, nothing

73
00:06:53,448 --> 00:06:59,859
is happening at time zero, which is today,
right now. So zero, one, two, three are

74
00:06:59,859 --> 00:07:06,017
points in time. Periods of time are zero
to one, one to two, two to three. And time

75
00:07:06,017 --> 00:07:12,281
value of money is simply the existant of
an interest rate per period. Okay? So,

76
00:07:12,281 --> 00:07:18,133
we'll come to that in a second. So,
suppose I come to ask you, how many years

77
00:07:18,133 --> 00:07:23,565
to the end? And you should be able to say
that there are three years left to the

78
00:07:23,565 --> 00:07:29,198
end. How did I figure that out? Very
simple. Zero to one is one, one to two is

79
00:07:29,198 --> 00:07:35,126
another one and two to three is the third
one. Why am I doing this? I'm just giving

80
00:07:35,126 --> 00:07:39,801
you a sense of how many years are left,
right? And usually this is something that

81
00:07:39,801 --> 00:07:43,867
should be second nature to you. But many
times people start counting, how many

82
00:07:43,867 --> 00:07:48,424
periods are there? How many time, times or
periods are involved? They get confused.

83
00:07:48,424 --> 00:07:53,984
Don't worry, how many periods are left
here? Two. How many periods left here?

84
00:07:53,984 --> 00:08:01,087
One, and how many periods left here? Zero.
So the thing to recognize is that there is

85
00:08:01,087 --> 00:08:07,879
no cash flow occurring at time point zero
and there is cash flow occurring at point

86
00:08:07,879 --> 00:08:12,994
three, which is the end of the period. So
those are the two things important to

87
00:08:12,994 --> 00:08:18,372
recognize. So now, let's do the future
value of this guy. And before, as I said,

88
00:08:18,372 --> 00:08:23,491
I jump into giving you the formula which
textbooks tend to do. And I, I really

89
00:08:23,491 --> 00:08:27,742
don't like that, because it doesn't take
advantage of your learning that you

90
00:08:27,742 --> 00:08:32,242
already are, has happened. What is the
future value of the first row? Lets do it

91
00:08:32,242 --> 00:08:36,646
one period at a time. One cash flow at a
time. Why am I doing that? Because you

92
00:08:36,646 --> 00:08:40,724
already know how to do it. Right? We did
it last time. So what is the future value

93
00:08:40,724 --> 00:08:46,379
of this? And I'll do it with you. It has
to be zero because, not because time is

94
00:08:46,379 --> 00:08:51,943
three years in our left. It's because by
convention, you do not get a cash flow at

95
00:08:51,943 --> 00:08:57,138
time zero. That's just it. And the notion
that you take a loan, and you start paying

96
00:08:57,138 --> 00:09:01,896
it one period later, right? You do,
typically, if you do pay back some today,

97
00:09:01,896 --> 00:09:07,917
then the loan is lower, right? Okay. So
what's happening in the period two?

98
00:09:07,917 --> 00:09:13,531
Clearly, you're taking C. But for how many
periods are you taking it forward? Well,

99
00:09:13,531 --> 00:09:21,774
it helps to have column number three, if
it says yes to the end. So you know it has

100
00:09:21,774 --> 00:09:28,515
to be (one + r)^2. Remember, you're
carrying it forward, and the reason why

101
00:09:28,515 --> 00:09:33,290
its only square is because the first
payment is at the end of the first year.

102
00:09:33,290 --> 00:09:39,458
And how many years are left from one to
three? Two. It's pretty straight froward.

103
00:09:39,458 --> 00:09:45,385
Second paymen t is c + r. So what am I
doing? I'm actually just breaking up the

104
00:09:45,385 --> 00:09:51,587
problem into bite size pieces to explain
what's going on. So annuity gives you C

105
00:09:51,587 --> 00:09:55,415
three times, not once. Three times.
Remember, we are talking about multiple

106
00:09:55,415 --> 00:10:00,396
payments. And the last one is C. So you
see what's going on. It's, it's pretty

107
00:10:00,396 --> 00:10:05,402
straight forward and I'll let you look at
this for a second. What's going on is, I

108
00:10:05,402 --> 00:10:10,890
have three C's happening at different
points in time. And, let me ask you this.

109
00:10:10,890 --> 00:10:16,363
If there was no time while you have money,
how many C's do you have? Answer's very

110
00:10:16,363 --> 00:10:21,208
simple, you have three C's. And that's
what people intend to do in their heads.

111
00:10:21,208 --> 00:10:27,841
In fact, Wall Street Journal, other
articles I've read and famous newspaper

112
00:10:27,841 --> 00:10:34,641
tend to just approximate and say you're
doing the three C's, well, they're not

113
00:10:34,641 --> 00:10:39,951
three C's. Three C's at different points
in time are a totally different animal

114
00:10:39,951 --> 00:10:45,034
than paying three C's at one point in
time, and that is simply because of time

115
00:10:45,034 --> 00:10:53,928
value of money, okay? So I'm going to move
on and show you how to create the formula

116
00:10:53,928 --> 00:10:59,368
now. Okay? So, again I'm going to use, I'm
going to be little slow in the beginning

117
00:10:59,368 --> 00:11:04,215
So that you understand what we're talking
about. And so, so what's the formula?

118
00:11:04,215 --> 00:11:10,348
Remember, I'm doing future value of how
many payments? Three payments. And as I've

119
00:11:10,348 --> 00:11:15,959
told you before, the one thing, not very
good about this is my handwriting is

120
00:11:15,959 --> 00:11:21,979
pretty awesome [laugh]. So this is FV,
future value. Okay, so what's the future

121
00:11:21,979 --> 00:11:30,586
value of the first guy? C (one + r)^2. And
the first guy occurred in which period?

122
00:11:30,586 --> 00:11:41,374
One. Second? C (one + r). And the third,
C. The one cool thing about an annuity is

123
00:11:41,374 --> 00:11:56,698
that I can take C out in common, right?
And I can do C (one + r)^2 + (one + r) +,,

124
00:11:56,698 --> 00:12:05,797
, right? So it's, let me ask you, what
would this be? Remember last time we spoke

125
00:12:05,797 --> 00:12:11,534
about how if I got a factor, I can just
multiply anything by it and I would get

126
00:12:11,534 --> 00:12:19,568
what I want. Well, this is what, this is
the future value of factor of what? Not

127
00:12:19,568 --> 00:12:28,096
one payment but three C's happening three
times in year one, two, three. At the end

128
00:12:28,096 --> 00:12:35,743
of those years, nothing happening at time
zero , Right? So, the general formula

129
00:12:35,743 --> 00:12:48,623
which I will write for you turns out to be
this, C (one + r) ^n - one, why n - one? I

130
00:12:49,036 --> 00:12:58,461
Will let you figure that out. N - one is
very obvious from here. If you stare at

131
00:12:58,461 --> 00:13:06,594
this. The annuity was three years, and
this annuity is five years. I mean, n

132
00:13:06,594 --> 00:13:13,523
years. Right? So three is different from
the two. Similarly, n is different from n

133
00:13:13,523 --> 00:13:19,170
- one by just one period and that goes
back to the issue I earlier emphasized.

134
00:13:19,170 --> 00:13:26,118
This is all happening because of the fact
that you don't get any payments or you

135
00:13:26,118 --> 00:13:32,105
don't make depends who you are. You're the
bank or you're the person. Nothing is

136
00:13:32,105 --> 00:13:36,684
happening at time zero and I want you to
please understand that convention, because

137
00:13:36,684 --> 00:13:40,823
it can confuse people. And as I said, the
nice thing about finance is that there are

138
00:13:40,823 --> 00:13:44,907
not too many conventions, right? And you
don`t have to memorize things. But this is

139
00:13:44,907 --> 00:13:52,025
a simple one you got to kinda remember and
keep in mind. Okay? What I`ll do is, I

140
00:13:52,025 --> 00:13:57,199
won`t try to simplify these formulas,
there are simplifications available and we

141
00:13:57,199 --> 00:14:02,097
can do all of that stuff. What I`ll do at
the, on the website is, I have provided

142
00:14:02,097 --> 00:14:08,066
you the short form formulas of all of
these, if you need to use them. However,

143
00:14:08,066 --> 00:14:14,088
we'll jump in class straight to not the
final formula, not simplifying things.

144
00:14:14,088 --> 00:14:20,010
It's a waste of your time and my time. I
think what's much more useful to do is to

145
00:14:20,010 --> 00:14:24,086
jump into directly into doing examples.
