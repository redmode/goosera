1
00:00:00,763 --> 00:00:03,660
Now I'm going to move over to the
pre-requisites of this class

2
00:00:03,660 --> 00:00:09,243
and this is a very important topic
because it's kind of a tension,

3
00:00:09,243 --> 00:00:13,700
I feel a little bit of a tension of telling you
what you should know and,

4
00:00:13,700 --> 00:00:17,563
at the same time, encouraging you not to
worry about pre-requisites

5
00:00:17,563 --> 00:00:24,611
So, some knowledge and skills, obviously
will help, and let me emphasize a few

6
00:00:24,611 --> 00:00:27,228
of those things, and this will be a little
bit much for you

7
00:00:27,228 --> 00:00:31,540
so just don't worry what things I'm putting up,
there's a reason for that.

8
00:00:31,540 --> 00:00:33,980
First of all: Economics.

9
00:00:34,211 --> 00:00:40,804
Economics is the mother-discipline of Finance.
The good news is it's extremely intuitive.

10
00:00:40,804 --> 00:00:45,243
In fact, when I teach, the thing that surprises me,
and I teach graduate students,

11
00:00:45,243 --> 00:00:49,331
repeatedly, and in a very pleasant way, it's people
that have had

12
00:00:49,331 --> 00:00:52,907
no background in Economics,
no background in Business

13
00:00:52,907 --> 00:00:58,523
and they just lap up Economics in this.
It kind of reminds me of my own

14
00:00:58,523 --> 00:01:03,860
journey in life.
I mean, I believe life is a bunch of accidents.

15
00:01:03,860 --> 00:01:06,850
I mean, nobody plans
and, if you do plan at sixteen

16
00:01:06,850 --> 00:01:09,943
to be the president of the country,
then there's something wrong with you.

17
00:01:11,158 --> 00:01:15,451
You just take advantage of opportunities.

18
00:01:15,451 --> 00:01:20,603
That's what we call life, anyway.
I'll say things like this throughout the class

19
00:01:20,603 --> 00:01:25,188
and I hope you enjoy them.
If you don't, just pause, or move forward.

20
00:01:25,972 --> 00:01:31,124
So, Economics is something that comes naturally
to most people. And I think,

21
00:01:31,354 --> 00:01:35,946
having a background is important,
but you could, again, use this class

22
00:01:35,946 --> 00:01:40,649
to develop that background and to
satisfy your curiosity.

23
00:01:41,002 --> 00:01:45,185
Mathematics and Statistics.
This is a tough one,

24
00:01:45,354 --> 00:01:49,195
and the reason is, I put Mathematics, which
pretty much covers

25
00:01:49,426 --> 00:01:53,730
all Math invented. But I think we'll largely use
Algebra.

26
00:01:53,946 --> 00:01:58,565
And if you feel comfortable with Algebra,
we will get through.

27
00:01:58,842 --> 00:02:04,787
But I promise you,
I will not use a formula, unless I can explain it in words.

28
00:02:05,002 --> 00:02:08,671
And I'm talking in English,
because that's the language I'm comfortable with.

29
00:02:08,933 --> 00:02:12,389
So, whatever language you're comfortable with,
force yourself

30
00:02:12,650 --> 00:02:16,586
to first say it in words, before using the formula.

31
00:02:16,740 --> 00:02:20,539
Because I think the formula reflects
what we are thinking, not the other way around.

32
00:02:20,647 --> 00:02:24,083
And that's one of the problems with Finance
and technical areas.

33
00:02:24,252 --> 00:02:29,162
That we somehow think, that, just because
there's Math, there's a definitive answer.

34
00:02:29,162 --> 00:02:32,532
All answers are wrong.
There's a bunch of assumptions,

35
00:02:32,532 --> 00:02:36,764
that we make in valuing things,
because it's about the future

36
00:02:36,764 --> 00:02:39,376
and it's your assumptions and
your way of thinking,

37
00:02:39,376 --> 00:02:41,954
that's the value part, not the final answer.
I mean,

38
00:02:41,954 --> 00:02:45,821
anybody can crank through Excel and come up
with an answer, it doesn't mean much.

39
00:02:46,867 --> 00:02:52,130
Statistics.
I think Mathematics broadly defined what I presume

40
00:02:52,130 --> 00:02:56,112
would cover Statistics, but I want to emphasize
Statistics a little bit and,

41
00:02:56,666 --> 00:03:00,890
the reason I'm doing that is,
when we go to Risk and Uncertainty

42
00:03:01,121 --> 00:03:03,342
I will be using Statistics.

43
00:03:03,742 --> 00:03:07,513
So, Economics, Math and Statistics are three things
that are

44
00:03:07,513 --> 00:03:12,967
I would say, form the integral part
of what Finance is all about.

45
00:03:13,244 --> 00:03:18,459
But I promise you that these three,
we will try to take care of

46
00:03:18,535 --> 00:03:20,323
during the class.

47
00:03:20,816 --> 00:03:25,393
So I am not going to dilute the content,
but I'm going to use whatever skills I have

48
00:03:25,670 --> 00:03:28,986
to motivate you to learn this stuff and
to use

49
00:03:29,509 --> 00:03:35,054
all three in a very real-world kind
of context, so that you all get excited about it.

50
00:03:35,946 --> 00:03:39,242
The next one is a little bit of a challenge

51
00:03:39,442 --> 00:03:45,138
because Accounting, is like a language.
I make fun of Accounting all the time,

52
00:03:45,138 --> 00:03:48,534
you see Accounting is like a language, I mean, if I didn't speak English

53
00:03:48,888 --> 00:03:52,450
and the only language you understood
was English, well,

54
00:03:52,865 --> 00:03:54,795
there was a problem here.

55
00:03:55,195 --> 00:03:57,312
Accounting is the language of
Business.

56
00:03:57,728 --> 00:04:01,031
But, like all languages, it's kind of,
limited.

57
00:04:01,246 --> 00:04:02,769
You know, 
don't you sometimes,

58
00:04:02,954 --> 00:04:07,796
want to say something and you know
ten languages and you just can't,

59
00:04:07,796 --> 00:04:11,825
the word still doesn't come to your
lips or your tongue or whatever.

60
00:04:12,332 --> 00:04:16,540
And the reason is, because languages are
limiting.

61
00:04:17,340 --> 00:04:20,467
They are useful, obviously,
but they're also limiting

62
00:04:20,836 --> 00:04:22,961
I mean, the emotion that I feel
sometimes

63
00:04:23,253 --> 00:04:25,895
I cannot put in words
and I'm pretty comfortable with that,

64
00:04:25,895 --> 00:04:31,987
so what do I do? I hug someone, right?
So, luckily, you don't need to talk as a human,

65
00:04:31,987 --> 00:04:37,131
but I can't hug you from a distance,
so I'll give you a virtual hug, if you want it.

66
00:04:37,762 --> 00:04:42,169
So, Accounting has the trouble
of being like English, or

67
00:04:42,415 --> 00:04:45,980
Japanese, or Hindi,
whichever is your mother tongue.

68
00:04:46,442 --> 00:04:51,251
It's that it's limiting.
Finance is awesome. Finance is life.

69
00:04:51,543 --> 00:04:55,154
So imagine you take Accounting to life,
you've got a problem, because [laughs]

70
00:04:55,154 --> 00:04:58,065
Accounting is a language and life is awesome.
So,

71
00:04:59,157 --> 00:05:04,003
the trouble, though, is unless you can communicate
to each other through a language,

72
00:05:04,326 --> 00:05:09,554
you will be stuck.
So, the one class that I think we require

73
00:05:09,554 --> 00:05:12,322
all our students to have is Accounting.

74
00:05:12,753 --> 00:05:17,106
And if I will shy away from one topic,
it's Accounting a little bit,

75
00:05:17,476 --> 00:05:21,485
but we will do enough of it, so that
you can go learn on your own.

76
00:05:21,485 --> 00:05:27,136
So, if you like this class, I would recommend very
strongly to do Accounting,

77
00:05:27,782 --> 00:05:30,715
on your own, or through a similar online class, or

78
00:05:31,284 --> 00:05:34,973
in a real classroom if you have that opportunity.
Because what it'll do is

79
00:05:35,450 --> 00:05:38,622
it'll make you understand
the language of Business better.

80
00:05:39,007 --> 00:05:41,723
The second aspect of Accounting which is important
is, it's

81
00:05:42,277 --> 00:05:45,751
languages are country-dependent, and
Accounting is just like that, so

82
00:05:46,627 --> 00:05:50,171
I can't teach you all the Accounting,
of every country in the world,

83
00:05:50,171 --> 00:05:54,961
even if I knew it. But I'll teach you the principles,
the important ones as we go along.

84
00:05:56,484 --> 00:05:57,843
Finally,

85
00:05:58,828 --> 00:06:04,352
something that's execution-oriented. I would call it an Excel, a calculator,

86
00:06:06,582 --> 00:06:09,374
a simple calculator, a financial calculator.

87
00:06:09,374 --> 00:06:14,907
All the stuff that we'll do on Excel and calculator,
at least for this class,

88
00:06:14,907 --> 00:06:19,891
is pretty easy. But, the trouble is, if you haven't ever
seen it before,

89
00:06:19,891 --> 00:06:24,262
and haven't ever used it before, it seems mind-bogglingly difficult. It's not,

90
00:06:25,185 --> 00:06:29,789
and the beauty of Finance is, whatever words
I say, are exactly, or symbols I use,

91
00:06:30,220 --> 00:06:35,108
are exactly the same ones that you'll see in Excel,
or a financial calculator.

92
00:06:35,616 --> 00:06:41,852
So, to make your life a little bit easier, what I'm going
to do is, I'm going to violate one of my principles,

93
00:06:42,637 --> 00:06:49,066
which I, in real time I follow, which is, I refuse to
do Excel in class,

94
00:06:49,574 --> 00:06:54,012
and the reason is, I don't want the class to become an
Excel class,

95
00:06:54,012 --> 00:06:56,173
because that's missing the boat completely.

96
00:06:56,173 --> 00:06:59,165
In fact, that's completely counter to my
philosophy.

97
00:06:59,165 --> 00:07:02,143
The awesomeness of Finance is
stand-alone,

98
00:07:02,528 --> 00:07:07,564
Excel, calculator, you need to figure out answers,
and they're all wrong anyway [laughs].

99
00:07:07,980 --> 00:07:11,815
It's a structure that you provide to your thinking
and not the other way around.

100
00:07:11,815 --> 00:07:16,549
I know so many people who can't answer questions without recalculating stuff on an Excel,

101
00:07:16,549 --> 00:07:18,854
that shows that you really don't know what's
going on.

102
00:07:19,561 --> 00:07:25,636
So, I'm going to emphasize Excel and calculators.
I'll use the Excel a little bit in the initial examples,

103
00:07:25,636 --> 00:07:27,335
but there's a note on the website,

104
00:07:27,950 --> 00:07:34,883
created by my very own Teaching Assistant Nathan,
who's awesome, and has agreed to help me with this class.

105
00:07:34,883 --> 00:07:41,532
And he's going to create this note,
put it on the website for you, and

106
00:07:41,532 --> 00:07:47,092
you feel free to refer to it, but it's just a teaser.
In other words,

107
00:07:47,938 --> 00:07:51,676
it couldn't possibly cover all applications and so on, but

108
00:07:51,676 --> 00:07:54,692
once you start learning,
you can learn a lot on your own.

109
00:07:55,785 --> 00:07:58,540
But, and there's a but,

110
00:07:58,540 --> 00:08:02,596
we will go, in spite of all these,
quote n' quote, requirements,

111
00:08:02,596 --> 00:08:06,157
I want to emphasize one thing:
I, and this is the tension between

112
00:08:06,480 --> 00:08:09,116
topics you need to know and so on.

113
00:08:09,777 --> 00:08:15,089
I'm a strong believer that prerequisites are
for people who are not curious.

114
00:08:15,935 --> 00:08:19,850
I REALLY believe that.
And that's another thing I'll challenge myself.

115
00:08:20,373 --> 00:08:27,715
Prerequisites are "easy" ways of making life simpler
for the faculty, and for me,

116
00:08:27,885 --> 00:08:33,236
I will challenge myself to make you learn as
much of this as possible,

117
00:08:33,236 --> 00:08:36,612
during Finance, because I think that's the beauty of
Finance too. However,

118
00:08:36,997 --> 00:08:39,476
whenever I think that the class,

119
00:08:39,476 --> 00:08:44,012
which is a fundamentally introductory class,
requires you to go on your own and to other stuff,

120
00:08:44,012 --> 00:08:46,156
I'll make it very obvious to you.

121
00:08:46,156 --> 00:08:50,045
So that you know where you're going.
So, the things I need from you,

122
00:08:50,045 --> 00:08:56,516
starting pretty much today,
is to have curiosity and a desire to learn

123
00:08:56,516 --> 00:09:00,140
and there's an "and" to it,
which I'll come to in a second.

124
00:09:00,140 --> 00:09:08,849
But curiosity and a desire to learn, I'm not kidding you,
if you do not fall in love with Finance

125
00:09:09,357 --> 00:09:15,196
then something's wrong with you, not with Finance.
I mean, I've known Finance for a long time,

126
00:09:15,842 --> 00:09:20,636
nothing's wrong with it.
It's an awesome thing, so if you don't love it,

127
00:09:20,636 --> 00:09:25,564
there's a problem, you need to dig deeper,
google "who am I", "who am I not".

128
00:09:25,564 --> 00:09:28,541
These days I believe you can google that too, it's like
"Who am I?"

129
00:09:28,757 --> 00:09:30,300
and Google will give you an answer.

130
00:09:30,300 --> 00:09:32,300
You may not like it,
but it will.

131
00:09:32,808 --> 00:09:34,892
So, what is the "and"?

132
00:09:34,892 --> 00:09:39,901
"and" is the willingness to work hard.
Now, and after this course is over.

133
00:09:40,378 --> 00:09:43,095
This course is not an advanced course in Finance.

134
00:09:43,095 --> 00:09:47,404
I will challenge you, I'll make you capable of doing an
advanced course,

135
00:09:47,404 --> 00:09:50,588
but we have a limited time and,

136
00:09:50,588 --> 00:09:54,373
willingness to work hard, within that limited time,

137
00:09:54,373 --> 00:09:57,565
I've emphasized already, 5 to 10 hours a week,

138
00:09:58,442 --> 00:10:03,602
but, I cannot emphasize your willingness to work hard
post the class.

139
00:10:04,202 --> 00:10:06,404
And the good news is,
we'll find a way

140
00:10:06,404 --> 00:10:12,212
to keep things available,
so that you can access resources that I have mentioned, or

141
00:10:12,935 --> 00:10:17,900
you can go read books, you can go learn on your own,
or take further classes.

142
00:10:18,962 --> 00:10:26,372
Today I want to, as I said, introduce the class and,
I have talked about most of the aspects of

143
00:10:26,434 --> 00:10:28,740
what we're going to do in the class, but
I want to, now,

144
00:10:28,740 --> 00:10:32,092
talk about one substantive aspect.

145
00:10:32,692 --> 00:10:36,868
This class cannot be taught without any
assumptions in the background.

146
00:10:36,868 --> 00:10:41,937
In fact, that's what Economics is, eventually,
a bunch of assumptions about human behavior and

147
00:10:41,937 --> 00:10:46,628
the hope is, human behavior matches
what the predictions or the assumptions are.

148
00:10:46,828 --> 00:10:51,598
So the first assumption we'll make is:
Competitive Markets.

149
00:10:52,152 --> 00:10:59,037
I know that Markets all around the world have,
especially Financial Markets,

150
00:10:59,037 --> 00:11:02,333
have seen a lot of trouble, recently,
and I know

151
00:11:02,333 --> 00:11:05,299
that Markets can't be perfect.

152
00:11:05,945 --> 00:11:13,949
But the way I want to emphasize Competitive Markets
is, Competitive Markets, to me, are

153
00:11:14,810 --> 00:11:20,605
like Democracy.
Democracy may not be a very easy thing to deal with,

154
00:11:20,605 --> 00:11:24,507
I mean, no it's not,
but there is nothing better.

155
00:11:24,661 --> 00:11:29,253
Similarly, Competitive Markets are extremely important.

156
00:11:29,253 --> 00:11:35,189
In fact, if Competitive Markets were to work
perfectly, or close to perfectly,

157
00:11:35,189 --> 00:11:38,717
they're better than the voting system, in any country.

158
00:11:39,271 --> 00:11:44,381
And the problem "inequality of income" and "lack of ability to participate"

159
00:11:44,381 --> 00:11:49,727
and "concentration of power in few businesses"
can take away from Competitive Markets.

160
00:11:50,266 --> 00:11:53,511
That's why Competitive Markets just don't happen.

161
00:11:53,834 --> 00:11:57,930
This is an institutional structure
that's extremely important

162
00:11:58,345 --> 00:12:03,896
and the one single reason that I like about Democracy
and Competitive Markets,

163
00:12:04,265 --> 00:12:10,968
and this is the more practical reason: I need to know,
when I value something,

164
00:12:10,968 --> 00:12:14,364
I need to know how similar things are valued.

165
00:12:14,825 --> 00:12:21,363
Because if I do not know how similar things are valued,
life becomes very difficult.

166
00:12:21,363 --> 00:12:26,229
That's why, when new ideas come,
we make a lot of mistakes.

167
00:12:26,721 --> 00:12:31,397
So if you have Competitive Markets and you want to
start a business in "bananas", well

168
00:12:31,397 --> 00:12:35,654
there are businesses in bananas being run already
and if they have survived and are surviving

169
00:12:35,992 --> 00:12:41,621
in a Competitive Market,
they tell you how you should value your banana business

170
00:12:41,621 --> 00:12:46,556
and we'll talk about it a little bit.
So prices are extremely important

171
00:12:47,017 --> 00:12:53,266
to figuring out what the value of something should be,
which is what you're thinking of.

172
00:12:53,574 --> 00:13:00,261
And if prices are not good, i.e., they are not set in a Competitive Market, they're not that meaningful.

173
00:13:02,076 --> 00:13:07,342
Related to this topic is, there are frictions in life and
we'll assume the frictions are small

174
00:13:07,342 --> 00:13:12,557
relative to the power of good ideas.
We are trying to value ideas, all kinds,

175
00:13:12,557 --> 00:13:17,401
personal, financial, corporate, name it.

176
00:13:17,863 --> 00:13:23,565
But what do I mean by friction.
I mean, the process needed to make an idea real

177
00:13:23,565 --> 00:13:27,681
shouldn't have so many hurdles,
either monetary, or

178
00:13:28,989 --> 00:13:35,257
effort, or standing in lines, or greasing palms of
government officials.

179
00:13:35,657 --> 00:13:39,079
Those frictions have to be small, because I'm going
to assume that

180
00:13:39,525 --> 00:13:42,725
the market is strong enough to make the

181
00:13:43,278 --> 00:13:46,891
value proposition of most ideas,
if there is some,

182
00:13:47,229 --> 00:13:51,574
larger than these frictions. Otherwise what
happens is you don't do this stuff,

183
00:13:51,928 --> 00:13:53,437
it's too costly to do.

184
00:13:53,437 --> 00:13:56,036
So we're going to make that assumption.
So, in some senses,

185
00:13:57,375 --> 00:14:02,782
Competitiveness and frictions are closely tied, lack of frictions,

186
00:14:02,782 --> 00:14:05,933
so, more competition, less friction kind of thing.

187
00:14:05,933 --> 00:14:11,629
Finally, and this is not the end of the story,
as we go along, we'll emphasize other aspects of it,

188
00:14:11,629 --> 00:14:18,485
we're going to assume Capital can Flow relatively easily.
All these things are, relatively,

189
00:14:20,685 --> 00:14:25,405
relatively is speaking. There's no such thing as a perfect Competitive Market,

190
00:14:25,405 --> 00:14:28,589
I think even the U.S. doesn't have a perfect
Competitive Market.

191
00:14:29,097 --> 00:14:34,661
It wants to, but it doesn't, and most experiments
of the world tend to fail.

192
00:14:34,953 --> 00:14:41,385
If you have the time, read a book called
"Saving Capitalism from Capitalism" and

193
00:14:41,400 --> 00:14:46,573
you'll get a sense of what I'm taling about.
It's written by two colleagues from the University of Chicago.

194
00:14:47,727 --> 00:14:53,909
I think it's an excellent book, making a point about the
role of Markets and Institutions and

195
00:14:53,909 --> 00:14:59,813
Capital, if it can flow easily, what happens is
good ideas get adopted,

196
00:15:00,367 --> 00:15:04,510
by people who may not have the capital, but
have a great idea.

197
00:15:04,510 --> 00:15:08,088
And if that happens, all of us
hopefully benefit, so

198
00:15:08,457 --> 00:15:13,382
these three assumptions are key to whatever
we're going to talk about in the class.

199
00:15:13,920 --> 00:15:19,783
I'm now going to stop this part of the introduction
and what I'm going to do in the

200
00:15:19,783 --> 00:15:25,747
next part of today's session, because we have spent some
time today introducing the class,

201
00:15:26,024 --> 00:15:31,087
but I want to spend the rest of the class, not just
taking time off and saying:

202
00:15:31,087 --> 00:15:37,162
"Ok, we have done the first week."
No, we have limited time, we've got to keep working, so

203
00:15:37,162 --> 00:15:42,159
after the introduction,
take a pause, have coffee, go for lunch,

204
00:15:42,159 --> 00:15:44,232
sleep, whatever you're doing,

205
00:15:44,801 --> 00:15:49,808
and I would recommend very strongly picking up
on the next part of today's session

206
00:15:49,808 --> 00:15:52,807
which is, I will start introducing 
you to the first topic of

207
00:15:52,807 --> 00:15:56,967
Time Value of Money, and because it's so important,
we're going to break it up,

208
00:15:56,967 --> 00:15:59,880
as I said earlier on, into two sessions.

209
00:15:59,880 --> 00:16:03,688
I would also recommend you, before you start watching
this video to,

210
00:16:03,688 --> 00:16:07,975
go back, look at the syllabus, and if you
have the inclination,

211
00:16:07,975 --> 00:16:11,735
look at the note created by Nate,
on how to use

212
00:16:11,735 --> 00:16:16,693
a calculator, or Excel and so on.
Get a little sense of it and

213
00:16:17,124 --> 00:16:24,213
if you don't have access to Excel, for some reason, try to get it, it's a plugin and

214
00:16:25,136 --> 00:16:30,335
don't worry, the mechanics of it can be done, initially,
without a calculator so

215
00:16:30,335 --> 00:16:33,099
you can understand what's going on very
easily

216
00:16:33,099 --> 00:16:35,896
and then you can use the calculator later.

217
00:16:36,312 --> 00:16:37,816
Thanks for listening in and

218
00:16:37,816 --> 00:16:42,112
I hope you join me soon, as we'll
continue with the class and

219
00:16:42,112 --> 00:16:44,216
do a lot of fun stuff together.

220
00:16:44,216 --> 99:59:59,000
Take care.
