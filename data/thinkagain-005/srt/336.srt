1
00:00:03,870 --> 00:00:08,780
In the last lecture, I raised the
question, why it is that we call

2
00:00:08,780 --> 00:00:10,390
the subject that we're going to study

3
00:00:10,390 --> 00:00:14,570
this week, categorical logic, the logic of
categories.

4
00:00:14,570 --> 00:00:19,490
When, really what we're going to be doing
is learning about quantifiers.

5
00:00:19,490 --> 00:00:24,690
quantifiers like all, some, none, only, at
least, and so forth.

6
00:00:24,690 --> 00:00:27,870
What do categories and quantifiers have to
do with each other?

7
00:00:27,870 --> 00:00:29,300
That's the topic of today's lecture.

8
00:00:33,740 --> 00:00:36,440
So, first, let me talk a bit about

9
00:00:36,440 --> 00:00:39,930
categories and what role they can play in
arguments.

10
00:00:39,930 --> 00:00:42,470
And frequently, we give arguments that

11
00:00:42,470 --> 00:00:42,580
[INAUDIBLE]

12
00:00:42,580 --> 00:00:49,440
make use of categories, of kinds of thing.
Consider the following argument.

13
00:00:49,440 --> 00:00:55,420
Brazilians speak Portuguese.
Portuguese speakers understand Spanish.

14
00:00:55,420 --> 00:00:57,400
And, therefore, Brazilians understand
Spanish.

15
00:00:58,660 --> 00:01:00,990
Now there's an argument with two premises
and a conclusion.

16
00:01:00,990 --> 00:01:06,720
And it's an argument that talks about
various categories of things.

17
00:01:06,720 --> 00:01:07,710
One category of thing

18
00:01:07,710 --> 00:01:09,780
it talks about is Brazilians.

19
00:01:09,780 --> 00:01:14,280
Another category is Portuguese speakers,
people who speak Portuguese.

20
00:01:14,280 --> 00:01:18,010
And the third category is people who
understand Spanish.

21
00:01:18,010 --> 00:01:18,240
Okay.

22
00:01:18,240 --> 00:01:24,700
Now, the argument brings those three
categories into relation with each other.

23
00:01:24,700 --> 00:01:26,850
But how does it do that exactly?

24
00:01:26,850 --> 00:01:30,830
What precisely is it that the argument is
telling us?

25
00:01:32,140 --> 00:01:32,480
Well,

26
00:01:32,480 --> 00:01:34,480
here's one interpretation of the argument.

27
00:01:34,480 --> 00:01:35,920
You could understand the argument as

28
00:01:35,920 --> 00:01:41,240
saying that some Brazilians speak
Portuguese.

29
00:01:41,240 --> 00:01:42,890
Certainly that's true.

30
00:01:42,890 --> 00:01:47,920
Maybe not all Brazilians speak Portuguese
maybe there's some Brazilian citizens

31
00:01:47,920 --> 00:01:50,880
who've only recently acquired citizenship
and

32
00:01:50,880 --> 00:01:53,680
have never learned to speak Portuguese.

33
00:01:53,680 --> 00:01:55,690
So some Brazilians speak Portuguese.

34
00:01:55,690 --> 00:01:58,450
That, that much at least is true.

35
00:01:58,450 --> 00:02:02,110
And some Portuguese speakers understand
Spanish.

36
00:02:02,110 --> 00:02:03,110
Of course, that's true.

37
00:02:05,200 --> 00:02:07,300
But suppose the argument concludes from
these

38
00:02:07,300 --> 00:02:10,540
two premises that some Brazilians
understand Spanish.

39
00:02:12,050 --> 00:02:13,200
Would that be a valid argument?

40
00:02:14,920 --> 00:02:18,370
Well, although I'm confident that the
conclusion of that

41
00:02:18,370 --> 00:02:22,320
argument is true, the argument itself
wouldn't be valid,

42
00:02:22,320 --> 00:02:24,330
because there is a way for both premises
of

43
00:02:24,330 --> 00:02:27,040
the argument to be true while the
conclusion is false.

44
00:02:27,040 --> 00:02:30,570
Just imagine this.
While it's true that some Brazilians

45
00:02:30,570 --> 00:02:34,680
speak Portuguese and some Portuguese
speakers understand Spanish.

46
00:02:34,680 --> 00:02:37,450
You could imagine that all that of the
Portuguese speakers

47
00:02:37,450 --> 00:02:42,060
who do understand Spanish are in Portugal,
not in Brazil.

48
00:02:42,060 --> 00:02:45,620
Or in any case, are in some other part of
the world than Brazil.

49
00:02:45,620 --> 00:02:47,790
And that none of the Brazilians who speak

50
00:02:47,790 --> 00:02:51,840
Portuguese are among the Portuguese
speakers who understand Spanish.

51
00:02:52,840 --> 00:02:54,599
In that circumstance,

52
00:02:55,600 --> 00:02:58,220
the premises could both be true while the

53
00:02:58,220 --> 00:03:02,380
conclusion that some Brazilians understand
Spanish would be false.

54
00:03:03,600 --> 00:03:07,940
So the argument, even if it's conclusion
is true, the argument is not valid.

55
00:03:10,000 --> 00:03:14,540
But maybe that's not how to understan the
argument that we just considered a moment

56
00:03:14,540 --> 00:03:17,373
ago, the argument from Brazilians speak
Portuguese and

57
00:03:17,373 --> 00:03:21,610
Portuguese speakers understand Spanish to
Brazilians understand Spanish.

58
00:03:21,610 --> 00:03:23,750
Maybe there's a different way to
understand the argument.

59
00:03:23,750 --> 00:03:26,110
Maybe the right way to understand the
argument

60
00:03:26,110 --> 00:03:29,800
is in saying this, most Brazilians speak
Portuguese.

61
00:03:29,800 --> 00:03:30,830
Certainly, that's true.

62
00:03:32,800 --> 00:03:35,320
And maybe the argument says, is, is
supposed

63
00:03:35,320 --> 00:03:39,080
to say also that most Portuguese speakers
understand Spanish.

64
00:03:41,300 --> 00:03:45,140
I don't know if that's true, but I, I
gather from people that it's probably

65
00:03:45,140 --> 00:03:51,150
true.
And maybe the argument intends to draw

66
00:03:51,150 --> 00:03:56,580
the conclusion from those two premises
that most Brazilians understand Spanish.

67
00:03:56,580 --> 00:03:58,280
Now, would that argument be valid?

68
00:04:00,300 --> 00:04:01,500
Well, no it wouldn't.

69
00:04:03,370 --> 00:04:08,960
Because even if the conclusion of the
argument is true, there's still a possible

70
00:04:08,960 --> 00:04:15,100
scenario in which the premises are true
while the conclusion is false.

71
00:04:15,100 --> 00:04:16,920
I could describe such a scenario to you.

72
00:04:16,920 --> 00:04:20,170
Suppose that while most Brazilians do
speak Portuguese and

73
00:04:20,170 --> 00:04:25,450
most Portuguese speakers do understand
Spanish, most Portuguese speakers,

74
00:04:25,450 --> 00:04:31,480
let's suppose, live outside Brazil.
And the Portuguese speakers who understand

75
00:04:31,480 --> 00:04:39,600
Spanish are just those Portuguese speakers
who live outside Brazil.

76
00:04:40,960 --> 00:04:45,160
So while, then, it would be true that most
Brazilians speak Portuguese and most

77
00:04:45,160 --> 00:04:48,240
Portuguese speakers understand Spanish, it
might not

78
00:04:48,240 --> 00:04:50,990
be true that most Brazilians understand
Spanish

79
00:04:50,990 --> 00:04:54,790
because, maybe, that minority of
Portuguese speakers

80
00:04:54,790 --> 00:04:57,790
who live in Brazil don't understand
Spanish.

81
00:04:57,790 --> 00:05:02,410
The only Portuguese speakers who
understand Spanish, we could suppose,

82
00:05:02,410 --> 00:05:06,010
are that majority of Portuguese speakers
who live outside Brazil.

83
00:05:07,570 --> 00:05:14,440
Now, of course, that isn't the actual
scenario, but it's a possible scenario.

84
00:05:14,440 --> 00:05:16,990
And since it's a possible scenario,

85
00:05:16,990 --> 00:05:19,200
there's a possible scenario in which the
premises

86
00:05:19,200 --> 00:05:21,590
of the argument are true and the
conclusion false.

87
00:05:21,590 --> 00:05:23,060
And so the argument is not valid.

88
00:05:26,120 --> 00:05:28,480
But maybe that's not even the, the correct

89
00:05:28,480 --> 00:05:32,760
way to understand our original argument
about Brazilians.

90
00:05:32,760 --> 00:05:37,110
Maybe the correct way to understand our
original argument is like this.

91
00:05:37,110 --> 00:05:39,910
Maybe the argument is intended to say all

92
00:05:39,910 --> 00:05:45,560
Brazilians speak Portuguese and all
Portuguese speakers understand Spanish.

93
00:05:45,560 --> 00:05:48,860
Therefore, all Brazilians understand
Spanish.

94
00:05:50,050 --> 00:05:50,550
Now,

95
00:05:52,170 --> 00:05:57,260
one thing that this argument has to it's
credit is that it is valid.

96
00:05:57,260 --> 00:06:00,260
If the premises of this argument are true
If

97
00:06:00,260 --> 00:06:03,770
it's true that all Brazilians speak
Portuguese, and it's

98
00:06:03,770 --> 00:06:07,320
true that all Portuguese speakers
understand Spanish, then it's

99
00:06:07,320 --> 00:06:11,430
got to be true that all Brazilians
understand Spanish.

100
00:06:11,430 --> 00:06:14,200
So this argument, in contrast to the last
two

101
00:06:14,200 --> 00:06:17,350
arguments that we looked at, this argument
is valid.

102
00:06:19,050 --> 00:06:20,959
Unfortunately, it's not sound.

103
00:06:22,020 --> 00:06:27,490
Because it's almost certainly not the case
that the two premises are both true.

104
00:06:28,910 --> 00:06:31,560
Be that as it may though, the argument is
valid.

105
00:06:32,810 --> 00:06:34,450
Now notice what we did.

106
00:06:34,450 --> 00:06:41,560
We started off with initial argument that
used three categories in the argument.

107
00:06:41,560 --> 00:06:44,160
The category of Brazilian, the category of
Portuguese

108
00:06:44,160 --> 00:06:47,470
speaker, and the category of person who
understands Spanish.

109
00:06:48,660 --> 00:06:50,710
Then we saw that by the use of

110
00:06:50,710 --> 00:06:56,360
certain modifiers, the modifiers some,
most, or all.

111
00:06:56,360 --> 00:07:00,790
We could make that original argument about
Brazilians understanding Spanish.

112
00:07:00,790 --> 00:07:03,470
We could make that original argument more
precise.

113
00:07:04,950 --> 00:07:09,355
And once we made it more precise, we could
test whether or not it was valid.

114
00:07:09,355 --> 00:07:12,720
Okay.

115
00:07:12,720 --> 00:07:19,010
Now, what I want to introduce is a way of
testing whether or not an argument

116
00:07:19,010 --> 00:07:21,800
is valid, when the argument is one that

117
00:07:21,800 --> 00:07:26,500
uses categories and quantifiers to modify
those categories.

118
00:07:27,950 --> 00:07:30,739
The method involves the use of what we'll
call a Venn Diagram.

119
00:07:32,040 --> 00:07:35,040
A Venn diagram is a diagram that
represents

120
00:07:35,040 --> 00:07:36,390
a number of different categories.

121
00:07:36,390 --> 00:07:40,810
In this simple Venn diagram, we have
representation of the category of

122
00:07:40,810 --> 00:07:44,960
Brazilians and we have a representation of
the category of Portuguese speakers.

123
00:07:45,980 --> 00:07:47,870
Premise one of our original arguments
stated

124
00:07:47,870 --> 00:07:50,920
a relationship between Brazilians and
Portuguese speakers.

125
00:07:50,920 --> 00:07:53,340
But what relation did it state?

126
00:07:53,340 --> 00:07:58,410
Well, that was left vague by the original
statement of the argument.

127
00:07:58,410 --> 00:08:00,220
But, then, when we looked at three

128
00:08:00,220 --> 00:08:02,670
different ways of making the argument more
precise,

129
00:08:02,670 --> 00:08:05,260
we saw three different relations that
could be stated.

130
00:08:06,430 --> 00:08:10,960
It could have stated that some Brazilians
are Portuguese speakers.

131
00:08:10,960 --> 00:08:14,900
The way of representing that would be by
drawing

132
00:08:14,900 --> 00:08:18,910
an x right here to show that there is
something.

133
00:08:21,170 --> 00:08:26,029
There is someone who is both a Brazilian
and a Portuguese speaker.

134
00:08:28,630 --> 00:08:30,280
The second modification we made of the

135
00:08:30,280 --> 00:08:34,690
statement that most Brazilians are
Portuguese speakers.

136
00:08:34,690 --> 00:08:38,160
Now, this week we're not going to discuss
a

137
00:08:38,160 --> 00:08:42,970
way of representing the quantifier most in
Venn Diagrams.

138
00:08:42,970 --> 00:08:47,970
I just call it to your attention as a
quantifier that can be used and frequently

139
00:08:47,970 --> 00:08:54,650
is used to modify categories and argument.
But the third way

140
00:08:54,650 --> 00:08:58,030
that we interpreted our original argument

141
00:08:58,030 --> 00:09:01,130
about Brazilians understanding Spanish,
the third

142
00:09:01,130 --> 00:09:06,840
way as an argument that began by stating
that all Brazilians speak Portuguese.

143
00:09:06,840 --> 00:09:10,920
Now, how would we represent that all
Brazilians speak Portuguese?

144
00:09:11,920 --> 00:09:15,650
Well, what are you saying when you say
that all Brazilians speak Portuguese?

145
00:09:15,650 --> 00:09:20,270
Well, you're saying that there is no
Brazilian who falls

146
00:09:20,270 --> 00:09:24,285
outside the category of Portuguese
speakers.

147
00:09:24,285 --> 00:09:25,050
Right.

148
00:09:25,050 --> 00:09:29,700
This circle right here represents the
category of Portuguese speakers.

149
00:09:29,700 --> 00:09:34,970
And when you say all Brazilians speak
Portuguese, what you're saying is that

150
00:09:34,970 --> 00:09:41,040
whatever Brazilians there are, they have
to be inside this circle.

151
00:09:41,040 --> 00:09:45,430
They can't be outside the circle.
They've gotta be inside

152
00:09:45,430 --> 00:09:47,220
the circle of Portuguese speakers.

153
00:09:48,662 --> 00:09:52,980
So the way to represent that is to shade
out the

154
00:09:52,980 --> 00:09:58,760
portion of the Brazilian circle that's
outside the Portuguese speakers circle.

155
00:09:58,760 --> 00:10:01,370
There, you're indicating that there isn't

156
00:10:01,370 --> 00:10:03,750
anything in the Brazilian circle, there
isn't

157
00:10:03,750 --> 00:10:07,120
anything in the category of Brazilians,
that

158
00:10:07,120 --> 00:10:11,210
falls outside the category of Portuguese
speakers.

159
00:10:11,210 --> 00:10:14,470
In other words, all Brazilians speak
Portuguese.

160
00:10:15,880 --> 00:10:18,090
That's how you could represent that.

161
00:10:18,090 --> 00:10:19,300
And that's what we're going to do

162
00:10:19,300 --> 00:10:22,860
to represent the statement all Brazilians
speak Portuguese.

163
00:10:22,860 --> 00:10:25,790
We simply shade out the part of the

164
00:10:25,790 --> 00:10:29,490
Brazilian circle that's outside the
Portuguese speakers circle.

165
00:10:29,490 --> 00:10:32,930
And to represent some Brazilians speak
Portuguese, we put

166
00:10:32,930 --> 00:10:36,540
an x in the Brazilian circle that's also
inside

167
00:10:36,540 --> 00:10:37,930
the Portuguese speakers circle.

168
00:10:42,130 --> 00:10:44,040
Now we used the Venn diagram to represent
the

169
00:10:44,040 --> 00:10:50,580
information contained in the first premise
of argument about Brazilians.

170
00:10:50,580 --> 00:10:52,230
But how could we use a Venn diagram to

171
00:10:52,230 --> 00:10:56,700
represent the information contained in
both premises and the conclusion?

172
00:10:56,700 --> 00:11:03,010
Well, here's how.
So,

173
00:11:03,010 --> 00:11:08,890
here are three circles corresponding to
the categories of Brazilians, Portuguese

174
00:11:08,890 --> 00:11:12,360
speakers, and those who understand
Spanish.

175
00:11:14,370 --> 00:11:17,112
Now, suppose we interpret the first
premise of our argument,

176
00:11:17,112 --> 00:11:23,120
Brazilians speak Portuguese, as saying
that some Brazilians speak Portuguese.

177
00:11:23,120 --> 00:11:29,140
Well, as we saw already, the way to
represent that is with an x, right here,

178
00:11:29,140 --> 00:11:33,730
that's both in the circle of Brazilians
and in the circle of Portuguese speakers.

179
00:11:33,730 --> 00:11:33,940
Okay.

180
00:11:33,940 --> 00:11:39,790
That represents that there is something
that x, that is both a Brazilian and

181
00:11:39,790 --> 00:11:40,790
a Portuguese speaker.

182
00:11:42,040 --> 00:11:44,520
Now, how would we represent the

183
00:11:44,520 --> 00:11:48,900
information that some Portuguese speakers
understand Spanish?

184
00:11:48,900 --> 00:11:53,350
Again, we could do that with an x, that
is, both in the

185
00:11:53,350 --> 00:11:57,715
circle of Portuguese speakers and in the
circle of those who understand Spanish.

186
00:11:57,715 --> 00:11:59,650
Alright.

187
00:11:59,650 --> 00:12:05,100
So that represents that there is something
that is both a Portuguese speaker and

188
00:12:05,100 --> 00:12:06,510
an understander of Spanish.

189
00:12:08,850 --> 00:12:12,350
But now notice, we've represented the
information that some

190
00:12:12,350 --> 00:12:17,310
Brazilians speak Portuguese and some
Portuguese speakers understand Spanish.

191
00:12:17,310 --> 00:12:26,040
Now, does that information imply that some
Brazilians understand Spanish?

192
00:12:27,290 --> 00:12:30,810
If the information that some Brazilians
are Portuguese speakers and

193
00:12:30,810 --> 00:12:34,590
some Portuguese speakers understand
Spanish, if that information is true,

194
00:12:34,590 --> 00:12:37,770
does it follow from that that some
Brazilians understand Spanish?

195
00:12:38,870 --> 00:12:43,940
Well, one look at this diagram should tell
us that, no, it doesn't follow.

196
00:12:43,940 --> 00:12:49,580
Because look here, right, you have a thing
that is a Brazilian

197
00:12:49,580 --> 00:12:51,710
and a Portuguese speaker, representing the

198
00:12:51,710 --> 00:12:53,740
fact that some Brazilians speak
Portuguese.

199
00:12:53,740 --> 00:12:55,890
Then, you have a thing that is a
Portuguese speaker

200
00:12:55,890 --> 00:12:59,620
and understands Spanish, representing the
fact that some Portuguese speakers

201
00:12:59,620 --> 00:13:00,910
understand Spanish.

202
00:13:00,910 --> 00:13:02,930
But do you have anything that is

203
00:13:02,930 --> 00:13:07,100
both a Brazilian and something that
understands Spanish?

204
00:13:07,100 --> 00:13:08,480
No, not necessarily.

205
00:13:09,860 --> 00:13:14,030
So, this diagram right here shows you that

206
00:13:14,030 --> 00:13:16,950
if the premises of our argument are some
Brazilians

207
00:13:16,950 --> 00:13:20,720
are Portuguese speakers and some
Portuguese speakers understand Spanish,

208
00:13:20,720 --> 00:13:24,830
from those premises, it doesn't follow
that some Brazilians

209
00:13:24,830 --> 00:13:26,808
understand Spanish.

210
00:13:26,808 --> 00:13:29,340
If you want to conclude that some
Brazilians understand

211
00:13:29,340 --> 00:13:32,140
Spanish, your argument is not going to be
valid.

212
00:13:32,140 --> 00:13:35,360
There's going to be a possible scenario in
which the premises

213
00:13:35,360 --> 00:13:39,440
of your argument are true and the
conclusion is false.

214
00:13:39,440 --> 00:13:42,590
And so, we can say, about this argument

215
00:13:42,590 --> 00:13:45,460
right here, that this argument is not
valid.

216
00:13:45,460 --> 00:13:50,490
And we can understand why it's not valid
by looking at this

217
00:13:50,490 --> 00:13:52,360
Venn diagram, right here.

218
00:13:54,670 --> 00:14:00,670
That shows why our argument about some
Brazilians being Portuguese

219
00:14:00,670 --> 00:14:04,930
speakers, why that argument is not valid.
Okay.

220
00:14:04,930 --> 00:14:07,730
But now, let's consider not the argument

221
00:14:07,730 --> 00:14:09,900
to the effect that some Brazilians speak
Portuguese.

222
00:14:09,900 --> 00:14:15,520
Let's consider the argument to the effect
that all Brazilians speak Portuguese.

223
00:14:15,520 --> 00:14:19,280
How would we represent the information in
those premises and that conclusion?

224
00:14:19,280 --> 00:14:19,790
Well,

225
00:14:19,790 --> 00:14:23,730
if all Brazilians speak Portuguese, what
that means is that all

226
00:14:23,730 --> 00:14:29,720
of the Brazilians must be inside the
category of Portuguese speakers.

227
00:14:29,720 --> 00:14:34,295
So there can't be any Brazilians outside
that category.

228
00:14:34,295 --> 00:14:35,110
Okay.

229
00:14:35,110 --> 00:14:37,110
So we can shade out the portion of the

230
00:14:37,110 --> 00:14:40,840
Brazilian circle that's outside the
category of Portuguese speakers.

231
00:14:40,840 --> 00:14:45,019
We know there's nothing in there because
all the Brazilians there

232
00:14:45,019 --> 00:14:48,400
are, are inside the category of Portuguese
speaker,

233
00:14:48,400 --> 00:14:51,700
according to premise one of this new
argument.

234
00:14:52,740 --> 00:14:52,920
Right?

235
00:14:52,920 --> 00:14:56,480
According to premise one, all Brazillians
are Portuguese speakers.

236
00:14:56,480 --> 00:15:00,676
According to that premise, all the
Brazilians there are have to be in here.

237
00:15:00,676 --> 00:15:00,676
[SOUND]

238
00:15:00,676 --> 00:15:01,730
Okay.

239
00:15:01,730 --> 00:15:07,930
Premise two says all Portuguese speakers
understand Spanish.

240
00:15:09,320 --> 00:15:10,360
Okay.

241
00:15:10,360 --> 00:15:12,630
Well, if all Portuguese speakers
understand

242
00:15:12,630 --> 00:15:15,200
Spanish, then there cannot be any

243
00:15:15,200 --> 00:15:21,480
Portuguese speakers who are outside the
category of those who understand Spanish.

244
00:15:21,480 --> 00:15:21,680
Right.

245
00:15:21,680 --> 00:15:24,670
Here's the category of those who
understand Spanish.

246
00:15:24,670 --> 00:15:26,240
And all the Portuguese

247
00:15:26,240 --> 00:15:30,880
speakers, according to premise two, have
to be inside that circle.

248
00:15:30,880 --> 00:15:36,330
So that means that we can shade out the
portion of the Portuguese

249
00:15:36,330 --> 00:15:42,700
speakers circle that's outside the
understands Spanish circle.

250
00:15:42,700 --> 00:15:43,000
Right?

251
00:15:43,000 --> 00:15:47,860
There's, there are no Portuguese speakers
that fall outside the category

252
00:15:47,860 --> 00:15:52,250
of understanders of Spanish, according to
premise two of this argument.

253
00:15:52,250 --> 00:15:52,480
Right?

254
00:15:52,480 --> 00:15:55,240
That all Portuguese speakers understand
Spanish.

255
00:15:55,240 --> 00:15:57,625
So now cannot present that information
that way.

256
00:15:57,625 --> 00:15:59,560
Okay.

257
00:15:59,560 --> 00:16:02,490
Now that we've represented the information
contained in

258
00:16:02,490 --> 00:16:05,760
those two premises, all Brazilians speak
Portuguese and all

259
00:16:05,760 --> 00:16:09,600
Portuguese speakers understand Spanish,
let's ask, does it follow

260
00:16:09,600 --> 00:16:14,803
from those two premises that all
Brazilians understand Spanish?

261
00:16:14,803 --> 00:16:17,345
Well, let's

262
00:16:17,345 --> 00:16:20,030
see.
Yes it does.

263
00:16:20,030 --> 00:16:21,650
What Brazilians can there be?

264
00:16:21,650 --> 00:16:26,270
There can't be any out here.
And there can't be any in here.

265
00:16:26,270 --> 00:16:26,420
And.

266
00:16:26,420 --> 00:16:29,330
of course, there can't be any in here
either.

267
00:16:29,330 --> 00:16:34,139
The only Brazilians there can be are these
in here.

268
00:16:36,120 --> 00:16:42,400
That's the only part of the Brazilian
circle that is left unshaded.

269
00:16:42,400 --> 00:16:45,870
Once we shaded in the circles that we had
to shade

270
00:16:45,870 --> 00:16:49,480
in order to represent the first two
premises of the argument.

271
00:16:49,480 --> 00:16:49,670
Right?

272
00:16:49,670 --> 00:16:53,740
Once we shaded in the part of the
Brazilian circle that we had to

273
00:16:53,740 --> 00:16:55,460
shade in, in order to represent the

274
00:16:55,460 --> 00:16:59,060
premise that all Brazilians are Portuguese
speakers.

275
00:16:59,060 --> 00:17:03,110
And then, we also shaded in the Portuguese
speaker circle outside

276
00:17:03,110 --> 00:17:07,460
the understanders of Spanish circle, the
only part of the Brazilian circle

277
00:17:07,460 --> 00:17:11,350
that's left unshaded is this, which means
that

278
00:17:11,350 --> 00:17:14,640
whatever Brazilians there are, have to be
in here.

279
00:17:15,790 --> 00:17:15,790
[SOUND]

280
00:17:15,790 --> 00:17:21,060
So if there are any Brazilians at all,
they've got to understand Spanish,

281
00:17:21,060 --> 00:17:21,340
[SOUND]

282
00:17:21,340 --> 00:17:25,080
because they're in the circle of things
that understand Spanish.

283
00:17:25,080 --> 00:17:28,040
Therefore, all Brazilians understand
Spanish.

284
00:17:28,040 --> 00:17:35,550
And we've just used this Venn Diagram to
prove visually that our argument is valid.

285
00:17:35,550 --> 00:17:39,058
This argument back here is valid.

286
00:17:39,058 --> 00:17:42,900
And our three-circle Venn diagram shows
that it's valid.

