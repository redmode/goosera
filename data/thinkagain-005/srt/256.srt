1
00:00:02,720 --> 00:00:04,980
Now the third common move in arguments, to

2
00:00:04,980 --> 00:00:09,740
protect premises and avoid the skeptical
regress is discounting.

3
00:00:09,740 --> 00:00:14,240
Discounting, is basically citing the
possible objection that

4
00:00:14,240 --> 00:00:16,660
you think other people might be thinking
of,

5
00:00:16,660 --> 00:00:18,890
in order to head it off by providing

6
00:00:18,890 --> 00:00:21,270
a quick and dirty response to it right
there.

7
00:00:22,830 --> 00:00:25,580
For example, you might be talking to

8
00:00:25,580 --> 00:00:27,820
someone and say well, I'm thinking of
buying

9
00:00:27,820 --> 00:00:30,800
that ring and it's really beautiful.

10
00:00:30,800 --> 00:00:32,680
And you're thinking, well they're going to
object

11
00:00:32,680 --> 00:00:35,620
that the ring is very expensive too.

12
00:00:35,620 --> 00:00:40,000
So you say, well the ring is expensive but
it's beautiful.

13
00:00:41,580 --> 00:00:43,612
So what your doing is citing the
objection.

14
00:00:43,612 --> 00:00:45,378
It's kind of odd if you think about it.

15
00:00:45,378 --> 00:00:48,050
If you want to buy the ring, why are you
saying that its expensive?

16
00:00:48,050 --> 00:00:52,840
And the reason, is that you have already
cited that

17
00:00:52,840 --> 00:00:56,650
objection, which makes it less likely that
a person on the other side

18
00:00:56,650 --> 00:00:59,400
who doesn't want you to buy the ring, is
going to say it.

19
00:00:59,400 --> 00:01:02,120
Because you've already said, I know that.

20
00:01:02,120 --> 00:01:07,170
But it's more important to me, that the
ring is beautiful, so I want to buy it.

21
00:01:07,170 --> 00:01:10,320
So when you say, the ring is expensive,
but it's

22
00:01:10,320 --> 00:01:14,765
beautiful, you're saying that it's
expensive, that's the first thing.

23
00:01:14,765 --> 00:01:17,640
You're saying that it's beautiful,

24
00:01:17,640 --> 00:01:18,755
that's the second thing.

25
00:01:18,755 --> 00:01:21,070
You're saying with the word but that

26
00:01:21,070 --> 00:01:23,940
there's a contrast between the two, and
you're

27
00:01:23,940 --> 00:01:27,070
indicating that the fact that it's
beautiful is

28
00:01:27,070 --> 00:01:29,360
more important than the fact that it's
expensive.

29
00:01:29,360 --> 00:01:32,970
You're saying all of that, simply by
saying

30
00:01:32,970 --> 00:01:35,400
that the ring is expensive, but it is
beautiful.

31
00:01:36,550 --> 00:01:39,540
So first, to say that the ring is
expensive

32
00:01:39,540 --> 00:01:42,380
but it is beautiful is to say two things.

33
00:01:42,380 --> 00:01:42,690
It's like

34
00:01:42,690 --> 00:01:45,480
saying the ring is expensive, and it's
beautiful.

35
00:01:46,630 --> 00:01:51,420
But, in other ways it's very different
from and because if you

36
00:01:51,420 --> 00:01:55,700
say the ring is expensive and it's
beautiful, you can switch them around.

37
00:01:55,700 --> 00:01:58,590
It's beautiful and it's expensive.
It's expensive and it's beautiful.

38
00:01:58,590 --> 00:02:00,735
It's beautiful and it's expensive.
You can say it either way.

39
00:02:00,735 --> 00:02:05,770
You can say it's expensive but it's
beautiful.

40
00:02:05,770 --> 00:02:07,790
That's very different from saying it's
beautiful but

41
00:02:07,790 --> 00:02:10,610
it's expensive.
Think about it.

42
00:02:11,610 --> 00:02:14,700
If you were trying to argue for buying the
ring.

43
00:02:14,700 --> 00:02:15,510
Which would you say.

44
00:02:15,510 --> 00:02:19,555
Well I would say, it's expensive, but it's
beautiful.

45
00:02:19,555 --> 00:02:22,470
And if I'm trying to argue against buying
the

46
00:02:22,470 --> 00:02:25,960
ring, I would say, it's beautiful, but
it's expensive.

47
00:02:27,380 --> 00:02:29,900
Because the word but, indicates that the
sentence

48
00:02:29,900 --> 00:02:33,550
after it, is in some way, more important

49
00:02:33,550 --> 00:02:38,055
than the other clause.
You're discounting, the other objection

50
00:02:38,055 --> 00:02:44,870
and citing after the but clause, the
reason for the belief

51
00:02:44,870 --> 00:02:51,550
or action that you favor.
Thus, and, and but are very different.

52
00:02:51,550 --> 00:02:54,580
The sentence on either side of and are
reversible and

53
00:02:54,580 --> 00:02:57,450
the sentences on either side of but are
not reversible.

54
00:02:57,450 --> 00:02:58,690
And there are other

55
00:02:58,690 --> 00:03:02,630
words that are discounting phrases like
but, that work

56
00:03:02,630 --> 00:03:05,840
the same way but fall in a different
place.

57
00:03:05,840 --> 00:03:11,310
Consider the word although, you can say
although the ring is expensive, it's

58
00:03:11,310 --> 00:03:16,820
beautiful and that sounds like the ring is
expensive, but it's beautiful.

59
00:03:17,900 --> 00:03:20,160
Those are the sentences that someone would
use if they're

60
00:03:20,160 --> 00:03:24,110
arguing for buying the ring because they
want to emphasize that

61
00:03:24,110 --> 00:03:24,950
the ring is beautiful.

62
00:03:26,180 --> 00:03:29,110
The difference is that the word but occurs
right before

63
00:03:29,110 --> 00:03:32,920
the clause that's getting emphasized,
where as the word although,

64
00:03:32,920 --> 00:03:36,470
appears before the clause that's
de-emphasized and it's the other

65
00:03:36,470 --> 00:03:40,280
clause that cites what the speaker takes
to be more important.

66
00:03:40,280 --> 00:03:44,460
So, but and although are each discounting
words.

67
00:03:44,460 --> 00:03:46,790
But the but, occurs before the emphasized

68
00:03:46,790 --> 00:03:51,050
clause and although occurs before the
de-emphasized clause.

69
00:03:51,050 --> 00:03:54,630
What's common to these words like but and
although is that they do three things.

70
00:03:56,420 --> 00:04:01,310
They assert two claims.
They contrast those two claims.

71
00:04:01,310 --> 00:04:06,089
And they indicate that one of those claims
is more important than the other.

72
00:04:07,930 --> 00:04:10,810
And there are lots of words that perform
these functions.

73
00:04:10,810 --> 00:04:12,203
It's not just but and although.

74
00:04:12,203 --> 00:04:17,696
You have even if, even though, whereas.

75
00:04:17,696 --> 00:04:23,227
Nevertheless, nonetheless, still.

76
00:04:23,227 --> 00:04:27,240
And, as with other words that we've been
studying like argument

77
00:04:27,240 --> 00:04:31,650
markers, for example, some of these words
get used in other ways.

78
00:04:31,650 --> 00:04:35,950
So the word still, isn't always a
discounting term.

79
00:04:35,950 --> 00:04:40,270
You say he's sitting still.
You're not discounting an objection.

80
00:04:40,270 --> 00:04:42,660
It's when you use the word still at the
beginning of the sentence.

81
00:04:42,660 --> 00:04:49,140
Still, the diamond is beautiful, or
something like that.

82
00:04:49,140 --> 00:04:51,260
Then, the word still is getting used as a
discounting word.

83
00:04:51,260 --> 00:04:54,610
And, like with other words we've studied,
if you want to know

84
00:04:54,610 --> 00:04:58,670
in a particular case, whether the word
still is being used

85
00:04:58,670 --> 00:05:03,310
as a discounting word, you ask whether you
can substitute a

86
00:05:03,310 --> 00:05:05,730
different discounting word, and the
sentence

87
00:05:05,730 --> 00:05:07,840
will function in basically the same

88
00:05:07,840 --> 00:05:09,470
way, and mean basically the same thing.

89
00:05:09,470 --> 00:05:12,810
And if it does, then, still is being used
as

90
00:05:12,810 --> 00:05:16,500
the discounting word, and if it's not,
then it's not being

91
00:05:16,500 --> 00:05:19,760
used as a discounting word, so we can use
the substitution

92
00:05:19,760 --> 00:05:23,090
method to test for what the function of
the word is.

93
00:05:23,090 --> 00:05:25,370
So why do people use discounting words
like these?

94
00:05:26,700 --> 00:05:29,380
They use them in order to head off
objections, because

95
00:05:29,380 --> 00:05:33,070
if you state the objection first, then
your opponent seems

96
00:05:33,070 --> 00:05:36,239
a little silly to be saying it again.
You just responded to them.

97
00:05:37,410 --> 00:05:39,690
And so, you can defend your premises or

98
00:05:39,690 --> 00:05:43,440
protect your premises and avoid the
skeptical regress by

99
00:05:43,440 --> 00:05:46,530
discounting the kinds of objections, that
people would

100
00:05:46,530 --> 00:05:50,270
raise that might seem to call for further
argument.

101
00:05:50,270 --> 00:05:53,570
And that's a perfectly legitimate use.
Sometimes you want to do that.

102
00:05:53,570 --> 00:05:56,220
You don't want to let your opponent raise

103
00:05:56,220 --> 00:05:58,575
an objection, because that might be
misleading and

104
00:05:58,575 --> 00:06:00,820
gets you off on a tangent and it's a

105
00:06:00,820 --> 00:06:04,570
perfectly effective and useful legitimate
move in an argument.

106
00:06:06,450 --> 00:06:08,020
But you also have to watch out,

107
00:06:08,020 --> 00:06:11,922
because there's some tricks associated
with discounting terms.

108
00:06:11,922 --> 00:06:17,610
In particular I want to talk about the
trick of discounting straw people.

109
00:06:17,610 --> 00:06:20,480
Well, one effective move in argument if

110
00:06:20,480 --> 00:06:23,770
you're just trying to persuade people is
to

111
00:06:23,770 --> 00:06:28,070
make them not see the problems with your
position and one way to

112
00:06:28,070 --> 00:06:33,212
do that is to say, I've got five
objections I'm going to respond to.

113
00:06:33,212 --> 00:06:36,280
You know, you might say this, but.

114
00:06:36,280 --> 00:06:39,566
You might say that, however.
You might say this, whereas.

115
00:06:39,566 --> 00:06:44,940
You might say that, still.
You might say that, although.

116
00:06:44,940 --> 00:06:45,580
Right?

117
00:06:45,580 --> 00:06:48,190
And you discount each of those five
objections,

118
00:06:49,630 --> 00:06:51,965
and yet what you do is you get to pick the
objections.

119
00:06:53,060 --> 00:06:53,400
Right?

120
00:06:53,400 --> 00:06:57,278
So you can pick the easiest objections,
not the hardest objections.

121
00:06:57,278 --> 00:07:01,440
And then, you've got the whole discussion
focusing on the easiest objections.

122
00:07:01,440 --> 00:07:03,580
And as people are trying to keep all
farther those in their

123
00:07:03,580 --> 00:07:06,150
minds straight, they forget about the

124
00:07:06,150 --> 00:07:09,140
other objections which might be even
stronger.

125
00:07:09,140 --> 00:07:12,400
So you discount these straw people.

126
00:07:12,400 --> 00:07:14,370
Straw meaning easy to knock over.

127
00:07:14,370 --> 00:07:15,675
Easy to destroy.

128
00:07:15,675 --> 00:07:20,560
And, make people forget about, the
objections that are harder

129
00:07:20,560 --> 00:07:23,640
to destroy that're going to cause more
serious problems for your theory.

130
00:07:25,140 --> 00:07:27,880
So if you don't really want to know
whether your theory's right or wrong.

131
00:07:27,880 --> 00:07:30,730
You're just trying to persuade people.
That can be an effective move.

132
00:07:30,730 --> 00:07:34,220
And if you don't want to be persuaded by
people who are trying to trick you

133
00:07:34,220 --> 00:07:39,390
like that, then you have to watch out for
other people, discounting straw men and

134
00:07:39,390 --> 00:07:44,170
not facing the really more difficult
objections to their views.

135
00:07:45,210 --> 00:07:47,308
And here's an even trickier trick.

136
00:07:47,308 --> 00:07:50,950
You can combine this trick of discounting
straw people

137
00:07:50,950 --> 00:07:54,210
with other tricks that we saw for other
words.

138
00:07:54,210 --> 00:07:57,205
So, suppose somebody says, well you know,
the

139
00:07:57,205 --> 00:08:00,190
President is all in favor of some kind of

140
00:08:00,190 --> 00:08:02,270
public health service, but a public health
service

141
00:08:02,270 --> 00:08:04,610
is not going to solve all of the medical
problems

142
00:08:04,610 --> 00:08:05,910
of our people.

143
00:08:05,910 --> 00:08:08,630
So, I think the President is off on the
wrong track.

144
00:08:10,990 --> 00:08:13,615
Well, notice what's happened here is

145
00:08:13,615 --> 00:08:16,640
you've discounted the objection that the
public

146
00:08:16,640 --> 00:08:20,155
health service is going to solve all of
the medical problems of our people.

147
00:08:20,155 --> 00:08:22,650
Whoever thought that a public health
service would

148
00:08:22,650 --> 00:08:25,240
serve all of them medical problems of the
people?

149
00:08:26,530 --> 00:08:33,560
So, you're discounting the straw man by
using an unguarded term all.

150
00:08:33,560 --> 00:08:36,040
You put the unguarded term in the

151
00:08:36,040 --> 00:08:37,470
mouth of the objector.

152
00:08:37,470 --> 00:08:42,770
By not guarding it, you make their view
more susceptible to

153
00:08:42,770 --> 00:08:47,110
refutation and make it easier for you to
respond to that objection.

154
00:08:47,110 --> 00:08:49,970
Where as the objector really would have
never used the unguarded

155
00:08:49,970 --> 00:08:54,100
term but would have used the guarding term
like most or many.

156
00:08:54,100 --> 00:08:56,530
Of the health problems of our people.

157
00:08:56,530 --> 00:09:01,250
So by using discounting terms along with
guarding terms and

158
00:09:01,250 --> 00:09:06,530
also assuring terms, you can make moves in
argument

159
00:09:06,530 --> 00:09:11,800
that will point people towards issues that
are framed in the

160
00:09:11,800 --> 00:09:17,170
way you want them to be framed instead of
the way that they want them to be framed.

161
00:09:17,170 --> 00:09:20,600
That's the trick that you have to learn to
watch out for.

162
00:09:20,600 --> 00:09:26,370
So here's a simple rule of thumb, when you
think someone is trying to use

163
00:09:26,370 --> 00:09:29,580
discounting terms to lead you to look at
easiest

164
00:09:29,580 --> 00:09:34,380
objections instead of the most difficult
objections, then you can

165
00:09:34,380 --> 00:09:37,290
think about just forgetting the one's that
this person

166
00:09:37,290 --> 00:09:41,410
mentioned, and ask what did they leave off
the list?

167
00:09:41,410 --> 00:09:45,680
As a rule of thumb, that's usually a good
idea.

168
00:09:45,680 --> 00:09:47,862
But, it's not always going to work.

169
00:09:47,862 --> 00:09:51,390
You're going to have to use your judgment.
Still try

170
00:09:51,390 --> 00:09:53,590
it, maybe it will work in some of the

171
00:09:53,590 --> 00:09:57,180
cases where you want to stop other people
from tricking you.

172
00:09:57,180 --> 00:10:00,040
Now, you should have a pretty good grasp
on.

173
00:10:00,040 --> 00:10:04,210
Assuring, guarding, and discounting.

174
00:10:04,210 --> 00:10:07,660
Three common moves in argument, that are
aimed at stopping the

175
00:10:07,660 --> 00:10:10,920
skeptical regress, and building common
assumptions,

176
00:10:10,920 --> 00:10:13,440
with the people you're talking to.

177
00:10:13,440 --> 00:10:16,440
Let's do a few exercises, in order

178
00:10:16,440 --> 00:10:20,053
to contrast these three and make sure you
understand them.

