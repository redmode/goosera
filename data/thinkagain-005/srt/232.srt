1
00:00:00,012 --> 00:00:04,230
Hi again.

2
00:00:04,230 --> 00:00:07,450
In a previous offering of this course, one
student made a

3
00:00:07,450 --> 00:00:10,850
great point about persuasion and
justification

4
00:00:10,850 --> 00:00:12,690
and the relation between those two.

5
00:00:12,690 --> 00:00:16,570
She did it in an argument that she
presented for the entire

6
00:00:16,570 --> 00:00:20,021
class and I think we can all learn by
listening to her.

7
00:00:20,021 --> 00:00:25,169
So here it is.
Strong arguments don't always persuade

8
00:00:25,169 --> 00:00:28,040
everyone by Jessica Hyde from the United
Kingdom.

9
00:00:29,510 --> 00:00:35,300
It's not enough for an argument to be
strong, valid and sound to be persuasive.

10
00:00:35,300 --> 00:00:40,200
You can have an argument for which every
premise is genuinely true, and where

11
00:00:40,200 --> 00:00:43,330
every conceivable flaw in the argument is

12
00:00:43,330 --> 00:00:47,110
negated and still, not have it be
persuasive.

13
00:00:48,420 --> 00:00:50,270
There will almost always be someone who

14
00:00:50,270 --> 00:00:53,760
either misunderstands the argument, or
blindly believes the

15
00:00:53,760 --> 00:00:56,820
opposite of a premise, in face of facts.

16
00:00:56,820 --> 00:01:00,220
Human beings aren't always logical and
don't

17
00:01:00,220 --> 00:01:03,790
always believe scientifically proven cause
and effect.

18
00:01:03,790 --> 00:01:07,751
Religious and cultural beliefs can be too
hard to overcome.

19
00:01:08,970 --> 00:01:13,042
So even the best arguments can have
disbelievers.

20
00:01:15,022 --> 00:01:20,118
Thank you, Jessica.
What a great argument.

21
00:01:20,118 --> 00:01:22,124
I'm convinced.

22
00:01:22,124 --> 00:01:25,380
Remember, to persuade someone is to
convince

23
00:01:25,380 --> 00:01:27,560
them or change their mind into believing
what

24
00:01:27,560 --> 00:01:31,040
you wanted them to believe, and what you
were trying to get them to believe.

25
00:01:31,040 --> 00:01:35,550
Whereas justifying, is giving them a
reason to believe your conclusion.

26
00:01:37,240 --> 00:01:38,130
And those are different.

27
00:01:38,130 --> 00:01:40,030
Because, as Jessica

28
00:01:40,030 --> 00:01:43,160
argues very well, sometimes people make
mistakes.

29
00:01:43,160 --> 00:01:46,480
You give a perfectly good argument with
perfectly good premises

30
00:01:46,480 --> 00:01:49,840
and it's formulated as clearly as could
ever be expected.

31
00:01:49,840 --> 00:01:54,210
And yet, they don't understand it, or they
don't believe your premises.

32
00:01:54,210 --> 00:01:59,940
And so they're not persuaded.
But still you did give a good argument.

33
00:01:59,940 --> 00:02:05,759
So you succeeded in justifying your
belief, but was it a good

34
00:02:05,759 --> 00:02:08,910
argument if you were trying to persuade
them?

35
00:02:08,910 --> 00:02:09,950
Well maybe not.

36
00:02:09,950 --> 00:02:13,590
If your purpose was to persuade them then
It

37
00:02:13,590 --> 00:02:15,620
might matter to you that you didn't
persuade them.

38
00:02:15,620 --> 00:02:19,460
Whereas, if your purpose was to justify
the conclusion, to give

39
00:02:19,460 --> 00:02:23,715
a reason to believe it, then you did
succeed, in that purpose.

40
00:02:23,715 --> 00:02:26,670
So, whether you see it as a good argument
or

41
00:02:26,670 --> 00:02:30,480
not, is going to depend a lot, on what
your purpose is.

42
00:02:30,480 --> 00:02:31,150
As Jessica

43
00:02:31,150 --> 00:02:34,165
points out, sometimes you're going to want
to

44
00:02:34,165 --> 00:02:36,020
persuade, and it's not always going to be

45
00:02:36,020 --> 00:02:39,450
easy to turn an argument that justifies
and

46
00:02:39,450 --> 00:02:42,345
gives a good reason into one that
persuades.

47
00:02:42,345 --> 00:02:46,670
That is going to depend on making sure
that they do

48
00:02:46,670 --> 00:02:50,990
accept your premises, and that they do
understand your arguments.

49
00:02:50,990 --> 00:02:56,900
And that's yet another trick that we
haven't discussed perhaps

50
00:02:56,900 --> 00:02:58,648
as much as we should have.

51
00:02:58,648 --> 00:03:01,810
And, we learn another lesson from one

52
00:03:01,810 --> 00:03:05,137
of the student comments on Jessica's
argument.

53
00:03:05,137 --> 00:03:09,600
Here it is, from Judith, I think Jessica

54
00:03:09,600 --> 00:03:12,660
has opened a very interesting discussion
with her argument.

55
00:03:12,660 --> 00:03:15,720
Thank you Jessica, I appreciate that, we
do too.

56
00:03:17,460 --> 00:03:19,550
When I'm learning, is the purpose of an
argument

57
00:03:19,550 --> 00:03:22,710
is to state with clarity, and some degree
of certainty,

58
00:03:22,710 --> 00:03:27,230
an opinion or point of view; a valid,
strong and sound argument in it

59
00:03:27,230 --> 00:03:30,070
of itself may never persuade or convert

60
00:03:30,070 --> 00:03:32,220
anyone to adopt a different way of
thinking.

61
00:03:32,220 --> 00:03:33,930
So what.

62
00:03:33,930 --> 00:03:36,610
What a strong argument does is communicate
clearly

63
00:03:36,610 --> 00:03:39,170
what one thinks and why they think it.

64
00:03:39,170 --> 00:03:42,720
So I guess the benchmark of success for
many arguments

65
00:03:42,720 --> 00:03:48,410
is not complete persuasion, but is how
clearly one is understood.

66
00:03:48,410 --> 00:03:50,970
If someone's intent is to blindly refute

67
00:03:50,970 --> 00:03:53,745
everything, that's not an intellectually
honest engagement.

68
00:03:53,745 --> 00:03:56,340
I've found that in construction better,

69
00:03:56,340 --> 00:03:59,030
more thoughtful arguments people may not
agree

70
00:03:59,030 --> 00:04:02,270
with me, but they're far more considerate
of what I have to say.

71
00:04:02,270 --> 00:04:05,180
And by using much of what we're learning,

72
00:04:05,180 --> 00:04:08,200
I'm listening much more intently to other
views.

73
00:04:08,200 --> 00:04:14,010
Yes, Jessica, many things do defy logic.
We just keep trying to do our best.

74
00:04:14,010 --> 00:04:18,335
Thank you Judith.
What a great point.

75
00:04:18,335 --> 00:04:22,205
Because what you've done is you've shown
us that there

76
00:04:22,205 --> 00:04:28,080
are other goals of arguments in addition
to persuasion and justification.

77
00:04:28,080 --> 00:04:30,750
One you mentioned was understanding.

78
00:04:30,750 --> 00:04:33,400
Sometimes the point of an argument is not
to

79
00:04:33,400 --> 00:04:35,960
bring other people over to your point of
view.

80
00:04:37,000 --> 00:04:39,110
But just to make them understand

81
00:04:39,110 --> 00:04:42,640
why you hold the position that you do.

82
00:04:42,640 --> 00:04:45,840
You're trying to show them your reasons
even if you know that

83
00:04:45,840 --> 00:04:50,520
those reasons are not reasons that they,
themselves, are going to accept.

84
00:04:51,950 --> 00:04:53,190
Well, why would you want to do that?

85
00:04:54,670 --> 00:04:57,120
As you say Because it makes them more

86
00:04:57,120 --> 00:05:01,601
considerate of what you believe and of
you.

87
00:05:01,601 --> 00:05:04,560
Because if we understand each other and

88
00:05:04,560 --> 00:05:07,280
the reasons why we hold our positions,
we'll

89
00:05:07,280 --> 00:05:09,780
respect each other more and be more
considerate.

90
00:05:09,780 --> 00:05:10,555
Not always.

91
00:05:10,555 --> 00:05:13,460
Of course there are going to be
exceptions.

92
00:05:13,460 --> 00:05:16,830
But as a general trend, we're going to get
along with each other

93
00:05:16,830 --> 00:05:23,310
much better if we understand why we
disagree and what reasons we have.

94
00:05:24,860 --> 00:05:28,540
The example where this is not working is
politics.

95
00:05:28,540 --> 00:05:29,600
Everybody knows that

96
00:05:29,600 --> 00:05:33,710
politicians just yell at each other and
don't really listen to each other.

97
00:05:33,710 --> 00:05:38,030
They just scream out what's going to
appeal to their base without

98
00:05:38,030 --> 00:05:41,320
thinking about what the real reasons are
for the positions they're holding.

99
00:05:42,410 --> 00:05:46,930
I think they'd be a lot better off, and
we'd be a lot better off, if they were

100
00:05:46,930 --> 00:05:50,430
to take Judith's lesson and say, give us
the

101
00:05:50,430 --> 00:05:54,600
reason so we can understand why you're
adopting that position.

102
00:05:54,600 --> 00:05:56,010
We'll give you our reasons so you

103
00:05:56,010 --> 00:05:58,880
can understand why we're adopting our
position.

104
00:05:58,880 --> 00:06:02,440
And then we can seek a compromise by
satisfying

105
00:06:02,440 --> 00:06:05,393
the values that we both are most concerned
about.

106
00:06:05,393 --> 00:06:10,750
And arguments can play a role then, in
helping us cooperate with each other and

107
00:06:10,750 --> 00:06:17,890
live with each other and compromise on the
very important issues that we all face.

108
00:06:17,890 --> 00:06:19,930
Another lesson that Jessica and Judith

109
00:06:19,930 --> 00:06:24,410
have taught us, is don't set your sights
too high.

110
00:06:25,670 --> 00:06:31,330
If your goal is to persuade everybody,
you're going to be constantly frustrated.

111
00:06:31,330 --> 00:06:32,930
Because there's always going to be people

112
00:06:32,930 --> 00:06:37,230
out there who, don't understand your
argument.

113
00:06:37,230 --> 00:06:41,619
Or who, understand it, but are obstinate,
and refuse

114
00:06:41,619 --> 00:06:45,200
to accept your premise, no matter how well
you argue

115
00:06:45,200 --> 00:06:45,810
for it.

116
00:06:46,850 --> 00:06:51,080
So, if you try to convince everybody,
you're not going to succeed.

117
00:06:52,080 --> 00:06:52,741
So give it up.

118
00:06:52,741 --> 00:07:01,400
You can't convince or persuade everyone.
Still you can accomplish a lot.

119
00:07:01,400 --> 00:07:06,440
You can help them understand you,you can
come

120
00:07:06,440 --> 00:07:10,870
to understand them and you can give them
good

121
00:07:10,870 --> 00:07:16,740
reasons to believe your conclusion.
Well,

122
00:07:16,740 --> 00:07:23,160
that's a lot, and that can be very
important, even if they're not persuaded.

123
00:07:23,160 --> 00:07:28,990
And you, if you're justified to believing
your conclusion can have reason to believe

124
00:07:28,990 --> 00:07:31,790
that the fault lies with them, not

125
00:07:31,790 --> 00:07:34,550
with you, when they don't accept your
conclusion.

126
00:07:35,740 --> 00:07:38,510
Course you probably can't convince them of
that.

127
00:07:38,510 --> 00:07:41,831
You can't convince them that the fault
lies with them, not with you.

128
00:07:41,831 --> 00:07:48,530
But still, you might have accomplished
your goals if your goals are reasonable.

129
00:07:48,530 --> 00:07:53,910
Namely, to increase understanding, to find
the reasons for your belief, and

130
00:07:53,910 --> 00:07:58,100
to present those reasons and ways that
people ought to understand and accept.

131
00:07:59,510 --> 00:08:00,760
That's what justification

132
00:08:00,760 --> 00:08:05,060
is and that's what understanding is, and
those can be extremely valuable in

133
00:08:05,060 --> 00:08:11,598
arguments even if there's still some
people out there who aren't persuaded.

