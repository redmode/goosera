1
00:00:00,025 --> 00:00:04,840
There's one more kind of language that we
need to

2
00:00:04,840 --> 00:00:10,000
discuss, because it's also used to stop
the skeptical regress.

3
00:00:10,000 --> 00:00:12,300
Much like assuring and guarding and

4
00:00:12,300 --> 00:00:16,300
discounting, and this language is
Evaluative.

5
00:00:16,300 --> 00:00:20,660
Just imagine, that a politician says, You
ought to

6
00:00:20,660 --> 00:00:25,070
support my healthcare plan, because it
would be good for

7
00:00:25,070 --> 00:00:29,183
the country.
What is the word good doing here?

8
00:00:29,183 --> 00:00:32,410
Some philosopher's going to tell you that
the word

9
00:00:32,410 --> 00:00:35,910
good is just a way of expressing your
emotions.

10
00:00:35,910 --> 00:00:38,630
Or maybe telling you what to do.

11
00:00:38,630 --> 00:00:44,300
So the politician is saying, Yay, for my
healthcare plan or telling

12
00:00:44,300 --> 00:00:48,245
you in an imperative form, you ought to
support my healthcare plan.

13
00:00:48,245 --> 00:00:50,210
But that can't really

14
00:00:50,210 --> 00:00:55,660
be the whole story, because when someone
says, Yay, Duke,

15
00:00:55,660 --> 00:00:58,118
like I do when I cheer for the Duke team.

16
00:00:58,118 --> 00:01:02,690
First of all I'm not saying that the team
is good, I

17
00:01:02,690 --> 00:01:06,710
might cheer for the Duke team, even when I
know they're not good.

18
00:01:06,710 --> 00:01:09,670
And secondly, you can't ask me why.

19
00:01:09,670 --> 00:01:14,527
If I say, Yay, Duke, it doesn't make any
sense if you turn to me and say, but why?

20
00:01:14,527 --> 00:01:15,391
Why, yay

21
00:01:15,391 --> 00:01:18,510
Duke?
It don't make any sense.

22
00:01:18,510 --> 00:01:21,674
So, merely to express your emotions with
something like Yay,

23
00:01:21,674 --> 00:01:25,980
Duke, is very different from saying Duke
has a good team.

24
00:01:25,980 --> 00:01:30,060
And saying Yay for my healthcare plan, is
very different

25
00:01:30,060 --> 00:01:33,920
from saying that the healthcare plan is
good for the country.

26
00:01:33,920 --> 00:01:39,985
Similarly, if I say, I don't like fish, so
we shouldn't have fish for dinner.

27
00:01:39,985 --> 00:01:42,400
Well,

28
00:01:42,400 --> 00:01:43,860
I don't really owe you a reason.

29
00:01:43,860 --> 00:01:49,380
I can just say I just don't like the taste
of fish, end of story, leave me alone.

30
00:01:50,500 --> 00:01:53,635
I don't owe you a reason for why I don't
like fish, I just don't.

31
00:01:53,635 --> 00:02:01,240
But, if I say it's immoral to eat fish,
it's wrong to eat fish.

32
00:02:01,240 --> 00:02:07,690
You ought not to eat fish.
It's a very different story, now I

33
00:02:07,690 --> 00:02:08,370
owe you a reason.

34
00:02:08,370 --> 00:02:12,930
If I say, it's immoral to eat fish I need
to say what's immoral about it?

35
00:02:12,930 --> 00:02:16,680
I need to point to some feature of eating
fish that makes it immoral.

36
00:02:16,680 --> 00:02:21,300
I can't just use that evaluative language
without some kind

37
00:02:21,300 --> 00:02:26,010
of reason to back it up, that would be
illegitimate.

38
00:02:26,010 --> 00:02:28,905
So, what that shows is that merely
expressing

39
00:02:28,905 --> 00:02:32,690
preferences is very different from making
an evaluation and

40
00:02:32,690 --> 00:02:36,740
saying that something is good or bad or
right or wrong or immoral or moral.

41
00:02:36,740 --> 00:02:41,830
And one way to capture this feature
evaluative language is to interpret a word

42
00:02:41,830 --> 00:02:47,910
like Good as meets the standards and Bad
as violates the standards.

43
00:02:47,910 --> 00:02:50,040
Notice it's very vague, because it doesn't
tell you what the

44
00:02:50,040 --> 00:02:53,942
standards are and those standards will
change from one context to another.

45
00:02:53,942 --> 00:02:57,790
If you're talking about a good painting,
the standards of a good painting are

46
00:02:57,790 --> 00:03:00,950
different from when you're talking about
say a good investment.

47
00:03:00,950 --> 00:03:02,540
Where the standards are going to be

48
00:03:02,540 --> 00:03:05,240
completely different from the aesthetic
case.

49
00:03:06,590 --> 00:03:10,460
So, if we interpret good as meets the
standards, and

50
00:03:10,460 --> 00:03:14,480
we say, my health care program is good for
the country.

51
00:03:14,480 --> 00:03:16,500
Then that means, it meets the standards
for what

52
00:03:16,500 --> 00:03:19,360
will make the country function in a
certain way.

53
00:03:20,420 --> 00:03:23,090
Whereas if we say, eating fish

54
00:03:23,090 --> 00:03:26,630
is immoral, what we're saying is that

55
00:03:26,630 --> 00:03:29,100
eating fish violates a certain kind of
standard.

56
00:03:29,100 --> 00:03:31,530
And more specifically it's a moral
standard.

57
00:03:31,530 --> 00:03:33,830
That's why we use the word immoral.

58
00:03:33,830 --> 00:03:37,980
So we can interpret this language in terms
of meeting or violating standards.

59
00:03:37,980 --> 00:03:41,400
And then, to give the reason, why it's
good

60
00:03:41,400 --> 00:03:44,500
or bad, or right or wrong, or moral or
immoral.

61
00:03:44,500 --> 00:03:48,520
We can cite the standard and apply it to
the case

62
00:03:48,520 --> 00:03:52,940
in order to give a reason for why the
evaluation holds.

63
00:03:52,940 --> 00:03:59,170
But now, here's the trick, when we call it
good, we don't say what the standards are.

64
00:03:59,170 --> 00:04:04,420
We leave that up to the context to specify
what kind of standard we're talking about.

65
00:04:04,420 --> 00:04:08,800
So, it's kind of like assuring, when you
say, I assure you, and you might cite

66
00:04:08,800 --> 00:04:13,764
some, authority or tell them that you have
some reason, and you don't tell them what

67
00:04:13,764 --> 00:04:14,695
the reason is.

68
00:04:14,695 --> 00:04:16,870
When you call it good, you say it does

69
00:04:16,870 --> 00:04:20,320
meet the standards, but you don't say the
standards are.

70
00:04:20,320 --> 00:04:24,145
So, by alluding to the standards without
actually laying them

71
00:04:24,145 --> 00:04:27,445
out, you have made your claim a little
more defensible.

72
00:04:27,445 --> 00:04:29,635
Because, if you lay out the standards,

73
00:04:29,635 --> 00:04:32,409
they might be questionable and your
audience

74
00:04:32,409 --> 00:04:36,598
would know exactly what to question and
what to deny and how to object.

75
00:04:36,598 --> 00:04:39,360
But, if you simply say it's good, and

76
00:04:39,360 --> 00:04:41,345
all you're saying is, it meets the
standards.

77
00:04:41,345 --> 00:04:47,530
Then, you've avoid it on objection and
made your premise more defensible.

78
00:04:47,530 --> 00:04:50,416
And that's how this type of evaluation of

79
00:04:50,416 --> 00:04:53,674
language might help to stave off the
skeptical regress.

80
00:04:53,674 --> 00:04:56,338
And here's another way evaluation can
help.

81
00:04:56,338 --> 00:04:59,506
We don't always have to agree what the
standards are.

82
00:04:59,506 --> 00:05:04,330
Suppose you're driving down the road and I
say, you know we ought to turn left here.

83
00:05:04,330 --> 00:05:06,778
And you say, yeah we ought to turn left
here.

84
00:05:06,778 --> 00:05:09,658
Well I might think that we ought to turn
left here,

85
00:05:09,658 --> 00:05:13,550
because that's going to be a quicker way
to get to our destination.

86
00:05:13,550 --> 00:05:17,780
But you might think that we ought to turn
left here, because that's going to

87
00:05:17,780 --> 00:05:21,040
be a more beautiful view and you'll be
able to look out on the hills.

88
00:05:22,750 --> 00:05:25,040
But, we can agree that we ought to turn
left here.

89
00:05:25,040 --> 00:05:27,130
Cause, we both agree that turning left

90
00:05:27,130 --> 00:05:29,880
meets the standards, even though my
standards

91
00:05:29,880 --> 00:05:31,835
are efficiency and getting there quickly.

92
00:05:31,835 --> 00:05:36,770
And your standards are aesthetic and
getting beautiful views.

93
00:05:36,770 --> 00:05:41,270
So if you can get more people to agree to
your premise simply by saying this

94
00:05:41,270 --> 00:05:43,130
health care plan will be good for the

95
00:05:43,130 --> 00:05:47,370
country, without saying exactly how it's
going to be good.

96
00:05:47,370 --> 00:05:51,990
Then you've avoided people disputing your
objections, because they can agree

97
00:05:51,990 --> 00:05:55,460
to it since they can use their own
standards to determine

98
00:05:55,460 --> 00:05:56,980
whether it is good or not.

99
00:05:56,980 --> 00:06:02,000
And that could be yet another way to avoid
the skeptical regress.

100
00:06:02,000 --> 00:06:06,350
Notice that evaluation can occur at a lot
of different levels.

101
00:06:06,350 --> 00:06:09,810
We have some words that are very abstract
like good and

102
00:06:09,810 --> 00:06:14,010
bad and ought and ought not, should,
should not, right, wrong.

103
00:06:15,130 --> 00:06:18,300
And those words can be used in a lot of
different context.

104
00:06:18,300 --> 00:06:20,930
You can have the wrong investment,

105
00:06:20,930 --> 00:06:24,830
or a good investment.
Or an investment that you ought to make.

106
00:06:24,830 --> 00:06:29,520
But you can also drive on the right path,
or a

107
00:06:29,520 --> 00:06:34,378
bad path or a way that you ought not to
go.

108
00:06:34,378 --> 00:06:41,220
And so, you can have navigational
standards and economic standards,

109
00:06:41,220 --> 00:06:46,470
but they can all be expressed by these
very general abstract evaluative words.

110
00:06:46,470 --> 00:06:48,730
Like good and bad and right and wrong and
ought

111
00:06:48,730 --> 00:06:51,678
and out not and should and should not and
so on.

112
00:06:51,678 --> 00:06:55,580
But other evaluative words are much more
specific.

113
00:06:55,580 --> 00:06:59,840
Now, for example, you can call a painting
beautiful or ugly.

114
00:07:00,880 --> 00:07:04,389
But you don't call fertilizer beautiful or
ugly.

115
00:07:04,389 --> 00:07:09,550
You would never say that a stock is
beautiful or ugly.

116
00:07:09,550 --> 00:07:12,190
They're just not the kind of thing to be
evaluated

117
00:07:12,190 --> 00:07:13,100
in that way.

118
00:07:13,100 --> 00:07:17,990
So an evaluative word like beautiful or
ugly is more specific.

119
00:07:17,990 --> 00:07:20,065
It only applies to a small range of
things.

120
00:07:20,065 --> 00:07:24,810
Things whereas other words apply like good
and bad, apply to almost anything.

121
00:07:24,810 --> 00:07:26,080
Here's another example.

122
00:07:26,080 --> 00:07:30,730
Cruel or brave, a person can be cruel or
brave.

123
00:07:30,730 --> 00:07:33,830
But you can't say that a painting is cruel
or brave.

124
00:07:33,830 --> 00:07:37,030
Or a desk is cruel or brave, or a chair is

125
00:07:37,030 --> 00:07:39,399
cruel or brave.
A chair might be comfortable.

126
00:07:40,880 --> 00:07:44,790
But a painting is not comfortable, and a
soldier is not comfortable.

127
00:07:44,790 --> 00:07:50,130
Soldiers are brave or not, chairs are
comfortable or not, but chairs

128
00:07:50,130 --> 00:07:52,765
are not brave or not, and soldiers are not
comfortable or not.

129
00:07:52,765 --> 00:07:57,824
So these evaluative words like brave or
cowardly,

130
00:07:57,824 --> 00:08:01,599
and beautiful are ugly, or comfortable or
uncomfortable.

131
00:08:01,599 --> 00:08:07,960
Apply only to limited ranges of things,
rather than to just about anything.

132
00:08:07,960 --> 00:08:11,370
So we have very general, or abstract
devaluative words,

133
00:08:11,370 --> 00:08:15,610
and we have more specific More concrete
evaluative words.

134
00:08:15,610 --> 00:08:19,590
And, of course, which ones are specific or
concrete will vary.

135
00:08:19,590 --> 00:08:23,800
Some are more concrete than others.
It's not an absolute dichotomy.

136
00:08:23,800 --> 00:08:27,270
But, some words that are evaluative

137
00:08:27,270 --> 00:08:29,190
really will apply to almost anything.

138
00:08:29,190 --> 00:08:32,810
And other words apply to a more limited
class.

139
00:08:32,810 --> 00:08:37,205
And they vary in how limited that class of
things they apply to will be.

140
00:08:37,205 --> 00:08:42,130
So you might ask, why are all these words
evaluative words?

141
00:08:42,130 --> 00:08:42,790
Think about it.

142
00:08:42,790 --> 00:08:45,590
If you want to explain a more limited
evaluative

143
00:08:45,590 --> 00:08:48,510
word like beautiful, you want to explain
what it means.

144
00:08:48,510 --> 00:08:52,430
You need to define it in terms of the more
general words, like

145
00:08:52,430 --> 00:08:52,680
good.

146
00:08:52,680 --> 00:08:57,000
If you want to say it's beautiful, you can
say that kind of means looks good.

147
00:08:57,000 --> 00:09:00,670
I know that's not quite right, but
basically, when you want to define the

148
00:09:00,670 --> 00:09:05,360
word beautiful, you need to cite one of
the more general words good.

149
00:09:05,360 --> 00:09:10,170
And then cite the specific way in which
it's good, namely, the way it looks.

150
00:09:10,170 --> 00:09:14,390
And when you want to say an economic word
like bargain.

151
00:09:14,390 --> 00:09:16,025
Bargain means a good price.

152
00:09:16,025 --> 00:09:17,460
It sells for

153
00:09:17,460 --> 00:09:20,880
a good price, and a good price is a low
price.

154
00:09:20,880 --> 00:09:24,039
So, when you define what a bargain is, you
need

155
00:09:24,039 --> 00:09:26,920
to cite the word good in order to define
bargain.

156
00:09:26,920 --> 00:09:31,230
So the relation between these very general
evaluative words and

157
00:09:31,230 --> 00:09:35,320
the more specific evaluative words that
makes them all evaluative.

158
00:09:35,320 --> 00:09:37,055
Is that you need to define the specific

159
00:09:37,055 --> 00:09:40,600
evaluative words in terms of the more
general ones.

160
00:09:40,600 --> 00:09:42,575
So it all comes down to what makes
something

161
00:09:42,575 --> 00:09:47,380
evaluative is its connection to what's
good or bad or right or wrong or what

162
00:09:47,380 --> 00:09:50,975
ought or ought not to be done or should or
shouldn't be done and so on.

163
00:09:50,975 --> 00:09:54,426
You know, the trickiest cases of
evaluative

164
00:09:54,426 --> 00:09:56,000
words are, words that are contextually
evaluative.

165
00:09:56,000 --> 00:10:00,510
They don't actually get defined by good or

166
00:10:00,510 --> 00:10:04,600
bad or right and wrong as their general
meaning.

167
00:10:04,600 --> 00:10:08,105
But they do suggest an evaluation in a
particular context.

168
00:10:08,105 --> 00:10:13,970
Let me give you an example of what I mean.
A conservative politician might criticize

169
00:10:13,970 --> 00:10:20,472
her opponent by saying, Wow, his policies
are way too liberal.

170
00:10:20,472 --> 00:10:26,490
Now, by calling them liberal, is that a
criticism?

171
00:10:26,490 --> 00:10:32,700
Well, she intends it as a criticism, but
does the word liberal mean that it's bad?

172
00:10:33,870 --> 00:10:36,450
Not really if you think about it, because
the

173
00:10:36,450 --> 00:10:39,257
opponent might say, i'm proud to be a
liberal.

174
00:10:39,257 --> 00:10:44,050
Being liberal's good, yes it's liberal, so
what?

175
00:10:44,050 --> 00:10:46,365
Yes it's liberal, nothing wrong with that.

176
00:10:46,365 --> 00:10:51,220
The word liberal by itself doesn't mean
that it's bad, even

177
00:10:51,220 --> 00:10:53,933
though the conservative thinks that things
that are liberal are bad.

178
00:10:53,933 --> 00:10:58,900
So that word liberal is not evaluative in
the strict

179
00:10:58,900 --> 00:11:01,810
sense, because it doesn't get defined by
the words good

180
00:11:01,810 --> 00:11:04,178
or bad or right or wrong or should or
should not.

181
00:11:04,178 --> 00:11:08,140
It's only evaluative in the context.

182
00:11:08,140 --> 00:11:13,470
It suggests an evaluation, because of the
assumptions of the speaker, but it doesn't

183
00:11:13,470 --> 00:11:18,840
in and of itself mean that anything is
bad, or good, for that matter.

184
00:11:18,840 --> 00:11:21,245
Because of this difference, we will call
language

185
00:11:21,245 --> 00:11:25,180
evaluative only when it's openly and
literally evaluative.

186
00:11:25,180 --> 00:11:28,540
So that it gets defined in terms of words
like good or

187
00:11:28,540 --> 00:11:34,110
bad or right or wrong, and not when it's
merely contextually evaluative.

188
00:11:34,110 --> 00:11:37,580
That is in the context given the
assumptions of the speaker.

189
00:11:37,580 --> 00:11:41,790
This person needs to be suggesting an
evaluation.

190
00:11:41,790 --> 00:11:45,190
If they're not openly, saying, this is
good or bad

191
00:11:45,190 --> 00:11:50,370
or right or wrong, then, they're not
really, using what language

192
00:11:50,370 --> 00:11:53,430
that we will call, evaluative.

193
00:11:53,430 --> 00:11:56,510
But there're a couple of tricky examples
that are worth bringing up, okay?

194
00:11:56,510 --> 00:11:59,830
You might think, that if you take two

195
00:11:59,830 --> 00:12:03,530
good things, and put them together, it
gets better.

196
00:12:03,530 --> 00:12:06,665
And when you add a bad thing to a good
thing, it makes it worse.

197
00:12:06,665 --> 00:12:09,460
At least, that's the way it usually works.

198
00:12:09,460 --> 00:12:14,365
But notice, that when you say something's
good, that suggests it's good.

199
00:12:14,365 --> 00:12:15,520
But when you say eh,

200
00:12:15,520 --> 00:12:18,820
it's pretty good, then you just added
pretty,

201
00:12:18,820 --> 00:12:21,375
which is something good, to the word good.

202
00:12:21,375 --> 00:12:26,800
But, pretty good, it's not really any
better than good, it might even

203
00:12:26,800 --> 00:12:30,854
be worse, but then you can add a negative
word in the middle.

204
00:12:30,854 --> 00:12:33,165
huh, that was pretty darn good.

205
00:12:33,165 --> 00:12:37,010
Well that means it's very good, so you're
actually taking a negative

206
00:12:37,010 --> 00:12:40,530
word darn and putting it in the middle of
two positive words, pretty

207
00:12:40,530 --> 00:12:43,990
and good, and made something that means
very good.

208
00:12:43,990 --> 00:12:49,330
So you really have to think carefully
about exactly what the language means.

209
00:12:49,330 --> 00:12:52,730
It's not going to be a simple formula of
adding and subtracting

210
00:12:52,730 --> 00:12:55,590
goods and bads to figure out whether the
language is evaluative.

211
00:12:55,590 --> 00:12:59,080
Now another word that's surprising is the
word too.

212
00:13:00,190 --> 00:13:04,860
I like spicy food so when I say, this food
is spicy.

213
00:13:05,930 --> 00:13:08,910
That's good or, at least, it's neutral.

214
00:13:08,910 --> 00:13:12,610
To say it's spicy to me means I'm probably
going to like

215
00:13:12,610 --> 00:13:15,420
it, but notice that if we just add that
little word too.

216
00:13:15,420 --> 00:13:20,275
If I were to say this food is too spicy,
that means it's bad.

217
00:13:20,275 --> 00:13:25,350
The little word too takes a positive
evaluation or sometimes just something

218
00:13:25,350 --> 00:13:31,195
that's neutral and makes it bad.
So the word too is actually a negative

219
00:13:31,195 --> 00:13:36,905
evaluative word, because it turns what was
neutral or positive into something bad.

220
00:13:36,905 --> 00:13:40,860
It moves it in that negative evaluative
direction.

221
00:13:40,860 --> 00:13:43,130
So it's a negative evaluative word.

222
00:13:43,130 --> 00:13:45,700
So is there anything wrong with using
evaluative language?

223
00:13:45,700 --> 00:13:49,090
No, some people seem to think that you

224
00:13:49,090 --> 00:13:51,755
shouldn't evaluate at all that you should
just describe.

225
00:13:51,755 --> 00:13:53,990
But they're just kidding themselves.

226
00:13:53,990 --> 00:13:56,200
Try going through life without

227
00:13:56,200 --> 00:13:58,470
deciding what's good or bad or right or
wrong

228
00:13:58,470 --> 00:13:59,900
or what you ought or ought not to do.

229
00:13:59,900 --> 00:14:04,930
You can't really live your life without
making evaluations at some point.

230
00:14:05,930 --> 00:14:09,610
So it's a mistake to think that evaluation
is always bad.

231
00:14:09,610 --> 00:14:13,913
Of course, when you do evaluate, it's not
like saying Yay, Duke.

232
00:14:15,460 --> 00:14:16,770
You have to give a reason.

233
00:14:16,770 --> 00:14:18,872
So you should think about the standards
that

234
00:14:18,872 --> 00:14:21,960
you're applying and why they apply to this
case.

235
00:14:21,960 --> 00:14:25,520
That's going to be your reason for
evaluating the thing as good or bad.

236
00:14:25,520 --> 00:14:26,950
Because often going to be hard to come

237
00:14:26,950 --> 00:14:29,115
up with the exact standards that you're
applying.

238
00:14:29,115 --> 00:14:31,280
Because people tend to think of things as
good or

239
00:14:31,280 --> 00:14:35,000
bad without getting very specific about
what the standards are.

240
00:14:35,000 --> 00:14:36,520
So you're not always going to be able to

241
00:14:36,520 --> 00:14:38,680
tell people what your standards are, and
when

242
00:14:38,680 --> 00:14:41,039
you ask them, they're not always going to

243
00:14:41,039 --> 00:14:43,180
be able to specify what their standards
are.

244
00:14:43,180 --> 00:14:47,010
But it's still going to be useful
exercise, whenever you make an

245
00:14:47,010 --> 00:14:50,400
evaluation to think about why you think
this

246
00:14:50,400 --> 00:14:52,350
thing is good or bad, or right or wrong.

247
00:14:52,350 --> 00:14:54,435
What are the standards that you're
applying?

248
00:14:54,435 --> 00:14:59,490
And when somebody disagrees with you, to
ask about what their standards are.

249
00:14:59,490 --> 00:15:03,130
So that you can understand where the
disagreement is coming from.

250
00:15:03,130 --> 00:15:07,970
Although evaluation can be very useful and
legitimate, it can also be dangerous.

251
00:15:07,970 --> 00:15:12,950
Because some people use evaluative terms
without reasons.

252
00:15:12,950 --> 00:15:15,250
Let's call that Slanting.

253
00:15:15,250 --> 00:15:19,160
You slant when you use an evaluative word
and

254
00:15:19,160 --> 00:15:23,360
don't give any justification for that use
of the word.

255
00:15:23,360 --> 00:15:27,710
So you might call somebody an idiot or a
queer and you're using

256
00:15:27,710 --> 00:15:31,670
an evaluative word or at least you take it
to be negatively evaluative.

257
00:15:31,670 --> 00:15:35,290
And you haven't given any reason why

258
00:15:35,290 --> 00:15:38,530
there's anything wrong with what you're
calling

259
00:15:38,530 --> 00:15:39,660
that nasty word.

260
00:15:41,170 --> 00:15:43,940
Now, that's slanting if you don't have

261
00:15:43,940 --> 00:15:47,480
any reason and that can be terribly
illegitimate.

262
00:15:47,480 --> 00:15:48,810
When do people do it?

263
00:15:48,810 --> 00:15:51,420
Well they typically do it when they don't
have any reason.

264
00:15:51,420 --> 00:15:53,800
If you don't have any reason for your
evaluation

265
00:15:53,800 --> 00:15:57,195
you just use some nasty name like, you
idiot.

266
00:15:57,195 --> 00:16:01,820
And, so when people start using language
like that.

267
00:16:01,820 --> 00:16:04,080
When they start slanting then

268
00:16:04,080 --> 00:16:07,460
that's a good indication to you as a
critic that

269
00:16:07,460 --> 00:16:10,820
that's the point at which their argument
is probably week.

270
00:16:10,820 --> 00:16:13,740
Their using that kind of language to paper
over

271
00:16:13,740 --> 00:16:17,410
cracks as I put it before in their
argument.

272
00:16:17,410 --> 00:16:20,050
So as to hide what's the real weakness.

273
00:16:20,050 --> 00:16:23,760
So we can to use evaluative language in
arguments, and how it

274
00:16:23,760 --> 00:16:29,510
gets placed at certain points to signal
where the weaknesses, and the strengths,

275
00:16:29,510 --> 00:16:34,790
in the argument are.
So now, what we've got, is we've got

276
00:16:34,790 --> 00:16:39,832
Argument Markers, we've got Assuring
Terms, Guarding

277
00:16:39,832 --> 00:16:45,300
Terms, Discounting Terms, Evaluative
Language.

278
00:16:45,300 --> 00:16:47,680
And in the next few lectures, we're
going to look at a

279
00:16:47,680 --> 00:16:52,100
general technique that looks at all those
different types of language.

280
00:16:52,100 --> 00:16:55,210
And uses those different categories to
analyze

281
00:16:55,210 --> 00:16:58,129
some real passages that we found in
newspapers.

282
00:16:59,200 --> 00:17:02,060
But before that, let's do a few exercises,

283
00:17:02,060 --> 00:17:06,065
just to make sure that you understand
Evaluation.

