1
00:00:02,960 --> 00:00:04,170
Welcome back.

2
00:00:04,170 --> 00:00:07,610
We've covered stages one through three of
argument reconstruction.

3
00:00:07,610 --> 00:00:12,650
Namely, close analysis, get down to
basics, and sharpen

4
00:00:12,650 --> 00:00:17,710
edges.
In this lecture, we'll cover stage four.

5
00:00:17,710 --> 00:00:19,135
Which is organized parts.

6
00:00:19,135 --> 00:00:23,326
because it's not enough to isolate the
parts and figure out what they are.

7
00:00:23,326 --> 00:00:28,020
You need to show how they fit together in
a structure, so that

8
00:00:28,020 --> 00:00:31,480
they work together to support the
conclusion of the argument.

9
00:00:32,560 --> 00:00:34,920
To see how this works, let's start with an
example.

10
00:00:36,370 --> 00:00:37,530
Consider this example.

11
00:00:37,530 --> 00:00:41,030
That fertilizer won't help the roses
bloom, because

12
00:00:41,030 --> 00:00:43,250
there's already a lot of nitrogen in the
soil.

13
00:00:43,250 --> 00:00:48,270
So the fertilizer will make the nitrogen
levels too high.

14
00:00:48,270 --> 00:00:50,920
Of course, so is a conclusion marker.

15
00:00:50,920 --> 00:00:53,170
So one conclusion is that,

16
00:00:53,170 --> 00:00:56,049
the fertilizer will make the nitrogen
levels too high.

17
00:00:57,120 --> 00:00:58,480
And then you might think that one might
have

18
00:00:58,480 --> 00:01:01,650
put the argument into standard form goes
like this.

19
00:01:01,650 --> 00:01:06,640
Premise one is, that fertilizer won't make
the roses bloom.

20
00:01:06,640 --> 00:01:11,340
Premise two is, the nitrogen levels in the
soil are already high.

21
00:01:11,340 --> 00:01:13,930
And then the conclusion is, that the

22
00:01:13,930 --> 00:01:16,815
fertilizer will make the nitrogen levels
too high.

23
00:01:16,815 --> 00:01:18,260
But that

24
00:01:18,260 --> 00:01:20,600
doesn't really make any sense if you think
about it.

25
00:01:20,600 --> 00:01:24,170
How could the fact that the roses won't
bloom, be

26
00:01:24,170 --> 00:01:26,820
a reason to believe that the nitrogen
levels are too high?

27
00:01:28,430 --> 00:01:33,065
This couldn't be a reason for that, so we
must have the wrong structure.

28
00:01:33,065 --> 00:01:36,930
However, there's another argument marker.

29
00:01:36,930 --> 00:01:39,770
This time it's a premise marker.
Because.

30
00:01:40,940 --> 00:01:43,520
And that indicates that the claim that
there's already

31
00:01:43,520 --> 00:01:45,410
a lot of nitrogen in the soil is

32
00:01:45,410 --> 00:01:49,230
a premise, but what's the conclusion for
that premise.

33
00:01:49,230 --> 00:01:53,210
That's supposed to show that the
fertilizer won't make the roses bloom.

34
00:01:55,270 --> 00:01:58,380
So we've missed that part of the
structure.

35
00:01:58,380 --> 00:02:02,200
If we put it in standard form the way we
first thought.

36
00:02:02,200 --> 00:02:04,840
The trick here is that there is really two
conclusions.

37
00:02:05,940 --> 00:02:09,470
One conclusion is that the fertilizer
won't help the roses bloom.

38
00:02:09,470 --> 00:02:14,570
And another conclusion is that the
fertilizer will make the nitrogen levels

39
00:02:14,570 --> 00:02:19,379
too high, but each argument is supposed to
have just one conclusion.

40
00:02:20,410 --> 00:02:23,630
So how are we going to put this into a
structure?

41
00:02:23,630 --> 00:02:27,088
The solution is that there are two
arguments.

42
00:02:27,088 --> 00:02:31,000
One is that the nitrogen levels in the
soil are already high.

43
00:02:31,000 --> 00:02:34,615
Therefore, adding the fertilizer will make
them too high.

44
00:02:34,615 --> 00:02:40,220
And the second argument is that.
Adding the fertilizer will make

45
00:02:40,220 --> 00:02:45,420
the nitrogen levels too high.
Therefore, the fertilizer will not make

46
00:02:45,420 --> 00:02:46,500
the roses bloom.

47
00:02:48,580 --> 00:02:53,590
Now notice that one argument really builds
on the other, because the

48
00:02:53,590 --> 00:02:58,379
conclusion of the first argument is really
a premise in the second argument.

49
00:02:59,440 --> 00:03:02,320
So we can represent them as two separate
arguments,

50
00:03:02,320 --> 00:03:05,230
but we can also put them together in a
chain.

51
00:03:05,230 --> 00:03:08,880
So that the argument says, the nitrogen
levels in

52
00:03:08,880 --> 00:03:13,660
the soil are already high, therefore
adding fertilizer will make

53
00:03:13,660 --> 00:03:15,030
them too high.

54
00:03:15,030 --> 00:03:20,670
And therefore, adding fertilizer will not
help the roses bloom.

55
00:03:22,160 --> 00:03:26,750
Now, if we take that whole structure, and
we try to represent

56
00:03:26,750 --> 00:03:31,180
it in a diagram, and we represent each
premise with a number.

57
00:03:31,180 --> 00:03:33,550
Which is the number that was given it in

58
00:03:33,550 --> 00:03:38,820
the standard form, then we can simply have
premise one.

59
00:03:38,820 --> 00:03:41,630
With an arrow to premise two indicating
that

60
00:03:41,630 --> 00:03:44,860
premise one is a reason for premise two.

61
00:03:44,860 --> 00:03:47,930
And then another arrow going from premise
two to premise

62
00:03:47,930 --> 00:03:51,920
three to indicate that two is a reason for
three.

63
00:03:53,250 --> 00:03:57,980
In a way, we've got two premises and two
conclusions because, that one claim in the

64
00:03:57,980 --> 00:04:01,120
middle, number two, operates as a
conclusion in

65
00:04:01,120 --> 00:04:04,110
the first argument, and premise in the
second argument.

66
00:04:05,220 --> 00:04:07,200
But overall, I hope the diagram makes it

67
00:04:07,200 --> 00:04:11,000
clear why I want to call this a linear
structure.

68
00:04:11,000 --> 00:04:13,330
When you have one premise giving a reason
for

69
00:04:13,330 --> 00:04:17,020
a conclusion, which is then a premise for
another conclusion.

70
00:04:17,020 --> 00:04:21,280
Then they form a line when you diagram
them in the way that I'm proposing.

71
00:04:22,540 --> 00:04:24,270
Arguments can have other structures too.

72
00:04:24,270 --> 00:04:27,820
In particular, sometimes there's more than

73
00:04:27,820 --> 00:04:31,450
one premise associated with a single
conclusion.

74
00:04:31,450 --> 00:04:33,094
And this can happen in two ways.

75
00:04:33,094 --> 00:04:37,070
The first we're going to call the
branching structure.

76
00:04:37,070 --> 00:04:39,480
And the second we're going to call the
joint structure.

77
00:04:40,780 --> 00:04:42,770
Here's an example of the branching
structure.

78
00:04:44,520 --> 00:04:48,185
I'm not going to go to the movie with you
because I don't like horror flicks.

79
00:04:48,185 --> 00:04:50,850
And besides, I'm too busy.

80
00:04:50,850 --> 00:04:57,280
The word because is a premise marker.
So that indicates that the conclusion

81
00:04:57,280 --> 00:04:59,045
is that I'm not going to go to the movie
with you.

82
00:04:59,045 --> 00:05:01,600
And there are two premises.

83
00:05:01,600 --> 00:05:06,000
One is I don't like horror flicks, and the
other is I'm too busy.

84
00:05:07,030 --> 00:05:08,580
Now you might think that, that could just
be

85
00:05:08,580 --> 00:05:11,155
put in the old linear structure that we
already saw.

86
00:05:11,155 --> 00:05:14,080
But then the argument's going to look like
this.

87
00:05:15,770 --> 00:05:19,660
I don't like horror flicks therefore, I'm

88
00:05:19,660 --> 00:05:22,340
too busy, therefore, I'm not going to go
to

89
00:05:22,340 --> 00:05:24,640
the movie.
But wait a minute.

90
00:05:24,640 --> 00:05:28,220
The fact that I don't like horror flicks
doesn't mean I'm too busy.

91
00:05:28,220 --> 00:05:31,550
That doesn't make any sense.
Oh, maybe it's the other way around.

92
00:05:31,550 --> 00:05:35,680
I'm too busy, therefore, I don't like
horror flicks,

93
00:05:35,680 --> 00:05:38,119
therefore, I'm not going to go to that
movie with you.

94
00:05:39,140 --> 00:05:40,210
That didn't make any sense either.

95
00:05:40,210 --> 00:05:42,945
The fact that I'm too busy isn't why I
don't like horror flicks.

96
00:05:42,945 --> 00:05:47,610
The problem is there are two premises
here, but neither one is a reason

97
00:05:47,610 --> 00:05:50,820
for the other, as we saw on the linear
structure.

98
00:05:50,820 --> 00:05:56,490
Instead, in this branching structure, each
premise is operating independently.

99
00:05:56,490 --> 00:05:58,300
There's one argument.

100
00:05:58,300 --> 00:06:02,720
I don't like horror flicks, therefore I'm
not going to go to that movie with you.

101
00:06:02,720 --> 00:06:04,760
There's another argument.

102
00:06:04,760 --> 00:06:08,145
I'm too busy, therefore I'm not going to
go to that movie with you.

103
00:06:08,145 --> 00:06:12,820
And each premise by itself is a sufficient
reason not to go to the movie

104
00:06:12,820 --> 00:06:14,795
with you.
I mean just think about it.

105
00:06:14,795 --> 00:06:18,300
If I wasn't too busy but I didn't like
horror flicks, I wouldn't go to the movie.

106
00:06:18,300 --> 00:06:20,370
But if I liked horror flicks but I was

107
00:06:20,370 --> 00:06:22,410
too busy, then I still wouldn't go to the
movie.

108
00:06:22,410 --> 00:06:26,540
So each premise itself, is enough and they
operate independently.

109
00:06:28,540 --> 00:06:32,620
That's what makes this a branching
structure, instead of a linear structure.

110
00:06:33,860 --> 00:06:37,760
Lets diagram it and you will see why we
call it a branching structure.

111
00:06:37,760 --> 00:06:40,160
One way to diagram it would be simply draw

112
00:06:40,160 --> 00:06:43,424
an arrow between premise one and
conclusion two, and

113
00:06:43,424 --> 00:06:46,400
then there's a separate argument, so you
draw another

114
00:06:46,400 --> 00:06:51,386
arrow from one star, another premise to
conclusion two.

115
00:06:51,386 --> 00:06:55,770
And that's okay, but notice that it
doesn't show

116
00:06:55,770 --> 00:07:01,470
you that both premises are reasons for the
same conclusion.

117
00:07:01,470 --> 00:07:03,120
So to capture that aspect

118
00:07:03,120 --> 00:07:07,180
of the structure, that both premise one
and premise one star support

119
00:07:07,180 --> 00:07:12,210
the same conclusion, namely two, it's
better to diagram it so that.

120
00:07:12,210 --> 00:07:17,770
There's an arrow that runs independently
from both premises to a single

121
00:07:17,770 --> 00:07:21,930
instance of conclusion two, as you see in
the diagram on the screen.

122
00:07:23,540 --> 00:07:25,090
And that should show you why we're calling
it

123
00:07:25,090 --> 00:07:28,250
a branching structure, because it kind of
branches, it looks like

124
00:07:28,250 --> 00:07:29,450
the branches of a tree.

125
00:07:29,450 --> 00:07:32,700
Okay, it doesn't really look like the
branches of a tree, but you get the idea.

126
00:07:32,700 --> 00:07:35,370
We're going to call it a branching
structure.

127
00:07:35,370 --> 00:07:37,940
Next, we have to separate this branching
structure

128
00:07:37,940 --> 00:07:40,112
from what we're going to call the joint
structure.

129
00:07:40,112 --> 00:07:42,820
The difference is that in the branching
structure

130
00:07:42,820 --> 00:07:46,840
the premises provide independent support
for the conclusion.

131
00:07:46,840 --> 00:07:50,540
Whereas in this joint structure, they work
together.

132
00:07:50,540 --> 00:07:53,260
And they're not going to have force
independent of

133
00:07:53,260 --> 00:07:55,820
each other.
It's like the joint in your leg.

134
00:07:55,820 --> 00:08:00,690
Which joins together the calf with the
thigh.

135
00:08:00,690 --> 00:08:03,480
And if you didn't have both, it wouldn't
work very well.

136
00:08:03,480 --> 00:08:06,009
So we're going to call it a joint
structure.

137
00:08:06,009 --> 00:08:06,790
Here's an example.

138
00:08:08,410 --> 00:08:13,360
For my birthday, my wife always gives me
either a sweater or a board game.

139
00:08:13,360 --> 00:08:18,290
This box does not contain a sweater.
So, this time she must have given me a

140
00:08:18,290 --> 00:08:23,700
board game.
Now notice that the argument marker, so,

141
00:08:23,700 --> 00:08:28,385
indicates that the conclusion is this time
she must have given me a board game.

142
00:08:28,385 --> 00:08:30,650
And it's got two premises.

143
00:08:30,650 --> 00:08:32,570
And you might think that they got a

144
00:08:32,570 --> 00:08:35,195
linear structure, and the argument goes
something like this.

145
00:08:35,195 --> 00:08:39,360
My wife always give me a sweater or a
board game.

146
00:08:39,360 --> 00:08:42,350
Therefore, this box does not contain a
sweater.

147
00:08:42,350 --> 00:08:43,199
Therefore,

148
00:08:43,199 --> 00:08:47,520
this time she gave me a board game.
That didn't make any sense, right?

149
00:08:47,520 --> 00:08:50,210
I mean the fact that she always give me a
sweater or a board

150
00:08:50,210 --> 00:08:53,262
game gives me no reason to believe that
this box doesn't contain a sweater.

151
00:08:53,262 --> 00:08:56,442
Well, okay, let's try it again.

152
00:08:56,442 --> 00:09:00,300
Maybe it's a branching structure.

153
00:09:00,300 --> 00:09:02,750
That would mean that the argument looks
like this.

154
00:09:04,430 --> 00:09:07,245
My wife always gives me either a sweater
or a board game.

155
00:09:07,245 --> 00:09:11,270
Therefore, this time she gave me a board
game.

156
00:09:11,270 --> 00:09:14,925
And as a separate argument, this box does
not contain a sweater.

157
00:09:14,925 --> 00:09:19,650
Therefore, this time she must have given
me a board game.

158
00:09:19,650 --> 00:09:21,410
Neither of those arguments makes any
sense.

159
00:09:21,410 --> 00:09:22,940
So it can't be a branching structure.

160
00:09:24,070 --> 00:09:27,330
Instead what we have here is the two
premises working together.

161
00:09:27,330 --> 00:09:32,245
She always gives me either a sweater or a
board game.

162
00:09:32,245 --> 00:09:38,550
And, the second premise, this box does not
contain a sweater.

163
00:09:38,550 --> 00:09:40,409
Those two premises have to work together.

164
00:09:40,409 --> 00:09:46,910
It's only jointly working together that
they can support their conclusion this

165
00:09:46,910 --> 00:09:52,080
time she must have given me a board game.
How can we diagram this joint structure.

166
00:09:54,190 --> 00:09:58,410
We can put, a plus sign between premiss
one and premiss two.

167
00:09:58,410 --> 00:10:01,740
Then draw a line under them to show that
they work together, jointly.

168
00:10:02,820 --> 00:10:05,490
And take a line from that line, and draw
an

169
00:10:05,490 --> 00:10:09,520
arrow down to the conclusion, just like in
the diagram.

170
00:10:09,520 --> 00:10:12,987
And this is what we're going to call the
joint structure.

171
00:10:12,987 --> 00:10:15,150
So, we've seen the linear structure,

172
00:10:15,150 --> 00:10:18,530
the branching structure, and the joint
structure.

173
00:10:18,530 --> 00:10:19,350
And we can

174
00:10:19,350 --> 00:10:22,860
combine more than one of these structures
into a single argument.

175
00:10:22,860 --> 00:10:28,800
To see how to do this, let's just do a
slide variation on the previous example.

176
00:10:28,800 --> 00:10:31,605
My wife always gives me either a sweater
or a board game.

177
00:10:31,605 --> 00:10:35,010
This box does not contain a sweater.

178
00:10:35,010 --> 00:10:37,590
Because it rattles when I shake it.

179
00:10:37,590 --> 00:10:42,690
So, this time she must have given me a
board game.

180
00:10:43,870 --> 00:10:44,530
This argument

181
00:10:44,530 --> 00:10:47,880
combined a linear structure with a joint
structure.

182
00:10:49,470 --> 00:10:51,200
There are two argument markers.

183
00:10:51,200 --> 00:10:55,550
One's a conclusion marker, so and that
indicates that the eventual conclusion

184
00:10:55,550 --> 00:10:57,395
is that she must of given me a board game
this time.

185
00:10:57,395 --> 00:11:02,780
But there's also that new word because,
which indicates that the fact

186
00:11:02,780 --> 00:11:06,970
that it rattles when I shake it, means
that it's not a sweater.

187
00:11:06,970 --> 00:11:09,760
So the first stage if the argument in
standard form

188
00:11:09,760 --> 00:11:10,487
looks like this.

189
00:11:10,487 --> 00:11:14,900
Premise one, this box rattles when I shake
it.

190
00:11:14,900 --> 00:11:18,290
Therefore, conclusion, this box does not
contain a sweater.

191
00:11:19,860 --> 00:11:25,040
Stage two says this box does not contain a
sweater.

192
00:11:25,040 --> 00:11:28,082
My wife always gives me either a sweater
or a board game.

193
00:11:28,082 --> 00:11:33,500
So the conclusion, this time she must have
given me a board game.

194
00:11:34,890 --> 00:11:38,380
And of course, the conclusion of that
first little argument.

195
00:11:38,380 --> 00:11:41,340
Is identical with the premise of the
second argument.

196
00:11:41,340 --> 00:11:44,250
So we can put them together into a chain.

197
00:11:44,250 --> 00:11:50,260
We can say, this box rattles when I shake
it, so it must not contain a sweater.

198
00:11:50,260 --> 00:11:53,690
My wife always gives me a sweater or a
board game

199
00:11:53,690 --> 00:11:58,010
so, this time she must have given me a
board game.

200
00:11:58,010 --> 00:12:00,080
That's how we get a linear structure,

201
00:12:00,080 --> 00:12:03,230
combined with a joint structure.

202
00:12:03,230 --> 00:12:06,150
And we can use our diagram methods to

203
00:12:06,150 --> 00:12:08,440
diagram this argument the same way we did
before.

204
00:12:08,440 --> 00:12:14,730
We simply start with premise one, the box
rattles when I shake it, drawn

205
00:12:14,730 --> 00:12:20,100
an arrow down to its conclusion, namely,
the box does not contain a sweater.

206
00:12:20,100 --> 00:12:25,120
That's two, and then we show that those
are joint by adding a

207
00:12:25,120 --> 00:12:31,210
plus premise three, mainly, my wife always
gives me either a sweater or a board game.

208
00:12:31,210 --> 00:12:33,510
Draw a line under them and an arrow from

209
00:12:33,510 --> 00:12:38,290
those two together, down to the eventual
conclusion namely, four.

210
00:12:38,290 --> 00:12:42,500
That this time she must have given me a
board game.

211
00:12:43,900 --> 00:12:50,300
The fact that the top arrow goes from
premise one to two, but does not

212
00:12:50,300 --> 00:12:54,260
go from premise one to three, indicates
that, that premise is

213
00:12:54,260 --> 00:12:58,290
a reason for two, but is not a reason for
three.

214
00:12:58,290 --> 00:13:00,650
So when you use this method to diagram
arguments,

215
00:13:00,650 --> 00:13:03,380
you have to be careful where you draw the
arrows.

216
00:13:03,380 --> 00:13:07,270
And draw them only where there really is a
rational connection.

217
00:13:07,270 --> 00:13:10,540
That is where one claim is being presented
as a

218
00:13:10,540 --> 00:13:14,870
reason for that particular claim that the
arrow's pointing towards.

219
00:13:14,870 --> 00:13:15,420
Now almost

220
00:13:15,420 --> 00:13:20,460
all arguments can be diagrammed using
these three simple structures, that is the

221
00:13:20,460 --> 00:13:24,290
linear structure, the branching structure,
the joint

222
00:13:24,290 --> 00:13:27,378
structure, and some combination of those
three.

223
00:13:27,378 --> 00:13:30,610
You can add more premises, because you can

224
00:13:30,610 --> 00:13:33,920
always add one plus two plus three plus
four.

225
00:13:33,920 --> 00:13:37,225
If there are four premises operating
together in a joint structure.

226
00:13:37,225 --> 00:13:40,720
And you can add extra

227
00:13:40,720 --> 00:13:44,470
arrows if you have a branch with more than
two branches, so

228
00:13:44,470 --> 00:13:48,860
you can cover a lot of arguments using
these kinds of diagrams.

229
00:13:48,860 --> 00:13:51,970
The method can be described in general
like this.

230
00:13:51,970 --> 00:13:57,760
You start by identifying the premises and
the conclusions, and you number them.

231
00:13:57,760 --> 00:13:59,420
So that you can just have numbers instead
of

232
00:13:59,420 --> 00:14:01,425
having to write out the whole sentence on
the diagram.

233
00:14:01,425 --> 00:14:05,820
Then when they work together, you put a
plus sign between them and

234
00:14:05,820 --> 00:14:07,530
draw a line under it to indicate that

235
00:14:07,530 --> 00:14:09,805
they're working together, they're
functioning as a group.

236
00:14:09,805 --> 00:14:14,650
Then, you draw an arrow from the claims
that

237
00:14:14,650 --> 00:14:17,830
are reasons to the claims that they are
reasons for.

238
00:14:19,050 --> 00:14:22,430
And then you move them around on the
diagram so that they form

239
00:14:22,430 --> 00:14:26,810
a line when it's a linear structure, and
branches when it's a branching structure.

240
00:14:26,810 --> 00:14:30,890
That'll be easy to rearrange them.
So as to show how all of the

241
00:14:30,890 --> 00:14:33,120
different premises, and conclusions work

242
00:14:33,120 --> 00:14:35,850
together in a single argumentative
structure.

243
00:14:37,540 --> 00:14:42,660
That's going to be enough to accomplish
this stage of reconstruction, namely to

244
00:14:42,660 --> 00:14:48,900
organize the parts and show how they work
together in the overall argument.

