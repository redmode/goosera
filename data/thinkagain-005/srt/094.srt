1
00:00:02,870 --> 00:00:02,870
[BLANK_AUDIO]

2
00:00:02,870 --> 00:00:04,310
.
Well, welcome back.

3
00:00:04,310 --> 00:00:07,360
I've really missed you over the last
couple of weeks, when you've been off with

4
00:00:07,360 --> 00:00:07,690
[INAUDIBLE]

5
00:00:07,690 --> 00:00:12,300
learning about propositional logic, and
then categorical logic.

6
00:00:13,440 --> 00:00:15,170
And I hope that you enjoyed that.

7
00:00:15,170 --> 00:00:16,990
It's very important.

8
00:00:16,990 --> 00:00:20,340
But in the next few weeks, we're going to
turn to a different kind of argument.

9
00:00:20,340 --> 00:00:25,140
Because, that type of logic is appropriate
for deductive arguments,

10
00:00:25,140 --> 00:00:30,010
but what we're going to study instead is
inductive arguments.

11
00:00:30,010 --> 00:00:32,890
Now deductive arguments are given

12
00:00:32,890 --> 00:00:35,190
quite often, throughout life.

13
00:00:35,190 --> 00:00:37,750
But inductive arguments are probably even
more common.

14
00:00:38,830 --> 00:00:42,660
Whenever you want to figure out who
committed a crime.

15
00:00:42,660 --> 00:00:44,910
We're talking about an inference to the
best explanation.

16
00:00:46,240 --> 00:00:50,170
Then there are arguments from analogy,
which happen quite often.

17
00:00:50,170 --> 00:00:53,900
Just think about all the analogies that
you run into in life.

18
00:00:53,900 --> 00:00:56,670
And then we'll see statistical
generalizations.

19
00:00:56,670 --> 00:00:57,970
Well, just think about all the

20
00:00:57,970 --> 00:01:03,120
polls in politics these days.
And statistical applications.

21
00:01:03,120 --> 00:01:06,350
Whenever you want to know how those
generalizations

22
00:01:06,350 --> 00:01:08,140
tell you something about a particular
person.

23
00:01:08,140 --> 00:01:12,250
And then we'll turn to causal reasoning.

24
00:01:12,250 --> 00:01:15,030
Well, causes are crucial to everything,
from science

25
00:01:15,030 --> 00:01:17,160
to figuring out what made your car stop.

26
00:01:18,650 --> 00:01:22,180
And, decision making and probability.

27
00:01:22,180 --> 00:01:23,010
So, we got a lot of

28
00:01:23,010 --> 00:01:24,500
things to cover.

29
00:01:24,500 --> 00:01:26,250
And the first thing that we need to do,

30
00:01:26,250 --> 00:01:30,360
is to see the difference between inductive
and deductive arguments.

31
00:01:30,360 --> 00:01:31,730
Because you've been focused for a couple

32
00:01:31,730 --> 00:01:35,160
of weeks with Ron about deductive
arguments.

33
00:01:35,160 --> 00:01:39,170
And you need to understand what's the
difference between those arguments, and

34
00:01:39,170 --> 00:01:42,320
the ones that we're going to study over
the next couple of weeks.

35
00:01:43,390 --> 00:01:47,500
In order to see that difference, I'm
going to show you a little video.

36
00:01:47,500 --> 00:01:48,030
It's a video I

37
00:01:48,030 --> 00:01:52,290
made a few years ago, so you might notice
that I was a little younger back then.

38
00:01:52,290 --> 00:01:53,610
Okay, fair enough.

39
00:01:53,610 --> 00:01:57,280
But, I think this video will help you see.

40
00:01:57,280 --> 00:02:01,340
What the difference is between inductive
arguments and deductive arguments.

41
00:02:03,110 --> 00:02:08,710
Rainy days and Mondays always get me down.

42
00:02:08,710 --> 00:02:13,120
>> The sun will come out tomorrow, bet
your bottom dollar that

43
00:02:13,120 --> 00:02:15,578
tomorrow there'll be sun, tomorrow,
tomorrow.

44
00:02:18,180 --> 00:02:21,245
>> Wait a minute, wait a minute.
How do you know that?

45
00:02:21,245 --> 00:02:23,950
>> I heard a weather report.
>> So what.

46
00:02:23,950 --> 00:02:25,225
They get it wrong all the time.

47
00:02:25,225 --> 00:02:27,680
>> Well, maybe the sun won't come out
tomorrow.

48
00:02:27,680 --> 00:02:30,470
But it will come up tomorrow.
>> So you say.

49
00:02:30,470 --> 00:02:31,400
But is that really true?

50
00:02:33,120 --> 00:02:36,262
How many of you agree that the sun'll come
up tomorrow morning?

51
00:02:36,262 --> 00:02:38,972
>> Of course.
>> Definitely.

52
00:02:38,972 --> 00:02:42,085
>> Absolutely yes.
>> Everybody knows that.

53
00:02:42,085 --> 00:02:43,770
>> Okay.

54
00:02:43,770 --> 00:02:47,320
So you all agree with her.
But do you really know that it's true?

55
00:02:47,320 --> 00:02:50,300
How could you show that the sun will come
up tomorrow?

56
00:02:51,380 --> 00:02:52,505
>> I don't know I'm just kid.

57
00:02:52,505 --> 00:02:55,350
>> Wait, I didn't, I didn't mean to get
you all upset.

58
00:02:56,830 --> 00:02:58,580
But there really is an issue here.

59
00:02:58,580 --> 00:03:03,580
The question is, how can you prove that
the sun will come up tomorrow morning?

60
00:03:03,580 --> 00:03:06,480
Well here's one way that people use.

61
00:03:06,480 --> 00:03:08,780
The conclusion is that the sun will come
up tomorrow.

62
00:03:10,040 --> 00:03:13,290
The premise is, yesterday the sun came up.

63
00:03:13,290 --> 00:03:16,110
The second premise, the day before that
the sun came up.

64
00:03:16,110 --> 00:03:18,400
The third premise the day before that the
sun came up.

65
00:03:18,400 --> 00:03:20,200
Then the day before that, the day before
that,

66
00:03:20,200 --> 00:03:21,370
the day before that, the day before that,
the

67
00:03:21,370 --> 00:03:23,790
day before that, the day before that Well
that

68
00:03:23,790 --> 00:03:26,810
can get awfully long so we'll shorten the
argument.

69
00:03:26,810 --> 00:03:30,030
The conclusion is still that the sun will
come up tomorrow.

70
00:03:30,030 --> 00:03:35,270
But the premises are just the sun came up
yesterday, and the sun came up every

71
00:03:35,270 --> 00:03:43,939
day before that for an awfully long time.
Now, is the argument valid, yes or no?

72
00:03:50,690 --> 00:03:52,410
Right.
It's not valid.

73
00:03:53,570 --> 00:03:56,330
An argument is valid if and only if it's
not

74
00:03:56,330 --> 00:03:59,950
possible for the premises to be true and
the conclusion false.

75
00:03:59,950 --> 00:04:03,070
But it is possible for these premises to
be true.

76
00:04:03,070 --> 00:04:08,170
And this conclusion to be false.
Can you imagine how that would happen.

77
00:04:08,170 --> 00:04:10,530
One way, would be if a meteor struck the
Earth

78
00:04:10,530 --> 00:04:13,670
in the middle of the night, and stopped it
from spinning.

79
00:04:13,670 --> 00:04:15,440
Then the sun wouldn't rise tomorrow.

80
00:04:15,440 --> 00:04:15,700
[MUSIC]

81
00:04:15,700 --> 00:04:19,375
>> Now you're really freaking me out.

82
00:04:19,375 --> 00:04:23,510
>> But don't worry, that's extremely
unlikely.

83
00:04:23,510 --> 00:04:25,730
However, it's possible.

84
00:04:25,730 --> 00:04:27,910
And that shows that the argument is not
valid.

85
00:04:29,520 --> 00:04:32,470
The next question is, is this argument any
good?

86
00:04:33,490 --> 00:04:36,690
An argument's good if it serves its
purpose.

87
00:04:36,690 --> 00:04:40,760
The purpose of this argument, and many
other arguments, is to provide reasons for

88
00:04:40,760 --> 00:04:41,490
its conclusion.

89
00:04:41,490 --> 00:04:46,600
So, do the premises in this argument
provide reasons for its conclusion?

90
00:04:47,660 --> 00:04:51,710
Yes, some philosophers deny this, but most
people think

91
00:04:51,710 --> 00:04:54,270
that these premises provide good reasons
for its conclusion.

92
00:04:54,270 --> 00:04:55,600
So let's assume that for now.

93
00:04:56,870 --> 00:04:58,220
What does that show?

94
00:04:58,220 --> 00:05:03,230
That shows that some arguments can be
good, even if they're not valid.

95
00:05:03,230 --> 00:05:06,310
But don't generalize too quickly.

96
00:05:06,310 --> 00:05:09,990
There are other arguments that are no good
because they're invalid.

97
00:05:09,990 --> 00:05:11,040
Take bug for example.

98
00:05:13,340 --> 00:05:16,525
>> Is it true that every sophomore is a
student.

99
00:05:16,525 --> 00:05:17,560
>> Yeah.

100
00:05:17,560 --> 00:05:19,601
>> Are you a sophomore?

101
00:05:19,601 --> 00:05:21,080
>> No.
>> So you are a student, right?

102
00:05:22,310 --> 00:05:24,100
>> Wrong, I am a student.

103
00:05:24,100 --> 00:05:25,790
>> Well then what's wrong with the
argument.

104
00:05:25,790 --> 00:05:27,350
Every sophomore is a student.

105
00:05:27,350 --> 00:05:30,470
You're not a sophomore, so you're not a
student.

106
00:05:30,470 --> 00:05:32,580
>> Well both the premises are true, but

107
00:05:32,580 --> 00:05:35,090
the conclusion's false so the argument
must be invalid.

108
00:05:35,090 --> 00:05:38,717
>> So you're telling me what makes that
argument

109
00:05:38,717 --> 00:05:40,966
bad is that it's invalid.
>> Yep.

110
00:05:40,966 --> 00:05:41,619
>> Good job.

111
00:05:42,760 --> 00:05:44,960
Now we've got a puzzle.

112
00:05:44,960 --> 00:05:48,090
Some arguments are bad, because they're
invalid.

113
00:05:48,090 --> 00:05:51,900
Whereas other arguments are good even
though they're invalid.

114
00:05:51,900 --> 00:05:54,815
How can that be?
Simple.

115
00:05:54,815 --> 00:05:58,780
There're two kinds of arguments, deductive
and inductive.

116
00:05:58,780 --> 00:06:01,900
Deductive arguments are bad when they're
invalid.

117
00:06:01,900 --> 00:06:04,550
Inductive arguments can be good,

118
00:06:04,550 --> 00:06:09,800
even though they are invalid, why?
Because deductive arguments are intended

119
00:06:09,800 --> 00:06:15,380
to be valid, where as inductive arguments
are not intended to be valid.

120
00:06:15,380 --> 00:06:17,110
The crucial point is that there are

121
00:06:17,110 --> 00:06:20,369
different kinds of standards for
evaluating arguments.

122
00:06:21,490 --> 00:06:25,980
Deductive standards ask whether an
argument is valid or invalid.

123
00:06:25,980 --> 00:06:30,170
Inductive standards ask whether an
argument is strong or weak.

124
00:06:31,495 --> 00:06:34,330
There're several important differences
between these standards.

125
00:06:34,330 --> 00:06:38,419
First, deductive validity is all or
nothing.

126
00:06:39,770 --> 00:06:41,900
An argument is either valid or not.

127
00:06:41,900 --> 00:06:44,362
It can't be partly valid or a little
valid,

128
00:06:44,362 --> 00:06:46,660
any more than a woman can be a little
pregnant.

129
00:06:47,730 --> 00:06:51,670
Inductive strength and contrast comes in
degrees.

130
00:06:53,100 --> 00:06:56,760
An argument is stronger when it gives
more, and better

131
00:06:56,760 --> 00:06:59,420
reasons for it's conclusion.

132
00:06:59,420 --> 00:07:04,240
So an argument can be very weak, when the
reasons it gives for its conclusion are

133
00:07:04,240 --> 00:07:06,770
very weak or can be moderately strong when

134
00:07:06,770 --> 00:07:09,220
it gives moderately strong reasons for its
conclusion.

135
00:07:09,220 --> 00:07:13,880
Or can be very strong when it gives very
strong reasons for its conclusion.

136
00:07:13,880 --> 00:07:15,900
Since inductive strength comes in degrees,
we

137
00:07:15,900 --> 00:07:19,100
can't simply ask whether an argument is
strong.

138
00:07:19,100 --> 00:07:21,910
We need to ask whether it is strong
enough.

139
00:07:21,910 --> 00:07:25,180
That depends on the context and the values
at stake.

140
00:07:25,180 --> 00:07:26,468
Psyched about your cooking lesson?

141
00:07:26,468 --> 00:07:28,360
>> Yeah!

142
00:07:28,360 --> 00:07:30,310
>> One of the most important things in
cooking is

143
00:07:30,310 --> 00:07:33,320
to make sure that whatever it is you're
cooking is done.

144
00:07:35,180 --> 00:07:37,020
When people cook cakes.

145
00:07:37,020 --> 00:07:41,030
The way they normally test it for being
done, is they take a straw

146
00:07:41,030 --> 00:07:43,880
or a piece of bamboo and they stick it in
the middle of the cake.

147
00:07:43,880 --> 00:07:46,800
If it comes up with raw dough it's not
done.

148
00:07:46,800 --> 00:07:50,080
But if it comes up clean like this that
means it's done.

149
00:07:51,360 --> 00:07:56,460
Well, at least in that spot, but you
want to make sure that it's done.

150
00:07:56,460 --> 00:08:01,430
Throughout the whole cake, then you have
to test other spots as well.

151
00:08:07,720 --> 00:08:08,795
>> Whoa!
What are you doing?

152
00:08:08,795 --> 00:08:09,295
>>

153
00:08:11,180 --> 00:08:12,880
So much for that.

154
00:08:12,880 --> 00:08:17,290
Now, when you're cooking turkey, you could
use the same method.

155
00:08:17,290 --> 00:08:19,310
You could stick in a straw or a piece

156
00:08:19,310 --> 00:08:22,900
of bamboo, and see whether any pink juice
comes out.

157
00:08:22,900 --> 00:08:25,610
If it comes out pink, then it's not done.

158
00:08:25,610 --> 00:08:29,060
But if out comes out clean, then the
turkey's done.

159
00:08:32,290 --> 00:08:34,499
See any pink juice?
>>Nope.

160
00:08:34,499 --> 00:08:36,710
>> Then it must be done.
Let's try a

161
00:08:39,470 --> 00:08:39,970
piece.

162
00:08:41,810 --> 00:08:42,310
There.

163
00:08:47,120 --> 00:08:49,170
But you wouldn't want to do that with
turkey,

164
00:08:49,170 --> 00:08:53,060
because you can get very sick from
uncooked turkey.

165
00:08:53,060 --> 00:08:54,210
That's why.

166
00:08:54,210 --> 00:08:57,530
Most of the time they build a pop-up meat
thermometer right

167
00:08:57,530 --> 00:09:01,472
into the turkey, to make sure that nobody
eats uncooked turkey.

168
00:09:01,472 --> 00:09:01,472
>>

169
00:09:01,472 --> 00:09:02,860
[SOUND]

170
00:09:02,860 --> 00:09:04,530
>> But this is not a cooking class.

171
00:09:05,970 --> 00:09:08,600
The point here is simply that, whether an

172
00:09:08,600 --> 00:09:12,380
argument is strong enough depends on
what's at stake.

173
00:09:12,380 --> 00:09:16,710
When there's a lot to lose, we demand
better reasons and stronger arguments.

174
00:09:17,800 --> 00:09:23,080
Context is also important in another way.
Additional information from the

175
00:09:23,080 --> 00:09:28,840
context can weaken an inductive argument.
That is, it can change it from strong

176
00:09:28,840 --> 00:09:32,820
to weak.
In technical language, the inductive

177
00:09:32,820 --> 00:09:37,900
standard of strength is called defeasible.
Or, non-monotonic.

178
00:09:37,900 --> 00:09:44,360
In contrast, the deductive standard of
validity is indefeasible, or monotonic.

179
00:09:44,360 --> 00:09:47,260
That means, no matter what premises you
add

180
00:09:47,260 --> 00:09:50,360
to a valid argument, it will still be
valid.

181
00:09:50,360 --> 00:09:51,330
Go ahead, try.

182
00:09:51,330 --> 00:09:53,920
Here's an example.
If Joe

183
00:09:53,920 --> 00:09:56,380
is a sophomore, then Joe is a student.

184
00:09:56,380 --> 00:09:59,070
Joe is not a student, so Joe is not a
sophomore.

185
00:10:00,590 --> 00:10:05,000
Add any premise you want to that argument,
and it will still be valid.

186
00:10:05,000 --> 00:10:09,380
Which shows that the deductive standard of
validity is indefeasible.

187
00:10:09,380 --> 00:10:13,560
In contrast, the inductive standard of
strength.

188
00:10:13,560 --> 00:10:14,820
Is defeasible.

189
00:10:14,820 --> 00:10:16,880
If you add additional premises or
additional

190
00:10:16,880 --> 00:10:19,930
information it can make a strong argument
weak.

191
00:10:21,800 --> 00:10:24,970
Consider a courtroom.
>> I'm sure it was him.

192
00:10:26,530 --> 00:10:28,505
He was only ten feet away from me, I saw
him do it.

193
00:10:28,505 --> 00:10:31,698
>> I'm sure it was him, I saw him with
my own eyes.

194
00:10:31,698 --> 00:10:32,740
>> Yes.

195
00:10:32,740 --> 00:10:35,480
>> I'd like to cross examine this
witness.

196
00:10:35,480 --> 00:10:38,910
Are you positive that it was the defendant
sitting right there?

197
00:10:38,910 --> 00:10:41,670
Or could it have been his identical twin

198
00:10:41,670 --> 00:10:44,015
brother, who is entering the court room
right now?

199
00:10:44,015 --> 00:10:44,515
>>

200
00:10:48,730 --> 00:10:50,655
It could have been.
I can't tell them apart.

201
00:10:50,655 --> 00:10:55,310
>> Now I would like to recall the first
witness.

202
00:10:55,310 --> 00:10:57,210
And let me remind you that you're still
under oath.

203
00:10:58,700 --> 00:11:01,205
Can you tell the difference between those
two gentlemen?

204
00:11:01,205 --> 00:11:03,645
>> No, they look like the same person to
me.

205
00:11:03,645 --> 00:11:04,145
>>

206
00:11:05,870 --> 00:11:08,640
Now there's a reasonable doubt, isn't
there?

207
00:11:08,640 --> 00:11:11,960
What was very strong evidence, becomes
much weaker when

208
00:11:11,960 --> 00:11:14,850
we add the new evidence about the
identical twin.

209
00:11:14,850 --> 00:11:18,680
That is an example of the defeasibility of
inductive standards.

210
00:11:18,680 --> 00:11:22,300
Although tricky, it's important to
classify arguments as inductive

211
00:11:22,300 --> 00:11:26,230
or deductive, because it affects whether
they're good or bad.

212
00:11:27,330 --> 00:11:31,290
An inductive argument might not be valid,
but it can still be good.

213
00:11:31,290 --> 00:11:34,990
For example, the sun came up yesterday.

214
00:11:34,990 --> 00:11:37,310
It came up every day for thousands of
years.

215
00:11:37,310 --> 00:11:39,570
Therefore, it will come up tomorrow.

216
00:11:40,760 --> 00:11:42,420
That's a good argument, even though it's

217
00:11:42,420 --> 00:11:44,920
invalid, because it's not intended to be
valid.

218
00:11:46,250 --> 00:11:49,070
This is just one instance of a general
rule that you

219
00:11:49,070 --> 00:11:53,770
shouldn't criticize something for not
being what it's not suppose to be.

220
00:11:53,770 --> 00:11:56,480
This book can be a good book,

221
00:11:56,480 --> 00:11:59,130
even though, it's no good as a frisbee.

222
00:12:00,230 --> 00:12:04,010
Similarly an argument can be a good
argument.

223
00:12:04,010 --> 00:12:07,420
Even if it's not valid, it's not intended
to be valid.

224
00:12:07,420 --> 00:12:09,420
That is, if it's an inductive argument.

225
00:12:10,600 --> 00:12:15,430
This point undermines many common mistakes
about inductive arguments.

226
00:12:15,430 --> 00:12:18,260
Many people think that inductive arguments
are somehow

227
00:12:18,260 --> 00:12:21,740
inferior to deductive arguments because
they're not valid.

228
00:12:21,740 --> 00:12:24,660
But that can't be right, because it's easy
to take

229
00:12:24,660 --> 00:12:28,690
any inductive argument and turn it into a
deductive argument.

230
00:12:28,690 --> 00:12:34,110
For example, most of Joe's friends are
seniors, so Joe must be a senior too.

231
00:12:35,260 --> 00:12:40,090
Well, we can make that argument valid
simply by adding a conditional premise.

232
00:12:40,090 --> 00:12:44,070
If most of Joe's friends are seniors then
Joe is a senior too.

233
00:12:45,090 --> 00:12:46,960
But that just shifts all the doubts

234
00:12:46,960 --> 00:12:51,740
about the inductive argument.
Into doubts about the conditional premise.

235
00:12:51,740 --> 00:12:57,940
So it hasn't really made us more sure of
whether Joe is a senior.

236
00:12:57,940 --> 00:13:02,250
Our definition of induction also
undermines a second common mistake.

237
00:13:02,250 --> 00:13:05,670
Many people believe that induction always
takes

238
00:13:05,670 --> 00:13:08,950
us from the particular to the general.

239
00:13:08,950 --> 00:13:12,460
But no matter how many people say that,
it's just plain wrong.

240
00:13:12,460 --> 00:13:16,390
Inductive generalization is one kind of
inductive argument.

241
00:13:16,390 --> 00:13:18,470
But it's not the only kind.

242
00:13:18,470 --> 00:13:22,400
In fact, we are going to study five
different types of inductive arguments.

243
00:13:23,430 --> 00:13:26,430
To see the differences between these
arguments.

244
00:13:26,430 --> 00:13:28,160
Let's do a simple experiment.

245
00:13:33,290 --> 00:13:35,770
Excuse me, we're testing a new type of
lemonade here.

246
00:13:35,770 --> 00:13:36,525
Would you help us out?

247
00:13:36,525 --> 00:13:37,147
>> Sure.

248
00:13:37,147 --> 00:13:40,215
>> Thank you.
first, could you try one from that cup?

249
00:13:40,215 --> 00:13:45,390
>> That's pretty good.
>> Great.

250
00:13:45,390 --> 00:13:47,570
Glad to hear it.
Now, could you try the second cup please?

251
00:13:52,420 --> 00:13:53,345
>> Oh, that's terrible.

252
00:13:53,345 --> 00:13:55,275
>>Your reaction suggests that you don't
like that.

253
00:13:55,275 --> 00:13:58,165
>>Well, obviously you put dish washing
liquid in there.

254
00:13:58,165 --> 00:14:01,525
>>How do you know that?
>>Well, it tastes like soap.

255
00:14:01,525 --> 00:14:03,545
>>Is that why you spit it out?

256
00:14:03,545 --> 00:14:06,115
>>Well, I didn't spit out the first one
did I?

257
00:14:06,115 --> 00:14:09,160
>> You might be interested to know you
agree

258
00:14:09,160 --> 00:14:12,360
with over 90% of the people we tested so
far.

259
00:14:12,360 --> 00:14:15,766
They all like our lemonade better than
dish washing liquid.

260
00:14:15,766 --> 00:14:17,640
>> That's great.
>> And if you take

261
00:14:17,640 --> 00:14:20,060
it back to your dorm and try this test

262
00:14:20,060 --> 00:14:22,845
on your dorm, I bet they'll like our
lemonade too.

263
00:14:22,845 --> 00:14:24,770
>> Well of course they would.

264
00:14:24,770 --> 00:14:26,630
Listen this is stupid, I'm out of here.

265
00:14:26,630 --> 00:14:32,480
>> This experiment would be stupid, if

266
00:14:32,480 --> 00:14:35,060
it were intended to show something about
lemonade.

267
00:14:35,060 --> 00:14:36,580
But all it's supposed to show is the

268
00:14:36,580 --> 00:14:40,260
differences among the different kinds of
inductive arguments.

269
00:14:41,750 --> 00:14:42,880
Our next step will

270
00:14:42,880 --> 00:14:46,310
be to look at each of these kinds of
induction in more detail.

