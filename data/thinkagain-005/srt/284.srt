1
00:00:03,240 --> 00:00:07,640
As you probably noticed already, these
arguments are not valid yet.

2
00:00:07,640 --> 00:00:12,770
So, we need to go to the next stage, and
fill in the gaps with suppressed premises.

3
00:00:13,930 --> 00:00:17,270
Let's do that now.
Let's start with the BLM's argument.

4
00:00:18,470 --> 00:00:21,700
Is the argument from 1 to 2 valid?

5
00:00:21,700 --> 00:00:26,000
Is it possible that Conoco has a lease,

6
00:00:26,000 --> 00:00:27,775
but it doesn't have a valid right to
drill?

7
00:00:27,775 --> 00:00:29,170
Well,

8
00:00:29,170 --> 00:00:32,147
sure.
And so, that argument's not valid.

9
00:00:32,147 --> 00:00:35,210
What do we need to add, to make it valid?

10
00:00:35,210 --> 00:00:39,330
Well, we can add a very simple suppressed
premise.

11
00:00:39,330 --> 00:00:42,410
If Conoco does have a lease then it has a
valid right to drill.

12
00:00:42,410 --> 00:00:47,180
If you add that to that premise that they
do have a lease, then it'll

13
00:00:47,180 --> 00:00:51,100
be a valid argument to the conclusion that
they have a valid right to drill.

14
00:00:51,100 --> 00:00:54,234
Next, is the argument from

15
00:00:54,234 --> 00:00:56,126
2 and 3 to 4 valid?

16
00:00:56,126 --> 00:01:00,470
Yes, because it's not possible that Conoco
has a valid right to drill.

17
00:01:00,470 --> 00:01:02,800
And also, if they have a valid right to

18
00:01:02,800 --> 00:01:06,240
drill, then the BLM must allow them to
drill.

19
00:01:06,240 --> 00:01:09,780
And it not be true that the BLM must allow
them to drill.

20
00:01:09,780 --> 00:01:15,450
So, we can take this part of the argument.
Which is really the BLM's argument.

21
00:01:15,450 --> 00:01:19,210
But it's in Redford's passage.
And reconstruct it like this.

22
00:01:19,210 --> 00:01:21,550
Conoco has a lease.

23
00:01:21,550 --> 00:01:24,340
If they have a lease, then they have a
valid right to drill.

24
00:01:24,340 --> 00:01:26,350
So they have a valid right to drill.

25
00:01:26,350 --> 00:01:30,540
And if they have a valid right to drill,
then the BLM has to allow them to drill.

26
00:01:30,540 --> 00:01:32,983
So the BLM has to allow them to drill.

27
00:01:32,983 --> 00:01:36,410
Now we're ready for Redford's own
argument.

28
00:01:36,410 --> 00:01:39,480
His goal is to refute the premise that we
had to

29
00:01:39,480 --> 00:01:43,990
add to the BLM argument in order to make
that argument valid.

30
00:01:43,990 --> 00:01:44,220
In the

31
00:01:44,220 --> 00:01:45,950
previous lecture, we saw that a lot

32
00:01:45,950 --> 00:01:49,720
of people suppress premises that are
questionable.

33
00:01:49,720 --> 00:01:53,170
And Redford wants to show how questionable
this one is.

34
00:01:53,170 --> 00:01:56,870
He's not going to let them get away with
that old trick.

35
00:01:56,870 --> 00:02:01,820
Starting with 1 and 2, is the argument
from 1 to 2 valid?

36
00:02:03,190 --> 00:02:07,850
No, because it's possible that the premise
is true and the conclusion false.

37
00:02:09,730 --> 00:02:12,750
So what do we need to add to make it
valid?

38
00:02:12,750 --> 00:02:14,320
Well, what we need to add is suppressed
premise.

39
00:02:15,600 --> 00:02:18,259
And that suppressed premise basically
says, no

40
00:02:18,259 --> 00:02:23,105
lease that is made without sufficient
environmental study.

41
00:02:23,105 --> 00:02:28,580
And public input is valid.
Or conveys a valid right to drill.

42
00:02:28,580 --> 00:02:35,272
Then, we have a valid argument from that
premise plus 1 to the conclusion

43
00:02:35,272 --> 00:02:39,770
2 that none of Conoco's leases give it a
valid right to drill.

44
00:02:39,770 --> 00:02:48,376
Next comes the step from 3 and 4 to 5.
Is that valid?

45
00:02:48,376 --> 00:02:49,218
No.

46
00:02:49,218 --> 00:02:53,480
3 and 4 say that they conducted an
abbreviated review and

47
00:02:53,480 --> 00:02:56,830
they didn't look at drilling on lands and
other federal leases.

48
00:02:58,060 --> 00:03:00,470
But those might be true and

49
00:03:00,470 --> 00:03:03,900
it still not be true that they didn't
conduct a full analysis.

50
00:03:03,900 --> 00:03:10,150
To explain how you get 5 out of 3 and 4,
we have to add another suppressed premise.

51
00:03:10,150 --> 00:03:16,930
And that premise can simply say, an
analysis is not full if it's abbreviated.

52
00:03:16,930 --> 00:03:21,210
And if it doesn't look at drilling on
lands in other federal leases.

53
00:03:21,210 --> 00:03:25,450
Notice that I used the word, and, instead
of, or, in the suppressed premise.

54
00:03:25,450 --> 00:03:27,000
And the reason is that I want to

55
00:03:27,000 --> 00:03:29,195
make Redford's argument look as good as
possible.

56
00:03:29,195 --> 00:03:35,310
And his premise is going to be more
defensible if he uses and instead of or.

57
00:03:35,310 --> 00:03:38,330
Because with and, the premise means that
the review's

58
00:03:38,330 --> 00:03:42,490
not full if it's, has both of those
problems.

59
00:03:42,490 --> 00:03:44,220
Whereas if you have or.

60
00:03:44,220 --> 00:03:46,350
It says that the review's not full if it

61
00:03:46,350 --> 00:03:49,269
has either one or the other of those
problems.

62
00:03:49,269 --> 00:03:50,580
Now some people might say,

63
00:03:50,580 --> 00:03:54,645
if it's only got one of those problems,
it's still a full review.

64
00:03:54,645 --> 00:03:56,870
You know, it's a little problem.

65
00:03:56,870 --> 00:03:58,094
It's a little flaw.

66
00:03:58,094 --> 00:04:01,670
But if it's got both of those problems,
then Redford's on stronger

67
00:04:01,670 --> 00:04:06,460
ground in saying, together they show but
that's not a full review.

68
00:04:06,460 --> 00:04:09,876
So if I want to make his premise look
good, and that's part of the

69
00:04:09,876 --> 00:04:14,720
point, then I want to use and in the
suppressed premise here instead of or.

70
00:04:14,720 --> 00:04:15,660
What about

71
00:04:15,660 --> 00:04:20,400
the step from 5 to 6, is that valid?
No, here we need a suppressed premise too.

72
00:04:20,400 --> 00:04:21,327
Which one?

73
00:04:21,327 --> 00:04:27,380
Well, we could just add, that if the BLM
did not

74
00:04:27,380 --> 00:04:33,440
conduct a full analysis of the
environmental impact, then the permit does

75
00:04:33,440 --> 00:04:36,200
not give the permit holder a valid right
to drill, because the

76
00:04:36,200 --> 00:04:41,440
process by which they obtained the permit
is not a proper process.

77
00:04:41,440 --> 00:04:44,655
Next, what about the move from 2 and 6 to
7?

78
00:04:44,655 --> 00:04:48,310
2 says that Conoco doesn't have a lease
that gives it a valid right to drill.

79
00:04:48,310 --> 00:04:50,430
And 6 says that Conoco doesn't have a

80
00:04:50,430 --> 00:04:52,400
permit that gives it a valid right to
drill.

81
00:04:53,910 --> 00:04:56,040
Is it possible that both of those are
true, and yet

82
00:04:56,040 --> 00:04:59,825
the conclusion's false and Conoco does
have a valid right to drill?

83
00:04:59,825 --> 00:05:03,460
Well yeah, if there were some other way
for it to get a valid right to drill.

84
00:05:03,460 --> 00:05:06,440
So, to make that argument valid,

85
00:05:06,440 --> 00:05:08,440
we have to add another suppressed premise

86
00:05:08,440 --> 00:05:12,570
which says, basically, you know, if the
lease

87
00:05:12,570 --> 00:05:15,555
and the permit don't give it a valid right
to drill, it ain't got none.

88
00:05:15,555 --> 00:05:21,350
More formally, and more stilted, we can
add a suppressed premise that says.

89
00:05:21,350 --> 00:05:23,730
If none of Conoco's leases gives it a

90
00:05:23,730 --> 00:05:27,060
valid right to drill, and Conoco's permit
does

91
00:05:27,060 --> 00:05:31,490
not give Conoco a valid right to drill,
then Conoco does not have a valid right

92
00:05:31,490 --> 00:05:32,880
to drill.

93
00:05:32,880 --> 00:05:35,720
And that should make that step of the
argument valid.

94
00:05:36,940 --> 00:05:41,016
There's only one left, the step from 7 and
8 to 9.

95
00:05:41,016 --> 00:05:46,130
Is that valid?
Yeah.

96
00:05:46,130 --> 00:05:49,860
It's not possible that Conoco doesn't have
a valid right to drill, and if they don't

97
00:05:49,860 --> 00:05:56,730
have a valid right to drill, then the BLM
must not allow

98
00:05:56,730 --> 00:06:02,800
them to drill and it not be true that the
BLM must not allow Conoco to drill.

99
00:06:02,800 --> 00:06:05,920
So, that's valid and we don't have to add
any suppressed premise to that one.

100
00:06:07,080 --> 00:06:11,055
And now we can take all these different
explicit premises and conclusions,

101
00:06:11,055 --> 00:06:16,230
and put them together with a suppressed
premises that we just talked about.

102
00:06:16,230 --> 00:06:20,411
And we end up with this final
reconstruction.

103
00:06:20,411 --> 00:06:22,140
1, all of Conoco's leases

104
00:06:22,140 --> 00:06:28,204
were originally issued without sufficient
environmental study or public input.

105
00:06:28,204 --> 00:06:32,660
2, no leases that were originally issued
without sufficient environmental study

106
00:06:32,660 --> 00:06:33,956
or public input give the leaseholder a
valid right to drill.

107
00:06:33,956 --> 00:06:36,128
Therefore, none of Conoco's leases give it
a valid right to drill.

108
00:06:36,128 --> 00:06:41,809
4, the BLM conducted an

109
00:06:41,809 --> 00:06:46,926
abbreviated review.

110
00:06:46,926 --> 00:06:53,420
5, the BLM didn't look at drilling on
lands under other federal leases.

111
00:06:53,420 --> 00:06:58,870
6, an analysis is not full if it's
abbreviated and it

112
00:06:58,870 --> 00:07:01,812
does not look at drilling on lands under
other federal leases.

113
00:07:01,812 --> 00:07:08,160
Therefore 7, the BLM did not conduct a
full analysis

114
00:07:08,160 --> 00:07:11,802
of the environmental impacts of drilling
on these incomparable lands.

115
00:07:11,802 --> 00:07:16,410
8, if the BLM does not conduct a full

116
00:07:16,410 --> 00:07:20,270
analysis of the environmental impacts of
drilling in these incomparable

117
00:07:20,270 --> 00:07:24,590
lands, before issuing a permit, then, that
permit does

118
00:07:24,590 --> 00:07:27,570
not give the permit holder a valid right
to drill.

119
00:07:28,970 --> 00:07:33,780
Therefore 9, Conoco's permit does not give
Conoco a valid right to drill.

120
00:07:36,415 --> 00:07:39,470
10, if none of Conoco's leases gives it

121
00:07:39,470 --> 00:07:42,800
a valid right to drill and Conoco's permit
does

122
00:07:42,800 --> 00:07:45,620
not give Conoco a valid right to drill,
then

123
00:07:45,620 --> 00:07:47,820
Conoco does not have a valid right to
drill.

124
00:07:47,820 --> 00:07:52,310
Therefore 11, Conoco does not have a valid
right to drill.

125
00:07:54,092 --> 00:07:56,980
12, if Conoco does not have a valid right
to

126
00:07:56,980 --> 00:08:01,310
drill, then the BLM must not allow Conoco
to drill.

127
00:08:01,310 --> 00:08:02,268
Therefore

128
00:08:02,268 --> 00:08:06,550
13.
The BLM must not allow Conoco to drill.

129
00:08:07,930 --> 00:08:11,960
Of course, other reconstructions could be
perfectly fine.

130
00:08:11,960 --> 00:08:13,920
Even if they differ in a few details.

131
00:08:13,920 --> 00:08:18,180
Because not every step in this process is
purely mechanical.

132
00:08:18,180 --> 00:08:21,930
So it doesn't always yield exactly the
same results every time.

133
00:08:21,930 --> 00:08:27,510
But I hope that this reconstruction seems
plausible, as a guess at what was going

134
00:08:27,510 --> 00:08:29,190
on in Redford's mind.

135
00:08:29,190 --> 00:08:33,978
And the reasons that he had for thinking
that the BLM should stop the drilling.

136
00:08:33,978 --> 00:08:36,690
If we want, we can check to make sure

137
00:08:36,690 --> 00:08:41,160
the structure makes sense, by trying to
diagram it.

138
00:08:41,160 --> 00:08:43,660
So here's a diagram that I made of

139
00:08:43,660 --> 00:08:45,640
the structure of the argument that I just
reconstructed.

140
00:08:47,440 --> 00:08:52,460
To the left, you see 1 plus 2, and an
underline under it.

141
00:08:52,460 --> 00:08:56,160
Saying that those premises work together
in a joint structure.

142
00:08:56,160 --> 00:09:01,023
And an arrow to the conclusion from those
two premises, namely 3.

143
00:09:02,550 --> 00:09:07,745
Then to the right of that, you see that 4,
5, and 6 are joined.

144
00:09:07,745 --> 00:09:13,080
4 plus 5 plus 6 and an underline with an
arrow to 7

145
00:09:13,080 --> 00:09:18,068
because that's the conclusion that those
three premises support.

146
00:09:18,068 --> 00:09:24,340
7 plus 8 is then underlined to show they
work jointly to lead to the conclusion 9.

147
00:09:24,340 --> 00:09:25,740
So there's an arrow to 9.

148
00:09:25,740 --> 00:09:31,780
And then there's a long underline between
3, 9, and 10.

149
00:09:31,780 --> 00:09:36,678
That other supressed premise that was
added.

150
00:09:36,678 --> 00:09:43,008
That shows that 3, 9, and 10 work together
jointly, and lead to the conclusion 11.

151
00:09:43,008 --> 00:09:46,258
And then 11 works together with 12, so you
have

152
00:09:46,258 --> 00:09:51,528
11 plus 12, and that's underlined with an
arrow to 13.

153
00:09:51,528 --> 00:09:53,450
Now the fact that we can draw this

154
00:09:53,450 --> 00:09:57,230
diagram confirms that the argument
structure makes sense.

155
00:09:58,380 --> 00:10:03,130
Now, let's see whether you can do it
yourself in the exercises.

