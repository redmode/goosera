1
00:00:04,090 --> 00:00:08,510
In unit two of this course, in our study
of the rules governing the validity

2
00:00:08,510 --> 00:00:11,430
of deductive arguments, we've looked at
other

3
00:00:11,430 --> 00:00:17,140
ways of representing information, other
ways besides sentences.

4
00:00:17,140 --> 00:00:19,830
We've looked at representing information
using a

5
00:00:19,830 --> 00:00:22,219
truth table, or using a Venn diagram.

6
00:00:23,520 --> 00:00:26,430
Now today, in this final lecture of unit
two,

7
00:00:26,430 --> 00:00:29,260
I'd like to talk about the advantages of
these other

8
00:00:29,260 --> 00:00:32,090
ways of representing information.

9
00:00:32,090 --> 00:00:36,120
What's the point of using truth tables, or
using Venn diagrams?

10
00:00:36,120 --> 00:00:40,900
Why did we learn these techniques?
And that's the topic of today's lecture.

11
00:00:43,600 --> 00:00:46,950
Okay, so let's consider some different
ways

12
00:00:46,950 --> 00:00:48,860
that we can represent the same
information.

13
00:00:50,720 --> 00:00:53,730
Consider for example this list of seven
sentences.

14
00:00:53,730 --> 00:00:55,070
Here's seven sentences.

15
00:00:55,070 --> 00:00:58,420
They all represent the very same
information,

16
00:00:58,420 --> 00:01:00,700
they all mean the very same thing.

17
00:01:00,700 --> 00:01:03,940
They just say the same thing in different
languages.

18
00:01:03,940 --> 00:01:09,420
One is in French, one is in Italian, one
is in Spanish and so on.

19
00:01:09,420 --> 00:01:12,510
But even though they're in different
languages and

20
00:01:12,510 --> 00:01:15,690
they look different, they all express the
same information.

21
00:01:15,690 --> 00:01:17,220
They represent the same information.

22
00:01:18,740 --> 00:01:21,840
Well similarly, just as we can use
different sentences.

23
00:01:21,840 --> 00:01:23,860
To represent that information.

24
00:01:23,860 --> 00:01:26,370
We can also use something that is not

25
00:01:26,370 --> 00:01:30,470
a sentence at all to represent that
information.

26
00:01:30,470 --> 00:01:32,430
For instance, here.

27
00:01:32,430 --> 00:01:34,860
We can use a truth table

28
00:01:34,860 --> 00:01:40,270
to represent the very same information
that we were representing

29
00:01:40,270 --> 00:01:43,000
by means of those different sentences in
the previous slide.

30
00:01:44,170 --> 00:01:45,758
So, look at this truth table for a moment.

31
00:01:45,758 --> 00:01:47,614
Now, first.

32
00:01:47,614 --> 00:01:49,700
[COUGH].

33
00:01:49,700 --> 00:01:53,630
First column of the Truth Table is the
proposition Walter likes bourbon,

34
00:01:53,630 --> 00:01:57,530
the second column of the Truth Table is
the proposition Walter likes vodka.

35
00:01:57,530 --> 00:02:00,010
And the third column is the conjunction of

36
00:02:00,010 --> 00:02:01,840
those two propositions.

37
00:02:01,840 --> 00:02:04,200
Which could naturally be expressed in
English,

38
00:02:04,200 --> 00:02:07,160
by the sentence Walter likes bourbon and
vodka.

39
00:02:07,160 --> 00:02:09,420
And now what we can do once we have this
truth table

40
00:02:09,420 --> 00:02:13,760
here, that shows how the truth or falsity
of the conjunction, depends

41
00:02:13,760 --> 00:02:19,220
on the truth or falsity of each of the
conjuncts, we can circle a particular

42
00:02:19,220 --> 00:02:25,380
line, of that truth table To indicate,
that this is

43
00:02:25,380 --> 00:02:30,480
the situation, that we're actually in.
Okay,

44
00:02:30,480 --> 00:02:35,980
now when we circle that particular, row of
the truth table,

45
00:02:35,980 --> 00:02:41,700
what we're doing, is expressing the very
same information

46
00:02:41,700 --> 00:02:46,060
that we expressed, using one of these
seven sentences earlier.

47
00:02:47,160 --> 00:02:50,540
Right.
There's no difference in what information

48
00:02:50,540 --> 00:02:53,780
we're expressing, what information we're
representing.

49
00:02:53,780 --> 00:02:57,560
We're just representing it, without using
a sentence.

50
00:02:57,560 --> 00:03:05,390
We're representing it, by circling, a row
of the truth table, okay?

51
00:03:05,390 --> 00:03:08,920
So it's just a different way of
representing the same information.

52
00:03:10,500 --> 00:03:15,620
Now you might wonder, well what's the
point of representing that same

53
00:03:15,620 --> 00:03:17,079
information in different ways?

54
00:03:18,260 --> 00:03:20,220
Well, the point of representing the

55
00:03:20,220 --> 00:03:22,690
same information using different sentences
in different

56
00:03:22,690 --> 00:03:24,370
languages is just that you can make

57
00:03:24,370 --> 00:03:27,220
yourself understood by different groups of
people.

58
00:03:27,220 --> 00:03:30,090
You can say it in French to make yourself
understood in France.

59
00:03:30,090 --> 00:03:35,100
You can say it in Spanish to make yourself
understood in Spain or in Latin America.

60
00:03:35,100 --> 00:03:39,100
You can say it in Russian to make yourself
understood in Russia, and so forth.

61
00:03:39,100 --> 00:03:40,870
So, what's the point

62
00:03:40,870 --> 00:03:42,770
of using the truth table?

63
00:03:42,770 --> 00:03:45,710
And there's no country where they speak
truth table.

64
00:03:45,710 --> 00:03:50,670
So what's the point of using a truth table
to express the very same information?

65
00:03:50,670 --> 00:03:53,740
Well the point is That, by using a

66
00:03:53,740 --> 00:03:56,930
truth table to represent the very same
information, you

67
00:03:56,930 --> 00:03:59,850
represent that information in a way that
makes very

68
00:03:59,850 --> 00:04:06,260
clear exactly what deductive arguments
that use that information

69
00:04:06,260 --> 00:04:09,470
are valid and which ones are invalid.

70
00:04:09,470 --> 00:04:12,510
So, for instance, by looking at this truth
table.

71
00:04:12,510 --> 00:04:16,480
Looking at our representation of the
information that uses this truth table, we

72
00:04:16,480 --> 00:04:22,440
can see very clearly that the argument
from the premise, Walter likes bourbon

73
00:04:22,440 --> 00:04:28,500
and vodka, to the conclusion, Walter likes
bourbon, is going to be a valid argument.

74
00:04:28,500 --> 00:04:31,370
There's no possible way for

75
00:04:31,370 --> 00:04:35,250
the premise to be true while the
conclusion is false.

76
00:04:35,250 --> 00:04:35,410
Right?

77
00:04:35,410 --> 00:04:38,080
If the premise is true, then the

78
00:04:38,080 --> 00:04:40,180
conclusion is also going to have to be
true.

79
00:04:42,110 --> 00:04:46,710
But, we can also see that the deductive
argument from the

80
00:04:46,710 --> 00:04:49,550
premise, Walter likes bourbon, to the

81
00:04:49,550 --> 00:04:52,360
conclusion, Walter likes bourbon and
vodka.

82
00:04:52,360 --> 00:04:54,360
Is invalid.

83
00:04:54,360 --> 00:04:57,000
There is a possible way,

84
00:04:57,000 --> 00:05:02,150
for the premise, Walter likes bourbon to
be true, while the conclusion, Walter

85
00:05:02,150 --> 00:05:07,050
likes bourbon and vodka, is false.
So,

86
00:05:08,110 --> 00:05:12,565
by looking at the truth table, you can
see, very plainly,

87
00:05:12,565 --> 00:05:17,180
why some deductive arguments, that involve
the

88
00:05:17,180 --> 00:05:22,090
proposition Walter likes bourbon and
vodka, are valid, and other deductive

89
00:05:22,090 --> 00:05:26,700
arguments, involving that same
proposition, are invalid.

90
00:05:26,700 --> 00:05:32,220
And that's something that you can't see
just as clearly by looking at

91
00:05:32,220 --> 00:05:36,039
any of the seven sentences that we can use
to represent that information.

92
00:05:37,220 --> 00:05:41,420
So, these sentences have some advantages
as a

93
00:05:41,420 --> 00:05:45,440
way of representing the information that
they all represent.

94
00:05:46,650 --> 00:05:47,150
But they all

95
00:05:47,150 --> 00:05:55,570
have a disadvantage relative to the truth
table, which shows us plainly why certain

96
00:05:55,570 --> 00:06:00,050
arguments that use that information are
valid and others are invalid.

97
00:06:00,050 --> 00:06:00,550
So

98
00:06:02,170 --> 00:06:06,680
that's the advantage of using a truth
table to represent

99
00:06:06,680 --> 00:06:12,360
information that could be represented more
naturally by means of sentences.

100
00:06:12,360 --> 00:06:17,550
It's not that we make ourselves understood
by more people when we

101
00:06:17,550 --> 00:06:21,710
use a truth table, it's rather that when
we use a truth table.

102
00:06:21,710 --> 00:06:27,700
We can see relations of deductive validity
that we can't just see when

103
00:06:27,700 --> 00:06:28,500
we use sentences.

104
00:06:31,150 --> 00:06:35,350
Okay, now, how about Venn diagrams?
Well,

105
00:06:35,350 --> 00:06:41,000
consider again.
Piece

106
00:06:41,000 --> 00:06:47,410
of information that could represented
using any of these seven sentences, right?

107
00:06:47,410 --> 00:06:51,620
One is in French, one is in Spanish, one
in is Italian, and so on.

108
00:06:51,620 --> 00:06:51,910
Right?

109
00:06:51,910 --> 00:06:56,310
All seven of these sentences mean the same
thing, they express the very

110
00:06:56,310 --> 00:07:00,750
same information, and they represent it.

111
00:07:00,750 --> 00:07:02,000
Even though they represent the same

112
00:07:02,000 --> 00:07:05,370
information, they're useful in different
situations.

113
00:07:05,370 --> 00:07:09,200
You might use one when you're in Russia,
and you want to be understood by Russians.

114
00:07:09,200 --> 00:07:12,050
You might use another when you're in
India, and you

115
00:07:12,050 --> 00:07:16,180
want to be understood by a certain
population of people in India.

116
00:07:16,180 --> 00:07:18,660
You might want to use another when you're
in Brazil

117
00:07:18,660 --> 00:07:21,799
or Portugal, and you want to be understood
by Portugese speakers.

118
00:07:23,690 --> 00:07:28,300
But notice, none of these sentences,
represent, the

119
00:07:28,300 --> 00:07:30,280
information that they represent, in a way
that

120
00:07:30,280 --> 00:07:34,390
makes it completely clear, which deductive
arguments that

121
00:07:34,390 --> 00:07:37,600
use that information are valid, and which
are invalid.

122
00:07:39,440 --> 00:07:41,750
' Kay, we can make that point clear

123
00:07:41,750 --> 00:07:46,170
by representing the same information using
a Venn Diagram.

124
00:07:46,170 --> 00:07:48,820
First, we construct

125
00:07:48,820 --> 00:07:54,430
a circle to represent the category of
Walter's

126
00:07:56,870 --> 00:07:57,370
drinks.

127
00:07:59,760 --> 00:08:05,408
And second, we construct a circle to
represent the category of,

128
00:08:05,408 --> 00:08:11,080
imported, things.
[SOUND]

129
00:08:11,080 --> 00:08:14,070
And, since these seven sentences all say
that all

130
00:08:14,070 --> 00:08:16,860
of Walter dri, all of Walter's drinks are
imported, we

131
00:08:16,860 --> 00:08:21,520
can then shade out, the, part of the
circle representing

132
00:08:21,520 --> 00:08:25,970
Walter's drinks, that's outside the circle
of imported things, right.

133
00:08:25,970 --> 00:08:29,600
To show us that, if Walter drinks
anything, than whatever it is

134
00:08:29,600 --> 00:08:33,525
that he drinks must be imported, it must
be inside this region here.

135
00:08:33,525 --> 00:08:36,290
[SOUND] Okay.

136
00:08:36,290 --> 00:08:39,480
And now, we have a Venn diagram that
represents the

137
00:08:39,480 --> 00:08:42,780
very same information that was represented
by those seven sentences.

138
00:08:44,160 --> 00:08:50,010
But, what's the point of representing this
information using a Venn diagram?

139
00:08:50,010 --> 00:08:51,160
Well the Venn diagram

140
00:08:51,160 --> 00:08:55,510
shows us, which deductive arguments that
use that information are

141
00:08:55,510 --> 00:08:59,800
valid, and which are invalid.
So for instance, consider the argument

142
00:08:59,800 --> 00:09:06,609
from, all, Walter's, drinks, are,

143
00:09:06,609 --> 00:09:15,639
imported.
[SOUND] To all, imported drinks,

144
00:09:15,639 --> 00:09:20,379
are Walter's, is that

145
00:09:20,379 --> 00:09:26,550
argument valid or invalid?

146
00:09:26,550 --> 00:09:28,580
Well, if you just look at the sentences,
it

147
00:09:28,580 --> 00:09:31,780
might not be obvious whether it's valid or
invalid,

148
00:09:31,780 --> 00:09:33,890
but if you look at the Venn diagram, you

149
00:09:33,890 --> 00:09:37,340
can see quite clearly that this argument
is invalid.

150
00:09:38,400 --> 00:09:41,170
For all of Walter's drinks to be imported,

151
00:09:41,170 --> 00:09:45,390
is for this part of the Venn diagram to be
shaded in.

152
00:09:45,390 --> 00:09:49,170
But for all imported drinks to be
Walter's, would be

153
00:09:49,170 --> 00:09:52,940
for this part of the Venn diagram to be
shaded in.

154
00:09:52,940 --> 00:09:54,990
And the question is, is there some way for

155
00:09:54,990 --> 00:09:59,940
the premise to be true while the
conclusion is false?

156
00:09:59,940 --> 00:10:02,550
And the answer is clearly yes.

157
00:10:02,550 --> 00:10:06,390
This part of the Venn diagram could be
shaded in even if

158
00:10:06,390 --> 00:10:09,640
this part of the Venn diagram is not
shaded in.

159
00:10:10,990 --> 00:10:13,370
And so, this is an example of how we

160
00:10:13,370 --> 00:10:18,730
can use the Venn diagram to show very
plainly, visually.

161
00:10:19,800 --> 00:10:22,580
Why certain arguments that use the

162
00:10:22,580 --> 00:10:25,390
information, all Walter's drinks are
imported.

163
00:10:25,390 --> 00:10:28,710
Certain of those arguments are valid.
And certain of them are invalid.

164
00:10:30,360 --> 00:10:32,130
Again, so the

165
00:10:32,130 --> 00:10:36,110
point of using a Venn diagram to represent
information, is, in that respect,

166
00:10:36,110 --> 00:10:40,510
very similar, to the point of using a
truth table to represent information.

167
00:10:41,700 --> 00:10:46,510
When you want to understand whether a
particular deductive argument is valid or

168
00:10:46,510 --> 00:10:52,510
invalid, sometimes it helps to translate
the information in that argument

169
00:10:52,510 --> 00:10:57,170
into a form where you can plainly see

170
00:10:57,170 --> 00:11:01,732
the relations of validity or invalidity.
Into a form like a

171
00:11:01,732 --> 00:11:05,600
truth table, or a Venn Diagram.
Where you can plainly see those

172
00:11:05,600 --> 00:11:10,850
relationships because ordinary language
doesn't always expose those

173
00:11:10,850 --> 00:11:16,229
relationships.
Okay, so that's why truth

174
00:11:16,229 --> 00:11:22,230
tables and Venn Diagrams are useful
devices for understanding whether

175
00:11:22,230 --> 00:11:25,800
deductive arguments are valid or invalid.

176
00:11:25,800 --> 00:11:28,950
And there are many other devices like
that.

177
00:11:28,950 --> 00:11:33,000
But truth tables and Venn diagrams are the
two simplest ones,

178
00:11:33,000 --> 00:11:35,300
and so the two that we focused on in this
course.

179
00:11:36,520 --> 00:11:41,840
Okay, well have fun with the quizzes, and
have fun with the rest of the course.

180
00:11:41,840 --> 00:11:43,520
See you next time.

