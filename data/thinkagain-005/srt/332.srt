1
00:00:04,120 --> 00:00:06,260
So to review, here's what we've done this
week.

2
00:00:06,260 --> 00:00:09,480
We've learned about some truth functional
connectives like

3
00:00:09,480 --> 00:00:13,370
conjunction, disjunction, and the
conditional and the bi-conditional.

4
00:00:13,370 --> 00:00:16,300
And we've also learned about the truth
functional operator negation.

5
00:00:16,300 --> 00:00:20,380
We've looked at the truth tables for these
connectives and operators.

6
00:00:20,380 --> 00:00:24,680
The truth tables that show how the truth
or falsity of the proposition

7
00:00:24,680 --> 00:00:29,230
that they can be used to create, depends
strictly on the truth or falsity

8
00:00:29,230 --> 00:00:31,480
of the propositions that they're applied
to.

9
00:00:32,660 --> 00:00:38,780
And then we've looked at how those truth
tables can be used to determine

10
00:00:38,780 --> 00:00:41,348
whether and why arguments that employ
those

11
00:00:41,348 --> 00:00:43,984
truth functional connectives or operators
are valid.

12
00:00:43,984 --> 00:00:49,670
Now in order to master

13
00:00:49,670 --> 00:00:54,290
the skills that we've learned this week,
there's no substitute

14
00:00:54,290 --> 00:00:55,680
for practice.

15
00:00:55,680 --> 00:00:59,930
And so what I'd like to recommend is that
all of you practice

16
00:00:59,930 --> 00:01:05,890
the skills that you've learned this week
using the resources that are listed.

17
00:01:05,890 --> 00:01:08,460
Okay, have fun and I'll see you next

18
00:01:08,460 --> 00:01:13,052
week when we talk about categorical logic
and quantifiers.

