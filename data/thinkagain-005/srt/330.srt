1
00:00:03,870 --> 00:00:06,820
Today, we're going to talk about a new
propositional connective.

2
00:00:06,820 --> 00:00:09,130
A truth functional connective, that I'll
call the biconditional.

3
00:00:09,130 --> 00:00:09,550
Now, what's a biconditional?

4
00:00:09,550 --> 00:00:12,900
In order to explain what a biconditional,
lemme

5
00:00:12,900 --> 00:00:15,390
start off by telling you the following
story.

6
00:00:15,390 --> 00:00:16,900
And this is a true story, I should add.

7
00:00:16,900 --> 00:00:18,750
When I was in eighth grade.

8
00:00:21,980 --> 00:00:25,845
I remember, my first class in the morning
was a math class,

9
00:00:25,845 --> 00:00:29,130
and there was a boy who sat next to me in
math class.

10
00:00:29,130 --> 00:00:30,660
The teacher called him Bob.

11
00:00:32,180 --> 00:00:35,800
My third class, later in the morning was a
science class.

12
00:00:35,800 --> 00:00:37,320
And there was a boy who sat next to me

13
00:00:37,320 --> 00:00:39,865
in the science class, and the teacher
called him George.

14
00:00:39,865 --> 00:00:46,325
And I was struck by the fact that Bob and
George,

15
00:00:46,325 --> 00:00:50,520
who sat next to me in math and science
class respectively, had the

16
00:00:50,520 --> 00:00:55,170
same last name, and they also bore and
uncanny resemblance to each other.

17
00:00:57,270 --> 00:01:03,390
Well, one day I asked Bob If he had a
brother

18
00:01:03,390 --> 00:01:07,600
named George, and it turned out that he
didn't have a brother named George.

19
00:01:07,600 --> 00:01:09,110
In fact, Bob was George.

20
00:01:09,110 --> 00:01:12,370
It was one and the same boy, and they just

21
00:01:12,370 --> 00:01:14,606
went by two different names to the two
different teachers.

22
00:01:14,606 --> 00:01:22,270
But suppose that all you knew from my
description of the situation,

23
00:01:22,270 --> 00:01:23,790
was that there was a boy who sat next to

24
00:01:23,790 --> 00:01:26,130
me in math class named Bob and a boy who
sat

25
00:01:26,130 --> 00:01:28,720
next to me in science class named George,
and you

26
00:01:28,720 --> 00:01:33,680
ask me one day was Bob born in the United
States?

27
00:01:33,680 --> 00:01:36,610
Now, I might say to you, well Bob was born

28
00:01:36,610 --> 00:01:39,710
in the United States if and only if George
was.

29
00:01:41,930 --> 00:01:46,140
Now, there I'm using the phrase, if and
only if.

30
00:01:46,140 --> 00:01:48,010
Now remember how the phrase, if works.

31
00:01:48,010 --> 00:01:53,003
If I say, if to connect two propositions,
P if Q.

32
00:01:53,003 --> 00:02:02,960
That's equivalent to saying, If Q then P
but now remember how only if works.

33
00:02:02,960 --> 00:02:07,250
If I say p only if q, that's equivalent to
saying

34
00:02:07,250 --> 00:02:08,299
if p then q.

35
00:02:08,299 --> 00:02:14,870
So when I use the phrase if and only if,
I'm conjoining the conditional if

36
00:02:17,380 --> 00:02:23,350
p then q.
And the conditional if Q then P.

37
00:02:23,350 --> 00:02:26,024
And so I get what I'll call a
biconditional.

38
00:02:26,024 --> 00:02:30,020
A biconditional is a propositional
connective that connects two

39
00:02:30,020 --> 00:02:34,070
propositions into a larger proposition,
and the larger proposition

40
00:02:34,070 --> 00:02:39,970
is true just in case the two propositions
that

41
00:02:39,970 --> 00:02:42,550
are part of it have the same truth value.

42
00:02:43,730 --> 00:02:48,110
In other words, the larger proposition, p
if and only

43
00:02:48,110 --> 00:02:52,997
if q, is going to be true just in case p
and q are both true

44
00:02:52,997 --> 00:02:57,820
or p and q are both false.

45
00:02:59,210 --> 00:03:04,120
So I could say, George was born in the
United States if and only if Bob was.

46
00:03:05,620 --> 00:03:08,590
Under what circumstances is that
proposition going to be true?

47
00:03:08,590 --> 00:03:09,460
Well it'll

48
00:03:09,460 --> 00:03:16,100
be true if George and Bob were both born
in the United States, or

49
00:03:16,100 --> 00:03:20,250
well I guess it's misleading to say both,
because really there's only one boy that

50
00:03:20,250 --> 00:03:23,660
we're talking about if George was born in
the United States and Bob was

51
00:03:23,660 --> 00:03:28,290
born in the United States, it'll be true
when both of those propositions are true.

52
00:03:28,290 --> 00:03:31,840
But it'll also be true when George was not
born in

53
00:03:31,840 --> 00:03:34,480
the United States and Bob was not born in
the United States.

54
00:03:34,480 --> 00:03:37,439
In other words, it'll be true when both of
those two propositions are false.

55
00:03:38,950 --> 00:03:42,200
What makes true, the proposition George
was born

56
00:03:42,200 --> 00:03:44,070
in the United States if and only if Bob

57
00:03:44,070 --> 00:03:46,860
was is simply that the two propositions
that are

58
00:03:46,860 --> 00:03:48,910
part of it, George was born in the United

59
00:03:48,910 --> 00:03:50,990
States and Bob was born in the United
States

60
00:03:50,990 --> 00:03:53,710
have the same truth value whatever that
truth value

61
00:03:53,710 --> 00:03:56,360
is whether its the truth value true or the

62
00:03:56,360 --> 00:03:59,790
truth value false as long as those two
propositions

63
00:03:59,790 --> 00:04:02,990
have the same truth value it's going to be
true that Bob was

64
00:04:02,990 --> 00:04:05,828
born in the United States if and only if
Bob was, and so

65
00:04:05,828 --> 00:04:10,316
the bi-conditional connecting those two
propositions, George was born in the US

66
00:04:10,316 --> 00:04:13,820
and Bob was born in the US, that
bi-conditional is going to be true.

67
00:04:13,820 --> 00:04:19,490
So we can state the truth table for the

68
00:04:19,490 --> 00:04:23,300
truth functional connective, which is the
biconditional as follows.

69
00:04:23,300 --> 00:04:26,479
The biconditional

70
00:04:26,479 --> 00:04:29,720
connects any two propositions, let's call
them P

71
00:04:29,720 --> 00:04:32,380
and Q, it doesn't matter what they are.

72
00:04:32,380 --> 00:04:34,515
When P is true and Q is true, then the

73
00:04:34,515 --> 00:04:39,840
biconditional P if and only if Q is
going to be true.

74
00:04:39,840 --> 00:04:43,020
When P is true and Q is false, then the
biconditional

75
00:04:43,020 --> 00:04:46,490
P if and only of Q is going to be false.

76
00:04:46,490 --> 00:04:51,710
When P is false and Q is true, then the
biconditional P if and only of Q is going

77
00:04:51,710 --> 00:04:52,430
to be false.

78
00:04:52,430 --> 00:04:58,600
And finally, if P is false and Q is false,
then the biconditional P if and only if Q.

79
00:04:58,600 --> 00:05:03,770
Is going to be true, so that's the truth
table for the biconditional.

80
00:05:03,770 --> 00:05:05,250
Now in the last couple of lectures,

81
00:05:05,250 --> 00:05:08,914
I described both the conditional and the
biconditional

82
00:05:08,914 --> 00:05:11,880
as truth-functional connectives, and I've
given some reason

83
00:05:11,880 --> 00:05:15,520
to think that they are both
truth-functional connectives.

84
00:05:15,520 --> 00:05:17,000
But you might worry that

85
00:05:17,000 --> 00:05:21,980
there's some examples that suggest that
both the

86
00:05:21,980 --> 00:05:26,880
conditional and the biconditional are not
truth functional connectives.

87
00:05:26,880 --> 00:05:30,652
For instance, consider the sentence, if
two plus 2

88
00:05:30,652 --> 00:05:33,820
equals 4, than Pierre is the capital of
South Dakota.

89
00:05:33,820 --> 00:05:38,660
Now there I'm using if then to express the
conditional.

90
00:05:39,890 --> 00:05:42,035
Right.
I'm saying if it's true that

91
00:05:42,035 --> 00:05:46,060
2 plus 2 equals 4, then it's true that
Pierre is the capital of South Dakota.

92
00:05:46,060 --> 00:05:51,210
Now, according to the truth table for the
conditional, that conditional

93
00:05:51,210 --> 00:05:56,270
has gotta be true because the first
proposition, what we'll call

94
00:05:56,270 --> 00:06:01,520
the antecedent, the proposition that
occurs right after the if, that

95
00:06:01,520 --> 00:06:06,260
antecedent is true, it's true that 2 plus
2 equals 4.

96
00:06:06,260 --> 00:06:07,085
And also the

97
00:06:07,085 --> 00:06:11,910
consequent, the proposition that occurs
right after the then.

98
00:06:11,910 --> 00:06:16,599
The consequent is also true, it's true
that Piere is the capital of South Dakota.

99
00:06:18,260 --> 00:06:21,820
So according to the truth table for the
conditional, it's going to be true

100
00:06:21,820 --> 00:06:24,955
that if two plus two equals four than
Pierre is the capitol of South Dakota.

101
00:06:24,955 --> 00:06:27,460
And now you might worry, wait a second,
this is

102
00:06:27,460 --> 00:06:32,450
a very strange consequence of the truth
table for the conditional.

103
00:06:32,450 --> 00:06:35,430
Is it really true that is two plus two

104
00:06:35,430 --> 00:06:37,770
equals four then Pierre is the capitol of
South Dakota?

105
00:06:37,770 --> 00:06:40,439
And that's a very baffling thing to say.

106
00:06:43,650 --> 00:06:46,550
Now I want to say I completely agree with
this objection.

107
00:06:46,550 --> 00:06:49,370
It is a baffling thing to say that if two
plus

108
00:06:49,370 --> 00:06:52,160
two equals four than Pierre is the capital
of South Dakota.

109
00:06:52,160 --> 00:06:58,380
But, just because it's a baffling thing to
say doesn't mean it's not true.

110
00:06:59,750 --> 00:07:04,420
Look, it's baffling that Pierre is the
capital of South Dakota, but baffling as

111
00:07:04,420 --> 00:07:08,610
that may be, it's still true that Pierre
is the capital of South Dakota.

112
00:07:08,610 --> 00:07:10,945
Some things are baffling, even though
they're true.

113
00:07:10,945 --> 00:07:16,176
And this is another example of that
general kind of thing.

114
00:07:16,176 --> 00:07:19,862
It's baffling, nonetheless true.

