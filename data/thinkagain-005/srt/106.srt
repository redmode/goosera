1
00:00:03,050 --> 00:00:05,906
In the previous lecture we saw one example
of inference to the

2
00:00:05,906 --> 00:00:09,040
best explanation, but it might help to go
through just one more.

3
00:00:10,150 --> 00:00:12,945
I told you, that detectives often
inference to the

4
00:00:12,945 --> 00:00:16,980
best explanation to figure out who
committed the crime.

5
00:00:16,980 --> 00:00:19,950
One really neat example of this kind of
argument, was

6
00:00:19,950 --> 00:00:24,400
provided by some students in the first
offering of this course.

7
00:00:24,400 --> 00:00:28,645
Just check out the video that they made.
>> I think I'm

8
00:00:28,645 --> 00:00:34,630
going to go for a run.
Phillip and McKenzie are sleeping.

9
00:00:35,860 --> 00:00:37,560
Work up an appetite.

10
00:00:37,560 --> 00:00:39,209
I might dig into some of those cookies
later.

11
00:00:41,660 --> 00:00:41,661
[SOUND].

12
00:00:41,661 --> 00:00:45,860
That was a good run.
I think I'm going to have some cookies.

13
00:00:45,860 --> 00:00:46,530
What?

14
00:00:48,130 --> 00:00:51,880
Oh, man.
That was a brand-new dish.

15
00:00:51,880 --> 00:00:54,400
The dish broke, and I was wondering, if
you heard

16
00:00:54,400 --> 00:00:56,850
anything or if you have anything to do
with it.

17
00:00:56,850 --> 00:00:59,699
>> No, I just woke up.
>> You didn't hear, huh.

18
00:00:59,699 --> 00:01:02,290
I wonder what, how this could have
happened.

19
00:01:02,290 --> 00:01:04,470
You didn't feel any earthquake, did you,
or something?

20
00:01:04,470 --> 00:01:07,188
>> Well, I suggest that

21
00:01:07,188 --> 00:01:11,272
perhaps it was Timmy.
>> Timmy?

22
00:01:11,272 --> 00:01:14,792
You know, come to think of it, I'm looking
at this counter, and

23
00:01:14,792 --> 00:01:18,222
I'm seeing a dump of cat's hair, I think
we found our answer.

24
00:01:18,222 --> 00:01:20,095
>> Mm-hm.
Timmy did it.

25
00:01:20,095 --> 00:01:21,923
>> Phillipa.

26
00:01:21,923 --> 00:01:22,943
>> Timmy.

27
00:01:22,943 --> 00:01:24,788
>> Timmy what do you have to say for
yourself?

28
00:01:24,788 --> 00:01:29,140
>> Meow.

29
00:01:29,140 --> 00:01:32,550
>> Love that video didn't you?
Great production values.

30
00:01:32,550 --> 00:01:35,880
He's ready for Hollywood.
The actors, Emmys.

31
00:01:35,880 --> 00:01:38,490
Oh man.
And what every hard professionals.

32
00:01:40,130 --> 00:01:41,620
And the argument was good too.

33
00:01:42,960 --> 00:01:45,025
First of all, it was really clear, I mean

34
00:01:45,025 --> 00:01:48,080
you know exactly why they think Timmy did
it.

35
00:01:48,080 --> 00:01:49,760
Structure was really good.

36
00:01:50,830 --> 00:01:53,638
You start with a phenomenon that needs to
be

37
00:01:53,638 --> 00:01:57,220
explained, namely the dish was broken on
the floor.

38
00:01:58,860 --> 00:02:01,996
And you look for alternative explanations,
and you compare those

39
00:02:01,996 --> 00:02:05,620
different explanations, and the best
explanation, is that Timmy did it.

40
00:02:07,230 --> 00:02:09,100
Okay, so what are the competing
explanations?

41
00:02:09,100 --> 00:02:12,464
Well, McKenzie might of done, but she
denied it, she wouldn't lie

42
00:02:12,464 --> 00:02:15,560
to her parents, she looks so innocent, at
least I thought so.

43
00:02:15,560 --> 00:02:17,099
We'll see whether other people agree.

44
00:02:18,870 --> 00:02:21,470
And the other possibility is an
earthquake, but nobody felt an earthquake,

45
00:02:21,470 --> 00:02:23,950
and they probably would have felt an
earthquake, so that's no good.

46
00:02:23,950 --> 00:02:25,470
Maybe Philipa did it.
Wait a minute.

47
00:02:25,470 --> 00:02:28,095
I don't know if you noticed that little
box.

48
00:02:28,095 --> 00:02:31,290
Phillipa sleeps like a log, and he got
back at 7:45.

49
00:02:31,290 --> 00:02:35,604
That's when the dish was broke, she didn't
come down till 9:45.

50
00:02:35,604 --> 00:02:39,310
If she's sleeping like a log, how could
she have done it?

51
00:02:39,310 --> 00:02:41,752
So we've got different hypotheses that are

52
00:02:41,752 --> 00:02:45,070
being considered, and he's looking for the
best.

53
00:02:45,070 --> 00:02:46,750
So what makes it best is not just that

54
00:02:46,750 --> 00:02:49,650
those other hypotheses have things that
rule them out.

55
00:02:49,650 --> 00:02:54,075
But also that this hypothesis has a lot of
positive support for it, one

56
00:02:54,075 --> 00:02:59,490
thing that's nice is he found the hair on
the counter, that was crucial.

57
00:02:59,490 --> 00:03:02,640
Because now the hypothesis that Timmy did
it, explains not

58
00:03:02,640 --> 00:03:05,530
just the dish, but also the hair on the
counter.

59
00:03:05,530 --> 00:03:08,040
And that's a sign, of a good explanation.

60
00:03:08,040 --> 00:03:11,340
That it explains not just the particular
things, but it's

61
00:03:11,340 --> 00:03:14,640
powerful, and broad, it applies to other
things as well,

62
00:03:14,640 --> 00:03:18,470
and explains other phenomenon.
that need to be explained.

63
00:03:19,580 --> 00:03:25,550
So, overall eh, I think there's pretty
good reason to think that Timmy did it.

64
00:03:27,040 --> 00:03:31,350
And the kicker that proves it all is that
the cat confessed.

65
00:03:31,350 --> 00:03:33,288
You heard it yourself.
The cat said.

66
00:03:33,288 --> 00:03:34,444
>> Meow.

67
00:03:34,444 --> 00:03:40,177
>> Of course, your argument could be
stronger, because inductive strength

68
00:03:40,177 --> 00:03:42,330
comes in degrees.

69
00:03:42,330 --> 00:03:46,470
But the fact that it could be stronger
doesn't mean it's no good.

70
00:03:46,470 --> 00:03:52,190
You can get a pretty good reason by ruling
out the most plausible hypotheses,

71
00:03:52,190 --> 00:03:58,220
even if there are a few that you didn't
quite get to in presenting your argument.

72
00:03:59,750 --> 00:04:05,430
And sure enough, in the discussion forums,
some students pointed out weaknesses.

73
00:04:05,430 --> 00:04:08,128
Which amounted to looking at these
different

74
00:04:08,128 --> 00:04:11,860
hypotheses as possible explanations of the
data.

75
00:04:11,860 --> 00:04:14,730
A lot of students seem to suspect
McKenzie.

76
00:04:14,730 --> 00:04:15,386
Here's one of them.

77
00:04:15,386 --> 00:04:19,894
Joe writes, Hi Kevin, while watching the
interrogation

78
00:04:19,894 --> 00:04:25,120
of McKenzie I got the feeling she was
hiding something.

79
00:04:25,120 --> 00:04:30,633
She had means, motive and opportunity.
I think in order to add to the

80
00:04:30,633 --> 00:04:34,183
strength of the inductive argument that
she's innocent,

81
00:04:34,183 --> 00:04:37,059
she should volunteer for a lie detector
test.

82
00:04:38,180 --> 00:04:41,410
And then he goes on, I suspect Timmy was
framed.

83
00:04:41,410 --> 00:04:44,791
Considering how minorities and the poor
are treated in the

84
00:04:44,791 --> 00:04:49,440
justice system I recommend he remain
silent until he secures representation.

85
00:04:51,110 --> 00:04:53,035
Well, I don't know about the second half

86
00:04:53,035 --> 00:04:55,895
of that but, with regard McKenzie, there's
an interesting

87
00:04:55,895 --> 00:04:57,180
point to be, notice here.

88
00:04:58,420 --> 00:05:01,800
Joe didn't know Mackenzie and he didn't
trust her.

89
00:05:01,800 --> 00:05:06,460
Thinks she looked suspicious.
But Kevin, he knows McKenzie.

90
00:05:06,460 --> 00:05:07,490
He lives with her.

91
00:05:07,490 --> 00:05:10,080
He spent a lot of time with her and he
trusts her.

92
00:05:11,770 --> 00:05:14,674
So we have a nice example of how an
inductive argument

93
00:05:14,674 --> 00:05:18,820
might be strong for one person and not for
the other person.

94
00:05:18,820 --> 00:05:21,260
Because Kevin has this background
information

95
00:05:21,260 --> 00:05:26,000
that McKenzie's trustworthy, whereas Joe
does not have that background information.

96
00:05:28,090 --> 00:05:30,770
So the argument that Kevin gave in the
very short

97
00:05:30,770 --> 00:05:34,030
form in which it occurred, might be good
enough for him.

98
00:05:35,210 --> 00:05:38,740
Because of his background knowledge about
Mackenzie's worthiness.

99
00:05:38,740 --> 00:05:40,660
Course it's not good enough for Joe,

100
00:05:40,660 --> 00:05:43,970
because Joe doesn't have that background
knowledge.

101
00:05:43,970 --> 00:05:47,330
And that's just a fact about inductive
arguments in general.

102
00:05:47,330 --> 00:05:52,850
That they might be strong for some people,
and not so strong for other people.

103
00:05:54,570 --> 00:05:56,470
And we could make the argument

104
00:05:56,470 --> 00:06:00,510
even stronger, by looking at other
explanations.

105
00:06:00,510 --> 00:06:06,078
So what about Phillipa, well she sleeps
like a log, but maybe she was

106
00:06:06,078 --> 00:06:11,870
sleep walking and knocked the cookies over
when she was sleep walking.

107
00:06:11,870 --> 00:06:15,260
What about Kevin himself, how come he is
getting off the hook.

108
00:06:15,260 --> 00:06:19,680
Maybe he did it when he came back from
running because he had sweat in

109
00:06:19,680 --> 00:06:22,655
his eyes and didn't notice it and didn't

110
00:06:22,655 --> 00:06:26,150
hear the plate drop because he was so
tired.

111
00:06:27,980 --> 00:06:31,040
Maybe there was a burglar who came in, and
while they were stealing things, they

112
00:06:31,040 --> 00:06:32,795
knocked the cookies over and that made
noise,

113
00:06:32,795 --> 00:06:34,210
and so they got scared and ran away.

114
00:06:36,190 --> 00:06:38,720
Maybe it was Santa Claus.

115
00:06:38,720 --> 00:06:43,240
He has been known to eat cookies that are
left out on the counter for him.

116
00:06:45,160 --> 00:06:46,230
Maybe it was a ghost.

117
00:06:47,380 --> 00:06:50,338
These are all kinds of possible
explanations, and you could

118
00:06:50,338 --> 00:06:54,300
drive yourself crazy trying to rule out
every possible explanation.

119
00:06:54,300 --> 00:07:00,210
So to a certain point you have to decide,
when is the argument strong enough?

120
00:07:00,210 --> 00:07:02,060
And that depends on what you're going to
do with it.

121
00:07:03,400 --> 00:07:05,070
Now, I've said I think this is a good
argument.

122
00:07:06,420 --> 00:07:08,010
I think Timmy did it.

123
00:07:08,010 --> 00:07:09,640
The confession really is the kicker.

124
00:07:12,020 --> 00:07:15,010
But I'm not ready to give Timmy capital
punishment.

125
00:07:15,010 --> 00:07:17,419
I'm not ready to banish Timmy from the
house.

126
00:07:18,740 --> 00:07:20,945
There's some steps that would just be too

127
00:07:20,945 --> 00:07:24,220
harsh, in response to this amount of
evidence.

128
00:07:24,220 --> 00:07:28,000
However, I do think, that the evidence is
strong

129
00:07:28,000 --> 00:07:32,410
enough, that we can guide our action with
it.

130
00:07:32,410 --> 00:07:35,060
At least in ways that don't have a lot of
cost.

131
00:07:36,660 --> 00:07:37,040
So for

132
00:07:37,040 --> 00:07:40,840
example, my suggestion to Kevin and his
entire family is,

133
00:07:40,840 --> 00:07:44,100
don't put the cookies on the edge of the
counter.

134
00:07:45,800 --> 00:07:47,920
I mean, if Timmy's going to knock them off
and you

135
00:07:47,920 --> 00:07:51,620
don't want them knocked off, you gotta
accept some responsibility yourself.

136
00:07:53,140 --> 00:07:56,440
And this argument can be good enough, to
guide our action, about

137
00:07:56,440 --> 00:07:59,680
where to put the cookies on the counter,
when to leave them out.

138
00:07:59,680 --> 00:08:02,094
And when not to leave them out.
Even if

139
00:08:02,094 --> 00:08:05,690
it's not good enough, to justify capital
punishment.

140
00:08:05,690 --> 00:08:10,100
So one thing that we need to think about
in accessing, not just how strong an

141
00:08:10,100 --> 00:08:13,110
argument it is, but whether it's strong
enough,

142
00:08:13,110 --> 00:08:15,480
is what are we going to do with it?

143
00:08:15,480 --> 00:08:18,660
And what are the costs incurred if we're
wrong.

