1
00:00:00,000 --> 00:00:00,000
[BLANK_AUDIO].

2
00:00:00,000 --> 00:00:05,720
Hi.
Welcome to our Coursera course.

3
00:00:05,720 --> 00:00:07,380
This is the first time we've ever taught

4
00:00:07,380 --> 00:00:09,628
an online course, and we're very excited
about it.

5
00:00:09,628 --> 00:00:12,324
>> All right!

6
00:00:12,324 --> 00:00:12,340
[LAUGH].

7
00:00:12,340 --> 00:00:14,714
>> I'm Walter Sinnott-Armstrong from
Duke University.

8
00:00:14,714 --> 00:00:19,230
And my co-teacher is Ram Neta from the
University of North Carolina, Chapel Hill.

9
00:00:19,230 --> 00:00:20,698
Say hi, Ram.
>> Hi.

10
00:00:20,698 --> 00:00:25,940
>> Thanks.
This is going to be a great course.

11
00:00:25,940 --> 00:00:27,890
It's going to cover a lot of important

12
00:00:27,890 --> 00:00:31,980
practical issues, raise some fascinating
theoretical questions.

13
00:00:31,980 --> 00:00:34,630
We'll also try to have some fun, because
there're

14
00:00:34,630 --> 00:00:38,320
lots of wacky examples where people make
silly mistakes and

15
00:00:38,320 --> 00:00:40,080
arguments in everyday life.

16
00:00:40,080 --> 00:00:42,065
And we'll try to teach you how to avoid
those.

17
00:00:42,065 --> 00:00:47,220
The title of the course is Think Again.
How to reason and argue.

18
00:00:47,220 --> 00:00:50,530
And the title pretty much tells you what
the course is about.

19
00:00:50,530 --> 00:00:52,590
We'll try to teach you to think again
about a

20
00:00:52,590 --> 00:00:58,050
wide range of issues that effect your life
in various ways.

21
00:00:58,050 --> 00:01:00,000
We're not going to try to convert you to
our point

22
00:01:00,000 --> 00:01:03,030
of view or, or teach you to believe what
we believe.

23
00:01:03,030 --> 00:01:06,300
Instead, we want you to think in a new way
and

24
00:01:06,300 --> 00:01:09,980
in a deeper way about the issues that
matter to you most.

25
00:01:11,370 --> 00:01:13,690
The subtitle of the course, How to Reason

26
00:01:13,690 --> 00:01:16,930
and Argue, tells you that we're going to
focus on

27
00:01:16,930 --> 00:01:20,730
a particular type of thinking, namely
reasoning, because most

28
00:01:20,730 --> 00:01:25,380
people don't want to be arbitrary or have
unjustified beliefs.

29
00:01:25,380 --> 00:01:28,570
They want to have reasons for what they
think and do.

30
00:01:28,570 --> 00:01:29,786
But how do you get reasons?

31
00:01:29,786 --> 00:01:34,010
Well, we're going to approach reasons by
way of

32
00:01:34,010 --> 00:01:37,440
arguments, because arguments are just ways
to express reasons.

33
00:01:38,470 --> 00:01:41,920
And if you can understand arguments, you
can understand reasons.

34
00:01:41,920 --> 00:01:44,570
And if you can formulate good arguments,
you can have

35
00:01:44,570 --> 00:01:48,000
good reasons for the ways in which you
think and behave.

36
00:01:49,060 --> 00:01:53,750
So that's one way in which it's important
to understand arguments, namely to

37
00:01:53,750 --> 00:01:56,930
get better reasons for your own beliefs
and actions.

38
00:01:56,930 --> 00:01:58,780
But, another way in which it's very

39
00:01:58,780 --> 00:02:02,535
important to understand arguments, is to
avoid mistakes.

40
00:02:02,535 --> 00:02:04,790
because there's lots of charlatans out
there who are

41
00:02:04,790 --> 00:02:08,020
going to try to convince you to think the
way they

42
00:02:08,020 --> 00:02:09,960
want you to think and to behave the way

43
00:02:09,960 --> 00:02:12,990
they want you to behave by giving you bad
arguments.

44
00:02:12,990 --> 00:02:14,595
So you need to spot them and avoid them.

45
00:02:14,595 --> 00:02:18,896
Just think about a used car salesman who
tries to convince

46
00:02:18,896 --> 00:02:22,040
you to buy a, a car, because it looks
really cool

47
00:02:22,040 --> 00:02:26,270
and you'll look even cooler if you're
sitting in the car.

48
00:02:26,270 --> 00:02:29,950
Well, that might be a good reason to buy a
car and it might not.

49
00:02:29,950 --> 00:02:32,680
And you're going to have to figure out
which kinds

50
00:02:32,680 --> 00:02:35,660
of arguments to believe and which kinds
not to believe.

51
00:02:37,070 --> 00:02:41,560
Consider another example, say a lawyer in
a courtroom, and you're

52
00:02:41,560 --> 00:02:44,450
sitting in the jury, and they're going to
try to convince you

53
00:02:44,450 --> 00:02:48,810
either to find the defendant guilty or to
find the defendant not guilty,

54
00:02:48,810 --> 00:02:53,010
but either way, you don't want your
decision to be like flipping a coin.

55
00:02:53,010 --> 00:02:55,560
You want to have reasons for what you're
thinking.

56
00:02:55,560 --> 00:02:57,990
And for the verdict that you reach.

57
00:02:57,990 --> 00:02:59,065
as a member of the jury.

58
00:02:59,065 --> 00:03:04,020
Or, an evangelist tries to convert you to
their religious beliefs

59
00:03:04,020 --> 00:03:07,010
and to get you to give up your old
religious beliefs.

60
00:03:07,010 --> 00:03:09,510
Well, you don't want to make that kind of
a decision

61
00:03:09,510 --> 00:03:12,000
arbitrarily either because it's so
important.

62
00:03:12,000 --> 00:03:14,690
And then what about your personal life.

63
00:03:14,690 --> 00:03:18,010
You might have a friend who says let's go
for a cross-country trip.

64
00:03:18,010 --> 00:03:22,260
It'll be great.
Well, maybe it will and maybe it won't.

65
00:03:22,260 --> 00:03:24,470
But you don't want to commit yourself to
such

66
00:03:24,470 --> 00:03:27,730
a big endeavor without having thought it
through properly.

67
00:03:27,730 --> 00:03:30,690
How are we going to study arguments.

68
00:03:30,690 --> 00:03:33,780
Well, in this course we'll have four
parts.

69
00:03:33,780 --> 00:03:35,045
The first part,

70
00:03:35,045 --> 00:03:36,990
we'll teach you how to analyze arguments.

71
00:03:36,990 --> 00:03:38,880
That might seem really simple, you just

72
00:03:38,880 --> 00:03:40,950
read the passage and hear what they're
saying.

73
00:03:40,950 --> 00:03:46,460
But actually it's quite hard because some
passages or some sets of words,

74
00:03:46,460 --> 00:03:52,040
if we're talking about spoken language,
contain arguments and others don't.

75
00:03:52,040 --> 00:03:53,660
Here's an example.

76
00:03:53,660 --> 00:03:56,060
Consider a letter to the editor.

77
00:03:56,060 --> 00:03:59,000
Some letters to the editor don't have
arguments at all.

78
00:03:59,000 --> 00:04:00,200
They just

79
00:04:00,200 --> 00:04:05,600
say, thank people for having behaved in
nice ways or done nice things.

80
00:04:07,040 --> 00:04:11,170
On the other hand, other letters to the
editor include arguments.

81
00:04:11,170 --> 00:04:12,841
They try to convince you.

82
00:04:12,841 --> 00:04:16,910
To vote for a certain political candidate
for example.

83
00:04:16,910 --> 00:04:19,810
So you need to distinguish which passages

84
00:04:19,810 --> 00:04:22,940
include arguments and which passages don't
include arguments.

85
00:04:22,940 --> 00:04:25,780
Then, you need to look at those passages
and

86
00:04:25,780 --> 00:04:30,610
figure out which of the words, which parts
of those passages contain the argument.

87
00:04:30,610 --> 00:04:33,330
Then you need to separate out those parts,
put

88
00:04:33,330 --> 00:04:36,690
them in a certain order, which we'll call
standard form.

89
00:04:36,690 --> 00:04:39,970
And, often these arguments will have
missing

90
00:04:39,970 --> 00:04:41,990
parts and you'll have to supply those
missing

91
00:04:41,990 --> 00:04:44,280
parts or suppressed premises in order to
get

92
00:04:44,280 --> 00:04:47,060
a full picture of how the argument works.

93
00:04:47,060 --> 00:04:50,830
And that's what we'll do in part one.
Then in part two,

94
00:04:50,830 --> 00:04:54,165
once we've got the argument in shape, we
can start to evaluate it.

95
00:04:54,165 --> 00:04:59,120
But evaluations are going to depend a lot
on what the purpose of the argument is.

96
00:05:00,330 --> 00:05:04,170
Some arguments try to be valid in a
logical way

97
00:05:04,170 --> 00:05:04,290
[SOUND]

98
00:05:04,290 --> 00:05:06,350
and those are deductive arguments.

99
00:05:06,350 --> 00:05:08,320
So we'll start first by looking at
deductive

100
00:05:08,320 --> 00:05:11,350
arguments and the formal structure of
deductive arguments.

101
00:05:11,350 --> 00:05:15,490
We'll look at propositional logic, then
categorical logic.

102
00:05:15,490 --> 00:05:18,040
That'll be part two of the course.

103
00:05:18,040 --> 00:05:20,200
Then in part three, we'll look at a
different kind of

104
00:05:20,200 --> 00:05:24,560
argument, inductive arguments that don't
even try to be deductively valid.

105
00:05:25,760 --> 00:05:27,910
Here there are just a lot of different
kinds.

106
00:05:27,910 --> 00:05:30,679
So we'll look at statistical
generalizations,

107
00:05:30,679 --> 00:05:34,180
applying generalizations down to
particular cases.

108
00:05:34,180 --> 00:05:38,600
We'll look at inference to the best
explanation and arguments from analogy.

109
00:05:38,600 --> 00:05:42,670
We'll look at causal reasoning and
probability and decision making.

110
00:05:42,670 --> 00:05:47,460
So it'll be a lot of different types of
inductive arguments covered in part three.

111
00:05:49,110 --> 00:05:52,600
Then in part four, we'll look at
fallacies.

112
00:05:52,600 --> 00:05:53,910
These are common.

113
00:05:53,910 --> 00:05:55,920
The very tempting ways to make

114
00:05:55,920 --> 00:05:59,205
mistakes in arguments.
Some of them have to do with vaugeness.

115
00:05:59,205 --> 00:06:01,530
Others have to do with ambiguity.

116
00:06:01,530 --> 00:06:04,260
Some of them are irrelevance, like
arguments

117
00:06:04,260 --> 00:06:08,140
ad hominem and appeals to ignorance and
we'll

118
00:06:08,140 --> 00:06:10,632
also look at a major fallacy called
begging

119
00:06:10,632 --> 00:06:12,889
the question that people commit all the
time.

120
00:06:12,889 --> 00:06:14,969
And in the end, we'll teach you a

121
00:06:14,969 --> 00:06:19,449
general method for spotting and avoiding
these common mistakes.

122
00:06:19,449 --> 00:06:21,049
So that'll be part four.

123
00:06:21,049 --> 00:06:23,609
And at the end of each part, we'll have a

124
00:06:23,609 --> 00:06:27,689
short quiz with some questions to make
sure you understood.

125
00:06:27,689 --> 00:06:30,889
So we're very glad to have you in this
course, and

126
00:06:30,889 --> 00:06:35,540
we're very honored that there's so many
students in this course.

127
00:06:35,540 --> 00:06:36,960
But that raises one problem.

128
00:06:36,960 --> 00:06:40,560
Namely, we cannot answer emails from
students.

129
00:06:40,560 --> 00:06:42,770
So please do not email us individually.

130
00:06:43,860 --> 00:06:46,230
There will be discussion forums

131
00:06:46,230 --> 00:06:49,925
where you can go and talk to other
students about the material in the course.

132
00:06:49,925 --> 00:06:52,540
And I bet that if you go to those forums,

133
00:06:52,540 --> 00:06:55,370
not only will you get your questions
answered, but if you

134
00:06:55,370 --> 00:06:58,586
go to those forums and help answer other
people's questions, everybody

135
00:06:58,586 --> 00:07:01,902
will learn more and that's what this
course is all about.

136
00:07:01,902 --> 00:07:06,110
So, thanks very much for joining us on
this adventure and we hope you stick with

137
00:07:06,110 --> 00:07:10,290
it because we've got a lot of fun and a
lot of important things to cover.

138
00:07:10,290 --> 00:07:13,670
One final recommendation.

139
00:07:13,670 --> 00:07:16,080
We've done these lectures so that you can
just watch the

140
00:07:16,080 --> 00:07:19,980
lectures by themselves and do the
exercises and take the quizzes.

141
00:07:19,980 --> 00:07:23,820
However, if you're listening to a lecture
and you're

142
00:07:23,820 --> 00:07:26,790
having a little trouble understanding it,
or if you're

143
00:07:26,790 --> 00:07:29,575
really fascinated and you want to get more
detail,

144
00:07:29,575 --> 00:07:33,289
then there is an accompanying textbook
called, Understanding Arguments.

145
00:07:34,524 --> 00:07:38,800
by myself and Robert Fogelin.
Many, many, many of the best ideas

146
00:07:38,800 --> 00:07:42,195
in this course come from him, he's been a
leader in this field of understanding

147
00:07:42,195 --> 00:07:47,250
arguments for many decades and I owe awful
lot to him and I really appreciate that.

148
00:07:47,250 --> 00:07:49,830
So, I want to do a little shout out to
thanks to

149
00:07:49,830 --> 00:07:53,680
Robert Fogelin before we get started on
this course and the next lecture.

