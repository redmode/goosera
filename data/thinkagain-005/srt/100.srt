1
00:00:03,380 --> 00:00:09,030
Generalizations are not much use if you
can't apply them back to particular cases.

2
00:00:11,590 --> 00:00:16,660
It rains about 35% of the days in North
Carolina.

3
00:00:16,660 --> 00:00:19,330
Great, but is it going to rain tomorrow?

4
00:00:19,330 --> 00:00:20,150
That's what I want to know.

5
00:00:20,150 --> 00:00:23,120
I don't want to know the general
statistics.

6
00:00:23,120 --> 00:00:26,210
I want to know about tomorrow, cause I
gotta decide where to go to a

7
00:00:29,050 --> 00:00:29,465
picnic.

8
00:00:29,465 --> 00:00:33,010
65% of the snakes in this are are
poisonous.

9
00:00:33,010 --> 00:00:34,340
Great.

10
00:00:34,340 --> 00:00:38,270
But, I want to know whether the one I'm
stepping on right now is poisonous.

11
00:00:38,270 --> 00:00:40,249
I don't want to know the general
statistics.

12
00:00:41,670 --> 00:00:43,230
So we need some way to take these

13
00:00:43,230 --> 00:00:46,860
generalizations, and apply them down to
particular cases.

14
00:00:46,860 --> 00:00:48,370
And that's the job of a form of

15
00:00:48,370 --> 00:00:52,930
argument we're going to call, application
of the generalization.

16
00:00:52,930 --> 00:00:54,470
Not a great name,

17
00:00:54,470 --> 00:00:58,530
but they're usually called, statistical
syllogisms, and that's an even worse name.

18
00:00:58,530 --> 00:01:02,110
Because they're not really necessarily
statistical in a mathematical

19
00:01:02,110 --> 00:01:04,029
sense, because they don't have to have
numbers in them.

20
00:01:05,090 --> 00:01:08,370
And they're also not syllogisms like the
categorical syllogisms

21
00:01:08,370 --> 00:01:12,910
that you studied back in the section on
deductive arguments.

22
00:01:12,910 --> 00:01:16,520
So we're going to call them applications
of generalizations.

23
00:01:16,520 --> 00:01:19,410
And they just happen all the time.

24
00:01:19,410 --> 00:01:20,820
They really do.

25
00:01:20,820 --> 00:01:25,880
You might say, for example, I almost never
Like horror movies.

26
00:01:25,880 --> 00:01:28,330
And that's a horror movie.
So I don't want to go to it.

27
00:01:30,580 --> 00:01:32,350
because I'm not going to like it.

28
00:01:32,350 --> 00:01:34,952
Or you might say, I don't want to invest
in that restaurant

29
00:01:34,952 --> 00:01:38,509
because 80% of restaurants fail during the
first two or three years.

30
00:01:40,290 --> 00:01:44,770
And so, this is a restaurant that's new.
It's probably going to fail too.

31
00:01:44,770 --> 00:01:45,680
Or, how about this one?

32
00:01:46,750 --> 00:01:50,630
Most people who are on the track team are
pretty thin.

33
00:01:50,630 --> 00:01:51,130
Well

34
00:01:52,920 --> 00:01:55,280
now, you know there are some exceptions to
that, like the

35
00:01:55,280 --> 00:01:57,350
people who throw shot put that's what I
used to do.

36
00:01:59,630 --> 00:02:02,380
But still, it's true that most of them do,
so

37
00:02:02,380 --> 00:02:05,040
if you know that Sally is on the track
team.

38
00:02:06,130 --> 00:02:07,820
That's all you know about her.

39
00:02:07,820 --> 00:02:12,840
Then, you got some reason to believe that
Sally's probably thin.

40
00:02:12,840 --> 00:02:16,220
She might throw the shot put but there're
are many more people on the track

41
00:02:16,220 --> 00:02:22,000
team who run and jump and they're going to
be thin if they're any good at it.

42
00:02:23,530 --> 00:02:24,780
so.

43
00:02:24,780 --> 00:02:26,850
Although it might be wrong, it's a pretty
good

44
00:02:26,850 --> 00:02:30,210
bet that Sally's thin if she's on the
track team.

45
00:02:30,210 --> 00:02:31,790
And that's the kind of argument that

46
00:02:31,790 --> 00:02:35,270
we're going to call an application of a
generalization.

47
00:02:36,470 --> 00:02:40,590
Here's my favorite.
Am I wearing shoes right now?

48
00:02:40,590 --> 00:02:43,589
Can't see, can you?
Maybe I am, maybe I'm not.

49
00:02:44,630 --> 00:02:48,370
But most professors wear shoes when
they're teaching a class.

50
00:02:49,760 --> 00:02:53,860
Walter is a professor who's teaching a
class.

51
00:02:53,860 --> 00:02:57,550
So Walter is probably wearing shoes right
now.

52
00:02:57,550 --> 00:03:00,000
That's an application of a generalization.

53
00:03:01,280 --> 00:03:05,040
And has the same form Is the other
examples that we saw before.

54
00:03:06,620 --> 00:03:09,210
To see that form we can substitute

55
00:03:09,210 --> 00:03:12,190
variables for the terms in the English
argument.

56
00:03:13,370 --> 00:03:14,760
So we can substitute the

57
00:03:14,760 --> 00:03:20,490
letter F for the reference class, which is
the set of professors

58
00:03:20,490 --> 00:03:26,850
who were teaching Then we can substitute
the letter G for what's called the

59
00:03:26,850 --> 00:03:32,390
attribute class which is the people who
are wearing shoes.

60
00:03:34,080 --> 00:03:36,950
Then we can substitute the letter A

61
00:03:36,950 --> 00:03:39,800
usually in lower case for the individual
that

62
00:03:39,800 --> 00:03:41,270
we're talking about.

63
00:03:41,270 --> 00:03:44,190
In this case that individual is me,
Walter.

64
00:03:45,670 --> 00:03:50,240
And we can substitute X % for the
quantifier.

65
00:03:50,240 --> 00:03:57,310
If it's most or almost all or something
like that then X% takes its place.

66
00:03:57,310 --> 00:03:59,800
And then the form of the argument.

67
00:03:59,800 --> 00:04:04,850
That I just gave about wearing shoes is
simply that X% of F are

68
00:04:04,850 --> 00:04:11,420
G, a is F, therefore a is probably G.

69
00:04:11,420 --> 00:04:15,190
Notice that this application of a
generalization moves in

70
00:04:15,190 --> 00:04:19,470
the opposite direction from the
generalization from the sample.

71
00:04:20,480 --> 00:04:26,130
We might start off saying; well X% of the
F's in the sample are G.

72
00:04:26,130 --> 00:04:30,170
So X% of F's are G.
And that's generalization from a sample.

73
00:04:30,170 --> 00:04:36,820
But then we take the generalization in
that conclusion and use it as a premise.

74
00:04:36,820 --> 00:04:38,319
In its, in an application.

75
00:04:39,700 --> 00:04:44,840
So we say X% of F's are G, a is F,
therefore a is probably G.

76
00:04:46,092 --> 00:04:49,530
And that's why I said that we're going to
study up to

77
00:04:49,530 --> 00:04:52,640
generalizations, and then down from
generalizations

78
00:04:54,160 --> 00:04:55,680
in this part of the course.

79
00:04:55,680 --> 00:04:58,420
And when they work together, we can use
information

80
00:04:58,420 --> 00:05:02,450
about the sample To reach a conclusion
about the individual.

81
00:05:03,600 --> 00:05:05,300
Which is pretty useful.

82
00:05:05,300 --> 00:05:10,980
Notice also that these applications or
generalizations are inductive.

83
00:05:10,980 --> 00:05:14,990
They share all the features of inductive
arguments.

84
00:05:14,990 --> 00:05:18,700
First of all, they're invalid because it's
possible for

85
00:05:18,700 --> 00:05:22,070
the premises to be true and the conclusion
false.

86
00:05:22,070 --> 00:05:26,570
That is, it's possible that X% of F are G,
a

87
00:05:26,570 --> 00:05:31,030
isn't F, but it's not true that a is
probably G.

88
00:05:31,030 --> 00:05:32,760
There might be no chance at all that a is
G.

89
00:05:34,230 --> 00:05:40,760
Second, applications of generalizations
are defeasible.

90
00:05:40,760 --> 00:05:45,530
You can add additional information to the
premises that make the argument very weak.

91
00:05:45,530 --> 00:05:48,090
They might completely undermine the
argument.

92
00:05:48,090 --> 00:05:53,610
For example, even if you know that Walter
is a professor and most professors wear

93
00:05:53,610 --> 00:05:59,950
shoes while they're teaching, a little bit
of additional information might show you.

94
00:05:59,950 --> 00:06:05,340
That it's just not true, that Walter is
wearing shoes while he's teaching.

95
00:06:06,750 --> 00:06:10,520
By the way, I'm going to, well, you'll
have to decide

96
00:06:10,520 --> 00:06:12,630
for yourself whether I'm really putting my
shoes back on now.

97
00:06:15,020 --> 00:06:20,890
But the third feature of inductive
arguments, is that, they're strong.

98
00:06:20,890 --> 00:06:23,960
Or they can be strong, they can vary in
strength.

99
00:06:23,960 --> 00:06:31,050
So, even if, I don't have shoes on, it
still might be a fairly

100
00:06:31,050 --> 00:06:34,860
strong argument that almost all professors
wear shoes while they're teaching.

101
00:06:34,860 --> 00:06:39,500
Walter's a professor and he's teaching
therefore he probably has shoes on.

102
00:06:39,500 --> 00:06:40,520
That could be strong

103
00:06:40,520 --> 00:06:42,680
before you get that additional
information.

104
00:06:42,680 --> 00:06:44,900
Before you see my feet without shoes on.

105
00:06:44,900 --> 00:06:46,400
And now that you don't know whether I've
got

106
00:06:46,400 --> 00:06:49,520
shoes on, it can still be a strong
argument.

107
00:06:50,970 --> 00:06:57,360
So, we have arguments that are not valid,
and they're defeasible.

108
00:06:57,360 --> 00:07:01,990
And they vary in strength.
And that makes them inductive arguments.

109
00:07:02,990 --> 00:07:05,620
So how do we tell when an application

110
00:07:05,620 --> 00:07:08,370
of a generalization really is strong?

111
00:07:08,370 --> 00:07:13,150
That is, when does it provide strong
reasons for the conclusion?

112
00:07:14,390 --> 00:07:16,780
Now the first standard should be obvious.

113
00:07:16,780 --> 00:07:21,220
It's the same for generalizations from
samples

114
00:07:21,220 --> 00:07:23,610
the premises have to be true and
justified.

115
00:07:27,100 --> 00:07:29,490
If it's not true that I'm a professor or
if it's not true that I'm teaching

116
00:07:29,490 --> 00:07:32,040
or you have no reason to believe you're

117
00:07:32,040 --> 00:07:34,940
not justified in believing that I'm a
professor or

118
00:07:34,940 --> 00:07:38,750
I'm teaching Then, the argument that we've
been

119
00:07:38,750 --> 00:07:41,060
looking at can't give you a good reason to

120
00:07:41,060 --> 00:07:43,660
believe that I'm wearing shoes because I
just

121
00:07:43,660 --> 00:07:45,770
don't fall in the classes that we're
talking about.

122
00:07:47,040 --> 00:07:47,600
Secondly.

123
00:07:49,190 --> 00:07:52,480
A standard that's specific to these kinds
of arguments is

124
00:07:52,480 --> 00:07:56,620
that the strength of the argument varies
with how big X is.

125
00:07:56,620 --> 00:08:02,460
If X is 99%, so 99% of F's are G's, and a
is

126
00:08:02,460 --> 00:08:06,000
an F, that's a pretty strong argument that
this a is a G.

127
00:08:07,350 --> 00:08:12,520
But if it's 60%, then it's not a very
strong argument.

128
00:08:12,520 --> 00:08:15,330
So 99's going to be stronger than 90,
which is stronger

129
00:08:15,330 --> 00:08:17,900
than 80, which is stronger than 70, which
is stronger

130
00:08:17,900 --> 00:08:18,740
than 60.

131
00:08:18,740 --> 00:08:24,550
And the strength of the inductive argument
can vary as the percentage X varies.

132
00:08:26,050 --> 00:08:30,510
And notice if X is 10%, then it

133
00:08:30,510 --> 00:08:34,020
becomes a pretty strong argument for the
opposite conclusion.

134
00:08:34,020 --> 00:08:38,730
If only 10% of professors wear shoes when
they're teaching, And I'm

135
00:08:38,730 --> 00:08:43,440
a professor who's teaching, then it's
pretty likely I'm not wearing shoes.

136
00:08:45,170 --> 00:08:47,050
And that's stronger than if it's 20, and
30, and 40.

137
00:08:47,050 --> 00:08:49,628
And when you get close to the middle, the

138
00:08:49,628 --> 00:08:52,830
50%, if 50% of professors wear shoes when
they're

139
00:08:52,830 --> 00:08:57,360
teaching, and I'm a professor, then you
can't really

140
00:08:57,360 --> 00:08:59,820
reach a conclusion one way or the other
about it.

141
00:08:59,820 --> 00:09:01,090
Whether I'm wearing shoes.

142
00:09:01,090 --> 00:09:03,900
At least, not on the basis of that
evidence.

143
00:09:03,900 --> 00:09:05,515
Not on the basis of that argument.

144
00:09:05,515 --> 00:09:11,310
Okay, so a second standard for assessing
applications

145
00:09:11,310 --> 00:09:15,108
of generalizations is to figure out what
the percentage is.

146
00:09:15,108 --> 00:09:19,595
But, another crucial.

147
00:09:20,790 --> 00:09:24,740
Feature of applications and
generalizations, is

148
00:09:24,740 --> 00:09:27,460
that there can be conflicting reference
classes.

149
00:09:28,700 --> 00:09:34,390
So it might be true that 95% of professors
wear shoes when they're teaching.

150
00:09:35,450 --> 00:09:36,780
But I'm not just any

151
00:09:36,780 --> 00:09:40,580
old professor, I'm an online professor.

152
00:09:40,580 --> 00:09:43,900
This course is online, and that's very
different.

153
00:09:43,900 --> 00:09:46,430
Because most professors wear shoes when
they teach,

154
00:09:46,430 --> 00:09:49,230
because the students we'll see their bare
feet.

155
00:09:49,230 --> 00:09:53,450
But you can't see my bare feet, so maybe,
it turns out that

156
00:09:53,450 --> 00:09:57,640
a lot of online professors are really not
wearing shoes when they teach.

157
00:09:58,680 --> 00:09:59,650
So.

158
00:09:59,650 --> 00:10:01,910
That's a conflicting reference class.

159
00:10:01,910 --> 00:10:04,480
It's a different reference class that
conflicts because it

160
00:10:04,480 --> 00:10:08,740
points to a different conclusion than the
original reference class.

161
00:10:08,740 --> 00:10:12,380
The original reference class was
professors who are teaching.

162
00:10:12,380 --> 00:10:15,770
The conflicting reference class is online
professors who

163
00:10:15,770 --> 00:10:19,710
are teaching, or professors who are
teaching online.

164
00:10:19,710 --> 00:10:23,340
When you run into conflicting reference
classes, as a general

165
00:10:23,340 --> 00:10:26,940
rule what you ought to do is look at the
smallest of

166
00:10:26,940 --> 00:10:28,220
those reference classes.

167
00:10:28,220 --> 00:10:31,310
because that's usually going to give you a
better estimate of

168
00:10:31,310 --> 00:10:34,570
how likely it is that this individual has
the general attribute.

169
00:10:34,570 --> 00:10:37,690
That is, how likely it is that a is G.

170
00:10:39,470 --> 00:10:42,590
So, if you want to know whether Walter is

171
00:10:42,590 --> 00:10:45,630
wearing shoes, you shouldn't look at the
broad class

172
00:10:45,630 --> 00:10:48,160
of all professors, but should instead look
at

173
00:10:48,160 --> 00:10:52,060
the narrow class of online professors, if
the course

174
00:10:52,060 --> 00:10:54,850
that he's teaching right now is an online
course.

175
00:10:56,300 --> 00:10:59,040
But there's a problem that you might run
into also.

176
00:10:59,040 --> 00:11:03,370
As you get a narrower and narrower
reference class, you sometimes just

177
00:11:03,370 --> 00:11:07,940
don't have enough data to figure out what
the exact percentage is.

178
00:11:07,940 --> 00:11:11,020
How do you know what the percentage of
professors

179
00:11:11,020 --> 00:11:14,450
who are teaching online is, that are
wearing shoes.

180
00:11:14,450 --> 00:11:16,240
How many of them are wearing shoes?

181
00:11:16,240 --> 00:11:17,910
I've never seen a survey.

182
00:11:17,910 --> 00:11:22,160
I've seen lots of professors give regular
lectures, but you rarely see the feet

183
00:11:22,160 --> 00:11:24,140
of online professors, so you might not

184
00:11:24,140 --> 00:11:27,190
have enough information to apply that
generalization.

185
00:11:28,810 --> 00:11:33,000
So, while the narrower reference class of
two

186
00:11:33,000 --> 00:11:36,930
reference classes that conflict Can be
more accurate.

187
00:11:36,930 --> 00:11:39,300
It can also be problematic if you don't

188
00:11:39,300 --> 00:11:43,240
have enough information to support the
premise that

189
00:11:43,240 --> 00:11:44,970
says that a certain percentage of that

190
00:11:44,970 --> 00:11:48,680
class, that F, have the general attribute
G.

191
00:11:49,700 --> 00:11:51,880
The fallacy of overlooking conflicting
reference

192
00:11:51,880 --> 00:11:53,530
classes can be a little confusing.

193
00:11:53,530 --> 00:11:56,490
But, it's very important.
So, let's look at another example.

194
00:11:56,490 --> 00:11:58,659
And this time, let's focus on a medical
example.

195
00:12:00,320 --> 00:12:04,050
Let's suppose that, 90% of the people with
a

196
00:12:04,050 --> 00:12:08,151
certain condition, certain medical
condition, a certain illness, die.

197
00:12:08,151 --> 00:12:09,074
And

198
00:12:09,074 --> 00:12:14,980
Bob has that illness.
So it looks very likely that Bob will die.

199
00:12:16,030 --> 00:12:18,140
And this is an application of

200
00:12:18,140 --> 00:12:21,130
the generalization About people with this
illness.

201
00:12:22,640 --> 00:12:28,320
But now suppose, we find out that most
people catch this illness

202
00:12:28,320 --> 00:12:34,330
when they're old, but Bob is quite young.
So,

203
00:12:34,330 --> 00:12:40,030
it turns out that young people with this
illness usually survive.

204
00:12:40,030 --> 00:12:43,570
Matter of fact Only about 20% of the
people with this

205
00:12:43,570 --> 00:12:47,480
illness who get it when they're young, die
from the illness.

206
00:12:47,480 --> 00:12:50,790
But Bob is young and has this illness.

207
00:12:50,790 --> 00:12:54,250
So now we can reach the conclusion that he
probably won't die from the illness.

208
00:12:55,950 --> 00:12:56,540
But wait a minute.

209
00:12:57,660 --> 00:12:59,919
We may have another conflicting reference
class.

210
00:13:00,980 --> 00:13:05,840
because Bob has a heart condition.
And it turns out that even young people,

211
00:13:05,840 --> 00:13:11,170
when they catch this illness, if they have
a heart condition they usually die.

212
00:13:11,170 --> 00:13:15,180
Matter of fact, 80% of the people with
this illness who

213
00:13:15,180 --> 00:13:19,099
have a heart condition, and are young, die
from this illness.

214
00:13:20,320 --> 00:13:25,820
So Bob, who is young and has this illness
and also has a heart condition,

215
00:13:26,850 --> 00:13:28,280
would probably die from this illness.

216
00:13:29,580 --> 00:13:35,080
But wait a minute, it turns out that
there's a new treatment, and

217
00:13:36,320 --> 00:13:40,970
of the people who were given this
treatment, only about Thirty percent

218
00:13:40,970 --> 00:13:44,690
of them die even if they're young, with
this illness, with a heart

219
00:13:44,690 --> 00:13:48,332
condition, and Bob lives in an area where
he can get the treatment.

220
00:13:48,332 --> 00:13:52,220
So now it looks

221
00:13:52,220 --> 00:13:54,930
like there's only a 30% chance that Bob
will die from this illness.

222
00:13:56,100 --> 00:14:00,310
So what's happening here is as we get more
and more informaiton The

223
00:14:00,310 --> 00:14:04,520
likelihood of Bob dying from this illness
starts out really high and then

224
00:14:04,520 --> 00:14:06,860
it goes low, and then it goes high again
and then it goes

225
00:14:06,860 --> 00:14:11,410
low again, and it goes back and forth as
we get more information.

226
00:14:14,050 --> 00:14:17,780
And then the question arises, Which

227
00:14:17,780 --> 00:14:20,490
of these different generalizations should
we use

228
00:14:20,490 --> 00:14:23,510
to figure out how likely it is that Bob
will die from the illness?

229
00:14:25,090 --> 00:14:29,620
And the answer here, as with most cases of
conflicting reference classes,

230
00:14:29,620 --> 00:14:32,998
is that we ought to look at the narrowest
class that we can.

231
00:14:32,998 --> 00:14:36,130
because if we know there's a treatment.

232
00:14:36,130 --> 00:14:39,140
And Bob is young and he has a heart
condition and he

233
00:14:39,140 --> 00:14:43,830
also has this illness and we put all that
together and compare him to

234
00:14:43,830 --> 00:14:47,410
other people who are young with this
illness and a heart condition who can get

235
00:14:47,410 --> 00:14:50,810
the treatment and look at how many of them
have died in order to

236
00:14:50,810 --> 00:14:56,260
form an estimate of how likely it is that
Bob will die from this illness.

237
00:14:56,260 --> 00:14:59,640
So we always want to look at the narrowest
reference class that

238
00:14:59,640 --> 00:15:04,230
we can in order to get the best estimate
of the probability

239
00:15:04,230 --> 00:15:04,970
in the conclusion.

240
00:15:07,770 --> 00:15:10,950
But then there's a problem.
It might not be very many people.

241
00:15:10,950 --> 00:15:12,630
Remember its a new treatment.

242
00:15:12,630 --> 00:15:16,450
So there are not very many people who are
young who have this illness to begin with.

243
00:15:16,450 --> 00:15:19,780
Remember most of the people with the
illness are old.

244
00:15:19,780 --> 00:15:22,260
And the treatment's new so it hasn't been
tried on

245
00:15:22,260 --> 00:15:26,920
very many young people and, of course
young people don't often

246
00:15:26,920 --> 00:15:30,200
have heart conditions, so it might be very
difficult to

247
00:15:30,200 --> 00:15:33,190
find enough people who were like Bob in
all the essentials

248
00:15:33,190 --> 00:15:35,860
respects in order to determine whether or

249
00:15:35,860 --> 00:15:38,480
not Bob would probably die from this
illness.

250
00:15:38,480 --> 00:15:40,790
So there is a kind of a tension here.

251
00:15:40,790 --> 00:15:44,950
More information gives us more accuracy
but only if

252
00:15:44,950 --> 00:15:48,105
we have enough information to be justified
in trusting the

253
00:15:48,105 --> 00:15:53,300
premises of the application that we're
looking at and

254
00:15:53,300 --> 00:15:58,860
that's one of the tricks in figuring out
how to estimate.

255
00:15:58,860 --> 00:16:03,580
Whether or not Bob is likely to die from
the illness.

256
00:16:03,580 --> 00:16:05,960
The same points apply to all kinds of
examples.

257
00:16:06,970 --> 00:16:11,330
You know, if you want to know the climate
in the area, that's

258
00:16:11,330 --> 00:16:15,810
going to be a generalization about days
around here at this time of year.

259
00:16:16,990 --> 00:16:20,510
But if you want to know the weather
tomorrow you need to apply it.

260
00:16:20,510 --> 00:16:24,090
And then you're going to need to look at
specifically what

261
00:16:24,090 --> 00:16:27,370
the weather was yesterday.
Specifically what the humidity is.

262
00:16:27,370 --> 00:16:29,150
The more information you can build in, the

263
00:16:29,150 --> 00:16:32,480
better estimate of what the weather's
going to be tomorrow.

264
00:16:33,890 --> 00:16:38,320
Or, if you want to bet on a sport's team.
You say, well, they're a very good team.

265
00:16:38,320 --> 00:16:40,780
They've won most of their games in the
last five years.

266
00:16:40,780 --> 00:16:41,610
But wait a minute.

267
00:16:41,610 --> 00:16:45,640
Maybe all their players left, and now it's
not likely that they're going to win.

268
00:16:45,640 --> 00:16:48,400
But wait a minute, they got new players
that are even better.

269
00:16:48,400 --> 00:16:49,330
That makes it more likely

270
00:16:49,330 --> 00:16:50,390
they're going to win.

271
00:16:50,390 --> 00:16:53,160
And, again, the information can make the
probability go

272
00:16:53,160 --> 00:16:55,660
low, and then high, and then low, and then
high.

273
00:16:55,660 --> 00:16:59,610
And you have to get the most specific
information you can to get

274
00:16:59,610 --> 00:17:02,960
the most accurate estimate of how likely
it is that this team will win.

275
00:17:04,250 --> 00:17:09,330
But you might not have enough information
about this team with these players under

276
00:17:09,330 --> 00:17:15,510
these circumstances because we just have
period of cases we need to observe.

277
00:17:15,510 --> 00:17:19,840
So here's what the medical example this
can be get tension between one in

278
00:17:19,840 --> 00:17:22,696
the most precise, the most smallest
reference

279
00:17:22,696 --> 00:17:26,310
class among the different conflicting
reference classes.

280
00:17:26,310 --> 00:17:28,720
And yet, you need premises that you

281
00:17:28,720 --> 00:17:32,020
have enough information that you can
justify them.

282
00:17:32,020 --> 00:17:35,870
And if you can reach that perfect point,
and you've got enough information to

283
00:17:35,870 --> 00:17:40,840
justify the premises, but that premise is
specific enough so that it has all

284
00:17:40,840 --> 00:17:43,200
the relevant information about the case.

285
00:17:43,200 --> 00:17:46,500
That's when you're going to have the best
outcome.

286
00:17:46,500 --> 00:17:48,500
Our applications of generalizations.

287
00:17:48,500 --> 00:17:48,500
[BLANK_AUDIO]

