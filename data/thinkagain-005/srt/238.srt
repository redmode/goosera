1
00:00:02,690 --> 00:00:07,260
The rules that we've looked at so far
apply to every use of language.

2
00:00:07,260 --> 00:00:14,590
But we want to focus on those rules that
are important specifically to arguments.

3
00:00:14,590 --> 00:00:17,960
At least normally, misspelling or
mispronouncing

4
00:00:17,960 --> 00:00:20,060
a word doesn't affect an argument.

5
00:00:20,060 --> 00:00:24,130
Of course, if you pronounce one word so
that it sounds like

6
00:00:24,130 --> 00:00:27,570
another word, it can be very confusing and
people won't understand you.

7
00:00:27,570 --> 00:00:29,570
Or if you misspell it so badly they don't
know

8
00:00:29,570 --> 00:00:33,103
what word you wanted, the argument is not
going to work.

9
00:00:33,103 --> 00:00:36,346
But normally misspelling and
mispronunciation

10
00:00:36,346 --> 00:00:38,790
don't make the argument bad.

11
00:00:38,790 --> 00:00:41,710
Instead, what affects the argument is
what's

12
00:00:41,710 --> 00:00:45,380
going to affect the proposition that's
getting expressed.

13
00:00:45,380 --> 00:00:48,440
The meaning of the sentence, because
that's going to affect whether

14
00:00:48,440 --> 00:00:53,314
the premises justify the conclusion or
whether they explain the conclusion.

15
00:00:53,314 --> 00:00:56,394
and so, we need to focus on those rules of
language

16
00:00:56,394 --> 00:01:00,920
that in particular deal with the meanings
of words and of sentences.

17
00:01:02,210 --> 00:01:03,660
So what's meaning?

18
00:01:03,660 --> 00:01:05,830
Well, one thing I can tell you is we're
not

19
00:01:05,830 --> 00:01:09,040
going to be talking about the meaning of
life in this course.

20
00:01:09,040 --> 00:01:11,680
The meaning of life is a totally different
topic.

21
00:01:11,680 --> 00:01:15,190
We're also not going to be talking about
whether clouds mean rain in the

22
00:01:15,190 --> 00:01:18,590
sense of giving some indication or
evidence that there's going to be rain.

23
00:01:19,640 --> 00:01:24,300
We're concerned with the meanings of words
and sentences in language.

24
00:01:24,300 --> 00:01:27,910
We're concerned with linguistic meaning in
particular.

25
00:01:27,910 --> 00:01:31,200
So, how are we going to understand
linguistic meaning?

26
00:01:31,200 --> 00:01:33,090
Well one way to think about the meanings
of

27
00:01:33,090 --> 00:01:35,670
words and sentences is to ask how you
would

28
00:01:35,670 --> 00:01:38,820
explain them to someone who doesn't
understand them, like

29
00:01:38,820 --> 00:01:41,860
a small child or someone who doesn't speak
this language.

30
00:01:42,906 --> 00:01:44,640
Well if somebody said,

31
00:01:44,640 --> 00:01:45,830
what's the meaning of the word chair?

32
00:01:45,830 --> 00:01:48,790
You might go, well when I use the word
chair,

33
00:01:48,790 --> 00:01:51,530
I'm referring to these things you know,
something like this.

34
00:01:53,170 --> 00:01:58,747
when I say, I am sitting on the chair,
what I mean is my body is above

35
00:01:58,747 --> 00:02:04,380
the chair.
And so, that approach to teaching language

36
00:02:04,380 --> 00:02:09,170
suggests to many people that what words
mean is the same as what they refer to.

37
00:02:10,230 --> 00:02:14,680
and what sentences mean are the facts that
they try to describe.

38
00:02:14,680 --> 00:02:16,250
Now that view of language is often

39
00:02:16,250 --> 00:02:20,660
called the referential or descriptive view
of language.

40
00:02:20,660 --> 00:02:23,640
So is this referential or descriptive
theory adequate?

41
00:02:23,640 --> 00:02:25,610
No, no, no, no!

42
00:02:25,610 --> 00:02:26,790
Absolutely not!

43
00:02:27,860 --> 00:02:32,270
That theory does not cover a lot of
language that's extremely important.

44
00:02:32,270 --> 00:02:34,680
Just think about greeting someone in the
morning.

45
00:02:34,680 --> 00:02:36,150
Hello!

46
00:02:36,150 --> 00:02:39,379
Are you referring to an object when you
say, hello?

47
00:02:41,110 --> 00:02:45,730
Or think about the word not in a, in a
sentence, and that the word not is

48
00:02:45,730 --> 00:02:48,165
going to be crucial to arguments, but does
the word

49
00:02:48,165 --> 00:02:51,190
not refer to something separate from the
other things?

50
00:02:51,190 --> 00:02:55,000
Well, if it did, then there would be more
objects in the room when

51
00:02:55,000 --> 00:02:58,090
I'm not sitting on the chair than when I
am sitting on the chair.

52
00:02:59,520 --> 00:02:59,750
So you

53
00:02:59,750 --> 00:03:04,754
cannot understand the meanings of many
words, like hello

54
00:03:04,754 --> 00:03:09,260
or not or for that matter, am or sitting.

55
00:03:09,260 --> 00:03:14,850
Many words don't fit this referential or
descriptive theory of meaning.

56
00:03:14,850 --> 00:03:18,110
So, if meaning is not referential
description, what is it?

57
00:03:18,110 --> 00:03:20,498
Well here we're going to take our cue from
Ludwig

58
00:03:20,498 --> 00:03:25,218
Wittgenstein, the great 20th century
Austrian philosopher, who argued

59
00:03:25,218 --> 00:03:27,048
that meaning is use.

60
00:03:27,048 --> 00:03:30,718
If you want to understand the meaning of
the word

61
00:03:30,718 --> 00:03:33,053
hello, you don't look for some object that
it

62
00:03:33,053 --> 00:03:34,595
refers to, you ask how is it used and

63
00:03:34,595 --> 00:03:36,890
the answer is obvious, it's used to greet
people.

64
00:03:37,960 --> 00:03:42,800
If you want to understand the meaning of
the question, where's the library?

65
00:03:42,800 --> 00:03:46,000
The answer is, well, that question is used
to

66
00:03:46,000 --> 00:03:49,010
inquire about the library, to ask where
the library is.

67
00:03:49,010 --> 00:03:50,708
If you want to

68
00:03:50,708 --> 00:03:55,095
understand the meaning of an imperative
like, gimme a pizza.

69
00:03:55,095 --> 00:04:01,510
Then that is a way of ordering a pizza.
You're using that phrase to order a pizza.

70
00:04:01,510 --> 00:04:04,790
So the meaning of the phrase is given by
the way those

71
00:04:04,790 --> 00:04:09,270
words are used in normal situations by
competent speakers of the language.

72
00:04:09,270 --> 00:04:11,590
Now, this point about meaning being used

73
00:04:11,590 --> 00:04:13,970
is very important to the language of
argument.

74
00:04:15,470 --> 00:04:15,710
As we

75
00:04:15,710 --> 00:04:18,670
saw the word not does not refer to an

76
00:04:18,670 --> 00:04:22,710
additional object and the same goes for
the word and.

77
00:04:22,710 --> 00:04:26,560
When you say, I am sitting in the chair
and I am

78
00:04:26,560 --> 00:04:30,668
in the office, the word and doesn't refer
to an object either.

79
00:04:30,668 --> 00:04:36,720
So if we want to understand what the word
and is used to do then we have

80
00:04:36,720 --> 00:04:40,830
to think about what people do with it.
And what

81
00:04:40,830 --> 00:04:43,660
they do is they conjoint different
sentences.

82
00:04:43,660 --> 00:04:49,500
They don't add an extra object or an extra
fact, they just conjoin sentences and form

83
00:04:49,500 --> 00:04:53,404
a whole sentence out of two parts, each of
which was a sentence to begin with.

84
00:04:53,404 --> 00:04:59,358
Of course, the word and is not always used
to conjoin sentences.

85
00:04:59,358 --> 00:05:03,390
We can say things like Roberto and Olivia
went to Sao Paulo together.

86
00:05:03,390 --> 00:05:06,440
And Roberto's not a sentence,

87
00:05:06,440 --> 00:05:12,196
Olivia's not a sentence, they're people,
not sentences.

88
00:05:12,196 --> 00:05:15,430
And there weren't three things that went
to Sao Paulo together.

89
00:05:15,430 --> 00:05:18,673
Roberto, Olivia, and this third thing and,
whatever that is.

90
00:05:18,673 --> 00:05:20,743
Still, the word and is used to conjoin.

91
00:05:20,743 --> 00:05:23,647
In this case, it's used to conjoin things
or people.

92
00:05:23,647 --> 00:05:31,651
In the previous examples it was used to
join, conjoin

93
00:05:31,651 --> 00:05:35,430
sentences.

94
00:05:35,430 --> 00:05:37,950
But if we want to know what the word and
means,

95
00:05:37,950 --> 00:05:40,384
we shouldn't be looking for some object
that it refers to.

96
00:05:40,384 --> 00:05:45,170
Instead, we should be describing how it's
used and what it's used to do.

97
00:05:45,170 --> 00:05:51,490
Namely, it's used to conjoin.
And when we look more closely at

98
00:05:51,490 --> 00:05:56,570
the uses of language, we see that language
is used in a lot of different ways.

99
00:05:56,570 --> 00:05:59,475
Use is diverse.
Just take a simple example.

100
00:05:59,475 --> 00:06:02,790
I'd like to give you a little piece of
personal advice.

101
00:06:02,790 --> 00:06:05,160
You ought to floss your teeth every day.

102
00:06:06,420 --> 00:06:10,010
So what did I just do?
I uttered a bunch of words.

103
00:06:10,010 --> 00:06:15,825
I made physical motion in the air.
But in addition, those words were

104
00:06:15,825 --> 00:06:21,580
meaningful, you ought to floss your teeth

105
00:06:21,580 --> 00:06:22,750
every day.

106
00:06:22,750 --> 00:06:26,820
Each of those words is meaningful in the
language, and I put them

107
00:06:26,820 --> 00:06:31,820
together in an order that was grammatical,
so the whole thing makes sense.

108
00:06:31,820 --> 00:06:34,310
When you make a meaningful utterance like
that, we're

109
00:06:34,310 --> 00:06:37,060
going to say you perform a linguistic act
and there's

110
00:06:37,060 --> 00:06:39,970
a linguistic level of the use of language
that

111
00:06:39,970 --> 00:06:44,020
we will study in one lecture, the next
lecture.

112
00:06:44,020 --> 00:06:46,840
But in addition, when I said you ought to
floss your teeth

113
00:06:46,840 --> 00:06:51,040
every day, I also gave you a piece of
friendly advice.

114
00:06:51,040 --> 00:06:52,730
I advised you.

115
00:06:52,730 --> 00:06:57,590
And notice that, even if you don't follow
my advice, I still advised you.

116
00:06:57,590 --> 00:07:00,090
And, if you don't follow my advice and
your teeth rot

117
00:07:00,090 --> 00:07:02,935
out, then I can say, look, you should've
listened to me.

118
00:07:02,935 --> 00:07:05,320
because I advised you to floss your teeth
every day.

119
00:07:05,320 --> 00:07:11,860
So, the speech act level occurs even when
it doesn't affect your actions at all.

120
00:07:11,860 --> 00:07:15,530
That's the second level of use of
language, the speech act level,

121
00:07:15,530 --> 00:07:19,790
and we'll talk about that in more detail
two lectures from now.

122
00:07:19,790 --> 00:07:24,620
The third level of language has to do with
the production of certain effects.

123
00:07:24,620 --> 00:07:28,410
Maybe when I say, you ought to floss your
teeth everyday, I persuade you.

124
00:07:28,410 --> 00:07:30,640
And what that means is I bring about a
certain

125
00:07:30,640 --> 00:07:35,080
effect on your behavior or your thought or
your attitudes.

126
00:07:36,440 --> 00:07:37,510
Well,

127
00:07:37,510 --> 00:07:40,730
that's what we're going to call the
conversational level.

128
00:07:40,730 --> 00:07:43,320
And conversational acts are the acts of
bringing

129
00:07:43,320 --> 00:07:46,000
about those effects and persuasion is one
example.

130
00:07:47,180 --> 00:07:50,020
So now we have three levels of language.

131
00:07:50,020 --> 00:07:52,170
We have the linguistic level, which is

132
00:07:52,170 --> 00:07:55,770
the meaningful utterance, producing a
meaningful utterance.

133
00:07:55,770 --> 00:07:57,960
We have the speech act level.

134
00:07:57,960 --> 00:08:02,520
Advising is a nice paradigm of that, which
can be accomplished even if you're

135
00:08:02,520 --> 00:08:03,390
not persuaded.

136
00:08:03,390 --> 00:08:06,990
And we have persuading you, which is the
conversational level.

137
00:08:06,990 --> 00:08:08,550
So there's the linguistic level, the

138
00:08:08,550 --> 00:08:11,380
speech act level, and the conversational
level.

139
00:08:11,380 --> 00:08:13,580
And each of these levels will be explored
in

140
00:08:13,580 --> 00:08:17,530
more detail in one of the next three
lectures.

141
00:08:17,530 --> 00:08:20,270
But here's a little warning.
And you have a choice.

142
00:08:20,270 --> 00:08:23,410
The material in the next three lectures is
a little bit more

143
00:08:23,410 --> 00:08:27,690
abstract and difficult than some of the
things we've been through so far.

144
00:08:27,690 --> 00:08:30,730
Now I think it's fascinating.
We're going to try to make it fun.

145
00:08:30,730 --> 00:08:33,655
And it's really important to understand
how your language works.

146
00:08:33,655 --> 00:08:36,940
But it's not absolutely essential to the
things that

147
00:08:36,940 --> 00:08:39,060
we're going to cover in the rest of this
course.

148
00:08:39,060 --> 00:08:40,730
So it's up to you.

149
00:08:40,730 --> 00:08:42,570
You can listen to these three lectures if

150
00:08:42,570 --> 00:08:45,710
you want and there will be some exercises
throughout

151
00:08:45,710 --> 00:08:48,500
those lectures that will test your
understanding, but

152
00:08:48,500 --> 00:08:50,900
none of that material's going to be on the
quiz.

153
00:08:50,900 --> 00:08:53,140
So you can listen to the next three
lectures, and

154
00:08:53,140 --> 00:08:57,210
I hope you do, and I hope you enjoy them,
but you don't have to in order to be able

155
00:08:57,210 --> 00:08:59,990
to go through the rest of the course and
do

156
00:08:59,990 --> 00:09:01,680
well on the quiz at the end of this part.

