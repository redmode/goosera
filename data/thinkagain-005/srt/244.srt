1
00:00:03,220 --> 00:00:05,160
So now we've discussed two levels of
language.

2
00:00:05,160 --> 00:00:08,344
The linguistic level and the speech act
level.

3
00:00:08,344 --> 00:00:11,070
In this lecture, we want to look at the
third level of language.

4
00:00:11,070 --> 00:00:13,445
Nominally the level of conversational
acts.

5
00:00:13,445 --> 00:00:16,079
And the basic idea is really simple.

6
00:00:16,079 --> 00:00:19,530
We use language to bring about a change in
the world.

7
00:00:19,530 --> 00:00:23,960
For example, I might turn to a friend and
say, could you loan me your car?

8
00:00:25,110 --> 00:00:26,530
Well, what am I doing?

9
00:00:26,530 --> 00:00:29,090
I'm performing a speech act of requesting
or

10
00:00:29,090 --> 00:00:31,500
asking a favor.
Something like that.

11
00:00:32,660 --> 00:00:34,360
But am I doing it just for its own sake?

12
00:00:34,360 --> 00:00:37,860
Did I ask a favor just in order to be
asking a favor?

13
00:00:37,860 --> 00:00:39,710
Like it was fun to ask a favor?

14
00:00:39,710 --> 00:00:40,640
No.

15
00:00:40,640 --> 00:00:43,680
I was asking a favor, to bring about a
certain effect.

16
00:00:43,680 --> 00:00:47,300
I wanted him to hand over the keys to his
car so I could use it.

17
00:00:47,300 --> 00:00:51,520
And I wanted him to give me permission to
use his car, so I could do it legally.

18
00:00:51,520 --> 00:00:54,430
So I'm trying to bring about a change, not
only in the physical

19
00:00:54,430 --> 00:00:57,240
location of the keys, but also in the
legal

20
00:00:57,240 --> 00:01:00,490
rights that I have with regard to his car.

21
00:01:00,490 --> 00:01:03,570
So I'm trying to bring about a change in
the world.

22
00:01:03,570 --> 00:01:07,460
Simply by uttering those words, could you
please loan me your car?

23
00:01:08,560 --> 00:01:09,490
It happens all the time.

24
00:01:09,490 --> 00:01:10,350
Here's another example.

25
00:01:11,370 --> 00:01:13,970
Suppose my friend is wondering whether the
moon is full.

26
00:01:13,970 --> 00:01:15,690
And I say, the moon is full.

27
00:01:15,690 --> 00:01:19,400
Well am I uttering those words just to
expel hot air?

28
00:01:19,400 --> 00:01:20,610
No.

29
00:01:20,610 --> 00:01:24,980
Am I uttering those words just to express
my own belief?

30
00:01:24,980 --> 00:01:25,770
No.

31
00:01:25,770 --> 00:01:28,320
I'm trying to inform my friend.

32
00:01:28,320 --> 00:01:31,070
I'm trying to bring about a change in my
friend's

33
00:01:31,070 --> 00:01:34,400
beliefs, and that's to bring about an
effect in the world.

34
00:01:34,400 --> 00:01:36,119
So that's a conversational act.

35
00:01:36,119 --> 00:01:39,565
To bring about the effect in the world of
informing my friend.

36
00:01:39,565 --> 00:01:44,490
Informing is a conversational act.
And almost all speech

37
00:01:44,490 --> 00:01:48,790
acts have particular effects that are
associated with them.

38
00:01:48,790 --> 00:01:50,500
When you ask a question, you're trying

39
00:01:50,500 --> 00:01:52,868
to bring about someone answering the
question.

40
00:01:52,868 --> 00:01:57,990
When you apologize, you're trying to bring
about forgiveness.

41
00:01:57,990 --> 00:02:02,450
When you promise somebody, you're trying
to bring about the person relying

42
00:02:02,450 --> 00:02:06,790
on your promise in order to believe that
you're going to do it.

43
00:02:06,790 --> 00:02:10,160
So speech acts are often associated with
particular

44
00:02:10,160 --> 00:02:13,660
effects that the speaker intends to bring
about and

45
00:02:13,660 --> 00:02:17,390
the bringing about of that effect is the
conversational act.

46
00:02:17,390 --> 00:02:20,660
So if we want an official definition of a
conversational

47
00:02:20,660 --> 00:02:23,700
act we can say that the conversational act
is the bringing

48
00:02:23,700 --> 00:02:28,010
about of the intended effect, which is the
standard effect

49
00:02:28,010 --> 00:02:31,680
for the kind of speech act that the
speaker is performing.

50
00:02:31,680 --> 00:02:33,890
That's what a conversational act is.

51
00:02:33,890 --> 00:02:35,670
Now.
Since the conversational

52
00:02:35,670 --> 00:02:38,220
act is the bringing about of the standard
effect.

53
00:02:38,220 --> 00:02:41,980
The conversational act does not occur when
that effect does not occur.

54
00:02:41,980 --> 00:02:47,050
And that find seem weird that what kind of
act you perform depends on whether the

55
00:02:47,050 --> 00:02:52,005
effect occurs maybe several seconds, maybe
even longer even longer in the future.

56
00:02:52,005 --> 00:02:55,030
But it's not that weird when you think
about it.

57
00:02:55,030 --> 00:03:01,440
Because if you pull the trigger of a gun
that's pointed at someone, then whether

58
00:03:01,440 --> 00:03:04,290
your act of pulling the trigger is an

59
00:03:04,290 --> 00:03:06,710
act of killing, depends on whether the
person dies.

60
00:03:06,710 --> 00:03:10,020
And yet the person's death is something
independent of it.

61
00:03:10,020 --> 00:03:13,590
It's an effect that occurs, maybe quite a
while in the future,

62
00:03:13,590 --> 00:03:16,900
but your act wasn't an act of killing
unless the person died.

63
00:03:16,900 --> 00:03:19,460
And that's the story of conversational
acts.

64
00:03:19,460 --> 00:03:24,040
Your act is not this conversational act
unless, the effect occurs.

65
00:03:24,040 --> 00:03:26,886
It has to be the intended effect, that's
the standard

66
00:03:26,886 --> 00:03:29,540
effect for the kind of speech act that
you're performing.

67
00:03:29,540 --> 00:03:34,665
So, the really tricky question is, how are
we going to bring about these effects?

68
00:03:34,665 --> 00:03:35,630
because it's not so easy.

69
00:03:36,690 --> 00:03:38,695
Think about how other people bring about
effects.

70
00:03:38,695 --> 00:03:41,650
Think about a baker baking a cake.

71
00:03:41,650 --> 00:03:45,840
Well, the baker needs to get together the
right ingredients and

72
00:03:45,840 --> 00:03:49,917
bring them to the right place, get the
right amount of ingredients.

73
00:03:49,917 --> 00:03:52,190
You know, if a baker fills the entire
kitchen

74
00:03:52,190 --> 00:03:54,875
with flour, he's not going to have any
room left over to bake the cake.

75
00:03:54,875 --> 00:03:58,190
And has to bring the right ingredients.

76
00:03:58,190 --> 00:04:02,845
That means, if instead of bringing flour
he brings gravel, he can't bake a cake.

77
00:04:02,845 --> 00:04:06,420
And he has to put together those
ingredients in

78
00:04:06,420 --> 00:04:09,110
the right way, in the right order for
example.

79
00:04:09,110 --> 00:04:12,655
You can't mix them in the wrong order, the
cake won't work out.

80
00:04:12,655 --> 00:04:15,910
And has to bake it for the right amount of
time and so on and so on.

81
00:04:15,910 --> 00:04:17,350
So there're a lot of tricky rules

82
00:04:17,350 --> 00:04:20,830
about how to bring about the effect of a
good cake.

83
00:04:20,830 --> 00:04:23,510
Well the same thing applies to
conversational acts.

84
00:04:23,510 --> 00:04:26,520
They're going to be rules that have to be
followed, in order

85
00:04:26,520 --> 00:04:30,230
to bring about the conversational act that
you're trying to bring about.

86
00:04:30,230 --> 00:04:32,740
That is in order to have that intended

87
00:04:32,740 --> 00:04:35,945
effect of the speech act in the
circumstances.

88
00:04:35,945 --> 00:04:40,670
And the same kind of rules apply to any
rational person trying to pursue any goal.

89
00:04:40,670 --> 00:04:42,410
Whenever you want to bring about

90
00:04:42,410 --> 00:04:45,560
an effect, you have to follow certain
general rules.

91
00:04:45,560 --> 00:04:49,320
And so it applies to people who are trying
to bring about effects

92
00:04:49,320 --> 00:04:54,080
by language, that is, to people who are
trying to perform conversational acts.

93
00:04:54,080 --> 00:04:57,430
If you want to inform someone, that is, to
have an effect

94
00:04:57,430 --> 00:05:00,950
on their beliefs, then you need to speak
in a certain way.

95
00:05:00,950 --> 00:05:03,650
And if you want to promise someone, that
is to

96
00:05:03,650 --> 00:05:08,010
get them to rely on you, that's the
conversational act associated

97
00:05:08,010 --> 00:05:09,880
with the speech act of promising.

98
00:05:09,880 --> 00:05:13,600
But you're not going to get them to rely
on you unless you follow certain rules.

99
00:05:13,600 --> 00:05:17,010
And so what we need to try to understand
are the rules of

100
00:05:17,010 --> 00:05:19,130
language that enable us to bring about

101
00:05:19,130 --> 00:05:22,380
these effects that are the conversational
acts.

102
00:05:22,380 --> 00:05:24,440
Now on this question Paul Grice helps us
out a

103
00:05:24,440 --> 00:05:27,470
lot, he's one of the great philosophers of
the 20th century.

104
00:05:27,470 --> 00:05:31,965
And he laid out a series of rules
governing conversational acts.

105
00:05:31,965 --> 00:05:33,130
He called them

106
00:05:33,130 --> 00:05:34,900
the conversational maxims.

107
00:05:34,900 --> 00:05:37,599
And we're going to look at them one by
one.

108
00:05:37,599 --> 00:05:42,290
Grice focuses in on context where people
are stating things and

109
00:05:42,290 --> 00:05:46,035
where they're cooperating with each other
and trying to inform each other.

110
00:05:46,035 --> 00:05:47,980
He's not trying to provide a general
theory

111
00:05:47,980 --> 00:05:50,750
so it's for statements in a cooperative
context.

112
00:05:51,760 --> 00:05:58,360
So the first maxim is the rule of quantity
and it basically says, don't say more

113
00:05:58,360 --> 00:06:01,060
than is required for the purpose that
you're trying to achieve.

114
00:06:01,060 --> 00:06:04,900
If you say too many words the point gets
lost in the words

115
00:06:04,900 --> 00:06:07,675
so you shouldn't say more than you need
for the purpose at hand.

116
00:06:07,675 --> 00:06:12,380
The second part of the rule of quantity is
you shouldn't say too little.

117
00:06:12,380 --> 00:06:12,670
Right?

118
00:06:12,670 --> 00:06:15,250
Because if you say too little then that's

119
00:06:15,250 --> 00:06:18,300
going to be misleading and it's not going
to fulfill

120
00:06:18,300 --> 00:06:20,500
your purpose because the person that
you're talking

121
00:06:20,500 --> 00:06:23,430
to won't have all the information that
they need.

122
00:06:24,690 --> 00:06:27,450
Second rule is the rule of quality.

123
00:06:27,450 --> 00:06:32,590
The rule of quality says, don't say what
you don't believe to be true.

124
00:06:32,590 --> 00:06:33,190
Don't lie.

125
00:06:33,190 --> 00:06:35,680
Don't mislead.
Don't deceive.

126
00:06:35,680 --> 00:06:36,250
Right?

127
00:06:36,250 --> 00:06:40,290
But also this is the second part of the
rule of quality.

128
00:06:40,290 --> 00:06:44,470
Don't say something that you lack adequate
justification for.

129
00:06:44,470 --> 00:06:47,210
Because you shouldn't just be talking off
the top of your head.

130
00:06:47,210 --> 00:06:50,330
With no reason to believe what you're
saying.

131
00:06:50,330 --> 00:06:53,660
These are all pretty common sense rules,
but

132
00:06:53,660 --> 00:06:57,690
they weren't apparent to people until
Grice formulated them.

133
00:06:57,690 --> 00:07:01,235
The third rule is the rule of relevance
and it's the toughest of all.

134
00:07:01,235 --> 00:07:05,089
The rule of relevance says, be relevant.

135
00:07:05,089 --> 00:07:07,470
Look, it's short, I'll grant you that.

136
00:07:07,470 --> 00:07:09,300
It's going to be easy to remember, I'll
grant you that.

137
00:07:09,300 --> 00:07:12,270
But it really is kind of tricky to apply
the rule,

138
00:07:12,270 --> 00:07:15,410
because you have to figure out what's
relevant and we'll see

139
00:07:15,410 --> 00:07:20,610
some problems with that, but for now just
remember that it should be obvious.

140
00:07:20,610 --> 00:07:23,020
When you're talking about a subject and
you want

141
00:07:23,020 --> 00:07:26,400
to achieve a certain purpose and the
person you're talking

142
00:07:26,400 --> 00:07:30,280
to is cooperating with you, as Grice is
assuming, then

143
00:07:30,280 --> 00:07:32,150
you ought to be talking about things that
are relevant.

144
00:07:32,150 --> 00:07:35,285
And if you change the subject, that's
going to be very misleading.

145
00:07:35,285 --> 00:07:39,290
And the fourth conversational maxim is the
rule of manner.

146
00:07:39,290 --> 00:07:40,695
It says be brief,

147
00:07:40,695 --> 00:07:45,970
be orderly.
Avoid obscurity, and avoid ambiguity.

148
00:07:45,970 --> 00:07:48,460
Pretty simple, it's all about style
because if you're

149
00:07:48,460 --> 00:07:51,090
not brief enough, people won't pay
attention to you.

150
00:07:51,090 --> 00:07:54,909
If it's not orderly people will get
confused by that, and

151
00:07:54,909 --> 00:07:59,600
if you're ambiguous or obscure then people
won't understand what you're saying.

152
00:07:59,600 --> 00:08:01,790
So these four rules are followed by

153
00:08:01,790 --> 00:08:04,820
speakers when they're cooperating with
each other.

154
00:08:04,820 --> 00:08:05,750
When people aren't

155
00:08:05,750 --> 00:08:08,340
cooperating, they're trying to trick or
deceive each other, they

156
00:08:08,340 --> 00:08:12,160
might violate these rules, and mislead
people by abusing these rules.

157
00:08:12,160 --> 00:08:15,370
But when they're cooperating, these are
the rules they follow

158
00:08:15,370 --> 00:08:20,610
and that makes them able to deceive people
by violating them.

159
00:08:20,610 --> 00:08:24,140
And also notice that these rules might not
be completely clear to you.

160
00:08:24,140 --> 00:08:25,950
You might not have ever thought of them
before, but

161
00:08:25,950 --> 00:08:28,840
now that you mention them they probably
seem pretty obvious.

162
00:08:28,840 --> 00:08:30,890
It's kind of like the finger and singer

163
00:08:30,890 --> 00:08:34,110
rule that we saw before regarding
pronunciation.

164
00:08:34,110 --> 00:08:36,520
That's a rule that you hadn't thought of
before

165
00:08:36,520 --> 00:08:39,260
but once it's pointed out it seems kind of
obvious.

166
00:08:39,260 --> 00:08:40,570
Well that's what Grice has done.

167
00:08:40,570 --> 00:08:43,720
But he's shown us the rules governing
conversational acts

168
00:08:43,720 --> 00:08:48,630
that enable us to bring about certain
effects by language.

169
00:08:48,630 --> 00:08:53,120
Now we can use these rules to understand
what's going on in a lot of conversations.

170
00:08:53,120 --> 00:08:56,280
Imagine you're at a restaurant and the
waiter walks up to your table and says,

171
00:08:56,280 --> 00:08:58,990
well for dessert you can have cake or ice
cream.

172
00:09:00,080 --> 00:09:00,980
Well.

173
00:09:00,980 --> 00:09:03,740
What has that waiter suggested?

174
00:09:03,740 --> 00:09:07,600
He's suggested that that's all you can
have cake, ice cream.

175
00:09:07,600 --> 00:09:09,702
Well he didn't mention pie so you can't
have pie.

176
00:09:09,702 --> 00:09:15,320
Because if he's a good waiter and he knows
that they have pie back there

177
00:09:15,320 --> 00:09:18,290
and you can order it, then he ought to be
telling you about the pie.

178
00:09:18,290 --> 00:09:21,708
He would be violating the rule of
quantity, that is not providing

179
00:09:21,708 --> 00:09:25,260
you all the relevant information, if he
said you can have cake or

180
00:09:25,260 --> 00:09:29,710
ice cream and you can also have pie but he
didn't mention pie.

181
00:09:29,710 --> 00:09:32,580
So because you assume that he's
cooperating with

182
00:09:32,580 --> 00:09:34,650
you and trying to get you what you

183
00:09:34,650 --> 00:09:37,110
want to eat, since he is your waiter,

184
00:09:37,110 --> 00:09:41,400
after all, there must not be pie
available.

185
00:09:41,400 --> 00:09:44,640
So you say, I'll take ice cream, even
though you would have preferred pie.

186
00:09:46,390 --> 00:09:47,030
What's happening

187
00:09:47,030 --> 00:09:49,270
here is called conversational implication.

188
00:09:49,270 --> 00:09:51,830
When the waiter said you could have cake
or ice

189
00:09:51,830 --> 00:09:57,020
cream, he was conversationally implying
that you can't have pie.

190
00:09:57,020 --> 00:10:00,620
And the reason that he conversationally
implied that is because, if he were

191
00:10:00,620 --> 00:10:04,100
cooperating and following the
conversational rules, or

192
00:10:04,100 --> 00:10:07,780
maxims, then he would have mentioned pie.

193
00:10:07,780 --> 00:10:12,290
So you assumed that since he said only
cake and I, cake

194
00:10:12,290 --> 00:10:15,000
or ice cream, that you can't have pie.

195
00:10:15,000 --> 00:10:20,280
He, in effect, conversationally implied
that you cannot have pie.

196
00:10:20,280 --> 00:10:24,670
And the way you figured that out was you
took what he said, a little

197
00:10:24,670 --> 00:10:27,850
background knowledge about him being a
waiter and

198
00:10:27,850 --> 00:10:31,320
having certain goals and what happens in
restaurants.

199
00:10:31,320 --> 00:10:36,380
Performed a little mini calculation using
the maxim of quantity.

200
00:10:36,380 --> 00:10:37,540
And inferred

201
00:10:37,540 --> 00:10:40,280
that he must believe that you can't have
pie.

202
00:10:40,280 --> 00:10:42,970
And of course since he's a waiter, he
ought to know whether

203
00:10:42,970 --> 00:10:46,896
you can have something else or not, and
therefore you can't have pie.

204
00:10:46,896 --> 00:10:52,620
But what if he had a favorite customer at
another table and he knew there

205
00:10:52,620 --> 00:10:55,730
was only one slice of pie back there and
he didn't want you to order it?

206
00:10:56,930 --> 00:10:59,750
And he said, you can have cake or ice
cream.

207
00:10:59,750 --> 00:11:02,560
And didn't mention the pie so you wouldn't
order it and his favorite

208
00:11:02,560 --> 00:11:05,030
customer would get it instead of you.

209
00:11:05,030 --> 00:11:08,790
Well, he still conversationally implied
that you can't have pie.

210
00:11:08,790 --> 00:11:10,030
But he misled you.

211
00:11:10,030 --> 00:11:14,370
He misled you because he was trying to get
the pie for somebody else.

212
00:11:14,370 --> 00:11:16,830
He was not cooperating with you.

213
00:11:16,830 --> 00:11:19,570
So the tricky thing about these
conversational maxims

214
00:11:19,570 --> 00:11:22,972
is they work perfectly fine when you're
cooperating with

215
00:11:22,972 --> 00:11:25,720
the person and trying to give them all

216
00:11:25,720 --> 00:11:28,010
of the information that they need for your
common

217
00:11:28,010 --> 00:11:29,428
purpose with that other person.

218
00:11:29,428 --> 00:11:36,250
But if you're not cooperating then you can
use them to mislead the other person.

219
00:11:36,250 --> 00:11:39,980
And that's the double edged sword of
conversational implication.

220
00:11:41,020 --> 00:11:43,950
But one of the features of conversational
implication is really

221
00:11:43,950 --> 00:11:46,100
important to arguments, and that's

222
00:11:46,100 --> 00:11:49,310
that you can cancel conversational
implications.

223
00:11:49,310 --> 00:11:52,832
The waiter can say you can have cake or
ice cream,

224
00:11:52,832 --> 00:11:57,150
oh yeah, and you can also have pie.

225
00:11:57,150 --> 00:12:02,220
And when he said, and also you can have
pie, he did not take back

226
00:12:02,220 --> 00:12:05,110
you can have cake or ice cream, because
you can still have cake or ice cream.

227
00:12:05,110 --> 00:12:07,190
It's just that, you can also have pie.

228
00:12:08,430 --> 00:12:11,730
So, we can cancel the conversational
implication that you cannot have

229
00:12:11,730 --> 00:12:14,910
pie by saying, oh, yeah, and you can also
have pie.

230
00:12:16,118 --> 00:12:17,930
So with a conversational implication,

231
00:12:17,930 --> 00:12:23,450
if a certain sentence, P, conversationally
implies another sentence, Q,

232
00:12:23,450 --> 00:12:26,360
then you can deny Q and P still might be
true.

233
00:12:27,850 --> 00:12:30,190
And that's an important fact because it
distinguishes

234
00:12:30,190 --> 00:12:36,420
conversational implications from logical
entailments or logical implications.

235
00:12:36,420 --> 00:12:43,080
If I say Alice is my sister, then that
implies Alice is female, and I can't go,

236
00:12:43,080 --> 00:12:46,720
Alice is my sister, oh yeah, and she's not
female.

237
00:12:46,720 --> 00:12:48,810
That doesn't make any sense because if
she's not female, she

238
00:12:48,810 --> 00:12:52,260
can't be my sister because that's a
logical implication or entailment.

239
00:12:53,440 --> 00:12:56,400
But with a conversational implication
instead,

240
00:12:56,400 --> 00:12:59,360
you can deny what is conversationally
implied.

241
00:12:59,360 --> 00:13:01,440
And the original sentence is still true.

242
00:13:01,440 --> 00:13:08,250
So if the waiter says you can have cake,
or ice cream, and then, I find out

243
00:13:08,250 --> 00:13:12,150
that he's been saving the last piece of
pie for this other table, then

244
00:13:12,150 --> 00:13:15,770
I can come up to him and say wait a
minute, you lied to me.

245
00:13:15,770 --> 00:13:19,100
He didn't really lie to me, because what
he said was still true.

246
00:13:19,100 --> 00:13:21,780
I could have cake or ice cream.

247
00:13:21,780 --> 00:13:23,950
It's still true, I can have cake or
ice-cream.

248
00:13:23,950 --> 00:13:25,860
He didn't say anything false to me.

249
00:13:25,860 --> 00:13:30,390
He simply didn't mention the pie that I
could also have.

250
00:13:30,390 --> 00:13:33,310
So that's very different in the case of
conversational

251
00:13:33,310 --> 00:13:37,520
implication, than in the case of logical
entailment.

252
00:13:37,520 --> 00:13:40,130
And that'll be important to us, especially
when we get

253
00:13:40,130 --> 00:13:42,378
to formal logic, in a later part of this
course.

254
00:13:42,378 --> 00:13:46,900
So let me give you another example that's
more important.

255
00:13:46,900 --> 00:13:50,780
Imagine a politician says I've got a
policy that's

256
00:13:50,780 --> 00:13:54,440
going to reduce crime by getting criminals
off the streets.

257
00:13:54,440 --> 00:13:57,480
And the policy is lock them all up.

258
00:13:57,480 --> 00:13:58,370
When people are suspected

259
00:13:58,370 --> 00:14:00,070
of crimes, you'll lock them all up.

260
00:14:00,070 --> 00:14:01,670
That's going to get criminals off the
street.

261
00:14:02,980 --> 00:14:06,260
Well that might convince people if they

262
00:14:06,260 --> 00:14:09,620
don't notice that he's left out another
fact.

263
00:14:09,620 --> 00:14:11,590
He's not just going to get people off the
street who are

264
00:14:11,590 --> 00:14:14,540
criminals, he's going to get lots of other
people off the street too.

265
00:14:14,540 --> 00:14:16,880
He didn't give you all the relevant
information,

266
00:14:16,880 --> 00:14:19,820
like the waiter who mislead you with the
pie.

267
00:14:19,820 --> 00:14:23,430
He suggested that his policy will solve
the problem of

268
00:14:23,430 --> 00:14:27,060
crime by putting people in prison who
would commit crimes.

269
00:14:27,060 --> 00:14:30,094
And just left out the other relevant fact,
that

270
00:14:30,094 --> 00:14:31,915
it's going to put lots of other people in
prison too.

271
00:14:31,915 --> 00:14:37,890
So he has conversationally implied that
there's no other relevant facts

272
00:14:37,890 --> 00:14:41,890
to consider by only mentioning that it's
going to reduce the crime rate.

273
00:14:41,890 --> 00:14:44,750
And you have to be good at looking through

274
00:14:44,750 --> 00:14:48,500
that implication and asking, yes, but is
there something he's

275
00:14:48,500 --> 00:14:49,339
leaving out?

276
00:14:49,339 --> 00:14:54,980
And that's often what you need to do in
order to avoid being misled by sleazy

277
00:14:54,980 --> 00:14:57,600
politicians and other people who leave out
the

278
00:14:57,600 --> 00:15:01,280
relevant information for the issue that
you're talking about.

279
00:15:01,280 --> 00:15:03,100
Now, of course, the politician might not
care

280
00:15:03,100 --> 00:15:04,930
that he misled you, that might be the
goal.

281
00:15:04,930 --> 00:15:07,590
He wants to persuade you and he doesn't
care whether he

282
00:15:07,590 --> 00:15:11,660
misleads you because it's persuasion not
justification that he's interested in.

283
00:15:12,900 --> 00:15:13,590
In addition,

284
00:15:13,590 --> 00:15:15,030
he's got his defense ready.

285
00:15:15,030 --> 00:15:18,650
He can say, but I didn't say anything
false, what I said was true.

286
00:15:18,650 --> 00:15:23,830
If we put all those potential criminals in
jail we're going to reduce the crime rate.

287
00:15:23,830 --> 00:15:26,420
Maybe it's also true that we're going to
put some innocent people in

288
00:15:26,420 --> 00:15:30,140
jail, but we will reduce the crime rate
and that's what I said.

289
00:15:30,140 --> 00:15:33,620
What Grice's maxim of quantity does is it
tells

290
00:15:33,620 --> 00:15:37,360
us exactly why we have a criticism of him
now.

291
00:15:37,360 --> 00:15:38,770
We can say he's not

292
00:15:38,770 --> 00:15:43,120
cooperating because he's not following the
conversational maxim of quantity.

293
00:15:43,120 --> 00:15:46,630
He's not giving us all the relevant
information that we need

294
00:15:46,630 --> 00:15:50,420
in order to achieve our purpose if we have
a common purpose.

295
00:15:50,420 --> 00:15:54,360
And this politician is pretending to have
a common purpose with us, the good of the

296
00:15:54,360 --> 00:15:57,790
country, when actually he doesn't have a
common

297
00:15:57,790 --> 00:15:59,770
purpose with us, he just wants to get
elected.

298
00:16:01,210 --> 00:16:03,860
And so Grice gives us an insight into

299
00:16:03,860 --> 00:16:07,410
what's going on when we get misled in
those contexts, and also

300
00:16:07,410 --> 00:16:11,570
what we need to do to respond to those
types of bad arguments.

301
00:16:11,570 --> 00:16:13,776
Now this distinction between
conversational

302
00:16:13,776 --> 00:16:17,820
implication and logical entailment is
crucial

303
00:16:17,820 --> 00:16:21,500
to arguments, because it tells us
something about how to refute arguments.

304
00:16:21,500 --> 00:16:23,700
When you don't like the premise of an
argument,

305
00:16:23,700 --> 00:16:28,590
because it's misleading, because it
conversationally implies something false.

306
00:16:28,590 --> 00:16:32,910
That's not a way to show that the premise
is false.

307
00:16:32,910 --> 00:16:35,440
In order to show it's false you have to
show that it actually logically

308
00:16:35,440 --> 00:16:37,790
entails something that's false then you
can

309
00:16:37,790 --> 00:16:40,500
infer that the premise itself is false.

310
00:16:40,500 --> 00:16:43,420
This will become important later when we
look at

311
00:16:43,420 --> 00:16:47,400
the role of conversational implication and
logical entailment in arguments.

312
00:16:47,400 --> 00:16:49,550
But for now, the important thing is to

313
00:16:49,550 --> 00:16:51,000
understand the distinction between

314
00:16:51,000 --> 00:16:54,810
conversational implication and logical
entailment.

315
00:16:54,810 --> 00:16:58,485
Speakers usually follow these
conversational maxims that Grice

316
00:16:58,485 --> 00:17:01,060
enunciated, when they speak, and when
they're cooperating.

317
00:17:01,060 --> 00:17:04,580
But they don't always follow these maxims,
sometimes they violate them.

318
00:17:04,580 --> 00:17:10,600
And of course, as always, there's a lot
more to be said about conversational acts.

319
00:17:10,600 --> 00:17:13,790
If you want to learn more about
conversational acts you should look

320
00:17:13,790 --> 00:17:18,500
at the chapter in understanding arguments
in the text that accompanies this course.

321
00:17:19,660 --> 00:17:19,820
But I

322
00:17:19,820 --> 00:17:23,075
think we've learned enough about
conversational acts to move on.

323
00:17:23,075 --> 00:17:27,300
Because so far we've looked at language in
general at the linguistic

324
00:17:27,300 --> 00:17:31,300
level, at the speech act level, and at the
conversational act level.

325
00:17:31,300 --> 00:17:33,125
Now we want to take these lessons.

326
00:17:33,125 --> 00:17:37,190
And apply them more specifically to the
language of argument.

327
00:17:37,190 --> 00:17:41,285
That is the particular kind of language
that gets used in arguments.

328
00:17:41,285 --> 00:17:44,610
And that's what will be the topic for the
next few lectures.

