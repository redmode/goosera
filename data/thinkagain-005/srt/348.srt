1
00:00:03,990 --> 00:00:07,820
In the last few lectures we've seen how we
can use Venn diagrams to visually

2
00:00:07,820 --> 00:00:15,030
represent the information, that's conveyed
in a, e, i, or o propositions.

3
00:00:15,030 --> 00:00:18,160
And, we've also seen how there are
different

4
00:00:18,160 --> 00:00:22,230
ways in ordinary language of expressing
such propositions.

5
00:00:22,230 --> 00:00:29,050
Sometimes such propositions are expressed
even by sentences that use individual's

6
00:00:29,050 --> 00:00:33,110
names and don't explicitly mention
categories.

7
00:00:33,110 --> 00:00:33,110
[INAUDIBLE]

8
00:00:33,110 --> 00:00:33,110
.

9
00:00:33,110 --> 00:00:35,170
Today, I want to talk a bit more about
that.

10
00:00:35,170 --> 00:00:38,300
I want to talk about the various ways in
which, in

11
00:00:38,300 --> 00:00:41,580
English at least, and in many other
natural languages, we

12
00:00:41,580 --> 00:00:45,200
can represent the information conveyed by
a, e, i, or

13
00:00:45,200 --> 00:00:50,669
o propositions using forms of language
that we haven't yet discussed.

14
00:00:51,790 --> 00:00:58,811
So let's consider some examples.
So consider propositions of the form Not

15
00:00:58,811 --> 00:01:04,632
all Fs are Gs.
We haven't talked about not all before.

16
00:01:04,632 --> 00:01:10,730
well, when you say not all Fs are Gs, what
exactly are you saying?

17
00:01:10,730 --> 00:01:15,190
Usually, when you say not all Fs or Gs,
what you mean

18
00:01:15,190 --> 00:01:18,950
to be saying is that there are some Fs
that are not Gs.

19
00:01:20,100 --> 00:01:23,930
In short, what you mean to be saying when
you say not all Fs are Gs is

20
00:01:23,930 --> 00:01:30,010
usually a proposition of the O form.
So how would you

21
00:01:30,010 --> 00:01:36,730
represent not all Fs are Gs?
Well, all Fs are Gs, remember,

22
00:01:36,730 --> 00:01:42,550
would be represented by shading this out.
That would be all Fs are Gs.

23
00:01:42,550 --> 00:01:47,560
But you're saying not all Fs are Gs.
In other words, some Fs are not G.

24
00:01:47,560 --> 00:01:49,520
So instead of shade there,

25
00:01:49,520 --> 00:01:52,820
what you would do is put an x right there.

26
00:01:52,820 --> 00:01:56,250
There's some F, something out there that's
not a G.

27
00:01:56,250 --> 00:01:58,440
It's outside the category of the Gs.

28
00:01:59,770 --> 00:02:06,720
And for some examples, you might say not
all geniuses take Coursera courses.

29
00:02:06,720 --> 00:02:11,270
In other words, some geniuses don't take
Coursera courses.

30
00:02:12,330 --> 00:02:14,650
Or you might say, not everything that

31
00:02:14,650 --> 00:02:21,090
Pat does is intended to annoy Chris.
In other words, some of the things

32
00:02:21,090 --> 00:02:26,829
that Pat does, some of things that fall
into the category, things that Pat does

33
00:02:29,030 --> 00:02:30,830
are intended to annoy Chris.

34
00:02:30,830 --> 00:02:32,650
That is to say, they do fall into

35
00:02:32,650 --> 00:02:35,960
the category, things that are intended to
annoy Chris.

36
00:02:37,510 --> 00:02:43,500
So those are both examples of statements
of a form, not all Fs are Gs.

37
00:02:43,500 --> 00:02:46,560
And they're both equivalent to a statement
to the form, some Fs are not Gs.

38
00:02:51,980 --> 00:02:57,440
Now consider statements of the form, all
Fs are not Gs.

39
00:02:57,440 --> 00:03:01,579
Well, when you say something of the form
all Fs are

40
00:03:01,579 --> 00:03:05,560
Gs, that's usually intended to convey that
no Fs are Gs.

41
00:03:05,560 --> 00:03:09,439
In other words, it's usually intended to
convey a proposition of the E form.

42
00:03:10,950 --> 00:03:14,400
So when you say no Fs are Gs, how do you
represent that?

43
00:03:14,400 --> 00:03:17,160
Well, you represent it by shading in the
intersection

44
00:03:17,160 --> 00:03:18,230
of the Fs and the Gs

45
00:03:18,230 --> 00:03:18,780
[SOUND]

46
00:03:18,780 --> 00:03:22,020
and that's just to say that all Fs are not
Gs.

47
00:03:22,020 --> 00:03:24,800
Right, if there are any Fs at all, they've
got to be out here.

48
00:03:24,800 --> 00:03:27,590
They've got to be outside the category of
the Gs.

49
00:03:29,040 --> 00:03:31,140
Some examples of statements like that.

50
00:03:32,370 --> 00:03:36,330
All Nobel Prize winners are not
alcoholics.

51
00:03:37,700 --> 00:03:39,230
Right, you're saying if there are any
Nobel

52
00:03:39,230 --> 00:03:43,500
Prize winners they are outside the
category of alcoholics.

53
00:03:45,060 --> 00:03:49,790
Everything that Pat does is not designed
to achieve victory.

54
00:03:51,430 --> 00:03:57,340
Again, you're saying that whatever falls
into the category of things that Pat does,

55
00:03:58,710 --> 00:04:03,200
it is outside the category of things
designed to achieve victory.

56
00:04:06,430 --> 00:04:14,753
If this is the category of Pat's actions

57
00:04:14,753 --> 00:04:14,770
[SOUND]

58
00:04:14,770 --> 00:04:20,830
and this is the category of things
designed to achieve victory.

59
00:04:25,140 --> 00:04:27,780
Then, when you say everything that Pat
does is not

60
00:04:27,780 --> 00:04:31,470
designed to achieve victory, you're saying
there's nothing in this intersection.

61
00:04:31,470 --> 00:04:34,020
Whatever Pat's actions are, they've got to
be out here.

62
00:04:37,620 --> 00:04:37,970
Okay.

63
00:04:37,970 --> 00:04:44,630
Finally, let's consider the statement of
the form some Fs are both Gs and Hs.

64
00:04:44,630 --> 00:04:49,430
If you say some Fs are both Gs and Hs,
what are you saying?

65
00:04:50,450 --> 00:04:57,720
Well, are you saying that there are some
Fs that are Gs and some Fs that are Hs?

66
00:04:59,460 --> 00:05:01,030
Well, that's something you could say of
course.

67
00:05:01,030 --> 00:05:02,826
You could say some Fs are Gs and some

68
00:05:02,826 --> 00:05:04,390
Fs are Hs, right?

69
00:05:04,390 --> 00:05:08,400
Some, you could say, some men are tall and
some men are short.

70
00:05:09,890 --> 00:05:14,590
But when you say some Fs are both Gs and
Hs, you're not saying that.

71
00:05:14,590 --> 00:05:21,858
You're saying that some Fs are in this
intersection of Gs and Hs, right?

72
00:05:21,858 --> 00:05:23,760
So some Fs have got to be here.

73
00:05:23,760 --> 00:05:27,515
That's what you're saying when you say
some Fs are both Gs and Hs.

74
00:05:27,515 --> 00:05:28,440
Right?

75
00:05:28,440 --> 00:05:30,900
That's why you couldn't say, at least you
couldn't

76
00:05:30,900 --> 00:05:34,670
truthfully say some men are both tall and
short, right?

77
00:05:34,670 --> 00:05:38,188
There's no man who's both tall and short.

78
00:05:38,188 --> 00:05:42,460
but you could say there are some men who
are both wealthy and happy.

79
00:05:43,920 --> 00:05:48,360
Then they're, a man who is wealthy and who
is happy.

80
00:05:48,360 --> 00:05:49,670
Other examples.

81
00:05:49,670 --> 00:05:52,990
Some philosophers are both robotic and
monotone.

82
00:05:54,030 --> 00:05:54,270
Right?

83
00:05:54,270 --> 00:05:56,310
There, it's not just that they're some
philosophers

84
00:05:56,310 --> 00:05:59,750
who are robotic and some philosophers who
are monotone.

85
00:05:59,750 --> 00:06:03,560
There are actually some philosophers who
are both robotic and monotone.

86
00:06:04,910 --> 00:06:09,400
some of the things that Pat does are both
intended to amuse and to provoke.

87
00:06:10,830 --> 00:06:11,080
Right?

88
00:06:11,080 --> 00:06:13,910
There, it's not just that some of things
she does are intended

89
00:06:13,910 --> 00:06:16,790
to amuse, and some of the things she does
are intended to provoke.

90
00:06:16,790 --> 00:06:19,250
It's that some of the things she does are
intended to do

91
00:06:19,250 --> 00:06:25,570
both amuse and to provoke.
So, if these are Pat's actions, right here

92
00:06:25,570 --> 00:06:26,070
[SOUND]

93
00:06:29,490 --> 00:06:34,660
and this is the category of things
intended to amuse

94
00:06:34,660 --> 00:06:38,740
and this is a category of things intended
to provoke.

95
00:06:38,740 --> 00:06:40,900
Then, this last statement some of the
things

96
00:06:40,900 --> 00:06:43,590
that Pat does are both intended to amuse

97
00:06:43,590 --> 00:06:45,270
and to provoke, is saying that some of

98
00:06:45,270 --> 00:06:49,940
Pat's actions fall into this area right
here.

99
00:06:49,940 --> 00:06:54,250
This intersection of things designed to
amuse and things designed to provoke.

100
00:06:54,250 --> 00:06:54,760
But of course

101
00:06:54,760 --> 00:06:58,056
since they are past actions, they have to
fall into this portion of the area.

102
00:06:58,056 --> 00:07:00,084
Right here.

103
00:07:00,084 --> 00:07:00,084
[SOUND]

104
00:07:00,084 --> 00:07:06,630
Okay I hope I've given you a sense of the
various ways in which ordinary

105
00:07:06,630 --> 00:07:12,390
language can express ideas that we can
represent visually using Venn diagrams.

