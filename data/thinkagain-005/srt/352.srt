1
00:00:04,220 --> 00:00:06,982
Over the past couple of weeks, we've seen
how we can use

2
00:00:06,982 --> 00:00:12,540
truth tables and Venn diagrams to predict
and explain the validity of arguments.

3
00:00:12,540 --> 00:00:15,690
But the arguments that we've looked at are
all arguments

4
00:00:15,690 --> 00:00:19,020
that are given in ordinary language that
we can understand.

5
00:00:19,020 --> 00:00:22,410
But, of course, not all arguments are like
that.

6
00:00:22,410 --> 00:00:25,810
Some arguments are given in foreign
languages that we don't understand

7
00:00:25,810 --> 00:00:28,960
or they are given in technical languages
that we don't understand.

8
00:00:30,460 --> 00:00:32,630
Could you use truth tables or Venn
diagrams

9
00:00:32,630 --> 00:00:35,300
to predict and explain the validity of
those arguments?

10
00:00:35,300 --> 00:00:36,610
Even those arguments that are given in
arguments that

11
00:00:36,610 --> 00:00:39,660
are given in languages that you don't yet
understand?

12
00:00:41,370 --> 00:00:44,200
Well, today I am going to show you that
you can.

13
00:00:44,200 --> 00:00:44,890
You can do that

14
00:00:44,890 --> 00:00:45,910
[SOUND]

15
00:00:45,910 --> 00:00:47,740
and I'm going to show it by giving some
examples.

16
00:00:48,940 --> 00:00:53,340
So let's start with a simple but fanciful
example.

17
00:00:54,750 --> 00:00:58,205
So suppose you're an anthropologist,
you're a cultural anthropologist.

18
00:00:58,205 --> 00:01:02,240
You're studying some foreign culture, and
you're trying to

19
00:01:02,240 --> 00:01:06,190
translate their language because their
language has been heretofore untranslated.

20
00:01:06,190 --> 00:01:09,460
So you go in and you're translating their
language.

21
00:01:09,460 --> 00:01:10,920
You're figuring out what they mean

22
00:01:10,920 --> 00:01:16,750
by various words that they use.
And you've translated most of their words.

23
00:01:16,750 --> 00:01:19,730
But there's this word they use, SPooG, and

24
00:01:20,780 --> 00:01:22,760
you haven't yet figured out what that
means.

25
00:01:22,760 --> 00:01:27,580
And so you're observing their behavior and
finally you come up with a hypothesis.

26
00:01:27,580 --> 00:01:28,080
[COUGH]

27
00:01:29,520 --> 00:01:31,990
And you're pretty confident of this
hypothesis.

28
00:01:31,990 --> 00:01:38,140
Your hypothesis is that they use SPooG as
a truth-functional connective.

29
00:01:38,140 --> 00:01:41,040
And it's a truth-functional connective
that has this truth table.

30
00:01:42,260 --> 00:01:45,880
It connects two propositions to make a
larger proposition.

31
00:01:45,880 --> 00:01:54,990
And, that resulting proposition, P SPooG Q
for whatever propositions,

32
00:01:54,990 --> 00:01:56,650
P and Q, SPooG is connecting.

33
00:01:56,650 --> 00:02:00,370
That resulting proposition is going to be
true whenever P

34
00:02:00,370 --> 00:02:04,390
is true or whenever both P and Q are
false.

35
00:02:04,390 --> 00:02:08,460
So the only scenario in which P SPooG Q is
going to

36
00:02:08,460 --> 00:02:12,080
be false is where P is false and Q is
true.

37
00:02:13,910 --> 00:02:16,845
So that's your hypothesis about what they
mean by SPooG.

38
00:02:18,900 --> 00:02:19,680
Okay.

39
00:02:19,680 --> 00:02:24,710
Now, one day you hear one of the members
of this foreign culture give an argument.

40
00:02:24,710 --> 00:02:27,200
And using the translation manual that
you've

41
00:02:27,200 --> 00:02:30,190
developed, you translate their argument as
follows.

42
00:02:31,710 --> 00:02:36,400
John is riding his bicycle SPooG Jill is
walking to the park.

43
00:02:38,610 --> 00:02:43,230
Premise to Jill is walking to the park,
therefore, John is riding his

44
00:02:43,230 --> 00:02:48,108
bicycle.
Now, is this argument valid

45
00:02:48,108 --> 00:02:53,250
or invalid?
Well, you can

46
00:02:53,250 --> 00:02:58,470
use the truth table for SPooG to figure
out whether the argument

47
00:02:58,470 --> 00:03:03,750
is valid or invalid and also to see why.
So, let's consider how

48
00:03:03,750 --> 00:03:09,120
the truth table would go.
Right.

49
00:03:09,120 --> 00:03:11,610
This is the truth table again for SPooG

50
00:03:11,610 --> 00:03:13,749
applied to the case that we are
considering.

51
00:03:15,050 --> 00:03:17,570
Well, premise one tells us that John is
riding

52
00:03:17,570 --> 00:03:21,160
his bicycle SPooG Jill is walking to the
park.

53
00:03:21,160 --> 00:03:23,390
So if premise one is true then we're
either

54
00:03:23,390 --> 00:03:25,840
in this scenario, in this scenario, or in
this scenario.

55
00:03:27,170 --> 00:03:28,940
Premise two tells us that Jill

56
00:03:28,940 --> 00:03:30,330
is walking to the park.

57
00:03:30,330 --> 00:03:31,990
So if premise two is true, then we're

58
00:03:31,990 --> 00:03:35,193
either in this scenario or in this
scenario.

59
00:03:35,193 --> 00:03:35,870
Okay.

60
00:03:35,870 --> 00:03:38,560
So what do premises one and two put
together?

61
00:03:38,560 --> 00:03:40,330
What do they tell us?

62
00:03:40,330 --> 00:03:44,640
Well premises one and two tell us that,
first of all, we're not in scenario

63
00:03:44,640 --> 00:03:48,360
because in this scenario it's not going to
be true that Jill is walking to the park.

64
00:03:50,370 --> 00:03:54,710
Again, premises one and two put together
tell us that we're not in this scenario.

65
00:03:54,710 --> 00:03:57,200
because in this scenario, it's not
going to be true that John

66
00:03:57,200 --> 00:04:00,240
is riding his bicycle SPooG Jill is
walking to the park.

67
00:04:02,110 --> 00:04:05,390
Again, premises one and two tell us that
we're not in this scenario, because

68
00:04:05,390 --> 00:04:08,980
in this scenario it's not going to be true
that Jill is walking to the park.

69
00:04:08,980 --> 00:04:12,480
So, if premises one and two were both
true,

70
00:04:12,480 --> 00:04:14,889
that tells us that we've gotta be in this
scenario.

71
00:04:16,080 --> 00:04:21,490
But in this scenario, it's gotta be true
that John is riding his bicycle.

72
00:04:21,490 --> 00:04:25,640
And so, using the truth table for SPooG

73
00:04:25,640 --> 00:04:30,100
you can figure out that this argument is
valid.

74
00:04:31,110 --> 00:04:32,720
And you can explain why it's valid.

75
00:04:34,990 --> 00:04:35,890
See?

76
00:04:35,890 --> 00:04:41,730
So, you can use the truth table for a
truth-functional connective

77
00:04:41,730 --> 00:04:47,350
that you don't have any understanding of
independently, of the truth table for it.

78
00:04:47,350 --> 00:04:47,540
Right?

79
00:04:47,540 --> 00:04:49,780
It's not a connective that you use in your
language.

80
00:04:51,130 --> 00:04:55,309
You can just use the truth table to figure
out that a particular argument is valid.

81
00:04:56,530 --> 00:04:58,620
Let's try that with a more complicated
case.

82
00:05:00,900 --> 00:05:02,250
Okay.

83
00:05:02,250 --> 00:05:05,810
So now suppose you hear some member of
this foreign

84
00:05:05,810 --> 00:05:10,450
culture arguing as follows, at least
according to your translation.

85
00:05:10,450 --> 00:05:15,970
John is riding his bicycle SPooG Jill is
walking to the park or Frank is sick.

86
00:05:18,060 --> 00:05:22,130
Frank is not sick.
John is not riding his bicycle.

87
00:05:23,430 --> 00:05:28,560
Therefore, Jill is walking to the park.
Okay.

88
00:05:28,560 --> 00:05:32,740
Now is that argument valid or not?
Okay.

89
00:05:32,740 --> 00:05:34,969
So let's look at this more complicated
truth table.

90
00:05:39,130 --> 00:05:39,440
Okay.

91
00:05:39,440 --> 00:05:43,700
So, premise three tells us that John is
not riding his bicycle.

92
00:05:43,700 --> 00:05:48,070
So, that tells us that we've gotta be in
one of these four scenarios.

93
00:05:48,070 --> 00:05:52,210
Premise two tells us that Frank is not
sick, so that tells us that we've

94
00:05:52,210 --> 00:05:52,400
[INAUDIBLE]

95
00:05:52,400 --> 00:05:54,627
gotta be in one of these four scenarios.

96
00:05:54,627 --> 00:05:59,012
And premise one tells us that John is
riding his bicycle

97
00:05:59,012 --> 00:06:02,460
SPooG Jill is walking to the park or Frank
is sick.

98
00:06:02,460 --> 00:06:07,085
So, that tells us that we've gotta be in
one of these scenarios right here.

99
00:06:07,085 --> 00:06:08,650
Okay.

100
00:06:08,650 --> 00:06:12,160
So, based on that information, what can we
figure out?

101
00:06:12,160 --> 00:06:13,300
Well.

102
00:06:13,300 --> 00:06:15,260
We can immediately figure out that we're
not in

103
00:06:15,260 --> 00:06:17,400
one of these top four scenarios because in
all

104
00:06:17,400 --> 00:06:21,150
of those top four scenarios, Frank is
riding his bicycle.

105
00:06:21,150 --> 00:06:21,400
Right?

106
00:06:21,400 --> 00:06:26,310
So premises one, two and three together
will rule out the top four scenarios.

107
00:06:26,310 --> 00:06:28,880
We can also figure out that we're not in
scenarios five

108
00:06:28,880 --> 00:06:32,570
or seven because in the scenarios five or
seven, Frank is sick.

109
00:06:33,720 --> 00:06:35,600
So we can rule out those scenarios.

110
00:06:38,050 --> 00:06:41,370
and, we can rule out the sixth, the

111
00:06:41,370 --> 00:06:44,620
sixth scenario because in the sixth
scenario, it's not

112
00:06:44,620 --> 00:06:48,740
true that John is riding his bicycle SPooG
Jill

113
00:06:48,740 --> 00:06:50,890
is walking to the park or Frank is sick.

114
00:06:50,890 --> 00:06:54,350
So, based on the information that we get
from premises one, two and

115
00:06:54,350 --> 00:06:58,990
three of the argument, we can deduce that
we're in this last scenario.

116
00:07:00,020 --> 00:07:03,500
We've gotta be in this last scenario.
And, in this last scenario,

117
00:07:03,500 --> 00:07:04,320
it's false

118
00:07:04,320 --> 00:07:04,960
[SOUND]

119
00:07:04,960 --> 00:07:06,520
that Jill is walking to the park.

120
00:07:06,520 --> 00:07:08,750
In other words, Jill is not walking to the
park.

121
00:07:11,340 --> 00:07:14,900
But that's not what the conclusion of our
argument says.

122
00:07:14,900 --> 00:07:19,750
So this argument, this more complex
argument is not valid.

123
00:07:19,750 --> 00:07:26,680
And we can prove that as we just did using
the truth table for SPooG and disjunction.

124
00:07:26,680 --> 00:07:27,616
Okay.

125
00:07:27,616 --> 00:07:27,616
[SOUND]

126
00:07:27,616 --> 00:07:33,300
Now, let me show how we can do the same
thing with quantifiers.

127
00:07:33,300 --> 00:07:38,964
So, imagine, as you're translating this
foreign language,

128
00:07:38,964 --> 00:07:44,038
you hear a word It's always said in a
high-pitched

129
00:07:44,038 --> 00:07:49,112
tone, an excited tone Jid!
And you wonder what does Jid!

130
00:07:49,112 --> 00:07:52,770
mean.
Well, after watching the way they

131
00:07:52,770 --> 00:07:56,964
use that word, you come up with a
hypothesis.

132
00:07:56,964 --> 00:08:00,560
Your hypothesis is that when they use

133
00:08:00,560 --> 00:08:04,504
the word Jid!
they're using a quantifier.

134
00:08:04,504 --> 00:08:09,740
And the quantifier works like this.
When you say Jid!

135
00:08:09,740 --> 00:08:18,820
F or G, you're saying that there aren't
any F that

136
00:08:18,820 --> 00:08:24,819
are outside the G category, but there is
an F that's inside the G category.

137
00:08:27,420 --> 00:08:32,300
So, this diagram represents, according to
your hypothesis, this

138
00:08:32,300 --> 00:08:37,676
diagram represents what the members of
this foreign culture mean by Jid!

139
00:08:37,676 --> 00:08:39,554
Okay.

140
00:08:39,554 --> 00:08:43,130
So now, suppose you hear one of the

141
00:08:43,130 --> 00:08:45,510
members of this foreign culture give the
following argument.

142
00:08:47,090 --> 00:08:47,790
Jid!

143
00:08:47,790 --> 00:08:51,114
giraffes are herbivores.
You translate.

144
00:08:51,114 --> 00:08:52,698
Then they

145
00:08:52,698 --> 00:08:57,310
say, Jid!
Herbivores are mammals.

146
00:08:57,310 --> 00:08:59,610
And then, they draw the conclusion.

147
00:08:59,610 --> 00:09:03,529
There are some giraffes and all of them
are mammals.

148
00:09:05,830 --> 00:09:13,320
Now, is that argument valid?
Well, let's use the Venn diagram for Jid!

149
00:09:13,320 --> 00:09:18,070
to figure out whether or not it's valid.
So the first premise says Jid!

150
00:09:18,070 --> 00:09:22,930
giraffes are herbivores.
So, how do we represent that?

151
00:09:22,930 --> 00:09:27,260
Well, remember we have to shade out the
portion

152
00:09:27,260 --> 00:09:31,100
of the giraffe circle that's outside the
herbivore circle,

153
00:09:31,100 --> 00:09:33,790
and we have to draw an x that's

154
00:09:33,790 --> 00:09:37,070
in the giraffe circle and in the herbivore
circle.

155
00:09:37,070 --> 00:09:38,850
We don't know quite where to draw that x,
so

156
00:09:38,850 --> 00:09:41,160
let's hedge our bets right now and draw it
over here.

157
00:09:42,850 --> 00:09:43,350
Okay.

158
00:09:48,630 --> 00:09:51,347
the second premise, though, tells us that
Jid!

159
00:09:51,347 --> 00:09:56,280
herbivores are mammals.

160
00:09:56,280 --> 00:09:56,350
Now

161
00:09:56,350 --> 00:09:57,380
[SOUND]

162
00:09:57,380 --> 00:09:58,690
how do we represent that?

163
00:09:58,690 --> 00:10:01,452
Well, remember by the Venn diagram, we
have

164
00:10:01,452 --> 00:10:03,960
to shade out that part of the herbivore
circle

165
00:10:03,960 --> 00:10:06,534
that's outside the mammal circle and draw
an x

166
00:10:06,534 --> 00:10:09,900
inside the herbivore circle that's also
the mammals circle.

167
00:10:09,900 --> 00:10:12,408
Right now we know that this x that we drew
on the

168
00:10:12,408 --> 00:10:18,282
edge has to be inside the mammals circle,
not outside the mammals circle.

169
00:10:18,282 --> 00:10:22,892
Okay?
So, we're going to put this x right

170
00:10:22,892 --> 00:10:28,220
here on the edge of the mammals circle and
the

171
00:10:28,220 --> 00:10:34,124
giraffes circle because we don't know
specifically

172
00:10:34,124 --> 00:10:38,608
which one it's supposed to go into.
Oh, but wait.

173
00:10:38,608 --> 00:10:38,750
We do.

174
00:10:38,750 --> 00:10:43,860
And here's why we do.
Remember, that when we drew the x on the

175
00:10:43,860 --> 00:10:50,130
border of the mammal circle and

176
00:10:50,130 --> 00:10:55,500
the herbivore circle right here, inside
the giraffe circle, we weren't sure

177
00:10:55,500 --> 00:10:58,570
if that x was supposed to go in here or in
here.

178
00:10:58,570 --> 00:11:01,330
Now we know that this x is supposed to go
in here.

179
00:11:02,620 --> 00:11:08,990
So maybe there are also herbivore mammals
that are not giraffes, but what we can

180
00:11:08,990 --> 00:11:15,406
definitely conclude from premises one and
two

181
00:11:15,406 --> 00:11:19,890
is that there are herbivore mammals that
are giraffes.

182
00:11:19,890 --> 00:11:23,719
So we know that there's an x that's
supposed to go in this region right here.

183
00:11:25,290 --> 00:11:25,290
[SOUND]

184
00:11:25,290 --> 00:11:31,940
There's an x in that region right here and
then, this whole region is shaded out.

185
00:11:31,940 --> 00:11:34,110
There's nothing in this whole region.

186
00:11:36,570 --> 00:11:37,440
That's what we know

187
00:11:37,440 --> 00:11:38,300
[SOUND]

188
00:11:38,300 --> 00:11:41,670
from premises one and two using the Venn
diagram for Jid.

189
00:11:43,230 --> 00:11:45,520
Excuse me, for Jid!.

190
00:11:45,520 --> 00:11:46,020
Now,

191
00:11:47,900 --> 00:11:52,340
let's apply that to our argument.
Well, what does our argument say?

192
00:11:52,340 --> 00:11:56,070
It says, there are some giraffes and all
of them are mammals.

193
00:11:56,070 --> 00:11:58,880
But, we can, that's exactly what we can
read

194
00:11:58,880 --> 00:12:03,080
off from the Venn diagram that we just
constructed.

195
00:12:03,080 --> 00:12:03,240
Right?

196
00:12:03,240 --> 00:12:04,714
If you look at the Venn diagram that we

197
00:12:04,714 --> 00:12:10,270
just constructed, there's an x that's
inside the giraffe circle.

198
00:12:10,270 --> 00:12:13,740
And inside the mammal circle and every

199
00:12:13,740 --> 00:12:17,720
part of the giraffe circle that's outside
of the mammal circle is shaded out.

200
00:12:19,570 --> 00:12:22,765
So there aren't any giraffes that are not
mammals.

201
00:12:22,765 --> 00:12:26,030
Okay.

202
00:12:26,030 --> 00:12:30,730
So we just figured out that this argument
is valid, and

203
00:12:30,730 --> 00:12:36,350
we figured it out even though our only
understanding of Jid!

204
00:12:36,350 --> 00:12:38,660
was from the Venn diagram that we
constructed.

205
00:12:40,620 --> 00:12:42,080
Now,

206
00:12:42,080 --> 00:12:42,950
I have another question.

207
00:12:45,080 --> 00:12:46,490
Given this Venn diagram

208
00:12:46,490 --> 00:12:46,990
[SOUND]

209
00:12:52,680 --> 00:12:55,490
how would you translate Jid!
into English.

210
00:12:57,410 --> 00:13:03,610
Well, often people use the word

211
00:13:03,610 --> 00:13:09,930
all, to mean the same thing that Jid!

212
00:13:09,930 --> 00:13:12,370
means according to this Van Diagram.

213
00:13:12,370 --> 00:13:14,770
People speaking ordinary English use the

214
00:13:14,770 --> 00:13:18,375
word all to mean precisely this
quantifier.

215
00:13:18,375 --> 00:13:18,375
[SOUND]

216
00:13:18,375 --> 00:13:20,090
Right?

217
00:13:20,090 --> 00:13:28,030
When they use the word all, they often
mean that there are things

218
00:13:28,030 --> 00:13:33,226
in the category that they're modifying by
all, like if I say all ravens are birds.

219
00:13:33,226 --> 00:13:38,390
Often, what I'm understood to mean is
there

220
00:13:38,390 --> 00:13:43,460
actually are some ravens.
And all the ravens there are, are birds.

221
00:13:43,460 --> 00:13:50,860
So there are some things in the F circle
but they're all inside the G circle.

222
00:13:50,860 --> 00:13:54,290
There are some things in the circle of
ravens, let's

223
00:13:54,290 --> 00:13:58,240
say, but they're all also inside the
circle of birds.

224
00:13:58,240 --> 00:14:01,570
There are no ravens outside the bird
circle,

225
00:14:01,570 --> 00:14:04,690
but there are ravens inside the bird
circle.

226
00:14:04,690 --> 00:14:08,968
That's often what people would understand
me to mean if I said, all ravens

227
00:14:08,968 --> 00:14:10,110
are birds.

228
00:14:10,110 --> 00:14:13,140
Now, we've been using the quantifier all,
we've

229
00:14:13,140 --> 00:14:16,410
been understanding the quantifier all in a
different way.

230
00:14:16,410 --> 00:14:19,960
So that it doesn't imply that there
actually are

231
00:14:19,960 --> 00:14:23,170
members of the category that's being
modified by all.

232
00:14:23,170 --> 00:14:26,380
So the way we've been using the quantifier
all, if you say all ravens

233
00:14:26,380 --> 00:14:31,860
are birds, all you mean is there aren't
any ravens that are not birds.

234
00:14:33,430 --> 00:14:34,000
But that's not

235
00:14:34,000 --> 00:14:38,620
the same as saying there are ravens and
all of them are birds.

236
00:14:41,770 --> 00:14:45,125
So it looks like we can use the Venn
diagram for Jid!

237
00:14:45,125 --> 00:14:49,040
to translate that quantifier in a foreign

238
00:14:49,040 --> 00:14:53,190
language into a familiar quantifier from
ordinary English.

