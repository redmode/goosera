1
00:00:02,970 --> 00:00:06,072
So as we saw last time there are three
distinct levels

2
00:00:06,072 --> 00:00:10,571
of language: linguistic level, the speech
level, and the conversational level.

3
00:00:10,571 --> 00:00:14,120
And all of these levels of meaning affect
arguments.

4
00:00:14,120 --> 00:00:18,570
So in the next three lectures we want to
work through these levels one by one.

5
00:00:18,570 --> 00:00:22,390
And this lecture is going to deal in
particular with the linguistic

6
00:00:22,390 --> 00:00:27,690
level of language, which is simply the
production of a meaningful utterance.

7
00:00:27,690 --> 00:00:28,200
So in order

8
00:00:28,200 --> 00:00:30,140
to perform a linguistic act, all you have
to

9
00:00:30,140 --> 00:00:34,420
do is utter a set of words that are
meaningful.

10
00:00:34,420 --> 00:00:37,190
That fit together according to the
semantics.

11
00:00:37,190 --> 00:00:39,130
That is, the meanings of particular words.

12
00:00:39,130 --> 00:00:43,296
And the syntax or the grammar of the
language in general.

13
00:00:43,296 --> 00:00:48,780
For example, it's easy.
It's easy is a linguistic act.

14
00:00:48,780 --> 00:00:53,460
Because it's as a contraction, you're
allowed to contract

15
00:00:53,460 --> 00:00:56,120
it and is according to the rules of
English.

16
00:00:56,120 --> 00:00:59,180
And easy is a word, so, it's easy, follows

17
00:00:59,180 --> 00:01:02,865
the semantics, and the syntax of the
language of English.

18
00:01:02,865 --> 00:01:05,590
That's all there is to it.

19
00:01:05,590 --> 00:01:10,190
Now although linguistic acts are really
simple, they do require some special

20
00:01:10,190 --> 00:01:11,929
components that are worth separating out,

21
00:01:12,930 --> 00:01:15,490
for example, they require meaningful
words.

22
00:01:15,490 --> 00:01:18,300
When you simply hum a tune like

23
00:01:18,300 --> 00:01:18,800
[MUSIC],

24
00:01:22,580 --> 00:01:24,360
then you're not performing a linguistic
act.

25
00:01:24,360 --> 00:01:26,120
Because there are no meaningful words in
it.

26
00:01:26,120 --> 00:01:27,880
But when you sing a song,

27
00:01:27,880 --> 00:01:27,884
[MUSIC].

28
00:01:27,884 --> 00:01:35,610
I love Miranda and Nicholas too.

29
00:01:35,610 --> 00:01:40,990
Then, you are performing a linguistic act,
because you uttered words

30
00:01:40,990 --> 00:01:44,142
that were meaningful when they were put
together in that way.

31
00:01:44,142 --> 00:01:47,150
And I've been taking this for granted, but
of

32
00:01:47,150 --> 00:01:49,050
course, the words you utter have to be
meaningful.

33
00:01:49,050 --> 00:01:52,635
it's not going to be a linguistic act if
you utter,

34
00:01:52,635 --> 00:01:56,860
a, what looks like a sentence.
Namely, a set of sounds.

35
00:01:56,860 --> 00:01:59,800
That look like words if they're not really
words.

36
00:01:59,800 --> 00:02:02,720
So if you say, twas brillig and the slithy
toves did grye and gimble in the wabe.

37
00:02:02,720 --> 00:02:08,965
And so on from Louis Carroll's famous
Jabberwocky poem.

38
00:02:08,965 --> 00:02:11,880
Then it's not going to be a linguistic
act,

39
00:02:11,880 --> 00:02:16,420
if those words are not meaningful words in
any language.

40
00:02:16,420 --> 00:02:17,985
And you can also get

41
00:02:17,985 --> 00:02:21,700
nonsense when you take words that have
meanings and put

42
00:02:21,700 --> 00:02:23,790
them together in an order that doesn't
make any sense.

43
00:02:24,980 --> 00:02:32,490
My dog has fleas makes sense but dog fleas
my has doesn't make any sense.

44
00:02:32,490 --> 00:02:37,690
So meaningful words with the wrong
grammatical structure won't

45
00:02:37,690 --> 00:02:43,210
work and Noam Chomsky from MIT taught us
that you can

46
00:02:43,210 --> 00:02:48,410
also get nonsense when you take words that
make sense and you put them together with

47
00:02:48,410 --> 00:02:51,770
the right grammatical structure, but they
still don't

48
00:02:51,770 --> 00:02:55,210
fit together because of the relation
between the meanings.

49
00:02:56,220 --> 00:03:00,750
His example here was colorless green ideas
sleep furiously.

50
00:03:00,750 --> 00:03:02,705
What does that mean?

51
00:03:02,705 --> 00:03:08,810
Colorless green ideas sleep furiously?
Well colorless makes sense;

52
00:03:08,810 --> 00:03:15,050
green that's a word, ideas, sleep,
furiously.

53
00:03:15,050 --> 00:03:18,510
Each of those words makes sense and
they're each in

54
00:03:18,510 --> 00:03:22,108
their appropriate grammatical role, but
altogether it doesn't make any sense.

55
00:03:22,108 --> 00:03:25,250
So they're lots of ways you can get
nonsense in language.

56
00:03:25,250 --> 00:03:28,540
And when you do, you're not performing a
linguistic act.

57
00:03:28,540 --> 00:03:35,100
You know there's some really fun examples
where it's not clear whether or not

58
00:03:35,100 --> 00:03:36,689
the utterance is meaningful.

59
00:03:36,689 --> 00:03:42,240
Now, some of these examples, among my
favorites are garden path sentences.

60
00:03:42,240 --> 00:03:46,232
Here's one.
The man who whistles tunes pianos.

61
00:03:46,232 --> 00:03:49,770
Wait a minute.
What does that mean?

62
00:03:49,770 --> 00:03:55,880
If you think of it as the man who whistles
tunes is one unit, then you don't

63
00:03:55,880 --> 00:04:00,450
understand what the word pianos is doing,
because the man who whistles tunes sounds

64
00:04:00,450 --> 00:04:04,760
like a reference to a particular man, and
pianos is not a verb.

65
00:04:05,770 --> 00:04:09,160
But if you think of it as the man who
whistles is one unit

66
00:04:10,180 --> 00:04:16,010
and the second unit is tunes pianos, so
it's the man who whistles tunes pianos.

67
00:04:16,010 --> 00:04:20,520
Then it makes sense, because it's the man
who's whistling also tunes pianos.

68
00:04:21,730 --> 00:04:25,560
So you have to be able to carve the set of
words up into the right

69
00:04:25,560 --> 00:04:28,300
units and see what grammatical structure
they

70
00:04:28,300 --> 00:04:31,350
have in order to understand the sentence.

71
00:04:31,350 --> 00:04:35,700
because tunes can either be a verb, which
tells you what the man is doing to the

72
00:04:35,700 --> 00:04:39,755
pianos, or it can be a noun which refers
to the thing that the man is whistling.

73
00:04:39,755 --> 00:04:43,370
And you have to get those grammatical
categories straight

74
00:04:43,370 --> 00:04:46,120
and the garden path sentences lead you
astray and make

75
00:04:46,120 --> 00:04:47,521
you think of it in the wrong way and

76
00:04:47,521 --> 00:04:50,610
there'll be some more examples of that in
the exercises.

77
00:04:51,680 --> 00:04:54,854
But my favorite example of all is buffalo,
buffalo, buffalo.

78
00:04:54,854 --> 00:04:57,358
What does that mean?

79
00:04:57,358 --> 00:05:03,490
Well buffalo are American bison, okay.
But buffalo, the word buffalo in English

80
00:05:03,490 --> 00:05:09,940
that is, can also be used as a verb to
refer to tricking or fooling someone.

81
00:05:09,940 --> 00:05:12,420
So you can have buffalo, American bison,
buffaloing,

82
00:05:12,420 --> 00:05:12,850
that is tricking or fooling, buffalo, American bison.

83
00:05:12,850 --> 00:05:13,541
Buffalo buffalo buffalo.

84
00:05:13,541 --> 00:05:16,737
But you can go even further,

85
00:05:16,737 --> 00:05:22,691
because there's a city in New York named
Buffalo,

86
00:05:22,691 --> 00:05:27,650
and of course, there can be buffalo, that
is American

87
00:05:27,650 --> 00:05:32,400
bison, from the city of Buffalo, New York
and they're called Buffalo, buffalo.

88
00:05:32,400 --> 00:05:36,910
And when they trick or fool other bi,
American bison

89
00:05:36,910 --> 00:05:41,392
from Buffalo New York, then you have
Buffalo, buffalo, buffalo, buffalo,

90
00:05:41,392 --> 00:05:42,141
buffalo.

91
00:05:43,240 --> 00:05:45,980
Or buffalo, buffalo, buffalo, buffalo,
buffalo.

92
00:05:45,980 --> 00:05:48,830
Which doesn't sound like a meaningful
utterance, but it is.

93
00:05:48,830 --> 00:05:51,200
And you can go even further, you can
actually build

94
00:05:51,200 --> 00:05:56,280
it out to 11 straight utterances of the
word buffalo.

95
00:05:56,280 --> 00:05:58,150
Buffalo, buffalo, buffalo, buffalo,
buffalo,

96
00:05:58,150 --> 00:06:00,486
buffalo, buffalo, buffalo, buffalo,
buffalo, buffalo.

97
00:06:00,486 --> 00:06:01,849
Now, tell me what that means.

98
00:06:01,849 --> 00:06:05,870
I'm not going to explain it to you,
because it takes a while to explain it.

99
00:06:05,870 --> 00:06:06,648
But If

100
00:06:06,648 --> 00:06:09,355
you think about it, you might be able to
figure it out.

101
00:06:09,355 --> 00:06:12,075
And even if you can't figure out eleven

102
00:06:12,075 --> 00:06:15,090
buffalos in a row, the point still holds,
because

103
00:06:15,090 --> 00:06:18,030
the point's just that sometimes what
doesn't seem

104
00:06:18,030 --> 00:06:21,220
meaningful turns out to be meaningful, and
if you're

105
00:06:21,220 --> 00:06:24,550
careful and, and charitable, and do your
best

106
00:06:24,550 --> 00:06:26,900
to interpret what it really means, then
you might

107
00:06:26,900 --> 00:06:28,430
be able to make sense out of some

108
00:06:28,430 --> 00:06:31,170
utterances that don't seem to make sense
at first.

109
00:06:31,170 --> 00:06:31,660
And then when

110
00:06:31,660 --> 00:06:35,910
you can makes sense out of them, then
they're linguistic acts.

111
00:06:35,910 --> 00:06:37,240
For now, I don't have time to go into

112
00:06:37,240 --> 00:06:41,040
any detail on semantics or syntax,
although we will

113
00:06:41,040 --> 00:06:45,060
discuss some details when we discuss
vagueness and ambiguity

114
00:06:45,060 --> 00:06:48,270
in the discussion of fallacies later in
this course.

115
00:06:48,270 --> 00:06:50,040
But I hope that the linguistic level

116
00:06:50,040 --> 00:06:53,180
is pretty simple and understandable, so we
can

117
00:06:53,180 --> 00:06:57,050
go on and look in more detail at the
speech act level and the conversational

118
00:06:57,050 --> 00:06:58,290
act level.

