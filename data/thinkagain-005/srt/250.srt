1
00:00:03,040 --> 00:00:05,540
Now we've learned how to identify an
argument

2
00:00:05,540 --> 00:00:07,620
and put it in standard form, and we've
also

3
00:00:07,620 --> 00:00:10,110
learned from the definition of argument
that the

4
00:00:10,110 --> 00:00:12,800
premises are intended to be reasons for
the conclusion.

5
00:00:14,100 --> 00:00:15,480
Great.

6
00:00:15,480 --> 00:00:17,860
Intended to be a reason for the
conclusion.

7
00:00:17,860 --> 00:00:20,460
Well, intentions are nice, but success is
better.

8
00:00:20,460 --> 00:00:23,350
What we need to figure out is when the
person

9
00:00:23,350 --> 00:00:29,250
succeeds in giving premises that really
are reasons for the conclusion.

10
00:00:29,250 --> 00:00:34,420
For simplicity, let's focus on arguments
whose purpose is justification.

11
00:00:35,970 --> 00:00:41,853
Then the question is, do the premises
justify you in believing the conclusion?

12
00:00:41,853 --> 00:00:47,930
Well, imagine that you don't know whether
there's any life on Mars.

13
00:00:47,930 --> 00:00:50,498
You have no evidence, one way or the
other.

14
00:00:50,498 --> 00:00:54,095
Then you ask a friend, and the friend
says,

15
00:00:54,095 --> 00:00:58,320
hah, I know there's life on Mars.
I can prove it to you.

16
00:00:58,320 --> 00:01:01,710
Here, this argument will show you that
there's life on Mars.

17
00:01:03,260 --> 00:01:06,730
There is at least one bacterium on Mars.

18
00:01:06,730 --> 00:01:09,380
Therefore there is life on Mars.

19
00:01:09,380 --> 00:01:14,680
Now, notice that if the premise is true,
the conclusion has gotta be true.

20
00:01:14,680 --> 00:01:19,466
And if you and your friend are justified
in believing

21
00:01:19,466 --> 00:01:21,740
the premise, then you and your friend

22
00:01:21,740 --> 00:01:25,450
are also justified in believing the
conclusion.

23
00:01:25,450 --> 00:01:27,550
So, this argument looks pretty good so
far.

24
00:01:28,570 --> 00:01:32,100
But of course, you have to ask your
friend, well, how

25
00:01:32,100 --> 00:01:34,735
do you know that there's at least one
bacterium on Mars?

26
00:01:34,735 --> 00:01:39,065
And suppose your friend says, well, I, I'm
just guessing.

27
00:01:39,065 --> 00:01:44,250
Then, the argument is clearly no good.

28
00:01:44,250 --> 00:01:44,590
If there's

29
00:01:44,590 --> 00:01:47,650
no reason to believe the premise because
your friend is

30
00:01:47,650 --> 00:01:52,130
just guessing, then you're not justified
in believing that premise,

31
00:01:52,130 --> 00:01:54,970
and if you're not justified in believing
the premise, then

32
00:01:54,970 --> 00:01:58,680
how can that premise make you justified in
believing the conclusion?

33
00:01:59,700 --> 00:02:03,700
More generally, an argument cannot justify
you in believing the

34
00:02:03,700 --> 00:02:08,519
conclusion unless you're justified in
accepting the premises of that argument.

35
00:02:09,920 --> 00:02:15,671
Now, suppose your friend says, but I do
have a reason for the premise, I do!

36
00:02:15,671 --> 00:02:19,330
Then, we have to ask, what kind of reason
is it?

37
00:02:19,330 --> 00:02:22,570
And, at that point, your friend needs to

38
00:02:22,570 --> 00:02:24,820
express that reason, and how do we express
reasons?

39
00:02:24,820 --> 00:02:25,760
In arguments.

40
00:02:25,760 --> 00:02:29,283
So your friend needs to give another
argument for the premise, where

41
00:02:29,283 --> 00:02:35,452
the premise of the first argument is the
conclusion of the second argument.

42
00:02:35,452 --> 00:02:38,520
But wait a minute, now we've got a
problem.

43
00:02:38,520 --> 00:02:43,640
Because that second argument is itself
going to have premises,

44
00:02:43,640 --> 00:02:46,410
and you have to be justified in believing
those.

45
00:02:46,410 --> 00:02:49,824
So, the premises of the second argument
have to be the

46
00:02:49,824 --> 00:02:54,980
conclusion of a third argument, and so on
and so on.

47
00:02:54,980 --> 00:02:57,220
Because the third argument needs premises,
they have to be

48
00:02:57,220 --> 00:03:00,540
justified, so they have to be the
conclusion of another argument,

49
00:03:00,540 --> 00:03:03,760
which also has to have premises, and those
have to be

50
00:03:03,760 --> 00:03:06,600
the conclusion of another argument, and so
on and so on.

51
00:03:06,600 --> 00:03:09,020
It looked like we've got a real problem
here.

52
00:03:10,400 --> 00:03:12,580
In order for the premises to be justified,
they have

53
00:03:12,580 --> 00:03:14,840
to be backed up by an argument, but the
argument

54
00:03:14,840 --> 00:03:16,900
has premises of its own that have to be
backed

55
00:03:16,900 --> 00:03:19,379
up by another argument, and so on and so
on.

56
00:03:19,379 --> 00:03:21,810
This problem is called The Problem of the

57
00:03:21,810 --> 00:03:25,810
Skeptical Regress, because you regress
back to one

58
00:03:25,810 --> 00:03:27,740
argument after another, after another,
after

59
00:03:27,740 --> 00:03:30,160
another, after another, after another,
after another.

60
00:03:30,160 --> 00:03:36,290
And it's hard to see how that regress is
ever going to come to an end.

61
00:03:36,290 --> 00:03:40,140
There seem to be only three ways to get
around the skeptical regress.

62
00:03:40,140 --> 00:03:43,388
The first is to start with a premise
that's unjustified.

63
00:03:43,388 --> 00:03:46,990
If it's unjustified, then it doesn't need
an argument to

64
00:03:46,990 --> 00:03:50,820
back it up, and that means that you're not
going to

65
00:03:50,820 --> 00:03:53,270
have this chain of arguments going back
and back

66
00:03:53,270 --> 00:03:55,976
and back and back and back and back and
back.

67
00:03:55,976 --> 00:03:58,170
The second possibility is to have a

68
00:03:58,170 --> 00:04:00,250
structure where the arguments move in a
circle.

69
00:04:01,570 --> 00:04:03,800
One claim is justified by another, which

70
00:04:03,800 --> 00:04:05,860
is justified by another, which is
justified by

71
00:04:05,860 --> 00:04:10,015
another, which is justified by the first
claim, and they just move in a circle.

72
00:04:10,015 --> 00:04:16,010
The third possibility is that the chain of
arguments goes back infinitely.

73
00:04:16,010 --> 00:04:20,390
It never stops.
Every claim has an argument to back it up.

74
00:04:20,390 --> 00:04:24,070
And there's no end, so you never have a
premise which

75
00:04:24,070 --> 00:04:26,520
doesn't have an argument to back it up,
because it's infinite.

76
00:04:27,670 --> 00:04:32,340
Those seem to be the three main options
here to avoid the skeptical regress.

77
00:04:32,340 --> 00:04:36,920
The first possibility then, is to start
with a premise that's unjustified.

78
00:04:36,920 --> 00:04:39,880
And that seems pretty neat, if you can get
away with it.

79
00:04:39,880 --> 00:04:41,180
But we already saw why that

80
00:04:41,180 --> 00:04:42,420
won't work.

81
00:04:42,420 --> 00:04:46,330
We saw your friend arguing that there's
life on Mars because

82
00:04:46,330 --> 00:04:49,855
there's at least one bacterium on Mars,
and he was just guessing.

83
00:04:49,855 --> 00:04:54,150
If you just guess at your premises, you
have no reason to believe them.

84
00:04:54,150 --> 00:04:57,345
Then, an argument that uses those premises

85
00:04:57,345 --> 00:05:00,236
cannot justify you in believing the
conclusion.

86
00:05:00,236 --> 00:05:06,790
But in addition, just think about it this
way, you could prove anything if

87
00:05:06,790 --> 00:05:09,050
we let you start with unjustified
premises.

88
00:05:09,050 --> 00:05:13,000
If you can just make up your premises for
no reason, then

89
00:05:13,000 --> 00:05:15,790
there's no stopping you from believing

90
00:05:15,790 --> 00:05:19,300
whatever, including things that are
obviously false.

91
00:05:19,300 --> 00:05:24,570
So it seems to be a real problem to start
with premises that are unjustified.

92
00:05:24,570 --> 00:05:28,049
Next, the second way to respond to the
skeptical

93
00:05:28,049 --> 00:05:32,240
regress is to use a circular structure,
and it's

94
00:05:32,240 --> 00:05:34,200
kind of neat, if you think about it.

95
00:05:34,200 --> 00:05:37,180
Because, if you want to prove one claim,

96
00:05:37,180 --> 00:05:39,340
you prove it on the basis of another
claim.

97
00:05:39,340 --> 00:05:41,790
And then you prove that second claim on
the basis of a third.

98
00:05:41,790 --> 00:05:43,210
And that third on the basis of fourth.

99
00:05:43,210 --> 00:05:46,820
And the fourth on the basis of a fifth.
And the fifth on the basis of the first.

100
00:05:46,820 --> 00:05:48,918
And now you've got this circle.

101
00:05:48,918 --> 00:05:53,580
And the arguments go in a circle, but that
means that every premise has

102
00:05:53,580 --> 00:05:57,190
an argument to back it up, because you can
keep going around the circle forever.

103
00:05:58,751 --> 00:06:03,133
And if you think about it a little bit,
it'll be obvious that that's

104
00:06:03,133 --> 00:06:06,290
no good, and that can be shown by looking
at the smallest circle there is.

105
00:06:07,530 --> 00:06:11,750
So, suppose your friend says, I can prove
there's life on Mars, here's my argument.

106
00:06:11,750 --> 00:06:12,830
There is life on Mars.

107
00:06:12,830 --> 00:06:17,005
Therefore, there is life on Mars.
Clearly that's no good.

108
00:06:17,005 --> 00:06:21,380
And the reason why it's no good is that if
you

109
00:06:21,380 --> 00:06:24,180
didn't know whether there was life on Mars
to begin with,

110
00:06:24,180 --> 00:06:26,625
you wouldn't know whether the premise was
true.

111
00:06:26,625 --> 00:06:28,940
because if you don't know the conclusion,
you can't

112
00:06:28,940 --> 00:06:31,310
know the premise since the premise is the
conclusion.

113
00:06:32,400 --> 00:06:34,670
So if you're not justified in believing

114
00:06:34,670 --> 00:06:37,030
the conclusion to begin with, you're not
justified

115
00:06:37,030 --> 00:06:38,340
in believing the premise, and that means

116
00:06:38,340 --> 00:06:41,380
that the argument didn't really get you
anywhere.

117
00:06:41,380 --> 00:06:42,925
It just ends up where it started.

118
00:06:42,925 --> 00:06:46,500
And in addition, it has the same problem

119
00:06:46,500 --> 00:06:50,065
we saw in the first approach, involving
unjustified premises.

120
00:06:50,065 --> 00:06:54,041
Because you can use circular arguments to
prove anything.

121
00:06:54,041 --> 00:06:54,541
You can prove there's life on Mars.

122
00:06:54,541 --> 00:06:55,315
There's life on Mars, therefore there's
life on Mars.

123
00:06:55,315 --> 00:06:55,880
You can prove there's

124
00:07:00,310 --> 00:07:01,130
no life on Mars.

125
00:07:01,130 --> 00:07:04,270
There's no life on Mars, therefore there's
no life on Mars.

126
00:07:04,270 --> 00:07:05,880
You can do it either way.

127
00:07:05,880 --> 00:07:08,830
And the fact that an argument can be used
either way to

128
00:07:08,830 --> 00:07:14,060
prove either conclusion suggests there's a
big problem with that kind of argument.

129
00:07:14,060 --> 00:07:17,550
So now we're down to the third and final
way to get

130
00:07:17,550 --> 00:07:23,142
around the skeptical regress, and that's
to use an infinite chain of arguments.

131
00:07:23,142 --> 00:07:25,690
But if you think about it in a concrete

132
00:07:25,690 --> 00:07:29,250
case, you'll see why that's a problem as
well.

133
00:07:30,460 --> 00:07:33,665
Suppose your friend says, there's life on
Mars and I can prove it.

134
00:07:33,665 --> 00:07:36,010
And you say fine, give me a reason.

135
00:07:37,090 --> 00:07:41,280
Well, there's at least one bacterium on
Mars, therefore, there's life on Mars.

136
00:07:41,280 --> 00:07:43,440
And you go, okay, fine, but how do

137
00:07:43,440 --> 00:07:46,070
you know there's at least one bacterium on
Mars?

138
00:07:46,070 --> 00:07:47,430
I've got another argument, he says.

139
00:07:48,610 --> 00:07:50,720
There are at least two bacteria

140
00:07:50,720 --> 00:07:51,510
on Mars.

141
00:07:51,510 --> 00:07:55,360
Therefore, there's at least one bacterium
on Mars.

142
00:07:55,360 --> 00:07:56,830
But how do you know there's at least two?

143
00:07:56,830 --> 00:07:59,465
Well, there are at least three bacteria on
Mars,

144
00:07:59,465 --> 00:08:03,310
therefore there are at least two bacteria
on Mars.

145
00:08:03,310 --> 00:08:05,360
But how do you know there are at least
three?

146
00:08:05,360 --> 00:08:07,460
Well, there are at least four, so there
are at least three.

147
00:08:07,460 --> 00:08:09,240
Well, there are at least five, so there
are at least four.

148
00:08:09,240 --> 00:08:10,440
Well, there are at least six, so there are
at

149
00:08:10,440 --> 00:08:12,492
least five, and so on, and so on, and so
on.

150
00:08:12,492 --> 00:08:15,840
You could go on infinitely.
So, an infinite chain

151
00:08:15,840 --> 00:08:19,490
of arguments would allow you to prove that
there's life on

152
00:08:19,490 --> 00:08:24,040
Mars, even if you have no evidence
whatsoever of any bacteria.

153
00:08:24,040 --> 00:08:28,010
Because you're going to have an argument,
but if the premise that you're arguing

154
00:08:28,010 --> 00:08:33,670
from doesn't have an independent
justification, then the infinite chain is

155
00:08:33,670 --> 00:08:40,230
going to be no good at all in justifying
the conclusion of that argument.

156
00:08:40,230 --> 00:08:41,300
So many people

157
00:08:41,300 --> 00:08:46,854
see the skeptical regress as a deep and
serious philosophical issue.

158
00:08:46,854 --> 00:08:54,290
If the unjustified premise approach
doesn't work, and the circular argument

159
00:08:54,290 --> 00:08:59,440
structure doesn't work, and the infinite
chain of arguments doesn't work, then it's

160
00:08:59,440 --> 00:09:02,889
hard to see how we can get around the
problem, which is

161
00:09:02,889 --> 00:09:06,700
to say it's hard to see how any kind of
argument could ever

162
00:09:06,700 --> 00:09:09,850
justify us in believing anything.

163
00:09:09,850 --> 00:09:13,320
Philosophers really scratched their heads
about that

164
00:09:13,320 --> 00:09:15,048
for a long time and worry about it.

165
00:09:15,048 --> 00:09:16,100
It keeps them up at night.

166
00:09:17,730 --> 00:09:19,880
But, we're going to have to look at how

167
00:09:19,880 --> 00:09:24,630
practical people solve a similar problem
in everyday life.

168
00:09:24,630 --> 00:09:27,720
So how do we solve the skeptical regress
problem in everyday life?

169
00:09:27,720 --> 00:09:32,015
Well, there are various tricks that you
can use.

170
00:09:32,015 --> 00:09:38,040
For example, one way is to just start from
assumptions that everybody shares.

171
00:09:38,040 --> 00:09:38,540
So,

172
00:09:40,350 --> 00:09:42,100
if I say, well you really ought to

173
00:09:42,100 --> 00:09:45,509
buy a Honda, because Hondas are very
reliable cars.

174
00:09:47,130 --> 00:09:50,410
Then I'm assuming that you want your car
to be reliable.

175
00:09:50,410 --> 00:09:53,200
You don't like to have to take it in to
the mechanic all the time.

176
00:09:53,200 --> 00:09:55,144
You don't want it to break down on the
road.

177
00:09:55,144 --> 00:10:02,200
And if you want reliability and I want
reliability, then we can start

178
00:10:02,200 --> 00:10:05,350
from the assumption that reliability is a
good thing and that that's a reason to

179
00:10:05,350 --> 00:10:10,870
buy a car that is reliable.
But of course you might say, well, but

180
00:10:10,870 --> 00:10:16,030
are Hondas reliable?
And then I might appeal to an authority.

181
00:10:16,030 --> 00:10:19,510
Well, it's obvious that they are, or
Consumer Reports

182
00:10:19,510 --> 00:10:21,840
has done a study that shows that they're
reliable.

183
00:10:22,920 --> 00:10:24,760
And I can appeal to an authority and if
you

184
00:10:24,760 --> 00:10:29,070
accept that authority, you go Consumer
Reports, we can trust them.

185
00:10:29,070 --> 00:10:30,736
Then my argument's

186
00:10:30,736 --> 00:10:31,620
going to work.

187
00:10:31,620 --> 00:10:34,120
You're going to have a reason to believe
the conclusion, and it

188
00:10:34,120 --> 00:10:36,380
might persuade you, and make you come to
believe the conclusion.

189
00:10:37,600 --> 00:10:41,706
But, suppose that someone is going to
raise an objection.

190
00:10:41,706 --> 00:10:44,290
They say, well, Consumer Reports has been

191
00:10:44,290 --> 00:10:46,823
wrong before, they might be wrong this
time.

192
00:10:48,110 --> 00:10:51,970
Well, then I need to discount that
objection.

193
00:10:51,970 --> 00:10:55,810
I need to respond to it and say, well,
maybe they have

194
00:10:55,810 --> 00:10:59,970
been wrong sometimes but this time, you
know, they've

195
00:10:59,970 --> 00:11:02,680
got a good study and it was careful or
whatever,

196
00:11:02,680 --> 00:11:05,780
and the but means I'm discounting the
objection that

197
00:11:05,780 --> 00:11:08,250
you raised, and I might even discount it
in advance.

198
00:11:10,040 --> 00:11:12,280
Or I might just guard my claim.

199
00:11:12,280 --> 00:11:16,920
I might say well, they might be right in
this case

200
00:11:16,920 --> 00:11:20,970
or they're probably right without claiming
that they definitely are right.

201
00:11:20,970 --> 00:11:21,180
So,

202
00:11:21,180 --> 00:11:23,880
I can assure you by citing some kind of
authority.

203
00:11:23,880 --> 00:11:28,110
I can discount objections and I can guard
my premises by

204
00:11:28,110 --> 00:11:31,920
saying, well, it's probably right, instead
of saying it's definitely right.

205
00:11:31,920 --> 00:11:37,748
And those are three ways of solving the
skeptical regress problem in everyday life

206
00:11:37,748 --> 00:11:42,700
that we're going to look at in much more
detail in the next three lectures.

207
00:11:42,700 --> 00:11:45,240
But the point of this lecture has been
more general.

208
00:11:45,240 --> 00:11:46,690
In order to solve the skeptical

209
00:11:46,690 --> 00:11:48,380
regress problem, you have to find some

210
00:11:48,380 --> 00:11:51,400
assumptions that you and your audience
share.

211
00:11:51,400 --> 00:11:54,770
They might be assumptions about the
premises of your arguments.

212
00:11:54,770 --> 00:11:58,690
They might be assumptions about
authorities that supposedly support

213
00:11:58,690 --> 00:12:03,090
your premises and they accept as
authorities, or whatever.

214
00:12:03,090 --> 00:12:05,000
But there have to be some assumptions that
you share

215
00:12:05,000 --> 00:12:07,690
with your audience in order to get the
argument going.

216
00:12:08,720 --> 00:12:11,380
And that's kind of tricky, because it's
going to depend on the context.

217
00:12:12,480 --> 00:12:14,940
If you're dealing with an audience that
shares a

218
00:12:14,940 --> 00:12:18,780
lot of your assumptions, then argument's
going to be relatively easy.

219
00:12:18,780 --> 00:12:22,410
But if you're dealing with an audience
that doesn't share any

220
00:12:22,410 --> 00:12:26,730
of your assumptions, it's going to be
impossible, and in areas where there's

221
00:12:26,730 --> 00:12:31,430
lots of disagreement, it's going to be
hard to get your arguments going because

222
00:12:31,430 --> 00:12:37,530
your premises are going to be questioned
and denied or rejected by

223
00:12:37,530 --> 00:12:43,340
the people in the audience.
So what these tricks do is they give

224
00:12:43,340 --> 00:12:48,020
you ways to get the argument going, but
they're not going to work in every case.

225
00:12:48,020 --> 00:12:50,750
And we'll have to look at that, as we're
looking at these three

226
00:12:50,750 --> 00:12:52,760
different ways to solve the skeptical

227
00:12:52,760 --> 00:12:54,730
regress problem in the next three
lectures.

