1
00:00:00,025 --> 00:00:04,660
In the last lecture, we learned about

2
00:00:04,660 --> 00:00:07,300
the truth functional connective that we
called conjunction.

3
00:00:07,300 --> 00:00:10,590
The truth functional connective that, in
English, at least,

4
00:00:10,590 --> 00:00:14,350
is sometimes expressed by certain uses of
the word and.

5
00:00:14,350 --> 00:00:18,450
And we learned about the truth-table for
conjunction.

6
00:00:18,450 --> 00:00:20,760
Now, one thing I'd like to do in today's
lecture is

7
00:00:20,760 --> 00:00:25,190
show how we can use the truth-table for
conjunction to show

8
00:00:25,190 --> 00:00:31,120
that certain kinds of inferences that use
conjunction are going to be valid.

9
00:00:31,120 --> 00:00:32,090
Let me give you an example.

10
00:00:32,090 --> 00:00:35,450
Consider an inference that starts off with
two premises.

11
00:00:35,450 --> 00:00:37,098
Doesn't matter what the premises are.

12
00:00:37,098 --> 00:00:41,060
Just any two premises, call them P and Q.
Because it doesn't matter what they are.

13
00:00:41,060 --> 00:00:44,160
And then the conclusion of the inference
is simply going to

14
00:00:44,160 --> 00:00:48,560
be the conjunction of those two premises
whatever they were.

15
00:00:49,560 --> 00:00:50,430
So the inference

16
00:00:50,430 --> 00:00:56,231
can be sketched as follows P, Q,
conclusion, P and Q.

17
00:00:56,231 --> 00:01:00,375
Now, we can call that a conjunction
introduction inference

18
00:01:00,375 --> 00:01:04,510
because the conclusion of the inference
uses a conjunction.

19
00:01:04,510 --> 00:01:09,070
It introduces a conjunction that wasn't
present in either of the two premises of

20
00:01:09,070 --> 00:01:15,620
the inference.
Now, notice If you look at the first line

21
00:01:15,620 --> 00:01:19,862
of the truth table for conjunction you can
see that

22
00:01:19,862 --> 00:01:24,780
any conjunction introduction inference is
going to have to be valid.

23
00:01:24,780 --> 00:01:25,033
Here's how you can see that.

24
00:01:25,033 --> 00:01:29,677
If you look at the first line of the
truth-table for conjunction

25
00:01:29,677 --> 00:01:33,719
that considers the situation which P is
true and Q is true.

26
00:01:33,719 --> 00:01:37,847
And in any such situation, according to
that truth-table,

27
00:01:37,847 --> 00:01:40,771
the conjunction P and Q is going to have
to

28
00:01:40,771 --> 00:01:41,320
be true.

29
00:01:41,320 --> 00:01:45,520
Well now, let's take that point and apply
it to the inference from

30
00:01:45,520 --> 00:01:49,300
the premise P and the premise Q to the
conclusion, P and Q.

31
00:01:50,440 --> 00:01:51,805
What does that first line of the

32
00:01:51,805 --> 00:01:54,310
truth-table for conjunction tell you about
that inference?

33
00:01:54,310 --> 00:01:57,200
Well it tells you that in any situation in
which

34
00:01:57,200 --> 00:02:01,940
the premises of that inference P and Q are
both true.

35
00:02:01,940 --> 00:02:06,380
In any situation in which those premises
are both true, the conclusion P and

36
00:02:06,380 --> 00:02:07,970
Q is going to have to be true.

37
00:02:10,000 --> 00:02:12,830
But that's just what it is for the
inference to be valid.

38
00:02:12,830 --> 00:02:16,310
Recall that for an inference to be valid
is just for it to be such

39
00:02:16,310 --> 00:02:18,610
that there's no possible situation where
the

40
00:02:18,610 --> 00:02:20,680
premises are true and the conclusion is
false.

41
00:02:21,710 --> 00:02:25,720
So, from the first line of the truth-table
for conjunction, you

42
00:02:25,720 --> 00:02:31,110
can see that conjunction introduction
inferences are all going to be valid.

43
00:02:33,870 --> 00:02:36,340
You can also see by looking at the
truth-table for

44
00:02:36,340 --> 00:02:40,180
conjunction that another kind of inference
is always going to be valid.

45
00:02:40,180 --> 00:02:42,140
So consider an inference that starts with
only

46
00:02:42,140 --> 00:02:46,860
one premise, a premise that conjoins two
propositions,

47
00:02:46,860 --> 00:02:48,940
again never mind what the two propositions
are,

48
00:02:48,940 --> 00:02:51,260
they could be anything, call them P and Q.

49
00:02:51,260 --> 00:02:53,340
Because it doesn't matter what they are.

50
00:02:53,340 --> 00:02:55,520
So consider an argument that starts with
that

51
00:02:55,520 --> 00:02:58,930
one premise, P and Q, and that moves to

52
00:02:58,930 --> 00:03:03,820
a conclusion that consists simply of one
of those two,

53
00:03:03,820 --> 00:03:07,380
one of those two propositions that is
conjoined in the premise.

54
00:03:07,380 --> 00:03:09,880
So the conclusion of the argument will be

55
00:03:09,880 --> 00:03:12,570
either the proposition P or the
proposition Q.

56
00:03:14,540 --> 00:03:18,910
Now, an argument like that we can call a
conjunction elimination argument.

57
00:03:18,910 --> 00:03:24,000
Because the conclusion of the argument
eliminates a conjunction that occurs in

58
00:03:24,000 --> 00:03:25,580
the premise of the argument, right?

59
00:03:25,580 --> 00:03:28,970
The premise is a conjunction of two
propositions P and Q.

60
00:03:28,970 --> 00:03:30,930
And the conclusion is simply one of

61
00:03:30,930 --> 00:03:35,290
those two propositions not conjoined to
anything else.

62
00:03:35,290 --> 00:03:38,963
So the conclusion is either the
proposition P or it's the proposition Q.

63
00:03:38,963 --> 00:03:43,290
Now, is that argument going to be valid?

64
00:03:43,290 --> 00:03:45,400
Well if you look at the truth-table for
conjunction,

65
00:03:45,400 --> 00:03:47,240
you'll see that it is going to be valid.

66
00:03:47,240 --> 00:03:49,180
In any possible situation

67
00:03:49,180 --> 00:03:51,170
in which the premise of that argument, P
and

68
00:03:51,170 --> 00:03:56,190
Q, is true, both of the two propositions
that

69
00:03:56,190 --> 00:03:58,640
are conjoined in that premise, both the
proposition P

70
00:03:58,640 --> 00:04:02,750
and the proposition Q, are also going to
be true.

71
00:04:02,750 --> 00:04:04,740
So any situation in which the premise

72
00:04:04,740 --> 00:04:07,320
of a conjunction elimination argument is
true is

73
00:04:07,320 --> 00:04:08,850
going to be a situation in which the

74
00:04:08,850 --> 00:04:11,902
conclusion of that conjunction elimination
argument is true.

75
00:04:11,902 --> 00:04:14,540
Therefore, all conjunction

76
00:04:14,540 --> 00:04:17,510
elimination arguments are valid no matter
what they're about.

77
00:04:17,510 --> 00:04:20,460
And we can see that just by looking at the
truth-table for conjunction.

78
00:04:20,460 --> 00:04:22,460
Now conjunction introduction and
conjunction

79
00:04:22,460 --> 00:04:23,910
elimination arguments are not the

80
00:04:23,910 --> 00:04:26,287
most interesting kinds of arguments there
are, to be sure.

81
00:04:26,287 --> 00:04:34,174
But I just wanted to give a simple example
for

82
00:04:34,174 --> 00:04:39,860
now of how we can use the truth table for
truth functional connective, in this case

83
00:04:39,860 --> 00:04:41,910
the truth functional connective
conjunction.

84
00:04:41,910 --> 00:04:45,350
And we can use the truth table for that
connective to

85
00:04:45,350 --> 00:04:49,590
discover that certain kinds of arguments
are going to be valid.

86
00:04:49,590 --> 00:04:54,847
In the next lecture, we'll show how we can
use other kinds of truth-tables for other

87
00:04:54,847 --> 00:04:57,700
connectives, for other truth functional
connectives, to show

88
00:04:57,700 --> 00:05:00,470
that certain other kinds of arguments are
valid.

89
00:05:00,470 --> 00:05:01,180
See you next time.

