1
00:00:03,470 --> 00:00:07,685
In the last lecture I said that
quantifiers modify categories,

2
00:00:07,685 --> 00:00:10,420
and that's how quantifiers and categories
are related to each other.

3
00:00:10,420 --> 00:00:16,620
Categories are kinds of things, and
quantifiers modify categories.

4
00:00:16,620 --> 00:00:18,610
They can modify them in different ways.

5
00:00:18,610 --> 00:00:20,560
There are different kinds of quantifiers.

6
00:00:20,560 --> 00:00:22,670
In today's lecture we'll talk about some
of the

7
00:00:22,670 --> 00:00:26,060
different kinds of quantifiers and how
they modify categories.

8
00:00:26,060 --> 00:00:29,150
We'll also talk about how the
representation

9
00:00:29,150 --> 00:00:32,080
of these different quantifiers differ,
differs both

10
00:00:32,080 --> 00:00:35,130
verbally and visually using the Venn
Diagram.

11
00:00:37,200 --> 00:00:40,850
Okay so, what are the different kinds of
quantifiers?

12
00:00:40,850 --> 00:00:43,120
Well here's some of the different kinds of
quantifiers.

13
00:00:44,290 --> 00:00:46,550
There's the quantifier all.

14
00:00:46,550 --> 00:00:51,890
And we can apply this to categories.
Notice by the way in this slide,

15
00:00:51,890 --> 00:00:56,950
I'm using an uppercase f and an uppercase
g to represent

16
00:00:58,440 --> 00:01:02,580
any category at all.
So in a statement of the form all

17
00:01:02,580 --> 00:01:08,050
Fs are Gs, that that's the claim that all
things

18
00:01:08,050 --> 00:01:13,120
that fall into one category, never mind
exactly what that category is, let's

19
00:01:13,120 --> 00:01:17,740
just call it F, also fall into a second
category, again,

20
00:01:17,740 --> 00:01:20,630
never mind what that category is, we can
just call it G.

21
00:01:22,330 --> 00:01:24,230
There are lots of examples of this kind of

22
00:01:24,230 --> 00:01:27,660
claim when you say all things of one
category

23
00:01:27,660 --> 00:01:33,830
also fall into a second category.
for instance, all dogs are mammals,

24
00:01:33,830 --> 00:01:40,690
all squares are rectangles, all Coursera
students are human, and so on.

25
00:01:40,690 --> 00:01:45,910
In all of these cases, you're making a
claim of the form that all things

26
00:01:45,910 --> 00:01:51,220
that fall into one category, the F
category, also fall into the G category.

27
00:01:51,220 --> 00:01:52,660
So all is one kind of

28
00:01:52,660 --> 00:01:56,170
quantifier, and any statement of the form
all Fs

29
00:01:56,170 --> 00:01:59,410
or Gs, we're going to call a statement of
type A.

30
00:01:59,410 --> 00:02:01,030
Proposition of type A.

31
00:02:01,030 --> 00:02:03,490
Then there's the quantifier no.

32
00:02:04,910 --> 00:02:11,530
Which we can use in a statement of the
form, no Fs are Gs, no things of one

33
00:02:11,530 --> 00:02:17,895
category also fall into a second category.
So, for instance, you might say,

34
00:02:17,895 --> 00:02:24,060
No humans are reptile.
No Coursera students are trees.

35
00:02:24,060 --> 00:02:27,190
No Democracies are at war.

36
00:02:29,262 --> 00:02:34,215
So they're examples of statements of the
form that we're going to call E.

37
00:02:34,215 --> 00:02:37,640
An E statement is a statement to the
effect that no

38
00:02:37,640 --> 00:02:42,540
things that fall into one category also
fall into a second category.

39
00:02:45,070 --> 00:02:47,630
Then there's the quantifier some.

40
00:02:47,630 --> 00:02:49,640
Now, the quantifier some can be used

41
00:02:49,640 --> 00:02:51,910
to make statements of two very different
kinds.

42
00:02:51,910 --> 00:02:57,320
You could say that some things that fall
into one category also fall into a second

43
00:02:57,320 --> 00:03:04,270
category.
So if you'd said, some humans are female,

44
00:03:04,270 --> 00:03:10,660
some Coursera students are American, some
Americans

45
00:03:10,660 --> 00:03:13,870
speak Spanish, right?

46
00:03:13,870 --> 00:03:17,400
Those are statements to the effect that
some things falling into

47
00:03:17,400 --> 00:03:22,190
one category, the Fs, also fall into a
second category, the Gs.

48
00:03:22,190 --> 00:03:23,460
Some Fs are Gs.

49
00:03:25,105 --> 00:03:28,800
Propositions of that form we'll call I
propositions.

50
00:03:30,050 --> 00:03:31,830
Finally we could also use the quantifier

51
00:03:31,830 --> 00:03:33,630
some to make a different kind of
statement.

52
00:03:33,630 --> 00:03:35,750
A statement to the effect that some things
that fallen

53
00:03:35,750 --> 00:03:38,904
into one category don't fall into a second
category.

54
00:03:38,904 --> 00:03:39,690
Right?

55
00:03:39,690 --> 00:03:47,030
Some humans are not female.
Some Coursera students are not American.

56
00:03:47,030 --> 00:03:50,130
Some Americans are not Spanish speakers.

57
00:03:51,600 --> 00:03:56,980
Statements of that form, we'll call O
statements, or O propositions.

58
00:03:56,980 --> 00:03:57,200
Right?

59
00:03:57,200 --> 00:04:00,790
They say that some things that fall into
one category don't

60
00:04:00,790 --> 00:04:03,780
fall into a second category.
Some Fs are not Gs.

61
00:04:05,720 --> 00:04:07,620
So those are the four kinds of statements
we're

62
00:04:07,620 --> 00:04:11,610
going to focus on in this week on
categorical logic.

63
00:04:11,610 --> 00:04:13,310
We'll consider other kinds of statements
as

64
00:04:13,310 --> 00:04:16,190
well, and we'll mention some other kinds
of

65
00:04:16,190 --> 00:04:19,509
quantifiers but those are the ones that
are going to be of primary interest to us.

66
00:04:21,360 --> 00:04:25,900
Now I've talked about these different
quantifiers and how they can be used

67
00:04:25,900 --> 00:04:28,050
to make statements of very different
kinds.

68
00:04:29,400 --> 00:04:33,870
But now we can visually represent the
differences in

69
00:04:33,870 --> 00:04:38,730
the information provided in these four
different kinds of statements.

70
00:04:38,730 --> 00:04:43,390
So consider, how would we visually
represent the, a

71
00:04:43,390 --> 00:04:46,100
statement of the form, that all Fs are Gs?

72
00:04:46,100 --> 00:04:51,140
All things that fall into one category
also fall into a second category.

73
00:04:51,140 --> 00:04:55,180
Well if you want to say all Fs are Gs,
let's see how would you do that?

74
00:04:55,180 --> 00:05:01,130
You'd be saying that all of the Fs that
there are fall into the G circle.

75
00:05:01,130 --> 00:05:03,250
But if all the Fs that there are fall into
the

76
00:05:03,250 --> 00:05:10,400
G circle, then there can't be any Fs
outside the G circle.

77
00:05:10,400 --> 00:05:10,790
Right?

78
00:05:10,790 --> 00:05:14,375
So the Venn Diagram for all Fs are Gs
would look like that.

79
00:05:14,375 --> 00:05:15,630
Right?

80
00:05:15,630 --> 00:05:16,860
You'd be saying,

81
00:05:16,860 --> 00:05:21,899
whatever Fs there are, they've got to be
in here.

82
00:05:24,440 --> 00:05:27,330
Right?
There can't be any outside there.

83
00:05:27,330 --> 00:05:31,200
So that would be the Venn diagram for all
Fs are Gs.

84
00:05:31,200 --> 00:05:33,520
Or, a proposition of the A form.

85
00:05:38,450 --> 00:05:40,970
Next, there are propositions of the E
form.

86
00:05:40,970 --> 00:05:43,830
Now, how would we represent those?

87
00:05:43,830 --> 00:05:44,330
Well, let's see.

88
00:05:46,480 --> 00:05:48,970
Here's the Venn diagram for no Fs and Gs.

89
00:05:48,970 --> 00:05:52,290
When you say no Fs are Gs, you're saying
if there

90
00:05:52,290 --> 00:05:55,910
are any Fs at all, they can't be inside
the G circle.

91
00:05:55,910 --> 00:06:01,330
So you've got to shade out that portion of
the F circle that's inside the G circle.

92
00:06:01,330 --> 00:06:01,530
Right?

93
00:06:01,530 --> 00:06:04,110
The shading implies that there's nothing
there.

94
00:06:05,600 --> 00:06:05,880
Right?

95
00:06:05,880 --> 00:06:11,900
So there are no Fs inside the G circle.
And that's how you represent no Fs are Gs.

96
00:06:14,440 --> 00:06:18,720
Next, there are statemenst of the I form,
some Fs are Gs.

97
00:06:18,720 --> 00:06:20,129
How would you represent those?

98
00:06:22,005 --> 00:06:26,530
Some Fs are Gs, you use an, an X mark

99
00:06:26,530 --> 00:06:28,990
to represent that there is a thing, so you
use

100
00:06:28,990 --> 00:06:31,630
an X to represent there is a thing that's
in

101
00:06:31,630 --> 00:06:35,490
the F circle and that's also in the G
circle.

102
00:06:35,490 --> 00:06:38,849
Right, so there's a thing right there.
It's in the F circle, so it is an F.

103
00:06:40,070 --> 00:06:44,360
But it's also a G.
So some Fs are Gs.

104
00:06:46,850 --> 00:06:48,740
That's how you'd represent, how'd you
visually

105
00:06:48,740 --> 00:06:50,595
represent a statement of the I form.

106
00:06:50,595 --> 00:06:53,940
Now finally, how about the statement of
the O form?

107
00:06:53,940 --> 00:06:57,000
Some Fs are not Gs.
How would you visually represent that?

108
00:06:58,450 --> 00:07:05,270
Well, you'd have to make an X to indicate
that there is an F, right?

109
00:07:05,270 --> 00:07:08,150
There is a thing that is F, but it's not
G.

110
00:07:08,150 --> 00:07:11,730
If it's not G, then it's got to be outside
the G circle.

111
00:07:11,730 --> 00:07:15,600
So it's inside the F circle, but outside
the G circle.

112
00:07:18,130 --> 00:07:18,340
Right?

113
00:07:18,340 --> 00:07:22,213
And that's how you'd indicate visually
some Fs are not Gs.

114
00:07:22,213 --> 00:07:27,049
Now, I'd

115
00:07:27,049 --> 00:07:31,885
like you

116
00:07:31,885 --> 00:07:37,411
to notice

117
00:07:37,411 --> 00:07:43,624
something.

118
00:07:43,624 --> 00:07:49,750
Are negations of proposition of the,
propositions of

119
00:07:49,750 --> 00:07:52,420
the O form some Fs are not Gs.

120
00:07:52,420 --> 00:07:52,680
Right?

121
00:07:52,680 --> 00:07:58,500
If all Fs are Gs, then it's not going to
be true that some Fs are not Gs.

122
00:07:58,500 --> 00:08:02,530
And if some Fs are not Gs, then it's not
going to be true that all Fs are Gs.

123
00:08:04,320 --> 00:08:08,480
All F's are G's, in other words, is
going to be true when,

124
00:08:08,480 --> 00:08:12,540
and only when, some Fs are not Gs is not
true.

125
00:08:14,120 --> 00:08:16,890
And similarly, propositions of the E form
are going to

126
00:08:16,890 --> 00:08:20,050
be negations of propositions of the I
form, right?

127
00:08:20,050 --> 00:08:22,700
When is it going to be true that no Fs are
Gs?

128
00:08:22,700 --> 00:08:24,970
It's going to be true that no Fs are Gs
when,

129
00:08:24,970 --> 00:08:28,580
and only when, it's not true that some Fs
are Gs.

130
00:08:28,580 --> 00:08:28,720
Right?

131
00:08:28,720 --> 00:08:31,420
If some Fs are Gs, then it's not going to
be true that no Fs are Gs.

132
00:08:31,420 --> 00:08:33,520
And if no Fs are Gs, then it's not
going to be

133
00:08:33,520 --> 00:08:34,470
true that some Fs are Gs.

134
00:08:36,390 --> 00:08:40,870
So, propositions of the A and O form are
negations of each other.

135
00:08:40,870 --> 00:08:44,620
And propositions of the E and I form are
negations of each other.

136
00:08:44,620 --> 00:08:47,640
And those relationships are represented
visually on

137
00:08:47,640 --> 00:08:49,390
the Venn diagrams that we just drew.

138
00:08:50,810 --> 00:08:51,420
Right?

139
00:08:51,420 --> 00:08:55,690
Consider again, the Venn Diagram for
propositions of the A form.

140
00:08:56,780 --> 00:08:58,540
Propositions of the A form, the Venn

141
00:08:58,540 --> 00:09:03,970
diagram is going to have shading in here.
But in propositions

142
00:09:03,970 --> 00:09:09,270
of the O form, the Venn diagram is
going to have an X in here

143
00:09:09,270 --> 00:09:09,640
[SOUND],

144
00:09:09,640 --> 00:09:13,129
in precisely the place where propositions
of the A form had shading.

145
00:09:15,020 --> 00:09:18,300
Propositions of the E form are going to
have shading in here

146
00:09:18,300 --> 00:09:18,300
[SOUND]

147
00:09:18,300 --> 00:09:22,670
, whereas propositions of the I form are
going to have an x in here.

148
00:09:22,670 --> 00:09:23,170
[NOISE]

149
00:09:24,790 --> 00:09:28,615
So, the way that we represent negation
using Venn

150
00:09:28,615 --> 00:09:31,630
Diagrams, is one proposition is going to
be the negation

151
00:09:31,630 --> 00:09:35,170
of another just in case the first has
shading

152
00:09:35,170 --> 00:09:37,780
wherever the second has an X, or vice
versa.

153
00:09:39,020 --> 00:09:39,290
Right?

154
00:09:39,290 --> 00:09:41,720
If one proposition has an X wherever the

155
00:09:41,720 --> 00:09:44,680
other has shading, or it has shading
wherever

156
00:09:44,680 --> 00:09:46,500
the other has an X, then the two

157
00:09:46,500 --> 00:09:49,480
propositions are going to be the negations
of each other.

158
00:09:49,480 --> 00:09:52,400
That's how the represent, that's how the
relation

159
00:09:52,400 --> 00:09:56,130
of negation is visually represented in
Venn Diagrams.

