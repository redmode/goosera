1
00:00:03,980 --> 00:00:05,510
Today, we're going to talk about the

2
00:00:05,510 --> 00:00:08,663
truth functional connective that we'll
call disjunction.

3
00:00:08,663 --> 00:00:12,790
Now disjunction is a true functional
connective that in English,

4
00:00:12,790 --> 00:00:17,100
is usually expressed by means of the word
or, o-r.

5
00:00:17,100 --> 00:00:19,200
But there are a couple different ways of
using

6
00:00:19,200 --> 00:00:22,295
that word, or, let me give you some
examples.

7
00:00:22,295 --> 00:00:25,690
suppose you ask me who won the game last
night,

8
00:00:25,690 --> 00:00:29,300
and I say, well, either Manchester won it,
or Barcelona

9
00:00:29,300 --> 00:00:30,181
won it.

10
00:00:30,181 --> 00:00:36,240
There, I'm clearly trying to indicate that
while Manchester may have won

11
00:00:36,240 --> 00:00:40,500
it and Barcelona may have won it, they
didn't both win the game.

12
00:00:40,500 --> 00:00:44,885
If Manchester was playing Barcelona last
night, then they couldn't both have won.

13
00:00:44,885 --> 00:00:47,350
All right, soccer doesn't work like that.

14
00:00:48,412 --> 00:00:50,380
Either one team wins or the other team
wins.

15
00:00:50,380 --> 00:00:51,620
But they can't both win.

16
00:00:52,680 --> 00:00:53,940
So if I say,

17
00:00:53,940 --> 00:00:56,880
either Manchester won it or Barcelona won
it, what

18
00:00:56,880 --> 00:01:01,210
I mean is that either one of two
possibilities occurred.

19
00:01:01,210 --> 00:01:05,360
Either Manchester won it or Barcelona won
it, but they couldn't both have occurred.

20
00:01:07,010 --> 00:01:09,440
But sometimes, when I use the word or,

21
00:01:09,440 --> 00:01:12,270
I don't mean to con-, convey anything like
that.

22
00:01:12,270 --> 00:01:17,525
For instance, suppose I say to you, this
is breakfast or lunch.

23
00:01:17,525 --> 00:01:20,660
Now,

24
00:01:20,660 --> 00:01:23,050
I'm not suggesting, when I say this is
breakfast

25
00:01:23,050 --> 00:01:25,256
or lunch, I'm not suggesting that it can't
be both.

26
00:01:25,256 --> 00:01:29,660
It could be breakfast, it could be lunch,
or it could be

27
00:01:29,660 --> 00:01:34,190
both, and when I say this is breakfast or
lunch, I mean.

28
00:01:34,190 --> 00:01:35,870
It could be one.
It could be the other.

29
00:01:35,870 --> 00:01:40,910
It could be both.
So there, in that second

30
00:01:40,910 --> 00:01:46,440
usage, I'll say or is inclusive.
It includes

31
00:01:46,440 --> 00:01:52,290
both of the options and the possibility of
their both being true.

32
00:01:52,290 --> 00:01:56,240
In the first usage, where I said either
Manchester

33
00:01:56,240 --> 00:01:59,620
won or Barcelona won, I'll say or is
exclusive.

34
00:01:59,620 --> 00:02:01,850
In other words, either one is true, or the

35
00:02:01,850 --> 00:02:03,850
other is true, but they can't both be
true.

36
00:02:05,320 --> 00:02:11,880
So or can be used exclusively, to mean
that either one of two options is

37
00:02:11,880 --> 00:02:13,610
true, but they can't both be true.

38
00:02:13,610 --> 00:02:17,280
Or, or could be used inclusively, to mean
one of two options could

39
00:02:20,520 --> 00:02:22,430
be true, or they could both be true.

40
00:02:24,310 --> 00:02:27,010
Now, the true functional connective
disjunction is expressed in

41
00:02:29,410 --> 00:02:32,805
English by the use of the inclusive or.

42
00:02:32,805 --> 00:02:36,330
The or, that leaves open the possibility
that

43
00:02:36,330 --> 00:02:38,350
both of the two options could be true.

44
00:02:39,880 --> 00:02:42,980
So, what's the truth table for the

45
00:02:42,980 --> 00:02:45,805
truth functional connective disjunction
going to look like?

46
00:02:45,805 --> 00:02:50,720
Well, you can start with two
possibilities, two propositions

47
00:02:50,720 --> 00:02:54,250
that are connected by the truth functional
connective disjunction.

48
00:02:54,250 --> 00:02:56,290
Doesn't matter what the propositions are.

49
00:02:56,290 --> 00:03:00,736
Just call them P and Q because it doesn't
matter what they are so.

50
00:03:00,736 --> 00:03:07,370
there's P, there's Q, and then there's the
disjunction of P and Q, which we'll

51
00:03:07,370 --> 00:03:15,420
symbolize as follows: P disjunction, which
looks like a V, Q.

52
00:03:15,420 --> 00:03:19,279
Now, when is P disjunction Q going to be
true?

53
00:03:21,590 --> 00:03:26,100
Remember disjunction is expressed by the
inclusive or, so, it's going to be true

54
00:03:26,100 --> 00:03:30,020
whenever P is true, and it's also going to
be true whenever Q is true.

55
00:03:30,020 --> 00:03:33,980
And of course, since it's expressed by the
inclusive or,

56
00:03:33,980 --> 00:03:36,375
it's going to be true whenever P and Q are
both true.

57
00:03:36,375 --> 00:03:41,830
So, the truth-table for disjunction is
going to look like this.

58
00:03:41,830 --> 00:03:47,210
Right, the first three lines, the lines
that consider the possibility

59
00:03:47,210 --> 00:03:52,940
of p and q's both being true, or p's being
true and q's being false, or p's being

60
00:03:52,940 --> 00:03:55,460
false and q's being true, and all three of

61
00:03:55,460 --> 00:03:58,710
those lines, the disjunction p or q will
be true.

62
00:04:01,360 --> 00:04:07,290
The only scenario in which, P or Q, is
false, is the

63
00:04:07,290 --> 00:04:12,630
scenario in which P and Q are both false.
In which neither P nor Q is true.

64
00:04:13,970 --> 00:04:16,410
Now that we've learned the truth table for

65
00:04:16,410 --> 00:04:20,040
the truth functional connective,
disjunction, we can use

66
00:04:20,040 --> 00:04:22,290
that truth table to figure out when
certain

67
00:04:22,290 --> 00:04:25,208
arguments that use disjunction, are
going to be valid.

68
00:04:25,208 --> 00:04:27,244
For example,

69
00:04:27,244 --> 00:04:29,890
consider this simple argument.

70
00:04:29,890 --> 00:04:33,030
Premise one, I'm going to tickle you with
my right hand.

71
00:04:34,300 --> 00:04:37,000
Premise two, I'm going to tickle you with
my left hand.

72
00:04:38,610 --> 00:04:40,150
Conclusion.

73
00:04:40,150 --> 00:04:42,858
I'm going to tickle you with either my
right or my left hand.

74
00:04:42,858 --> 00:04:47,990
Is that argument valid?

75
00:04:47,990 --> 00:04:49,380
Well, it obviously is valid.

76
00:04:49,380 --> 00:04:52,280
But you can use the truth table for
disjunction to explain

77
00:04:52,280 --> 00:04:52,950
why it's valid.

78
00:04:52,950 --> 00:04:56,060
Look at the truth table for disjunction
again.

79
00:04:57,190 --> 00:05:00,530
In this argument we have premise one being
the

80
00:05:00,530 --> 00:05:03,050
proposition, I'm going to tickle you with
my right hand.

81
00:05:03,050 --> 00:05:06,850
Premise two being the proposition, I'm
going to tickle you with my left hand.

82
00:05:06,850 --> 00:05:10,390
And the conclusion that's the disjunction
of those two propositions.

83
00:05:11,760 --> 00:05:14,630
Well, what can you tell from the truth
table for disjunction?

84
00:05:14,630 --> 00:05:18,300
What you can tell is that, in a situation
in which

85
00:05:18,300 --> 00:05:22,670
P is true and Q is true, in which both of
two propositions are

86
00:05:22,670 --> 00:05:25,120
going to be true, the disjunction of those

87
00:05:25,120 --> 00:05:28,200
two propositions is also going to to be
true.

88
00:05:28,200 --> 00:05:32,250
You see, that's what you can read off from
the first line of the truth table.

89
00:05:32,250 --> 00:05:33,820
In a situation in which each of two

90
00:05:33,820 --> 00:05:37,840
propositions is true, their disjunction
has to be true.

91
00:05:37,840 --> 00:05:41,010
So, in the argument that I just gave you,
which has

92
00:05:41,010 --> 00:05:43,610
as premise one, I'm going to tickle you
with my right hand, and

93
00:05:43,610 --> 00:05:45,650
as premise two, I'm going to tickle you
with my left hand.

94
00:05:47,470 --> 00:05:50,750
That argument has to be valid because the
conclusion of

95
00:05:50,750 --> 00:05:54,180
that argument is simply the disjunction of
the two premises.

96
00:05:54,180 --> 00:05:57,500
So there's no possible way for the
premises of

97
00:05:57,500 --> 00:06:00,150
that argument to be true while the
conclusion is false.

98
00:06:00,150 --> 00:06:03,470
If the premises of that argument are true,
the conclusion has to be true.

99
00:06:03,470 --> 00:06:09,389
We could think of that argument as a kind
of disjunction introduction

100
00:06:09,389 --> 00:06:12,270
argument, because just as in the case of a

101
00:06:12,270 --> 00:06:14,200
conjunction introduction argument, where
the

102
00:06:14,200 --> 00:06:16,790
conclusion introduces a conjunction that

103
00:06:16,790 --> 00:06:19,730
wasn't there in the premises, in this
argument the

104
00:06:19,730 --> 00:06:23,430
conclusion introduces a disjunction that
wasn't there in the premises.

105
00:06:23,430 --> 00:06:24,020
Right?

106
00:06:24,020 --> 00:06:26,640
There where two premises, I'm going to
tickle you with my right

107
00:06:26,640 --> 00:06:29,110
hand and I'm going to tickle you with my
left hand.

108
00:06:29,110 --> 00:06:32,810
The conclusion introduces the disjunction
of those two premises.

109
00:06:32,810 --> 00:06:34,850
The conclusion just is the disjunction

110
00:06:34,850 --> 00:06:36,820
of those two premises.

111
00:06:36,820 --> 00:06:41,425
But notice, even though that disjunction
introduction argument is valid.

112
00:06:41,425 --> 00:06:45,180
There're even simpler disjunction
introduction arguments that are valid.

113
00:06:45,180 --> 00:06:47,810
Consider for instance, the argument that
starts

114
00:06:47,810 --> 00:06:50,760
with just one premise, could be anything,
call

115
00:06:50,760 --> 00:06:57,100
it p, and that draws us a conclusion, the
disjunction of p with anything else.

116
00:06:57,100 --> 00:06:59,850
So let's say P is the premise, I'm
going to tickle you with

117
00:06:59,850 --> 00:07:04,405
my right hand, and then the conclusion is
that it's just the disjunction of

118
00:07:04,405 --> 00:07:08,750
that premise, I'm going to tickle you with
my right hand, with any other proposition.

119
00:07:08,750 --> 00:07:10,990
Like say, I'm going to tickle you with my
left hand.

120
00:07:12,960 --> 00:07:15,760
That argument is going to have to be
valid.

121
00:07:15,760 --> 00:07:20,050
And you can read that off the truth table
for disjunction, because you can see that

122
00:07:20,050 --> 00:07:22,262
in any situation in which one of the

123
00:07:22,262 --> 00:07:24,942
disjuncts of a disjunction is true, then
the

124
00:07:24,942 --> 00:07:27,126
disjunction is going to have to be true.

125
00:07:27,126 --> 00:07:31,760
So, any disjunction introduction argument
that starts

126
00:07:31,760 --> 00:07:34,794
with just one premise, and that concludes
the

127
00:07:34,794 --> 00:07:37,800
disjunction of that premise with anything
else, any

128
00:07:37,800 --> 00:07:39,660
such argument is going to have to be
valid.

129
00:07:39,660 --> 00:07:41,410
There's no possible way for the premise of
that

130
00:07:41,410 --> 00:07:43,988
argument to be true, while the conclusion
is false.

131
00:07:43,988 --> 00:07:48,764
So, all disjuntion arguments are valid.

132
00:07:48,764 --> 00:07:49,370
But what about disjunction elimination
arguments?

133
00:07:49,370 --> 00:07:50,410
Well, remember conjunction

134
00:07:50,410 --> 00:07:58,360
elimination arguments are valid, because
if you start off with a

135
00:07:58,360 --> 00:08:04,320
premise that states the conjunction of two
propositions, then the conclusion that is

136
00:08:04,320 --> 00:08:06,390
either one of those two conjoined

137
00:08:06,390 --> 00:08:09,150
propositions either one of those
propositions,

138
00:08:09,150 --> 00:08:12,540
the conclusion is going to have to be true
whenever that conjunction is true.

139
00:08:12,540 --> 00:08:15,435
You saw that from the truth table for
conjunction.

140
00:08:15,435 --> 00:08:21,090
But what about with disjunction?
Does it work that way with disjunction?

141
00:08:21,090 --> 00:08:24,340
Well, consider an argument that starts
from the premise, I'm

142
00:08:24,340 --> 00:08:26,990
going to tickle you with either my right
or my left hand.

143
00:08:28,740 --> 00:08:31,060
And then it draws the conclusion that

144
00:08:31,060 --> 00:08:33,770
is simply one of those two disjoined
propositions.

145
00:08:33,770 --> 00:08:37,580
Let's say it draws the conclusion I'm
going to tickle you with my right hand.

146
00:08:37,580 --> 00:08:38,610
Is that argument valid?

147
00:08:39,720 --> 00:08:41,290
No, it's not.

148
00:08:41,290 --> 00:08:45,640
Because there's a possible situation in
which the premise, I'm going to

149
00:08:45,640 --> 00:08:49,010
tickle you with either my right hand or
left hand, is true.

150
00:08:49,010 --> 00:08:52,470
While the conclusion, I'm going to tickle
you with my right hand, is false.

151
00:08:52,470 --> 00:08:54,820
Namely, the situation in which I'm
going to tickle you with my left hand.

152
00:08:54,820 --> 00:09:00,640
In that situation, the premise would be
true, but the conclusion would be false.

153
00:09:00,640 --> 00:09:07,520
So, that disjunction elimination argument
is not valid.

154
00:09:07,520 --> 00:09:11,670
And you can also see that by looking at
the truth table, for disjunction.

155
00:09:11,670 --> 00:09:14,920
If you look at the truth-table for
disjunction

156
00:09:14,920 --> 00:09:17,780
you'll see that there are three possible
scenarios in

157
00:09:17,780 --> 00:09:20,820
which the premise, I'm going to tickle you
with either

158
00:09:20,820 --> 00:09:23,110
my right hand or my left hand, is true.

159
00:09:23,110 --> 00:09:26,985
The three possible scenarios, or the three
scenarios in which both of the

160
00:09:26,985 --> 00:09:29,110
disjunctions are true, the first
disjunction

161
00:09:29,110 --> 00:09:30,390
is true, and the second one false.

162
00:09:30,390 --> 00:09:33,190
Or the first disjunction is false and the
second one is true.

163
00:09:35,630 --> 00:09:39,670
Well, there are three possible scenarios
in which that disjunction is true.

164
00:09:39,670 --> 00:09:43,100
When I say I'm not going to tickle you
with my right hand, I'm ruling out two of

165
00:09:43,100 --> 00:09:45,760
those scenarios, namely the two scenarios
in which it's

166
00:09:45,760 --> 00:09:48,280
true that I tickle you with my right hand.

167
00:09:48,280 --> 00:09:53,060
But that still leaves open a scenario, in
which, it's true that

168
00:09:53,060 --> 00:09:55,940
I'm going to tickle you with either my
right hand or my left hand.

169
00:09:55,940 --> 00:10:00,660
But for the disjunctional elimination
argument to be valid, there'd have to be

170
00:10:00,660 --> 00:10:04,950
the case that in every scenario in which
the disjunction, I'm going to

171
00:10:04,950 --> 00:10:08,280
tickle you with either my right or my left
hand, is true.

172
00:10:08,280 --> 00:10:10,715
Each of those disjuncts is true.

173
00:10:10,715 --> 00:10:14,000
But that's not the case, and you can see

174
00:10:14,000 --> 00:10:17,100
that just by looking at the truth table
for disjunction.

175
00:10:17,100 --> 00:10:20,310
So the truth table for disjunction shows
you why it is

176
00:10:20,310 --> 00:10:22,320
that disjunction introduction arguments
are all

177
00:10:22,320 --> 00:10:25,780
valid, but simple disjunction eliminations
arguments

178
00:10:25,780 --> 00:10:29,570
that start with a premise, that's just the
disjunction of two propositions and

179
00:10:29,570 --> 00:10:34,230
end with a conclusion that is simply one
of those disjoined propositions by itself.

180
00:10:34,230 --> 00:10:38,212
Those arguments are not valid, you can see
that by the truth table from disjunction.

