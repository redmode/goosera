1
00:00:03,700 --> 00:00:05,881
Today's lecture is about conditionals.

2
00:00:05,881 --> 00:00:09,000
Conditionals are a kind of truth
functional connective, that

3
00:00:09,000 --> 00:00:11,910
we haven't discussed yet, but today we're
going to

4
00:00:11,910 --> 00:00:15,110
introduce this new kind of truth
functional connective, discuss

5
00:00:15,110 --> 00:00:18,350
the truth table for it, and say why it's
important.

6
00:00:18,350 --> 00:00:22,020
It's especially important in understanding
the rules by

7
00:00:22,020 --> 00:00:25,950
which to assess the validity of deductive
arguments.

8
00:00:25,950 --> 00:00:28,910
Now in order to introduce this new truth
functional

9
00:00:28,910 --> 00:00:32,150
connective, the conditional, I want to
start by telling you a story.

10
00:00:33,920 --> 00:00:37,300
Imagine that Walter's been breaking into
my office and stealing my stuff.

11
00:00:38,440 --> 00:00:40,760
So, each day I come into my office and I

12
00:00:40,760 --> 00:00:42,687
find that more and more of my stuff is
gone.

13
00:00:42,687 --> 00:00:46,070
You know, one day he steals my
electronics,

14
00:00:46,070 --> 00:00:48,420
and, another day he steals my coffee cup,

15
00:00:48,420 --> 00:00:51,250
and the another day he steals my clothing,

16
00:00:51,250 --> 00:00:54,520
and then yet another day he steals my
glasses.

17
00:00:54,520 --> 00:00:57,380
And, before long, there's very little
stuff in my office.

18
00:00:57,380 --> 00:00:59,830
Now, as this happens, I start to get

19
00:00:59,830 --> 00:01:02,720
suspicious, and I decide to have Walter
followed

20
00:01:02,720 --> 00:01:06,560
by a private investigator, just to figure
out

21
00:01:06,560 --> 00:01:10,260
exactly whether or not Walter is stealing
my stuff.

22
00:01:10,260 --> 00:01:13,210
So I have the private investigator follow
Walter everywhere he goes, right?

23
00:01:13,210 --> 00:01:15,350
He follows him to his house at night.

24
00:01:15,350 --> 00:01:18,630
Then he follows him to the bar early in
the morning.

25
00:01:18,630 --> 00:01:19,730
He follows him to the golf

26
00:01:19,730 --> 00:01:20,930
course later in the morning.

27
00:01:20,930 --> 00:01:23,950
Then he follows him to the water polo
tournament in the afternoon.

28
00:01:25,000 --> 00:01:26,970
follows him everywhere he goes.

29
00:01:26,970 --> 00:01:30,140
Now, suppose you ask me, is the private

30
00:01:30,140 --> 00:01:33,180
investigator having lunch at that Cuban
restaurant, New Havana?

31
00:01:34,710 --> 00:01:37,800
Now, I might say to you, well, if Walter
is

32
00:01:37,800 --> 00:01:41,200
having lunch there, then the private
investigator is having lunch there.

33
00:01:42,690 --> 00:01:44,200
Okay, now notice what I just said.

34
00:01:44,200 --> 00:01:44,600
I said,

35
00:01:44,600 --> 00:01:47,030
if Walter is having lunch there, then

36
00:01:47,030 --> 00:01:49,110
the private investigator is having lunch
there.

37
00:01:51,490 --> 00:01:57,780
That phrase, if, then, can be used to
connect two propositions.

38
00:01:57,780 --> 00:02:01,140
The proposition Walter is having lunch at
New Havana, with

39
00:02:01,140 --> 00:02:05,120
the proposition, the private investigator
is having lunch in New Havana.

40
00:02:05,120 --> 00:02:11,280
So, that phrase, if then, works as a
propositional connective.

41
00:02:11,280 --> 00:02:14,020
It connects two propositions to make a
larger proposition.

42
00:02:15,770 --> 00:02:16,500
But does it work

43
00:02:16,500 --> 00:02:21,990
as a truth functional connective?
Now, I'd like to argue that it does.

44
00:02:21,990 --> 00:02:26,970
Okay, so how are we going to argue that if
then is

45
00:02:26,970 --> 00:02:31,128
not just a propositional connective, but
is also a truth functional connective?

46
00:02:31,128 --> 00:02:36,410
Well, I'd like to begin by

47
00:02:36,410 --> 00:02:42,120
considering the following truth functional
construction.

48
00:02:42,120 --> 00:02:48,184
Consider the negation of the conjunction

49
00:02:48,184 --> 00:02:53,370
of P and the negation of Q, right, I've
written that out here.

50
00:02:53,370 --> 00:02:57,950
The negation of the conjunction of P and
the negation of Q.

51
00:02:59,700 --> 00:03:04,500
Now suppose I say that whole proposition
is true.

52
00:03:07,110 --> 00:03:09,900
Well, if I say that whole proposition is
true,

53
00:03:11,120 --> 00:03:14,425
then what I'm saying, is just that, the
negation of

54
00:03:14,425 --> 00:03:19,120
that proposition, in other words, just the
conjunction of P

55
00:03:19,120 --> 00:03:23,230
and the negation of Q, that that
proposition, is false.

56
00:03:23,230 --> 00:03:23,730
But

57
00:03:26,020 --> 00:03:30,668
what does it mean when I say that the
conjunction of P and the negation of Q, is

58
00:03:30,668 --> 00:03:37,346
false.
Well, what I'm saying there

59
00:03:37,346 --> 00:03:44,365
is, that if P is true, then the negation
of Q has to be false.

60
00:03:44,365 --> 00:03:45,834
Right?

61
00:03:45,834 --> 00:03:51,770
Because if the conjunction of P and the
negation of Q is false, then you

62
00:03:51,770 --> 00:03:55,040
can't have both of those conjuncts being
true.

63
00:03:55,040 --> 00:03:57,090
At least one of them has to be false.

64
00:03:57,090 --> 00:04:01,989
So if P is true, than the other conjunct,
the negation of Q has to be false.

65
00:04:05,190 --> 00:04:08,780
So you're saying there that is P is true
than the negation of Q is false.

66
00:04:11,430 --> 00:04:16,150
But remember for the negation of Q to be
false is just the same thing as for Q

67
00:04:16,150 --> 00:04:19,320
to be true, because the negation of Q has

68
00:04:19,320 --> 00:04:21,592
just the opposite truth value of whatever
Q has.

69
00:04:23,220 --> 00:04:26,960
So, to say, if P is true, then the
negation of Q is false,

70
00:04:26,960 --> 00:04:30,680
is just the same as to say, if P is true,
then Q is true.

71
00:04:33,300 --> 00:04:35,500
But that's exactly what you're doing
whenever

72
00:04:35,500 --> 00:04:38,750
you use if then as a propositional
connective.

73
00:04:38,750 --> 00:04:42,660
You're saying if one proposition is true.

74
00:04:42,660 --> 00:04:45,780
Like, let's say, Walter is eating lunch at
New Havana.

75
00:04:45,780 --> 00:04:50,980
Then, another proposition is true.
Like, let's say, the private investigator

76
00:04:50,980 --> 00:04:58,990
is eating lunch at New Havana.
If P then Q, that proposition follows from

77
00:04:58,990 --> 00:05:04,175
the negation of the conjunction P and the
negation of Q, but while if

78
00:05:04,175 --> 00:05:09,970
P then Q follows from the negation of that
conjunction, is it equivalent to it?

79
00:05:11,040 --> 00:05:16,698
Now I just argued that if P then Q follows

80
00:05:16,698 --> 00:05:22,290
from the negation of the conjunction, P
and the negation of Q.

81
00:05:22,290 --> 00:05:24,160
Now what I'd like to do is argue

82
00:05:24,160 --> 00:05:27,650
that latter proposition, the negation of
the conjunction of p and the

83
00:05:27,650 --> 00:05:32,360
negation of q, that that proposition,
follows from if p then q.

84
00:05:33,390 --> 00:05:36,580
Consider for a moment what you're saying
when you say if p then q.

85
00:05:36,580 --> 00:05:39,920
You're saying if p is true.
Then Q has gotta be true.

86
00:05:42,130 --> 00:05:45,360
So you're ruling out a certain option.

87
00:05:45,360 --> 00:05:51,660
You're ruling out the possibility that P
is true, while Q is false.

88
00:05:51,660 --> 00:05:54,840
When you say if P, then Q, you're ruling
out

89
00:05:54,840 --> 00:05:57,540
the possibility that P is true and Q is
false.

90
00:05:58,710 --> 00:06:01,940
In other words, you're ruling out the
possibility that

91
00:06:01,940 --> 00:06:05,290
P is true and the negation of Q is true.

92
00:06:05,290 --> 00:06:05,790
In

93
00:06:07,970 --> 00:06:10,700
other words, you're saying P and the
negation of

94
00:06:10,700 --> 00:06:14,840
Q, that conjunction right there, has gotta
be false.

95
00:06:14,840 --> 00:06:20,110
In other words, you're saying that the
negation Of

96
00:06:20,110 --> 00:06:24,670
the conjunction P and the negation of Q,
is true.

97
00:06:26,250 --> 00:06:29,800
So you see, the negation of the
conjunction, P

98
00:06:29,800 --> 00:06:33,380
and the negation of Q, follows from, if P

99
00:06:33,380 --> 00:06:34,980
then Q.

100
00:06:34,980 --> 00:06:38,580
And, if P then Q, follows from, the
negation

101
00:06:38,580 --> 00:06:41,320
of the conjunction of P and the negation
of Q.

102
00:06:43,920 --> 00:06:49,480
So what that tells, us is the proposition
if P, then Q is going to

103
00:06:49,480 --> 00:06:54,970
be true in precisely the same situations
as the proposition

104
00:06:54,970 --> 00:07:00,090
which is the negation of the conjunction,
P in the the negation of Q.

105
00:07:02,720 --> 00:07:05,630
So if they're true in all the same
situations,

106
00:07:05,630 --> 00:07:09,170
that just means that they're going to have
the same truth-table.

107
00:07:09,170 --> 00:07:13,680
In other words, if P then Q is going to
have a truth-table.

108
00:07:13,680 --> 00:07:18,610
And it's going to have precisely the same
truth-table as the

109
00:07:18,610 --> 00:07:23,310
negation of the conjunction of P and the
negation of Q.

110
00:07:23,310 --> 00:07:27,740
That's what we learn by noticing that if P
then Q

111
00:07:27,740 --> 00:07:33,215
follows from the negation of the
conjunction of P and the negation of Q.

112
00:07:33,215 --> 00:07:35,570
And that the negation of the conjunction
of P and

113
00:07:35,570 --> 00:07:39,350
the negation of Q follows from if P then
Q.

114
00:07:39,350 --> 00:07:41,910
Is that they have the same truth-table.

115
00:07:41,910 --> 00:07:44,890
And since if P then Q has a truth-table.

116
00:07:44,890 --> 00:07:47,950
That proves that it's a truth functional
connective,

117
00:07:47,950 --> 00:07:52,960
it's a propositional connective that
creates propositions whose truth

118
00:07:52,960 --> 00:07:59,310
value depends solely on the truth values
of the propositions that go into it.

119
00:08:00,420 --> 00:08:02,190
So it's a truth functional connective.

120
00:08:03,820 --> 00:08:06,000
Now that we've talked about the
conditional, and we've

121
00:08:06,000 --> 00:08:09,110
explained how the truth table for the
conditional works.

122
00:08:09,110 --> 00:08:12,890
Let's talk about, some rules governing our
use of the conditional.

123
00:08:12,890 --> 00:08:14,850
And then let me say something about why

124
00:08:14,850 --> 00:08:18,359
the conditional is an especially important
truth functional connective.

125
00:08:19,700 --> 00:08:22,709
So first, I want to talk about a rule
called Modus Ponens.

126
00:08:24,560 --> 00:08:31,080
Modus Ponens says from the premises P,
whatever P is, and if

127
00:08:31,080 --> 00:08:37,390
P then Q, whatever exactly P and Q are,
infer the conclusion Q.

128
00:08:38,640 --> 00:08:40,380
That's what Modus Ponens says.

129
00:08:40,380 --> 00:08:45,780
Now notice, we can use the truth table for
the conditional to show that Modus Ponens

130
00:08:45,780 --> 00:08:49,934
is a good rule of inference.
Look here.

131
00:08:49,934 --> 00:08:54,430
So suppose you know that P is true and if

132
00:08:54,430 --> 00:08:57,117
P, then Q is true, so what does that tell
you?

133
00:08:57,117 --> 00:09:03,410
Well, since P is true, we've gotta be in
one of these first two scenarios.

134
00:09:03,410 --> 00:09:09,560
One of the two top rows of the
truth-table.

135
00:09:09,560 --> 00:09:10,800
And since the conditional

136
00:09:10,800 --> 00:09:16,020
if p then q is true, we've gotta be in
either the first, third or fourth

137
00:09:17,280 --> 00:09:22,880
row of that truth table.
But then, what does that tell us.

138
00:09:22,880 --> 00:09:27,100
Well, if we've gotta be in one of the two
top rows, and we've gotta be in either the

139
00:09:27,100 --> 00:09:30,000
first, third or fourth row, then, the only
possible

140
00:09:30,000 --> 00:09:31,950
choice is that we've gotta be in the top
row.

141
00:09:31,950 --> 00:09:35,913
In other words, we've gotta be in a
situation in which P is true,

142
00:09:35,913 --> 00:09:37,640
if P, then Q is true and Q is true.

143
00:09:41,160 --> 00:09:44,640
Another way of putting that is, in any
situation in which P is

144
00:09:44,640 --> 00:09:50,050
true and if P, then Q is true, Q has got
to be true.

145
00:09:50,050 --> 00:09:53,070
And so modus ponens, is a good rule of
inference.

146
00:09:53,070 --> 00:09:58,455
If we follow modus ponens, we'll never
give an invalid argument, right.

147
00:09:58,455 --> 00:10:03,350
No modus ponens argument can be invalid,
because whenever P

148
00:10:03,350 --> 00:10:06,310
is true, and if P then Q is true, Q is

149
00:10:06,310 --> 00:10:07,064
gotta be true.

150
00:10:07,064 --> 00:10:13,384
Okay, so modus ponens is a good rule of
inference.

151
00:10:13,384 --> 00:10:18,050
There's another rule, called modus
tollens, which says the following.

152
00:10:18,050 --> 00:10:26,320
From the premises not Q, the negation of
Q, and the premise if P, then Q, infer the

153
00:10:26,320 --> 00:10:31,229
conclusion not P, or the negation of P.
Now,

154
00:10:32,230 --> 00:10:34,250
is that a good rule?

155
00:10:34,250 --> 00:10:37,530
Well once again, we can use the truth
table to see that it is.

156
00:10:37,530 --> 00:10:42,460
So, here's what the two premises tell us:
they tell us

157
00:10:42,460 --> 00:10:44,830
if P then Q is true, so we've got to be in

158
00:10:44,830 --> 00:10:48,000
the first, third, or fourth rows of that
truth table, and

159
00:10:48,000 --> 00:10:51,360
they also tell us that that the negation
of Q is true.

160
00:10:51,360 --> 00:10:55,460
In other words, that Q itself, is false,
right?

161
00:10:55,460 --> 00:10:57,130
'Cuz when the negation of Q is true,

162
00:10:57,130 --> 00:10:57,980
then Q is false.

163
00:10:59,270 --> 00:11:01,400
So that tells us we've gotta be in either

164
00:11:01,400 --> 00:11:06,200
the second or the fourth row of that truth
table.

165
00:11:06,200 --> 00:11:08,580
Okay, so here's what the premises tell us.

166
00:11:08,580 --> 00:11:11,335
We're either in the second or fourth row
of the truth table.

167
00:11:11,335 --> 00:11:16,860
And, we're in either the first, third or
fourth of the truth table.

168
00:11:16,860 --> 00:11:22,350
Well, the only option left is that we're
in the fourth row of the truth table.

169
00:11:22,350 --> 00:11:27,590
In other words, if we're in a situation in
which if p then q is true,

170
00:11:27,590 --> 00:11:31,890
and not Q is true.
In other words, Q is false.

171
00:11:31,890 --> 00:11:38,630
Then the only possible situation we could
be in is a situation in which P is false.

172
00:11:38,630 --> 00:11:43,400
But of course that's a situation in which
not Q is true.

173
00:11:43,400 --> 00:11:47,540
So, if we follow modus tollens, and make

174
00:11:47,540 --> 00:11:52,770
arguments that follow that rule, all of
our arguments will be valid.

175
00:11:52,770 --> 00:11:56,610
Any argument that has as its premises, two
premises of

176
00:11:56,610 --> 00:11:59,280
the form if P than Q, and the negation of

177
00:11:59,280 --> 00:12:02,610
Q, and has as its conclusion the negation
of P,

178
00:12:02,610 --> 00:12:05,650
any argument of that form will have to be
valid.

179
00:12:05,650 --> 00:12:08,834
And that's what we can see using the truth
table for the conditional.

180
00:12:08,834 --> 00:12:12,550
So modus ponens and modus tollens are

181
00:12:12,550 --> 00:12:14,240
two rules governing our use of the

182
00:12:14,240 --> 00:12:16,510
conditional, and we can see using the
truth

183
00:12:16,510 --> 00:12:18,210
table that they're both valid rules of

184
00:12:18,210 --> 00:12:22,905
argument, just like conjunction
elimination or disjunction introduction.

185
00:12:22,905 --> 00:12:29,380
Okay, now I've said something about what
the conditional

186
00:12:29,380 --> 00:12:33,660
means, what its truth-table is, and what
some good rules for its usage are.

187
00:12:35,180 --> 00:12:38,140
Why is the conditional an important

188
00:12:38,140 --> 00:12:39,360
truth functional connective?

189
00:12:39,360 --> 00:12:43,500
It's pretty clear why disjunction and

190
00:12:43,500 --> 00:12:46,500
conjunction are important truth functional
connectives.

191
00:12:46,500 --> 00:12:47,350
They're very intuitive.

192
00:12:47,350 --> 00:12:51,790
They seem to correspond to notions that we
operate with in everyday life.

193
00:12:51,790 --> 00:12:53,010
But what about the conditional.

194
00:12:54,630 --> 00:12:56,030
Here's what makes the conditional

195
00:12:56,030 --> 00:13:01,520
especially important, for propositional
logic.

196
00:13:01,520 --> 00:13:03,250
Consider any argument

197
00:13:03,250 --> 00:13:06,660
whatsoever, that has some premises and a
conclusion.

198
00:13:06,660 --> 00:13:10,930
Let's call the premises of that argument
P, and the conclusion C.

199
00:13:12,400 --> 00:13:18,460
Now whatever that argument is, if that
argument from premises p to conclusion c

200
00:13:18,460 --> 00:13:26,770
is valid, then the conditional if p, then
c, has got to be true.

201
00:13:26,770 --> 00:13:28,480
And here's why.
If the

202
00:13:28,480 --> 00:13:33,180
argument from P to C is valid, what that
tells us is that there's no possible

203
00:13:33,180 --> 00:13:37,630
way for P to be true while C is false.

204
00:13:39,490 --> 00:13:44,320
But what does it mean for the conditional
if P then C to be true?

205
00:13:44,320 --> 00:13:51,280
All it means is that, if P is true, then C
has gotta be true.

206
00:13:52,310 --> 00:13:53,900
It's not the case

207
00:13:53,900 --> 00:13:59,200
that P is true and C is false.
So, whenever the argument

208
00:13:59,200 --> 00:14:04,400
from P to C is valid, the conditional if P
then C, is going to be true.

209
00:14:04,400 --> 00:14:07,210
And so we can use the conditional to

210
00:14:07,210 --> 00:14:10,870
express, in the form of a single
proposition,

211
00:14:10,870 --> 00:14:14,640
the validity of an argument, from premises
P

212
00:14:14,640 --> 00:14:18,220
to conclusion C, whatever that argument is
about.

213
00:14:18,220 --> 00:14:19,110
The conditional

214
00:14:19,110 --> 00:14:22,070
can be used to express the validity of
that argument,

215
00:14:22,070 --> 00:14:26,720
and that's what makes the conditional
especially useful in propositional logic.

