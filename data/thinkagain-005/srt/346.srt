1
00:00:00,033 --> 00:00:08,120
>> Today, we're going to talk about how
you can use Venn diagrams

2
00:00:08,120 --> 00:00:13,172
to show that a particular inference is
valid, and to explain why it's valid.

3
00:00:13,172 --> 00:00:14,624
>> Woo hoo.

4
00:00:14,624 --> 00:00:17,283
>> Yeah.
>> Alright.

5
00:00:17,283 --> 00:00:19,217
>> Yeah.
>> Yeah.

6
00:00:20,840 --> 00:00:20,840
>>

7
00:00:20,840 --> 00:00:20,840
[NOISE]

8
00:00:20,840 --> 00:00:22,990
We're going to do that by considering the

9
00:00:22,990 --> 00:00:27,190
particular inferences, the particular
arguments, that we looked

10
00:00:27,190 --> 00:00:32,350
at, at the beginning of last week, when we
were starting our unit on deductive logic.

11
00:00:33,480 --> 00:00:36,050
So, let's look at those inferences right
now.

12
00:00:36,050 --> 00:00:40,460
Remember the first one, was the example
that Walter gave back in week three.

13
00:00:40,460 --> 00:00:42,550
Our argument about Mary.

14
00:00:42,550 --> 00:00:45,500
Premise one was, Mary has a child who is
pregnant.

15
00:00:45,500 --> 00:00:48,580
Premise two was, only daughters can become
pregnant.

16
00:00:48,580 --> 00:00:50,930
And so the conclusion of those two
premises

17
00:00:50,930 --> 00:00:53,020
was, therefore, Mary has at least one
daughter.

18
00:00:54,680 --> 00:00:54,980
Okay.

19
00:00:54,980 --> 00:01:00,310
Now, how do we represent the information
contained in

20
00:01:00,310 --> 00:01:03,340
the premises of this inference, using a
Venn diagram.

21
00:01:03,340 --> 00:01:06,620
Well, let's begin by asking, what are

22
00:01:06,620 --> 00:01:10,520
the categories that this inference, that
this argument,

23
00:01:10,520 --> 00:01:15,280
brings into relation with each other.
Is Mary one of the categories?

24
00:01:17,050 --> 00:01:17,560
Well, no.

25
00:01:18,595 --> 00:01:21,210
'Cuz Mary's not a category, Mary's an
object.

26
00:01:21,210 --> 00:01:22,360
Mary's a thing.

27
00:01:22,360 --> 00:01:25,210
She's a particular thing.
She's not a category.

28
00:01:26,580 --> 00:01:29,160
What is a category though, that's

29
00:01:29,160 --> 00:01:32,560
mentioned in this argument, is Mary's
children.

30
00:01:34,070 --> 00:01:35,520
And the

31
00:01:35,520 --> 00:01:42,740
information in the first premise, is that
the category of Mary's children, include

32
00:01:42,740 --> 00:01:48,970
something that is also in the category of
pregnant people, or pregnant beings.

33
00:01:50,180 --> 00:01:55,610
So, we can visually represent that
information as follows.

34
00:01:55,610 --> 00:01:58,929
The category of Mary's children,

35
00:02:00,070 --> 00:02:06,960
includes something that's in the category
of pregnant people,

36
00:02:09,680 --> 00:02:09,970
right?

37
00:02:09,970 --> 00:02:12,660
So, we don't know whether or not the

38
00:02:12,660 --> 00:02:15,910
category of Mary's children includes
anything out here, but

39
00:02:15,910 --> 00:02:18,450
whether or not it includes anything out
here,

40
00:02:18,450 --> 00:02:22,300
it's got to include something that's in
this region.

41
00:02:24,450 --> 00:02:28,900
So, do we draw an "X"?
Well, let's wait a minute.

42
00:02:28,900 --> 00:02:31,264
Let's ask, where would we draw that "X".

43
00:02:31,264 --> 00:02:36,080
Would we draw it here, or would we draw it
here?

44
00:02:36,080 --> 00:02:37,359
That's going to make a difference.

45
00:02:38,880 --> 00:02:40,920
That's going to make a difference, where
we draw it.

46
00:02:40,920 --> 00:02:44,620
So we don't yet know whether to draw the X
in here or in here.

47
00:02:46,110 --> 00:02:49,726
We know though, that the X is supposed to
go somewhere

48
00:02:49,726 --> 00:02:55,970
in this football-shaped region, that's in
the intersection

49
00:02:55,970 --> 00:02:59,750
of the category of Mary's children and the
category of pregnant people.

50
00:03:01,310 --> 00:03:05,280
Maybe premise two, will help us figure out
where to draw that X.

51
00:03:05,280 --> 00:03:05,780
Let's see.

52
00:03:08,000 --> 00:03:11,080
So premise two says, only daughters can
become pregnant.

53
00:03:12,480 --> 00:03:14,070
So how do we visually represent the

54
00:03:14,070 --> 00:03:16,750
information that only daughters can become
pregnant?

55
00:03:16,750 --> 00:03:22,040
Well, only daughters can become pregnant
means, that

56
00:03:22,040 --> 00:03:26,940
there can't be any pregnant people, who
are not daughters.

57
00:03:26,940 --> 00:03:33,360
So, we have to shade out, the circle, the
portion of the circle of pregnant people,

58
00:03:33,360 --> 00:03:35,329
that's outside the circle of daughters.

59
00:03:37,530 --> 00:03:37,880
Right?

60
00:03:37,880 --> 00:03:42,710
There can't be any pregnant people who are
outside this circle.

61
00:03:44,220 --> 00:03:47,270
All the pregnant people are inside this
circle.

62
00:03:49,340 --> 00:03:52,200
Okay, but remember, what did premise one
tell us?

63
00:03:52,200 --> 00:03:58,010
Premise one told us that Mary has a child
who is inside this circle.

64
00:03:58,010 --> 00:03:58,930
So here's what we know.

65
00:03:58,930 --> 00:04:02,680
Mary has a child who's inside the circle
of pregnant people.

66
00:04:03,740 --> 00:04:07,190
And anyone who's inside this circle of
pregnant

67
00:04:07,190 --> 00:04:10,540
people must also be inside the circle of
daughters.

68
00:04:10,540 --> 00:04:14,440
Well, so Mary's child, who's inside the
pregnant people

69
00:04:14,440 --> 00:04:18,710
circle, must be right here.
So that's where the X goes.

70
00:04:19,910 --> 00:04:23,420
So what we can figure out from just the
first two premises.

71
00:04:23,420 --> 00:04:26,110
Remember, I haven't relied on any
information here other

72
00:04:26,110 --> 00:04:29,710
than the information given in premises one
and two.

73
00:04:29,710 --> 00:04:34,880
What premises one and two tell us is that
there's an X and it goes right here.

74
00:04:37,500 --> 00:04:41,560
Okay, but if the x goes right there, then
what does that tell us?

75
00:04:41,560 --> 00:04:46,550
That tells us that at least one of Mary's
children, is a daughter.

76
00:04:48,840 --> 00:04:51,030
Mary has, at least one daughter.

77
00:04:52,470 --> 00:04:53,930
Maybe she has more than one daughter,

78
00:04:53,930 --> 00:04:57,120
but we know, based on the information
contained

79
00:04:57,120 --> 00:05:01,850
in premises one and two, we can conclude,
that she has at least one daughter.

80
00:05:01,850 --> 00:05:03,230
We know that for sure, based on

81
00:05:03,230 --> 00:05:05,630
the information contained in premises one
and two.

82
00:05:07,610 --> 00:05:10,390
So, if the information contained in
premises one and two

83
00:05:10,390 --> 00:05:13,720
is correct, then Mary must have at least
one daughter.

84
00:05:13,720 --> 00:05:14,200
And so

85
00:05:14,200 --> 00:05:15,770
the argument is valid.

86
00:05:15,770 --> 00:05:18,040
And the Venn diagram shows us that the
argument

87
00:05:18,040 --> 00:05:21,210
is valid, because once we visually
represent the information

88
00:05:21,210 --> 00:05:24,390
contained in premises one and two, then we
can

89
00:05:24,390 --> 00:05:28,210
simply read off the diagram, that the
conclusion is true.

90
00:05:28,210 --> 00:05:31,540
We can read off the diagram that Mary has
at least one daughter.

91
00:05:33,500 --> 00:05:34,320
Okay.

92
00:05:34,320 --> 00:05:39,330
Now, let's see if we can apply this
technique to the other arguments

93
00:05:39,330 --> 00:05:42,760
of the same form, that we considered at
the beginning of last week.

94
00:05:42,760 --> 00:05:46,206
Remember, we considered an argument
concerning Terry.

95
00:05:46,206 --> 00:05:51,320
Premise one was, Terry has a job in which
she arrests people.

96
00:05:52,720 --> 00:05:55,880
Premise two was, only police officers can
arrest people.

97
00:05:57,270 --> 00:06:00,070
And, the conclusion of those two premises
was, therefore, at

98
00:06:00,070 --> 00:06:03,370
least one of Terry's jobs is, as a police
officer.

99
00:06:04,480 --> 00:06:10,990
Now, how would we visually represent the
information contained in the premises,

100
00:06:10,990 --> 00:06:15,680
well let's consider.
Premise one tells

101
00:06:15,680 --> 00:06:20,534
us, that, Terry's jobs

102
00:06:20,534 --> 00:06:25,970
includes, a job in which she arrests
people.

103
00:06:25,970 --> 00:06:29,870
Right, so at least one of Terry's jobs,

104
00:06:29,870 --> 00:06:33,500
has to be in this circle.

105
00:06:33,500 --> 00:06:37,630
Because whatever else it is that Terry
does, she has a job in which

106
00:06:37,630 --> 00:06:42,800
she arrests people, so at least one of her
jobs must be in this circle.

107
00:06:42,800 --> 00:06:46,280
And of course, it has to be in this circle
because that's the circle Terry's jobs.

108
00:06:46,280 --> 00:06:51,000
So, at least one of Terry's jobs must be
in this football shaped

109
00:06:51,000 --> 00:06:55,480
region right here, in this intersection of
the category of Terry's jobs and

110
00:06:55,480 --> 00:07:00,410
professionals who arrest people.
That's what premise one tells us.

111
00:07:00,410 --> 00:07:05,930
But, again, it doesn't tell us where that
particular job of Terry's would be.

112
00:07:05,930 --> 00:07:09,210
Is it here, or is it here?

113
00:07:09,210 --> 00:07:10,140
Right?

114
00:07:10,140 --> 00:07:12,390
Premise one doesn't give us that
information.

115
00:07:14,260 --> 00:07:16,350
But what does premise two tell us?

116
00:07:16,350 --> 00:07:20,880
Well, remember, premise two tells us that
only police officers can arrest people.

117
00:07:20,880 --> 00:07:25,380
So, how would we visually represent that?
Only police officers can arrest people,

118
00:07:25,380 --> 00:07:30,440
that means, that, there's, in the circle
of

119
00:07:30,440 --> 00:07:35,700
professionals who arrest people, we have
to shade in this part, because

120
00:07:35,700 --> 00:07:40,850
there are no professionals who arrest
people, other than,

121
00:07:40,850 --> 00:07:46,020
police officers.
Only police officers arrest people.

122
00:07:46,020 --> 00:07:50,505
So we shade in that portion of the
professionals who arrest people circle.

123
00:07:50,505 --> 00:07:56,440
And now remember what we learned from
premise one, was

124
00:07:56,440 --> 00:07:59,570
that Terry has a job in which she arrests
people,

125
00:07:59,570 --> 00:08:03,810
so somewhere in this football-shaped
region there has to be

126
00:08:03,810 --> 00:08:08,284
an "X," but we know now that it can't be
here.

127
00:08:08,284 --> 00:08:12,336
So, the "X" must be here.

128
00:08:14,620 --> 00:08:20,620
So, that's the information, that we get
from premises one and two.

129
00:08:20,620 --> 00:08:25,750
But notice, once we visually represent
that information in our Venn diagram, then

130
00:08:25,750 --> 00:08:30,840
we, we can simply read off the Venn
diagram that Terry has a job.

131
00:08:30,840 --> 00:08:34,610
At least one job in which she's a police
officer.

132
00:08:34,610 --> 00:08:38,760
So at least one of Terry's jobs is as a
police officer.

133
00:08:40,080 --> 00:08:43,780
And we can read that off the Venn diagram,
once we've used

134
00:08:43,780 --> 00:08:45,910
the Venn diagram to represent the

135
00:08:45,910 --> 00:08:48,800
information contained in premises one and
two.

136
00:08:48,800 --> 00:08:53,219
So the Venn diagram, again, can be used to
represent the validity of the inference.

137
00:08:54,800 --> 00:09:00,240
And not only that, notice also that the
Venn diagram, for this

138
00:09:00,240 --> 00:09:05,460
inference, is the same, in the placement
of its shading and

139
00:09:05,460 --> 00:09:09,510
its Xs, as the Venn diagram for the
previous inference about Mary.

140
00:09:09,510 --> 00:09:11,300
Well, what does that tell us?

141
00:09:11,300 --> 00:09:17,530
That tells us, that the two inferences,
the two arguments, are of the same form.

142
00:09:19,760 --> 00:09:22,300
They're of the same form and you can see

143
00:09:22,300 --> 00:09:26,360
that by seeing that their Venn Diagrams
look the same.

144
00:09:26,360 --> 00:09:31,680
The only difference between the two Venn
Diagrams is the labeling of the circles.

145
00:09:31,680 --> 00:09:36,540
Is the categories, the specific
categories, involved in the arguments.

146
00:09:36,540 --> 00:09:39,290
Other than the specific categories
involved in the arguments,

147
00:09:39,290 --> 00:09:42,530
the labeling of the circles, everything
else is the same.

148
00:09:42,530 --> 00:09:45,430
The shading is in the same place, the x is
in the same place.

149
00:09:47,450 --> 00:09:52,500
Okay, now, can we apply this to our third
argument, about Robert?

150
00:09:52,500 --> 00:09:55,930
You'll remember this third argument that
we considered of the same form, at

151
00:09:55,930 --> 00:10:00,800
the beginning of week four, at the
beginning of our unit on deductive logic.

152
00:10:00,800 --> 00:10:03,060
Remember, we considered an argument that
went like this.

153
00:10:03,060 --> 00:10:06,600
Premise one, Robert has a pet who is
canine.

154
00:10:06,600 --> 00:10:09,120
Premise two, only mammals are canine.

155
00:10:10,550 --> 00:10:12,750
conclusion, therefore at least one of
Robert's

156
00:10:12,750 --> 00:10:15,190
pets is mammal.

157
00:10:15,190 --> 00:10:18,050
Okay, well how would we visually represent
the information

158
00:10:18,050 --> 00:10:22,150
contained, in premises one and two of
this, argument?

159
00:10:22,150 --> 00:10:23,160
Here's how.

160
00:10:23,160 --> 00:10:26,340
At least one of Robert's pets is canine.

161
00:10:26,340 --> 00:10:31,408
So that means that, whatever other pets
Robert has, right, maybe Robert has some

162
00:10:31,408 --> 00:10:36,450
non-canine pets, but at least one of
Robert's pets must be in this circle.

163
00:10:37,620 --> 00:10:41,820
Well, that particular pet, that we're
talking about, has to be in this circle.

164
00:10:41,820 --> 00:10:43,310
But, of course, it also has to be in

165
00:10:43,310 --> 00:10:46,210
this circle, because this is the circle of
Robert's pets.

166
00:10:46,210 --> 00:10:49,700
So, the pet must be in this
football-shaped region, right here.

167
00:10:49,700 --> 00:10:53,290
The intersection of Robert's pets and
canines.

168
00:10:55,150 --> 00:10:56,070
But where is it.

169
00:10:56,070 --> 00:11:00,570
Is it here or is it here.
Well, now let's look at premise two.

170
00:11:00,570 --> 00:11:03,090
What do we find out from premise two.
What we

171
00:11:03,090 --> 00:11:05,730
find out from premise two is that only
mammals are canine.

172
00:11:05,730 --> 00:11:10,880
Well, okay, if only mammals are canine,
then that means

173
00:11:11,940 --> 00:11:17,330
that there can't be any canines outside
the circle of mammals.

174
00:11:17,330 --> 00:11:17,630
Right?

175
00:11:17,630 --> 00:11:23,420
All the canines, whatever canines there
are, are in

176
00:11:23,420 --> 00:11:28,280
this region right here.
There aren't any canines

177
00:11:28,280 --> 00:11:30,380
outside the mammal circle.

178
00:11:30,380 --> 00:11:34,790
So, we know that whatever canines there
are, have to

179
00:11:34,790 --> 00:11:37,750
be in this intersection, right here, in
the circle of and

180
00:11:37,750 --> 00:11:41,880
the circle of mammals And we also know
that whatever that

181
00:11:41,880 --> 00:11:46,240
Robert has at least one pet in the circle
of canines.

182
00:11:46,240 --> 00:11:49,030
Okay, well if Robert has a pet in the
circle of canines, and

183
00:11:49,030 --> 00:11:54,150
we also know that there's nothing in the
circle of canines that's over here,

184
00:11:54,150 --> 00:11:58,000
then we know that Robert's pet.

185
00:11:58,000 --> 00:12:01,520
Which has to be in this circle, right,
this is the circle of Robert's pets.

186
00:12:01,520 --> 00:12:06,350
Robert's pet, has gotta be, in here,
right.

187
00:12:06,350 --> 00:12:08,585
The pet that we're talking about, the pet
that's

188
00:12:08,585 --> 00:12:11,020
can-, the pet that's canine, has gotta be
in there.

189
00:12:13,510 --> 00:12:17,290
Okay, so that's a way of visually
representing the information

190
00:12:17,290 --> 00:12:20,580
contained in the, i the two premises of
our argument.

191
00:12:20,580 --> 00:12:23,060
But once we visually represent that
information,

192
00:12:23,060 --> 00:12:25,060
we can just look at the diagram,

193
00:12:25,060 --> 00:12:31,590
and read off the fact that at least one of
Robert's pets is mammal.

194
00:12:32,735 --> 00:12:36,290
'Cuz, at least one of Robert's pets is
inside the circle of mammals.

195
00:12:36,290 --> 00:12:36,510
Right?

196
00:12:36,510 --> 00:12:38,600
Inside this circle, which means that at
least one

197
00:12:38,600 --> 00:12:39,940
of Robert's pets is mammal.

198
00:12:41,180 --> 00:12:45,000
But that's exactly what the conclusion
says.

199
00:12:45,000 --> 00:12:51,260
And so, this Venn diagram shows us that
this argument about

200
00:12:51,260 --> 00:12:54,410
Robert is valid, just as the argument
about Mary and Terry

201
00:12:54,410 --> 00:12:58,960
were valid, and furthermore this argument
about Robert is valid for

202
00:12:58,960 --> 00:13:03,310
the same reason as the arguments about
Mary and Terry were valid.

203
00:13:03,310 --> 00:13:04,100
They're valid

204
00:13:04,100 --> 00:13:06,610
not because of their subject matter,
right?

205
00:13:06,610 --> 00:13:09,510
The three arguments have completely
different subject matters.

206
00:13:09,510 --> 00:13:12,440
They're valid because of their form, and
their

207
00:13:12,440 --> 00:13:15,640
form is what's revealed by the Venn
diagram.

208
00:13:15,640 --> 00:13:19,050
The form is what's in common to the three
Venn diagrams.

209
00:13:19,050 --> 00:13:23,440
The placement of shading and Xs in the
Venn diagrams is their common

210
00:13:23,440 --> 00:13:26,770
form, and that's what explains why all
three of the arguments are valid.

211
00:13:27,980 --> 00:13:29,370
Okay, see you next time.

