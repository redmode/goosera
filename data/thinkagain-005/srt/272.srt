1
00:00:02,770 --> 00:00:04,420
We're right in the middle of
reconstruction.

2
00:00:04,420 --> 00:00:09,690
We did stage one last week, because that's
just close analysis.

3
00:00:09,690 --> 00:00:12,610
In the previous lecture, we did stage two,
which is to remove

4
00:00:12,610 --> 00:00:14,475
excess verbiage and to put the

5
00:00:14,475 --> 00:00:18,410
explicit premises and conclusion into
standard form.

6
00:00:18,410 --> 00:00:21,430
And this lecture is going to focus on
stage three.

7
00:00:21,430 --> 00:00:23,850
Which is to clarify the premises, and to

8
00:00:23,850 --> 00:00:27,560
break them up where possible, without
distorting them.

9
00:00:27,560 --> 00:00:28,150
Let's start

10
00:00:28,150 --> 00:00:30,110
with step 4, which is to clarify

11
00:00:30,110 --> 00:00:32,758
the premises and the conclusion when it's
needed.

12
00:00:32,758 --> 00:00:38,590
So we might need to clarify them, just in
order to make them easier to understand.

13
00:00:38,590 --> 00:00:42,290
Or to make them less likely to mislead.
And.

14
00:00:42,290 --> 00:00:45,530
All that sounds pretty good.
So let's try on this example.

15
00:00:46,970 --> 00:00:53,000
It was hot today, so it will probably be
hot tomorrow again.

16
00:00:53,000 --> 00:00:53,170
Now we

17
00:00:53,170 --> 00:00:57,863
need to clarify that.
What exactly counts as today?

18
00:00:57,863 --> 00:01:02,830
Is that you know, the time when there's
daylight?

19
00:01:02,830 --> 00:01:06,032
Well, does it also include night?
Even though night is not day.

20
00:01:06,032 --> 00:01:11,300
And what exactly do we mean by hot?
How hot was it today?

21
00:01:11,300 --> 00:01:12,860
And how hot will it be tomorrow?

22
00:01:12,860 --> 00:01:18,260
And what, after all, is heat?
And what about, it'll probably

23
00:01:18,260 --> 00:01:20,080
be hot tomorrow?
Probability.

24
00:01:20,080 --> 00:01:21,180
That's a tough notion.

25
00:01:21,180 --> 00:01:24,700
We're going to spend a whole week on that
later on in the course.

26
00:01:24,700 --> 00:01:26,432
There are different kinds of probability.

27
00:01:26,432 --> 00:01:29,930
I want to know what kinds you're talking
about here and

28
00:01:29,930 --> 00:01:33,680
when you ask, for example about, will it
be hot tomorrow?

29
00:01:33,680 --> 00:01:35,460
What is, what is will mean?

30
00:01:35,460 --> 00:01:37,320
It means it's going to happen in the
future.

31
00:01:37,320 --> 00:01:41,775
And what exactly is the future and is the
future real, is time real?

32
00:01:41,775 --> 00:01:43,720
You can go a long

33
00:01:43,720 --> 00:01:46,238
way towards asking how to clarify that
argument.

34
00:01:47,310 --> 00:01:49,280
But that's ridiculous.

35
00:01:49,280 --> 00:01:53,200
You know, we don't have to clarify a
simple argument like,

36
00:01:53,200 --> 00:01:57,430
it was hot today, so it will probably be
hot tomorrow.

37
00:01:57,430 --> 00:01:59,320
And it's lucky we don't have to clarify

38
00:01:59,320 --> 00:02:03,150
every word in the argument because you
couldn't.

39
00:02:03,150 --> 00:02:06,450
After all when you explain one of the
words or give a definition

40
00:02:06,450 --> 00:02:08,740
for it, its going to be in terms of other
words, and they

41
00:02:08,740 --> 00:02:11,390
have to be clarified and you will never
get to the end of it.

42
00:02:12,500 --> 00:02:17,570
The search for perfect clarity and
absolute precision is impossible.

43
00:02:17,570 --> 00:02:18,940
You will never complete that search.

44
00:02:18,940 --> 00:02:22,266
You will never find perfect clarity or
absolute precision.

45
00:02:22,266 --> 00:02:24,720
So give it up.

46
00:02:24,720 --> 00:02:28,360
What we should seek is not absolute
precision but adequate

47
00:02:28,360 --> 00:02:33,820
precision, not absolute clarity but
adequate clarity and that means that

48
00:02:33,820 --> 00:02:36,710
we ought to try to clear up those parts of

49
00:02:36,710 --> 00:02:41,700
the premises and conclusions that are
likely to produce confusion later.

50
00:02:41,700 --> 00:02:45,620
You have to be able to kind of predict,
whether this part of

51
00:02:45,620 --> 00:02:47,920
the argument needs to be clarified,
because

52
00:02:47,920 --> 00:02:50,490
people are going to get confused by it.

53
00:02:50,490 --> 00:02:54,760
Now, that's not going to be easy, and
there's no simple or mechanical rule to

54
00:02:54,760 --> 00:02:59,310
tell you what needs to be clarified and
what doesn't need to be clarified.

55
00:02:59,310 --> 00:03:02,030
The only way to learn this skill.

56
00:03:02,030 --> 00:03:04,990
Is to go through some examples that'll
give you

57
00:03:04,990 --> 00:03:07,928
models of what needs to be clarified and
what done.

58
00:03:07,928 --> 00:03:11,268
Sometimes the unclarity lies in a single
word.

59
00:03:11,268 --> 00:03:18,120
In the 1980s, Nancy Reagan used to say,
"Just say 'No' to drugs".

60
00:03:18,120 --> 00:03:24,300
What does that mean?
Well, she is telling you not to use drugs.

61
00:03:24,300 --> 00:03:28,920
To say no when somebody offers you drugs
or tries to tell you to use drugs.

62
00:03:28,920 --> 00:03:32,320
So in effect she's saying, you ought not
to use drugs.

63
00:03:34,280 --> 00:03:37,270
That's pretty clear, but now what does she
mean by drugs?

64
00:03:38,510 --> 00:03:39,840
Does she mean Aspirin?

65
00:03:42,440 --> 00:03:45,100
I don't think she's telling me not to use
Aspirin?

66
00:03:45,100 --> 00:03:46,840
Does she mean prescription drugs?

67
00:03:46,840 --> 00:03:50,840
I don't think she's telling me not to
follow the advice of

68
00:03:50,840 --> 00:03:54,351
your doctor and use the prescriptions that
the doctor told you to take.

69
00:03:55,810 --> 00:04:00,790
So that can't be what she means.
Well maybe she means illegal drugs.

70
00:04:01,940 --> 00:04:04,240
Okay.
Maybe she means illegal drugs.

71
00:04:04,240 --> 00:04:07,550
What about heroin or cocaine?
Yes, that's what

72
00:04:07,550 --> 00:04:08,780
she's telling you not to do.

73
00:04:08,780 --> 00:04:12,080
She's definitely telling you not to take
illegal drugs.

74
00:04:13,150 --> 00:04:14,590
But then there's somethings in the middle.

75
00:04:14,590 --> 00:04:16,240
She might be telling you not to

76
00:04:16,240 --> 00:04:19,082
take dangerous drugs, whether they're
illegal or not.

77
00:04:19,082 --> 00:04:20,880
What about.

78
00:04:20,880 --> 00:04:24,380
Nicotine.
What about alcohol?

79
00:04:24,380 --> 00:04:26,480
Those are both dangerous drugs.

80
00:04:26,480 --> 00:04:30,030
At least when you used to excess, alcohol
is very dangerous

81
00:04:30,030 --> 00:04:32,630
and smoking can lead to lung cancer and
that's how most people

82
00:04:32,630 --> 00:04:33,790
get nicotine.

83
00:04:33,790 --> 00:04:38,160
So maybe she's telling you not to take
nicotine or

84
00:04:38,160 --> 00:04:42,540
alcohol in addition to illegal drugs like
heroine and cocaine.

85
00:04:42,540 --> 00:04:46,170
Now it's not clear.
So how do we clear it up?

86
00:04:46,170 --> 00:04:51,660
Well, you want Nancy Reagan's claim to
look as good as possible.

87
00:04:51,660 --> 00:04:55,670
Remember, you're always trying to make the
argument look as good as possible.

88
00:04:55,670 --> 00:04:57,700
And one way to make it look good is

89
00:04:57,700 --> 00:05:04,046
to make your claim no more than she has to
claim.

90
00:05:04,046 --> 00:05:08,230
So, she could be claiming that in addition
to heroin and cocaine,

91
00:05:08,230 --> 00:05:13,900
you shouldn't take alcohol and nicotine,
but probably or at at least more

92
00:05:13,900 --> 00:05:14,780
[INAUDIBLE]

93
00:05:14,780 --> 00:05:17,700
she's telling you not to take illegal
drugs.

94
00:05:17,700 --> 00:05:20,840
She had to choose between interpret her as
saying don't

95
00:05:20,840 --> 00:05:25,410
take any illegal drugs and don't take any
dangerous drugs.

96
00:05:25,410 --> 00:05:28,230
And it seems like the more charitable
interpretation that makes

97
00:05:28,230 --> 00:05:32,360
her claim look more plausible is don't
take any illegal drugs.

98
00:05:32,360 --> 00:05:36,180
So we could clarify her claim, just say
"No" to drug,

99
00:05:36,180 --> 00:05:39,940
by interpreting it to mean you ought not
to take any

100
00:05:39,940 --> 00:05:44,340
illegal drugs.
So in general then, the lesson is.

101
00:05:44,340 --> 00:05:49,720
That, when there are options about how to
clarify a certain sentence, we ought to

102
00:05:49,720 --> 00:05:53,180
pick the most charitable option that makes
the claim look as good as possible.

103
00:05:54,340 --> 00:05:58,040
Here's another example, where the
unclarity can

104
00:05:58,040 --> 00:05:59,580
be traced to a single word, but

105
00:05:59,580 --> 00:06:03,750
in this case it's the word that, and it's
not clear what it refers to.

106
00:06:03,750 --> 00:06:05,410
So imagine that someone argues

107
00:06:05,410 --> 00:06:06,470
like this.

108
00:06:06,470 --> 00:06:09,840
They say, she claims that our strategy

109
00:06:09,840 --> 00:06:12,270
won't work because the enemy knows our
plan.

110
00:06:12,270 --> 00:06:14,870
But that is a big mistake.

111
00:06:17,110 --> 00:06:19,350
What is that refer to?

112
00:06:19,350 --> 00:06:23,960
That could refer to, that is the word that
could refer to,

113
00:06:26,260 --> 00:06:28,500
that the enemy knows our plan.

114
00:06:28,500 --> 00:06:31,530
Someone says, that's a mistake, they might
be saying it's a mistake to think that the

115
00:06:31,530 --> 00:06:37,470
enemy knows our plan, but it could refer
to the claim that our strategy won't work.

116
00:06:37,470 --> 00:06:38,900
They could be saying it's a mistake to
think

117
00:06:38,900 --> 00:06:43,002
our strategy won't work or they could be
saying,

118
00:06:43,002 --> 00:06:47,580
but the mistake is to think that the enemy

119
00:06:47,580 --> 00:06:50,150
knowing our plan, is enough to make it not
work.

120
00:06:51,920 --> 00:06:53,140
They might be saying, it's not that it

121
00:06:53,140 --> 00:06:55,210
won't work because the enemy knows the
plan.

122
00:06:55,210 --> 00:06:57,810
Or, here's a fourth possibility.

123
00:06:59,270 --> 00:07:03,190
They could be saying, that is a mistake to
think that she claims that.

124
00:07:03,190 --> 00:07:04,250
That's not what she claims.

125
00:07:05,992 --> 00:07:10,000
So, there're four different ways to
interpret this argument.

126
00:07:11,350 --> 00:07:14,340
And in order to figure out how to
interpret

127
00:07:14,340 --> 00:07:16,920
it we have to figure out, which of those

128
00:07:16,920 --> 00:07:23,770
is most likely as an interpretation of
what the arguer is trying to say.

129
00:07:23,770 --> 00:07:29,050
And that's going to depend on which one
makes the argument look the best.

130
00:07:29,050 --> 00:07:32,680
Now in this example, it's not clear which
interpretation is the

131
00:07:32,680 --> 00:07:36,700
best because someone might give that
argument in a context where they're

132
00:07:36,700 --> 00:07:39,349
saying the mistake is to think she claims
that, but in

133
00:07:39,349 --> 00:07:41,940
other cases they might be saying that the
mistake is to think

134
00:07:41,940 --> 00:07:44,260
that the enemy knows our plan.
They don't really know our plan.

135
00:07:44,260 --> 00:07:47,750
And in other cases, they might be claiming
that other things, you're mistaken.

136
00:07:47,750 --> 00:07:51,470
So, we need to figure out what the person
is saying, but that

137
00:07:51,470 --> 00:07:55,294
could depend on the particular context and
might vary from context to context.

138
00:07:55,294 --> 00:08:03,135
Now, these unclarities seem unintentional.
But sometimes people use unclarity.

139
00:08:03,135 --> 00:08:07,090
To hide problems with their argument.
To try to fool you.

140
00:08:07,090 --> 00:08:13,250
So imagine a politician says, we need to
stop our enemies and stand by our friends.

141
00:08:13,250 --> 00:08:16,140
So we must remain strong and resolute.

142
00:08:18,670 --> 00:08:23,310
Well, if somebody starts arguing like that
you ought to be asking yourself.

143
00:08:23,310 --> 00:08:25,080
Who do they think our friends are?

144
00:08:25,080 --> 00:08:30,600
Who do they think our enemies are?
What do they mean, stop our enemies?

145
00:08:30,600 --> 00:08:32,290
Are they calling for military action?

146
00:08:32,290 --> 00:08:34,940
How do they think we ought to stop our
enemies?

147
00:08:34,940 --> 00:08:37,390
And standing by our friends, does that
mean we

148
00:08:37,390 --> 00:08:39,397
ought to support them no matter what they
do?

149
00:08:39,397 --> 00:08:43,700
There're lots of questions that you would
want to ask to clarify

150
00:08:43,700 --> 00:08:48,795
exactly which claim is being made before
you accept something like this.

151
00:08:48,795 --> 00:08:53,160
Heres another claim that might be made by
an opponent of a first politician.

152
00:08:53,160 --> 00:08:56,490
"We have to help the needy," wait a
minute.

153
00:08:57,570 --> 00:09:00,810
Which people are needy?
I mean everybody needs something.

154
00:09:00,810 --> 00:09:05,170
How needy do you have to be to be needy?
And, we ought to help the needy?

155
00:09:05,170 --> 00:09:06,350
Well, how are we going to help them?

156
00:09:06,350 --> 00:09:08,730
Does that mean we just give them whatever
they want?

157
00:09:08,730 --> 00:09:13,180
Or, what are we supposed to give them, and
when are we supposed to

158
00:09:13,180 --> 00:09:16,480
give them, and how much are we willing to
spend on giving it to them?

159
00:09:16,480 --> 00:09:19,510
Politicians on both sides of the political
spectrum

160
00:09:19,510 --> 00:09:22,255
make vague claims that need to be
clarified.

161
00:09:22,255 --> 00:09:26,760
Before you should be willing to endorse
one or the other of those claims.

162
00:09:26,760 --> 00:09:30,580
If you try to decide, what to believe

163
00:09:30,580 --> 00:09:33,870
before you know exactly what the claim
means,

164
00:09:33,870 --> 00:09:38,190
before you've clarified it, you could end
up committing yourself to

165
00:09:38,190 --> 00:09:42,610
all kinds of nonsense and all kinds of
very problematic positions.

166
00:09:42,610 --> 00:09:44,447
You could get yourself into a lot of
trouble.

167
00:09:44,447 --> 00:09:47,950
That's why we need to clarify the terms in
arguments.

168
00:09:49,000 --> 00:09:53,270
Now, one special way in which premises
need to be clarified, is that

169
00:09:53,270 --> 00:09:56,804
they need to be broken up into smaller
parts, where you can do that.

170
00:09:56,804 --> 00:09:58,920
And the point of this is that

171
00:09:58,920 --> 00:10:01,820
the smaller parts are going to be easier
to understand,

172
00:10:01,820 --> 00:10:05,026
and easier to access whether they are true
or not.

173
00:10:05,026 --> 00:10:10,070
So step 4, clarify the premises, belongs
together

174
00:10:10,070 --> 00:10:14,570
with step 5, break up the premises into
parts.

175
00:10:14,570 --> 00:10:16,450
What needs to be broken up?

176
00:10:16,450 --> 00:10:21,360
Well, the explicit premises and sometimes
the conclusion, as well.

177
00:10:21,360 --> 00:10:22,320
Here's a simple example.

178
00:10:23,840 --> 00:10:26,501
That shirt looks great on you and it's on
sale.

179
00:10:26,501 --> 00:10:28,581
So you ought to buy it.

180
00:10:28,581 --> 00:10:32,982
We might put that in standard form like
this.

181
00:10:32,982 --> 00:10:37,960
That shirt looks great on you and, it's on
sale, is the premise.

182
00:10:37,960 --> 00:10:40,811
And the conclusion is, you ought to buy
it.

183
00:10:40,811 --> 00:10:47,080
But notice, that the premise has two
parts, joined by an and.

184
00:10:47,080 --> 00:10:49,500
So, we could break

185
00:10:49,500 --> 00:10:49,950
them up.

186
00:10:49,950 --> 00:10:52,500
And have the first premise, that shirt
looks great

187
00:10:52,500 --> 00:10:55,310
on you, and the second premise, it's on
sale.

188
00:10:55,310 --> 00:11:00,230
And then the conclusion is, you ought to
buy it.

189
00:11:00,230 --> 00:11:02,160
Breaking it up like that is supposed to
make

190
00:11:02,160 --> 00:11:06,230
it easier to asses the premise for truth
or falsehood.

191
00:11:06,230 --> 00:11:09,350
Now in this case it doesn't make it much
easier, because it was so simple to begin

192
00:11:09,350 --> 00:11:12,170
with, but we'll see that breaking up
premises will

193
00:11:12,170 --> 00:11:14,730
really help when we get to more complex
examples.

194
00:11:15,800 --> 00:11:22,100
So, it makes sense to break up premises.
Well, at least sometimes.

195
00:11:22,100 --> 00:11:27,080
We should not break up premises when
breaking them up distorts the argument.

196
00:11:27,080 --> 00:11:28,070
Here's an example of that.

197
00:11:29,570 --> 00:11:34,980
We still need to add either one more cup
of white sugar or one more cup of brown

198
00:11:34,980 --> 00:11:40,390
sugar to complete the recipe, so we've
gotta add another cup of ingredients.

199
00:11:41,780 --> 00:11:44,070
Then one way to represent that argument
would be to say

200
00:11:44,070 --> 00:11:47,340
that premises, we still need to add either
one more cup

201
00:11:47,340 --> 00:11:49,870
of white sugar or one more cup of brown
sugar and

202
00:11:49,870 --> 00:11:53,750
the conclusion is, we have another cup of
ingredients to add.

203
00:11:55,660 --> 00:11:57,610
But we could break it up because it's got
parts.

204
00:11:57,610 --> 00:12:00,670
We could change the argument into, we
still

205
00:12:00,670 --> 00:12:02,510
need to add one more cup of white sugar.

206
00:12:02,510 --> 00:12:03,650
That's the first premise.

207
00:12:03,650 --> 00:12:06,880
And the second premise is, we still need
to add one more cup

208
00:12:06,880 --> 00:12:10,730
of brown sugar and then the conclusion is,
we have one more

209
00:12:10,730 --> 00:12:14,980
cup of ingredients to add, but that
argument doesn't make any sense.

210
00:12:14,980 --> 00:12:16,780
If we've got to add one of white and one
of

211
00:12:16,780 --> 00:12:19,728
brown, we don't just have one more cup of
ingredients to add.

212
00:12:19,728 --> 00:12:25,870
And as always, we're supposed to be making
the argument look good.

213
00:12:25,870 --> 00:12:32,340
And that change made it look bad.
And the problem is that here,

214
00:12:32,340 --> 00:12:34,740
we broke up the word or.

215
00:12:34,740 --> 00:12:37,610
Because it's one cup of white or one cup
of brown.

216
00:12:37,610 --> 00:12:40,080
And presumably you didn't know which it
was

217
00:12:40,080 --> 00:12:42,430
or maybe you had a choice between the two.

218
00:12:42,430 --> 00:12:44,090
But you weren't suppose to both.

219
00:12:44,090 --> 00:12:48,100
That would be too much.
And the word or signals that.

220
00:12:48,100 --> 00:12:52,880
So, in general, you should not break up,
when the word that joins the

221
00:12:52,880 --> 00:12:57,340
two is or, but it's okay to break up when
the word that joins

222
00:12:57,340 --> 00:12:58,300
the two is and.

223
00:12:58,300 --> 00:13:01,560
You still gotta be careful about context,
it's not

224
00:13:01,560 --> 00:13:05,180
always going to work that way, but as a
general rule.

225
00:13:05,180 --> 00:13:09,160
You know, that usually works.
Other cases are even trickier.

226
00:13:09,160 --> 00:13:13,350
One particularly problematic case is
dependent clauses.

227
00:13:14,740 --> 00:13:15,510
Here's an example.

228
00:13:16,550 --> 00:13:19,410
Nancy finished all her homework because
all she had to

229
00:13:19,410 --> 00:13:22,390
do was write 25 lines of poetry, and she
wrote

230
00:13:22,390 --> 00:13:24,572
two sonnets which have 14 lines each.

231
00:13:24,572 --> 00:13:29,200
The dependent clause is, which have 14
lines each.

232
00:13:29,200 --> 00:13:32,955
And the question is, how do we fit that
into standard form?

233
00:13:32,955 --> 00:13:35,495
Well, here's one stab.

234
00:13:35,495 --> 00:13:41,580
The first premise can say, all she had to
do is write 25 lines of poetry.

235
00:13:41,580 --> 00:13:46,730
And the second premise can be, she wrote
two sonnets which have 14 lines each.

236
00:13:46,730 --> 00:13:47,650
And then the conclusion

237
00:13:47,650 --> 00:13:52,810
is, Nancy finished all her homework.
Now the question is, can we

238
00:13:52,810 --> 00:13:58,210
break up that second premise into two
different parts, and it seems like we can.

239
00:13:58,210 --> 00:14:01,900
We should be able to represent the
argument so the first

240
00:14:01,900 --> 00:14:04,510
premise is, all she had to do was write 25
lines of

241
00:14:04,510 --> 00:14:08,310
poetry and the second premise says she
wrote two sonnets, and the

242
00:14:08,310 --> 00:14:12,435
third premise says sonnets have 14 lines
each, and the conclusion is,

243
00:14:12,435 --> 00:14:14,690
she finished all her homework.

244
00:14:14,690 --> 00:14:16,940
In this case, breaking down the premise

245
00:14:16,940 --> 00:14:19,870
actually helps us understand and assess
it.

246
00:14:19,870 --> 00:14:21,830
Because we can decide whether it's really
true,

247
00:14:21,830 --> 00:14:24,912
for example, that sonnets have 14 lines
each.

248
00:14:24,912 --> 00:14:26,525
That's going to be a question.

249
00:14:26,525 --> 00:14:29,830
If the answer is no, then the argument
might fail.

250
00:14:29,830 --> 00:14:31,530
If the answer is yes, at least for

251
00:14:31,530 --> 00:14:34,840
standard sonnets, so the argument looks
pretty good.

252
00:14:34,840 --> 00:14:36,430
Contrast that example with this one.

253
00:14:37,510 --> 00:14:39,710
Our legal system isn't fair, because

254
00:14:39,710 --> 00:14:42,250
authorities go easy on white collar
criminals

255
00:14:42,250 --> 00:14:45,804
who have been allowed to get away with
their crimes in recent years.

256
00:14:45,804 --> 00:14:51,380
Well the premise could be authorities go
easy

257
00:14:51,380 --> 00:14:53,460
on criminals who have been allowed to get

258
00:14:53,460 --> 00:14:55,849
away with their crimes in recent years,
and

259
00:14:55,849 --> 00:15:00,240
the conclusion is our legal system isn't
fair.

260
00:15:01,300 --> 00:15:02,750
Now the question is can we break

261
00:15:02,750 --> 00:15:03,875
up that first premise.

262
00:15:03,875 --> 00:15:06,360
because it has that deep ended clause who
have been

263
00:15:06,360 --> 00:15:08,615
allowed to get away with their crimes in
recent years.

264
00:15:08,615 --> 00:15:11,630
Well, that depends.

265
00:15:11,630 --> 00:15:15,620
Because the person giving the argument
might be saying

266
00:15:15,620 --> 00:15:18,690
that authorities go easy on all white
collar criminals.

267
00:15:18,690 --> 00:15:22,940
And they might be saying, that authorities
only

268
00:15:22,940 --> 00:15:26,250
go easy on a certain subset of
white-collar criminals.

269
00:15:26,250 --> 00:15:27,800
Namely, the subset that have

270
00:15:27,800 --> 00:15:30,080
been allowed to get away with their crimes
in recent years.

271
00:15:31,170 --> 00:15:34,740
If the premise is about all white-collar
criminals, then we can break it up.

272
00:15:35,790 --> 00:15:40,920
So that one premise says, authorities go
easy on white-collar criminals.

273
00:15:40,920 --> 00:15:44,920
And the next premise says, white-collar
criminals have been

274
00:15:44,920 --> 00:15:46,900
allowed to get away with their crimes in
recent years.

275
00:15:49,050 --> 00:15:52,690
But if the arguer is only talking about
some white-collar criminals.

276
00:15:52,690 --> 00:15:55,140
And admits that other white-collar
criminals have not

277
00:15:55,140 --> 00:15:56,650
been allowed to get away with their crime.

278
00:15:56,650 --> 00:15:59,360
He's only saying that.

279
00:15:59,360 --> 00:16:02,630
Authorities go easy on those white-collar
criminals who

280
00:16:02,630 --> 00:16:04,380
have been allowed to get away with their
crime.

281
00:16:04,380 --> 00:16:07,215
That subset of white-collar criminals and
then

282
00:16:07,215 --> 00:16:09,130
it would distort the argument to break

283
00:16:09,130 --> 00:16:14,580
it up, because if you do break it up, then
that second premise says white-collar

284
00:16:14,580 --> 00:16:17,885
criminals have been allowed to get away
with their crimes in recent years.

285
00:16:17,885 --> 00:16:23,790
And if some of 'em haven't, then that
premise turns out to be false.

286
00:16:25,120 --> 00:16:27,480
So if you break it up, you can criticize
it by

287
00:16:27,480 --> 00:16:30,472
pointing out that it doesn't really apply
to all white-collar criminals.

288
00:16:30,472 --> 00:16:35,620
But if you leave it as a single premise,
then it's not subject to that criticism.

289
00:16:35,620 --> 00:16:37,990
So, if you want to be charitable.

290
00:16:37,990 --> 00:16:39,580
You probably ought to keep this

291
00:16:39,580 --> 00:16:44,860
premise together, unless you know on
independent grounds, that the person was

292
00:16:44,860 --> 00:16:50,220
making that claim about all white-collar
criminals, and not just a subset.

293
00:16:50,220 --> 00:16:53,780
So, to make that argument look better, we
don't break up the premise.

294
00:16:53,780 --> 00:16:55,920
And the general lesson is that with

295
00:16:55,920 --> 00:16:59,930
dependent clauses like that, and which,
and who.

296
00:16:59,930 --> 00:17:02,410
You have to look very carefully to figure

297
00:17:02,410 --> 00:17:04,635
out what the speaker wanted to say and
what's

298
00:17:04,635 --> 00:17:08,630
going to make their argument look best and
use that information

299
00:17:08,630 --> 00:17:11,255
to determine whether or not to break up
the premise.

300
00:17:11,255 --> 00:17:14,960
There are no airtight rules as always, so

301
00:17:14,960 --> 00:17:18,490
we needed a few exercises to practice this
skill.

