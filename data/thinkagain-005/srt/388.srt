1
00:00:03,440 --> 00:00:09,240
Another very common kind of inductive
argument is an argument from analogy.

2
00:00:09,240 --> 00:00:11,760
We'll see that these arguments from
analogy are

3
00:00:11,760 --> 00:00:15,210
very closely related to inferences to the
best explanation.

4
00:00:15,210 --> 00:00:18,300
But first, we gotta ask, what's an
analogy?

5
00:00:19,470 --> 00:00:22,270
Analogy is basically a comparison.

6
00:00:22,270 --> 00:00:23,730
between two things.

7
00:00:23,730 --> 00:00:27,310
It points out similarities between those
two things.

8
00:00:27,310 --> 00:00:29,100
And analogies

9
00:00:29,100 --> 00:00:30,600
are given all the time.

10
00:00:31,730 --> 00:00:36,829
For example, a poet might say her eyes
were like emeralds.

11
00:00:38,500 --> 00:00:42,490
Well, in what way were they like emeralds?
They might have had the same color.

12
00:00:42,490 --> 00:00:44,090
They might have shimmered like emeralds.

13
00:00:44,090 --> 00:00:46,270
They might have been valuable like
emeralds.

14
00:00:47,670 --> 00:00:50,500
And the analogy, her eyes were like

15
00:00:50,500 --> 00:00:54,710
emeralds, doesn't really tell you exactly
which respect

16
00:00:54,710 --> 00:00:59,620
her eyes resembled emeralds.
That's part of the point.

17
00:00:59,620 --> 00:01:05,040
When you're writing poetry, you want to
stimulate creative comparisons and

18
00:01:05,040 --> 00:01:10,140
analogies of, so that readers of the poem
can think about it in their own way.

19
00:01:12,190 --> 00:01:15,619
And the same thing holds for other
analogies in other areas.

20
00:01:16,800 --> 00:01:20,700
But the cause is not very specific, some
people think

21
00:01:20,700 --> 00:01:23,520
that analogies are just no good at all in
arguments.

22
00:01:25,210 --> 00:01:29,300
Actually though, we use analogies in
arguments all the time.

23
00:01:30,720 --> 00:01:34,310
Here's an example from public policy, they
built

24
00:01:34,310 --> 00:01:36,970
a transportation system in the city of
Houston,

25
00:01:36,970 --> 00:01:39,960
Texas it worked pretty well.

26
00:01:41,100 --> 00:01:44,470
And then the planners in the city of
Phoenix, Arizona

27
00:01:44,470 --> 00:01:48,630
were wondering what kind of public
transportation system to build there.

28
00:01:48,630 --> 00:01:50,360
And they reasoned like this.

29
00:01:50,360 --> 00:01:54,330
They said, Phoenix is a lot like Houston
in many ways.

30
00:01:55,360 --> 00:02:00,340
Large population, hot during the summer,
many many people.

31
00:02:00,340 --> 00:02:02,050
And large area.

32
00:02:03,820 --> 00:02:10,330
So they said, Phoenix resembles Houston in
a lot of ways.

33
00:02:10,330 --> 00:02:13,450
This type of transportation system worked
in Houston,

34
00:02:13,450 --> 00:02:16,770
so it'll probably work well in Phoenix,
also.

35
00:02:18,120 --> 00:02:20,030
Now, what about law?

36
00:02:20,030 --> 00:02:23,010
Lots of legal decisions were based on
analogies

37
00:02:23,010 --> 00:02:27,850
too, because common law systems at least
follow precedent.

38
00:02:27,850 --> 00:02:28,880
When judges decide

39
00:02:28,880 --> 00:02:32,570
a case one way in one time, at one point,

40
00:02:32,570 --> 00:02:36,940
then later on other judges are supposed to
make similar decisions.

41
00:02:36,940 --> 00:02:40,620
So you can say for example The Supreme
Court declared

42
00:02:40,620 --> 00:02:46,220
that segregated public high schools are
unconstitutional in the United States.

43
00:02:47,830 --> 00:02:54,270
Colleges are a lot like high schools, so
segregated public colleges are

44
00:02:54,270 --> 00:02:56,420
also unconstitutional in the United
States.

45
00:02:57,600 --> 00:03:01,010
And then med schools are a lot like
colleges.

46
00:03:01,010 --> 00:03:04,240
So segregated public medical schools are

47
00:03:04,240 --> 00:03:07,410
also unconstitutional in the United
States.

48
00:03:07,410 --> 00:03:10,320
And that's the way the legal system
evolves by drawing

49
00:03:10,320 --> 00:03:15,150
analogies among the different cases that
come up within that jurisdiction.

50
00:03:16,280 --> 00:03:19,490
This form of argument in law might seem to
be a real problem because

51
00:03:19,490 --> 00:03:22,010
you don't say exactly what the
similarities are.

52
00:03:23,150 --> 00:03:25,670
But actually, it's very useful.

53
00:03:25,670 --> 00:03:28,280
Because it is predictable if you know that

54
00:03:28,280 --> 00:03:31,820
segregated high schools have been declared
unconstitutional, you

55
00:03:31,820 --> 00:03:34,040
pretty much know that judges are going to
find

56
00:03:34,040 --> 00:03:38,090
colleges unconstitutional too, if they are
segregated .

57
00:03:38,090 --> 00:03:43,700
And it also gives flexibility, so that
judges can see when they're going too far.

58
00:03:43,700 --> 00:03:44,500
They say well that

59
00:03:44,500 --> 00:03:47,690
precedence's different and they
distinguish the precedence.

60
00:03:47,690 --> 00:03:51,600
So by resting legal reasoning on arguments
from analogy, they

61
00:03:51,600 --> 00:03:56,730
gain both predictability and also
flexibility in the legal system.

62
00:03:56,730 --> 00:03:58,790
So arguments from analogy can be pretty
useful.

63
00:04:00,310 --> 00:04:05,710
Fine but policies and laws Are all about
norms and values.

64
00:04:05,710 --> 00:04:08,570
What about science and hard facts?

65
00:04:08,570 --> 00:04:10,200
The science issues analogies too.

66
00:04:11,220 --> 00:04:14,570
For example, scientists at one point
didn't know

67
00:04:14,570 --> 00:04:16,730
what was at the center of the earth.

68
00:04:16,730 --> 00:04:22,109
But they found a bunch of meteors and
meteorites, that had a high iron content.

69
00:04:23,660 --> 00:04:26,360
Much higher than the content of iron in
the crust

70
00:04:26,360 --> 00:04:30,720
of the earth so they reasoned the earth
must be

71
00:04:30,720 --> 00:04:33,850
like these other meteors and meteorites so
they must have

72
00:04:33,850 --> 00:04:36,260
a similar amount of iron in it because
they were

73
00:04:36,260 --> 00:04:39,030
produced in the same way in the history of
the universe

74
00:04:40,150 --> 00:04:43,180
but that means there must be a similar
amount of iron.

75
00:04:43,180 --> 00:04:46,210
In the earth, if it's not in the crust,
where could it be?

76
00:04:46,210 --> 00:04:48,040
It must be down in the core.

77
00:04:48,040 --> 00:04:52,600
So they figured, probably the core of the
earth has a lot of iron in it.

78
00:04:54,060 --> 00:04:55,640
That's just one example.

79
00:04:55,640 --> 00:04:58,830
But scientists actually use analogies a
lot, and

80
00:04:58,830 --> 00:05:01,680
if you don't believe me, go read some
psychological

81
00:05:01,680 --> 00:05:03,499
studies of scientific reasoning.

82
00:05:05,412 --> 00:05:08,300
We're going to focus on an example from
art history.

83
00:05:09,590 --> 00:05:13,920
Just imagine that you're going through the
attic and you find an old painting.

84
00:05:13,920 --> 00:05:19,230
It looks a lot like a painting by the
famous impressionist Cezanne.

85
00:05:19,230 --> 00:05:21,780
And if it is by Cezanne, it's worth an
awful lot.

86
00:05:21,780 --> 00:05:26,370
But you've got to figure out Whether this
painting is by Cezzane?

87
00:05:26,370 --> 00:05:26,730
How do you

88
00:05:26,730 --> 00:05:29,550
figure it out, because Cezzane didn't sign
it?

89
00:05:29,550 --> 00:05:30,760
He didn't sign a lot of his paintings.

90
00:05:32,340 --> 00:05:34,910
Well, what you do is you look at other

91
00:05:34,910 --> 00:05:38,190
Cezzane paintings and try to find out
whether they're similar.

92
00:05:38,190 --> 00:05:40,480
And if you're not an expert, you probably
better

93
00:05:40,480 --> 00:05:42,088
check with an expert and have them do it.

94
00:05:42,088 --> 00:05:43,302
But they're going to do the same thing.

95
00:05:43,302 --> 00:05:45,800
They're going to compare this painting to
a lot

96
00:05:45,800 --> 00:05:48,560
of other paintings that we know are by
Cezanne.

97
00:05:49,600 --> 00:05:51,630
And then you can reason like this.

98
00:05:51,630 --> 00:05:58,210
This painting has a certain kind of brush
work and coloring, and so on and so on.

99
00:05:58,210 --> 00:05:59,740
Subject matter, whatever.

100
00:06:01,440 --> 00:06:05,200
other paintings by Cezanne have very
similar

101
00:06:05,200 --> 00:06:08,870
brush work and color patterns and topic.

102
00:06:08,870 --> 00:06:09,440
And so on.

103
00:06:10,730 --> 00:06:12,920
Those other paintings are definitely by
Cezanne, we know

104
00:06:12,920 --> 00:06:16,770
that, therefore, this painting is probably
by Cezanne as well.

105
00:06:19,160 --> 00:06:21,020
Now that's an argument from analogy.

106
00:06:22,100 --> 00:06:24,900
This argument from analogy shares a
certain form with

107
00:06:24,900 --> 00:06:28,250
the other arguments from analogy that we
discussed before,

108
00:06:28,250 --> 00:06:31,790
and we can pick out that form By
substituting

109
00:06:31,790 --> 00:06:35,200
letters for the English words in the
English argument.

110
00:06:36,400 --> 00:06:39,430
For example, we can substitute the letter
A for

111
00:06:39,430 --> 00:06:41,950
the subject, that is the topic of the
argument.

112
00:06:41,950 --> 00:06:44,410
The painting that we don't know whether
it's a Cezanne or not.

113
00:06:45,880 --> 00:06:50,030
And we can substitute the letters B, C,
and D, for.

114
00:06:50,030 --> 00:06:55,030
The similar objects that are also Cezanne
paintings.

115
00:06:56,210 --> 00:07:02,030
And we can substitute the letters P, Q,
and R for the similarities between the

116
00:07:02,030 --> 00:07:05,430
paintings that we know are by Cezanne and
the one that we're not sure of.

117
00:07:06,720 --> 00:07:10,980
And then we can substitute the letter X.
For that

118
00:07:10,980 --> 00:07:13,570
particular property of being by Cezanne.

119
00:07:15,370 --> 00:07:19,710
And when you substitute all those letters
for the English words, then

120
00:07:19,710 --> 00:07:25,090
the argument simply says that object A has
properties P, Q, and R.

121
00:07:25,090 --> 00:07:30,230
And objects B, C, and D also have those
properties P, Q, and R.

122
00:07:31,270 --> 00:07:36,890
And B,C, and D also have the property X,
so the subject,

123
00:07:36,890 --> 00:07:43,508
object A, probably also has the property
X, namely this painting is by Cezanne.

124
00:07:43,508 --> 00:07:45,764
Probably.

125
00:07:45,764 --> 00:07:49,650
Of course since this argument only tries
to show

126
00:07:49,650 --> 00:07:53,270
that the conclusion is probably true, it's
an inductive argument.

127
00:07:53,270 --> 00:07:55,500
It's not valid, it's possible for the

128
00:07:55,500 --> 00:07:57,750
premises to be true and the conclusion
false.

129
00:07:57,750 --> 00:08:02,100
Namely this painting might resemble all
those other paintings in those

130
00:08:02,100 --> 00:08:07,780
respects and yet it's not by Cezanne.
Secondly the argument is defeasible.

131
00:08:09,340 --> 00:08:11,830
You could get some additional information
that

132
00:08:11,830 --> 00:08:14,840
makes it Really look like a bad argument.

133
00:08:14,840 --> 00:08:16,810
For example, you could turn the painting
over.

134
00:08:16,810 --> 00:08:20,110
And on the back, you find the signature of
a different artist, like

135
00:08:20,110 --> 00:08:20,110
[INAUDIBLE]

136
00:08:20,110 --> 00:08:20,610
.

137
00:08:21,740 --> 00:08:23,890
And then you realize, this isn't by
Cezanne at all.

138
00:08:25,750 --> 00:08:28,800
But nonetheless, the argument can be
strong.

139
00:08:28,800 --> 00:08:33,010
It can always be stronger, because there
can be more similarities.

140
00:08:33,010 --> 00:08:38,330
And more important similarities, but it
can be a strong argument and a good

141
00:08:38,330 --> 00:08:44,250
argument because it's inductive, so it
doesnt even try or pretend to be valid.

142
00:08:45,720 --> 00:08:49,960
How can we tell when an argument from
analogy really

143
00:08:49,960 --> 00:08:53,080
does give us a strong reason to believe
the conclusion.

144
00:08:53,080 --> 00:08:55,820
What are the standards by which we measure
How

145
00:08:55,820 --> 00:08:58,630
strong the argument is and how strong the
reasons are.

146
00:08:59,880 --> 00:09:01,960
Well, one of them should be obvious.

147
00:09:01,960 --> 00:09:05,730
Of course the premises have to be true and
justified, like in any argument.

148
00:09:07,650 --> 00:09:11,190
A standard that's relevant here is that
when there are more

149
00:09:11,190 --> 00:09:14,450
important analogies, then it provides a
stronger

150
00:09:14,450 --> 00:09:19,720
reason because some analogies are just
totally unimportant,

151
00:09:19,720 --> 00:09:22,508
the painting is square, other paintings by

152
00:09:22,508 --> 00:09:25,950
Cezzane are square therefore this is by
Cezzane.

153
00:09:25,950 --> 00:09:31,140
Well that's ridiculous right because lots
of painters use square canvases.

154
00:09:31,140 --> 00:09:32,570
For something that's important that it's
going to

155
00:09:32,570 --> 00:09:36,290
be specific to Cezzane and very, very
idiosyncratic.

156
00:09:36,290 --> 00:09:38,800
Is going to be more important for this
type of argument.

157
00:09:40,420 --> 00:09:43,640
Secondly, when they're more analogies.

158
00:09:43,640 --> 00:09:46,140
because we don't know exactly which one is

159
00:09:46,140 --> 00:09:47,700
the one that's important, that's the point
of an

160
00:09:47,700 --> 00:09:49,817
argument from an analogy, you draw the
analogy

161
00:09:49,817 --> 00:09:52,730
without knowing exactly which respect is
the crucial one.

162
00:09:52,730 --> 00:09:57,270
So the more analogies that you have The
more likely

163
00:09:57,270 --> 00:09:58,820
you are going to hit on the ones that are
crucial.

164
00:10:00,820 --> 00:10:05,720
So if it's not just brush work it's also
the

165
00:10:05,720 --> 00:10:10,750
type of paint that is used.
It's also the color scheme that was used.

166
00:10:10,750 --> 00:10:16,070
It's also the geometric shapes.
It's also the subject matter.

167
00:10:16,070 --> 00:10:19,530
It's a particular mountain that's close by
where Cezzane lived.

168
00:10:19,530 --> 00:10:21,155
And he painted a lot of that mountain.

169
00:10:21,155 --> 00:10:25,830
And on and on and on.
The more analogies, the more

170
00:10:25,830 --> 00:10:27,390
likely that some of them are going to be

171
00:10:27,390 --> 00:10:30,560
the crucial ones and therefore the
stronger the argument

172
00:10:30,560 --> 00:10:32,210
is and the stronger reason it gives you

173
00:10:32,210 --> 00:10:37,510
to believe that this particular painting
is by Cezzane.

174
00:10:37,510 --> 00:10:39,342
But of course there are always going to

175
00:10:39,342 --> 00:10:42,790
be some dis-analogies as well because
Cezanne didn't

176
00:10:42,790 --> 00:10:46,390
paint the same thing over and over again
exactly the way he did the first time.

177
00:10:48,428 --> 00:10:51,980
The fewer dis-analogies the stronger the
argument.

178
00:10:51,980 --> 00:10:54,610
There'll always be some or there wouldn't
be much of

179
00:10:54,610 --> 00:10:56,509
an argument there it would be exactly the
same painting.

180
00:10:58,540 --> 00:11:02,445
But the fewer dis-analogies, and the less
important those

181
00:11:02,445 --> 00:11:06,670
dis-analogies are, then the stronger the
argument's going to be.

182
00:11:09,580 --> 00:11:13,270
Next, the objects that you're comparing,

183
00:11:13,270 --> 00:11:15,200
because they're similar in various
respects.

184
00:11:15,200 --> 00:11:18,520
That is, the other paintings that we know
are by Cezzane.

185
00:11:18,520 --> 00:11:25,020
If they're quite diverse then that means
that you have similarities

186
00:11:25,020 --> 00:11:30,440
among a diverse group, they all share
these particular properties and

187
00:11:30,440 --> 00:11:34,990
that means that Cezzane continued to use
those features throughout

188
00:11:34,990 --> 00:11:37,820
all the different types of paintings that
he did.

189
00:11:37,820 --> 00:11:40,260
And that means that it's going to be a
stronger

190
00:11:40,260 --> 00:11:43,005
reason to believe that this painting is by
Cezzane.

191
00:11:43,005 --> 00:11:51,010
Finally, the conclusion is weaker.

192
00:11:51,010 --> 00:11:54,920
You could say, therefore, this painting is
definitely

193
00:11:54,920 --> 00:11:57,640
by Cezzane and it couldn't be anybody
else.

194
00:11:57,640 --> 00:11:59,590
Well that's kind of crazy, right.

195
00:11:59,590 --> 00:12:00,180
But if you say it's

196
00:12:00,180 --> 00:12:04,160
probably by Cezzane, it has some chance
it's

197
00:12:04,160 --> 00:12:07,020
by Cezzane, maybe you want to check it
further.

198
00:12:07,020 --> 00:12:10,980
Then you're weakening the conclusion and
that can make the argument stronger.

199
00:12:11,980 --> 00:12:14,230
So in all of these different ways we can
assess

200
00:12:14,230 --> 00:12:19,060
how strong the argument from analogy is By
looking at the

201
00:12:19,060 --> 00:12:22,610
respects in which the objects are
analogous, the diversity among

202
00:12:22,610 --> 00:12:25,870
the objects that are analogous, the
strength of the conclusion, and

203
00:12:25,870 --> 00:12:26,810
so on, and so on.

204
00:12:27,920 --> 00:12:32,000
And that is how we access an argument from
analogy for strength.

205
00:12:32,000 --> 00:12:37,980
I want to close With one more example that
raises interesting questions about

206
00:12:37,980 --> 00:12:40,470
the relationship between arguments from
analogy

207
00:12:40,470 --> 00:12:42,810
and inferences to the best explanation.

208
00:12:42,810 --> 00:12:47,440
It concerns the pressing issue of whether
neanderthals were cannibals.

209
00:12:48,940 --> 00:12:50,910
Now it's not a pressing issue for most
people but it is

210
00:12:50,910 --> 00:12:54,360
a very pressing issue for people who study
neanderthals.

211
00:12:54,360 --> 00:12:56,350
And so it's quite a breakthrough when they
found some

212
00:12:56,350 --> 00:12:59,460
bones in a cave that they knew was
inhabited by neanderthals.

213
00:13:00,750 --> 00:13:03,750
In that cave, next to what looked like a
firepit.

214
00:13:03,750 --> 00:13:08,480
There were bones of deer, with marking of
a certain sort.

215
00:13:08,480 --> 00:13:10,810
That looked like they had been cutting the
meat off the bone.

216
00:13:10,810 --> 00:13:15,960
And they also found bones of humans in
that

217
00:13:15,960 --> 00:13:20,630
cave where they had been cutting, where
they had similar markings.

218
00:13:20,630 --> 00:13:25,840
And they argued since the bones have
similar markings, and

219
00:13:25,840 --> 00:13:31,060
these bones, the bones of the deer, were
probably cut up

220
00:13:31,060 --> 00:13:36,890
for food, well, the human bones were
probably also cut up for food.

221
00:13:36,890 --> 00:13:41,380
So they reached the conclusion that at
least some times Neanderthals

222
00:13:41,380 --> 00:13:42,420
ate humans.

223
00:13:43,470 --> 00:13:46,780
What's interesting is that there are two
ways to reconstruct this argument.

224
00:13:47,990 --> 00:13:50,590
First, you can reconstruct it as an
argument from analogy.

225
00:13:51,750 --> 00:13:57,160
The bones of the humans were found in this
location with these kinds of markings.

226
00:13:57,160 --> 00:14:00,180
And the bones of deer were also found

227
00:14:00,180 --> 00:14:03,010
in this location with these kinds of
markings.

228
00:14:03,010 --> 00:14:06,490
That the deer were cutup for food,

229
00:14:06,490 --> 00:14:10,950
therefore the humans were probably also
cutup for food.

230
00:14:10,950 --> 00:14:13,960
Now that sounds like an argument from
analogy when you think about it that way.

231
00:14:16,100 --> 00:14:20,520
But you can also reconstruct the argument
as an inference to the best explanation.

232
00:14:21,560 --> 00:14:27,790
The bones of the humans Had these markings
on them and we're in this location.

233
00:14:29,120 --> 00:14:30,020
How do you explain that?

234
00:14:31,630 --> 00:14:34,860
The best explanation of why they have
these particular kinds

235
00:14:34,860 --> 00:14:39,300
of markings is that they were cut up for
food.

236
00:14:39,300 --> 00:14:40,840
Therefore.

237
00:14:40,840 --> 00:14:41,840
The humans

238
00:14:41,840 --> 00:14:46,888
were probably cut up for food as well.
Then notice

239
00:14:46,888 --> 00:14:52,420
that both reconstructions of the argument
make the argument look okay.

240
00:14:53,420 --> 00:14:57,760
And so it's not clear which tells you the
real

241
00:14:57,760 --> 00:15:01,139
structure of the argument that the author
was trying to give.

242
00:15:02,706 --> 00:15:07,340
And that means that arguments from analogy
and inferences to the best explanation

243
00:15:07,340 --> 00:15:09,770
are actually very closely related.

244
00:15:09,770 --> 00:15:13,818
And sometimes you can take an argument and
reconstruct it either way.

245
00:15:13,818 --> 00:15:16,980
It's not going to affect, very much, how
strong the argument

246
00:15:16,980 --> 00:15:21,240
is, but it might affect how you see the
argument working.

247
00:15:22,710 --> 00:15:27,280
And the big difference is that when you do
an argument from analogy,

248
00:15:28,760 --> 00:15:32,800
you don't have to specify exactly which
respect is important.

249
00:15:32,800 --> 00:15:35,290
So you can point out lots of analogies,
and

250
00:15:35,290 --> 00:15:38,010
hope that you hit the one that really
matters.

251
00:15:38,010 --> 00:15:39,380
Whereas when your doing an inference to

252
00:15:39,380 --> 00:15:42,710
the best explanation, Then you have to
pick

253
00:15:42,710 --> 00:15:46,130
out the specific property that gives you

254
00:15:46,130 --> 00:15:49,010
the explanation of the phenomenon that you
observe.

255
00:15:49,010 --> 00:15:50,440
So it forces you to get a little

256
00:15:50,440 --> 00:15:53,980
bit more specific than with an argument
from analogy.

257
00:15:53,980 --> 00:15:58,990
But otherwise, these two arguments are
clearly very closely related.

258
00:15:58,990 --> 00:16:02,150
And they're basically two different ways
to argue.

259
00:16:03,460 --> 00:16:04,720
For similar conclusions.

260
00:16:06,810 --> 00:16:10,700
For example, in this case, with the
argument from analogy, you don't know

261
00:16:10,700 --> 00:16:15,070
whether it's the location next to the
fire, or the types of markings.

262
00:16:15,070 --> 00:16:18,200
Or maybe there's several different types
of markings, and they're all the same.

263
00:16:18,200 --> 00:16:20,010
But you don't know which ones are the ones

264
00:16:20,010 --> 00:16:23,920
that indicate how it was killed, and which
markings indicate.

265
00:16:23,920 --> 00:16:27,360
How it was cut up to be eaten and an
argument from analogy

266
00:16:27,360 --> 00:16:32,465
can leave all that vague and just think
that probably one of those similarities

267
00:16:32,465 --> 00:16:37,180
like justify the conclusion that the
humans were cut up to be eaten.

268
00:16:38,440 --> 00:16:39,510
But if you're going to give an inference
to

269
00:16:39,510 --> 00:16:43,550
the best explanation Then, you're saying
that these markings.

270
00:16:43,550 --> 00:16:48,400
For example, diagonal markings on leg
bones, might suggest

271
00:16:48,400 --> 00:16:51,080
that they cut that up in a certain way,

272
00:16:51,080 --> 00:16:53,440
because that's how they prepared the deer
meat, and

273
00:16:53,440 --> 00:16:56,260
they were used to preparing deer meat that
way.

274
00:16:56,260 --> 00:16:57,610
So they used similar cutting

275
00:16:57,610 --> 00:17:00,720
techniques when they were preparing human
meat.

276
00:17:00,720 --> 00:17:02,790
and you've got an explanatory story that's

277
00:17:02,790 --> 00:17:05,700
much more specific than a mere analogy,
but

278
00:17:05,700 --> 00:17:09,240
it also commits you to a lot, so it might
be questionable in various ways.

279
00:17:10,800 --> 00:17:14,610
So when you look at an argument like this,
you've got to decide which way to

280
00:17:14,610 --> 00:17:17,860
reconstruct the argument, as an argument
from analogy

281
00:17:17,860 --> 00:17:20,200
or as an inference to the best
explanation.

282
00:17:20,200 --> 00:17:22,910
And the general rule is one that we saw
Long

283
00:17:22,910 --> 00:17:25,390
ago in early weeks of this course.

284
00:17:25,390 --> 00:17:28,710
If you really want to understand an
argument, you

285
00:17:28,710 --> 00:17:31,330
want to understand your opponents or you
want to have

286
00:17:31,330 --> 00:17:33,190
a better argument for yourself, then you
try

287
00:17:33,190 --> 00:17:35,330
to make the argument look as good as
possible.

288
00:17:36,440 --> 00:17:38,930
So when you face a particular example like
the neanderthal

289
00:17:38,930 --> 00:17:42,610
example, you have to decide is this
argument be, going to

290
00:17:42,610 --> 00:17:45,050
be better if I reconstruct it as an
argument from

291
00:17:45,050 --> 00:17:48,370
analogy Or is it going to be better if I
reconstruct

292
00:17:48,370 --> 00:17:51,350
it as an inference to the best explanation
and the best

293
00:17:51,350 --> 00:17:55,560
reconstruction is going to be the one that
makes the argument look best.

