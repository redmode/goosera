1
00:00:00,243 --> 00:00:03,119
[BLANK_AUDIO].

2
00:00:03,119 --> 00:00:08,130
We've learned that inferences to the best
explanation are inductive arguments.

3
00:00:08,130 --> 00:00:11,170
They're not valid, and they are
defeasible.

4
00:00:12,260 --> 00:00:16,220
But still, they might be strong and good
arguments.

5
00:00:16,220 --> 00:00:19,969
They might give you good reasons to
believe their conclusions.

6
00:00:19,969 --> 00:00:23,151
Great, they might be, sure, but how do you

7
00:00:23,151 --> 00:00:28,570
tell whether they really are strong
arguments and good arguments?

8
00:00:28,570 --> 00:00:32,096
We need standards to determine when an
inference

9
00:00:32,096 --> 00:00:35,330
to the best explanation is a good
argument.

10
00:00:36,860 --> 00:00:42,564
And to understand those standards,

11
00:00:42,564 --> 00:00:48,084
we're going to view another skit made

12
00:00:48,084 --> 00:00:52,899
by my students a few years ago.

13
00:00:52,899 --> 00:00:54,099
>> It's

14
00:00:54,099 --> 00:00:57,475
time for class.

15
00:00:57,475 --> 00:00:57,476
[CROSSTALK]

16
00:00:57,476 --> 00:01:03,780
>> Whoa, where is everybody?
>> Beats me.

17
00:01:03,780 --> 00:01:04,695
It's Monday, isn't it?

18
00:01:04,695 --> 00:01:08,396
>> Yeah, first day of the week, remember
the weekend?

19
00:01:08,396 --> 00:01:15,722
>> Hmmm, the weekend.

20
00:01:15,722 --> 00:01:16,162
[NOISE]

21
00:01:16,162 --> 00:01:20,920
Yeah, I remember, vaguely.
Good times.

22
00:01:20,920 --> 00:01:23,340
Now, it's back to work.

23
00:01:25,410 --> 00:01:30,140
It's Monday, but what time is it?
>> It's 11:15.

24
00:01:30,140 --> 00:01:32,990
>> Well, isn't that when class is
supposed to start?

25
00:01:32,990 --> 00:01:36,030
>> Here's the start of a good
explanation.

26
00:01:36,030 --> 00:01:39,017
There are surprising circumstances that
need to be explained.

27
00:01:39,017 --> 00:01:45,290
>> Yeah, maybe my watch is fast, it has
been giving me some trouble lately.

28
00:01:45,290 --> 00:01:46,254
Do you check yours?

29
00:01:46,254 --> 00:01:47,420
>> Oh, I don't wear a watch.

30
00:01:47,420 --> 00:01:48,955
I just ask you when I need to know the
time.

31
00:01:48,955 --> 00:01:50,879
>> Great, so I'm your designated
timekeeper.

32
00:01:50,879 --> 00:01:53,490
>> kind of, but there's usually a clock
around.

33
00:01:53,490 --> 00:01:53,935
One right there.

34
00:01:53,935 --> 00:01:57,815
>> Okay, so we know it's 11:15.
>> Right.

35
00:01:57,815 --> 00:02:00,503
>> In any inference to the best
explanation, it's crucial

36
00:02:00,503 --> 00:02:04,200
to get an accurate picture of what you're
trying to explain.

37
00:02:04,200 --> 00:02:08,028
In this case, if it were 11:10 instead of
11:15, then the

38
00:02:08,028 --> 00:02:12,450
explanation would be different, or there
might be nothing at all to explain.

39
00:02:14,300 --> 00:02:16,110
>> So where is everybody?

40
00:02:16,110 --> 00:02:18,830
>> Well, maybe everybody else is late.

41
00:02:18,830 --> 00:02:22,360
After all, the professor does show up a
few minutes late.

42
00:02:22,360 --> 00:02:24,752
He told me that he doesn't like to start
teaching right away,

43
00:02:24,752 --> 00:02:27,600
because he doesn't like when he's talking
and the students walk in.

44
00:02:27,600 --> 00:02:29,540
>> Oh, that makes me mad.

45
00:02:29,540 --> 00:02:32,722
>> So maybe, they're coming in later,
because they

46
00:02:32,722 --> 00:02:36,830
don't want to wait around for him to start
talking.

47
00:02:36,830 --> 00:02:38,715
It's one of those vicious circles.

48
00:02:38,715 --> 00:02:42,369
>> Okay, maybe that's

49
00:02:42,369 --> 00:02:49,271
it, but then some students should start

50
00:02:49,271 --> 00:02:55,970
showing up, so let's just wait and see

51
00:02:55,970 --> 00:02:57,430
[SOUND].

52
00:03:01,660 --> 00:03:02,450
Enough is enough.

53
00:03:03,460 --> 00:03:05,368
If people were coming they'd be here by
now.

54
00:03:05,368 --> 00:03:09,120
I mean the professor's been late but he's
never been this late.

55
00:03:09,120 --> 00:03:12,270
>> Ann refutes Bob's explanation, by
pointing out new facts

56
00:03:12,270 --> 00:03:16,598
that falsify Bob's hypothesis, that all
the students are late.

57
00:03:16,598 --> 00:03:19,028
A good explanation has to be compatible
with all of the

58
00:03:19,028 --> 00:03:22,190
facts, not just the particular ones that
it's trying to explain.

59
00:03:23,670 --> 00:03:26,730
Bond's hypothesis can't be the best
explanation of

60
00:03:26,730 --> 00:03:31,319
why the students aren't here at 11:15, if
there still not there by noon.

61
00:03:31,319 --> 00:03:36,444
>> Well that brings us right back to
where we started.

62
00:03:36,444 --> 00:03:39,444
Why isn't anybody else here?

63
00:03:39,444 --> 00:03:45,967
>> Your guess is as good as mine.
>> Well, well my guess is everybody else

64
00:03:45,967 --> 00:03:51,835
is here, we just can't see or hear them.
>> I bet they're laughing

65
00:03:51,835 --> 00:03:53,142
at us right now.
>>

66
00:03:53,142 --> 00:03:53,642
[LAUGH]

67
00:03:57,600 --> 00:03:59,000
>> I bet the professor is up there

68
00:03:59,000 --> 00:04:01,248
jabbering away right now like he always
does.

69
00:04:01,248 --> 00:04:01,248
>>

70
00:04:01,248 --> 00:04:05,955
[SOUND].

71
00:04:05,955 --> 00:04:12,630
>> Oh, I hope this material is not on
the test.

72
00:04:12,630 --> 00:04:17,532
>> Congratulations, you proved me wrong.
Your guess is not as good as mine.

73
00:04:17,532 --> 00:04:20,112
>> How?
>> Humans can't become invisible.

74
00:04:20,112 --> 00:04:21,144
>> How do you know?

75
00:04:21,144 --> 00:04:22,778
>> Have you ever seen it?
>> No.

76
00:04:22,778 --> 00:04:26,476
They're invisible, that's why I've never
seen it.

77
00:04:26,476 --> 00:04:27,934
>> Okay, but the laws of physics,

78
00:04:27,934 --> 00:04:30,440
don't allow humans to become silent and
invisible.

79
00:04:30,440 --> 00:04:31,172
>> And dismisses

80
00:04:31,172 --> 00:04:34,570
Bob's new hypothesis, because it's not
conservative.

81
00:04:34,570 --> 00:04:38,440
It conflicts with well-established, prior
beliefs in physics.

82
00:04:38,440 --> 00:04:41,980
If you start believing that humans can
become invisible, you'll have

83
00:04:41,980 --> 00:04:45,109
to give up everything you know about how
light and matter work.

84
00:04:46,290 --> 00:04:49,265
>> Besides if they were here the seats
wouldn't be popped up like this.

85
00:04:49,265 --> 00:04:56,747
>> Nope, because besides being invisible
and silent, these students

86
00:04:56,747 --> 00:05:02,250
are also weightless, they don't smell
either.

87
00:05:05,030 --> 00:05:08,095
>> Okay, so there is no way to tell
whether or not there're students here.

88
00:05:09,250 --> 00:05:10,105
>> You're catching on.

89
00:05:10,105 --> 00:05:11,905
>> But the laws of physics still don't

90
00:05:11,905 --> 00:05:14,825
allow people to be weightless, invisible
and silent.

91
00:05:14,825 --> 00:05:18,240
>> Well, the laws of physics are based
on observation.

92
00:05:18,240 --> 00:05:23,040
And there is no way these students can be
observed at all.

93
00:05:23,040 --> 00:05:25,016
The laws of physics can't apply to
something

94
00:05:25,016 --> 00:05:27,090
that can't be observed or at least
students.

95
00:05:27,090 --> 00:05:30,192
>> So then nothing could possibly tell
whether or not there

96
00:05:30,192 --> 00:05:33,723
are students in these chairs.
>> Exactly.

97
00:05:33,723 --> 00:05:35,410
>> Hey Ed.

98
00:05:35,410 --> 00:05:39,330
We're here live with an unidentifiable,
unobservable student.

99
00:05:39,330 --> 00:05:40,865
What's it like to be unobservable?

100
00:05:40,865 --> 00:05:43,520
Okay.

101
00:05:43,520 --> 00:05:46,260
Back to you Ed.
>> Pretty sweet huh?

102
00:05:46,260 --> 00:05:48,140
>> No, it is not sweet.

103
00:05:48,140 --> 00:05:53,140
The only reason why your idea isn't
falsified is because it's not falsifiable!

104
00:05:53,140 --> 00:05:55,980
If you can't prove it then it's useless.

105
00:05:55,980 --> 00:05:59,690
I mean, you might as well say there's
invisible elephants floating around.

106
00:05:59,690 --> 00:06:00,190
[NOISE]

107
00:06:01,285 --> 00:06:06,710
>> Ann rejects Bob's new explanation on
the grounds that it's not falsifiable.

108
00:06:06,710 --> 00:06:10,614
It's compatible with anything that could
possibly happen, but that means

109
00:06:10,614 --> 00:06:14,420
that it can't explain why one thing
happens rather than another.

110
00:06:14,420 --> 00:06:15,770
It's completely empty.

111
00:06:15,770 --> 00:06:16,940
[NOISE]

112
00:06:16,940 --> 00:06:22,790
>> I was just joking.
This is a philosophy class after all.

113
00:06:22,790 --> 00:06:24,100
Okay, I don't mind jokes.

114
00:06:24,100 --> 00:06:27,280
But let's get serious, because it's not
helping us with our problem.

115
00:06:27,280 --> 00:06:28,395
Why isn't anybody here?

116
00:06:28,395 --> 00:06:34,170
>> Speaking of joking, maybe they're all
playing a trick on us.

117
00:06:34,170 --> 00:06:36,600
Maybe they decided not to show up.

118
00:06:36,600 --> 00:06:37,100
[LAUGH]

119
00:06:41,640 --> 00:06:45,296
>> Okay, but why would they do that?
Is it April Fool's Day?

120
00:06:45,296 --> 00:06:46,292
>> No.

121
00:06:46,292 --> 00:06:49,290
It's January.
>> Oh.

122
00:06:49,290 --> 00:06:51,910
Well, then I don't know why they're
tricking us.

123
00:06:51,910 --> 00:06:53,880
They just are.
>> Okay.

124
00:06:53,880 --> 00:06:56,745
I can't believe that unless you can tell
me why they would be so silly.

125
00:06:56,745 --> 00:07:00,399
>> Ann is criticizing Bob's new
explanation because it's not deep.

126
00:07:00,399 --> 00:07:06,769
It's shallow, because it depends on a
principle that is not self-explaining and

127
00:07:06,769 --> 00:07:11,840
needs to be explained.
I don't know, I guess the professor just

128
00:07:11,840 --> 00:07:16,911
decided to cancel class because he didn't
feel like teaching today.

129
00:07:16,911 --> 00:07:16,912
[SOUND].

130
00:07:16,912 --> 00:07:22,191
>> Maybe, but then other students
wouldn't know, so they would be here.

131
00:07:22,191 --> 00:07:25,730
>> Unless, he e-mailed the entire class.

132
00:07:25,730 --> 00:07:26,570
Then they would know.

133
00:07:26,570 --> 00:07:31,430
Did you check your e-mail recently?
>> About an hour ago.

134
00:07:31,430 --> 00:07:34,100
>> Then maybe he e-mailed us in the past
hour.

135
00:07:34,100 --> 00:07:34,770
>> I guess.

136
00:07:34,770 --> 00:07:37,810
I mean, that would explain why nobody's
here.

137
00:07:38,820 --> 00:07:40,860
Some professors do cancel class at the
last minute.

138
00:07:40,860 --> 00:07:42,350
But not him.

139
00:07:43,490 --> 00:07:44,360
He's too strict.

140
00:07:44,360 --> 00:07:48,800
I, I've had five classes with him and he's
never cancelled class at the last minute.

141
00:07:48,800 --> 00:07:51,014
>> If you miss even one class in this

142
00:07:51,014 --> 00:07:55,380
course you, you, will, will, fail, fail,
fail, fail.

143
00:07:55,380 --> 00:07:57,535
>> So, maybe this time it's different.

144
00:07:57,535 --> 00:07:59,740
>> Yeah.
Maybe the sun didn't rise.

145
00:07:59,740 --> 00:08:01,385
Either way it's totally out of character.

146
00:08:01,385 --> 00:08:01,386
[SOUND]

147
00:08:01,386 --> 00:08:05,430
>> Ann's point is that Bob's new
explanation is Ad Hoc.

148
00:08:05,430 --> 00:08:08,730
It applies only to the very circumstances
that it was

149
00:08:08,730 --> 00:08:12,560
invoked to explain, and it doesn't apply
to other cases.

150
00:08:12,560 --> 00:08:21,840
That means it lacks power and breadth.
>> Okay, but, I've got it!

151
00:08:21,840 --> 00:08:24,818
Classes must have been cancelled for some
reason.

152
00:08:24,818 --> 00:08:25,864
What reason?

153
00:08:25,864 --> 00:08:26,890
>> maybe

154
00:08:26,890 --> 00:08:30,632
it's a holiday.
>> Okay, what holiday?

155
00:08:30,632 --> 00:08:33,121
>> I, Martin Luther King.

156
00:08:33,121 --> 00:08:37,500
That's it.
It's Marting Luther King day.

157
00:08:37,500 --> 00:08:44,273
>> That would explain it, I mean nobody
goes to class but a fool on a holiday.

158
00:08:44,273 --> 00:08:45,015
>> Hi, I'm here.

159
00:08:45,015 --> 00:08:47,589
>> I guess that makes us both fools

160
00:08:47,589 --> 00:08:52,048
>> Yeah, so I guess that's why he didn't
send a note, because he

161
00:08:52,048 --> 00:08:57,489
figured no one would come here.
I bet he wrote it down in the syllabus.

162
00:08:57,489 --> 00:09:03,525
It also explains why there's no one's in
the whole classroom building.

163
00:09:03,525 --> 00:09:04,709
>> Bob's new explanation works because
it has

164
00:09:04,709 --> 00:09:06,250
all of the virtues that the other
explanations lack.

165
00:09:06,250 --> 00:09:08,986
>> It's powerful, broad, deep, simple,

166
00:09:08,986 --> 00:09:13,820
conservative, and not falsified, and yet,
falsifiable.

167
00:09:13,820 --> 00:09:17,996
Those virtues make his explanation good or
in his words, sweet.

168
00:09:17,996 --> 00:09:21,208
>> You're right.

169
00:09:21,208 --> 00:09:24,258
You know, I think there's some kind of
ceremony today with

170
00:09:24,258 --> 00:09:28,180
the president of the college and some big
hotshot from Washington.

171
00:09:28,180 --> 00:09:29,890
For Martin Luther King Day.

172
00:09:29,890 --> 00:09:32,660
So probably that's where the whole class
is.

173
00:09:32,660 --> 00:09:33,695
That's why they're not there.

174
00:09:33,695 --> 00:09:35,525
>> Maybe, but even if there weren't a

175
00:09:35,525 --> 00:09:38,087
ceremony, the fact that it's Martin Luther
King

176
00:09:38,087 --> 00:09:40,161
Day would explain why nobody is here, so

177
00:09:40,161 --> 00:09:43,450
you don't really need to add anything
else.

178
00:09:43,450 --> 00:09:46,349
>> Anne rejects Bob's newest explanation
because it's not modest.

179
00:09:46,349 --> 00:09:49,387
It commits him to more matters of fact

180
00:09:49,387 --> 00:09:53,823
than are needed to explain the
observations at hand.

181
00:09:53,823 --> 00:09:56,954
>> So I guess the holiday explains it
all.

182
00:09:56,954 --> 00:10:00,358
>> Well, not really, we still don't know
why

183
00:10:00,358 --> 00:10:03,721
that big head keeps popping in on the
side.

184
00:10:03,721 --> 00:10:07,357
>> That's one that I can't figure out
myself.

185
00:10:07,357 --> 00:10:08,760
>> It's so weird.

186
00:10:08,760 --> 00:10:08,809
>> I

187
00:10:08,809 --> 00:10:09,250
[INAUDIBLE]

188
00:10:09,250 --> 00:10:11,420
used to babysit me when I was younger.

189
00:10:11,420 --> 00:10:14,774
>> This whole exchange can be seen as a
single argument

190
00:10:14,774 --> 00:10:19,760
that takes the form of a long inference to
the best explanation.

191
00:10:19,760 --> 00:10:22,860
The first premise is an observation.

192
00:10:22,860 --> 00:10:28,760
Nobody else is in the room.
The second premise is an explanation.

193
00:10:28,760 --> 00:10:31,466
The hypothesis that class was cancelled
because

194
00:10:31,466 --> 00:10:34,502
of the holiday, plus accepted facts and
principles,

195
00:10:34,502 --> 00:10:38,198
gives a suitably strong explanation of the
observation in premise

196
00:10:38,198 --> 00:10:41,870
one, namely, the observation that nobody
else is in the room.

197
00:10:43,460 --> 00:10:48,295
Premise three is a comparison.
No other hypothesis provides

198
00:10:48,295 --> 00:10:52,950
an explanation nearly as good as the
holiday hypothesis in premise two.

199
00:10:52,950 --> 00:11:00,140
And the conclusion is that the holiday
hypothesis in premise two is true.

200
00:11:00,140 --> 00:11:04,310
The most controversial premise is probably
premise three.

201
00:11:04,310 --> 00:11:07,250
It says that no other explanation provides
nearly

202
00:11:07,250 --> 00:11:10,310
as good an explanation as the holiday
hypothesis.

203
00:11:11,320 --> 00:11:15,900
To justify this premise, we need to
compare other possible explanations.

204
00:11:15,900 --> 00:11:19,300
And that's exactly what the two students
do throughout the skit.

205
00:11:20,750 --> 00:11:25,850
Their discussion can then be summarized in
this background

206
00:11:25,850 --> 00:11:29,370
argument that supports premise three.

207
00:11:29,370 --> 00:11:31,710
The hypothesis that the other students are
late is falsified by the passing of time.

208
00:11:31,710 --> 00:11:35,470
The patients in the forecastle were also
getting better.

209
00:11:35,470 --> 00:11:40,120
The hypothesis that the other students are
invisible is not conservative.

210
00:11:40,120 --> 00:11:44,960
The hypothesis that the other students are
undetectable is not falsifiable.

211
00:11:44,960 --> 00:11:48,730
The hypothesis that the other students are
playing a joke is not deep.

212
00:11:49,800 --> 00:11:50,922
The hypothesis that

213
00:11:50,922 --> 00:11:53,970
the professor skipped class is not
powerful or broad.

214
00:11:55,310 --> 00:11:59,900
The hypothesis that the other students are
at a ceremony is not modest.

215
00:12:00,970 --> 00:12:03,140
No other hypothesis seems plausible.

216
00:12:03,140 --> 00:12:07,645
Therefore no other hypothesis provides an
explanation nearly

217
00:12:07,645 --> 00:12:11,144
as good as the holiday hypothesis in
premise 2.

218
00:12:11,144 --> 00:12:16,016
Even if the other explanations are
inadequate, the inference of the

219
00:12:16,016 --> 00:12:20,132
best explanation fails unless the
hypothesis succeeds at

220
00:12:20,132 --> 00:12:23,759
explaining why there was nobody else in
the room.

221
00:12:23,759 --> 00:12:27,455
Premise two claims that the holiday
hypothesis does

222
00:12:27,455 --> 00:12:30,806
explain why there's nobody else in the
room.

223
00:12:30,806 --> 00:12:33,606
So we need to analyze why it succeeds, and

224
00:12:33,606 --> 00:12:37,505
to do that, we can look at another
background argument.

225
00:12:37,505 --> 00:12:41,113
The holiday hypothesis explains why nobody
else

226
00:12:41,113 --> 00:12:41,940
is there.

227
00:12:41,940 --> 00:12:44,436
Because, if classes were canceled because
of

228
00:12:44,436 --> 00:12:46,870
a holiday, then nobody else would be
there.

229
00:12:46,870 --> 00:12:49,900
That is, nobody else would be in the room
at the usual time.

230
00:12:51,940 --> 00:12:56,149
The holiday hypothesis is also broad,
because it explains other actual

231
00:12:56,149 --> 00:13:00,579
observations, such as the observation that
the whole building is empty.

232
00:13:02,770 --> 00:13:09,040
The holiday hypothesis is also powerful,
because it applies to many separate cases.

233
00:13:09,040 --> 00:13:14,834
For example, it explains why students
won't be there on future holidays.

234
00:13:14,834 --> 00:13:18,799
The holiday hypothesis is falsifiable,
because the two students might

235
00:13:18,799 --> 00:13:22,010
find out that classes were not cancelled
for the holiday.

236
00:13:23,040 --> 00:13:28,290
The holiday hypothesis is not falsified,
because the two students do not actually

237
00:13:28,290 --> 00:13:31,215
find out that classes were not cancelled,
and

238
00:13:31,215 --> 00:13:34,620
similarly for other ways to falsify the
hypothesis.

239
00:13:36,970 --> 00:13:40,246
The hypothesis is also conservative,
because it

240
00:13:40,246 --> 00:13:44,040
does not conflict with any
well-established beliefs.

241
00:13:45,920 --> 00:13:49,712
The holiday hypothesis is also deep,
because it does not

242
00:13:49,712 --> 00:13:55,470
depend on any assumptions that need but
lack independent explanation.

243
00:13:55,470 --> 00:14:01,197
So, the holiday hypothesis explains why
nobody else is there, and it's broad,

244
00:14:01,197 --> 00:14:06,294
powerful, falsifiable, but not falsified,
conservative, deep.

245
00:14:06,294 --> 00:14:11,586
Therefore, the holiday hypothesis plus
accepted facts and principles,

246
00:14:11,586 --> 00:14:17,280
gives a suitably strong explanation of why
nobody else is in the room.

247
00:14:17,280 --> 00:14:19,781
These two background arguments use a
common list

248
00:14:19,781 --> 00:14:23,570
of virtues that are usually called
explanatory virtues.

249
00:14:23,570 --> 00:14:26,335
In general, one explanation is better or

250
00:14:26,335 --> 00:14:30,340
stronger than another if it has more of
these virtues.

251
00:14:30,340 --> 00:14:33,008
When you understand these explanatory
virtues, then

252
00:14:33,008 --> 00:14:35,725
you've mastered inference to the best
explanation.

253
00:14:35,725 --> 00:14:37,230
Right?

254
00:14:37,230 --> 00:14:38,690
>> Right.

255
00:14:38,690 --> 00:14:42,750
>> Now we can go on and look at other
kinds of inductive argument.

