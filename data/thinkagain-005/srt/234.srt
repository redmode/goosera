1
00:00:02,790 --> 00:00:06,201
As I said in the first lecture, arguments
are used for many purposes.

2
00:00:06,201 --> 00:00:09,524
And in the lecture just before this one,
we saw that it

3
00:00:09,524 --> 00:00:14,110
can be used for persuasion and also for
justification of various sorts.

4
00:00:15,250 --> 00:00:16,890
But persuasion and justification are not
the

5
00:00:16,890 --> 00:00:20,990
only purposes for which arguments are
given.

6
00:00:20,990 --> 00:00:24,770
Arguments are also given in order to
explain things.

7
00:00:24,770 --> 00:00:28,449
So we're going to spend this lecture
talking about explanation.

8
00:00:28,449 --> 00:00:32,060
Indeed, what we're going to try to do is
to explain explanation.

9
00:00:33,220 --> 00:00:35,250
We explain things all the time and I may
give an

10
00:00:35,250 --> 00:00:39,130
example that'll probably really tick off
my co-teacher Ram because he's

11
00:00:39,130 --> 00:00:43,105
from Chapel Hill, but my example is
somebody might ask why

12
00:00:43,105 --> 00:00:48,220
did the Duke basketball team win the
National Championship in 2010.

13
00:00:48,220 --> 00:00:53,510
The answer might be, they had great
players and a great coach and of

14
00:00:53,510 --> 00:00:55,510
course, they got lucky.

15
00:00:55,510 --> 00:00:58,590
you have to have some luck to win a
National Championship.

16
00:00:58,590 --> 00:01:06,998
But in any case, an explanation of that
event is a reason why it happened.

17
00:01:06,998 --> 00:01:09,430
so to explain something is to give a
reason why

18
00:01:09,430 --> 00:01:12,460
it happened or to answer a question about
why it happened.

19
00:01:13,700 --> 00:01:17,320
Notice that when you explain something,
you assume that it's true.

20
00:01:17,320 --> 00:01:18,570
It wouldn't make any sense to

21
00:01:18,570 --> 00:01:21,900
ask why did Duke win the National
Championship in 2011, because they didn't.

22
00:01:21,900 --> 00:01:28,340
You can only ask why this thing happened
if it happened.

23
00:01:28,340 --> 00:01:29,800
So we've already got one difference

24
00:01:29,800 --> 00:01:33,810
between explanation and justification and
persuasion.

25
00:01:33,810 --> 00:01:36,583
When you try to persuade someone to
believe something.

26
00:01:36,583 --> 00:01:38,680
That thing doesn't have to be true, and

27
00:01:38,680 --> 00:01:40,652
they don't have to of believed it in
advance.

28
00:01:40,652 --> 00:01:43,630
When you justify something,

29
00:01:43,630 --> 00:01:43,910
right?

30
00:01:43,910 --> 00:01:47,450
You can justify belief in something that
they don't already believe.

31
00:01:48,630 --> 00:01:50,875
But when you explain something, both the

32
00:01:50,875 --> 00:01:54,160
arguer and the audience are assuming that,
that

33
00:01:54,160 --> 00:01:56,910
thing happened, that the conclusion is
true, and

34
00:01:56,910 --> 00:01:58,954
they're looking for the reasons why it
happened.

35
00:01:59,960 --> 00:02:03,180
So if the goal of explanation is not to
persuade or to justify and the

36
00:02:03,180 --> 00:02:06,130
arguer and the, and the audience both

37
00:02:06,130 --> 00:02:08,815
already believe the conclusion then what's
the point?

38
00:02:08,815 --> 00:02:10,970
What's the goal of explanation?

39
00:02:10,970 --> 00:02:16,660
The goal of explanation is to increase
understanding, not to

40
00:02:16,660 --> 00:02:21,040
convince us that the conclusion is true,
but to help us understand why it's true.

41
00:02:22,280 --> 00:02:24,470
And we can do that in a number of
different ways.

42
00:02:24,470 --> 00:02:28,840
There're actually, according to Aristotle,
four different

43
00:02:28,840 --> 00:02:30,740
types of causes, he would call them.

44
00:02:30,740 --> 00:02:32,306
But we would probably call them
explanations.

45
00:02:32,306 --> 00:02:33,674
The first,

46
00:02:33,674 --> 00:02:39,360
were, he called efficient causation.
We'll just call it causal explanation.

47
00:02:39,360 --> 00:02:43,460
And that tells you why something happened.
Why did the bridge collapse?

48
00:02:43,460 --> 00:02:46,630
Because there was an earthquake.
That explains why the bridge collapsed.

49
00:02:48,120 --> 00:02:54,771
The second type of explanation, he called
teleological, or purposeful explanation,

50
00:02:54,771 --> 00:02:57,780
because it's looking at the purpose or the
telos, or the goal,

51
00:02:58,820 --> 00:03:03,640
why did Joe go to the grocery store?
To buy milk.

52
00:03:03,640 --> 00:03:08,085
His goal of buying milk is what explains
why he went to the grocery store.

53
00:03:08,085 --> 00:03:12,535
Third type of explanation is

54
00:03:12,535 --> 00:03:19,530
formal.
Why does this peg not fit into this hole?

55
00:03:19,530 --> 00:03:24,860
And the answer is, because the peg is
square and the hole is round.

56
00:03:24,860 --> 00:03:26,500
That's why it doesn't fit in the hole and
that

57
00:03:26,500 --> 00:03:28,895
explains it, that helps you understand why
it didn't fit.

58
00:03:28,895 --> 00:03:34,140
The fourth kind of explanation is
material.

59
00:03:34,140 --> 00:03:39,100
Why is this golf club so light?
Why does it weigh so little?

60
00:03:39,100 --> 00:03:42,060
The answer might be because it's made out
of graphite.

61
00:03:42,060 --> 00:03:47,010
That helps you understand why the weight
is so low on this golf club.

62
00:03:47,010 --> 00:03:49,240
it would be a lot heavier if it were made
out of steel.

63
00:03:50,930 --> 00:03:53,330
So we get four different types of
explanation.

64
00:03:53,330 --> 00:03:55,020
We can have causal.

65
00:03:55,020 --> 00:03:56,930
We can have teleological.

66
00:03:56,930 --> 00:04:00,610
We can have formal and we can have
material.

67
00:04:00,610 --> 00:04:02,950
And all of those different types of
explanation are

68
00:04:02,950 --> 00:04:06,740
aimed at helping us to understand why
something happened.

69
00:04:06,740 --> 00:04:07,240
[SOUND]

70
00:04:10,740 --> 00:04:12,000
So did you hear that train whistle?

71
00:04:12,000 --> 00:04:16,190
We want to ask, why does the train emit
such a loud noise?

72
00:04:16,190 --> 00:04:20,772
Well one answer might be, that what causes
it to make that noise

73
00:04:20,772 --> 00:04:24,920
is that the conductor pull a lever on the
train, which creates that noise.

74
00:04:24,920 --> 00:04:26,398
That would be a causal explanation.

75
00:04:27,420 --> 00:04:30,945
Another explanation might be the
teleological explanation.

76
00:04:30,945 --> 00:04:35,570
The train was crossing an intersection
with cars,

77
00:04:35,570 --> 00:04:37,409
and wanted the cars to know that they're
coming.

78
00:04:38,490 --> 00:04:41,510
Another explanation might be a formal
explanation,

79
00:04:41,510 --> 00:04:43,610
because the whistle on the top of the

80
00:04:43,610 --> 00:04:48,992
train has a certain shape that makes the
air come out with a certain vibration.

81
00:04:48,992 --> 00:04:55,830
And a final explanation might be a
material explanation, because air has

82
00:04:55,830 --> 00:05:01,010
a certain density and a certain material
that makes it create that kind of sound.

83
00:05:01,010 --> 00:05:05,420
So we can give all four types of
explanations for the same event.

84
00:05:06,570 --> 00:05:08,170
Here's another example.

85
00:05:08,170 --> 00:05:12,550
We can apply all four types of explanation
to a single event.

86
00:05:12,550 --> 00:05:14,930
Joe jumped out of an airplane.

87
00:05:14,930 --> 00:05:16,452
That's what caused him to fall.

88
00:05:16,452 --> 00:05:20,770
But then, why did he jump out of the
airplane to get excitement?

89
00:05:20,770 --> 00:05:23,140
Why did he fall so fast?

90
00:05:23,140 --> 00:05:26,110
Because of his shape, it was aerodynamic,
and because

91
00:05:26,110 --> 00:05:28,390
of the material that he was made out of,
heavy

92
00:05:28,390 --> 00:05:32,140
flesh, which was a lot denser than the
surrounding air.

93
00:05:32,140 --> 00:05:35,470
So all four of those factors go into an
explanation

94
00:05:35,470 --> 00:05:38,920
of why Joe fell when he jumped out of the
airplane.

95
00:05:40,370 --> 00:05:42,830
So next we need to talk about the forms of
explanation.

96
00:05:42,830 --> 00:05:45,595
You can actually give explanations in, in
several different forms.

97
00:05:45,595 --> 00:05:50,380
For example if somebody said, oh, why did
you move to Duke?

98
00:05:50,380 --> 00:05:51,600
I might tell a story

99
00:05:51,600 --> 00:05:53,990
about things that happened before I moved
to Duke

100
00:05:53,990 --> 00:05:55,960
that led me to want to move to Duke

101
00:05:55,960 --> 00:06:00,330
and I could talk about moving to Duke and,
and all the nice people here and so on.

102
00:06:00,330 --> 00:06:04,690
You can give explanations in the form of
narratives like that.

103
00:06:04,690 --> 00:06:07,120
But notice that, that's not going to imply
that

104
00:06:07,120 --> 00:06:10,000
everybody in similar circumstances is
going to behave in

105
00:06:10,000 --> 00:06:11,630
exactly the same way so you're not going
to

106
00:06:11,630 --> 00:06:16,730
get general principles out of those type
of narrative explanations.

107
00:06:16,730 --> 00:06:19,310
In other explanations are given in the
forms of arguments

108
00:06:19,310 --> 00:06:21,742
and that's the kind we're going to be
interested in here.

109
00:06:21,742 --> 00:06:27,080
The form in which explanations occur in
arguments is really pretty simple.

110
00:06:27,080 --> 00:06:30,240
One premise usually states some kind of
general principle

111
00:06:30,240 --> 00:06:33,280
that can apply to a lot of different
situations.

112
00:06:33,280 --> 00:06:37,430
And then the second premise talks about
the current situation

113
00:06:37,430 --> 00:06:41,770
and says that those types of features that
the principle mentions

114
00:06:41,770 --> 00:06:44,380
are instantiated in this case.

115
00:06:44,380 --> 00:06:46,560
And then the conclusions says that
explains

116
00:06:46,560 --> 00:06:48,960
why it happened this way, in this case.

117
00:06:50,370 --> 00:06:51,050
For example.

118
00:06:51,050 --> 00:06:54,890
If we want to know why objects fall,
right?

119
00:06:54,890 --> 00:06:55,648
So there's a book.

120
00:06:55,648 --> 00:06:57,346
And it

121
00:06:57,346 --> 00:06:57,354
[SOUND]

122
00:06:57,354 --> 00:07:00,220
falls.
We want to explain that.

123
00:07:00,220 --> 00:07:03,190
We need to cite a general principle.

124
00:07:03,190 --> 00:07:08,270
But notice that not all objects fall.
Some objects actually rise.

125
00:07:08,270 --> 00:07:10,410
Helium balloons rise.

126
00:07:10,410 --> 00:07:12,150
So we need a principle that's going to
explain

127
00:07:12,150 --> 00:07:16,550
why some objects fall and other objects
rise.

128
00:07:16,550 --> 00:07:21,730
Then we'll understand why helium balloons
rise, just to stick with that example.

129
00:07:21,730 --> 00:07:22,630
And the answer is that,

130
00:07:22,630 --> 00:07:27,676
when an object is suspended freely in a
medium, where

131
00:07:27,676 --> 00:07:33,269
the medium is more dense than the object,
then it rises.

132
00:07:33,269 --> 00:07:36,210
And when an object is suspended freely in
a medium, where

133
00:07:36,210 --> 00:07:39,518
the object is more dense than the medium,
then it falls.

134
00:07:39,518 --> 00:07:43,210
So you can explain why helium balloons
rise by having

135
00:07:43,210 --> 00:07:47,998
as your first premise, whenever an object
is sus, freely suspended

136
00:07:47,998 --> 00:07:50,815
in a medium, like a gas or a liquid.

137
00:07:50,815 --> 00:07:56,620
And the medium is more dense than the
object, then the object rises.

138
00:07:56,620 --> 00:08:00,500
Now let's talk about the circumstances in
this particular case.

139
00:08:00,500 --> 00:08:03,650
The helium balloon is less dense than the

140
00:08:03,650 --> 00:08:08,820
air that surrounds it, therefore, the
helium balloon rises.

141
00:08:08,820 --> 00:08:11,320
And that explains why the helium balloon
rises.

142
00:08:11,320 --> 00:08:13,030
And you can see how you give another

143
00:08:13,030 --> 00:08:16,790
argument to explain why the book fell.

144
00:08:16,790 --> 00:08:21,680
Now, this form of argument gives some
people the impression that

145
00:08:21,680 --> 00:08:26,110
any generalization could be used for
explanation, but that's not quite right.

146
00:08:26,110 --> 00:08:32,140
What example is Bode's law?
Bode's law says that 0.4 plus 0.3 times 2

147
00:08:32,140 --> 00:08:38,800
to the n can be used to predict all the
distances between planets and

148
00:08:38,800 --> 00:08:42,790
the sun, where n is the number of the
planet.

149
00:08:42,790 --> 00:08:45,424
So if n is Venus, then n is 0.

150
00:08:45,424 --> 00:08:49,140
Earth is 1, Mars is 2, and so on.

151
00:08:49,140 --> 00:08:52,240
this law was actually used to predict both
the

152
00:08:52,240 --> 00:08:58,350
largest asteroid in the asteroid belt,
Ceres, and also Uranus.

153
00:08:58,350 --> 00:09:00,930
So, this law is a generalization that held
for

154
00:09:00,930 --> 00:09:05,000
all the planets that they knew in Bode's
day and

155
00:09:05,000 --> 00:09:08,890
also used to predict new observations of
planets.

156
00:09:08,890 --> 00:09:09,620
Pretty cool!

157
00:09:09,620 --> 00:09:13,630
It, it actually turns out to fail when you
get to other planets.

158
00:09:13,630 --> 00:09:15,505
In, including Neptune and Pluto.

159
00:09:15,505 --> 00:09:20,593
But, still, it worked pretty well for the
data that they had.

160
00:09:20,593 --> 00:09:24,410
But, nobody thought that this law
explained why

161
00:09:24,410 --> 00:09:26,570
the planets were that far from the sun.

162
00:09:26,570 --> 00:09:30,520
They happen to fall in that pattern.
It could be used to predict.

163
00:09:30,520 --> 00:09:33,680
But it didn't explain why they were, the

164
00:09:33,680 --> 00:09:36,240
distance that they actually were, from the
sun.

165
00:09:37,710 --> 00:09:43,810
So Bode's law, is an example, where you
can get a prediction, without explanation.

166
00:09:43,810 --> 00:09:45,395
Now let's look at the reverse.

167
00:09:45,395 --> 00:09:50,587
And example where you have explanation.
Without prediction.

168
00:09:50,587 --> 00:09:55,185
Just imagine that a woman is HIV positive,

169
00:09:55,185 --> 00:10:01,202
and she get's pregnant and has a baby, and
the baby is also HIV

170
00:10:01,202 --> 00:10:07,090
positive.
How can the kid become HIV positive?

171
00:10:07,090 --> 00:10:09,270
Well the explanation is that the mother

172
00:10:09,270 --> 00:10:13,550
was HIV positive and they were sharing
blood.

173
00:10:13,550 --> 00:10:16,820
But notice that you can not predict from
the

174
00:10:16,820 --> 00:10:20,340
fact that the mother is HIV positive that
the child

175
00:10:20,340 --> 00:10:25,770
will be HIV positive because less than
half of the children born to mothers who

176
00:10:25,770 --> 00:10:31,340
are HIV positive are themselves HIV
positive.

177
00:10:31,340 --> 00:10:34,540
You also cannot use the fact that the
mother is HIV

178
00:10:34,540 --> 00:10:40,088
positive, to justify the claim that the
child is HIV positive.

179
00:10:40,088 --> 00:10:43,680
If you want to know whether the child's
HIV positive, you need to check its blood.

180
00:10:45,580 --> 00:10:50,060
So in this example, we have an explanation
of why

181
00:10:50,060 --> 00:10:54,180
the child is HIV positive when it is, but
we don't

182
00:10:54,180 --> 00:10:57,810
get any prediction that it will be HIV
positive and

183
00:10:57,810 --> 00:11:02,680
we don't justify the belief that the child
is HIV positive.

184
00:11:02,680 --> 00:11:07,670
So you can have explanation without
prediction and without justification.

185
00:11:07,670 --> 00:11:10,770
So then more positively, what is the goal
of explanation?

186
00:11:10,770 --> 00:11:15,030
Well the goal of explanation is to fit
this particular phenomenon

187
00:11:15,030 --> 00:11:20,250
into a general pattern, and that's what
all of these explanations do.

188
00:11:20,250 --> 00:11:23,480
Why do you want to fit them into a general
pattern?

189
00:11:23,480 --> 00:11:26,220
It's simply to increase your
understanding.

190
00:11:26,220 --> 00:11:28,140
Of why they came about.

191
00:11:28,140 --> 00:11:32,930
They came about because they fit into this
particular type of pattern.

192
00:11:32,930 --> 00:11:35,800
And this kind of understanding of fitting
them into a well known

193
00:11:35,800 --> 00:11:41,030
pattern is useful, because most of the
things that we want to explain are

194
00:11:41,030 --> 00:11:46,150
kind of weird, unusual, bewildering,
suprising phenomenon.

195
00:11:46,150 --> 00:11:49,250
That's when you need an explanation,
because fitting into the

196
00:11:49,250 --> 00:11:52,720
pattern makes it a little less
bewildering, a little less surprising,

197
00:11:52,720 --> 00:11:55,240
because it shows that it's kind of like
other things that

198
00:11:55,240 --> 00:11:59,950
have happened before, and that's what
Bode's law does not do.

199
00:11:59,950 --> 00:12:01,040
Because Bode's law,

200
00:12:01,040 --> 00:12:05,190
although it holds for all the planets that
have been observed in the day of Bode.

201
00:12:05,190 --> 00:12:07,830
It doesn't explain anything else, it
doesn't fit into

202
00:12:07,830 --> 00:12:12,260
a larger pattern with other planets around
other solar systems.

203
00:12:12,260 --> 00:12:15,320
And now that we've discovered planets, we
found many planets around

204
00:12:15,320 --> 00:12:19,870
other stars that don't seem to follow
Bode's law at all.

205
00:12:19,870 --> 00:12:23,200
So it doesn't fit our solar system into a
general

206
00:12:23,200 --> 00:12:26,330
pattern, and that's why even though it's a
generalization, and

207
00:12:26,330 --> 00:12:29,870
was used to predict other planets, it does
not provide an

208
00:12:29,870 --> 00:12:35,040
explanation of why the planets are certain
distances from the sun.

209
00:12:35,040 --> 00:12:36,650
So now we've learned a little bit about

210
00:12:36,650 --> 00:12:40,750
what explanation is, and what explanation
is not.

211
00:12:40,750 --> 00:12:46,300
Explanation is, an attempt to fit a
particular phenomenon into a general

212
00:12:46,300 --> 00:12:51,450
pattern in order to increase our
understanding of why it happened and

213
00:12:51,450 --> 00:12:56,503
to remove bewilderment or surprise.
Explanation is not

214
00:12:56,503 --> 00:13:02,330
persuasion, or justification, or
generalization, or prediction.

215
00:13:02,330 --> 00:13:04,490
Those are other uses of argument.

216
00:13:04,490 --> 00:13:06,440
So we've seen quite a variety of different

217
00:13:06,440 --> 00:13:10,530
uses of argument, but we've only scratched
the surface.

218
00:13:10,530 --> 00:13:13,940
There can be lots more and lots more to
say about each

219
00:13:13,940 --> 00:13:16,600
of these, so if you want to learn more
about these purposes

220
00:13:16,600 --> 00:13:19,380
of argument, a good place to start would

221
00:13:19,380 --> 00:13:24,010
be chapter one of the accompanying text,
Understanding Arguments.

222
00:13:24,010 --> 00:13:29,140
We're going to leave this topic for now
and turn to a separate topic.

223
00:13:29,140 --> 00:13:31,360
In order to understand something you
want to know not just

224
00:13:31,360 --> 00:13:34,650
it's purpose, but the material out of
which it's made.

225
00:13:34,650 --> 00:13:36,405
So the next few lectures will be about the

226
00:13:36,405 --> 00:13:40,110
material out of which argument are made
namely, language.

