1
00:00:03,430 --> 00:00:04,850
Now I just said that in English,

2
00:00:04,850 --> 00:00:08,270
the word and functions as a propositional
connective.

3
00:00:08,270 --> 00:00:11,707
But it doesn't always function that way,
even in English.

4
00:00:11,707 --> 00:00:16,410
For instance, think about a sentence that
uses the word and.

5
00:00:16,410 --> 00:00:19,863
Like the sentence, Jack and Jill finally
talked.

6
00:00:21,760 --> 00:00:25,890
I can think of at least three different
interpretations of that sentence.

7
00:00:25,890 --> 00:00:28,035
Three different things that that sentence
could mean,

8
00:00:28,035 --> 00:00:33,170
and corresponding to those three different
things that the sentence could mean,

9
00:00:33,170 --> 00:00:37,840
three different ways that the word and is
getting used that sentence.

10
00:00:37,840 --> 00:00:39,800
So here's one thing that the sentence
could mean.

11
00:00:39,800 --> 00:00:42,890
Jack and Jill could be a name of a

12
00:00:42,890 --> 00:00:47,330
particular entity like, let's say, a fast
food company.

13
00:00:47,330 --> 00:00:50,190
There could be a fast food company called
Jack

14
00:00:50,190 --> 00:00:53,060
and Jill and maybe Jack and Jill the fast
food

15
00:00:53,060 --> 00:00:56,560
company makes some kind of special stew.

16
00:00:56,560 --> 00:00:59,060
And lawyers have been asking Jack and Jill

17
00:00:59,060 --> 00:01:02,520
to disclose the ingredients of the special
stew because

18
00:01:02,520 --> 00:01:05,290
they find that a lot of the, customers

19
00:01:05,290 --> 00:01:07,390
who eat this stew have been getting sick
recently.

20
00:01:07,390 --> 00:01:11,510
So the lawyers are asking Jack and Jill to
disclose the ingredients, and Jack and

21
00:01:11,510 --> 00:01:15,130
Jill refuses to do so until finally, the

22
00:01:15,130 --> 00:01:18,170
spokesperson for Jack and Jill holds a
press

23
00:01:18,170 --> 00:01:22,925
conference, in which he discloses the
ingredients of the special stew.

24
00:01:22,925 --> 00:01:27,420
Okay, I might describe that situation by
saying Jack and Jill finally talked.

25
00:01:27,420 --> 00:01:32,600
What I mean there is there's a particular
entity, namely the company Jack and Jill.

26
00:01:32,600 --> 00:01:37,680
And that company finally talked through
it's spokesperson, finally

27
00:01:37,680 --> 00:01:42,790
disclosed the ingredients of it's special
stew through it's spokesperson,

28
00:01:42,790 --> 00:01:45,790
now, on that interpretation of the
sentence, Jack and

29
00:01:45,790 --> 00:01:49,250
Jill finally talked, the word and is not
being used

30
00:01:49,250 --> 00:01:53,800
as a propositional connective, it's not
connecting two propositions and

31
00:01:53,800 --> 00:01:57,930
forming a larger proposition out of those
two smaller propositions.

32
00:01:59,620 --> 00:02:03,510
Here's another way of understanding the
sentence, Jack and Jill finally talked.

33
00:02:06,280 --> 00:02:09,960
let's suppose Jack, Jill, and Rodger are
having a silence contest, to

34
00:02:09,960 --> 00:02:14,900
see who among the three of them can be
silent the longest.

35
00:02:14,900 --> 00:02:19,220
And, all three of them are silent for a
long period of time.

36
00:02:19,220 --> 00:02:20,190
And you're calling in.

37
00:02:20,190 --> 00:02:22,610
While I'm watching the silence contest,
you're calling

38
00:02:22,610 --> 00:02:24,790
in to me periodically, to find out if.

39
00:02:24,790 --> 00:02:27,550
Any one of these three contestants finally
talked.

40
00:02:28,560 --> 00:02:31,430
And finally at one point you call

41
00:02:31,430 --> 00:02:33,500
in and you say okay, has anyone talked
yet?

42
00:02:33,500 --> 00:02:38,760
And I say well, Jack and Jill finally
talked, so Roger won the competition.

43
00:02:39,960 --> 00:02:42,870
Now there in that sentence when I say Jack
and Jill

44
00:02:42,870 --> 00:02:47,630
finally talked, the word and is being used
as a propositional connective.

45
00:02:47,630 --> 00:02:54,050
What I'm saying in effect is that Jack
finally talked and Jill finally talked.

46
00:02:54,050 --> 00:02:54,428
So there are two propositions that I'm trying to communicate -

47
00:02:54,428 --> 00:02:54,883
the proposition Jack finally talked and the proposition Jill finally talked.

48
00:02:54,883 --> 00:02:56,570
And I'm using the word and to connect
those two propositions

49
00:02:56,570 --> 00:03:06,570
into a larger proposition Jack And Jill

50
00:03:08,500 --> 00:03:12,620
finally talked, and so Roger won the
competition.

51
00:03:12,620 --> 00:03:16,200
There, the word and is being used as a
propositional connective.

52
00:03:16,200 --> 00:03:19,070
It's just connecting the proposition Jack
finally

53
00:03:19,070 --> 00:03:21,600
talked and the proposition Jill finally
talked to

54
00:03:21,600 --> 00:03:24,619
form a larger proposition, Jack and Jill
finally talked.

55
00:03:26,690 --> 00:03:28,910
And there's a third way the the word and
could

56
00:03:28,910 --> 00:03:33,030
get used, just in that sentence, Jack and
Jill finally talked.

57
00:03:33,030 --> 00:03:37,600
Suppose Jack and Jill are a couple, and
recently they'd been having a tough time.

58
00:03:37,600 --> 00:03:40,010
They've been angry and resentful, and they
haven't been talking

59
00:03:40,010 --> 00:03:43,820
to each other about the sources of their
anger and resentment.

60
00:03:43,820 --> 00:03:46,960
Well, suppose they decide that they're
going to

61
00:03:46,960 --> 00:03:49,300
finally get their grievances out into the
open.

62
00:03:49,300 --> 00:03:50,500
They're going to talk to each other.

63
00:03:50,500 --> 00:03:52,230
Then I might say, Jack

64
00:03:52,230 --> 00:03:54,480
and Jill finally talked.

65
00:03:54,480 --> 00:03:56,780
Now there, when I use the word and, I'm
not

66
00:03:56,780 --> 00:04:01,080
just connecting the proposition Jack
finally talked and Jill finally talked.

67
00:04:01,080 --> 00:04:04,190
What I'm trying to communicate is not just
that each of them talked.

68
00:04:04,190 --> 00:04:07,040
I'm trying to communicate that each of
them talked.

69
00:04:07,040 --> 00:04:08,130
To the other.

70
00:04:08,130 --> 00:04:11,730
I'm trying to say that they talk to each
other.

71
00:04:11,730 --> 00:04:17,510
So there, in that third interpretation,
the word, and, is not working just

72
00:04:17,510 --> 00:04:18,950
as a propositional connective.

73
00:04:18,950 --> 00:04:21,880
It's not just connecting the proposition,
Jack finally talked, and the

74
00:04:21,880 --> 00:04:26,640
proposition, Jill finally talked, cause
I'm trying to get across something more.

75
00:04:26,640 --> 00:04:29,440
Then just that Jack finally talked, and
Jill finally talked.

76
00:04:29,440 --> 00:04:33,050
I'm also trying to get across the point
that they talk to each other.

77
00:04:34,310 --> 00:04:34,530
Right.

78
00:04:34,530 --> 00:04:37,080
That's what I'm trying to communicate
there

79
00:04:37,080 --> 00:04:40,030
with the sentence, Jack and Jill finally
talked.

80
00:04:40,030 --> 00:04:42,610
So there the word and is not being

81
00:04:42,610 --> 00:04:44,389
used as a propositional connective.

82
00:04:45,570 --> 00:04:47,810
So, I hope I've made it clear that the
word, and,

83
00:04:47,810 --> 00:04:50,560
can get used in a bunch of different ways
in English.

84
00:04:51,880 --> 00:04:53,735
One of the ways it can get used, is as a

85
00:04:53,735 --> 00:04:58,640
propositional connective to connect up two
propositions into a larger proposition.

86
00:04:58,640 --> 00:05:00,500
But that's not the only way it can get
used.

87
00:05:00,500 --> 00:05:02,130
It can get used in other ways, as well,

88
00:05:02,130 --> 00:05:05,060
and I've tried to give examples of those,
just now.

89
00:05:05,060 --> 00:05:08,440
What we're going to be concerned about
here is just

90
00:05:08,440 --> 00:05:12,420
the use of and as a propositional
connective.

91
00:05:12,420 --> 00:05:16,150
And then we'll look at other expressions
in English, like or, not,

92
00:05:16,150 --> 00:05:21,840
but, only, if, and so forth, that also get
used as propositional connectives.

93
00:05:21,840 --> 00:05:24,095
We'll look at those and see how those
work.

94
00:05:24,095 --> 00:05:25,760
Okay.

95
00:05:25,760 --> 00:05:31,350
So now, let's consider the propositional
connective, and.

96
00:05:31,350 --> 00:05:33,590
Notice though, that even when the word and

97
00:05:33,590 --> 00:05:36,230
is being used as a propositional
connective,

98
00:05:36,230 --> 00:05:38,690
it can still get used in different ways.

99
00:05:38,690 --> 00:05:42,932
For instance, suppose I say I took a
shower and got dressed.

100
00:05:42,932 --> 00:05:44,960
Well there.

101
00:05:44,960 --> 00:05:47,320
And is being used as a propositional
connective.

102
00:05:47,320 --> 00:05:48,820
It's connecting two propositions.

103
00:05:48,820 --> 00:05:52,990
The proposition, I took a shower and the
proposition, I got dressed.

104
00:05:52,990 --> 00:05:55,430
It's connecting those into a larger
proposition.

105
00:05:55,430 --> 00:05:57,100
I took a shower and got dressed.

106
00:05:59,280 --> 00:06:02,720
But there, the word and, is being used to
convey

107
00:06:02,720 --> 00:06:06,130
a sense of temporal order and a sense of
time.

108
00:06:06,130 --> 00:06:08,945
The idea is when I say, I took a shower
and got

109
00:06:08,945 --> 00:06:15,030
dressed, the idea is I first took a shower
and then got dressed.

110
00:06:15,030 --> 00:06:15,210
Right?

111
00:06:15,210 --> 00:06:18,020
It wouldn't mean the same thing if I said,
I got dressed and took

112
00:06:18,020 --> 00:06:24,900
a shower, but sometimes when the word and
is being used as a propositional

113
00:06:24,900 --> 00:06:28,390
connective there is no suggestion of
temporal ordering.

114
00:06:28,390 --> 00:06:29,800
So for instance, if I say

115
00:06:32,650 --> 00:06:36,680
if I say I'm holding the binoculars and
looking through them,

116
00:06:36,680 --> 00:06:40,140
there's no suggestion that I'm doing one
first and then the other.

117
00:06:40,140 --> 00:06:40,310
Right?

118
00:06:40,310 --> 00:06:45,280
I'm just doing both, I'm holding them and
looking through them.

119
00:06:45,280 --> 00:06:47,629
There's no suggestion that one is
happening before the other.

120
00:06:49,060 --> 00:06:53,340
Now, in that second usage, when the word
and doesn't convey any

121
00:06:53,340 --> 00:06:58,180
kind of temporal ordering, where it just
combines two proposi-, two propositions

122
00:06:58,180 --> 00:07:01,220
into a larger proposition without
conveying

123
00:07:01,220 --> 00:07:03,360
any sense of temporal ordering, then

124
00:07:03,360 --> 00:07:07,340
I'll say the word and is not just
functioning as a proposition connective.

125
00:07:07,340 --> 00:07:11,720
But it' functioning as what I'll call a
truth-functional connective.

126
00:07:12,840 --> 00:07:17,654
So what's a truth-functional connective?
A truth-functional connective is a

127
00:07:17,654 --> 00:07:23,440
propositional connective that creates new
propositions

128
00:07:23,440 --> 00:07:29,450
whose truth or falsity depends on nothing
other that the truth or falsity

129
00:07:29,450 --> 00:07:35,570
of the propositions that went into
creating them.

130
00:07:35,570 --> 00:07:37,958
Let me give you some examples to
illustrate that definition.

131
00:07:37,958 --> 00:07:43,650
So consider, again, our example Jack and
Jill finally talked.

132
00:07:43,650 --> 00:07:46,170
Where that's used to mean just that Jack

133
00:07:46,170 --> 00:07:51,770
finally talked and that Jill finally
talked, okay.

134
00:07:51,770 --> 00:07:55,080
So when is that proposition going to be
true, when is

135
00:07:55,080 --> 00:07:59,169
it going to be true that Jack and Jill
finally talked.

136
00:08:00,670 --> 00:08:03,130
Well that's going to be true whenever it's

137
00:08:03,130 --> 00:08:06,660
true that Jack finally talked, and, Jill
finally talked.

138
00:08:07,920 --> 00:08:11,720
As long as, those two conditions are true
as long as Jack

139
00:08:11,720 --> 00:08:17,170
finally talked, and, Jill finally talked,
it's also going to be true that

140
00:08:17,170 --> 00:08:19,219
Jack and Jill finally talked.

141
00:08:20,750 --> 00:08:24,770
So, if it's not true that Jack finally
talked, then

142
00:08:24,770 --> 00:08:27,280
it won't be true that Jack and Jill
finally talked.

143
00:08:27,280 --> 00:08:29,420
If it's not true that Jill finally talked,
then

144
00:08:29,420 --> 00:08:32,280
it won't be true that Jack and Jill
finally talked.

145
00:08:32,280 --> 00:08:35,120
But as long as it is true that Jack
finally talked and that

146
00:08:35,120 --> 00:08:39,290
Jill finally talked, it will be true that
Jack and Jill finally talked.

147
00:08:40,310 --> 00:08:43,300
So that use of and

148
00:08:43,300 --> 00:08:47,140
is a use of and as a truth functional
connective,

149
00:08:47,140 --> 00:08:51,970
because it creates a new proposition, Jack
and Jill finally talked.

150
00:08:51,970 --> 00:08:56,320
The truth or falsity of which depends on
nothing other than

151
00:08:56,320 --> 00:08:59,989
the truth or falsity of the two
propositions that it connects.

152
00:09:02,090 --> 00:09:03,640
There are lots of other examples.

153
00:09:03,640 --> 00:09:05,500
So, for instance, consider the use of

154
00:09:05,500 --> 00:09:09,810
and as a truth-functional connective in
the proposition.

155
00:09:09,810 --> 00:09:12,670
I'm holding the binoculars and looking
through them.

156
00:09:12,670 --> 00:09:15,660
Again, when is that proposition going to
be true?

157
00:09:15,660 --> 00:09:21,060
It's going to be true only in these cases,
where it's true that

158
00:09:21,060 --> 00:09:23,580
I'm holding my binoculars, and it's also

159
00:09:23,580 --> 00:09:25,405
true that I'm looking through my
binoculars.

160
00:09:25,405 --> 00:09:30,600
If both of those are true, then the whole
proposition, I'm

161
00:09:30,600 --> 00:09:34,710
holding my binoculars and looking through
them, will also be true.

162
00:09:34,710 --> 00:09:37,230
If either one of those initial
propositions is

163
00:09:37,230 --> 00:09:40,210
false, if it's false that I'm holding the
binoculars.

164
00:09:40,210 --> 00:09:43,440
Or if it's false that I'm looking through
the binoculars then the whole

165
00:09:43,440 --> 00:09:46,300
proposition, I'm holding the biinoculrs
and

166
00:09:46,300 --> 00:09:48,695
looking through them will also be flase.

167
00:09:48,695 --> 00:09:50,573
So there again,

168
00:09:50,573 --> 00:09:54,710
the truth or falsity of the whole
proposition depends on nothing other

169
00:09:54,710 --> 00:09:58,180
than the truth or falsity of the
propositions that go into creating it.

170
00:09:58,180 --> 00:09:59,940
Using the truth functional connective,
And.

171
00:09:59,940 --> 00:10:04,480
And that's what shows that And, in that
use

172
00:10:04,480 --> 00:10:07,740
of, in that usage, is a truth functional
connective.

173
00:10:07,740 --> 00:10:08,590
It's that.

174
00:10:08,590 --> 00:10:12,330
The proposition that it creates by joining
other propositions is

175
00:10:12,330 --> 00:10:15,780
a proposition whose truth or falsity
depends on nothing other

176
00:10:15,780 --> 00:10:18,160
than the truth or falsity of the

177
00:10:18,160 --> 00:10:21,000
ingredient propositions that went into
creating it.

178
00:10:21,000 --> 00:10:23,680
That's a truth-functional connective.

179
00:10:23,680 --> 00:10:27,930
Now in week four, this week on
propositional logic

180
00:10:27,930 --> 00:10:32,100
we're going to be studying truth
functional connectives and how

181
00:10:32,100 --> 00:10:35,950
the use of truth functional connectives in
argument can make

182
00:10:35,950 --> 00:10:39,330
those arguments valid no matter what those
arguments are about.

183
00:10:40,910 --> 00:10:42,540
Now lets move to some examples.

