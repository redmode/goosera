1
00:00:03,440 --> 00:00:06,030
Welcome to the second unit of our course.

2
00:00:06,030 --> 00:00:08,530
In the first unit of the course, you
learned

3
00:00:08,530 --> 00:00:11,140
how to listen to what someone was saying,
or

4
00:00:11,140 --> 00:00:14,540
read what they wrote, and separate out the
arguments

5
00:00:14,540 --> 00:00:17,260
that they were giving from the rest of
their words.

6
00:00:17,260 --> 00:00:20,640
You learned what arguments are, what their
parts are, and how

7
00:00:20,640 --> 00:00:25,890
those parts work together to achieve the
various purposes of argument.

8
00:00:25,890 --> 00:00:28,460
Now, in this second unit of the course and
the third unit that

9
00:00:28,460 --> 00:00:33,550
follows it, you'll learn some rules for
evaluating the successive arguments.

10
00:00:33,550 --> 00:00:36,810
In particular, in the second unit of the
course, we'll

11
00:00:36,810 --> 00:00:42,350
focus on rules for evaluating the validity
of deductive arguments.

12
00:00:42,350 --> 00:00:44,399
And then in the third unit of the course
we'll

13
00:00:44,399 --> 00:00:46,461
focus on rules for evaluating the strength
of inductive arguments.

14
00:00:46,461 --> 00:00:47,960
Recall that deductive arguments are
arguments that are presented as

15
00:00:47,960 --> 00:00:51,140
being valid and they are successful only
if they're valid.

16
00:00:51,140 --> 00:00:56,432
Inductive arguments in

17
00:00:56,432 --> 00:01:03,830
contrast are arguments that are not
presented as being valid.

18
00:01:03,830 --> 00:01:06,930
They're presented as being strong, and
they're successful

19
00:01:06,930 --> 00:01:08,629
only to the extent that they are strong.

20
00:01:10,350 --> 00:01:12,340
Okay, now since in this second unit of the

21
00:01:12,340 --> 00:01:16,720
course, we're focusing on rules for
evaluating the validity

22
00:01:16,720 --> 00:01:18,269
of deductive arguments.

23
00:01:18,269 --> 00:01:21,120
Let me say a bit more now about

24
00:01:21,120 --> 00:01:25,210
validity, and about the rules for
assessing validity.

25
00:01:25,210 --> 00:01:31,070
See, I just got a new pet clownfish, Nimo.
Now,

26
00:01:33,670 --> 00:01:38,640
maybe you don't know much about clownfish
anatomy, but I'm

27
00:01:38,640 --> 00:01:43,460
going to try to persuade you right now
that clownfish have gills.

28
00:01:43,460 --> 00:01:44,830
Here's an argument that I could give

29
00:01:44,830 --> 00:01:46,520
you for the conclusion that clownfish have
gills.

30
00:01:46,520 --> 00:01:47,765
Well, catfish have gills.

31
00:01:47,765 --> 00:01:52,840
Goldfish have

32
00:01:52,840 --> 00:01:58,199
gills, and sharks have gills.
Therefore, clownfish have gills.

33
00:01:58,199 --> 00:02:04,760
Now, is that argument valid?
No, it's not.

34
00:02:04,760 --> 00:02:08,600
It's not valid because it's possible for
the premises

35
00:02:08,600 --> 00:02:14,050
to be true even when the conclusion is
false.

36
00:02:14,050 --> 00:02:18,450
It could be that catfish and sharks and

37
00:02:18,450 --> 00:02:21,340
goldfish all have gills, even though
clownfish don't.

38
00:02:23,410 --> 00:02:25,235
But now suppose I give you a different

39
00:02:25,235 --> 00:02:30,140
argument, for the conclusion that
clownfish have gills.

40
00:02:30,140 --> 00:02:32,620
Here's how this different argument goes.

41
00:02:32,620 --> 00:02:34,365
All fish have gills.

42
00:02:34,365 --> 00:02:41,070
Clownfish are a kind of fish, therefore,
clownfish have gills.

43
00:02:41,070 --> 00:02:43,650
Now that argument is valid.

44
00:02:43,650 --> 00:02:48,090
There's no possible way for the premises
of that argument to be true,

45
00:02:48,090 --> 00:02:50,560
if the conclusion is false.

46
00:02:50,560 --> 00:02:51,850
Recall that in the first unit of

47
00:02:51,850 --> 00:02:55,070
the course, Walter defined validity as
follows.

48
00:02:55,070 --> 00:02:59,330
He said an argument is valid if there's no
possible way for all

49
00:02:59,330 --> 00:03:03,890
the premises of that argument to be true,
while the conclusion is false.

50
00:03:03,890 --> 00:03:06,760
And he gave an example of an argument that
was valid.

51
00:03:06,760 --> 00:03:08,530
The example, recall, went like this.

52
00:03:08,530 --> 00:03:12,699
Premise one, Mary has a child who is
pregnant.

53
00:03:13,870 --> 00:03:17,780
Premise two, only daughters can become
pregnant.

54
00:03:18,820 --> 00:03:23,070
Therefore, conclusion, Mary has at least
one daughter.

55
00:03:24,280 --> 00:03:26,160
Now remember, as Walter pointed out, you
can

56
00:03:26,160 --> 00:03:29,730
imagine situations where the first premise
is false.

57
00:03:29,730 --> 00:03:32,750
Maybe Mary doesn't have a child who is
pregnant.

58
00:03:32,750 --> 00:03:36,800
You can imagine situations in which the
second premise is false.

59
00:03:36,800 --> 00:03:39,190
Maybe not only daughters

60
00:03:39,190 --> 00:03:40,080
can become pregnant.

61
00:03:40,080 --> 00:03:43,590
Maybe there's some way for sons to become
pregnant, or maybe there's some

62
00:03:43,590 --> 00:03:46,760
way for children of some third gender

63
00:03:46,760 --> 00:03:49,030
or some unspecified gender to become
pregnant.

64
00:03:50,930 --> 00:03:53,300
And you can imagine a situation in which
the conclusion

65
00:03:53,300 --> 00:03:56,230
is false, in which Mary doesn't have at
least one daughter.

66
00:03:56,230 --> 00:03:57,920
But what you can't imagine, what's

67
00:03:57,920 --> 00:04:01,220
completely incoherent, what, what's
completely impossible.

68
00:04:01,220 --> 00:04:04,480
Is for there to be a situation in which
both of the premises

69
00:04:04,480 --> 00:04:09,516
are true and the conclusion is false.
In any possible situation,

70
00:04:09,516 --> 00:04:15,500
either the premises are, in any

71
00:04:15,500 --> 00:04:20,580
possible situation in which the premises
are true, the conclusion has to be true.

72
00:04:22,370 --> 00:04:24,570
There's no possible situation for the
premises

73
00:04:24,570 --> 00:04:26,045
to be true and the conclusion false.

74
00:04:26,045 --> 00:04:31,120
So, that's what makes that argument valid.

75
00:04:31,120 --> 00:04:33,120
Now what I'd like to do right now is show
that there are

76
00:04:33,120 --> 00:04:38,990
other arguments that have the same form as
this first argument about Mary,

77
00:04:40,420 --> 00:04:44,810
and they're valid for what seems to be the
same reason, as the

78
00:04:44,810 --> 00:04:47,010
argument about Mary is valid, even though

79
00:04:47,010 --> 00:04:49,080
they have a completely different subject
matter.

80
00:04:50,870 --> 00:04:52,610
So here's another example.

81
00:04:52,610 --> 00:04:53,800
Consider the following argument.

82
00:04:53,800 --> 00:04:56,780
Premise one.
Terry has

83
00:04:56,780 --> 00:05:00,970
a job in which she arrests people.
Premise two.

84
00:05:00,970 --> 00:05:06,016
Only police officers can arrest people,
and therefore,

85
00:05:06,016 --> 00:05:11,770
conclusion, at least one of Terry's jobs
is as a police officer.

86
00:05:11,770 --> 00:05:19,260
Now again, maybe, premise one of this
argument is false.

87
00:05:19,260 --> 00:05:21,590
You can certainly imagine that it's false,

88
00:05:21,590 --> 00:05:21,870
right?

89
00:05:21,870 --> 00:05:25,210
Maybe Terry doesn't have a job in which
she arrests people.

90
00:05:25,210 --> 00:05:27,640
Maybe premise two of the argument is
false.

91
00:05:27,640 --> 00:05:32,960
You can imagine a situation which not only
police officers can arrest people.

92
00:05:32,960 --> 00:05:36,210
Maybe there's a world in which police
officers and

93
00:05:36,210 --> 00:05:41,080
judges, and plumbers and actors, can all
arrest people.

94
00:05:42,820 --> 00:05:44,010
And of course you could imagine a

95
00:05:44,010 --> 00:05:46,260
situation in which the conclusion is
false.

96
00:05:46,260 --> 00:05:46,815
In which,

97
00:05:46,815 --> 00:05:51,940
it's not true that at least one of Terry's
jobs is as a police officer.

98
00:05:53,950 --> 00:05:58,600
But what you can't imagine, what's
completely impossible, is that both of

99
00:05:58,600 --> 00:06:02,940
the premises of that argument are true,
and the conclusion is false.

100
00:06:02,940 --> 00:06:05,850
If both of the premises of that argument
about Terry are true,

101
00:06:05,850 --> 00:06:11,860
the conclusion has to be true, and so the
argument is valid.

102
00:06:11,860 --> 00:06:15,290
The argument about Terry is valid just as
the argument about Mary is valid.

103
00:06:16,320 --> 00:06:19,260
Now notice, the two arguments have
completely different subject matters.

104
00:06:19,260 --> 00:06:22,770
The first is about Mary's children, the
second is about Terry's jobs.

105
00:06:24,140 --> 00:06:25,610
But even though they have completely
different

106
00:06:25,610 --> 00:06:28,150
subject matters, they have something in
common.

107
00:06:28,150 --> 00:06:31,260
There seems to be something in common to
their form.

108
00:06:31,260 --> 00:06:38,050
Something signaled by their use of the
terms only and at least.

109
00:06:38,050 --> 00:06:42,610
Something in common to the two arguments
that explains why they're valid.

110
00:06:44,140 --> 00:06:49,940
To get at what this common feature is a
bit more clearly, let me give a third

111
00:06:49,940 --> 00:06:55,920
and final example of this form and see if
you get the idea.

112
00:06:55,920 --> 00:06:57,940
So consider the following argument.

113
00:06:57,940 --> 00:07:01,460
Premise one, Robert has a pet who is
canine.

114
00:07:02,610 --> 00:07:09,350
Premise two, only mammals can be canine.
So, conclusion, Robert has at least one

115
00:07:09,350 --> 00:07:10,470
pet who is mammal.

116
00:07:12,090 --> 00:07:15,410
Now, of course, you could imagine a
situation in which premise one is false.

117
00:07:15,410 --> 00:07:18,910
Maybe Robert doesn't have any canine pets.

118
00:07:18,910 --> 00:07:21,290
You can imagine a situation in which
premise two

119
00:07:21,290 --> 00:07:24,470
is false, in which not only mammals are
canines.

120
00:07:24,470 --> 00:07:28,300
Maybe some canines are reptiles, or some
canines are robots.

121
00:07:28,300 --> 00:07:29,930
Or who knows?

122
00:07:29,930 --> 00:07:34,200
And of course you can imagine a situation
in which the conclusion is false.

123
00:07:34,200 --> 00:07:34,340
I mean,

124
00:07:34,340 --> 00:07:36,270
in which Robert doesn't have any mammal
pets.

125
00:07:36,270 --> 00:07:37,905
Maybe all of his pets are birds, or

126
00:07:37,905 --> 00:07:41,170
reptile, or maybe he doesn't have any
pets.

127
00:07:41,170 --> 00:07:45,820
But in any case, you can imagine these
possibilities.

128
00:07:45,820 --> 00:07:49,780
But what you cannot imagine, what's
completely ruled out, is the possibility

129
00:07:49,780 --> 00:07:55,510
that the premises of that argument are
true and the conclusion is false.

130
00:07:55,510 --> 00:07:59,840
If the premises of the argument are true
the conclusion has got to be true.

131
00:07:59,840 --> 00:08:02,930
And so this argument about Robert is also
valid,

132
00:08:02,930 --> 00:08:05,360
just like the arguments about Mary and
Terry were valid.

133
00:08:06,920 --> 00:08:09,440
But, of course, this argument about Robert
has completely different

134
00:08:09,440 --> 00:08:13,000
subject matter than the arguments about
Mary or Terry, right?

135
00:08:13,000 --> 00:08:16,540
One was an argument about Mar-, Mary's
children, the other was an

136
00:08:16,540 --> 00:08:19,750
argument about Terry's jobs, and this is
an argument about Robert's pets.

137
00:08:20,890 --> 00:08:25,570
Completely different subject matters, but
they're all valid and

138
00:08:25,570 --> 00:08:28,570
it seems they're all valid for the same
reason.

139
00:08:28,570 --> 00:08:32,110
They have the same form, and it's a form
that's

140
00:08:32,110 --> 00:08:36,960
signaled by their use of the phrases only
and at least.

141
00:08:38,920 --> 00:08:44,810
So what we'll be doing in the second unit
of the course is studying

142
00:08:44,810 --> 00:08:50,670
the forms of valid argument, the forms
that explain why

143
00:08:50,670 --> 00:08:52,586
valid arguments are valid.

144
00:08:52,586 --> 00:08:57,590
Now, in the arguments that we just looked
at,

145
00:08:57,590 --> 00:09:01,070
the arguments about Mary, Terry, and
Robert, their common form,

146
00:09:01,070 --> 00:09:03,560
I said, involved the use of the word only,
and

147
00:09:03,560 --> 00:09:06,205
it also involved the use of the phrase at
least.

148
00:09:06,205 --> 00:09:11,675
Now only and at least are both phrases
that we're going to call

149
00:09:11,675 --> 00:09:16,920
quanitfiers, and next week in week five of
the course when

150
00:09:16,920 --> 00:09:18,400
we study the subject we're going to

151
00:09:18,400 --> 00:09:22,050
call categorical logic We're going to be
studying,

152
00:09:22,050 --> 00:09:26,210
in particular, quantifiers like only, at
least,

153
00:09:26,210 --> 00:09:29,640
some, all, none, and phrases like that.

154
00:09:29,640 --> 00:09:34,825
And we're going to be studying how the use
of phrases like that, the use of

155
00:09:34,825 --> 00:09:40,930
quantifier, can make arguments valid, no
matter what those arguments are about.

156
00:09:40,930 --> 00:09:41,945
If the arguments use

157
00:09:41,945 --> 00:09:47,470
quantifiers like only, at least, some, all
and none in certain ways

158
00:09:47,470 --> 00:09:51,070
then those arguments are going to be valid
regardless of their subject matter.

159
00:09:52,810 --> 00:09:55,290
But this week in week four, we're not

160
00:09:55,290 --> 00:09:58,400
going to be studying quantifiers like only
or at least,

161
00:09:58,400 --> 00:10:00,160
instead we're going to be studying a kind
of

162
00:10:00,160 --> 00:10:04,170
phrase That we're going to call a
propositional connective.

163
00:10:04,170 --> 00:10:07,450
In particular, a kind of propositional
connective

164
00:10:07,450 --> 00:10:10,740
that we'll call a truth-functional
connective.

165
00:10:10,740 --> 00:10:12,324
So, what are propositional connectives ?

166
00:10:12,324 --> 00:10:14,068
What are truth-functional connectives?

167
00:10:14,068 --> 00:10:19,710
How do they work to make arguments valid?

168
00:10:19,710 --> 00:10:23,360
Those questions and more we'll address in
the next lecture.

