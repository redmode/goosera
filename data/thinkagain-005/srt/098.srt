1
00:00:04,440 --> 00:00:07,690
So now we really gotta face that crucial
question, how can

2
00:00:07,690 --> 00:00:12,660
we tell when a generalization from a
sample is a good argument?

3
00:00:12,660 --> 00:00:18,180
We're not going to be able to do a lot of
mathematical statistics here, that would

4
00:00:18,180 --> 00:00:19,960
take a whole course, and you ought to

5
00:00:19,960 --> 00:00:23,370
take one, it's useful it's important, it's
interesting.

6
00:00:23,370 --> 00:00:26,180
Definitely worth taking a course on
statistics.

7
00:00:26,180 --> 00:00:29,500
But all we're going to be able to do here,
is look at some really

8
00:00:29,500 --> 00:00:32,530
common errors that people make.

9
00:00:32,530 --> 00:00:36,130
So you won't be le, misled in these
obvious ways.

10
00:00:36,130 --> 00:00:39,780
It won't so much help you do statistical
studies of your own.

11
00:00:39,780 --> 00:00:42,760
But it will help you avoid getting fooled
by

12
00:00:42,760 --> 00:00:46,630
people who cite statistical studies for
conclusions that you

13
00:00:46,630 --> 00:00:50,920
might not want to believe, to illustrate
these problems,

14
00:00:50,920 --> 00:00:54,520
I did little survey, I happen to love
chocolate chip

15
00:00:54,520 --> 00:00:59,980
cookies, now not all chocolate chip
cookies are equally good, some of them

16
00:00:59,980 --> 00:01:04,550
I think have too few chips, you need a lot
of chocolate chips.

17
00:01:04,550 --> 00:01:06,190
But you don't want too many because then
it's just

18
00:01:06,190 --> 00:01:08,310
a bunch of chocolate and you don't get the
dough.

19
00:01:08,310 --> 00:01:09,850
The, the butter, the sugar.

20
00:01:09,850 --> 00:01:12,980
That stuff's good too.
So you want just the right balance here.

21
00:01:12,980 --> 00:01:15,570
You want, I would say, in a, say a three

22
00:01:15,570 --> 00:01:19,480
inch diameter cookie, you want about ten
to 12 chocolate chips.

23
00:01:20,550 --> 00:01:22,060
And there happen to be five bakeries

24
00:01:22,060 --> 00:01:26,670
in my hometown that serve chocolate chip
cookies.

25
00:01:26,670 --> 00:01:28,120
So.

26
00:01:28,120 --> 00:01:32,080
I went and got ten chocolate chip cookies
from each of the five bakeries, and

27
00:01:32,080 --> 00:01:37,710
counted the chocolate chips in the
cookies, and I found out that 80% of the

28
00:01:37,710 --> 00:01:44,560
chocolate chip cookies that I bought from
Bakery A had ten to 12 chips in them.

29
00:01:44,560 --> 00:01:46,140
So I conclude

30
00:01:46,140 --> 00:01:49,520
that 80% of the chocolate chip cookies
from Bakery

31
00:01:49,520 --> 00:01:52,000
A had ten to 12 chocolate chips in them.

32
00:01:53,070 --> 00:01:54,840
That sounds like a pretty good argument,
doesn't it?

33
00:01:54,840 --> 00:01:59,706
And I could have sampled 20 or 30, but 10
is a pretty good number for a sample.

34
00:01:59,706 --> 00:02:03,250
So, you ought to believe that conclusion,
right?

35
00:02:03,250 --> 00:02:05,320
There's nothing wrong with that argument,
is there?

36
00:02:06,940 --> 00:02:07,870
Well what's the problem?

37
00:02:09,330 --> 00:02:11,710
The problem is, I'm lying.

38
00:02:11,710 --> 00:02:14,570
I didn't buy a single cookie.
I didn't do this survey.

39
00:02:16,450 --> 00:02:20,800
What does that show you?
That shows you, that one problem for

40
00:02:20,800 --> 00:02:26,990
statistical generalizations from samples,
is that the premises have to be true.

41
00:02:29,630 --> 00:02:31,180
Kind of obvious.

42
00:02:31,180 --> 00:02:33,210
And what it shows is that, just like

43
00:02:33,210 --> 00:02:36,560
a valid argument's no good unless it's
also sound.

44
00:02:36,560 --> 00:02:38,180
When it's a deductive argument.

45
00:02:38,180 --> 00:02:42,110
Similarly, what would be a strong
argument, if the premises

46
00:02:42,110 --> 00:02:44,760
were true, is no good if the premises
aren't true.

47
00:02:46,600 --> 00:02:50,490
But also like deductive arguments, it's
not enough for the premises to be true.

48
00:02:52,450 --> 00:02:54,790
What if I counted,

49
00:02:54,790 --> 00:02:58,060
I really did buy the cookies and I really
did count but

50
00:02:58,060 --> 00:03:00,900
I missed a bunch of them because I was
going so fast.

51
00:03:02,010 --> 00:03:05,630
Or I couldn't count the chocolate chips
very

52
00:03:05,630 --> 00:03:07,650
well because they were like all melted
together.

53
00:03:08,830 --> 00:03:11,960
Or, maybe I got the cookies from Bakery A
mixed up

54
00:03:11,960 --> 00:03:15,220
with the cookies from Bakery B or Bakery C
or Bakery D.

55
00:03:16,390 --> 00:03:20,020
Then, I just made a mistake when I was
counting the cookies.

56
00:03:20,020 --> 00:03:22,830
Then, it turns out the premises of

57
00:03:22,830 --> 00:03:26,320
the argument, of the generalization from
the sample.

58
00:03:26,320 --> 00:03:28,240
Those premises are false.

59
00:03:28,240 --> 00:03:32,730
But not because I'm lying, rather because
I didn't count well.

60
00:03:32,730 --> 00:03:34,700
So I'm unjustified.

61
00:03:34,700 --> 00:03:39,800
And of course it doesn't help if I'm
justified, if you're not justified.

62
00:03:39,800 --> 00:03:41,420
You're not going to have any reason to
believe

63
00:03:41,420 --> 00:03:45,130
this about the cookies, unless not only
did I count

64
00:03:45,130 --> 00:03:48,210
them accurately, and carefully, and
reliably but you

65
00:03:48,210 --> 00:03:49,569
have reason to believe that I did so.

66
00:03:51,950 --> 00:03:57,170
So the general point is simply that when
you face a generalization from a

67
00:03:57,170 --> 00:04:03,320
sample, then the premises have to be true
and justified.

68
00:04:03,320 --> 00:04:05,115
So the first question you ought to ask
about

69
00:04:05,115 --> 00:04:09,700
any generalization from a sample is, are
the premises true?

70
00:04:09,700 --> 00:04:11,120
And are they justified?

71
00:04:12,140 --> 00:04:18,430
Next let's assume that I'm honest, I'm not
lying and I count carefully and thoroughly

72
00:04:18,430 --> 00:04:20,220
so I don't make a mistake and I'm

73
00:04:20,220 --> 00:04:22,560
justified in believing that I haven't made
a mistake.

74
00:04:24,780 --> 00:04:28,130
I go to all five of the bakeries and I buy
a cookie from

75
00:04:28,130 --> 00:04:32,380
each bakery and I count the chips in the,
in the cookies that I bought.

76
00:04:32,380 --> 00:04:35,760
And it turns out that the cookie from
Bakery A has 11 chips in it.

77
00:04:35,760 --> 00:04:39,390
And the cookies from Bakery B, Bakery C,
Bakery D,

78
00:04:39,390 --> 00:04:43,100
Bakery E, they all have less than ten
chips in them.

79
00:04:43,100 --> 00:04:46,930
So now I can do two of these
generalizations from samples.

80
00:04:46,930 --> 00:04:47,180
Right?

81
00:04:47,180 --> 00:04:49,830
I can say, well, 100% of the cookies that

82
00:04:49,830 --> 00:04:54,530
I sampled from Bakery A have.
Between ten and 12 chips.

83
00:04:54,530 --> 00:04:59,130
Therefore 100% of the cookies from Bakery
A have between ten and 12 chips.

84
00:04:59,130 --> 00:05:05,150
But 0% of the cookies I sampled from
Bakery B have ten to 12 chips.

85
00:05:05,150 --> 00:05:10,140
So 0% of the cookies from Bakery B have
ten to 12 chips.

86
00:05:11,590 --> 00:05:12,409
what's wrong with that argument?

87
00:05:14,000 --> 00:05:14,920
Well I hope it's pretty

88
00:05:14,920 --> 00:05:20,100
clear the, the problem is you can't
generalize from just one cookie.

89
00:05:20,100 --> 00:05:22,790
The cookies aren't made in a totally
mechanical way where

90
00:05:22,790 --> 00:05:26,080
they count the chips before they put them
in the cookies.

91
00:05:26,080 --> 00:05:29,160
Then you can't know that every cookie is
the same, and

92
00:05:29,160 --> 00:05:33,270
if you can't know that every cookie's the
same, you can't generalize.

93
00:05:33,270 --> 00:05:37,210
From one cookie alone, to all the cookies
in the bakery, you might

94
00:05:37,210 --> 00:05:40,350
have gotten one that happened to have 11
chips, but all the other

95
00:05:40,350 --> 00:05:42,430
cookies in the bakery had less than 10.

96
00:05:42,430 --> 00:05:45,650
For the other bakeries for b,c,d,e, you
might

97
00:05:45,650 --> 00:05:47,810
have gotten a cookie that had less than 10

98
00:05:47,810 --> 00:05:50,690
when actually they all, all the rest of

99
00:05:50,690 --> 00:05:53,354
cookies in the store had between ten and
12.

100
00:05:55,410 --> 00:05:58,740
So when you generalize from such a small
sample, from a

101
00:05:58,740 --> 00:06:04,430
sample that's too small, then it's called,
the fallacy of hasty generalization.

102
00:06:04,430 --> 00:06:08,790
And it's so obvious, that it's hard to
believe,

103
00:06:08,790 --> 00:06:13,546
but it's actually often committed, it's a
very common fallacy.

104
00:06:13,546 --> 00:06:17,100
You know, your next-door neighbor will buy
a new car and it

105
00:06:17,100 --> 00:06:20,560
breaks down, and you say, aaah, that
kind of car is no good.

106
00:06:20,560 --> 00:06:25,380
Or you meet somebody from Sweden, and this
person from Sweden

107
00:06:25,380 --> 00:06:30,120
likes football, and so you say, aaah,
people from Sweden like football.

108
00:06:30,120 --> 00:06:36,090
and people just constantly generalize
extremely small samples, in order

109
00:06:36,090 --> 00:06:39,460
for generalizations that guide their
behavior in every day life.

110
00:06:40,510 --> 00:06:43,870
Sometimes they're right, a lot of times
they're wrong.

111
00:06:43,870 --> 00:06:45,880
That's when you have the fallacy of

112
00:06:45,880 --> 00:06:49,730
hasty generalization.
To avoid that fallacy.

113
00:06:49,730 --> 00:06:53,170
We need to ask a second question about
generalizations from

114
00:06:53,170 --> 00:06:57,600
samples, namely we have to ask, is the
sample large enough?

115
00:06:58,930 --> 00:07:00,770
Now I noticed that the samples come in
varying

116
00:07:00,770 --> 00:07:05,420
sizes from just one item to almost the
whole set.

117
00:07:05,420 --> 00:07:08,995
And so the question that we have to ask
first, is it large?

118
00:07:08,995 --> 00:07:11,200
True, but it's not clear

119
00:07:11,200 --> 00:07:12,160
what that means.

120
00:07:12,160 --> 00:07:15,410
What we really want to know is, is it
large enough?

121
00:07:15,410 --> 00:07:20,510
Because sometimes a very small sample can
be plenty big.

122
00:07:20,510 --> 00:07:23,700
Just imagine that you come across an apple
tree and you want

123
00:07:23,700 --> 00:07:27,580
to find out whether the apples off that
tree float in the water.

124
00:07:27,580 --> 00:07:29,270
So you bring in a tub of water, you pull
off

125
00:07:29,270 --> 00:07:31,660
an apple off the tree and you put it in
the tub.

126
00:07:31,660 --> 00:07:32,340
And it floats.

127
00:07:33,460 --> 00:07:36,720
Now you can generalize that all the
apples, or

128
00:07:36,720 --> 00:07:41,470
maybe almost all the apples off that tree
will float in water.

129
00:07:41,470 --> 00:07:44,020
One is a big enough sample in that case.

130
00:07:44,020 --> 00:07:45,330
Now why is that?

131
00:07:45,330 --> 00:07:49,690
It's because you have background
information from biology.

132
00:07:49,690 --> 00:07:54,060
That the apples on that tree are going to
be very similar to each other.

133
00:07:54,060 --> 00:07:55,840
Because of how they arose.

134
00:07:55,840 --> 00:08:01,020
And so sometimes, a single instance is
going to be enough.

135
00:08:01,020 --> 00:08:01,770
Even if it's not

136
00:08:01,770 --> 00:08:05,010
enough, when we're counting chips in
chocolate chip cookies.

137
00:08:07,260 --> 00:08:09,350
The other point about the sample being
large

138
00:08:09,350 --> 00:08:11,740
enough, that you need to keep in mind.

139
00:08:11,740 --> 00:08:15,900
Is that, whether it's large enough,
depends on what the stakes are.

140
00:08:17,210 --> 00:08:19,620
If you're testing a bunch of parachutes to
see

141
00:08:19,620 --> 00:08:24,310
if they work, you better not just check a
few.

142
00:08:24,310 --> 00:08:27,910
Even ten is not enough, like we used for
the chocolate chip cookies.

143
00:08:27,910 --> 00:08:29,980
You wana to check every parachute to make

144
00:08:29,980 --> 00:08:33,030
sure that it's packed properly, and is
going to work.

145
00:08:33,030 --> 00:08:35,290
Or you're going to have disaster when they
fail.

146
00:08:36,670 --> 00:08:38,815
Well, what about chocolate chip cookies?

147
00:08:38,815 --> 00:08:41,820
Suppose you take a sample of ten, and it

148
00:08:41,820 --> 00:08:45,230
turns out that, that sample is not really
representative?

149
00:08:45,230 --> 00:08:50,330
Big deal, so there are nine chips, or 13
chips.

150
00:08:50,330 --> 00:08:52,970
You know, it's just not that serious an
issue.

151
00:08:52,970 --> 00:08:58,410
So, a sample can be large enough, for
something that doesn't matter,

152
00:08:58,410 --> 00:09:02,290
like chocolate chip cookies, without being
large enough, for something

153
00:09:02,290 --> 00:09:07,560
that really does matter like, whether
parachutes are packed properly.

154
00:09:07,560 --> 00:09:11,680
So whether the sample is large enough
depends on the background information,

155
00:09:11,680 --> 00:09:16,220
that's what the apple case showed us And
also on what's at stake.

156
00:09:16,220 --> 00:09:23,630
That's what the parachute case showed us.
Next, let's assume that I'm not lying and

157
00:09:23,630 --> 00:09:25,130
the counts accurate.

158
00:09:25,130 --> 00:09:27,750
You're justified in believing that it's
accurate.

159
00:09:27,750 --> 00:09:28,990
And the sample's big enough.

160
00:09:28,990 --> 00:09:30,710
All of that is settled.
Alright?

161
00:09:30,710 --> 00:09:34,540
So, what I did was.
I went into the five bakeries.

162
00:09:34,540 --> 00:09:38,610
And I turned to the person behind the
counter, and I said, I'm doing this little

163
00:09:38,610 --> 00:09:40,550
survey, because I gotta figure out which
place

164
00:09:40,550 --> 00:09:42,860
I want to buy my chocolate chip cookies in
town.

165
00:09:42,860 --> 00:09:45,980
And, and I want to find out whether, how
many

166
00:09:45,980 --> 00:09:48,800
of your cookies have between ten and 12
chocolate chips.

167
00:09:48,800 --> 00:09:52,750
So, could you sell me ten chocolate
cookies and I'm going to do my survey.

168
00:09:52,750 --> 00:09:55,120
So, then I take ten cookies from each of
the

169
00:09:55,120 --> 00:09:58,850
five bakers and I bring 'em home and I
count them.

170
00:09:58,850 --> 00:10:02,910
And sure enough from Bakery A we found
that 80% of the

171
00:10:02,910 --> 00:10:06,950
cookies that are bought from Bakery A have
ten to 12 chips.

172
00:10:06,950 --> 00:10:09,230
and so I conclude that 80% of the cookies
that

173
00:10:09,230 --> 00:10:13,110
are made in Bakery A have 10 to 12 chips.

174
00:10:13,110 --> 00:10:13,870
and,

175
00:10:13,870 --> 00:10:15,750
that seems like a pretty good argument,
doesn't it?

176
00:10:15,750 --> 00:10:17,150
It's got a big enough sample.

177
00:10:17,150 --> 00:10:19,650
And if you don't believe that, let's say
it's 20 cookies.

178
00:10:21,610 --> 00:10:22,460
But there's still something wrong.

179
00:10:22,460 --> 00:10:25,140
There's something wrong with that
argument.

180
00:10:25,140 --> 00:10:26,210
What's wrong with that argument?

181
00:10:27,960 --> 00:10:31,820
Well, I told the guy behind the counter, I
was doing this

182
00:10:31,820 --> 00:10:34,330
survey to figure out where I was going to
buy my chocolate chip cookies.

183
00:10:35,540 --> 00:10:38,785
So if he wants me to buy chocolate chip
cookies from his cookie shop,

184
00:10:38,785 --> 00:10:45,220
from his bakery then he's going to look in
the counter for the ten

185
00:10:45,220 --> 00:10:48,710
cookies that look like they have about ten
to 12 chips in it.

186
00:10:48,710 --> 00:10:53,060
So might be no surprise that 80%of the
cookies that I sampled.

187
00:10:53,060 --> 00:10:56,020
Have ten to 12 chips because he picked out
the ones that did.

188
00:10:57,260 --> 00:11:00,950
This is called the power of biased
sampling.

189
00:11:00,950 --> 00:11:04,170
Sometimes the sample that you take is not
representative

190
00:11:04,170 --> 00:11:08,080
of the whole, because it's biased in a
certain way, in

191
00:11:08,080 --> 00:11:11,910
this case it was biased by the person
behind the counter and

192
00:11:11,910 --> 00:11:14,180
what he knew about what I was doing, and
what his

193
00:11:14,180 --> 00:11:16,449
motives were in trying to get me to buy
from his shop.

194
00:11:18,180 --> 00:11:23,510
But the fallacy of biased sampling is
something we have to watch out for.

195
00:11:23,510 --> 00:11:29,230
And what we need to do then to avoid this
fallacy is to ask a

196
00:11:29,230 --> 00:11:32,870
third question about all generalizations
from samples.

197
00:11:34,240 --> 00:11:36,183
We want to know whether the premises are
true and justified.

198
00:11:36,183 --> 00:11:38,870
We want to know whether the sample is
large enough we also have to

199
00:11:38,870 --> 00:11:44,410
ask Is the sample biased in any way that's
going to weaken the argument?

200
00:11:45,600 --> 00:11:50,030
It might seem that the fallacy of biased
sampling is so obvious that nobody

201
00:11:50,030 --> 00:11:53,860
who was careful and any good at what they
were doing would commit that mistake.

202
00:11:53,860 --> 00:11:54,890
But actually

203
00:11:54,890 --> 00:11:56,440
people do it all the time.

204
00:11:56,440 --> 00:11:59,380
Some of the top pollsters in history have
done it.

205
00:11:59,380 --> 00:12:01,970
The most famous example was Franklin
Deleanor

206
00:12:01,970 --> 00:12:06,350
Roosevelt who was running for president in
1936

207
00:12:06,350 --> 00:12:14,320
against Alf Landon and the Literary Digest
did a poll that took just tons of data.

208
00:12:14,320 --> 00:12:19,300
I can't remember how many tens of
thousands of letters they sent out.

209
00:12:19,300 --> 00:12:20,740
in this poll, and

210
00:12:20,740 --> 00:12:27,590
they reached a conclusion, form their poll
that Alf Landon was going to win 56 to

211
00:12:27,590 --> 00:12:34,530
44, but that's not the way it turned out.
Turned out that Roosevelt won 62 to 38.

212
00:12:34,530 --> 00:12:38,450
They were way off, they weren't even
close.

213
00:12:38,450 --> 00:12:39,420
So what was going on with that?

214
00:12:40,550 --> 00:12:45,936
Well they needed to get addresses of
people to send the survey to, and what did

215
00:12:45,936 --> 00:12:47,790
they use for that?
They used a phone book.

216
00:12:48,828 --> 00:12:55,020
But back then, remember this is 1936 a lot
of people didn't have phones and in

217
00:12:55,020 --> 00:12:57,990
particular poor people didn't have phones
and people

218
00:12:57,990 --> 00:13:00,120
who lived in a rural areas didn't have
phones.

219
00:13:00,120 --> 00:13:02,180
And it was those poor people, and people

220
00:13:02,180 --> 00:13:05,780
who lived in rural areas that loved
Franklin Roosevelt,

221
00:13:05,780 --> 00:13:08,950
because of the New Deal and all the
policies

222
00:13:08,950 --> 00:13:11,380
that he had put in that helped them out.

223
00:13:11,380 --> 00:13:14,300
So they voted for him, and he won by a
landslide.

224
00:13:14,300 --> 00:13:15,680
And the prediction had gone the other way.

225
00:13:15,680 --> 00:13:21,720
And it's all because of biased sampling.
And the same problem continues today.

226
00:13:21,720 --> 00:13:24,550
Many pollsters, especially when they
want to do a really

227
00:13:24,550 --> 00:13:28,790
quick poll, will call up people on their
phones.

228
00:13:28,790 --> 00:13:31,890
But there are lots of restrictions and
lots of problems with that.

229
00:13:31,890 --> 00:13:35,880
For one thing, you're not supposed to do
polls on cell phones.

230
00:13:36,950 --> 00:13:40,239
But a lot of young people only have cell
phones and don't have landlines.

231
00:13:41,580 --> 00:13:45,460
So that means that young people are going
to be under represented in the sample.

232
00:13:46,870 --> 00:13:51,860
And even if they have a landline, well,
they often have

233
00:13:51,860 --> 00:13:55,270
caller id, and they know it's a pollster,
so they don't answer.

234
00:13:55,270 --> 00:13:58,060
So get very low response rates.

235
00:13:58,060 --> 00:14:01,920
And some studies have found that women
tend to answer the phone more than men.

236
00:14:01,920 --> 00:14:04,190
So you get samples Screw the net
direction.

237
00:14:06,600 --> 00:14:09,930
So what do you do when you get it buy a
sample like this.

238
00:14:09,930 --> 00:14:13,020
Well if you know it's been biased in these
ways, then you

239
00:14:13,020 --> 00:14:17,860
try to correct for that, that's where the
pollsters don't all agree.

240
00:14:17,860 --> 00:14:21,010
They correct in different ways, because
think about it.

241
00:14:21,010 --> 00:14:22,548
Suppose if this time.

242
00:14:22,548 --> 00:14:27,830
You got a lot more people from the liberal
party than you did last time.

243
00:14:27,830 --> 00:14:31,290
That might mean that your sample is
skewed!

244
00:14:31,290 --> 00:14:31,660
But it might

245
00:14:31,660 --> 00:14:36,470
mean that there are just more people who
have moved over to the liberal party.

246
00:14:36,470 --> 00:14:37,590
Or to the conservative party.

247
00:14:37,590 --> 00:14:40,790
Either way, how do you know the proper way

248
00:14:40,790 --> 00:14:43,340
to correct, so as to get an accurate
answer?

249
00:14:43,340 --> 00:14:47,240
And different polling organizations use
different techniques.

250
00:14:47,240 --> 00:14:51,300
And that explains why very often the polls
reach quite

251
00:14:51,300 --> 00:14:54,980
different results about who's ahead in a,
in an election.

252
00:14:56,100 --> 00:14:56,770
Another reason why

253
00:14:56,770 --> 00:15:01,570
polls often reach different results, is
that some pollsters are dishonest.

254
00:15:01,570 --> 00:15:03,320
I know it comes as a shock.

255
00:15:03,320 --> 00:15:04,300
But it's true.

256
00:15:05,690 --> 00:15:09,010
And pollsters can sometimes reach the
conclusions that

257
00:15:09,010 --> 00:15:12,340
they want to reach by slanting their
questions.

258
00:15:12,340 --> 00:15:15,560
So the fourth question that we want to ask
about any

259
00:15:15,560 --> 00:15:20,670
generalization from samples is, was the
question slanted in some way?

260
00:15:20,670 --> 00:15:22,030
Which means.

261
00:15:22,030 --> 00:15:25,180
It was phrased in a way that made it more

262
00:15:25,180 --> 00:15:29,180
likely to reach one result rather than a
conflicting result.

263
00:15:30,180 --> 00:15:31,010
So how does this happen?

264
00:15:32,290 --> 00:15:35,820
Well a simple way to do it is to word the
question in such

265
00:15:35,820 --> 00:15:40,250
a way that people are going to feel bad
about giving a certain answer.

266
00:15:41,350 --> 00:15:46,650
So for example, if you want to find out
how many people in a certain society,

267
00:15:48,120 --> 00:15:53,710
think that it's, it's wrong to experiment
in scientific experiments on animals.

268
00:15:53,710 --> 00:15:57,730
Then you could always ask, one question if
you want

269
00:15:57,730 --> 00:16:00,010
one answer and another question if you
want another answer.

270
00:16:00,010 --> 00:16:04,540
So what about this one, supose they say is
it

271
00:16:04,540 --> 00:16:08,210
okay to kill a mouse in order to save a
human?

272
00:16:10,360 --> 00:16:11,230
Well I think most people are going to

273
00:16:11,230 --> 00:16:15,480
say yes, and then they're going to say so
they,

274
00:16:15,480 --> 00:16:18,960
those people who gave that answer, they
actually

275
00:16:18,960 --> 00:16:22,700
support scientific experments on mice to
save human lives.

276
00:16:22,700 --> 00:16:24,860
Therefore they support animal
experimentation.

277
00:16:26,160 --> 00:16:29,690
If you want to reach the other result in

278
00:16:29,690 --> 00:16:35,680
your poll, then you can say; should
scientists torture animals

279
00:16:35,680 --> 00:16:36,610
in their experiments?

280
00:16:37,780 --> 00:16:42,630
Nobody wants to say they are going to be
for torture, so their going to say no.

281
00:16:42,630 --> 00:16:43,760
Scientists shouldn't do that.

282
00:16:43,760 --> 00:16:46,370
And then, you can conclude, so most people
are

283
00:16:46,370 --> 00:16:48,450
against animal experimentation because

284
00:16:48,450 --> 00:16:51,050
animal experimentation is torturing
animals.

285
00:16:52,750 --> 00:16:54,250
In fact, you can go a little further, you

286
00:16:54,250 --> 00:16:59,850
can say, should scientists stop torturing
animals in their experiments?

287
00:16:59,850 --> 00:17:00,920
It's like, should.

288
00:17:00,920 --> 00:17:02,640
When did you stop beating your wife?

289
00:17:02,640 --> 00:17:05,980
It presupposes that they are torturing
animals.

290
00:17:05,980 --> 00:17:09,730
And then, people who say, of course they
should stop it.

291
00:17:09,730 --> 00:17:11,700
I didn't even realize they were doing it.

292
00:17:11,700 --> 00:17:16,070
Well, you're just guaranteeing the result
in your

293
00:17:16,070 --> 00:17:19,320
poll, by the way you ask the question.

294
00:17:19,320 --> 00:17:22,200
Another way to slant your questions in a
poll.

295
00:17:22,200 --> 00:17:26,770
Is to give the survey participants limited
options.

296
00:17:26,770 --> 00:17:30,880
So in one example the New York Times
magazine reported a poll about the Doris

297
00:17:30,880 --> 00:17:37,740
Day Animal League where they said 51% of
the people

298
00:17:37,740 --> 00:17:42,949
we surveyed think that chimpanzees should
be treated about the same as children.

299
00:17:44,530 --> 00:17:48,210
What happened in the survey was, they were
only given four options.

300
00:17:48,210 --> 00:17:52,040
They could say, chimpanzees should be
treated like property,

301
00:17:57,260 --> 00:17:58,968
or they should be treated a lot like
children, or

302
00:17:58,968 --> 00:18:02,610
they should be treated like adult humans
Or you're not sure.

303
00:18:03,970 --> 00:18:06,010
Notice that given those options, you don't
want to say chimps are

304
00:18:06,010 --> 00:18:10,800
like property because you can just destroy
your property if you want.

305
00:18:10,800 --> 00:18:12,590
If you've got an old car and you want to

306
00:18:12,590 --> 00:18:14,490
tear it to bits with a sledgehammer,
that's up to you.

307
00:18:14,490 --> 00:18:14,990
And

308
00:18:16,870 --> 00:18:21,830
they're not like adult humans because we
don't think they should

309
00:18:21,830 --> 00:18:26,270
be given schooling and so on, or votes or
so on.

310
00:18:26,270 --> 00:18:30,620
So you're pretty much left with either
saying you're not sure, and

311
00:18:30,620 --> 00:18:33,780
you feel like well I should have thought
about this a little bit.

312
00:18:33,780 --> 00:18:37,200
So most people were saying, 51% were
saying

313
00:18:37,200 --> 00:18:41,080
that chimpanzees ought to be treated
similar to children.

314
00:18:42,960 --> 00:18:46,860
So by limiting the options to a list

315
00:18:46,860 --> 00:18:49,740
where all the other options were
undesirable for one

316
00:18:49,740 --> 00:18:51,980
reason or another the posters can get you
to

317
00:18:51,980 --> 00:18:53,690
pick the option that they want you to
pick.

318
00:18:55,570 --> 00:18:59,990
And then the next trick is you report the
conclusion right.

319
00:18:59,990 --> 00:19:02,430
They actually didn't say.

320
00:19:02,430 --> 00:19:06,390
That 51% said that chimpanzees should be
treated similar to children.

321
00:19:06,390 --> 00:19:08,270
They said that the survey

322
00:19:08,270 --> 00:19:12,790
showed that primates should be treated the
same as children.

323
00:19:14,080 --> 00:19:18,840
They changed chimpanzees to primates which
includes a lot of very small

324
00:19:18,840 --> 00:19:23,379
primates that aren't nearly as intelligent
and close to us as chimps are.

325
00:19:24,580 --> 00:19:28,880
And they also said, it's not similar to
children, the same as children.

326
00:19:28,880 --> 00:19:33,330
So, you can also slant the pol.
Not by playing around with

327
00:19:33,330 --> 00:19:34,920
the question, but by playing around with
the

328
00:19:34,920 --> 00:19:38,210
way you report the conclusion, of the
survey.

329
00:19:40,390 --> 00:19:42,629
So those are some of the tricks that
people use.

330
00:19:43,820 --> 00:19:48,460
in order to reach a predetermined result
and to fool

331
00:19:48,460 --> 00:19:52,330
you into believing that their poll ,uh,
results are reliable.

332
00:19:52,330 --> 00:19:55,360
And we saw others mistakes that people
make

333
00:19:55,360 --> 00:19:56,970
even when they're not trying to fool you.

334
00:19:59,330 --> 00:20:03,910
So, what you need to do is to keep your
eye on all those, you need to ask

335
00:20:03,910 --> 00:20:07,670
these questions that I emphasized
throughout this lecture, namely

336
00:20:07,670 --> 00:20:11,820
you got to see are the premises true and
justified?

337
00:20:11,820 --> 00:20:13,880
Is the sample large enough?

338
00:20:14,900 --> 00:20:17,620
Is the sample biased in some way?

339
00:20:17,620 --> 00:20:21,690
And have the questions or the conclusions
been reported in a slated way?

340
00:20:21,690 --> 00:20:25,230
And there's a lot more to learn about
statistics.

341
00:20:25,230 --> 00:20:27,990
And again I want to emphasize it would be
useful to

342
00:20:27,990 --> 00:20:31,900
take a statistics course People ought to
know more about statistics.

343
00:20:31,900 --> 00:20:34,920
But we can't go into all of those details
here.

344
00:20:34,920 --> 00:20:39,910
Still, if you can just avoid these few
simple mistakes that I've been talking

345
00:20:39,910 --> 00:20:47,730
about, then you can avoid being misled in
a number of situations in everyday life.

