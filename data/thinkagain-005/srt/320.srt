1
00:00:03,360 --> 00:00:05,050
In the last couple of lectures, we've

2
00:00:05,050 --> 00:00:08,880
learned about the truth functional
connective conjunction.

3
00:00:08,880 --> 00:00:13,140
And we've learned about the truth
functional connective disjunction.

4
00:00:13,140 --> 00:00:15,700
Now both of these connectives can be used,
to

5
00:00:15,700 --> 00:00:19,610
take prepositions and put them together to
create new prepositions.

6
00:00:19,610 --> 00:00:23,380
Whose truth depends solely on the truth of

7
00:00:23,380 --> 00:00:27,660
the propositions that you used as their
ingredients.

8
00:00:27,660 --> 00:00:28,530
Now, in today's

9
00:00:28,530 --> 00:00:32,070
short lecture, I just want to make the
point that just

10
00:00:32,070 --> 00:00:37,250
as you can combine propositions using
conjunction and you can combine

11
00:00:37,250 --> 00:00:39,580
propositions using disjunction, you can

12
00:00:39,580 --> 00:00:42,499
also use conjunction and disjunction
together

13
00:00:42,499 --> 00:00:46,870
to string together a bunch of propositions
into a larger proposition.

14
00:00:46,870 --> 00:00:49,100
Let me give you an example to illustrate
this point.

15
00:00:50,140 --> 00:00:53,860
Suppose I say I'm going to tickle you with
this hand.

16
00:00:55,580 --> 00:01:00,750
And then I say, and I'm either going to
tickle you with

17
00:01:00,750 --> 00:01:06,240
this hand or with this hand, okay?

18
00:01:06,240 --> 00:01:09,650
Now here, what I've done is string
together three

19
00:01:09,650 --> 00:01:12,870
propositions, the proposition I'm going to
tickle you with

20
00:01:12,870 --> 00:01:16,770
this hand and the proposition and I'm
either going to

21
00:01:16,770 --> 00:01:20,450
tickle you with this hand, or with this
hand.

22
00:01:20,450 --> 00:01:23,320
So there, we've created a new proposition
by

23
00:01:23,320 --> 00:01:25,050
stringing together three other

24
00:01:25,050 --> 00:01:27,260
propositions, using conjunction and
disjunction.

25
00:01:27,260 --> 00:01:31,720
Now notice, we can construct a
truth-table,

26
00:01:31,720 --> 00:01:34,110
to show how the truth of the larger

27
00:01:34,110 --> 00:01:36,570
proposition, I'm going to tickle you with
this hand

28
00:01:36,570 --> 00:01:39,080
and with either this hand or this hand.

29
00:01:39,080 --> 00:01:41,580
How the truth of that larger proposition
depends on

30
00:01:41,580 --> 00:01:45,140
the truth of the propositions that go into
creating it.

31
00:01:45,140 --> 00:01:45,370
Okay,

32
00:01:45,370 --> 00:01:47,360
so there are three propositions that go
into

33
00:01:47,360 --> 00:01:50,200
creating it, and they're connected in
different ways.

34
00:01:50,200 --> 00:01:53,285
So one of the propositions was, I'm
going to tickle you with this hand.

35
00:01:53,285 --> 00:01:58,660
Let's find terms to distinguish the hands,
so I

36
00:01:58,660 --> 00:02:01,260
don't have to keep waving around all my
hands.

37
00:02:01,260 --> 00:02:02,570
Let's call this hand number one.

38
00:02:03,722 --> 00:02:06,200
Now the proposition we can say is I'm
going to tickle you with

39
00:02:06,200 --> 00:02:10,235
hand number one and with either hand
number two or hand number three.

40
00:02:10,235 --> 00:02:15,890
Okay, so there are three propositions that
go into creating that larger proposition.

41
00:02:15,890 --> 00:02:17,515
So the three propositions are these.

42
00:02:17,515 --> 00:02:21,005
First, I'm going to tickle you with hand
number one.

43
00:02:21,005 --> 00:02:25,380
Second, I'm going to tickle you with hand
number two.

44
00:02:25,380 --> 00:02:29,215
And third, I'm going to tickle you with
hand number three.

45
00:02:29,215 --> 00:02:35,280
Now, we can use the truth-table to show
when precisely

46
00:02:35,280 --> 00:02:38,162
it would be true that I'm going to tickle
you with hand

47
00:02:38,162 --> 00:02:41,286
number one and with either hand number two
or hand number three.

48
00:02:41,286 --> 00:02:42,162
Right.

49
00:02:42,162 --> 00:02:46,480
How does the truth of that whole statement
depend on

50
00:02:46,480 --> 00:02:50,182
the truth of the three prepositions that
go into creating it?

51
00:02:50,182 --> 00:02:54,880
Well, that whole statement I'm going to
tickle you with hand number one,

52
00:02:54,880 --> 00:02:58,250
and with either hand number two or hand
number three is a conjunction.

53
00:02:58,250 --> 00:03:00,175
It's a conjunction with two conjuncts.

54
00:03:00,175 --> 00:03:03,266
The first conjunct is I'm going to tickle
you with hand number one.

55
00:03:03,266 --> 00:03:06,620
And the second conjunct is, I'm going to
tickle you

56
00:03:06,620 --> 00:03:08,460
with either hand number two or hand number
three.

57
00:03:09,940 --> 00:03:13,040
So, since it's a conjunction, we know that
for it

58
00:03:13,040 --> 00:03:16,400
to be true, both of it's conjuncts have to
be true.

59
00:03:16,400 --> 00:03:19,530
So, it has to be true that I'm going to
tickle you with hand number one.

60
00:03:19,530 --> 00:03:22,170
So that's a, that proposition, I'm
going to tickle you

61
00:03:22,170 --> 00:03:24,501
with hand number one, that proposition has
to be true.

62
00:03:24,501 --> 00:03:25,514
Does it have

63
00:03:25,514 --> 00:03:30,273
to be true that I'm going to tickle you
with hand number two?

64
00:03:30,273 --> 00:03:30,480
No.

65
00:03:30,480 --> 00:03:32,340
All that has to be true is that I'm either
going to

66
00:03:32,340 --> 00:03:34,598
tickle you with hand number two or with
hand number three.

67
00:03:34,598 --> 00:03:39,480
So that disjunction, I'm going to tickle
you with hand number

68
00:03:39,480 --> 00:03:42,469
two or with hand number three, has to be
true.

69
00:03:43,770 --> 00:03:46,750
But for the disjunction to be true, it
doesn't have to

70
00:03:46,750 --> 00:03:49,050
be true that I'm going to tickle you with
hand number two.

71
00:03:49,050 --> 00:03:50,770
After all, there's a scenario

72
00:03:50,770 --> 00:03:53,850
where I'm not going to tickle you with
hand number two and yet it's still

73
00:03:53,850 --> 00:03:55,080
true that I'm going to tickle you with

74
00:03:55,080 --> 00:03:56,590
either hand number two or hand number
three.

75
00:03:58,250 --> 00:04:00,130
And for the disjunction, I'm going to
tickle you

76
00:04:00,130 --> 00:04:01,550
with hand number two or hand number three
to

77
00:04:01,550 --> 00:04:03,680
be true, it doesn't have to be true

78
00:04:03,680 --> 00:04:05,560
that I'm going to tickle you with hand
number three.

79
00:04:05,560 --> 00:04:08,040
Again, remember, there is a scenario where
I'm not

80
00:04:08,040 --> 00:04:10,200
going to tickle you with hand number
three, but

81
00:04:10,200 --> 00:04:11,750
it's still going to be the case that I'm
going to

82
00:04:11,750 --> 00:04:13,680
tickle you with either hand number two or
number three.

83
00:04:17,110 --> 00:04:19,570
But what does have to be true for the
disjunction to be

84
00:04:19,570 --> 00:04:23,170
true, is that at least one of those other
two propositions are true.

85
00:04:23,170 --> 00:04:26,240
It's at least true that either, I'm going
to tickle you with hand

86
00:04:26,240 --> 00:04:30,148
number two, or I'm going to tickle you
with hand number three, or both.

87
00:04:30,148 --> 00:04:34,039
But at least one of those has to be true.

88
00:04:35,210 --> 00:04:38,840
So, for the whole proposition, I'm
going to tickle you with hand

89
00:04:38,840 --> 00:04:41,890
number one and with hand number two or
hand number three.

90
00:04:41,890 --> 00:04:42,180
For that

91
00:04:42,180 --> 00:04:45,680
whole proposition to be true, it's gotta
be true that I'm

92
00:04:45,680 --> 00:04:50,610
going to tickle you with hand number one,
and it's either gotta be

93
00:04:50,610 --> 00:04:53,690
true that I'm going to tickle you with
hand number two or it's

94
00:04:53,690 --> 00:04:55,920
gotta be true that I'm going to tickle you
with hand number three.

95
00:04:57,600 --> 00:05:00,325
So if we look at the truth-table, we can
see

96
00:05:00,325 --> 00:05:06,000
precisely which lines are going to make
the whole proposition true.

97
00:05:07,230 --> 00:05:10,390
So here's how we can construct a
truth-table

98
00:05:10,390 --> 00:05:15,134
for a complex proposition that combines
conjunction and disjunction.

99
00:05:16,450 --> 00:05:19,179
And notice, by the way, when we're
combining conjunction

100
00:05:19,179 --> 00:05:24,480
and disjunction, the order in which we
combine them matters.

101
00:05:24,480 --> 00:05:30,200
We can say that conjunction and
disjunction are not associative.

102
00:05:30,200 --> 00:05:33,288
What matters to determining the
truth-table

103
00:05:33,288 --> 00:05:37,410
of the proposition that they're used to
create is not just

104
00:05:37,410 --> 00:05:39,940
the occurrence of conjunction and
disjunction

105
00:05:39,940 --> 00:05:41,358
but what order they're taken in.

106
00:05:41,358 --> 00:05:45,110
In the proposition that I considered just
a moment ago, I'm going to

107
00:05:45,110 --> 00:05:48,060
tickle you with hand number one and with
either hand number two or

108
00:05:48,060 --> 00:05:53,710
hand number three, that proposition has a
different truth table than

109
00:05:53,710 --> 00:05:58,800
the proposition I'm going to tickle you
with hand number one and hand number two,

110
00:05:58,800 --> 00:06:01,910
or I'm going to tickle you with hand
number three.

111
00:06:03,010 --> 00:06:06,760
That second proposition has a very
different truth-table from the first.

112
00:06:06,760 --> 00:06:10,200
As we can see if we construct the two
truth-tables and compare them.

113
00:06:10,200 --> 00:06:15,160
So, conjunction and disjunction are not
associative.

114
00:06:15,160 --> 00:06:19,150
The truth table, for the proposition that
they create, depends

115
00:06:19,150 --> 00:06:23,871
on the order in which we apply conjunction
and disjunction.

