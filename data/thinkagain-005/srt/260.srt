1
00:00:02,750 --> 00:00:06,850
So far, we've looked at the language of
argument in some detail.

2
00:00:06,850 --> 00:00:11,510
Because we've separated the reason markers
from conclusion markers.

3
00:00:11,510 --> 00:00:14,440
And we've talked about assuring, and

4
00:00:14,440 --> 00:00:18,460
guarding, and discounting, and evaluative
words.

5
00:00:18,460 --> 00:00:20,480
So we've picked out a lot of different

6
00:00:20,480 --> 00:00:24,390
words in language that play distinct roles
in arguments.

7
00:00:24,390 --> 00:00:28,070
What we need to do for a real argument is
to bring it all together and

8
00:00:28,070 --> 00:00:32,170
show how these types of words can work
together in a single passage,

9
00:00:32,170 --> 00:00:36,870
and to do that we're going to learn a
method called Close Analysis.

10
00:00:36,870 --> 00:00:42,660
And what you do with Close Analysis is you
simply take a passage,

11
00:00:42,660 --> 00:00:47,460
and you mark the words in that passage
that play those roles.

12
00:00:47,460 --> 00:00:53,390
So, a Reason Marker you can mark with an
R, and a Conclusion Marker

13
00:00:53,390 --> 00:00:59,050
you can mark with a C, Assuring Term you
mark with an A.

14
00:00:59,050 --> 00:01:04,870
a Guarding Term marked with G.
A Discounting Term you mark with D.

15
00:01:06,080 --> 00:01:09,440
An Evaluative Term marked with E, and if
it's clear you put a

16
00:01:09,440 --> 00:01:11,090
plus or a minus to indicate

17
00:01:11,090 --> 00:01:14,830
whether it's positive evaluation or
negative evaluation.

18
00:01:16,490 --> 00:01:18,740
Now these marks will just be scratching

19
00:01:18,740 --> 00:01:19,640
the surface.

20
00:01:19,640 --> 00:01:22,550
There's obviously a lot more that you can
do and

21
00:01:22,550 --> 00:01:25,660
need to do in order to fully understand
the passage.

22
00:01:25,660 --> 00:01:28,734
So, when it's a Discounting Term you ought

23
00:01:28,734 --> 00:01:31,540
to think about which objection is being
discounted.

24
00:01:31,540 --> 00:01:39,220
And you also ought to think about the
rhetorical moves, the metaphors and irony.

25
00:01:39,220 --> 00:01:44,270
We'll look at rhetorical questions and
we'll basically go through the passage

26
00:01:44,270 --> 00:01:47,120
very carefully word by word in order to

27
00:01:47,120 --> 00:01:49,300
figure out what's going on in that
passage.

28
00:01:50,810 --> 00:01:53,260
So, how do you learn the technique?

29
00:01:53,260 --> 00:01:54,650
The answer is very simple.

30
00:01:54,650 --> 00:01:57,040
You practice and then you practice again,
and

31
00:01:57,040 --> 00:01:59,730
then you practice, and practice, and
practice, and practice.

32
00:01:59,730 --> 00:02:03,720
Practice won't make perfect because
nothing's perfect.

33
00:02:03,720 --> 00:02:05,870
But practice will surely help a lot.

34
00:02:05,870 --> 00:02:10,170
And we'll get better and better the more
we practice.

35
00:02:10,170 --> 00:02:12,320
So in this lecture, what we're going to do
is

36
00:02:12,320 --> 00:02:15,460
go through one example in a lot of detail.

37
00:02:15,460 --> 00:02:20,665
And mark it up very carefully, in order to
practice the method of close analysis.

38
00:02:20,665 --> 00:02:26,360
The particular example we chose for this
lecture is by Robert Redford.

39
00:02:26,360 --> 00:02:30,120
It's an op-ed that was written for the
Washington Post.

40
00:02:30,120 --> 00:02:32,470
We chose it because it's an interesting
issue.

41
00:02:32,470 --> 00:02:34,115
It's about the environment.

42
00:02:34,115 --> 00:02:35,350
But it's not

43
00:02:35,350 --> 00:02:39,870
an issue that people will necessarily have
very strong emotions about.

44
00:02:39,870 --> 00:02:41,640
Because you might not even know the
particular

45
00:02:41,640 --> 00:02:44,010
part of the environment that he's talking
about.

46
00:02:45,450 --> 00:02:49,190
We also chose it because it's a really
good argument.

47
00:02:49,190 --> 00:02:51,570
You learn how to analyze arguments and how
to

48
00:02:51,570 --> 00:02:54,980
formulate your own arguments by looking at
good examples.

49
00:02:54,980 --> 00:02:58,190
Course it's fun to tear down bad examples,
but

50
00:02:58,190 --> 00:03:00,980
we need a nice model of a good argument,

51
00:03:00,980 --> 00:03:04,750
in order to, see what's lacking, in the
arguments that are bad.

52
00:03:04,750 --> 00:03:06,610
So we're going to go through an example,

53
00:03:06,610 --> 00:03:09,160
partly because, it's actually a pretty
good argument.

54
00:03:10,740 --> 00:03:14,300
We're also going to go through this
passage because it's really thick.

55
00:03:14,300 --> 00:03:16,580
With these argument words.

56
00:03:16,580 --> 00:03:20,530
So you'll see that we're marking a lot of
different things and we'll have

57
00:03:20,530 --> 00:03:23,240
to go through it paragraph by paragraph,

58
00:03:23,240 --> 00:03:25,910
and sentence by sentence, and word by
word,

59
00:03:25,910 --> 00:03:27,000
in great detail.

60
00:03:27,000 --> 00:03:32,190
This lecture will seem like it's looking
at the passage with a microscope.

61
00:03:32,190 --> 00:03:35,290
and that's the point, to learn to analyze.

62
00:03:35,290 --> 00:03:39,670
With a microscope.
The passages where people give arguments.

63
00:03:39,670 --> 00:03:42,500
Okay, so the first sentence says, just

64
00:03:42,500 --> 00:03:44,570
over a year ago President Clinton created
the

65
00:03:44,570 --> 00:03:48,140
Grand Staircase Escalante National
Monument to protect

66
00:03:48,140 --> 00:03:51,550
once and for all some of Utah's
extraordinary

67
00:03:51,550 --> 00:03:55,020
red rock canyon country.
Word number one.

68
00:03:55,020 --> 00:03:59,960
Just.
Well, justice is a good thing, right?

69
00:03:59,960 --> 00:04:04,010
So that must be an evaluative word.
No.

70
00:04:04,010 --> 00:04:07,040
One of the first lessons in close analysis
is that simply

71
00:04:07,040 --> 00:04:11,260
because you have the word just, doesn't
mean you're talking about justice.

72
00:04:11,260 --> 00:04:13,720
When he says, just over a year ago,

73
00:04:13,720 --> 00:04:16,780
he means slightly over a year ago, or
somewhat

74
00:04:16,780 --> 00:04:20,240
over a year ago, or sometime over a year
ago.

75
00:04:20,240 --> 00:04:23,230
So maybe he's guarding, you might want to
mark this

76
00:04:23,230 --> 00:04:27,380
one as a Guarding Term, by putting a g out
there.

77
00:04:27,380 --> 00:04:31,880
But, he's not using an evaluation.

78
00:04:31,880 --> 00:04:35,060
To say, just over a year ago, well why
would he guard?

79
00:04:35,060 --> 00:04:37,230
Because, he's not very precise.

80
00:04:37,230 --> 00:04:41,650
He's not going to say, 17 days over a year
ago, he's saying, just over a year ago,

81
00:04:41,650 --> 00:04:44,030
so that nobody will raise a question at
this point.

82
00:04:44,030 --> 00:04:49,930
He does not want people raising questions
this early in the op-ed.

83
00:04:49,930 --> 00:04:51,360
So let's keep going.

84
00:04:51,360 --> 00:04:52,740
Just over a year ago, President

85
00:04:52,740 --> 00:04:58,040
Clinton created the Grand
Staircase-Escalante National Monument

86
00:04:58,040 --> 00:05:02,505
to protect once and for all some of the
extraordinary red rock country.

87
00:05:02,505 --> 00:05:04,380
Okay?

88
00:05:04,380 --> 00:05:06,480
What about the word, to?

89
00:05:09,120 --> 00:05:12,290
Might seem like not much, because it's
such a short word.

90
00:05:12,290 --> 00:05:15,730
But, it's actually doing a lot of work
there if you think about it.

91
00:05:16,900 --> 00:05:24,330
We actually, I think, should mark it as a,
an Argument Marker of some sort.

92
00:05:24,330 --> 00:05:26,850
Is it a Reason Marker or is it a
Conclusion Marker?

93
00:05:26,850 --> 00:05:28,280
We'll come back to that.

94
00:05:28,280 --> 00:05:31,200
But first let's get clear that it's an
Argument Marker of some sort.

95
00:05:32,830 --> 00:05:34,270
When he says that he created

96
00:05:34,270 --> 00:05:37,950
the monument to protect once and for all,
he means in order to

97
00:05:37,950 --> 00:05:42,550
protect because he wanted to protect once
and for all some of that country.

98
00:05:42,550 --> 00:05:45,570
It's an explanation of why he created it.

99
00:05:45,570 --> 00:05:48,770
It's giving you the teleological
explanation which tells

100
00:05:48,770 --> 00:05:50,796
you the purpose for which he created it.

101
00:05:50,796 --> 00:05:55,040
So the bit that comes out, protect once
and

102
00:05:55,040 --> 00:05:59,460
for all some of the country, is the reason
why

103
00:05:59,460 --> 00:06:04,070
he created it, that explains the
conclusion that he did create it.

104
00:06:04,070 --> 00:06:06,700
So this is a Reason Marker.

105
00:06:10,210 --> 00:06:14,410
Now the next word, protect.

106
00:06:14,410 --> 00:06:18,780
Well you might think that, protect is a
neutral word.

107
00:06:18,780 --> 00:06:24,090
Because after all, protectionism is
criticized by some people.

108
00:06:24,090 --> 00:06:26,855
But actually to protect something is to
keep it safe.

109
00:06:26,855 --> 00:06:31,660
To keep it safe from harm, to keep it safe
from bad things happening to it.

110
00:06:31,660 --> 00:06:35,705
So to explain what counts as protection
and what doesn't count as protection,

111
00:06:35,705 --> 00:06:40,930
you have to cite what's good or bad.
And that makes it an evaluative word.

112
00:06:40,930 --> 00:06:44,260
And in this case, protecting is a good
thing.

113
00:06:44,260 --> 00:06:47,890
So it gets marked as e plus.

114
00:06:47,890 --> 00:06:52,910
Okay, the next words are, once and for
all.

115
00:06:52,910 --> 00:06:54,385
What does once and for all do?

116
00:06:54,385 --> 00:06:56,660
Nothing.

117
00:06:56,660 --> 00:07:00,120
Some of these words are going to get
marked as nothing whatsoever.

118
00:07:02,180 --> 00:07:04,370
Because, once and for all doesn't guard,
it

119
00:07:04,370 --> 00:07:07,870
says once and for all, it's the absolute
limit.

120
00:07:07,870 --> 00:07:09,362
But the next word.

121
00:07:09,362 --> 00:07:12,910
Some and what does that do?
That guards.

122
00:07:14,220 --> 00:07:20,450
It's saying that what's protected is not
all of Utah's red rock country.

123
00:07:20,450 --> 00:07:21,715
It's only some of it.

124
00:07:21,715 --> 00:07:24,980
And it's important for him to guard that,
because

125
00:07:24,980 --> 00:07:27,275
he wants to say later on as we'll see.

126
00:07:27,275 --> 00:07:32,020
But there's lots of it outside the
monument that's not getting protected.

127
00:07:32,020 --> 00:07:34,080
So he wants to guard it and say it's

128
00:07:34,080 --> 00:07:37,390
not all that's going to be important to
his argument.

129
00:07:38,550 --> 00:07:43,320
Now, Utah is pretty neutral, unless you're
from that state, then

130
00:07:43,320 --> 00:07:45,580
you love it and you might say that's an
evaluative word.

131
00:07:45,580 --> 00:07:47,735
But let's skip that group of people right
now.

132
00:07:47,735 --> 00:07:52,060
Extraordinary, what about extraordinary?

133
00:07:52,060 --> 00:07:52,640
Is that

134
00:07:52,640 --> 00:07:53,730
an evaluative word?

135
00:07:53,730 --> 00:07:57,660
It might seem to be an evaluative word
because clearly, what

136
00:07:57,660 --> 00:08:03,620
Redford means is extraordinarily beautiful
or extraordinarily good red rock country.

137
00:08:03,620 --> 00:08:06,439
But the word extraordinary doesn't say
extraordinarily good.

138
00:08:07,540 --> 00:08:10,220
You can have things that are
extraordinarily bad.

139
00:08:10,220 --> 00:08:12,270
To say it's extraordinary is to say it's
out of

140
00:08:12,270 --> 00:08:16,820
the ordinary and the red rock country
might be extraordinarily ugly.

141
00:08:16,820 --> 00:08:18,140
So the word extraordinary

142
00:08:18,140 --> 00:08:24,610
itself is not by itself an evaluative
word, so it should be marked as nothing.

143
00:08:24,610 --> 00:08:28,460
And red rock country.
Also is going to be neutral.

144
00:08:28,460 --> 00:08:32,530
It's beautiful stuff, but simply describe
it as made out of red rock.

145
00:08:32,530 --> 00:08:37,380
Doesn't say that it's beautiful, even
though, we all know that it is.

146
00:08:37,380 --> 00:08:38,410
Just look at the picture.

147
00:08:48,190 --> 00:08:52,780
So, now we finished a whole sentence!
Isn't that great?

148
00:08:52,780 --> 00:08:54,180
A whole sentence.

149
00:08:54,180 --> 00:08:55,731
Oh, wow.
>>Wow!

150
00:08:55,731 --> 00:09:01,390
>>And all we did was find six things to
mark in that sentence.

151
00:09:01,390 --> 00:09:03,875
Well, four were marked and two were
nothing.

152
00:09:03,875 --> 00:09:08,130
But it shows you that you can go through a
single sentence.

153
00:09:08,130 --> 00:09:10,670
Do a lot of analysis in order to figure

154
00:09:10,670 --> 00:09:13,900
out what's going on and we're just getting
started.

155
00:09:13,900 --> 00:09:15,907
Now let's move on to the second sentence.

156
00:09:15,907 --> 00:09:21,475
It starts in response to plans of the
Dutch company to mine coal.

157
00:09:21,475 --> 00:09:26,315
President Clinton used his authority to
establish the new monument and so on.

158
00:09:26,315 --> 00:09:29,310
Let's go to end response to.

159
00:09:29,310 --> 00:09:30,235
What does that tell you?

160
00:09:30,235 --> 00:09:33,440
It tells you that what's coming after

161
00:09:33,440 --> 00:09:37,940
it explains why president Clinton used his
authority.

162
00:09:37,940 --> 00:09:39,100
It was a response

163
00:09:39,100 --> 00:09:44,750
to the plans of the Dutch company.
Which means that it's an explanation.

164
00:09:44,750 --> 00:09:49,879
Notice that the previous explanation says
why Clinton wanted to do it in general.

165
00:09:50,910 --> 00:09:54,540
This explanation tells you why President
Clinton did it

166
00:09:54,540 --> 00:09:57,300
at that particular time rather than
earlier or later.

167
00:09:57,300 --> 00:10:00,340
It's because he was responding to
particular plans by

168
00:10:00,340 --> 00:10:04,150
a particular company, so the end response
to is an

169
00:10:04,150 --> 00:10:05,130
Argument Marker.

170
00:10:05,130 --> 00:10:08,625
Now, is it a Reason Marker or a Conclusion
Marker?

171
00:10:08,625 --> 00:10:10,450
Well the conclusion, the thing that is

172
00:10:10,450 --> 00:10:13,650
getting explained is that Clinton used his
authority.

173
00:10:13,650 --> 00:10:17,099
So, this must be a Reason, or a Premise
Marker.

174
00:10:17,099 --> 00:10:20,805
You can also put P for Premise Marker, or
R for Reason Marker.

175
00:10:20,805 --> 00:10:24,200
Now, in response to plans of the Dutch

176
00:10:24,200 --> 00:10:27,760
company Andalex to mine coal on the
Kaiparowits Plateau.

177
00:10:27,760 --> 00:10:29,250
President Clinton used

178
00:10:29,250 --> 00:10:35,230
his authority under the Antiquities Act to
establish the new monument.

179
00:10:35,230 --> 00:10:37,900
Now this is actually a pretty tricky one.

180
00:10:37,900 --> 00:10:41,580
We know that the plans of the company, are
the

181
00:10:41,580 --> 00:10:46,190
premise that explains the conclusion that
Clinton used his authority.

182
00:10:46,190 --> 00:10:47,894
But what's the word, under doing.

183
00:10:47,894 --> 00:10:52,310
Well, under means, it's the Antiquities
Act

184
00:10:52,310 --> 00:10:54,260
that gave him that authority, that
explains

185
00:10:54,260 --> 00:10:55,430
why he had the authority.

186
00:10:55,430 --> 00:10:57,870
And justified him in doing what he was
doing.

187
00:10:57,870 --> 00:11:00,390
Namely, establishing the monument.

188
00:11:00,390 --> 00:11:05,010
So the word under suggests that there's
another argument in the background here.

189
00:11:05,010 --> 00:11:07,960
That the Antiquities Act gives the
President the authority

190
00:11:07,960 --> 00:11:12,040
to establish monuments, and President
Clinton used that authority.

191
00:11:12,040 --> 00:11:15,930
So the Antiquities Act is again a premise
or as

192
00:11:15,930 --> 00:11:19,450
I said you can call it a Reason Marker
for.

193
00:11:19,450 --> 00:11:22,280
The premise that the Antiquities act gives
the president

194
00:11:22,280 --> 00:11:25,850
that authority and that justifies Clinton
in using his

195
00:11:25,850 --> 00:11:29,330
authority or explains why he was able to
abolish

196
00:11:29,330 --> 00:11:34,295
the monument and the word to also
indicates that.

197
00:11:34,295 --> 00:11:38,530
What comes after it is establishing the
new monument.

198
00:11:38,530 --> 00:11:39,930
That's what he was trying to do.

199
00:11:39,930 --> 00:11:44,450
That also is an argument that explains why
he did it.

200
00:11:44,450 --> 00:11:49,730
He had the authority, but you don't always
exercise your authority, right?

201
00:11:49,730 --> 00:11:52,920
And so the point of exercising the
authority,

202
00:11:52,920 --> 00:11:56,310
the real reason why he exercised his
authority, was.

203
00:11:56,310 --> 00:11:57,935
To establish the new monument.

204
00:11:57,935 --> 00:12:00,680
Again, it might seem tricky to keep citing
the

205
00:12:00,680 --> 00:12:03,150
word to as an argument marker, but think
about it.

206
00:12:03,150 --> 00:12:05,540
You can substitute in order to.

207
00:12:05,540 --> 00:12:09,590
He used his authority in order to
establish the

208
00:12:09,590 --> 00:12:14,630
new monument or because he wanted to
establish the new monument.

209
00:12:14,630 --> 00:12:17,467
And we learned a few lectures ago, that if
you

210
00:12:17,467 --> 00:12:22,010
can substitute another Argument Marker for
this particular word, then

211
00:12:22,010 --> 00:12:25,130
that shows that in this case, the word to
is

212
00:12:25,130 --> 00:12:28,260
getting used as an Argument Marker, in
this case, the premise.

213
00:12:28,260 --> 00:12:30,800
Because it's his wanting to establish the

214
00:12:30,800 --> 00:12:33,698
monument that explains why he used his
authority.

215
00:12:33,698 --> 00:12:34,414
Okay?

216
00:12:34,414 --> 00:12:40,590
And here's a tricky one.
What about the word authority?

217
00:12:40,590 --> 00:12:44,770
Well that's a really tricky word and
sometimes it's not completely clear.

218
00:12:44,770 --> 00:12:46,232
How do you want to mark it?

219
00:12:46,232 --> 00:12:47,720
Right?

220
00:12:47,720 --> 00:12:52,185
You might think that this word is getting
used as a discounting word.

221
00:12:52,185 --> 00:12:56,030
Namely, answering a potential objection.

222
00:12:56,030 --> 00:12:58,230
Some people might say, he didn't have the
authority to do that.

223
00:12:59,450 --> 00:13:02,055
But you might think it's a positive

224
00:13:02,055 --> 00:13:04,800
evaluation, having authority is a good
thing.

225
00:13:06,240 --> 00:13:10,500
And you might think that it's an Argument
Marker because it's

226
00:13:10,500 --> 00:13:13,320
a reason why he would have the ability to
set up.

227
00:13:13,320 --> 00:13:16,260
The monument, namely that he had the
authority, but

228
00:13:16,260 --> 00:13:19,550
it doesn't actually say, openly, any of
those things.

229
00:13:19,550 --> 00:13:24,490
So I would probably mark that as a
nothing, but I think it's better just

230
00:13:24,490 --> 00:13:28,160
to put a question mark, because sometimes
words are

231
00:13:28,160 --> 00:13:31,140
not going to have one clear function or
another.

232
00:13:31,140 --> 00:13:32,620
You know we're doing our best to put them

233
00:13:32,620 --> 00:13:35,760
into these little bins of the different
types of words.

234
00:13:35,760 --> 00:13:38,890
But sometimes they're not going to fall
neatly into one

235
00:13:38,890 --> 00:13:41,540
or the other and you just have to
recognize that.

236
00:13:41,540 --> 00:13:43,120
Of course when it comes to the quizzes
we're

237
00:13:43,120 --> 00:13:45,060
not going to ask you about those kinds of
words.

238
00:13:45,060 --> 00:13:46,502
But it's worth knowing that they are
there.

239
00:13:46,502 --> 00:13:47,774
Okay?

240
00:13:47,774 --> 00:13:53,100
Now, let's move on.
Setting aside for protection.

241
00:13:53,100 --> 00:13:56,055
What he described as some of the most
remarkable land in the world.

242
00:13:56,055 --> 00:14:00,520
Again, what is that telling you?

243
00:14:00,520 --> 00:14:02,040
Setting aside for protection.

244
00:14:02,040 --> 00:14:06,280
That tells you why he used his authority
to establish the monument.

245
00:14:06,280 --> 00:14:09,400
So again, we've got an implicit reason
here.

246
00:14:09,400 --> 00:14:12,920
But notice there's just a space, there's
no actual word

247
00:14:12,920 --> 00:14:15,200
that can be marked as an Argument Marker.

248
00:14:15,200 --> 00:14:17,160
But still, there is a separate argument
here.

249
00:14:17,160 --> 00:14:21,980
He set it aside for the protection, that
was why he established the monument.

250
00:14:21,980 --> 00:14:24,790
That's why he used his authority to
establish the monument, if

251
00:14:24,790 --> 00:14:26,970
you want to include that part of the
argument, as well.

252
00:14:28,010 --> 00:14:28,496
Okay.

253
00:14:28,496 --> 00:14:32,260
For protection.
Protection again.

254
00:14:32,260 --> 00:14:34,260
That's going to be evaluative, right?

255
00:14:34,260 --> 00:14:38,080
Because to protect something is to keep it
safe from harm, harm is

256
00:14:38,080 --> 00:14:42,300
bad, so protecting it must be good.
When you explain what protection is,

257
00:14:42,300 --> 00:14:48,400
you're going to need to use the words good
and bad, as we saw in the first sentence.

258
00:14:50,150 --> 00:14:53,070
What about these little quotation marks?

259
00:14:53,070 --> 00:14:56,630
I love quotation marks.
You gotta watch out for 'em.

260
00:14:56,630 --> 00:14:59,490
What he described as some of the most

261
00:14:59,490 --> 00:15:03,490
remarkable land in the world, why is
Robert Redford

262
00:15:03,490 --> 00:15:08,530
quoting President Clinton and saying how
Clinton described this land?

263
00:15:09,790 --> 00:15:12,890
Because if you're trying to convince
Clinton, and trying

264
00:15:12,890 --> 00:15:16,990
to convince the general public to try to
convince Clinton.

265
00:15:16,990 --> 00:15:19,370
There's nothing better than quoting
Clinton himself.

266
00:15:19,370 --> 00:15:23,090
I mean, after all, Clinton can't say I'm
not an authority.

267
00:15:23,090 --> 00:15:23,610
Right?

268
00:15:23,610 --> 00:15:27,550
So those quotation marks and saying that
he described it.

269
00:15:27,550 --> 00:15:29,463
That all amounts to assuring.

270
00:15:29,463 --> 00:15:33,041
He's assuring Clinton that, that has to be

271
00:15:33,041 --> 00:15:36,790
true, because after all, you said it
yourself.

272
00:15:36,790 --> 00:15:39,220
And then he says, I couldn't agree more.

273
00:15:39,220 --> 00:15:42,800
Well, that's a different type of assuring.

274
00:15:42,800 --> 00:15:49,640
Remember when we saw, that, some assuring
terms were authoritative.

275
00:15:49,640 --> 00:15:54,610
And other assuring terms were reflexive.
Where quoting President Clinton

276
00:15:54,610 --> 00:15:57,480
is an authoritative assurance, it's citing
an authority.

277
00:15:57,480 --> 00:16:01,810
I couldn't agree more says how much he
agrees,

278
00:16:01,810 --> 00:16:05,790
or how much certainty he has, it certainly
suggests.

279
00:16:05,790 --> 00:16:09,310
And so he seems to be assuring you but on
a different basis.

280
00:16:09,310 --> 00:16:11,760
Clinton and I both agree, we might
disagree

281
00:16:11,760 --> 00:16:14,230
about other things, but we agree about
this.

282
00:16:14,230 --> 00:16:18,080
Which gives you some reason to be sure
that it must be true.

283
00:16:18,080 --> 00:16:21,700
Next for over two

284
00:16:21,700 --> 00:16:25,400
decades the word for is sometimes an
Argument Marker.

285
00:16:25,400 --> 00:16:27,085
Is it an Argument Marker here?

286
00:16:27,085 --> 00:16:30,500
No, how can you tell that?

287
00:16:30,500 --> 00:16:34,010
It's actually nothing here, but how can
you tell that?

288
00:16:34,010 --> 00:16:36,750
Try substituting an Argument Marker.

289
00:16:36,750 --> 00:16:41,425
You can't say, because over two decades,
many have fought battle over battle.

290
00:16:41,425 --> 00:16:45,140
It's not because, it's just saying during
that period.

291
00:16:45,140 --> 00:16:46,710
The term for and the

292
00:16:46,710 --> 00:16:51,800
words after it, over two decades.
Are simply being used to indicate time,

293
00:16:51,800 --> 00:16:57,270
not to indicate any kind of reason in this
case so it should be marked as nothing.

294
00:16:59,000 --> 00:17:04,270
Many have fought battle after battle, is
that a guarding term?

295
00:17:04,270 --> 00:17:07,725
Sometimes many is a guarding term, instead
of saying all you say many.

296
00:17:07,725 --> 00:17:11,510
But here you say many have fought battle
after battle.

297
00:17:11,510 --> 00:17:14,390
Nobody thinks all have fought battle after
battle

298
00:17:14,390 --> 00:17:18,490
to keep the mining conglomerates from
despoiling the country.

299
00:17:18,490 --> 00:17:23,170
After all the mining conglomerates
themselves didn't so it can't be all.

300
00:17:23,170 --> 00:17:25,880
So nobody would expect the word all so in
this case

301
00:17:25,880 --> 00:17:29,834
the word many is not functioning to guard
the term by weakening.

302
00:17:29,834 --> 00:17:33,320
because it never started out as this
strong claim all.

303
00:17:33,320 --> 00:17:34,542
There was nothing to weaken.

304
00:17:34,542 --> 00:17:36,740
They fought

305
00:17:36,740 --> 00:17:38,250
battle after battle.

306
00:17:38,250 --> 00:17:41,560
Well you might think that battles are a
bad thing.

307
00:17:41,560 --> 00:17:44,120
So you might mark that as E minus.

308
00:17:44,120 --> 00:17:47,625
Because after all, conflict is a bad
thing, and in

309
00:17:47,625 --> 00:17:50,860
battles people get hurt and try to hurt
each other.

310
00:17:50,860 --> 00:17:57,030
So to explain what a battle is, you need
to introduce an evaluative word.

311
00:17:57,030 --> 00:17:58,750
And what do they fight those battles for?

312
00:17:58,750 --> 00:18:02,020
To keep mining conglomerates from
despoiling

313
00:18:02,020 --> 00:18:03,730
the treasures, right?

314
00:18:03,730 --> 00:18:08,370
Again to, can be seen as in order to,
that's why they fought the battle.

315
00:18:08,370 --> 00:18:10,550
It explains the battle.

316
00:18:10,550 --> 00:18:12,900
Or because they wanted to keep

317
00:18:12,900 --> 00:18:16,290
the mining conglomerates from despoiling
the countries.

318
00:18:16,290 --> 00:18:20,760
So it looks like to there is indicating
the premise

319
00:18:20,760 --> 00:18:24,620
in an argument that explains why they
fought battle after battle.

320
00:18:26,410 --> 00:18:27,450
'Kay?

321
00:18:27,450 --> 00:18:31,290
Mining conglomerates, is mining bad?
No?

322
00:18:31,290 --> 00:18:32,890
Are conglomerates bad?

323
00:18:32,890 --> 00:18:33,829
Not necessarily.

324
00:18:33,829 --> 00:18:37,591
You can explain what a conglomerate is
without talking about good or bad.

325
00:18:37,591 --> 00:18:40,271
From despoiling.
Wait a minute.

326
00:18:40,271 --> 00:18:43,273
Now we've got an evaluative term.

327
00:18:43,273 --> 00:18:46,389
It's an evaluative negative term.

328
00:18:46,389 --> 00:18:52,639
Despoiling means spoiling things, or
making them bad, and what about treasures?

329
00:18:52,639 --> 00:18:54,210
Treasures.

330
00:18:54,210 --> 00:18:59,270
Is going to be an evaluative plus term,
because treasures are good things.

331
00:19:00,730 --> 00:19:03,600
Now, the next word of the last sentence of

332
00:19:03,600 --> 00:19:06,850
this paragraph, just a temporal indicator,
so that's nothing.

333
00:19:06,850 --> 00:19:08,400
We thought, okay, thought

334
00:19:10,560 --> 00:19:14,080
means it's not really true.
He's just guarding it.

335
00:19:14,080 --> 00:19:16,190
It's not really true that some of it was
safe.

336
00:19:16,190 --> 00:19:18,150
We thought is was.

337
00:19:18,150 --> 00:19:22,080
Some of it was safe or even at least some
of it was safe.

338
00:19:23,240 --> 00:19:24,845
Now that's going to be a guarding term,

339
00:19:24,845 --> 00:19:26,040
because it's not saying it's not saying
all of

340
00:19:26,040 --> 00:19:28,130
it was safe, it's just a little part of

341
00:19:28,130 --> 00:19:30,595
it and that'll become important later in
the argument.

342
00:19:30,595 --> 00:19:31,700
Woah.

343
00:19:31,700 --> 00:19:35,930
Look at this diagram.
It's got letters all over the place, and

344
00:19:35,930 --> 00:19:40,422
they're running into each other.
That shows you what close analysis does.

345
00:19:40,422 --> 00:19:44,110
When you start looking in detail, a lot of
the different words are doing

346
00:19:44,110 --> 00:19:49,910
things that you can find out by trying to
put them into these different categories.

347
00:19:49,910 --> 00:19:57,658
So, we finished the first paragraph.
An entire paragraph!

348
00:19:57,658 --> 00:19:59,470
Oh my God!

349
00:19:59,470 --> 00:19:59,476
[NOISE]

350
00:19:59,476 --> 00:20:00,250
>> Joy!

