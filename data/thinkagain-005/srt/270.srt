1
00:00:03,070 --> 00:00:04,700
Now that we understand validity, we can

2
00:00:04,700 --> 00:00:07,508
use the notion of validity in
reconstructing arguments.

3
00:00:07,508 --> 00:00:11,340
Now the point of reconstructing an
argument, is to

4
00:00:11,340 --> 00:00:13,960
put it in a shape that makes it easier for

5
00:00:13,960 --> 00:00:17,720
us to assess the argument more accurately
and fearly

6
00:00:17,720 --> 00:00:19,819
for whether it's a good argument or a bad
argument.

7
00:00:21,410 --> 00:00:24,430
And when you do the reconstruction
remember you want to make it

8
00:00:24,430 --> 00:00:28,270
as good an argument as possible, because
you don't learn anything by putting

9
00:00:28,270 --> 00:00:31,250
down your enemies by making them look
silly.

10
00:00:31,250 --> 00:00:31,670
Right?

11
00:00:31,670 --> 00:00:35,220
If you want to learn from somebody else's
argument, you need to put it

12
00:00:35,220 --> 00:00:38,099
in the best shape you can to make it look
as good as possible.

13
00:00:39,170 --> 00:00:41,710
So, that's going to be the goal of
reconstruction and we're

14
00:00:41,710 --> 00:00:45,550
going to accomplish that goal in a series
of stages.

15
00:00:45,550 --> 00:00:47,370
The first stage is simply to do a

16
00:00:47,370 --> 00:00:49,786
close analysis and we talked about that
last week.

17
00:00:49,786 --> 00:00:53,480
The second stage is to get down

18
00:00:53,480 --> 00:00:58,450
to basics and is to remove all the excess
words and focus on the premises

19
00:00:58,450 --> 00:01:00,480
and conclusion that really make up the

20
00:01:00,480 --> 00:01:02,564
argument and then put those into standard
form.

21
00:01:03,780 --> 00:01:06,260
The third stage is to clarify those
premises.

22
00:01:06,260 --> 00:01:09,170
They're not always going to be as clear as
you like, and

23
00:01:09,170 --> 00:01:13,440
that's going to take some work, and it's
going to include breaking them into parts.

24
00:01:13,440 --> 00:01:16,630
And then the next stage is to take those
parts and organize

25
00:01:16,630 --> 00:01:18,490
them, to put them in order, so you can see
how the

26
00:01:18,490 --> 00:01:25,410
argument flows from one part to another.
But not all arguments are complete.

27
00:01:25,410 --> 00:01:28,070
So the next stage, we have to fill in the
gaps.

28
00:01:28,070 --> 00:01:30,090
That is, supply suppressed premises.

29
00:01:31,220 --> 00:01:33,650
And once we've done that, then the final

30
00:01:33,650 --> 00:01:35,930
stage is going to be to assess the
argument.

31
00:01:35,930 --> 00:01:37,985
If we are able to come up with a sound

32
00:01:37,985 --> 00:01:41,120
reconstruction, we know that the
conclusion has to be true.

33
00:01:41,120 --> 00:01:44,300
Because.
As we learned in the previous lecture,

34
00:01:44,300 --> 00:01:46,944
the conclusion of sound arguments is
always true.

35
00:01:46,944 --> 00:01:50,390
But if we don't come up with a sound
reconstruction, then

36
00:01:50,390 --> 00:01:53,790
we've got to decide is it the fault of the
argument or

37
00:01:53,790 --> 00:01:55,820
is it our own fault because we didn't come
up with

38
00:01:55,820 --> 00:02:00,040
a sound reconstruction when there really
is one that we didn't find.

39
00:02:00,040 --> 00:02:01,972
So that's going to be something we have to
discuss.

40
00:02:01,972 --> 00:02:05,625
We're going to discuss all of these stages
over the next few lectures.

41
00:02:05,625 --> 00:02:09,490
Now, the first stage of reconstruction is
to do a close

42
00:02:09,490 --> 00:02:12,490
analysis, but we already learned how to do
that.

43
00:02:12,490 --> 00:02:13,335
That was easy.

44
00:02:13,335 --> 00:02:16,770
Boy, hope the rest of them are that easy.

45
00:02:16,770 --> 00:02:18,410
This lecture is mainly going to be about

46
00:02:18,410 --> 00:02:21,745
the second stage namely getting down to
basics.

47
00:02:21,745 --> 00:02:24,750
And what we want to do is to pull out the

48
00:02:24,750 --> 00:02:29,090
explicit premise and conclusion from all
the other words around it.

49
00:02:29,090 --> 00:02:32,932
And the first step is to remove all the
access verbiage.

50
00:02:32,932 --> 00:02:34,670
You know, it might

51
00:02:34,670 --> 00:02:39,170
seem really surprising, but people often
repeat themselves.

52
00:02:39,170 --> 00:02:40,620
I'm sure you've all run in to it.

53
00:02:40,620 --> 00:02:45,045
I mean, listen to somebody give a talk,
and it takes them

54
00:02:45,045 --> 00:02:49,019
50 minutes to say what they could've said
easily in five minutes.

55
00:02:49,019 --> 00:02:52,852
And one of the reasons is that they say
everything 10 times.

56
00:02:52,852 --> 00:02:56,140
You know, for instance, people often say
the same thing twice.

57
00:02:56,140 --> 00:02:57,240
They repeat themselves.

58
00:02:57,240 --> 00:03:00,030
They say the same thing over again.
And they restate

59
00:03:00,030 --> 00:03:02,040
the same thing in different words.

60
00:03:02,040 --> 00:03:06,560
They utter sentences that mean the same
thing and they say something and you

61
00:03:06,560 --> 00:03:09,970
know, then they say it again, and they
make a claim twice or more.

62
00:03:09,970 --> 00:03:12,980
They assert exactly what they just said,
and they

63
00:03:12,980 --> 00:03:16,480
reformulate their claim in different words
that are equivalent.

64
00:03:16,480 --> 00:03:19,560
They say it once, and then they say it
again.

65
00:03:19,560 --> 00:03:20,470
You get the idea.

66
00:03:21,540 --> 00:03:25,220
Now here's a real example from a U.S,
politician during

67
00:03:25,220 --> 00:03:29,920
a debate.
I want to be honest with people.

68
00:03:29,920 --> 00:03:31,966
We can't eliminate this deficit.

69
00:03:31,966 --> 00:03:37,295
People have heard that over and over again
in four years.

70
00:03:37,295 --> 00:03:38,995
We cannot do it.

71
00:03:38,995 --> 00:03:45,410
We're in too deep a hole.
Now, if you think about it.

72
00:03:45,410 --> 00:03:49,090
It's going to be obvious that we cannot do
it.

73
00:03:49,090 --> 00:03:50,730
Repeat, we can't eliminate

74
00:03:50,730 --> 00:03:51,445
this deficit.

75
00:03:51,445 --> 00:03:55,270
because that's what doing it is,
eliminating the deficit.

76
00:03:55,270 --> 00:03:57,835
But also, we're into deep a hole.

77
00:03:57,835 --> 00:04:01,560
Well that's just a metaphorical way of
saying the same thing.

78
00:04:01,560 --> 00:04:04,310
Why is the hole too deep?
because we can't get out of it.

79
00:04:04,310 --> 00:04:05,670
What hole is it?

80
00:04:05,670 --> 00:04:10,190
It's the deficit hole, so to say we can't
get out of this hole, we're

81
00:04:10,190 --> 00:04:14,615
in too deep a hole is just another way of
saying we can't eliminate the deficit,

82
00:04:14,615 --> 00:04:19,635
so in these three lines he's already
repeated himself three times.

83
00:04:19,635 --> 00:04:22,550
Now why does this politician repeat
himself?

84
00:04:22,550 --> 00:04:24,790
It might be that he thinks people will
remember it

85
00:04:24,790 --> 00:04:28,830
better or that one version will make more
sense than another.

86
00:04:28,830 --> 00:04:31,060
But he might have a special reason because
this was a

87
00:04:31,060 --> 00:04:35,300
live debate and he had to give a 90 second
answer.

88
00:04:35,300 --> 00:04:36,770
So he had to fill up the time.

89
00:04:36,770 --> 00:04:40,450
Sometimes people repeat themselves just to
fill up the time or

90
00:04:40,450 --> 00:04:42,636
maybe to give himself time to think
because

91
00:04:42,636 --> 00:04:44,790
he didn't have a real answer ready yet.

92
00:04:44,790 --> 00:04:47,480
And repeating himself is easy while he
thinks about

93
00:04:47,480 --> 00:04:49,938
what he's really going to say in the next
few sentences.

94
00:04:51,400 --> 00:04:51,400
.

95
00:04:51,400 --> 00:04:55,530
Fine, but repeating it still doesn't make
the argument any better.

96
00:04:55,530 --> 00:04:58,410
And, we want to get down to the basics of
the argument.

97
00:04:58,410 --> 00:05:02,560
That is, the parts of the argument that
really effect how good it is.

98
00:05:02,560 --> 00:05:05,660
So we can cross out those repetitions that

99
00:05:05,660 --> 00:05:07,270
don't make the argument any better.

100
00:05:07,270 --> 00:05:11,830
So first, we can cross out we cannot do
it.

101
00:05:11,830 --> 00:05:15,350
Then we can cross out we're into deep a
hole.

102
00:05:15,350 --> 00:05:18,370
We already saw that those are just
repeating

103
00:05:18,370 --> 00:05:21,593
the claim that we can't eliminate the
deficit.

104
00:05:21,593 --> 00:05:27,840
In addition, we can cross out I want to be
honest with people because

105
00:05:27,840 --> 00:05:31,520
that's not a reason to believe that we're
in too deep a deficit.

106
00:05:31,520 --> 00:05:35,220
And, next, we can cross out, that people
have

107
00:05:35,220 --> 00:05:38,880
heard that over and over again in four
years.

108
00:05:38,880 --> 00:05:41,320
Well, that might be seen as a reason to

109
00:05:41,320 --> 00:05:44,330
believe that we're in a deficit, because
everybody seems

110
00:05:44,330 --> 00:05:47,880
to say it, but, let's assume that's not
part

111
00:05:47,880 --> 00:05:49,826
of his argument here and we'll cross it
out.

112
00:05:49,826 --> 00:05:52,600
Now, let's move on.

113
00:05:52,600 --> 00:05:56,650
A second form of excess verbiage.
That is, words that don't contribute

114
00:05:56,650 --> 00:06:01,230
to the force of the argument, is what I
like to call road markers.

115
00:06:01,230 --> 00:06:04,790
A lot of times, people, you know, good
speakers.

116
00:06:04,790 --> 00:06:08,990
They tell you what topic they're talking
about, and why they're talking about it.

117
00:06:08,990 --> 00:06:10,805
Why it's important, and worth talking
about.

118
00:06:10,805 --> 00:06:17,510
But to say why it's an important issue.
And to say what issue it is.

119
00:06:17,510 --> 00:06:19,650
Doesn't provide any reason to believe that
what

120
00:06:19,650 --> 00:06:22,830
they say about the issue is true or false.

121
00:06:22,830 --> 00:06:25,350
So it doesn't contribute to the argument.

122
00:06:25,350 --> 00:06:29,750
Here's an example from the same politician
in the same debate as we just saw.

123
00:06:30,950 --> 00:06:33,030
This politician said.

124
00:06:33,030 --> 00:06:38,198
Now I want to go back to the whole issue
of health care because we touched

125
00:06:38,198 --> 00:06:43,140
it and I think the American people deserve
to know what we would do different.

126
00:06:43,140 --> 00:06:47,960
Now notice, he says he's going to talk
about

127
00:06:47,960 --> 00:06:52,040
healthcare but, he doesn't say anything
about healthcare he

128
00:06:52,040 --> 00:06:54,580
doesn't tell you what he's going to say
about healthcare.

129
00:06:54,580 --> 00:06:56,490
All he says he wants to go back to that
issue and

130
00:06:56,490 --> 00:06:59,128
he tells you why he wants to go back to
that issue.

131
00:06:59,128 --> 00:07:02,160
But he doesn't add any reason to believe
that

132
00:07:02,160 --> 00:07:05,938
what he's about to say about the issue is
true.

133
00:07:05,938 --> 00:07:09,315
Now this can of course still be useful
because.

134
00:07:09,315 --> 00:07:12,650
You might get confused about what the
issue is.

135
00:07:12,650 --> 00:07:13,000
And he might

136
00:07:13,000 --> 00:07:16,670
be changing the topic and he wants to
signal that he is changing the topic.

137
00:07:16,670 --> 00:07:19,010
And that'll help his listeners.

138
00:07:19,010 --> 00:07:20,850
But it still doesn't add to the argument.

139
00:07:20,850 --> 00:07:22,640
It doesn't give you any reason for the

140
00:07:22,640 --> 00:07:25,460
conclusion that he is going to want to
draw.

141
00:07:25,460 --> 00:07:28,060
We can cross out these excess words.

142
00:07:28,060 --> 00:07:31,902
We can cross out, now I want to go back to
the whole issue of healthcare.

143
00:07:31,902 --> 00:07:36,880
Because that doesn't show that his views
on healthcare are correct.

144
00:07:36,880 --> 00:07:37,910
And we can cross out,

145
00:07:37,910 --> 00:07:39,160
because we touched it.

146
00:07:39,160 --> 00:07:40,990
That's a reason why we're going to that
issue.

147
00:07:40,990 --> 00:07:44,510
But again, that doesn't give any reason
why his views are correct.

148
00:07:44,510 --> 00:07:48,930
And we can even cross out that I think the
American people deserve to know.

149
00:07:48,930 --> 00:07:50,470
But we would do different.

150
00:07:50,470 --> 00:07:53,120
Because the fact that they deserve to know
what you're going to do

151
00:07:53,120 --> 00:07:56,810
doesn't show that what you're going to do
is the right thing to do.

152
00:07:56,810 --> 00:07:59,710
So none of these claims are really
reasons.

153
00:07:59,710 --> 00:08:03,630
They're going to be reasons for the main
part of his argument.

154
00:08:03,630 --> 00:08:06,780
Which is to support the particular views
on healthcare that

155
00:08:06,780 --> 00:08:09,130
he's going to tell you about, a few
seconds after this.

156
00:08:10,570 --> 00:08:13,790
The next type of access verbiage is
tangents.

157
00:08:13,790 --> 00:08:16,435
People go off on tangents all the time.

158
00:08:16,435 --> 00:08:18,000
Here is an example.

159
00:08:19,930 --> 00:08:23,806
You know, you really ought to think about
taking a history course.

160
00:08:23,806 --> 00:08:25,815
I, I still remember my history courses in
college.

161
00:08:25,815 --> 00:08:25,815
[UNKNOWN]

162
00:08:25,815 --> 00:08:28,280
There's this one time when.

163
00:08:28,280 --> 00:08:31,010
There's a dog that one of the students
brought to

164
00:08:31,010 --> 00:08:34,500
class and, and the dog like barked and
then he ran

165
00:08:34,500 --> 00:08:37,360
up on stage and he, he cut under the
professor and

166
00:08:37,360 --> 00:08:40,490
knocked the professor on his rear end, it
was really funny.

167
00:08:40,490 --> 00:08:44,445
So you know I think that history is a good
thing to study.

168
00:08:44,445 --> 00:08:51,270
Now notice that all this stuff about the
dog has nothing to do with history.

169
00:08:51,270 --> 00:08:53,350
It's no reason to take a history course
instead of

170
00:08:53,350 --> 00:08:56,790
a philosophy course, or a classics course,
or a science course.

171
00:08:56,790 --> 00:09:00,130
The same thing could happen in those
courses just as well.

172
00:09:00,130 --> 00:09:02,610
So, the tangent plays a certain role.

173
00:09:02,610 --> 00:09:05,385
It makes it interesting, it keeps your
attention,

174
00:09:05,385 --> 00:09:07,740
maybe it makes it memorable for you, what

175
00:09:07,740 --> 00:09:10,030
he said, but it doesn't actually provide a

176
00:09:10,030 --> 00:09:13,360
reason why, you ought to take a history
course.

177
00:09:13,360 --> 00:09:17,070
So, since those parts of

178
00:09:17,070 --> 00:09:20,790
the words were just a tangent, that don't
provide any reason

179
00:09:20,790 --> 00:09:26,240
we can cross them out too because they are
excess verbiage.

180
00:09:26,240 --> 00:09:31,010
But sometimes people go off on irrelevant
not just by accident because

181
00:09:31,010 --> 00:09:35,065
they lose their train of thought, but
because their trying to fool you.

182
00:09:35,065 --> 00:09:38,350
They're trying to produce what's called a

183
00:09:38,350 --> 00:09:42,910
red herring, the, the name red herring
supposedly

184
00:09:42,910 --> 00:09:48,520
comes from somebody who crossed the red
herring over the trail and then the

185
00:09:48,520 --> 00:09:53,330
hounds couldn't track his scent any more.
And that's basically what's going on here.

186
00:09:53,330 --> 00:09:58,470
Sometimes people produce tangents that
distract you from

187
00:09:58,470 --> 00:10:00,750
the main line of argument, because they
know

188
00:10:00,750 --> 00:10:02,680
that their weakness in that line of
argument

189
00:10:02,680 --> 00:10:04,590
and they don't want you to notice them.

190
00:10:04,590 --> 00:10:08,140
That's what a red herring is and its a
type of tangent

191
00:10:08,140 --> 00:10:11,100
that you have to learn to watch out for.

192
00:10:11,100 --> 00:10:14,090
Because if you want to see the problems in

193
00:10:14,090 --> 00:10:16,780
your opponent's arguments or even in your
friend's

194
00:10:16,780 --> 00:10:20,250
arguments, then you need to not get
distracted

195
00:10:20,250 --> 00:10:23,010
by tangents that are in a fact, red
herring.

196
00:10:24,340 --> 00:10:29,980
Yet another example of excess verbiage is
well, examples.

197
00:10:29,980 --> 00:10:31,990
Here's an example.
Of that.

198
00:10:33,250 --> 00:10:37,444
A different politician, in the same debate
said this, here's

199
00:10:37,444 --> 00:10:41,100
what's happened; in the time that they
have been in

200
00:10:41,100 --> 00:10:46,700
office, in the last four years, 1.6
million private sector jobs have been

201
00:10:46,700 --> 00:10:52,494
lost, 2.7 million manufacturing jobs have
been lost.

202
00:10:52,494 --> 00:10:56,060
And it's had real consequences in places
like Cleveland.

203
00:10:56,060 --> 00:10:57,825
Cleveland is a wonderful,

204
00:10:57,825 --> 00:11:01,550
distinguished city that's done a lot of
great things,

205
00:11:01,550 --> 00:11:05,640
but it has the highest poverty rate in the
country.

206
00:11:05,640 --> 00:11:10,079
One out of almost two children in
Cleveland are now living in poverty.

207
00:11:12,210 --> 00:11:17,487
Then notice that this politician is
talking about the unemployment rate

208
00:11:17,487 --> 00:11:21,066
in the rest of the country, in the country
as a whole.

209
00:11:21,066 --> 00:11:22,898
So why bring in Cleveland?

210
00:11:22,898 --> 00:11:27,010
Well, you might be saying that Cleveland
shows that there's problems

211
00:11:27,010 --> 00:11:29,660
throughout the rest of the country but
that can't be right because.

212
00:11:29,660 --> 00:11:30,940
Cleveland's just one example.

213
00:11:30,940 --> 00:11:35,530
And it might be an outlier that doesn't
represent the general trends.

214
00:11:35,530 --> 00:11:37,360
So what he's really doing with this
example

215
00:11:37,360 --> 00:11:42,495
is, he's trying to bring it down to home.
And make you feel for the real effects.

216
00:11:42,495 --> 00:11:46,110
But he doesn't come out and say that you

217
00:11:46,110 --> 00:11:48,650
can generalize from Cleveland to the rest
of the country.

218
00:11:48,650 --> 00:11:51,910
Or that everyone else is suffering in
exactly the same way.

219
00:11:51,910 --> 00:11:54,130
He's just giving one example.

220
00:11:54,130 --> 00:11:57,130
And so, it doesn't really support his
general claim,

221
00:11:57,130 --> 00:12:00,800
that the unemployment is a problem
throughout the whole country.

222
00:12:02,040 --> 00:12:02,640
That means,

223
00:12:02,640 --> 00:12:07,320
that it's not an extra premise in the
argument, and

224
00:12:07,320 --> 00:12:11,760
we can cross it out like other forms of
excess verbiage.

225
00:12:11,760 --> 00:12:16,556
Now we've seen that excess verbiage can
take the form of repetition.

226
00:12:16,556 --> 00:12:23,450
Road markers, or tangents, or examples and
people use these alot.

227
00:12:23,450 --> 00:12:27,950
Matter of fact, I'd like to think of a
general trick that people

228
00:12:27,950 --> 00:12:30,196
use called the trick of excess verbiage.

229
00:12:30,196 --> 00:12:33,060
A lot of people talk too much, and they
keep

230
00:12:33,060 --> 00:12:37,910
saying things over and over again, go off
on tangents, and

231
00:12:37,910 --> 00:12:41,460
give more examples than they really need,
and all of

232
00:12:41,460 --> 00:12:44,380
that is a way of hiding the problem with
their position.

233
00:12:44,380 --> 00:12:49,610
It's a trick to use too many words,
because, the

234
00:12:49,610 --> 00:12:52,020
real point gets lost in the middle of
those words,

235
00:12:53,170 --> 00:12:57,450
so you can fool people by throwing in
those extra words.

236
00:12:57,450 --> 00:12:59,070
That's the trick of excess verbiage.

237
00:13:00,120 --> 00:13:01,615
But, be careful.

238
00:13:01,615 --> 00:13:05,700
What seems like excess verbiage that's
just there to trick

239
00:13:05,700 --> 00:13:09,610
you might really be an essential part of
the argument.

240
00:13:09,610 --> 00:13:12,430
So what you need to do when you have a
passage and you're trying to get

241
00:13:12,430 --> 00:13:18,550
the argument out of it is to cross out all
of the excess words, but also look

242
00:13:18,550 --> 00:13:19,745
at what's left over.

243
00:13:19,745 --> 00:13:24,010
If what's left over is enough premises and
conclusion to make a

244
00:13:24,010 --> 00:13:30,460
good argument then this stuff that you
crossed out probably really is excess.

245
00:13:30,460 --> 00:13:32,530
But if it turns out that what's left over
is not a

246
00:13:32,530 --> 00:13:35,810
very good arguments, you ought to check
all those words you crossed

247
00:13:35,810 --> 00:13:39,370
out and make sure they really weren't
necessary, because you're not being

248
00:13:39,370 --> 00:13:43,850
fair, to the person that you're
interpreting, if you crossed out something

249
00:13:43,850 --> 00:13:45,849
that was an essential part of the
argument.

250
00:13:47,510 --> 00:13:49,733
And some cases, are going to be tricky.

251
00:13:49,733 --> 00:13:53,960
It's not going to be clear whether or not
to cross them out.

252
00:13:55,050 --> 00:13:57,990
Some small words that are tricky are
guarding terms.

253
00:13:57,990 --> 00:13:59,150
Here's an example.

254
00:13:59,150 --> 00:14:03,350
I think Miranda is at home, so we can meet
her there.

255
00:14:03,350 --> 00:14:05,190
What's the guarding word?

256
00:14:05,190 --> 00:14:07,358
You already found that out when you did
the close analysis.

257
00:14:07,358 --> 00:14:09,431
Right?
I think.

258
00:14:10,550 --> 00:14:13,620
Now one way to read this argument is, that
the premise is I

259
00:14:13,620 --> 00:14:18,230
think Miranda is a home, and the
conclusion is we can meet her there.

260
00:14:19,460 --> 00:14:22,850
But that's kind of weird, because the fact
that you think she's at

261
00:14:22,850 --> 00:14:25,800
home is not what makes it true that you
can meet her there.

262
00:14:25,800 --> 00:14:27,760
It's the fact that she is at home that it

263
00:14:27,760 --> 00:14:30,310
can make it the case that you can meet her
there.

264
00:14:30,310 --> 00:14:32,530
So if the premise is about what you think
and the

265
00:14:32,530 --> 00:14:35,365
conclusion is about where she is, and
where you can meet her.

266
00:14:35,365 --> 00:14:38,620
And the argument doesn't make any sense.

267
00:14:38,620 --> 00:14:41,220
So, in this case, what we want to do is to
cross out

268
00:14:41,220 --> 00:14:45,520
the words I think because that's going to
make the argument look silly.

269
00:14:45,520 --> 00:14:50,190
And the argument really amounts to Miranda
is at home, so we

270
00:14:50,190 --> 00:14:53,290
can meet her there, and the I think covers
that whole thing.

271
00:14:53,290 --> 00:14:56,610
It's saying, I think she's at home, so I
think we can meet her there.

272
00:14:56,610 --> 00:15:02,440
But the argument doesn't involve some
premise about what your thoughts are.

273
00:15:02,440 --> 00:15:05,730
And contrast this with a different
argument.

274
00:15:05,730 --> 00:15:08,770
Miranda's at home, so we can probably meet
her there.

275
00:15:09,790 --> 00:15:11,220
Now, there's another guarding term.

276
00:15:11,220 --> 00:15:12,260
Right?
Probably.

277
00:15:13,800 --> 00:15:17,610
Can you get rid of that?
Well then, the argument becomes.

278
00:15:17,610 --> 00:15:20,610
Miranda is at home, so we can meet her
there.

279
00:15:20,610 --> 00:15:23,390
But that's clearly not the speaker was
trying to say.

280
00:15:23,390 --> 00:15:25,880
If they included the word probably,
they'll

281
00:15:25,880 --> 00:15:27,630
realize that the fact that she's at home

282
00:15:27,630 --> 00:15:29,660
right now doesn't mean that we can meet
her there

283
00:15:29,660 --> 00:15:31,390
because it might take us a while to get
there.

284
00:15:31,390 --> 00:15:33,230
And she might leave while we're on the
way.

285
00:15:34,550 --> 00:15:38,130
So its not fair to the person giving the
argument and

286
00:15:38,130 --> 00:15:42,020
it makes the argument look worse, to cross
out the word probably.

287
00:15:42,020 --> 00:15:44,050
So in that case you want to keep the
guarding

288
00:15:44,050 --> 00:15:48,290
term, in order to properly represent the
force of the argument.

289
00:15:48,290 --> 00:15:51,670
So it looks like sometimes, you need to
keep the guarding terms.

290
00:15:51,670 --> 00:15:52,910
And sometimes,

291
00:15:52,910 --> 00:15:54,430
you need to cross them out.

292
00:15:54,430 --> 00:15:58,310
And there's not going to be any strict
rule that you can follow.

293
00:15:58,310 --> 00:16:00,440
You have to use your sense of what's

294
00:16:00,440 --> 00:16:02,608
going to make the argument as good as
possible.

295
00:16:02,608 --> 00:16:07,535
What's going to fit what the speaker was
really trying to say?

296
00:16:07,535 --> 00:16:10,680
Another tricky case is assuring term.

297
00:16:12,250 --> 00:16:14,230
Suppose I'm writing a letter of
recommendation and

298
00:16:14,230 --> 00:16:18,650
I say, he is clearly a great worker.

299
00:16:18,650 --> 00:16:22,970
I know that, so you ought to hire him.

300
00:16:22,970 --> 00:16:26,460
The assuring terms are clearly and I know
that.

301
00:16:27,920 --> 00:16:30,740
But now the question is, is the argument

302
00:16:30,740 --> 00:16:34,120
really first premise, he's clearly a great
worker.

303
00:16:34,120 --> 00:16:38,503
Second premise, I know that.
Conclusion, you ought to hire them.

304
00:16:38,503 --> 00:16:40,710
It's kind of weird again if you think

305
00:16:40,710 --> 00:16:43,913
about it, because you're not hiring him
because

306
00:16:43,913 --> 00:16:45,140
its clear.

307
00:16:45,140 --> 00:16:47,830
If he's a great worker but it's not clear
that he's a great

308
00:16:47,830 --> 00:16:52,060
worker, then you still ought to hire him
because he's still a great worker.

309
00:16:52,060 --> 00:16:55,100
Or if he's a great worker and I don't know
he's a great

310
00:16:55,100 --> 00:16:58,300
worker, you still ought to hire him
because he's still a great worker.

311
00:16:58,300 --> 00:17:00,870
The fact that I know it is irrelevant to
whether you ought

312
00:17:00,870 --> 00:17:04,060
to hire him because that's about my mental
states not his abilities.

313
00:17:05,210 --> 00:17:08,990
So that representation of the argument
doesn't really capture the force of

314
00:17:08,990 --> 00:17:11,023
somebody who writes this letter of
recomendation.

315
00:17:12,690 --> 00:17:16,980
So we can cross out the words I know that
and we can cross out

316
00:17:16,980 --> 00:17:23,340
clearly and then the argument is, he's a
great worker so you ought to hire him.

317
00:17:23,340 --> 00:17:28,750
But contrast this example, I am certain
that Jacob is cheating on his wife,

318
00:17:29,870 --> 00:17:34,250
so I ought to tell her.
Now you might think I

319
00:17:34,250 --> 00:17:38,280
am certain that is just another assuring
term, so we can cross it out.

320
00:17:38,280 --> 00:17:40,310
And then the real argument is Jacob is
cheating

321
00:17:40,310 --> 00:17:42,430
on his wife, so I ought to tell her.

322
00:17:42,430 --> 00:17:46,530
But now think about that argument.

323
00:17:46,530 --> 00:17:48,770
The mere fact that he's cheating on his
wife doesn't

324
00:17:48,770 --> 00:17:51,765
mean I ought to tell her if I'm not
certain.

325
00:17:51,765 --> 00:17:55,400
because if I have some suspicions or I'm
just guessing but I

326
00:17:55,400 --> 00:17:59,280
really don't know then I probably ought
not to tell Jacob's wife

327
00:17:59,280 --> 00:18:02,160
that, you know Jacob is cheating on her.

328
00:18:02,160 --> 00:18:08,049
So here the force of the argument does
seem to depend on my certainty.

329
00:18:08,049 --> 00:18:13,420
If I'm not certain I shouldn't tell her.
If I am certain maybe I should.

330
00:18:13,420 --> 00:18:15,240
So we can't cross out the assuring term

331
00:18:15,240 --> 00:18:18,680
in this case, because that would distort
the argument.

332
00:18:18,680 --> 00:18:20,770
Now, of course, some people might disagree
with that.

333
00:18:20,770 --> 00:18:24,660
They might say, well look, if you have
some reason but you're not certain

334
00:18:24,660 --> 00:18:27,360
you ought to tell her.
And that can be controversial.

335
00:18:27,360 --> 00:18:32,760
But we're talking here not about what
those people think, but what, the speaker

336
00:18:32,760 --> 00:18:38,000
thinks, the person giving this argument,
when this person said, I'm certain that

337
00:18:38,000 --> 00:18:42,870
Jacob is cheating on his wife, they seem
to indicate that to them, the

338
00:18:42,870 --> 00:18:45,450
fact they're certain provides an even
better

339
00:18:45,450 --> 00:18:48,610
reason, why he should tell Jacob's wife.

340
00:18:48,610 --> 00:18:49,700
So, if we

341
00:18:49,700 --> 00:18:53,220
want to capture what the person giving the
argument intended.

342
00:18:53,220 --> 00:18:56,590
In this case, we have to leave in the
assuring term.

343
00:18:56,590 --> 00:18:59,950
So we've seen one example where you ought
to get rid of the assuring terms.

344
00:18:59,950 --> 00:19:04,630
And another example where you ought to
keep the assuring terms.

345
00:19:04,630 --> 00:19:06,870
And just like with guarding terms, the
same point

346
00:19:06,870 --> 00:19:12,180
applies, there's no mechanical rule
that'll apply to every case.

347
00:19:12,180 --> 00:19:15,450
You have to think through the argument and
decide

348
00:19:15,450 --> 00:19:20,390
whether crossing out those words and
removing them distorts the argument

349
00:19:20,390 --> 00:19:25,440
or instead crossing them out makes the
argument look even better.

350
00:19:25,440 --> 00:19:27,940
Because the point of removing excess
verbiage is

351
00:19:27,940 --> 00:19:30,010
to get rid of the things that aren't
necessary,

352
00:19:30,010 --> 00:19:33,205
but keep everything that is necessary to
make the

353
00:19:33,205 --> 00:19:36,700
argument look as good as it possibly can
look.

354
00:19:36,700 --> 00:19:40,410
Finally, once we've removed all the excess
verbiage,

355
00:19:40,410 --> 00:19:42,110
what's left over?

356
00:19:42,110 --> 00:19:46,860
The answer is, the explicit premises and
conclusion in the argument.

357
00:19:46,860 --> 00:19:51,270
The point of removing the excess verbiage
was to separate those essential parts

358
00:19:51,270 --> 00:19:54,050
of the argument, those basics of the

359
00:19:54,050 --> 00:19:56,845
argument from, all the stuff that's
unnecessary.

360
00:19:56,845 --> 00:19:59,830
Of course, we still have to decide which

361
00:19:59,830 --> 00:20:02,164
ones are premises and which one is the
conclusion.

362
00:20:02,164 --> 00:20:02,508
Right?

363
00:20:02,508 --> 00:20:06,050
And that's why the close analysis helps
because we indicated

364
00:20:06,050 --> 00:20:08,650
which ones were reason markers and which
ones were conclusion

365
00:20:08,650 --> 00:20:12,040
markers, and that lets you identify that
these are the

366
00:20:12,040 --> 00:20:14,870
premises and that's the conclusion, and so
now, we can

367
00:20:14,870 --> 00:20:18,248
do step three, we can put the argument in
standard form.

368
00:20:18,248 --> 00:20:20,830
We put the premises above the line.

369
00:20:20,830 --> 00:20:25,430
And we put dot pyramid and then the
conclusion below the line.

370
00:20:25,430 --> 00:20:27,966
And we've got the argument in standard
form.

371
00:20:27,966 --> 00:20:31,578
Which completes stage two of the
reconstruction project.

372
00:20:31,578 --> 00:20:36,790
At this point, it's useful to look back at
the passage, and see whether you've

373
00:20:36,790 --> 00:20:42,530
gotten rid of all the excess included, all
of the basics of the argument.

374
00:20:42,530 --> 00:20:44,890
So you can look at the passage and say is
everything that's

375
00:20:44,890 --> 00:20:51,080
not crossed out in a premise or a
conclusion of the standard form.

376
00:20:51,080 --> 00:20:54,140
And if there's something that's still
there in the passage that

377
00:20:54,140 --> 00:20:56,730
isn't used you got to decide at that
point, is it really

378
00:20:56,730 --> 00:20:57,540
excess or not.

379
00:20:58,550 --> 00:21:01,020
And of course If the argument looks really
bad

380
00:21:01,020 --> 00:21:04,000
you've gotta look back and see whether
it's missing something

381
00:21:04,000 --> 00:21:06,600
that you had crossed out as being excess
verbiage

382
00:21:06,600 --> 00:21:09,390
when it really was an essential part of
the argument.

383
00:21:09,390 --> 00:21:14,150
So we can use this process of putting it
into standard form as a test of whether

384
00:21:14,150 --> 00:21:19,710
we've performed properly the other step of
getting rid of excess verbiage.

385
00:21:19,710 --> 00:21:21,875
So steps, two and

386
00:21:21,875 --> 00:21:27,910
three really work together in this stage
two of getting down to basics.

387
00:21:27,910 --> 00:21:31,280
That's what helps us to use the different
parts

388
00:21:31,280 --> 00:21:33,870
to see whether we've done each of them
properly.

