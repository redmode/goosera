1
00:00:03,650 --> 00:00:05,030
We've been talking about the truth

2
00:00:05,030 --> 00:00:07,270
functional connective that I've called the
conditional.

3
00:00:07,270 --> 00:00:10,190
And I said that in English, the
conditional

4
00:00:10,190 --> 00:00:13,170
is normally expressed by using the phrase,
if then.

5
00:00:14,580 --> 00:00:17,040
But that statement needs some
qualification.

6
00:00:17,040 --> 00:00:20,770
First of all there's some ocassions on
which we use the phrase if

7
00:00:20,770 --> 00:00:23,080
then but we're not expressing the

8
00:00:23,080 --> 00:00:25,330
true functional connective which is the
conditional.

9
00:00:25,330 --> 00:00:28,990
In fact we're not expressing any truth
functional connective at all.

10
00:00:28,990 --> 00:00:34,380
For instance if we use if then around
phrases that are in the subjunctive

11
00:00:34,380 --> 00:00:38,640
mood then often what we end up with is not
a truth functional connective.

12
00:00:38,640 --> 00:00:43,640
If I say, if I had been four feet tall,
then

13
00:00:43,640 --> 00:00:46,260
I would have been in the Guinness Book of
World Records.

14
00:00:47,380 --> 00:00:50,740
I'm not saying something the truth or
falsity of which is

15
00:00:50,740 --> 00:00:54,880
a product of the truth or falsity of the
ingredient propositions.

16
00:00:56,320 --> 00:00:56,550
Right?

17
00:00:56,550 --> 00:01:00,530
The truth or falsity of, if I had been
four feet tall then I would be in

18
00:01:00,530 --> 00:01:02,280
the Guinness Book of World Records or I
would

19
00:01:02,280 --> 00:01:05,160
have been in the Guinness Book of World
Records.

20
00:01:05,160 --> 00:01:09,335
Depends on things other than the truth or
the falsity of my being

21
00:01:09,335 --> 00:01:13,710
4 feet tall and my being in the Guiness
book of World Records.

22
00:01:13,710 --> 00:01:15,610
It depends on all sorts of other things as
well.

23
00:01:15,610 --> 00:01:22,000
So that's the situation where the phrase,
if then Isn't

24
00:01:22,000 --> 00:01:24,040
being used as a truth functional
connective.

25
00:01:24,040 --> 00:01:25,450
It's being used as a propositional

26
00:01:25,450 --> 00:01:27,690
connective, it builds a larger proposition
out

27
00:01:27,690 --> 00:01:29,870
of two ingredient propositions, but there

28
00:01:29,870 --> 00:01:32,270
the propositional connective is not truth
functional.

29
00:01:32,270 --> 00:01:35,880
In order for the propositional connective
expressed by if and then to be

30
00:01:35,880 --> 00:01:39,610
truth functional, the propositions inside,
if and

31
00:01:39,610 --> 00:01:42,160
then, can't be in the subjunctive mood.

32
00:01:43,860 --> 00:01:47,320
I should also mention that there's
sometimes other phrases in English

33
00:01:47,320 --> 00:01:51,400
that are used to express the
truth-functional conditional.

34
00:01:51,400 --> 00:01:54,650
For instance, we sometimes just used the
word if.

35
00:01:54,650 --> 00:01:58,850
So, for instance, I might say to you the
private investigator

36
00:01:58,850 --> 00:02:01,615
is eating lunch at New Havana if Walter is
eating lunch there.

37
00:02:01,615 --> 00:02:03,060
There.

38
00:02:03,060 --> 00:02:06,800
I just connected two propositions using
the word if,

39
00:02:06,800 --> 00:02:10,305
but what I'm expressing is a truth
functional conditional.

40
00:02:10,305 --> 00:02:15,650
It's the same thing as I would have been
expressing if I had said, if Walter

41
00:02:15,650 --> 00:02:17,320
is eating lunch at New Havana, then the

42
00:02:17,320 --> 00:02:20,960
private investigator is eating lunch at
New Havana.

43
00:02:20,960 --> 00:02:21,490
Right?

44
00:02:21,490 --> 00:02:23,780
Those are just two different ways of
saying the same thing.

45
00:02:25,120 --> 00:02:28,360
But, in the one case, I use the words, if
and

46
00:02:28,360 --> 00:02:30,770
then, and in the other case, I just use
the word, if.

47
00:02:32,690 --> 00:02:35,340
So, sometimes we express the truth
functional conditional

48
00:02:35,340 --> 00:02:40,050
just by use of the word, if.
We could also use the phrase, only if.

49
00:02:40,050 --> 00:02:42,370
And that could also express the true
functional conditional.

50
00:02:42,370 --> 00:02:45,750
For instance I might say, Walter is eating
lunch at New

51
00:02:45,750 --> 00:02:49,015
Havana only if the private investigator is
eating lunch at New Havana.

52
00:02:49,015 --> 00:02:53,980
And that expresses just the same thing as
saying, if the

53
00:02:53,980 --> 00:02:57,462
private investigator is eating lunch at
New Havana then Walter is too.

54
00:02:57,462 --> 00:03:00,660
Alright they, they express just

55
00:03:00,660 --> 00:03:01,410
the same thing.

56
00:03:01,410 --> 00:03:04,130
They're true or false in just the same
circumstances.

57
00:03:04,130 --> 00:03:08,970
It's just a different phrasing, different
phrasing, same meaning.

58
00:03:08,970 --> 00:03:14,602
So, the truth functional conditional,
while it's often expressed in English

59
00:03:14,602 --> 00:03:20,080
using the words if and then, is sometimes
expressed using other words.

60
00:03:20,080 --> 00:03:23,440
And it's not always expressed by means of
if then.

61
00:03:23,440 --> 00:03:26,110
Sometimes if then expresses propositional
connectives

62
00:03:26,110 --> 00:03:27,760
that are not truth functional.

63
00:03:29,890 --> 00:03:33,767
In the next lecture, we're going to talk
about bi-conditionals.

