1
00:00:03,030 --> 00:00:04,900
The next question has probably been
bothering you

2
00:00:04,900 --> 00:00:07,800
ever since very early on in the previous
lecture.

3
00:00:08,880 --> 00:00:15,330
Namely, if valid arguments can have false
premises, then what good are they?

4
00:00:15,330 --> 00:00:21,360
Sure, there's this technical logician's
notion of valid argument, but why

5
00:00:21,360 --> 00:00:26,550
should we care whether arguments are valid
if valid arguments can be really bad?

6
00:00:27,690 --> 00:00:30,890
Validity might be necessary for an
argument to be

7
00:00:30,890 --> 00:00:33,730
good or at least for deductive argument to
be good.

8
00:00:33,730 --> 00:00:37,710
Because remember, there are also inductive
arguments,

9
00:00:37,710 --> 00:00:40,260
but even though it's necessary, it's not
enough.

10
00:00:40,260 --> 00:00:44,720
You can have a horrible argument that's
still valid.

11
00:00:44,720 --> 00:00:48,210
The great thing about validity is that
when a valid argument

12
00:00:48,210 --> 00:00:52,606
has true premises, then you get something
that really is reliable, namely,

13
00:00:52,606 --> 00:00:53,443
soundness.

14
00:00:55,340 --> 00:00:57,740
Because if you know that the premises are

15
00:00:57,740 --> 00:01:01,120
true and you also know that it's not
possible

16
00:01:01,120 --> 00:01:04,132
for the premises to be true and the
conclusion

17
00:01:04,132 --> 00:01:08,340
false, then you know the conclusion must
be true.

18
00:01:09,850 --> 00:01:13,367
So in a sound argument the conclusion has
to be true.

19
00:01:13,367 --> 00:01:17,640
And that is what makes it valuable,
because if we can get

20
00:01:17,640 --> 00:01:21,740
a deductive argument to be sound, then you
really got something.

21
00:01:21,740 --> 00:01:23,845
What you've got, is a true conclusion.

22
00:01:23,845 --> 00:01:27,520
Officially then, a sound argument is one
where

23
00:01:27,520 --> 00:01:31,369
the premises are true, and the argument is
valid.

24
00:01:32,950 --> 00:01:39,070
And we've got the same combinations of
truth and falsity as possibilities

25
00:01:39,070 --> 00:01:43,560
that we had in valid arguments.
You can have both premises and

26
00:01:43,560 --> 00:01:47,940
conclusion are true and then if it's
valid, the argument's sound.

27
00:01:47,940 --> 00:01:49,294
And if it's not valid, it's not.

28
00:01:50,380 --> 00:01:54,210
Or you can have, the premises are true and
the conclusion's false.

29
00:01:54,210 --> 00:01:56,610
And then it can't be valid.

30
00:01:56,610 --> 00:01:58,510
But if it's invalid, it's not sound.

31
00:01:59,820 --> 00:02:05,459
Or you can have, the premises are false
and the conclusion's true.

32
00:02:05,459 --> 00:02:08,650
And then if it's valid, it's not sound.
And if it's invalid

33
00:02:08,650 --> 00:02:10,260
it's not sound.

34
00:02:10,260 --> 00:02:14,480
We can have both the premises and the
conclusion are false.

35
00:02:14,480 --> 00:02:17,622
And then it's not going to be sound
whether it's valid or not.

36
00:02:17,622 --> 00:02:24,340
So the only combination where it's sound
is when the premises are true

37
00:02:24,340 --> 00:02:31,230
and the argument is valid and in that case
you know that the conclusion is true.

38
00:02:31,230 --> 00:02:33,200
What about lack of soundness?

39
00:02:33,200 --> 00:02:33,700
Well, there are two

40
00:02:33,700 --> 00:02:37,860
ways for an argument to fail to be sound,
mainly either

41
00:02:37,860 --> 00:02:42,230
the argument can be invalid or whether its
premises can be false.

42
00:02:42,230 --> 00:02:44,660
So it's a lot easier for an argument to be
unsound.

43
00:02:45,930 --> 00:02:49,070
And we know that a deductive argument
tries to be

44
00:02:49,070 --> 00:02:51,530
valid and of course it wants its premises
to be true.

45
00:02:51,530 --> 00:02:55,530
So a deductive argument is trying to be
sound.

46
00:02:55,530 --> 00:02:58,090
When it fails to be sound, it's not going
to be any good.

47
00:02:59,500 --> 00:03:01,820
Now the next question is, how can you
know?

48
00:03:02,910 --> 00:03:04,720
If you don't know whether the premises are
true,

49
00:03:04,720 --> 00:03:07,970
you're not going to know whether the
argument is sound.

50
00:03:07,970 --> 00:03:09,240
Well, not quite.

51
00:03:09,240 --> 00:03:14,450
Because if you, if the argument's valid
and you know it's valid, then

52
00:03:14,450 --> 00:03:17,160
you don't know whether it's sounds unless
you know the premises are true.

53
00:03:17,160 --> 00:03:22,780
But if you know the argument's invalid,
you already know it's unsound.

54
00:03:22,780 --> 00:03:25,880
Even if you don't know whether the
premises are true.

55
00:03:25,880 --> 00:03:28,300
So if you think about it that shows why
you want to be

56
00:03:28,300 --> 00:03:32,928
able to test for validity, because if you
can show the arguments is

57
00:03:32,928 --> 00:03:36,250
invalid, then you're going to be able to
say, well I know it's

58
00:03:36,250 --> 00:03:40,570
unsound regardless of what you think about
whether the premises are true or not.

59
00:03:41,680 --> 00:03:44,495
So there's going to be some value to
validity.

60
00:03:44,495 --> 00:03:47,850
Namely, if you can show it's invalid,
you're going to show

61
00:03:47,850 --> 00:03:50,880
it's unsound, and that means that the
deductive argument didn't

62
00:03:50,880 --> 00:03:51,680
get what it wanted.

63
00:03:52,770 --> 00:03:56,680
So validity is going to be necessary for
soundness, and soundness is

64
00:03:56,680 --> 00:04:00,160
going to be important because it
guarantees the truth of the conclusion.

65
00:04:00,160 --> 00:04:05,200
And then validity derives it's value from
the fact

66
00:04:05,200 --> 00:04:10,400
that if it's not valid it's not sound.

67
00:04:10,400 --> 00:04:13,040
Okay, now there's a lot more to say about
validitiy and

68
00:04:13,040 --> 00:04:16,530
we'll say a lot more about validity when
we get to formal

69
00:04:16,530 --> 00:04:18,570
logic in the second part of this course,

70
00:04:18,570 --> 00:04:21,500
but for now we're just going to stick with
this

71
00:04:21,500 --> 00:04:24,710
pretty intuitive notion of validity and
see how we

72
00:04:24,710 --> 00:04:28,550
can use this notion of validity to
reconstruct arguments.

