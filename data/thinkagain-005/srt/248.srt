1
00:00:02,930 --> 00:00:05,770
Now that we've identified arguments, and
we've also identified

2
00:00:05,770 --> 00:00:09,800
premises and conclusions, we need to put
them in order.

3
00:00:09,800 --> 00:00:13,300
The actual word order doesn't always tell
us the order of argument.

4
00:00:13,300 --> 00:00:18,110
Compare these two sentences: because I am
a professor, I

5
00:00:18,110 --> 00:00:23,940
teach classes, I teach classes, because I
am a professor.

6
00:00:23,940 --> 00:00:28,060
In those two examples, the words, I am a
professor,

7
00:00:28,060 --> 00:00:32,174
in one instance, occur at the beginning,
and the other instance occur at the end.

8
00:00:32,174 --> 00:00:38,250
But they actually express exactly the same
argument, namely, the fact

9
00:00:38,250 --> 00:00:42,075
that I'm a professor, is a reason why I
teach classes.

10
00:00:42,075 --> 00:00:46,040
But contrast both of those with this
example.

11
00:00:47,890 --> 00:00:53,100
I teach classes so I must be a professor.
The point there must be something

12
00:00:53,100 --> 00:00:56,760
like, nobody but professors can teach
classes, and whether nor not that's

13
00:00:56,760 --> 00:01:01,620
true, point here is, that's a different
argument from the first one.

14
00:01:01,620 --> 00:01:02,840
And we need to represent the

15
00:01:02,840 --> 00:01:06,810
difference between these two arguments
very carefully.

16
00:01:06,810 --> 00:01:13,330
In order to show what is shared by the
first two examples that's different

17
00:01:13,330 --> 00:01:18,765
from the third example, we put the
arguments in what's called standard form.

18
00:01:18,765 --> 00:01:20,740
And it's really easy.

19
00:01:22,190 --> 00:01:25,740
Basically, you put the premise down, and

20
00:01:25,740 --> 00:01:27,465
if there's another premise you put that
down.

21
00:01:27,465 --> 00:01:29,850
Then you draw a line.

22
00:01:30,880 --> 00:01:35,520
Then you put what I call a dot pyramid,
it's basically three little dots with two

23
00:01:35,520 --> 00:01:39,547
at the bottom and one at the top, and then
you put the conclusion after that.

24
00:01:39,547 --> 00:01:43,050
It's also useful; we'll see why later.

25
00:01:43,050 --> 00:01:43,915
But it's useful

26
00:01:43,915 --> 00:01:50,200
to number the premises, and to number the
conclusion, so that we can refer back to

27
00:01:50,200 --> 00:01:52,390
the premise and conclusion with the
number, instead

28
00:01:52,390 --> 00:01:54,360
of having to repeat it all the time.

29
00:01:54,360 --> 00:01:55,892
And that's all there is to standard form.

30
00:01:55,892 --> 00:01:58,650
You just put down the premises on
different lines.

31
00:01:58,650 --> 00:02:03,170
Then you draw a line, put the little dot
pyramid,

32
00:02:03,170 --> 00:02:06,010
and then write down the conclusion, and
then number them all.

33
00:02:07,080 --> 00:02:07,580
That's it.

34
00:02:09,030 --> 00:02:12,490
And this standard form accomplishes what
we wanted it to.

35
00:02:12,490 --> 00:02:16,690
Namely, it helps us show what's common to
the first two examples

36
00:02:16,690 --> 00:02:21,550
at the beginning of this lecture, that
distinguishes them from the third example.

37
00:02:21,550 --> 00:02:25,380
So the first two examples were, I teach
classes because

38
00:02:25,380 --> 00:02:29,238
I'm a professor, and because I'm a
professor I teach classes.

39
00:02:29,238 --> 00:02:34,610
There the sentence, the premise that goes
above the line is,

40
00:02:34,610 --> 00:02:37,870
I am a professor, and the conclusion that
goes below the

41
00:02:37,870 --> 00:02:42,350
line and next to the dot pyramid is, I
teach classes.

42
00:02:43,360 --> 00:02:46,410
But the next example, the third example at

43
00:02:46,410 --> 00:02:48,330
the beginning of the lecture was, I teach
classes,

44
00:02:50,880 --> 00:02:52,940
so I must be a professor.

45
00:02:52,940 --> 00:02:58,510
Now that means that, I am a professor, is
the conclusion that goes

46
00:02:58,510 --> 00:03:04,500
below the line, next to the dot pyramid,
and the premise is, I teach classes.

47
00:03:04,500 --> 00:03:07,810
So when you put these two in standard
form, right next

48
00:03:07,810 --> 00:03:12,280
to each other, it makes it absolutely
clear what the difference is.

49
00:03:12,280 --> 00:03:16,070
And what's in common to the first two that
distinguishes them from

50
00:03:16,070 --> 00:03:16,620
the third one.

51
00:03:18,070 --> 00:03:23,370
No matter how easy, I think it's still
worthwhile to do a few exercises just to

52
00:03:23,370 --> 00:03:25,620
make sure we've got it straight, because
this

53
00:03:25,620 --> 00:03:29,290
notion of standard form will become
important later.

