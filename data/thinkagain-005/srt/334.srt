1
00:00:00,025 --> 00:00:00,525
>>

2
00:00:04,180 --> 00:00:08,970
Welcome to week five of our course, the
week on categorical logic.

3
00:00:08,970 --> 00:00:09,950
Woo hoo!

4
00:00:09,950 --> 00:00:12,026
Yeah!
All right.

5
00:00:12,026 --> 00:00:15,470
>> Yeah!
>> Yeah!

6
00:00:15,470 --> 00:00:18,390
Remember, last week was on a part

7
00:00:18,390 --> 00:00:21,710
of deductive logic that we called
propositional logic.

8
00:00:21,710 --> 00:00:24,350
Last week we tried to understand why

9
00:00:24,350 --> 00:00:29,950
certain inferences, certain arguments that
use propositional connectives,

10
00:00:29,950 --> 00:00:32,840
were valid because of the truth tables

11
00:00:32,840 --> 00:00:35,149
for the propositional connectives that
they use.

12
00:00:36,610 --> 00:00:39,650
This week we're going to be looking at
arguments that are

13
00:00:39,650 --> 00:00:44,210
valid not because of the propositional
connectives that they use,

14
00:00:44,210 --> 00:00:45,870
in fact a lot of the arguments that we'll
be

15
00:00:45,870 --> 00:00:49,440
looking at this week have no propositional
connectives at all.

16
00:00:49,440 --> 00:00:52,290
We'll be looking at arguments that are
valid for a different reason.

17
00:00:52,290 --> 00:00:54,579
We'll try to understand what that
different reason is.

18
00:00:55,830 --> 00:00:59,580
But first, to review in more detail what
we did last week, let's

19
00:00:59,580 --> 00:01:04,370
consider an example of a deductive
argument

20
00:01:04,370 --> 00:01:06,910
that propositional logic can help us
understand.

21
00:01:08,080 --> 00:01:09,510
So let's look at this.

22
00:01:13,010 --> 00:01:15,380
Consider the following argument.

23
00:01:15,380 --> 00:01:23,030
Premise 1, Jill is riding her bicycle, if,
and only if, John is walking to the park.

24
00:01:23,030 --> 00:01:26,270
You can imagine, let's say that Jill has a
bicycle, but the only

25
00:01:26,270 --> 00:01:31,130
time she ever rides her bicycle is to meet
John at the park.

26
00:01:31,130 --> 00:01:34,690
And, the only way John ever gets to the
park is by walking there.

27
00:01:34,690 --> 00:01:38,220
Let's say he lives just a block down from
the park, so it doesn't

28
00:01:38,220 --> 00:01:41,310
make sense for him to get their any other
way, so he only goes to the

29
00:01:41,310 --> 00:01:44,030
park by walking there, and Jill only rides

30
00:01:44,030 --> 00:01:45,830
her bicycle when she's meeting him at the
park.

31
00:01:45,830 --> 00:01:48,700
So according to premise one, Jill is
riding her bicycle

32
00:01:48,700 --> 00:01:51,030
if and only if John is walking to the
park.

33
00:01:52,630 --> 00:01:54,290
Premise two.

34
00:01:54,290 --> 00:02:01,080
John is walking to the park if and only if
premise one is true,

35
00:02:04,550 --> 00:02:09,450
and the conclusion of the argument is
therefore Jill is riding her bicycle.

36
00:02:11,400 --> 00:02:14,420
Now let me ask you, is that argument
valid?

37
00:02:15,800 --> 00:02:20,420
Is there any possible way, for the premise
to be true, while the conclusion is false?

38
00:02:22,080 --> 00:02:23,470
Well, that's not obvious, is it?

39
00:02:24,880 --> 00:02:29,150
It takes a while, to see, that in fact,
that argument is valid.

40
00:02:30,790 --> 00:02:33,460
And, the way to see it, is by using the

41
00:02:33,460 --> 00:02:38,200
truth table for the bi-conditional,
expressed in English by if,

42
00:02:38,200 --> 00:02:41,030
and only if, we can use the truth table
for

43
00:02:41,030 --> 00:02:45,660
the bi-conditional, to see that that
argument really is valid.

44
00:02:45,660 --> 00:02:47,139
Here, let me show you what I have in mind.

45
00:02:50,860 --> 00:02:51,700
So look here.

46
00:02:51,700 --> 00:02:54,050
There's the proposition, Jill is riding
her bicycle.

47
00:02:54,050 --> 00:02:56,330
Now, that proposition can be either true
or false.

48
00:02:56,330 --> 00:02:59,220
Then there's the proposition, John is
walking to the park,

49
00:02:59,220 --> 00:03:01,670
and, of course, that proposition could be
either true or false.

50
00:03:01,670 --> 00:03:04,180
So there are four possible combinations.

51
00:03:04,180 --> 00:03:08,590
Either Jill is riding her bicycle is true
and John is walking to the park is true.

52
00:03:08,590 --> 00:03:12,390
Jill is riding her bicycle is true and
John is walking to the park is false,

53
00:03:12,390 --> 00:03:16,380
Jill is riding her bicycle is false and
John is walking to the park is true,

54
00:03:16,380 --> 00:03:20,230
or both of those propositions are false.
Those are the 4 possible scenarios.

55
00:03:21,660 --> 00:03:26,350
Now, according to premise 1, Jill is
riding her bicycle

56
00:03:26,350 --> 00:03:28,279
if and only if John is walking to the
park.

57
00:03:30,070 --> 00:03:34,940
So, if premise one is true that shows us
that we're either in

58
00:03:34,940 --> 00:03:38,860
the first of those four scenarios or the
last of those four scenarios.

59
00:03:38,860 --> 00:03:41,510
We're either in a scenario where it's true
that

60
00:03:41,510 --> 00:03:43,640
Jill is riding her bicycle and it's true
that John

61
00:03:43,640 --> 00:03:45,750
is walking to the park, or else we're in
in

62
00:03:45,750 --> 00:03:48,230
a scenario where it's false that Jill is
riding her

63
00:03:48,230 --> 00:03:50,560
bicycle and it's false that John is
walking to the park.

64
00:03:52,560 --> 00:03:56,570
Okay, now consider the proposition, John
is walking to

65
00:03:56,570 --> 00:04:00,890
the park if, and only if, that last
statement, the

66
00:04:00,890 --> 00:04:03,200
statement to the left, that Jill is riding
her bicycle

67
00:04:03,200 --> 00:04:05,420
if, and only if John is walking to the
park.

68
00:04:05,420 --> 00:04:06,770
John is walking to the park

69
00:04:06,770 --> 00:04:09,290
if, and only if that statement is true.

70
00:04:11,450 --> 00:04:14,720
Now, what's the truth table for that
going to be?

71
00:04:16,010 --> 00:04:20,630
Well, the truth table for that, is going
to be as follows.

72
00:04:20,630 --> 00:04:23,940
It's going to be true, whenever it's, true
that

73
00:04:23,940 --> 00:04:25,900
John is walking to the park, and it's also

74
00:04:25,900 --> 00:04:30,160
true that Jill is riding her bicycle if,
and only if, John is walking to the park.

75
00:04:30,160 --> 00:04:33,800
It's also going to be true whenever it's
false that John is walking

76
00:04:33,800 --> 00:04:36,400
to the park and it's false that Jill is
riding her bicycle if,

77
00:04:36,400 --> 00:04:38,010
and only if, John is walking to the park.

78
00:04:39,090 --> 00:04:43,590
So that means that that last
bi-conditional, that

79
00:04:43,590 --> 00:04:45,950
John is walking to the park if, and only

80
00:04:45,950 --> 00:04:51,370
if, premise one is true That's going to be
true in the first two of our scenarios.

81
00:04:51,370 --> 00:04:53,700
It's going to be true when it's true that
Jill is

82
00:04:53,700 --> 00:04:56,890
riding her bicycle and true that John is
walking to the park.

83
00:04:56,890 --> 00:04:59,680
It's also going to be true when it's true
that Jill is

84
00:04:59,680 --> 00:05:02,220
riding her bicycle and false that John is
walking to the park.

85
00:05:04,850 --> 00:05:08,930
Okay, so now under what conditions will
premise one

86
00:05:08,930 --> 00:05:13,290
and premise two of our argument B both
true

87
00:05:14,600 --> 00:05:17,930
remember premise one says Jill is riding
her bicycle

88
00:05:17,930 --> 00:05:19,860
if and only if John is walking to the
park.

89
00:05:19,860 --> 00:05:22,550
Premise two says John is walking to the
park, if and only if

90
00:05:22,550 --> 00:05:24,690
premise one is true so under what

91
00:05:24,690 --> 00:05:27,740
conditions will those premises will be
true?

92
00:05:27,740 --> 00:05:30,290
Well, those two premises

93
00:05:30,290 --> 00:05:35,210
will be true only in the first of our four
scenarios, because in the first of our

94
00:05:35,210 --> 00:05:39,570
four scenarios it'll be true that Jill is
riding her bicycle if and only if John is

95
00:05:39,570 --> 00:05:44,820
walking to the park and in the first of
our four scenarios it'll also be true that

96
00:05:44,820 --> 00:05:47,090
John is walking to the park if and only

97
00:05:47,090 --> 00:05:51,660
if That last statement, premise 1, is also
true.

98
00:05:52,660 --> 00:05:55,400
So that's the only scenario

99
00:05:55,400 --> 00:05:57,950
under which both of those premises are
true.

100
00:05:57,950 --> 00:06:01,320
But now notice, in that scenario, when
both of

101
00:06:01,320 --> 00:06:04,850
those premises are true, Jill is riding
her bicycle.

102
00:06:07,060 --> 00:06:12,040
And so the argument that we just looked at
is going to be valid.

103
00:06:12,040 --> 00:06:15,190
Because in any situation in which the two
premises are

104
00:06:15,190 --> 00:06:19,000
true, the conclusion is going to have to
be true.

105
00:06:19,000 --> 00:06:24,410
And we learn that by looking at the truth
table for the bi-conditional.

106
00:06:24,410 --> 00:06:26,440
Now see, that's an example of how we can
use

107
00:06:26,440 --> 00:06:28,150
the truth table for propositional

108
00:06:28,150 --> 00:06:32,180
connective like the bi-conditional, to
discover

109
00:06:32,180 --> 00:06:36,550
that a particularly tricky argument is
valid.

110
00:06:38,040 --> 00:06:38,260
Right?

111
00:06:38,260 --> 00:06:41,980
We have a tricky deductive argument right
here.

112
00:06:41,980 --> 00:06:44,600
It's not obvious whether or not it's
valid, but we can use

113
00:06:44,600 --> 00:06:48,900
the truth table for the bi-conditional to
discover that the argument is valid.

114
00:06:48,900 --> 00:06:51,500
But remember, I said we don't just use
truth tables

115
00:06:51,500 --> 00:06:55,270
to discover when arguments or valid or
that they're valid.

116
00:06:55,270 --> 00:06:57,840
We can also use truth tables to explain

117
00:06:57,840 --> 00:06:59,170
why they are valid to explain even

118
00:06:59,170 --> 00:07:01,930
in cases where they are obviously valid,
right?

119
00:07:01,930 --> 00:07:05,800
So last week we looked at lots of example
of our arguments that

120
00:07:05,800 --> 00:07:10,870
were obviously valid and in some cases
obviously invalid And we used the

121
00:07:10,870 --> 00:07:14,560
truth table not to figure out that they
were valid or invalid, it

122
00:07:14,560 --> 00:07:18,450
was already obvious that they were valid
or invalid, as the case may be.

123
00:07:18,450 --> 00:07:23,000
We used the truth table to understand why
they were valid or invalid.

124
00:07:23,000 --> 00:07:25,210
That's what the truth table was for in
those cases.

125
00:07:29,160 --> 00:07:33,790
Now, this week in our study of categorical
logic

126
00:07:33,790 --> 00:07:37,580
we want to find a method that can function
like

127
00:07:37,580 --> 00:07:42,550
the method of truth tables to help us
discover whether particular

128
00:07:42,550 --> 00:07:47,740
inferences are valid and why particular
inferences are valid.

129
00:07:48,950 --> 00:07:50,740
But it's not going to be the same as the
method

130
00:07:50,740 --> 00:07:54,670
of truth tables, because truth tables only
work for inferences or

131
00:07:54,670 --> 00:07:58,620
arguments that use truth functional
connectives.

132
00:07:58,620 --> 00:08:02,500
But, not every valid argument, uses a
truth functional connective.

133
00:08:03,840 --> 00:08:06,509
Consider a couple of the examples we
looked at last week

134
00:08:09,160 --> 00:08:13,490
consider this argument from a week ago: no
fish have wings,

135
00:08:14,510 --> 00:08:19,250
all birds have wings, all animals with
gills are fish,

136
00:08:20,280 --> 00:08:25,340
therefore no birds have gills.
Is that argument valid?

137
00:08:28,540 --> 00:08:32,240
It's not immediately obvious, whether or
not it's valid.

138
00:08:32,240 --> 00:08:39,100
But, it turns out that it is valid and, we
have a method for proving it's valid.

139
00:08:39,100 --> 00:08:41,780
And this method is what we'll be talking
about this week.

140
00:08:41,780 --> 00:08:44,220
It's the central method of categorical
logic.

141
00:08:46,230 --> 00:08:50,620
Notice also, that there are other
inferences which are obviously valid,

142
00:08:50,620 --> 00:08:54,330
but the validity of which, truth tables
don't help us understand.

143
00:08:54,330 --> 00:08:56,550
Consider for instance, this example.

144
00:08:56,550 --> 00:08:58,810
Mary has a child who is pregnant.

145
00:08:58,810 --> 00:09:03,330
Only daughters can be come pregnant.
therefore, May has at least one daughter.

146
00:09:03,330 --> 00:09:05,429
Now that argument is pretty obviously
valid.

147
00:09:07,690 --> 00:09:08,200
But why?

148
00:09:09,370 --> 00:09:11,640
What is it about the argument that makes
it valid?

149
00:09:11,640 --> 00:09:15,420
I said at the beginning of week four, that
there's something about the form of the

150
00:09:15,420 --> 00:09:19,430
argument, something about, the, use of the
terms

151
00:09:19,430 --> 00:09:23,170
only, and at least that makes that
argument valid.

152
00:09:23,170 --> 00:09:26,690
And any argument, no matter what it's
about, that uses the terms only and

153
00:09:26,690 --> 00:09:30,090
at least in the way that this argument
does, is also going to be valid.

154
00:09:30,090 --> 00:09:32,750
But what is it about the use of those
terms that

155
00:09:32,750 --> 00:09:34,740
makes the argument valid?

156
00:09:34,740 --> 00:09:36,540
Well, this week, in our study of
categorical

157
00:09:36,540 --> 00:09:41,090
logic, we're going to discover a way of
understanding what's

158
00:09:41,090 --> 00:09:43,600
going on with that use of those terms,
only

159
00:09:43,600 --> 00:09:47,000
and at least, those terms that we call
quantifiers.

160
00:09:47,000 --> 00:09:52,440
We're going to discover a method of
understanding quantifiers that can help us

161
00:09:52,440 --> 00:09:58,190
to understand why this particular argument
and others that are of the same form

162
00:09:58,190 --> 00:10:01,050
are valid.
Okay.

163
00:10:01,050 --> 00:10:04,170
So that's what we'll be doing this week in
categorical logic.

164
00:10:04,170 --> 00:10:07,800
We'll be understanding how quantifiers
work.

165
00:10:07,800 --> 00:10:09,800
But what are qualifiers?

166
00:10:09,800 --> 00:10:12,550
And after all if the central words that

167
00:10:12,550 --> 00:10:14,610
we're going to be concerned about this
week.

168
00:10:14,610 --> 00:10:17,900
The central concept this week is the
concept of the quantifier.

169
00:10:17,900 --> 00:10:22,060
Why is this called categorical logic
anyway why not quantifier logic.

170
00:10:22,060 --> 00:10:23,220
Categorical logic is

171
00:10:23,220 --> 00:10:25,470
the logic of categories.

172
00:10:25,470 --> 00:10:30,960
Quantifiers are words like all, some,
none, only, at least, and so forth.

173
00:10:32,370 --> 00:10:34,480
What do these things have to do with each
other?

174
00:10:35,630 --> 00:10:37,550
We'll talk about that in the next lecture.

