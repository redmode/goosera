1
00:00:03,240 --> 00:00:05,860
Perhaps the most common kind of inductive
argument

2
00:00:05,860 --> 00:00:09,060
is called an inference to the best
explanation.

3
00:00:09,060 --> 00:00:12,420
And to understand this kind of argument we
need to think way back

4
00:00:12,420 --> 00:00:14,440
to the first week when we

5
00:00:14,440 --> 00:00:17,300
discussed the difference between
justification and explanation.

6
00:00:17,300 --> 00:00:22,900
If somebody uses an argument to justify a
conclusion.

7
00:00:22,900 --> 00:00:28,930
Then they are trying to give a reason to
believe that the conclusion is true and

8
00:00:28,930 --> 00:00:31,660
may be you didn't believe that conclusion
before you heard the argument.

9
00:00:33,480 --> 00:00:37,865
But if somebody gives an argument to
explain the conclusion and

10
00:00:37,865 --> 00:00:40,500
they're taking it for granted that the
thing they were explaining

11
00:00:40,500 --> 00:00:44,660
is true and they are trying to understand
why it's true

12
00:00:44,660 --> 00:00:48,080
and the argument is supposed to help us
understand why it's true.

13
00:00:49,930 --> 00:00:51,700
Now the weird thing about an inference to

14
00:00:51,700 --> 00:00:54,370
the best explanation is it really combines
these two.

15
00:00:55,720 --> 00:01:00,630
It uses the argument to justify belief in
the conclusion.

16
00:01:00,630 --> 00:01:03,160
That is, to give you a reason to believe
that it's true.

17
00:01:03,160 --> 00:01:09,480
And yet, a premise of the argument is that
that conclusion explains.

18
00:01:09,480 --> 00:01:11,460
A phenomenon that you took for granted.

19
00:01:13,060 --> 00:01:16,530
Inferences of the best explanation are
extremely common.

20
00:01:16,530 --> 00:01:19,430
For example, almost every detective story

21
00:01:19,430 --> 00:01:22,000
uses an inferences of the best
explanation.

22
00:01:22,000 --> 00:01:25,340
Just imagine that there are three suspects
and you know

23
00:01:25,340 --> 00:01:28,530
that one of those three suspects must have
done it because.

24
00:01:28,530 --> 00:01:33,160
The door wasn't jimmied open, so whoever
got into the room must have had a key.

25
00:01:33,160 --> 00:01:35,330
And there are only three people who had
keys.

26
00:01:35,330 --> 00:01:37,160
But one of them couldn't have been the one

27
00:01:37,160 --> 00:01:39,510
who did it, because she has really small
feet.

28
00:01:39,510 --> 00:01:41,630
And the footprints outside of the door are

29
00:01:41,630 --> 00:01:44,280
really large, and so it couldn't have been
her.

30
00:01:44,280 --> 00:01:45,550
And the second person couldn't have done
it

31
00:01:45,550 --> 00:01:47,100
because they have an alibi on the other
side

32
00:01:47,100 --> 00:01:47,950
of town.

33
00:01:47,950 --> 00:01:51,530
So it must have been the third person who
did it.

34
00:01:51,530 --> 00:01:56,910
Notice what you did is, you can't explain
why the door wasn't jimmied

35
00:01:56,910 --> 00:02:01,710
by the person who got in because there
were

36
00:02:01,710 --> 00:02:04,730
no marks and that means that the best
explanation of that

37
00:02:04,730 --> 00:02:06,220
must be that the person who did it had a
key.

38
00:02:07,280 --> 00:02:10,400
And the best explanation why the
footprints were so large, was

39
00:02:10,400 --> 00:02:13,440
that it was a large person who wore large
shoes, and

40
00:02:13,440 --> 00:02:17,000
the best explanation of why the alibi
would place the

41
00:02:17,000 --> 00:02:18,640
person on the other side of town was that
they

42
00:02:18,640 --> 00:02:21,610
were being honest assuming it's not their
brother they're not

43
00:02:21,610 --> 00:02:24,330
that kind of person who would lie or so
on.

44
00:02:24,330 --> 00:02:26,200
So the best explanation.

45
00:02:26,200 --> 00:02:28,630
Of why the jewels are missing from the
room.

46
00:02:28,630 --> 00:02:31,440
Or why there's a dead body in the room.

47
00:02:31,440 --> 00:02:35,760
is that this third suspect is the one who
did it.

48
00:02:35,760 --> 00:02:38,930
So all the detective stories work like
that.

49
00:02:38,930 --> 00:02:42,180
It's almost always an inference to the
best explanation.

50
00:02:42,180 --> 00:02:46,010
The best explanation is: he's the one who
did it.

51
00:02:46,010 --> 00:02:50,100
The one implication of this, is that no
matter what Sherlock Holmes says.

52
00:02:50,100 --> 00:02:52,530
The form of reasoning that he was using,

53
00:02:52,530 --> 00:02:56,280
was inductive reason, not deduction, as he
claimed.

54
00:02:57,430 --> 00:03:00,050
It's not only detective novels, it's also

55
00:03:00,050 --> 00:03:03,000
science that uses inference as the best
explanation.

56
00:03:03,000 --> 00:03:04,390
Take, for example, the greatest

57
00:03:04,390 --> 00:03:07,470
murder mystery in history, what killed the
dinosaurs?

58
00:03:08,660 --> 00:03:12,700
Well some people think that it was mammals
eating their eggs.

59
00:03:12,700 --> 00:03:13,960
Not a chance.

60
00:03:13,960 --> 00:03:17,850
That doesn't explain why there were also
mass extinctions

61
00:03:17,850 --> 00:03:21,200
in the oceans, where the mammals weren't
eating their eggs.

62
00:03:21,200 --> 00:03:27,110
Well some people think that the best
explanation is that there were.

63
00:03:27,110 --> 00:03:29,960
Big volcanic eruptions in southern India.

64
00:03:29,960 --> 00:03:36,060
That seems pretty unlikely too because the
volcanoes that we know, didn't

65
00:03:36,060 --> 00:03:41,350
put out enough material to kill things all
the way around the world.

66
00:03:43,150 --> 00:03:44,180
So.

67
00:03:44,180 --> 00:03:47,510
We think that the best explanation of what
killed the dinosaurs

68
00:03:47,510 --> 00:03:52,690
is that a giant meteor hit in the Yucatan
Peninsula in Mexico.

69
00:03:52,690 --> 00:03:55,000
And that produced lots of soot in

70
00:03:55,000 --> 00:04:00,980
the atmosphere that killed the dinosaurs,
as well as many other species at the time.

71
00:04:00,980 --> 00:04:03,180
But luckily, not mammals.

72
00:04:03,180 --> 00:04:04,490
And so.

73
00:04:04,490 --> 00:04:08,240
We now have a best explanation in science

74
00:04:08,240 --> 00:04:10,130
and a lot of scientific theories work that
way.

75
00:04:10,130 --> 00:04:15,240
As a matter of fact, the point of most
scientific theories is to provide the

76
00:04:15,240 --> 00:04:20,930
best explanation of all the experimental
data, that has been observed by scientist

77
00:04:20,930 --> 00:04:21,609
throughout the world.

78
00:04:23,410 --> 00:04:27,650
And of course we also use inference to the
best explanation in our every day lives.

79
00:04:27,650 --> 00:04:30,680
To try to figure out what's going on
around us.

80
00:04:30,680 --> 00:04:34,770
Here's an example.
Whoa, what was that?

81
00:04:34,770 --> 00:04:39,160
A drop of water just hit my head.
It must be a leak in the roof.

82
00:04:40,950 --> 00:04:43,870
Now that is an inference to the best
explanation.

83
00:04:43,870 --> 00:04:46,210
Starts with an observation of

84
00:04:46,210 --> 00:04:48,210
the water hitting my head.

85
00:04:48,210 --> 00:04:52,340
Then I think about why the water would be
coming down.

86
00:04:52,340 --> 00:04:55,840
And the best explanation I can come up
with is that there's a leak in the roof.

87
00:04:57,050 --> 00:04:59,650
So I conclude that there is a leak in the
roof.

88
00:05:01,010 --> 00:05:02,110
Notice.

89
00:05:02,110 --> 00:05:04,870
That this argument uses an explanation but
it

90
00:05:04,870 --> 00:05:08,210
runs in the opposite direction from an
ordinary explanation.

91
00:05:08,210 --> 00:05:11,350
In an explanation the premises

92
00:05:11,350 --> 00:05:13,100
explain the conclusion.

93
00:05:13,100 --> 00:05:16,570
And the conclusion describes the
phenomenon that was observed.

94
00:05:17,740 --> 00:05:20,769
When there is a leak in the roof water
drops through the roof.

95
00:05:21,800 --> 00:05:26,200
When water comes through the roof, it
drops on to whatever is in its way.

96
00:05:26,200 --> 00:05:30,518
I am in its way, therefore, water drops
onto me.

97
00:05:30,518 --> 00:05:36,670
This explains that.

98
00:05:36,670 --> 00:05:38,480
In contrast, an inference to the

99
00:05:38,480 --> 00:05:40,959
best explanation works in the opposite
direction.

100
00:05:42,610 --> 00:05:46,160
The conclusion is what does the
explaining.

101
00:05:46,160 --> 00:05:49,530
The premises are what get explained.

102
00:05:49,530 --> 00:05:54,481
And the point is to justify belief in the
conclusion, not to explain it.

103
00:05:54,481 --> 00:05:56,069
Water drops onto me.

104
00:05:56,069 --> 00:06:01,410
A leak in the roof would explain why water
drops onto me.

105
00:06:01,410 --> 00:06:01,920
No better

106
00:06:01,920 --> 00:06:06,590
explanation is available, therefore there
must be a leak in the roof.

107
00:06:08,300 --> 00:06:10,720
Notice that the conclusion of the left

108
00:06:10,720 --> 00:06:14,880
argument is a premise of the right
argument.

109
00:06:14,880 --> 00:06:20,299
And the conclusion of the right argument
is part of a premise of the left argument.

110
00:06:21,650 --> 00:06:27,070
So, the form is different.
The function is different as well.

111
00:06:27,070 --> 00:06:29,770
In the explanation on the left, we start

112
00:06:29,770 --> 00:06:32,200
out already knowing that the conclusion is
true.

113
00:06:33,270 --> 00:06:35,430
In the inference to the best explanation,

114
00:06:35,430 --> 00:06:38,030
on the right, we argue that that
conclusion

115
00:06:38,030 --> 00:06:40,670
is probably true because it is the best

116
00:06:40,670 --> 00:06:43,749
explanation of the observation in the
first premise.

117
00:06:45,460 --> 00:06:48,460
That's why we call it an inference to the
best explanation.

118
00:06:50,060 --> 00:06:51,196
Is this argument valid?

119
00:06:51,196 --> 00:06:55,990
Yes or no?
Right,

120
00:06:55,990 --> 00:06:59,820
it's not valid because it's possible for
the premises to be true,

121
00:06:59,820 --> 00:07:04,360
and the conclusion false.
Now, does its invalidity.

122
00:07:04,360 --> 00:07:10,320
Make the argument bad?
Yes or no?

123
00:07:10,320 --> 00:07:17,050
Right it does not make it bad, because an
inductive argument can be good even

124
00:07:17,050 --> 00:07:21,650
if it's invalid.
The next question is a bit trickier.

125
00:07:21,650 --> 00:07:26,620
Remember that an argument is defeasible.
If further information can make

126
00:07:26,620 --> 00:07:31,505
the argument weak.
So, is this argument

127
00:07:31,505 --> 00:07:36,268
defeasible?
Yes or no?

128
00:07:36,268 --> 00:07:42,130
Right, it is defeasible because further
information could make this

129
00:07:42,130 --> 00:07:45,250
argument weak.
So what?

130
00:07:46,400 --> 00:07:52,380
Does its defeasibility mean that this
argument was weak even before

131
00:07:52,380 --> 00:07:57,992
we got that additional information.
Yes or no?

132
00:07:57,992 --> 00:07:59,170
No.

133
00:07:59,170 --> 00:08:04,269
Because inductive arguments can be strong
even though they are defeasible.

134
00:08:05,520 --> 00:08:07,510
So now we know that this argument

135
00:08:07,510 --> 00:08:13,870
is invalid, and is defeasible, but it
still might be strong and good.

