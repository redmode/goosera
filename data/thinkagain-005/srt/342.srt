1
00:00:03,680 --> 00:00:06,270
In the last class, we talked about
immediate categorical

2
00:00:06,270 --> 00:00:10,220
inferences, which are inferences that have
a single premise

3
00:00:10,220 --> 00:00:16,840
and a conclusion where each of those two
propositions is of the a, e, i or o form.

4
00:00:16,840 --> 00:00:21,950
Today, we're going to talk about a new
kind of inference called a syllogism.

5
00:00:21,950 --> 00:00:23,180
So what's a syllogism?

6
00:00:24,230 --> 00:00:29,350
Here's a definition.
A syllogism is an argument that

7
00:00:29,350 --> 00:00:34,880
has two premises and a conclusion, where
all three of those propositions

8
00:00:34,880 --> 00:00:40,980
are of the form A, E, I, or O.
Now.

9
00:00:40,980 --> 00:00:45,920
The conclusion is going to have two
categories in it, one category that

10
00:00:45,920 --> 00:00:50,610
is modified by a quantifier and we're
going to call that the subject category.

11
00:00:50,610 --> 00:00:53,250
The other category is not modified by a
quantifier.

12
00:00:53,250 --> 00:00:55,030
We're going to call that the predicate
category.

13
00:00:56,050 --> 00:01:01,290
Now, the subject category is what we call
the subject term of the syllogism.

14
00:01:01,290 --> 00:01:05,300
The predicate category, the category
that's not modified by a quantifier in

15
00:01:05,300 --> 00:01:09,740
the conclusion, that's what we're going to
call the predicate term of the syllogism.

16
00:01:09,740 --> 00:01:13,790
Now every syllogism is going to have a
premise that

17
00:01:13,790 --> 00:01:18,030
includes the subject term and another
premise that includes.

18
00:01:18,030 --> 00:01:21,060
The predicate term.
Now the premise

19
00:01:21,060 --> 00:01:23,140
that includes the subject term is what
we're

20
00:01:23,140 --> 00:01:26,470
going to call the minor premise of the
syllogism.

21
00:01:26,470 --> 00:01:29,730
And the premise that includes the
predicate term, is

22
00:01:29,730 --> 00:01:33,240
what we're going to call the major premise
of the syllogism.

23
00:01:33,240 --> 00:01:36,350
So every syllogism is going to have two
premises where

24
00:01:36,350 --> 00:01:40,710
one premise is the minor premise and it's
going to include.

25
00:01:40,710 --> 00:01:43,170
The subject term of the syllogism which is

26
00:01:43,170 --> 00:01:46,120
the category that's modified by the
quantifier in the

27
00:01:46,120 --> 00:01:49,010
conclusion of the syllogism and the other
premise is

28
00:01:49,010 --> 00:01:52,630
the major premise of syllogism, it
includes the predicate term

29
00:01:52,630 --> 00:01:55,120
of the syllogism which is the category
that's not

30
00:01:55,120 --> 00:01:58,978
modified by a quantifier in the conclusion
of the syllogism.

31
00:01:58,978 --> 00:02:03,770
Okay, now let's look at how we can use
Venn diagrams to represent the information

32
00:02:03,770 --> 00:02:07,200
that's carried by syllogisms and so to
asses

33
00:02:07,200 --> 00:02:09,370
whether or not a particular syllogism is
valid.

34
00:02:11,350 --> 00:02:16,320
In order to visually represent the
information that's given in the

35
00:02:16,320 --> 00:02:20,300
syllogism, we need to use a Venn diagram
with three circles.

36
00:02:20,300 --> 00:02:22,240
Not just two circles because in the

37
00:02:22,240 --> 00:02:25,080
syllogism we have three categories, we
have the

38
00:02:25,080 --> 00:02:27,932
subject term, the subject category, the
middle term,

39
00:02:27,932 --> 00:02:31,260
the G's, and the predicate term, the H's.

40
00:02:31,260 --> 00:02:34,080
So we need to get a Venn diagram with

41
00:02:34,080 --> 00:02:36,590
all three of those circles to represent
the information

42
00:02:36,590 --> 00:02:38,199
contained in the syllogism.

43
00:02:39,460 --> 00:02:42,340
Now I've described all this very
abstractly.

44
00:02:42,340 --> 00:02:46,080
Let me give some examples so you can see
how this works and how we can use Venn

45
00:02:46,080 --> 00:02:48,820
diagrams like this to figure out whether
or not

46
00:02:48,820 --> 00:02:52,140
syllogisms are valid, and to explain why
they're valid.

47
00:02:52,140 --> 00:02:52,640
Okay.

48
00:02:54,230 --> 00:02:59,159
So consider this syllogism.
All Duke students are humans.

49
00:03:00,510 --> 00:03:02,860
All humans are animals.

50
00:03:02,860 --> 00:03:07,530
Therefore, all Duke students are animals.
Okay.

51
00:03:07,530 --> 00:03:11,790
Valid or not?
Well, pretty obviously, it is valid.

52
00:03:13,280 --> 00:03:17,150
But a Venn diagram could help us to
understand why it's valid.

53
00:03:17,150 --> 00:03:20,990
So let's diagram the information contained
in the premises.

54
00:03:20,990 --> 00:03:24,710
So the first premise, recall, was that all
Duke students are humans.

55
00:03:24,710 --> 00:03:27,730
Well, if we want to show that all Duke
students are humans,

56
00:03:27,730 --> 00:03:30,590
that's to say that whatever Duke students
there are,

57
00:03:30,590 --> 00:03:34,350
have got to be inside the circle of the
humans.

58
00:03:34,350 --> 00:03:36,940
So this region of the Duke students
circle,

59
00:03:36,940 --> 00:03:38,690
this region of the circle that's outside
the

60
00:03:38,690 --> 00:03:43,160
circle of the humans, We can shade that in
to indicate that there is nothing there.

61
00:03:43,160 --> 00:03:43,320
Right.

62
00:03:43,320 --> 00:03:46,060
Nothing here because whatever Duke
students there

63
00:03:46,060 --> 00:03:48,660
are, have gotta be in this region.

64
00:03:50,570 --> 00:03:51,070
Okay.

65
00:03:51,070 --> 00:03:53,980
The second premise says that all humans
are animals.

66
00:03:53,980 --> 00:03:58,070
Well, if all humans are animals, then what
that tells us.

67
00:03:58,070 --> 00:04:02,750
Is that whatever humans there are have got
to be inside the circle of the animal.

68
00:04:02,750 --> 00:04:05,440
So we can shade in the portion of

69
00:04:05,440 --> 00:04:09,890
the human circle that's outside the animal
circle, right?

70
00:04:09,890 --> 00:04:13,050
because there aren't any humans out there.
So shade that in.

71
00:04:14,172 --> 00:04:19,180
Okay, but now we look at the diagram, with
those regions

72
00:04:19,180 --> 00:04:23,110
shaded in, and what can we conclude.
Well we can conclude that whatever Duke

73
00:04:23,110 --> 00:04:30,940
students there are, have got to be in this
region right here.

74
00:04:30,940 --> 00:04:35,150
That's the only region where there could
be any Duke students.

75
00:04:35,150 --> 00:04:38,050
Given the two premises of our argument.

76
00:04:38,050 --> 00:04:44,190
In other words, all Duke students are
animals, and that's just

77
00:04:44,190 --> 00:04:48,780
the conclusion of our syllogism, remember.
All Duke students are animals.

78
00:04:49,870 --> 00:04:55,280
So we just used the Venn diagram to
explain why.

79
00:04:55,280 --> 00:04:59,250
This syllogism, which is obviously valid,
is valid.

80
00:04:59,250 --> 00:05:00,860
We explained why it is valid.

81
00:05:00,860 --> 00:05:05,620
It is valid because when you shade in the
portion of the Duke students

82
00:05:05,620 --> 00:05:09,390
circle that's outside the human circle,
and you shade in the portion of the human

83
00:05:09,390 --> 00:05:14,430
circle that's outside the animal circle.
The only place left over in the Duke

84
00:05:14,430 --> 00:05:19,940
student circle, for there to be any Duke
students, is inside the animal circle.

85
00:05:19,940 --> 00:05:24,380
And so, all Duke students are animals, as
we all know.

86
00:05:27,220 --> 00:05:30,520
Now, here's a second syllogism, let's
consider this one.

87
00:05:30,520 --> 00:05:32,089
Some Duke students are humans.

88
00:05:33,340 --> 00:05:34,680
All humans are animals.

89
00:05:34,680 --> 00:05:39,148
Therefore, some Duke students are animals.
Valid, or not?

90
00:05:39,148 --> 00:05:46,990
Well, let's see.
So, some Duke students are humans.

91
00:05:46,990 --> 00:05:49,120
How would we represent that information?

92
00:05:49,120 --> 00:05:52,610
Some Duke students are humans.
What that means is that

93
00:05:52,610 --> 00:05:56,370
there's got to be something in this part
of the

94
00:05:56,370 --> 00:06:01,650
circle of Duke Students that's also in the
circle of humans.

95
00:06:03,180 --> 00:06:06,180
But the first premise doesn't tell us
where that thing

96
00:06:06,180 --> 00:06:09,690
would be, would it be here or would it be
here.

97
00:06:11,310 --> 00:06:15,430
Well, let's hedge our bets and draw it
right here, since we don't know.

98
00:06:15,430 --> 00:06:17,810
We'll draw it on the borderline of the
animal

99
00:06:17,810 --> 00:06:24,300
circle, since the first premise doesn't
yet tell us whether it's here or here.

100
00:06:24,300 --> 00:06:24,800
Okay.

101
00:06:26,480 --> 00:06:30,250
The second premise said all humans are
animals.

102
00:06:30,250 --> 00:06:30,520
Okay.

103
00:06:30,520 --> 00:06:34,820
Well, if all humans are animals, what that
tells us is that,

104
00:06:36,900 --> 00:06:43,640
there aren't any humans outside the animal
circle, so we can just shade in the

105
00:06:43,640 --> 00:06:45,740
part of the human circle that's outside
the

106
00:06:45,740 --> 00:06:48,250
animal circle, just shade it in right
there.

107
00:06:49,760 --> 00:06:53,290
But then notice, remember we had to have
an X inside

108
00:06:53,290 --> 00:06:58,140
our Duke student circle and our human
circle inside the intersection.

109
00:06:58,140 --> 00:07:00,150
Well, it can't be in here because this

110
00:07:00,150 --> 00:07:02,310
is shaded in, which means there's nothing
there.

111
00:07:02,310 --> 00:07:05,070
So the only place it can be is right
there.

112
00:07:06,190 --> 00:07:09,160
So now we know that the X had to be over
here.

113
00:07:09,160 --> 00:07:12,060
But once we put the X over there, which we
have to

114
00:07:12,060 --> 00:07:13,530
given the information given the
information

115
00:07:13,530 --> 00:07:14,670
and the two premises of our syllogism.

116
00:07:14,670 --> 00:07:14,670
[UNKNOWN].

117
00:07:14,670 --> 00:07:16,500
What can we conclude?

118
00:07:16,500 --> 00:07:20,930
We can conclude that some Duke students
are animals.

119
00:07:20,930 --> 00:07:24,870
And that's exactly what the conclusion of
the syllogism is.

120
00:07:24,870 --> 00:07:28,270
That some Duke students are animals.

121
00:07:31,530 --> 00:07:35,230
Finally, oh that should say example three
not example one.

122
00:07:35,230 --> 00:07:37,890
Finally, consider this syllogism.

123
00:07:37,890 --> 00:07:40,020
No Duke students are humans.

124
00:07:40,020 --> 00:07:43,760
All humans are animals.
Therefore no Duke students are animals.

125
00:07:45,260 --> 00:07:48,420
OK, now how would we represent that using
the Venn Diagram?

126
00:07:48,420 --> 00:07:49,230
Well.

127
00:07:49,230 --> 00:07:51,790
No Duke students are humans.

128
00:07:51,790 --> 00:07:56,610
So, that means that we have to shade in
the portion

129
00:07:56,610 --> 00:07:58,720
of the Duke students circle that's inside
the

130
00:07:58,720 --> 00:08:02,110
humans circle to show that there's nothing
in there.

131
00:08:02,110 --> 00:08:02,370
Right?

132
00:08:02,370 --> 00:08:05,210
None of the Duke students are humans.

133
00:08:06,550 --> 00:08:09,230
Okay.
All humans are animals.

134
00:08:09,230 --> 00:08:11,140
So that means we have to shade in the

135
00:08:11,140 --> 00:08:14,520
portion of the human circle that's outside
the animal circle,

136
00:08:18,330 --> 00:08:21,600
all right?
Because there are no humans out there.

137
00:08:21,600 --> 00:08:23,140
All humans are animals.

138
00:08:24,390 --> 00:08:24,890
Right?
And

139
00:08:27,050 --> 00:08:29,809
the conclusion was that no Duke students
are animals.

140
00:08:32,830 --> 00:08:33,610
But wait a second.

141
00:08:35,620 --> 00:08:37,290
You can't read that off the diagram.

142
00:08:38,340 --> 00:08:42,330
There could be lots of Duke students over
here who are animals.

143
00:08:42,330 --> 00:08:45,480
There could be all sorts of Duke students
who are animals right there.

144
00:08:45,480 --> 00:08:48,450
They wouldn't be human, but there could
still

145
00:08:48,450 --> 00:08:50,179
be plenty of Duke students that are
animals.

146
00:08:52,540 --> 00:08:58,550
In other words, this third syllogism is
not valid, and it's not

147
00:08:58,550 --> 00:09:02,930
valid for a reason that's made clear by
the Venn diagram that we just looked at.

148
00:09:02,930 --> 00:09:06,360
The Venn diagram shows us that and
explains

149
00:09:06,360 --> 00:09:09,920
why the inference, the syllogism, is not
valid.

150
00:09:09,920 --> 00:09:12,960
Just because no Duke students are humans
and all humans are

151
00:09:12,960 --> 00:09:17,580
animals It doesn't follow that no Duke
students are animals; those premises

152
00:09:17,580 --> 00:09:21,590
leave it open that there are plenty of
animal Duke students.

153
00:09:21,590 --> 00:09:28,180
They just wouldn't be the humans.
So, in this lecture I've tried to show how

154
00:09:28,180 --> 00:09:35,480
we can use Venn diagrams to predict that
and to explain why syllogisms.

155
00:09:35,480 --> 00:09:37,730
Are valid or invalid, as the case may be.

156
00:09:39,120 --> 00:09:42,850
Next time, we'll apply these lessons to
some examples

157
00:09:42,850 --> 00:09:45,310
that we've already looked at before, but
that we

158
00:09:45,310 --> 00:09:49,330
weren't treating as syllogisms when we
looked at them before.

159
00:09:49,330 --> 00:09:50,310
See you next time.

