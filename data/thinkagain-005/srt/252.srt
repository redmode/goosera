1
00:00:00,025 --> 00:00:06,510
In the previous lecture, we looked at the
skeptical regress problem and

2
00:00:06,510 --> 00:00:12,130
I kind of introduced three different ways
of solving that problem in practical life.

3
00:00:12,130 --> 00:00:15,859
Namely assuring, guarding, and
discounting.

4
00:00:16,960 --> 00:00:18,530
Those are three ways to solve the

5
00:00:18,530 --> 00:00:21,700
skeptical regress problem in practical
life, and

6
00:00:21,700 --> 00:00:25,470
in the next three lectures what we're
going to do is look at each of these,

7
00:00:25,470 --> 00:00:30,240
in much more detail, and more carefully,
so as to understand how they work.

8
00:00:30,240 --> 00:00:31,271
Let's begin with assuring.

9
00:00:32,950 --> 00:00:34,750
Here's an example.

10
00:00:34,750 --> 00:00:37,945
I assure you that smoking is bad for your
health.

11
00:00:37,945 --> 00:00:41,750
If I say, I assure you, then I'm trying

12
00:00:41,750 --> 00:00:45,150
to get you to accept that claim on my
authority.

13
00:00:45,150 --> 00:00:48,250
Now I might have some reason for saying

14
00:00:48,250 --> 00:00:50,630
that smoking is bad for your health,
namely,

15
00:00:50,630 --> 00:00:54,360
I've read the US Surgeon General's report,
which cites the

16
00:00:54,360 --> 00:00:58,025
scientific evidence to show, that smoking
is bad for your health.

17
00:00:58,025 --> 00:01:00,690
But notice that I don't actually say it,
when I

18
00:01:00,690 --> 00:01:05,200
say, well it's obvious that smoking is bad
for your health.

19
00:01:05,200 --> 00:01:08,620
Or everybody knows that smoking is bad for
your health.

20
00:01:08,620 --> 00:01:13,552
Or I assure you, certainly, clearly,
smoking is bad for your health.

21
00:01:13,552 --> 00:01:15,950
Then what I'm doing

22
00:01:15,950 --> 00:01:17,770
is I am trying to get you to accept

23
00:01:17,770 --> 00:01:22,410
that premise without actually citing the
Surgeon General or anybody.

24
00:01:22,410 --> 00:01:23,700
I haven't cited the evidence.

25
00:01:23,700 --> 00:01:27,560
I simply have indicated to you I do have
evidence.

26
00:01:27,560 --> 00:01:30,920
I do have a reason to believe that smoking
is bad

27
00:01:30,920 --> 00:01:33,086
for your health, but I haven't given you
what the reason is.

28
00:01:34,930 --> 00:01:35,906
So what good is that?

29
00:01:35,906 --> 00:01:38,790
It's a lot of good, because what it means

30
00:01:38,790 --> 00:01:41,070
is that if I don't give the reason, you
can't

31
00:01:41,070 --> 00:01:42,418
question the reason.

32
00:01:42,418 --> 00:01:48,480
If I simply say everybody believes it,
it's certainly true, then you can't

33
00:01:48,480 --> 00:01:52,510
ask whether those people who believe it
have any reason to believe it.

34
00:01:52,510 --> 00:01:54,900
Or rather I have any reason to be so
certain about it.

35
00:01:54,900 --> 00:01:58,080
If I don't give the reason, I've cut off

36
00:01:58,080 --> 00:02:00,410
your attack of the reason that I would
give.

37
00:02:01,570 --> 00:02:03,980
So assuring is kind of tricky.

38
00:02:03,980 --> 00:02:05,275
I say I assure you,

39
00:02:05,275 --> 00:02:08,376
and if you can trust me, that's fine.

40
00:02:08,376 --> 00:02:11,280
But if you can't trust me, then you should
be

41
00:02:11,280 --> 00:02:14,380
asking, well, what kind of reason does he
really have?

42
00:02:15,590 --> 00:02:18,068
There are many ways to assure people.

43
00:02:18,068 --> 00:02:19,790
There are three kinds that we're

44
00:02:19,790 --> 00:02:23,290
going to look at: authoritative,
reflexive, and abusive.

45
00:02:24,330 --> 00:02:25,560
Let's start with authoritative.

46
00:02:26,820 --> 00:02:29,750
Authoritative is just what it says, it
cites an authority.

47
00:02:29,750 --> 00:02:30,440
So, I might

48
00:02:30,440 --> 00:02:34,550
say I assure you that smoking is bad for
your health.

49
00:02:34,550 --> 00:02:39,350
The Surgeon General has shown that smoking
is bad for your health.

50
00:02:39,350 --> 00:02:42,690
And I cite an authority, the Surgeon
General.

51
00:02:42,690 --> 00:02:44,160
Okay.
Fine.

52
00:02:44,160 --> 00:02:48,520
But, have you seen the studies?
Have I cited the numbers?

53
00:02:48,520 --> 00:02:50,010
Have I told you when the studies

54
00:02:50,010 --> 00:02:52,490
were done, how many subjects, under what
circumstances?

55
00:02:52,490 --> 00:02:55,680
Which statistical tests were used?
How do you know it's causation instead or

56
00:02:55,680 --> 00:02:57,265
correlation and so on and so on.

57
00:02:57,265 --> 00:03:00,650
No, I just say the Surgeon General has
shown this.

58
00:03:00,650 --> 00:03:03,530
And you're supposed to take my word for it
that those are good

59
00:03:03,530 --> 00:03:06,090
studies, unless of course your share my

60
00:03:06,090 --> 00:03:09,590
assumption that the Surgeon General is
trustworthy.

61
00:03:09,590 --> 00:03:13,820
So, that's the way an authoritative
assurance works.

62
00:03:13,820 --> 00:03:17,090
It cites an authority that the audience

63
00:03:17,090 --> 00:03:20,090
shares as an authority with the other
person.

64
00:03:20,090 --> 00:03:21,245
They share the assumption

65
00:03:21,245 --> 00:03:25,570
that that the authority is trustworthy.

66
00:03:25,570 --> 00:03:27,880
Now, sometimes it's the Surgeon General

67
00:03:27,880 --> 00:03:30,390
and the Surgeon General is pretty
reliable.

68
00:03:30,390 --> 00:03:31,875
But you also get this kind of

69
00:03:31,875 --> 00:03:37,246
thing from reporters they say, an
unimpeachable source

70
00:03:37,246 --> 00:03:43,190
close to the President has assured me that
the President's plans are such and such.

71
00:03:43,190 --> 00:03:46,390
And they cite an unimpeachable source, but
they won't tell you want the source

72
00:03:46,390 --> 00:03:48,650
is and they won't tell you how the source
knew it was true.

73
00:03:49,690 --> 00:03:53,940
So they're citing an authority but they're
not telling you what the

74
00:03:53,940 --> 00:03:57,490
authority is, or what reason the authority
has for what the authority claims.

75
00:03:58,510 --> 00:04:01,836
Now if you trust that authority, then
that's fine.

76
00:04:01,836 --> 00:04:08,170
And if authority's reliable, then you
might be safe, but if you don't know

77
00:04:08,170 --> 00:04:11,860
who the authority is or where they got
their information, there can be problems.

78
00:04:13,250 --> 00:04:15,460
And here's my favorite.
Here's my favorite.

79
00:04:15,460 --> 00:04:18,372
I sometimes hear students say, but
Professor

80
00:04:18,372 --> 00:04:18,373
[UNKNOWN]

81
00:04:18,373 --> 00:04:25,550
Armstrong, my teacher in the other course
said that blah, blah, blah, blah, blah.

82
00:04:25,550 --> 00:04:30,880
Well, why do you trust your professors?
I mean why do you trust me?

83
00:04:30,880 --> 00:04:33,070
You shouldn't be trusting me or your other
professors any

84
00:04:33,070 --> 00:04:36,620
more than you're trusting an unimpeachable
source close to the president.

85
00:04:37,780 --> 00:04:41,260
So, you have to trust somebody sometime,
and

86
00:04:41,260 --> 00:04:42,880
you have to decide who you're going to
trust.

87
00:04:44,430 --> 00:04:47,610
And then, it's going to be okay to cite an
authority, in

88
00:04:47,610 --> 00:04:51,620
this kind of assurance, but you have to
watch out for tricks when

89
00:04:51,620 --> 00:04:54,350
people start citing authorities that
aren't

90
00:04:54,350 --> 00:04:56,570
really authorities, because even the best

91
00:04:56,570 --> 00:04:58,840
authorities sometimes do studies that
aren't

92
00:04:58,840 --> 00:05:03,250
very careful, and might even be wrong.

93
00:05:03,250 --> 00:05:09,430
A second kind of assurance, is what I call
reflexive, reflexive because it's

94
00:05:09,430 --> 00:05:10,559
talking about yourself.

95
00:05:11,900 --> 00:05:16,960
I believe that, I know that, I am certain
that, I feel sure that.

96
00:05:18,150 --> 00:05:20,636
And you're signing something about your
own mental state.

97
00:05:20,636 --> 00:05:21,325
You feel sure.

98
00:05:21,325 --> 00:05:25,710
Great, you feel sure, but why do you feel
sure?

99
00:05:25,710 --> 00:05:29,880
Notice however, that this assurance works,
because people don't

100
00:05:29,880 --> 00:05:32,470
want to question what other people feel
sure about.

101
00:05:32,470 --> 00:05:34,762
If I say, well, I feel sure that

102
00:05:34,762 --> 00:05:38,110
this is going to happen, in many
societies, it's going to

103
00:05:38,110 --> 00:05:40,676
be impolite to go, well, I don't feel
sure.

104
00:05:40,676 --> 00:05:44,020
Matter of fact, I think you're wrong.

105
00:05:44,020 --> 00:05:46,055
Or, why do you feel sure?

106
00:05:46,055 --> 00:05:47,662
because then you're questioning the
person.

107
00:05:47,662 --> 00:05:54,130
And if that's impolite, then using this
type of phrase, I am

108
00:05:54,130 --> 00:06:00,320
certain that, will get people to shut up
and not say anything.

109
00:06:00,320 --> 00:06:02,202
So here's an example from Monty Python.

110
00:06:02,202 --> 00:06:06,900
>> Now let's get one thing quite clear,
I most definitely told you.

111
00:06:06,900 --> 00:06:10,140
But my favorite is when people say you
know, I've held

112
00:06:10,140 --> 00:06:15,140
this opinion for years or I've thought
about it year after year.

113
00:06:15,140 --> 00:06:19,420
It's really bothered me and you know,
after careful consideration I've come to

114
00:06:19,420 --> 00:06:25,790
the conclusion that blah, blah, blah,
whatever, and you're supposed to say,

115
00:06:25,790 --> 00:06:28,850
well, since you've thought about it so
much, I'll save myself a lot of

116
00:06:28,850 --> 00:06:32,590
trouble of thinking about it, and trust
you and go along with what you say.

117
00:06:34,130 --> 00:06:38,150
Well if you let other people do your
thinking for you, then that's up to you.

118
00:06:39,190 --> 00:06:41,870
But you're going to be misled in some
cases cause they thought about it for

119
00:06:41,870 --> 00:06:43,900
years and years and gotten themself into a

120
00:06:43,900 --> 00:06:46,570
tizzy and ended up at the wrong
conclusion.

121
00:06:46,570 --> 00:06:48,120
So the fact that somebody's thought about

122
00:06:48,120 --> 00:06:50,785
it for years doesn't necessarily mean it's
right.

123
00:06:50,785 --> 00:06:54,240
But somehow in conversation, when you say
I've thought about it

124
00:06:54,240 --> 00:06:59,070
for a long time and checked all the
sources I could,

125
00:06:59,070 --> 00:07:02,410
then people will take the fact that you've
reached a certain

126
00:07:02,410 --> 00:07:08,600
conclusion to be okay for them to reach
the same conclusion.

127
00:07:08,600 --> 00:07:12,940
And that's how this reflexive type of
assurance works.

128
00:07:14,440 --> 00:07:17,610
The most fun of all is the abusive
assurance.

129
00:07:18,620 --> 00:07:23,225
Here's another example from Monty Python.
>> No, no, nonsense!

130
00:07:23,225 --> 00:07:27,110
>> Maybe he's right.
Maybe it is nonsense.

131
00:07:27,110 --> 00:07:30,640
That the point is that he gets you to
accept

132
00:07:30,640 --> 00:07:35,345
what he believes by abusing you and
calling it nonsense.

133
00:07:35,345 --> 00:07:41,006
And the same thing happens when somebody
says, nobody but a fool would think that.

134
00:07:41,006 --> 00:07:44,499
Everybody knows this.

135
00:07:44,499 --> 00:07:48,635
If you say, everybody knows it, then
you're saying,

136
00:07:48,635 --> 00:07:52,130
well, if you don't know it, then you're a
dummy.

137
00:07:52,130 --> 00:07:57,910
And so they're abusing you in order to get
you to agree with them

138
00:07:57,910 --> 00:08:03,750
by making a conditional abuse that applies
to you only if you don't agree with them.

139
00:08:03,750 --> 00:08:04,890
And you don't want to be a dummy.

140
00:08:04,890 --> 00:08:07,716
You don't want to speak nonsense.

141
00:08:07,716 --> 00:08:10,040
You don't want to say something that's
stupid.

142
00:08:10,040 --> 00:08:13,320
So if somebody says, you'd have to be
stupid to disagree with

143
00:08:13,320 --> 00:08:18,000
me about this, then that's going to
incline you to agree with them.

144
00:08:18,000 --> 00:08:20,820
Some of these abusive assurances are a
little

145
00:08:20,820 --> 00:08:23,219
bit more subtle, so it's worth mentioning
one

146
00:08:23,219 --> 00:08:28,455
that's used all the time, and this is what
might be called appeal to common sense.

147
00:08:28,455 --> 00:08:31,887
Someone says, it's just common sense that
such and such.

148
00:08:31,887 --> 00:08:34,785
It doesn't matter what the such and such
is,

149
00:08:34,785 --> 00:08:40,290
the point here is simply that if you deny
that, then you're

150
00:08:40,290 --> 00:08:45,370
telling that person you lack common sense.
And nobody wants to lack common sense.

151
00:08:45,370 --> 00:08:49,800
So by saying, it's just common sense that,
it's just plain common sense.

152
00:08:49,800 --> 00:08:52,670
People say it all the time, but what
they're doing is

153
00:08:52,670 --> 00:08:56,000
they're abusing their opponents by saying
that their opponents lack common sense.

154
00:08:56,000 --> 00:08:59,920
So it's a little bit more subtle, but it
shows that abuse

155
00:08:59,920 --> 00:09:05,800
of assurances occur either subtly or
openly all the time.

156
00:09:05,800 --> 00:09:09,868
So we've see three different types of
assurances.

157
00:09:09,868 --> 00:09:10,630
Okay.

158
00:09:10,630 --> 00:09:16,210
The first is authoritative, the second is
reflexive and the third is abusive.

159
00:09:17,870 --> 00:09:22,990
And they're all used in common speech to
stop the skeptical regress problem.

160
00:09:22,990 --> 00:09:25,330
So why do we need any of these assurances

161
00:09:25,330 --> 00:09:30,190
in the first place?
And the answer is, we've got limited time.

162
00:09:31,380 --> 00:09:33,540
You know, you can't go out and check every

163
00:09:33,540 --> 00:09:37,230
study, you can't go out and look into
every issue.

164
00:09:37,230 --> 00:09:43,040
You've just got to accept authorities and
listen to other people and learn from

165
00:09:43,040 --> 00:09:48,100
them, or you'll never be able to figure
out the issues that matter to you in life.

166
00:09:49,900 --> 00:09:53,400
So assuring can be a perfectly good thing.

167
00:09:53,400 --> 00:09:57,510
If you trust the Surgeon General of the
United States instead of going out and

168
00:09:57,510 --> 00:09:59,810
starting your own laboratory and doing
your own

169
00:09:59,810 --> 00:10:02,840
statistical studies, there's absolutely
nothing wrong with that.

170
00:10:04,830 --> 00:10:09,130
But when you start trusting authorities
that are not worthwhile,

171
00:10:09,130 --> 00:10:13,640
or not trustworthy, then, you can get
yourself into trouble.

172
00:10:13,640 --> 00:10:15,125
And we'll talk later in the course,

173
00:10:15,125 --> 00:10:18,560
about, ways to tell which authorities are

174
00:10:18,560 --> 00:10:20,780
reliable or trustworthy and which ones are
not.

175
00:10:21,900 --> 00:10:24,080
That tells you why, assurances are needed

176
00:10:24,080 --> 00:10:26,680
in general, but what about particular
cases?

177
00:10:27,890 --> 00:10:29,649
Well when you're talking to a certain
audience,

178
00:10:30,700 --> 00:10:33,880
if they share your trust of a certain

179
00:10:33,880 --> 00:10:37,080
authority, or if they trust you when you

180
00:10:37,080 --> 00:10:40,200
give a reflexive assurance, or if what
you're

181
00:10:40,200 --> 00:10:45,370
saying really is something that everybody
with common sense believes in,

182
00:10:45,370 --> 00:10:49,300
then again, it can be perfectly fine to
use an assurance.

183
00:10:49,300 --> 00:10:52,850
It saves you the time of having to go
check out every issue

184
00:10:52,850 --> 00:10:59,460
and it helps you avoid the skeptical
regress problem in a very practical way.

185
00:10:59,460 --> 00:11:04,970
So assuring can be a really useful tool in
argument.

186
00:11:04,970 --> 00:11:08,930
But, you have to be careful because
assurances are also subject to

187
00:11:08,930 --> 00:11:12,320
a lot of tricks that you have to learn to
watch out for.

188
00:11:12,320 --> 00:11:15,140
We've already seen that you can cite
authorities

189
00:11:15,140 --> 00:11:17,160
that aren't trustworthy and that's kind of
obvious.

190
00:11:18,260 --> 00:11:22,542
But something that's maybe a little less
obvious is that people

191
00:11:22,542 --> 00:11:27,745
use assurances at points in their argument
in order to distract you.

192
00:11:27,745 --> 00:11:31,160
When something really is questionable,

193
00:11:31,160 --> 00:11:32,640
they often say, well, that's obvious.

194
00:11:33,950 --> 00:11:37,070
In order to get you to not pay attention
to it.

195
00:11:37,070 --> 00:11:43,270
So when someone says that's obvious, it's
certain, I'm sure, it's worth looking

196
00:11:43,270 --> 00:11:48,140
carefully at what they're so sure about
and asking yourself whether you agree with

197
00:11:48,140 --> 00:11:52,630
them because people try to paper over the
cracks in their arguments with

198
00:11:52,630 --> 00:11:56,210
these assurances, at least in some cases,
and you have to learn to watch

199
00:11:56,210 --> 00:11:57,380
out for that.

200
00:11:57,380 --> 00:12:00,420
Another trick is when assurances get
dropped.

201
00:12:00,420 --> 00:12:04,250
People will start off saying, you know, he
believes this, I believe

202
00:12:04,250 --> 00:12:07,005
that, and then they drop it and start
talking as if it's true.

203
00:12:07,005 --> 00:12:10,590
You know, my favorite example of this was
a case a few

204
00:12:10,590 --> 00:12:16,580
years ago when there was a person caught
in Germany for cannibalism.

205
00:12:16,580 --> 00:12:21,480
And it started off with the reporters
saying, well he,

206
00:12:21,480 --> 00:12:25,640
the person accused of cannibalism says
that

207
00:12:25,640 --> 00:12:28,550
there are lots of other cannibals in
Germany.

208
00:12:29,720 --> 00:12:36,190
And then, it becomes, it is reported that
there are lots of cannibals in Germany.

209
00:12:36,190 --> 00:12:42,190
And then it became, sources have said
that, as if the sources are reliable.

210
00:12:42,190 --> 00:12:46,460
And pretty soon they were saying, there
are lots of cannibals in Germany.

211
00:12:46,460 --> 00:12:52,207
And it moved from, he believes it to there
are lots of them.

212
00:12:52,207 --> 00:12:55,860
And that's the trick of dropping the

213
00:12:55,860 --> 00:13:00,100
assurance in a way that can be
illegitimate.

214
00:13:00,100 --> 00:13:05,340
So, assurances can be useful and they can
also be misleading.

215
00:13:05,340 --> 00:13:07,480
And, that's going to be true of all

216
00:13:07,480 --> 00:13:09,690
the different ways of stopping the
skeptical regress.

217
00:13:09,690 --> 00:13:12,310
There can be uses and abuses

218
00:13:12,310 --> 00:13:14,010
of all of these tricks.

219
00:13:14,010 --> 00:13:16,540
For assurances, we want to have an
assurance

220
00:13:16,540 --> 00:13:20,428
when, first of all, somebody might
question it.

221
00:13:20,428 --> 00:13:27,540
Second, the audience accepts the authority
that is being referred to.

222
00:13:27,540 --> 00:13:32,340
And third, it would be too much trouble to
actually cite the

223
00:13:32,340 --> 00:13:35,540
study and all the numbers, and the
evidence, and the particular people.

224
00:13:36,680 --> 00:13:37,390
The assurances

225
00:13:37,390 --> 00:13:42,035
are not appropriate when nobody would
question the claim anyway.

226
00:13:42,035 --> 00:13:44,340
Then why are you wasting your time
assuring people?

227
00:13:45,640 --> 00:13:50,920
When the authority that you're basing your
assurance on is not really trustworthy.

228
00:13:50,920 --> 00:13:53,590
Because then, why should they believe your
assurances?

229
00:13:53,590 --> 00:13:56,650
And third when you've got plenty of time
and it would be

230
00:13:56,650 --> 00:13:58,205
really easy to give the reasons

231
00:13:58,205 --> 00:14:01,068
straight-forwardly instead of simply
assuring them.

232
00:14:01,068 --> 00:14:01,556
So,

233
00:14:01,556 --> 00:14:06,410
you've got good and bad uses of
assurances.

234
00:14:06,410 --> 00:14:10,250
And I hope we understand what they are and
how to distinguish them, but

235
00:14:10,250 --> 00:14:14,039
let's do a few exercises just to make sure
that we've got the general idea.

