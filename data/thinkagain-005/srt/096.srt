1
00:00:02,820 --> 00:00:04,650
Now that we've seen which features

2
00:00:04,650 --> 00:00:07,150
distinguish inductive arguments from
deductive arguments,

3
00:00:07,150 --> 00:00:11,040
we want to look at the different kinds of
inductive arguments one by one.

4
00:00:11,040 --> 00:00:13,300
And the first two we're going to look at

5
00:00:13,300 --> 00:00:16,950
are moving up from a sample to a
generalization.

6
00:00:16,950 --> 00:00:20,220
And then moving back down from the
generalization

7
00:00:20,220 --> 00:00:23,420
to some kind of prediction in a particular
case.

8
00:00:23,420 --> 00:00:27,500
Generalizations are all around us.
Almost all movies have credits.

9
00:00:28,540 --> 00:00:33,530
Most popular bands have drummers.
Many restaurants are closed on Mondays.

10
00:00:34,530 --> 00:00:38,376
three quarters of Police officers are very
nice people.

11
00:00:38,376 --> 00:00:43,670
2 3rds of books have chapters in them.
Half of

12
00:00:43,670 --> 00:00:49,040
the people I know like to play sports.
And my favorite

13
00:00:49,040 --> 00:00:54,180
of all, 87.2% of statistics

14
00:00:54,180 --> 00:00:55,140
are made up on the spot.

15
00:00:56,550 --> 00:00:58,920
It should come as no surprise at all that
there's so

16
00:00:58,920 --> 00:01:01,280
many generalizations filling our lives

17
00:01:01,280 --> 00:01:04,570
because generalizations can be extremely
useful.

18
00:01:04,570 --> 00:01:07,400
Especially when you need to make a
decision.

19
00:01:07,400 --> 00:01:09,470
So, for example, if you're feeling a
little

20
00:01:09,470 --> 00:01:12,400
nauseous, that is a little sick at your
stomach,

21
00:01:12,400 --> 00:01:15,630
then you need to know whether most people

22
00:01:15,630 --> 00:01:19,340
who have symptoms of your sort Have
something serious

23
00:01:19,340 --> 00:01:22,010
enough that they need to go to a doctor
and they also need

24
00:01:22,010 --> 00:01:28,530
to know whether doctors can usually help
people who have symptoms of that sort.

25
00:01:28,530 --> 00:01:33,930
All of those generalizations are relevant
to whether or not you want to go

26
00:01:33,930 --> 00:01:37,140
to the doctor to see if you can get some
help with your sick stomach.

27
00:01:38,830 --> 00:01:44,570
But then the problem is how can we decide
which generalizations to believe.

28
00:01:44,570 --> 00:01:47,570
Since they're generalizations and they
plan all instances for

29
00:01:47,570 --> 00:01:50,270
a certain sort, you can't check them all
out.

30
00:01:50,270 --> 00:01:53,100
You have to take a sample of some sort.

31
00:01:53,100 --> 00:01:57,590
I mean just imagine that you're running a
bakery and you're making jelly doughnuts.

32
00:01:57,590 --> 00:02:00,590
And you want to know whether the jelly
doughnuts

33
00:02:00,590 --> 00:02:01,999
are filled with the right amount of jelly.

34
00:02:03,080 --> 00:02:06,280
You've made hundreds of jelly doughnuts,
you're not going to tear them all apart

35
00:02:06,280 --> 00:02:09,075
and check every one for how much jelly
they have in the middle.

36
00:02:09,075 --> 00:02:10,000
because then you'd have no

37
00:02:10,000 --> 00:02:16,640
doughnuts left to sell to your customers.
Or imagine that you want to buy a car.

38
00:02:16,640 --> 00:02:18,340
This time you're the customer.

39
00:02:18,340 --> 00:02:20,806
You want to buy a car and you need to know

40
00:02:20,806 --> 00:02:26,840
how often these cars have problems in the
first year.

41
00:02:26,840 --> 00:02:29,360
Because if they have a problems a lot
during the

42
00:02:29,360 --> 00:02:31,210
first year, you don't want to buy that
kind of car.

43
00:02:32,290 --> 00:02:35,070
But you don't want every car of that sort
to have been

44
00:02:35,070 --> 00:02:38,598
tested for the first year because then you
can't buy a new car.

45
00:02:38,598 --> 00:02:41,886
You're only going to have a used car since
they've all been used in the test.

46
00:02:41,886 --> 00:02:47,540
Or imagine that you want to know what
types

47
00:02:47,540 --> 00:02:51,180
of tree grew during a certain period of
history.

48
00:02:51,180 --> 00:02:54,730
So you look at the soil and you dig down
and you start looking for

49
00:02:54,730 --> 00:02:57,310
how many pollen grains there are, at

50
00:02:57,310 --> 00:02:59,579
a certain level that indicates a certain
age.

51
00:03:00,760 --> 00:03:03,420
Well, you can't check every spot in the
field because

52
00:03:03,420 --> 00:03:06,570
if you did that, you'd just be destroying
the entire field.

53
00:03:06,570 --> 00:03:11,740
So in these cases and many other cases,
you just have to take samples.

54
00:03:11,740 --> 00:03:13,880
You can't test the whole class.

55
00:03:13,880 --> 00:03:18,230
And then you have to generalize from the
sample to the larger class.

56
00:03:19,640 --> 00:03:22,980
All of these generalizations from samples
share a certain form.

57
00:03:24,510 --> 00:03:25,790
First we look at one instance

58
00:03:25,790 --> 00:03:26,970
of the sample.

59
00:03:26,970 --> 00:03:30,310
We say that first F is G as the thing

60
00:03:30,310 --> 00:03:34,250
that fits in the class F and has the
property G.

61
00:03:34,250 --> 00:03:35,280
Then we check another.

62
00:03:35,280 --> 00:03:39,920
The second F is also G and the third F is
also G and all the rest of the Fs

63
00:03:39,920 --> 00:03:44,358
in the sample are G.
So we conclude that all Fs are G.

64
00:03:45,490 --> 00:03:51,400
Now notice that all in the conclusion
means everything in the class

65
00:03:51,400 --> 00:03:52,210
of Fs.

66
00:03:52,210 --> 00:03:54,380
It doesn't only mean the ones in our
sample.

67
00:03:54,380 --> 00:03:57,510
So we've started from premises that are
only about the

68
00:03:57,510 --> 00:04:00,550
sample, which is only a part of the
general class.

69
00:04:00,550 --> 00:04:05,420
And we've reached a conclusion about the
whole class when we say that all Fs are G.

70
00:04:07,790 --> 00:04:10,410
Now other arguments of the same general
type are a

71
00:04:10,410 --> 00:04:14,930
little bit different because you don't
always get complete uniformity.

72
00:04:14,930 --> 00:04:18,340
You don't always get all Fs are Gs,
sometimes you get the first F

73
00:04:18,340 --> 00:04:22,830
is G, the second F is G but the third F is
not G.

74
00:04:22,830 --> 00:04:27,270
And the fourth F is G and the fifth F is G
but the sixth F is not G.

75
00:04:27,270 --> 00:04:32,303
So you get like 2 3rds of the Fs are G.
And then you reach the conclusion,

76
00:04:32,303 --> 00:04:36,560
that 2 3rds of all fFs are G.

77
00:04:36,560 --> 00:04:39,930
Again, though, you've only looked at the
f's in the sample.

78
00:04:39,930 --> 00:04:46,160
You've only observed a small part of that
total class of all Fs.

79
00:04:46,160 --> 00:04:49,350
And you draw a conclusion that 2 3rds of
overall

80
00:04:49,350 --> 00:04:54,540
class, the whole class of Fs, has that
property G.

81
00:04:54,540 --> 00:04:57,220
So that's why you have a general, that's
why it's called a generalization.

82
00:04:57,220 --> 00:05:02,550
You start from a smaller sample and reach
a conclusion about a much larger class.

83
00:05:02,550 --> 00:05:04,569
You generalize to the whole class.

84
00:05:05,640 --> 00:05:08,450
The fact that the argument moves from a
small part

85
00:05:08,450 --> 00:05:12,320
of the class to the whole class shows that
it's inductive.

86
00:05:12,320 --> 00:05:13,390
And what does that mean?

87
00:05:13,390 --> 00:05:17,436
Well, first is the argument valid?
Think back to one of the examples.

88
00:05:17,436 --> 00:05:22,680
Almost all bands that I know of have
drummers.

89
00:05:22,680 --> 00:05:26,240
Therefore, almost all bands have drummers.

90
00:05:26,240 --> 00:05:30,340
Well, is it possible that the premises are
true and the conclusion false?

91
00:05:30,340 --> 00:05:31,630
Of course, it is.

92
00:05:31,630 --> 00:05:35,330
Because maybe I only listen to bands that
have drummers but there are a lot of

93
00:05:35,330 --> 00:05:37,140
bands out there that don't have drummers
and

94
00:05:37,140 --> 00:05:38,800
I just didn't happen to listen to them.

95
00:05:38,800 --> 00:05:42,920
That is, they weren't part of my sample.
Okay?

96
00:05:44,280 --> 00:05:47,720
And is the argument defeasible?
That means that you can

97
00:05:47,720 --> 00:05:50,590
add further information to the premises
and

98
00:05:50,590 --> 00:05:53,490
it'll make the argument much less strong.

99
00:05:53,490 --> 00:05:56,390
It might even undermine the argument
totally.

100
00:05:56,390 --> 00:05:59,920
And, of course, it could because you could
give me all kinds of examples

101
00:05:59,920 --> 00:06:03,960
of bands that don't have drummers and I
would have to change my conclusion.

102
00:06:03,960 --> 00:06:05,660
That almost all bands have drummers.

103
00:06:07,670 --> 00:06:13,350
So, this argument is defeasible.
Does that mean that it can't be strong?

104
00:06:13,350 --> 00:06:13,750
No.

105
00:06:13,750 --> 00:06:14,810
It could still be strong.

106
00:06:14,810 --> 00:06:17,680
It can come in different types of
strength.

107
00:06:17,680 --> 00:06:21,490
How would you get a stronger argument or a
weaker argument in this case?

108
00:06:21,490 --> 00:06:24,020
Well, you could have a larger or smaller
sample.

109
00:06:24,020 --> 00:06:26,960
If I take a very large sample, the
argument's going to be stronger.

110
00:06:26,960 --> 00:06:30,420
If I take a very small sample, the
argument's going to be weaker.

111
00:06:30,420 --> 00:06:32,940
So it's not like validity, which is on or
off.

112
00:06:32,940 --> 00:06:34,790
It's either valid or not.

113
00:06:34,790 --> 00:06:37,440
Instead, the strength of the argument
comes in

114
00:06:37,440 --> 00:06:40,260
degrees depending on how big the sample
is.

115
00:06:42,020 --> 00:06:44,690
Now, does that mean that since it can't be
valid it

116
00:06:44,690 --> 00:06:48,410
can only be strong to a certain degree,
that it's not good?

117
00:06:48,410 --> 00:06:51,500
No.
Because it's an inductive argument.

118
00:06:51,500 --> 00:06:53,920
Inductive arguments don't even try to be
valid.

119
00:06:53,920 --> 00:06:56,010
They don't even pretend to valid.

120
00:06:56,010 --> 00:06:58,050
That's not what they're supposed to be.
And so you

121
00:06:58,050 --> 00:07:01,480
can't criticize this argument by saying
it's not valid.

122
00:07:01,480 --> 00:07:06,100
Is doing everything that it's supposed to
do, if it provides you

123
00:07:06,100 --> 00:07:12,070
with a strong reason and a strong enough
reason for the conclusion.

124
00:07:12,070 --> 00:07:15,020
That's what an inductive argument is
supposed to do.

125
00:07:15,020 --> 00:07:17,846
So, if this argument does it, it's a good
argument.

126
00:07:17,846 --> 00:07:23,150
And even if inductive arguments including
generalizations

127
00:07:23,150 --> 00:07:27,100
from samples don't have to be valid they
still need to be strong.

128
00:07:27,100 --> 00:07:30,500
And so we need to figure out how to tell

129
00:07:30,500 --> 00:07:34,520
when a generalization from a sample is a
strong argument.

130
00:07:34,520 --> 00:07:37,270
When it provides a strong reason for the
conclusion.

131
00:07:37,270 --> 00:07:40,690
And to really answer that question, you

132
00:07:40,690 --> 00:07:44,380
gotta turn to statistics, an area of
mathematics.

133
00:07:44,380 --> 00:07:46,170
And learn how to analyze the data much

134
00:07:46,170 --> 00:07:48,050
more carefully than we'll be able to do
here.

135
00:07:49,550 --> 00:07:53,310
But even mentioning the name statistics
raises fear in some people.

136
00:07:54,590 --> 00:07:58,220
Mark Twain is famous for having said there
are three kinds of lies.

137
00:07:58,220 --> 00:08:02,080
There are lies There are damn lies and
there are statistics.

138
00:08:03,470 --> 00:08:06,620
He actually gives credit to Disraeli for
having said it first.

139
00:08:06,620 --> 00:08:11,770
But the point is that statistics are even
worse than damn lies.

140
00:08:11,770 --> 00:08:12,860
You can't trust them at all.

141
00:08:13,980 --> 00:08:15,730
Or so Mark Twain says.

142
00:08:15,730 --> 00:08:18,390
But on the other hand, some people say,
statistics?

143
00:08:18,390 --> 00:08:19,260
That's math!

144
00:08:19,260 --> 00:08:20,890
It's all about numbers!

145
00:08:20,890 --> 00:08:21,910
Can't question that.

146
00:08:23,350 --> 00:08:27,290
Here's one example of somebody who
suggests such a position.

147
00:08:28,760 --> 00:08:34,270
>> There's still one thing that's
irrefutable and that is the numbers.

148
00:08:34,270 --> 00:08:37,060
Numbers don't lie.

149
00:08:37,060 --> 00:08:41,248
You can't parse poll numbers.
They're infallible.

150
00:08:41,248 --> 00:08:42,060
>> Okay fine.

151
00:08:42,060 --> 00:08:44,160
He knows that statistics aren't
infallible.

152
00:08:44,160 --> 00:08:45,850
He's just being sarcastic.

153
00:08:45,850 --> 00:08:50,500
But there are many people who put a lot of
faith in the numbers and in statistics.

154
00:08:50,500 --> 00:08:53,560
They seem to think that you always have to

155
00:08:53,560 --> 00:08:55,900
trust it when a statistician comes up with
an answer.

156
00:08:57,260 --> 00:09:00,600
And we're going to learn that neither of
these views is correct.

157
00:09:00,600 --> 00:09:04,210
It's not true the statistics were always
worse than damn lies.

158
00:09:04,210 --> 00:09:06,310
And it's also not true that the numbers

159
00:09:06,310 --> 00:09:09,420
are irrefutable.
The truth lies somewhere in the middle.

