1
00:00:04,290 --> 00:00:08,430
In the last lecture, we saw how you can
combine conjunction

2
00:00:08,430 --> 00:00:13,690
and disjunction to build up even larger
propositions from propositional units.

3
00:00:15,110 --> 00:00:16,360
In this lecture, we're going to talk

4
00:00:16,360 --> 00:00:20,709
about two properties, commutativity and
associativity.

5
00:00:21,760 --> 00:00:26,700
Properties that we find in conjunction,
and in disjunction,

6
00:00:26,700 --> 00:00:29,200
but that we don't find in other truth
functions,

7
00:00:29,200 --> 00:00:31,410
like the conditional, which we'll study
later this week.

8
00:00:32,980 --> 00:00:35,110
Okay, so what are commutativity, and the
associativity?

9
00:00:35,110 --> 00:00:40,170
Well let me start, by giving a definition
of commutativity.

10
00:00:41,720 --> 00:00:47,980
A function of two things is commutative,
it has commutativity,

11
00:00:47,980 --> 00:00:53,090
when it delivers the same result no matter
what order it operates on those things.

12
00:00:54,580 --> 00:00:58,700
Okay, now that's a very abstract
characterization, and that's a

13
00:00:58,700 --> 00:01:02,650
characterization that can apply to
functions in all different domains.

14
00:01:02,650 --> 00:01:07,990
For instance, in arithmetic, there are
functions like addition.

15
00:01:07,990 --> 00:01:11,710
And addition is an example of a
commutative function.

16
00:01:11,710 --> 00:01:16,260
Right, addition is a function, that can
operate on two inputs.

17
00:01:16,260 --> 00:01:17,990
You can add two numbers together.

18
00:01:19,250 --> 00:01:20,090
And the result

19
00:01:20,090 --> 00:01:25,030
that you get when you add two numbers
together doesn't depend on the order in

20
00:01:25,030 --> 00:01:31,104
which you add the numbers.
For example, 2 plus 3 is the same as 3

21
00:01:31,104 --> 00:01:37,592
plus 2.
And in general, for any two numbers, x and

22
00:01:37,592 --> 00:01:44,374
y, x plus y is the same as y plus x.
So addition is commutative.

23
00:01:44,374 --> 00:01:51,050
Addition is an example of a function that
has commutativity.

24
00:01:51,050 --> 00:01:55,530
Another example of a arithmetical function
that's commutative is multiplication.

25
00:01:57,440 --> 00:02:05,158
For example, four multiplied by eight is
the same as eight multiplied by four.

26
00:02:05,158 --> 00:02:09,604
More generally, for any two numbers x and
y,

27
00:02:09,604 --> 00:02:14,460
x multiplied by y is the same as y
multiplied by x.

28
00:02:15,460 --> 00:02:19,480
So multiplication is a commutative
function.

29
00:02:19,480 --> 00:02:20,760
It's a function that has commutativity.

30
00:02:21,920 --> 00:02:25,580
But notice, not all arithmetical functions
are commutative.

31
00:02:25,580 --> 00:02:30,786
For instance, subtraction is not
commutative, right?

32
00:02:30,786 --> 00:02:34,890
Seven minus three is not the same as three
minus seven.

33
00:02:34,890 --> 00:02:41,298
And more generally, for most numbers x and

34
00:02:41,298 --> 00:02:47,220
y, x minus y is not the same as y minus x.

35
00:02:47,220 --> 00:02:50,630
That's not true for every assignment of
numbers

36
00:02:50,630 --> 00:02:53,360
to x and y, but it's true for most.

37
00:02:53,360 --> 00:03:00,600
Generally we can't expect x minus y to be
equal to y minus x, so subtraction is not

38
00:03:00,600 --> 00:03:04,420
a commutative function.
Neither is division.

39
00:03:04,420 --> 00:03:06,400
So addition and multiplication are
commutative,

40
00:03:06,400 --> 00:03:09,420
but subtraction and division are not
commutative.

41
00:03:11,550 --> 00:03:14,210
Now that same points can be made

42
00:03:14,210 --> 00:03:18,590
about truth functional connectives in
propositional logic.

43
00:03:18,590 --> 00:03:23,440
For instance, the truth functional
connective conjunction is commutative.

44
00:03:24,730 --> 00:03:28,130
Right, consider the proposition "I'm
standing and waving".

45
00:03:28,130 --> 00:03:33,780
That's the conjunction of two propositions
"I'm standing" and "I'm waving".

46
00:03:34,790 --> 00:03:36,800
"I'm standing and waving" is the
conjunction

47
00:03:36,800 --> 00:03:38,470
of those two propositions.

48
00:03:38,470 --> 00:03:42,940
Well, that's the same proposition as "I'm
waving and standing".

49
00:03:44,280 --> 00:03:47,070
Right, there just two different ways of
saying the same thing.

50
00:03:47,070 --> 00:03:50,650
Maybe one is a more elegant way of saying
it than the other.

51
00:03:50,650 --> 00:03:54,100
But they're saying the same thing, what
they're saying is the same.

52
00:03:55,750 --> 00:04:01,900
In general, the conjunction of any two
propositions p and q is exactly

53
00:04:01,900 --> 00:04:07,000
the same as the conjunction of the
propositions q and p.

54
00:04:07,000 --> 00:04:09,940
Conjunction operates on two propositions
and it doesn't

55
00:04:09,940 --> 00:04:12,530
matter what order you put the propositions
in.

56
00:04:12,530 --> 00:04:18,320
P conjunction q is the same proposition as
q conjunction p.

57
00:04:20,150 --> 00:04:24,990
So conjunction is a commutative function,
a commutative truth function.

58
00:04:26,550 --> 00:04:27,030
The same is

59
00:04:27,030 --> 00:04:30,710
true of disjunction, disjunction is also a
commutative truth function.

60
00:04:31,710 --> 00:04:34,318
So consider the proposition "I'm standing
or waving".

61
00:04:34,318 --> 00:04:36,730
That's a disjunction.

62
00:04:36,730 --> 00:04:40,310
But it's the same proposition as "I'm
waving or standing".

63
00:04:41,420 --> 00:04:46,950
In general for any two propositions p q
the disjunction

64
00:04:46,950 --> 00:04:52,270
p or q is the same proposition as the
disjunction q or p.

65
00:04:53,380 --> 00:04:58,030
So, disjunction is a commutative truth
function, like conjunction.

66
00:04:59,610 --> 00:05:01,820
But later on this week, we'll learn about

67
00:05:01,820 --> 00:05:04,220
another truth function that we'll call the
conditional.

68
00:05:04,220 --> 00:05:06,590
Sometimes, it's called the material
conditional.

69
00:05:08,260 --> 00:05:12,340
But the conditional is not a commutative
truth function.

70
00:05:12,340 --> 00:05:19,540
With a conditional the order in which it
operates on propositions determines

71
00:05:19,540 --> 00:05:20,610
the result that you get.

72
00:05:20,610 --> 00:05:24,299
And we'll see that later this week when we
study the conditional.

73
00:05:26,180 --> 00:05:30,980
Okay, so that's commutativity.
Now what about associativity?

74
00:05:30,980 --> 00:05:34,850
Well, what's associativity?
Here's a definition.

75
00:05:34,850 --> 00:05:39,420
A function of three or more things is
associative when it delivers

76
00:05:39,420 --> 00:05:43,249
the same result no matter what order it
operates on those things.

77
00:05:45,970 --> 00:05:52,760
Okay, so let's look at some examples of
associative functions

78
00:05:52,760 --> 00:05:56,980
and of functions that are not associative.
First we'll begin with arithmetic.

79
00:05:58,750 --> 00:06:00,300
So just as addition is a

80
00:06:00,300 --> 00:06:04,010
commutative function, it's also an
associative function.

81
00:06:04,010 --> 00:06:07,630
To illustrate, consider a case where
addition

82
00:06:07,630 --> 00:06:10,880
applies to three things, instead of just
two.

83
00:06:11,920 --> 00:06:18,860
So consider, adding two to three plus six.

84
00:06:18,860 --> 00:06:24,688
Well, the sum of two and three plus six,
notice that's

85
00:06:24,688 --> 00:06:30,435
going to be the same as the sum of two
plus three and six.

86
00:06:30,435 --> 00:06:33,532
Right, both of them, are going to add up
to eleven.

87
00:06:33,532 --> 00:06:37,228
And more generally, for any

88
00:06:37,228 --> 00:06:43,234
three numbers, x, y, and z, the sum of x
and y

89
00:06:43,234 --> 00:06:49,599
plus z is going to be equal to the sum of
x plus y and z.

90
00:06:49,599 --> 00:06:55,340
It doesn't matter whether we group the y
and z together and then add that to

91
00:06:55,340 --> 00:07:00,040
the x, or whether we group the x and y
together and add that to the z.

92
00:07:00,040 --> 00:07:01,490
Either way, we get the same result.

93
00:07:02,830 --> 00:07:06,660
So addition is an associative function.
It's

94
00:07:06,660 --> 00:07:10,200
an example of an arithmetical function
that has associativity.

95
00:07:11,240 --> 00:07:17,135
The same is true for multiplication.
Notice if we multiply two by the product

96
00:07:17,135 --> 00:07:22,700
of three and six we get the same number as
we get if we multiply the

97
00:07:22,700 --> 00:07:28,037
product of two and three by six.
Either way,

98
00:07:28,037 --> 00:07:33,480
we get thirty six.
So, in

99
00:07:33,480 --> 00:07:39,820
general, for any three numbers, x, y, and
z, the product of x and

100
00:07:39,820 --> 00:07:45,470
y times z is the same as the product of x
times y and z.

101
00:07:47,500 --> 00:07:52,110
So, that explains why multiplication is
associative.

102
00:07:52,110 --> 00:07:56,000
It's an example of an arithmetical
function that has associativity.

103
00:07:56,000 --> 00:07:58,970
But not all arithmetical functions are
associative.

104
00:07:58,970 --> 00:08:02,860
In particular, subtraction is not
associative.

105
00:08:02,860 --> 00:08:04,796
Take any three numbers, x, y, and z.

106
00:08:04,796 --> 00:08:13,328
Now, start by subtracting z from y, and
then subtract the result of that from x.

107
00:08:13,328 --> 00:08:17,550
You're going to get a different number, in
general, that if you start

108
00:08:17,550 --> 00:08:22,410
by subtracting y from x, and then subtract
z from that number.

109
00:08:22,410 --> 00:08:29,181
Right, so x minus (y minus z) is in
general

110
00:08:29,181 --> 00:08:34,870
not the same as (x minus y) minus z.

111
00:08:34,870 --> 00:08:39,079
So subtraction is not associative, and
neither for that matter is division.

112
00:08:40,480 --> 00:08:44,920
So, addition and multiplication are
associative just as they're commutative.

113
00:08:44,920 --> 00:08:46,710
Subtraction and division are not

114
00:08:46,710 --> 00:08:48,600
associative, just as they're not
commutative.

115
00:08:49,930 --> 00:08:55,320
Analogously with truth functions, we can
see that conjunction is associative.

116
00:08:55,320 --> 00:09:01,090
For example, consider the proposition "I'm
standing and he's sitting and waving".

117
00:09:01,090 --> 00:09:05,430
Well, that's the same as the proposition
"I'm standing and he's sitting,

118
00:09:05,430 --> 00:09:10,530
and he's also waving".
In general for any three propositions, p,

119
00:09:10,530 --> 00:09:15,920
q and r, the conjunction of p with q and r
is going to be the

120
00:09:15,920 --> 00:09:21,360
same proposition as the conjunction p and
q with r.

121
00:09:23,880 --> 00:09:27,480
Now, just as conjunction is associative,
so too is disjunction.

122
00:09:28,600 --> 00:09:32,260
We can see an example.
For instance if I say "I am standing or

123
00:09:32,260 --> 00:09:37,690
he's sitting or waving" well that's the
same proposition as "I'm

124
00:09:37,690 --> 00:09:43,210
standing or he's sitting, or he's waving".
In general,

125
00:09:43,210 --> 00:09:49,480
for any three propositions p, q, and r,
the disjunction of p with

126
00:09:49,480 --> 00:09:55,140
q or r, is going to be the same
proposition as the

127
00:09:55,140 --> 00:10:00,474
disjunction of p or q with r.

128
00:10:00,474 --> 00:10:05,080
So disjunction, like conjunction, is
associative.

129
00:10:05,080 --> 00:10:07,390
But again not all truth functions are
associative.

130
00:10:07,390 --> 00:10:08,850
Later this week we're going to learn about
a

131
00:10:08,850 --> 00:10:13,240
truth function called the conditional that
is not associative.

132
00:10:13,240 --> 00:10:14,660
When the conditional operates on

133
00:10:14,660 --> 00:10:17,760
three or more inputs, it matters very much

134
00:10:17,760 --> 00:10:20,260
the order in which it operates on those
inputs.

135
00:10:20,260 --> 00:10:25,540
Because depending on the order you end up
with a different propositional result.

136
00:10:25,540 --> 00:10:28,860
Okay, so that's enough about communitivity
and associativity.

137
00:10:28,860 --> 00:10:30,790
See you next time.

