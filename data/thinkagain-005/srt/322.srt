1
00:00:03,780 --> 00:00:05,604
In the last few lectures we've talked

2
00:00:05,604 --> 00:00:09,164
about the truth-functional connectives,
conjunction and disjunction.

3
00:00:09,164 --> 00:00:13,030
We've gone over the truth tables for those
connectives and shown how we can use

4
00:00:13,030 --> 00:00:16,510
the truth tables to figure out when

5
00:00:16,510 --> 00:00:22,150
arguments that use conjunction or
disjunction, are valid.

6
00:00:22,150 --> 00:00:23,970
Now, today what I want to do is talk

7
00:00:23,970 --> 00:00:28,980
about a propositional device that's not a
connective,

8
00:00:28,980 --> 00:00:31,196
but that's what I'll call an operator.

9
00:00:31,196 --> 00:00:36,510
It doesn't connect two propositions to
make a larger proposition.

10
00:00:36,510 --> 00:00:38,840
Rather, what it does is it just operates

11
00:00:38,840 --> 00:00:42,020
on a single proposition to make another
proposition.

12
00:00:42,020 --> 00:00:47,810
You can think of it as converting one
proposition to another proposition.

13
00:00:47,810 --> 00:00:49,990
now there are lots of propositional
operators.

14
00:00:49,990 --> 00:00:53,318
For, for instance, consider the phrase, I
believe that.

15
00:00:53,318 --> 00:00:56,030
That's a propositional operator.

16
00:00:56,030 --> 00:01:02,070
You take any proposition, let's say the
proposition, it's raining today.

17
00:01:03,140 --> 00:01:07,570
And now you put I believe that, in front
of it, and you get a new proposition.

18
00:01:07,570 --> 00:01:09,460
I believe that it's raining.

19
00:01:11,450 --> 00:01:15,908
So, I believe that, is an example of a
propositional operator.

20
00:01:15,908 --> 00:01:19,490
But I want to talk about a kind of
propositional operator.

21
00:01:19,490 --> 00:01:22,870
That is a truth functional operator,
that's to say

22
00:01:22,870 --> 00:01:26,780
the truth or falsity of the proposition
that results from

23
00:01:26,780 --> 00:01:30,960
applying that operator, depends solely
upon the truth or

24
00:01:30,960 --> 00:01:34,510
falsity of the proposition to which the
operator is applied.

25
00:01:34,510 --> 00:01:40,089
Now the propositional operator, I believe
that, is clearly not truth functional.

26
00:01:41,210 --> 00:01:44,620
For instance, suppose it is raining today.
Suppose it's true

27
00:01:44,620 --> 00:01:46,240
that it's raining today.

28
00:01:46,240 --> 00:01:49,910
What does that tell you about whether or
not I believe that it's raining today?

29
00:01:51,160 --> 00:01:52,560
Well, nothing.

30
00:01:52,560 --> 00:01:55,720
It could be raining today even though I
don't believe that it's raining today.

31
00:01:55,720 --> 00:01:59,730
It could be raining today even though I do
believe that it's raining today.

32
00:01:59,730 --> 00:02:04,160
The truth or falsity of the proposition I
believe that it's raining today.

33
00:02:04,160 --> 00:02:06,900
Doesn't depend solely upon the truth or

34
00:02:06,900 --> 00:02:09,490
falsity of the proposition, it is raining
today.

35
00:02:10,610 --> 00:02:12,130
It depends on other things as well.

36
00:02:12,130 --> 00:02:14,616
It depends in particular, on my
psychology.

37
00:02:14,616 --> 00:02:20,410
So, the propositional operator, I believe
that, is not a truth functional

38
00:02:20,410 --> 00:02:26,270
operator, but there is a propositional
operator that's truth functional.

39
00:02:26,270 --> 00:02:30,710
In English, the propositional operator
that's truth functional, is expressed

40
00:02:30,710 --> 00:02:35,000
by the phrase, it is not the case that, or
sometimes,

41
00:02:35,000 --> 00:02:37,660
simply by the use of the phrase, not.

42
00:02:40,100 --> 00:02:42,960
The propositional operator that I have in
mind is called negation.

43
00:02:42,960 --> 00:02:44,900
Negation is a truth functional operator.

44
00:02:44,900 --> 00:02:47,320
It takes any proposition.

45
00:02:47,320 --> 00:02:50,000
And creates a new proposition whose truth
value is

46
00:02:50,000 --> 00:02:53,560
the opposite of the truth value of the
original proposition.

47
00:02:53,560 --> 00:02:55,820
So you take any proposition, if it's true,

48
00:02:55,820 --> 00:02:58,170
then the negation of that proposition is
false.

49
00:02:59,890 --> 00:03:04,710
Take any proposition, if it's false, then
the negation of that proposition is true.

50
00:03:04,710 --> 00:03:05,830
So negation

51
00:03:05,830 --> 00:03:09,800
is a propositional operator that's an
example of a truth functional operator.

52
00:03:09,800 --> 00:03:14,380
The truth of the proposition that results
from applying negation depends

53
00:03:14,380 --> 00:03:18,105
solely on the truth of the proposition to
which negation is applied.

54
00:03:18,105 --> 00:03:20,430
In that respect, negation is very
different

55
00:03:20,430 --> 00:03:22,950
from the propositional operator, I believe
that.

56
00:03:24,480 --> 00:03:27,350
Now, I said just a moment ago that
sometimes the English

57
00:03:27,350 --> 00:03:31,110
word not is used to express the truth
functional operator negation.

58
00:03:32,610 --> 00:03:36,418
But it's not always used to do that.
Let me give you an example.

59
00:03:36,418 --> 00:03:41,915
Consider the proposition, Walter has
stopped beating his dogs.

60
00:03:41,915 --> 00:03:44,270
Okay.

61
00:03:44,270 --> 00:03:47,170
Now that's a proposition and something
that could be true or false and

62
00:03:47,170 --> 00:03:49,740
it's something that could be the premise
or the conclusion of an argument.

63
00:03:51,570 --> 00:03:55,100
But suppose we put the word not into that
proposition.

64
00:03:55,100 --> 00:03:58,129
I say, Walter has not stopped beating his
dogs.

65
00:03:59,450 --> 00:04:03,060
Is that the negation of Walter has stopped
beating his dogs?

66
00:04:04,530 --> 00:04:07,530
Well, let's think about that for a moment.

67
00:04:07,530 --> 00:04:12,230
Suppose it's true that Walter has stopped
beating his dogs.

68
00:04:12,230 --> 00:04:14,180
Well, then pretty clearly it seems like
it's

69
00:04:14,180 --> 00:04:17,180
false that Walter has not stopped beating
his dogs.

70
00:04:17,180 --> 00:04:20,350
So if he has stopped beating his dogs,
then anyone who

71
00:04:20,350 --> 00:04:23,800
says that he's not stopped beating his
dogs is saying something false.

72
00:04:25,830 --> 00:04:30,280
But now, suppose it's false that Walter
has stopped beating his dogs.

73
00:04:30,280 --> 00:04:33,728
Does that mean it's true that Walter has
not stopped beating his dogs?

74
00:04:33,728 --> 00:04:38,600
Well, not necessarily.

75
00:04:38,600 --> 00:04:40,820
Think about the different reasons why it
could

76
00:04:40,820 --> 00:04:44,070
be false that Walter has stopped beating
his dogs.

77
00:04:44,070 --> 00:04:45,640
It could be false that Walter has stopped

78
00:04:45,640 --> 00:04:48,240
beating his dogs because he's still
beating his dogs.

79
00:04:49,300 --> 00:04:51,050
Or, it could be false that Walter

80
00:04:51,050 --> 00:04:56,130
has stopped beating his dogs because, in
fact, he never did beat his dogs.

81
00:04:56,130 --> 00:04:57,750
Or, it could be false that Walter has
stopped

82
00:04:57,750 --> 00:05:01,180
beating his dogs because Walter doesn't
have any dogs.

83
00:05:02,410 --> 00:05:03,780
For that matter, it could be false that

84
00:05:03,780 --> 00:05:06,770
Walter has stopped beating his dogs
because there

85
00:05:06,770 --> 00:05:08,890
is no such person as Walter, Walter
doesn't

86
00:05:08,890 --> 00:05:11,307
exist, Walter is a mere figment of your
imagination.

87
00:05:12,710 --> 00:05:16,070
So there are all sorts of reasons why it
could

88
00:05:16,070 --> 00:05:21,210
be false that Walter has stopped beating
his dogs.

89
00:05:21,210 --> 00:05:25,380
And in some of those situations it would
not be true.

90
00:05:25,380 --> 00:05:27,850
That Walter has not stopped beating his
dogs, for

91
00:05:27,850 --> 00:05:31,820
instance if Walter doesn't have any dogs
then even though

92
00:05:31,820 --> 00:05:34,130
it's false that Walter has stopped beating
his dogs it's

93
00:05:34,130 --> 00:05:36,850
also false that Walter has not stopped
beating his dogs.

94
00:05:39,430 --> 00:05:45,860
So, in that example, the word not, is not
being used as negation.

95
00:05:47,340 --> 00:05:51,274
So suppose I say, Walter has stopped
beating his dogs.

96
00:05:52,280 --> 00:05:53,020
Not.

97
00:05:54,210 --> 00:06:00,440
Okay, not there where I say, not, all I'm
doing

98
00:06:00,440 --> 00:06:05,180
is operating on the original proposition,
Walter has stopped beating his dogs, and

99
00:06:05,180 --> 00:06:07,310
creating a new proposition.

100
00:06:07,310 --> 00:06:10,830
Whose truth value is the opposite of that
original proposition.

101
00:06:10,830 --> 00:06:15,700
So if I say Walter has stopped beating his
dogs, but Walter doesn't

102
00:06:15,700 --> 00:06:19,770
exist, or Walter never had any dogs, or
Walter never did beat his dogs.

103
00:06:19,770 --> 00:06:23,680
In any of those situations, if I say
Walter has stopped beating his dogs,

104
00:06:25,030 --> 00:06:30,240
not.
What I just said is true, so sometimes the

105
00:06:30,240 --> 00:06:35,485
word not is used to express the truth
functional operator, negation.

106
00:06:35,485 --> 00:06:40,100
But often it's not used that way.

107
00:06:40,100 --> 00:06:44,670
Often when we want to express the truth
functional operator negation we have

108
00:06:44,670 --> 00:06:48,740
to use the more cumbersome English phrase
it is not the case that.

109
00:06:49,850 --> 00:06:51,690
Now I've told you how negation works.

110
00:06:51,690 --> 00:06:55,485
It operates on a particular proposition to
create new proposition

111
00:06:55,485 --> 00:06:59,940
whose truth value is the opposite of the
truth value of the original proposition.

112
00:07:01,360 --> 00:07:03,290
But because negation works in such a
simple

113
00:07:03,290 --> 00:07:07,078
way, it's, it's truth-table is going to be
very simple.

114
00:07:07,078 --> 00:07:10,790
So consider any proposition, no matter
what, any proposition.

115
00:07:10,790 --> 00:07:11,900
Call it p.

116
00:07:11,900 --> 00:07:15,820
When p is true the negation of p is
going to be false.

117
00:07:15,820 --> 00:07:19,130
And when p is false the negation of p is
going to be true.

118
00:07:20,470 --> 00:07:23,345
That's how negation works.

