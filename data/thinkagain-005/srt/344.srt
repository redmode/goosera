1
00:00:03,410 --> 00:00:08,440
In the last few lectures, we've seen how
we can use Venn diagrams

2
00:00:08,440 --> 00:00:13,690
to predict that and to show why, explain
why, certain

3
00:00:13,690 --> 00:00:18,559
immediate categorical inferences and
certain syllogism are valid or invalid.

4
00:00:20,430 --> 00:00:21,460
Now I'd like to make a point

5
00:00:21,460 --> 00:00:27,880
today about categorical logic, about
categories in general.

6
00:00:27,880 --> 00:00:29,230
It might seem

7
00:00:29,230 --> 00:00:33,720
that the application of the method of Venn
diagrams is really quite

8
00:00:33,720 --> 00:00:38,930
limited because a lot of our statements
aren't really about categories at all.

9
00:00:38,930 --> 00:00:41,290
They're about individuals, not categories.

10
00:00:43,580 --> 00:00:49,620
I want to say that this is an example of
how ordinary language can mislead us.

11
00:00:50,660 --> 00:00:53,510
If we think about precisely what it is
that we're

12
00:00:53,510 --> 00:00:56,840
saying in a lot of the ordinary statements
that appear

13
00:00:56,840 --> 00:01:00,680
to be about individuals We'll see that, in
fact, those

14
00:01:00,680 --> 00:01:05,810
ordinary statements really are about
categories, not just about individuals.

15
00:01:05,810 --> 00:01:07,910
Let me give you an example to illustrate
this point.

16
00:01:10,130 --> 00:01:13,560
Consider the statement, Mary owns a
Ferrari.

17
00:01:13,560 --> 00:01:17,650
Now, that statement appears to be just
about individuals.

18
00:01:17,650 --> 00:01:19,820
It doesn't appear to involve any
categories, right?

19
00:01:19,820 --> 00:01:24,550
There's Mary, who let's suppose is a
particular person

20
00:01:24,550 --> 00:01:26,970
that I'm talking about, like a coworker of
mine.

21
00:01:28,420 --> 00:01:30,660
not that any of my coworkers would own a
Ferrari.

22
00:01:30,660 --> 00:01:35,220
But suppose that Mary is a particular
person, and we're talking about

23
00:01:35,220 --> 00:01:38,570
Mary's Ferrari, which is a particular car.

24
00:01:38,570 --> 00:01:43,450
And it seems that that statement is just
claiming that the person, the

25
00:01:43,450 --> 00:01:49,390
particular person I'm talking about, owns
that particular car.

26
00:01:49,390 --> 00:01:49,590
Right?

27
00:01:49,590 --> 00:01:51,660
There don't seem to be any categories
involved at all.

28
00:01:54,130 --> 00:01:54,890
But wait a second.

29
00:01:57,220 --> 00:01:59,490
Compare the statement, Mary owns a
Ferrari, to

30
00:01:59,490 --> 00:02:04,700
the statement, some of Mary's possessions
are Ferrari cars.

31
00:02:07,000 --> 00:02:12,350
Now that second statement, notice, is of
the I form.

32
00:02:12,350 --> 00:02:15,980
It's of the form, some f's are Gs.

33
00:02:15,980 --> 00:02:21,180
You're saying some things that fall into
one category, the category of Mary's

34
00:02:21,180 --> 00:02:28,230
possessions also fall into a second
category, the category of Ferrari cars.

35
00:02:30,370 --> 00:02:30,870
Now,

36
00:02:33,130 --> 00:02:37,960
these two statements seem to amount to the
same thing.

37
00:02:37,960 --> 00:02:43,470
When I say, Mary owns a Ferrari, what I'm
saying is true, if,

38
00:02:43,470 --> 00:02:49,380
and only if, some of Mary's possessions
are Ferrari cars.

39
00:02:49,380 --> 00:02:52,560
The information carried by one of those
statements seems to

40
00:02:52,560 --> 00:02:55,610
be the same as the information carried by
the other.

41
00:02:58,070 --> 00:03:02,850
So here's a case where to look at the
language we were using when we

42
00:03:02,850 --> 00:03:05,760
said Mary owns a Ferrari, you would've
thought,

43
00:03:05,760 --> 00:03:08,220
well we're not talking about any
categories there.

44
00:03:08,220 --> 00:03:11,040
So the method of Venn diagrams and all of

45
00:03:11,040 --> 00:03:14,730
this stuff about categorical logic is
completely irrelevant to any

46
00:03:14,730 --> 00:03:18,700
argument we might make that uses the
statement Mary

47
00:03:18,700 --> 00:03:21,630
owns a Ferrari, either as a premise or a
conclusion,

48
00:03:23,080 --> 00:03:24,310
but in fact, that's not true.

49
00:03:25,360 --> 00:03:27,060
In fact, the statement that Mary owns a

50
00:03:27,060 --> 00:03:31,370
Ferrari is equivalent to the statement
some of

51
00:03:31,370 --> 00:03:34,380
Mary's possessions are Ferrari cars, which
is a

52
00:03:34,380 --> 00:03:37,070
statement of the I form: some Fs are Gs.

53
00:03:37,070 --> 00:03:42,680
And so, the categorical logic that we've
been developing

54
00:03:42,680 --> 00:03:48,100
this week and the method of Van Diagrams,
actually can be used to study the validity

55
00:03:48,100 --> 00:03:52,470
of arguments where the statement, Mary
owns a Ferrari, is

56
00:03:52,470 --> 00:03:55,240
included as one of the premises or as the
conclusion.

57
00:03:56,690 --> 00:04:00,120
Because the statement Mary owns a Ferrari
is just equivalent to a

58
00:04:00,120 --> 00:04:04,530
statement of the i form, some of Mary's
possessions are Ferrari cars.

59
00:04:04,530 --> 00:04:07,580
I want to say this phenomenon is very
general.

60
00:04:07,580 --> 00:04:13,460
Not every statement is of the a, e, i, or
o forms, but lots

61
00:04:13,460 --> 00:04:18,160
of lots of ordinary statements that we
make are of one of those forms.

62
00:04:21,880 --> 00:04:25,900
Many more than ordinary language would
suggest.

63
00:04:25,900 --> 00:04:30,320
So with that in mind, I want now to show
how

64
00:04:30,320 --> 00:04:34,450
we can apply the categorical logic, how we
can apply the

65
00:04:34,450 --> 00:04:38,450
lessons we learned Concerning the validity
of syllogisms, and how we

66
00:04:38,450 --> 00:04:43,000
can use Venn diagrams to, predict and
explain the validity of syllogisms.

67
00:04:43,000 --> 00:04:47,540
How we can now apply those lessons, to
arguments that,

68
00:04:48,650 --> 00:04:52,050
are syllogisms, but you wouldn't think
that they are,

69
00:04:52,050 --> 00:04:55,310
just to look at the language in which
they're expressed.

70
00:04:55,310 --> 00:04:56,440
Okay, see you next time.

