1
00:00:03,410 --> 00:00:07,222
Now, we've really done it.
We're finished with reconstruction.

2
00:00:07,222 --> 00:00:13,420
Hooray!
But we want to go through it one more time

3
00:00:13,420 --> 00:00:17,875
in a detailed example to show how the, all
the different stages fit together.

4
00:00:17,875 --> 00:00:20,620
And which example are we going to pick?

5
00:00:20,620 --> 00:00:24,450
Well, if you've been in the course this
far, I bet you can guess.

6
00:00:24,450 --> 00:00:26,380
Robert Redford again.

7
00:00:26,380 --> 00:00:28,630
We're going to look at paragraph three of
his

8
00:00:28,630 --> 00:00:33,770
article, and go through reconstruction to
show that reconstruction gives you an even

9
00:00:33,770 --> 00:00:36,560
deeper understanding than when we first

10
00:00:36,560 --> 00:00:39,080
went through it using close analysis
alone.

11
00:00:39,080 --> 00:00:41,160
Reconstruction begins with close analysis.

12
00:00:41,160 --> 00:00:44,320
So, the first thing I'll do is read
through the passage and mark

13
00:00:44,320 --> 00:00:49,836
the important words in order to do a close
analysis of Redford's paragraph.

14
00:00:49,836 --> 00:00:53,790
The BLM says its hands are tied.
Remember, the BLM is the Bureau

15
00:00:53,790 --> 00:00:56,760
of Land Management.
Why?

16
00:00:58,960 --> 00:01:01,700
Because, because, is a premise marker.

17
00:01:04,040 --> 00:01:06,490
It's indicating that the sentence after it
is a

18
00:01:06,490 --> 00:01:10,730
premise for the conclusion that the BLM's
hands are tied.

19
00:01:12,630 --> 00:01:16,935
Because these lands were set aside subject
to valid existing rights.

20
00:01:16,935 --> 00:01:22,980
You could say that valid and rights are
evaluative

21
00:01:22,980 --> 00:01:26,040
terms, but that's not going to matter to
our analysis here.

22
00:01:28,110 --> 00:01:32,260
And Conoco has a lease that gives it a
right to drill.

23
00:01:33,260 --> 00:01:36,760
And notice, we've got a tricky argument
marker here.

24
00:01:36,760 --> 00:01:40,474
Gives it, because the point of that
sentence

25
00:01:40,474 --> 00:01:45,550
is that it's the lease that explains why
it

26
00:01:45,550 --> 00:01:47,860
has the right to drill, or justifies the

27
00:01:47,860 --> 00:01:50,300
claim that it does have the right to
drill.

28
00:01:50,300 --> 00:01:52,970
So, gives it is an argument marker.

29
00:01:52,970 --> 00:01:55,300
And in particular, it has the right to

30
00:01:55,300 --> 00:01:58,580
drill is the conclusion, so it's a
conclusion marker.

31
00:01:59,870 --> 00:02:02,350
Sure, Conoco has a lease.

32
00:02:02,350 --> 00:02:05,180
Sure is going to be an assuring term.

33
00:02:06,600 --> 00:02:11,120
More than one, in fact.
In fact, is another assuring term.

34
00:02:11,120 --> 00:02:11,620
But,

35
00:02:14,780 --> 00:02:17,630
but is going to be a discounting term,

36
00:02:17,630 --> 00:02:21,510
indicating that there's an objection being
responded to.

37
00:02:23,480 --> 00:02:26,220
Those leases were originally issued
without

38
00:02:26,220 --> 00:02:29,345
sufficient environmental study or public
input.

39
00:02:29,345 --> 00:02:34,350
Now, you could think that originally is a
guarding term.

40
00:02:34,350 --> 00:02:36,100
Because it's not saying that there never

41
00:02:36,100 --> 00:02:39,820
was sufficient environmental input, that
is environmental

42
00:02:39,820 --> 00:02:42,240
study or public input.

43
00:02:42,240 --> 00:02:45,190
It might of come later just not
originally, but again,

44
00:02:45,190 --> 00:02:48,980
that's not going to play any part in the
argument itself.

45
00:02:48,980 --> 00:02:52,430
So, it could be a guarding term, but you
can mark it if you want.

46
00:02:52,430 --> 00:02:55,810
But as a result, the first three words

47
00:02:55,810 --> 00:02:59,280
of the next sentence that's clearly an
argument marker.

48
00:02:59,280 --> 00:03:02,530
And it's indicating that the sentence
after it is a conclusion.

49
00:03:02,530 --> 00:03:03,800
So, it's a conclusion marker.

50
00:03:04,830 --> 00:03:07,111
None of them conveyed a valid right to
drill.

51
00:03:07,111 --> 00:03:12,322
What's more, now, we're indicating that
there's a separate

52
00:03:12,322 --> 00:03:17,329
argument, there's a new premise coming,
another reason for

53
00:03:17,329 --> 00:03:22,160
the same conclusion, in deciding to issue
a permit to drill right now.

54
00:03:23,270 --> 00:03:26,290
The BLM did not conduct a full analysis of

55
00:03:26,290 --> 00:03:29,928
the environmental impacts of drilling in
these incomparable lands,

56
00:03:29,928 --> 00:03:36,300
but, discounting.
Right?

57
00:03:36,300 --> 00:03:38,590
Instead determined that there would be no

58
00:03:38,590 --> 00:03:43,180
significant environmental harm on the
basis of.

59
00:03:43,180 --> 00:03:50,650
This is telling you how they reached that
determination, was an abbreviated review.

60
00:03:50,650 --> 00:03:55,080
They had an abbreviated review that
justified or explained

61
00:03:55,080 --> 00:03:58,035
their determination.
So, that's a premise marker.

62
00:03:58,035 --> 00:04:00,610
That didn't even look.

63
00:04:00,610 --> 00:04:05,072
Even is a tricky one.
It's discounting an objection.

64
00:04:05,072 --> 00:04:07,610
It's saying, well, they looked a little
bit, but they didn't look at this.

65
00:04:08,640 --> 00:04:11,745
Discounting the objection that they did
look at least some.

66
00:04:11,745 --> 00:04:17,368
They didn't even look at drilling on the
other federal leases.

67
00:04:17,368 --> 00:04:20,469
Okay?
Sounds like,

68
00:04:20,469 --> 00:04:21,720
clearly a guarding term.

69
00:04:21,720 --> 00:04:26,482
It's not saying it is, but it sounds like
Washington doublespeak.

70
00:04:26,482 --> 00:04:31,070
Doublespeak is bad, you don't want to
doublespeak to

71
00:04:31,070 --> 00:04:34,370
me, and maybe to me is another guarding
term.

72
00:04:34,370 --> 00:04:37,240
In the sense that it sounds that way to me
but not to others.

73
00:04:37,240 --> 00:04:39,870
So we could mark that as a guard as well.

74
00:04:40,870 --> 00:04:45,880
So, we have finished stage one of
reconstructing this argument.

75
00:04:45,880 --> 00:04:46,860
We've done a close analysis.

76
00:04:48,900 --> 00:04:52,660
The next stage is to get rid of all the
excess verbiage.

77
00:04:52,660 --> 00:04:55,120
So we'll start a new screen and do that.

78
00:04:56,760 --> 00:04:58,900
The BLM says its hand are tied.

79
00:04:58,900 --> 00:05:01,980
Well, the claim is that its hands are
tied.

80
00:05:01,980 --> 00:05:05,970
So we can get rid of the fact that the BLM
says that.

81
00:05:05,970 --> 00:05:08,850
Why, because, we can get rid of those
words, because

82
00:05:08,850 --> 00:05:11,980
they're going to get replaced by a dot
pyramid in standard form.

83
00:05:13,080 --> 00:05:14,060
These lands were set

84
00:05:14,060 --> 00:05:17,930
aside subject to valid existing rights,
good.

85
00:05:17,930 --> 00:05:22,680
And Conoco has a lease that gives it the
right to drill, good.

86
00:05:22,680 --> 00:05:26,665
And we can get rid of, because we're
going to have two separate premises there.

87
00:05:26,665 --> 00:05:30,070
That gives it, right.

88
00:05:30,070 --> 00:05:34,110
Well, that's going to be an argument
marker as we saw.

89
00:05:34,110 --> 00:05:36,068
That's going to get replaced by another
dot pyramid.

90
00:05:36,068 --> 00:05:39,450
Sure, Conoco has a lease,

91
00:05:39,450 --> 00:05:41,060
more than one in fact.

92
00:05:41,060 --> 00:05:46,070
Now notice there Redford is admitting what
his opponent claims.

93
00:05:46,070 --> 00:05:48,380
But that's not going to be part of his
argument.

94
00:05:48,380 --> 00:05:50,368
He's just saying, I recognize that.

95
00:05:50,368 --> 00:05:52,690
His argument is going to be based on
different claims.

96
00:05:52,690 --> 00:05:57,590
So we can get rid of, sure, Conoco has a
lease, more than one in fact.

97
00:05:57,590 --> 00:06:00,760
And the but, tells you that he's just
answering an objection.

98
00:06:02,480 --> 00:06:04,620
These leases were originally issued
without

99
00:06:04,620 --> 00:06:08,068
sufficient environmental study or input.
That's going to be an important premise.

100
00:06:08,068 --> 00:06:12,260
As a result, we saw that was an argument
marker.

101
00:06:12,260 --> 00:06:15,540
going to be replaced by a dot pyramid like
the others.

102
00:06:15,540 --> 00:06:17,680
None of them conveyed a valid right to
drill.

103
00:06:17,680 --> 00:06:20,400
What's more, another argument marker So,
it's

104
00:06:20,400 --> 00:06:22,780
going to get replaced by a dot pyramid.

105
00:06:22,780 --> 00:06:25,570
In deciding that, to issue a permit to
drill now, the

106
00:06:25,570 --> 00:06:30,470
BLM did not conduct a full analysis blah
blah blah, but instead.

107
00:06:30,470 --> 00:06:33,900
That's going to be discounting terms.

108
00:06:33,900 --> 00:06:36,960
And that's going to be important for
understanding

109
00:06:36,960 --> 00:06:38,660
what the sentences are doing, but it's

110
00:06:38,660 --> 00:06:42,859
not going to get repeated in the premise
when we put it in standard form.

111
00:06:44,210 --> 00:06:47,140
They determined there'd be no significant
impact on the basis of an

112
00:06:47,140 --> 00:06:52,470
abbreviated review that didn't even look
at drilling on the other federal leases.

113
00:06:52,470 --> 00:06:55,580
The next step is to take all the parts
that weren't

114
00:06:55,580 --> 00:06:59,040
crossed out, their going to be the
explicit premises and

115
00:06:59,040 --> 00:07:02,592
conclusion in the argument, and put them
into standard form.

116
00:07:02,592 --> 00:07:06,880
And if you think about this paragraph,
there really two arguments.

117
00:07:06,880 --> 00:07:10,670
Because at the start, what Redford tries
to do is

118
00:07:10,670 --> 00:07:14,710
to state what the BLM's argument was to
begin with.

119
00:07:14,710 --> 00:07:20,129
And then he gives his own argument against
what the BLM says.

120
00:07:20,129 --> 00:07:20,471
So,

121
00:07:20,471 --> 00:07:24,490
BLM's argument can be put in standard form
like this.

122
00:07:25,610 --> 00:07:30,360
First premise, Conoco has a lease that
gives it the right to drill.

123
00:07:30,360 --> 00:07:36,004
Second premise, these lands were set aside
subject to valid existing rights.

124
00:07:36,004 --> 00:07:41,506
And those two premises lead to the
conclusion that the BLM's hands were tied.

125
00:07:43,210 --> 00:07:45,710
Now, that's the BLM's arguement that
Redford

126
00:07:45,710 --> 00:07:47,088
is arguing against.

127
00:07:47,088 --> 00:07:52,710
Redford's own argument on the other side
starts with the premise, those leases

128
00:07:52,710 --> 00:07:57,500
were originally issued without sufficient
environmental study or public input.

129
00:07:58,890 --> 00:08:04,600
Second premise, in deciding to issue a
permit to drill now, the BLM did

130
00:08:04,600 --> 00:08:06,610
not conduct a full analysis of the

131
00:08:06,610 --> 00:08:10,530
environmental impacts of drilling on these
incomparable lands.

132
00:08:10,530 --> 00:08:11,200
But instead

133
00:08:11,200 --> 00:08:15,990
determined there would be no significant
environmental harm on the basis of an

134
00:08:15,990 --> 00:08:21,390
abbreviated review, that didn't even look
at drilling on the other federal leases.

135
00:08:23,220 --> 00:08:27,300
Those two premises lead to the conclusion
that

136
00:08:27,300 --> 00:08:29,990
none of these conveyed a valid right to
drill.

137
00:08:31,590 --> 00:08:36,430
And, then there's another premise, these
lands were set aside subject to

138
00:08:36,430 --> 00:08:38,200
valid existing rights.

139
00:08:39,500 --> 00:08:40,520
And that's supposed to lead

140
00:08:40,520 --> 00:08:43,585
the conclusion, this is Washington
doublespeak.

141
00:08:43,585 --> 00:08:48,920
Well, we'll have to see what all that
means when we clarify.

142
00:08:48,920 --> 00:08:51,610
But that's basically the standard form.

143
00:08:51,610 --> 00:08:55,560
It might be a good idea to double check,
because you want to make sure

144
00:08:55,560 --> 00:09:01,490
that every sentence you didn't cross out,
is somewhere there in the standard form.

145
00:09:01,490 --> 00:09:05,010
So, let's look back at the passage where
we crossed out

146
00:09:05,010 --> 00:09:08,840
the excess and make sure everything is
there in the standard form.

147
00:09:11,720 --> 00:09:12,980
Okay.
It's all there.

