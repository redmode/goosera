1
00:00:03,660 --> 00:00:05,230
Welcome back.

2
00:00:05,230 --> 00:00:07,800
Now, you've done paragraph three
yourselves and

3
00:00:07,800 --> 00:00:09,380
we're going to go on to paragraph four.

4
00:00:09,380 --> 00:00:14,420
It might seem like we're doing an awful
lot of this but remember, the only way to

5
00:00:14,420 --> 00:00:19,410
learn close analysis is to practice,
practice, practice, practice, practice.

6
00:00:19,410 --> 00:00:23,020
Over and over again, on as many passages
as you can find.

7
00:00:23,020 --> 00:00:25,460
So, we're going to spend one more lecture
going

8
00:00:25,460 --> 00:00:29,340
through paragraphs four and five of Robert
Redford's

9
00:00:29,340 --> 00:00:35,788
piece in the Washington Post from 1997.
The title was A Piece of God's Handiwork.

10
00:00:35,788 --> 00:00:43,050
Paragraph four starts, sounds like
Washington double-speak to me.

11
00:00:43,050 --> 00:00:46,820
What is it that sounds like Washington
double-speak to him?

12
00:00:46,820 --> 00:00:51,870
It's the sentence at the end of the
previous paragraph that you analyze.

13
00:00:51,870 --> 00:00:54,360
And that's the claim that they issued the

14
00:00:54,360 --> 00:00:59,300
permit without a full review, using an
abbreviated review, and

15
00:00:59,300 --> 00:01:02,400
they didn't even look at the leases on
other federal lands.

16
00:01:03,570 --> 00:01:05,790
So, he's saying that sounds like the kind

17
00:01:05,790 --> 00:01:08,600
of thing Washington does, when they double
speak.

18
00:01:08,600 --> 00:01:12,240
Double speak's obviously a metaphor,
right?

19
00:01:12,240 --> 00:01:15,630
It's a metaphor for saying one thing to
one person another

20
00:01:15,630 --> 00:01:19,440
thing to another person or sometimes
saying one thing that seems

21
00:01:19,440 --> 00:01:21,930
to mean one thing when what they really
mean is another thing.

22
00:01:21,930 --> 00:01:28,209
But however, you interpret the metaphor
double speak, it's not good so we

23
00:01:28,209 --> 00:01:34,770
can indicate that double speaking is a
negative evaluation.

24
00:01:34,770 --> 00:01:39,290
And it's a, a metaphor, but clearly
negative in its meaning.

25
00:01:39,290 --> 00:01:44,940
You wouldn't say someone who's speaking
properly is double-speak.

26
00:01:44,940 --> 00:01:51,010
Notice, however, that he has this phrase
sounds like, and it sounds like it to him.

27
00:01:51,010 --> 00:01:53,764
What's sounds like doing?

28
00:01:53,764 --> 00:01:57,700
Well, he's not saying that it is
Washington double-speak.

29
00:01:57,700 --> 00:02:01,510
So, he's guarding it, he's weakening the
claim, he's

30
00:02:01,510 --> 00:02:04,070
not saying it is, he's saying it sounds
like.

31
00:02:04,070 --> 00:02:07,154
And so that can be labeled as a guarding
term.

32
00:02:07,154 --> 00:02:07,906
Okay.

33
00:02:07,906 --> 00:02:10,536
So, he's guarded

34
00:02:10,536 --> 00:02:14,480
that claim but now, he's going to go on

35
00:02:14,480 --> 00:02:19,150
and argue against the plan that Washington
was using.

36
00:02:21,210 --> 00:02:26,500
I've spent considerable time on these
extraordinary lands for years.

37
00:02:26,500 --> 00:02:28,910
I've spent, for years, let's just circle

38
00:02:28,910 --> 00:02:31,455
that whole thing to indicate that whole
phrase.

39
00:02:31,455 --> 00:02:32,860
because it's all doing the same thing.

40
00:02:32,860 --> 00:02:36,800
What's it doing?
It's assuring you.

41
00:02:36,800 --> 00:02:41,430
You see, because I have spent so much time
on these lands, I spent

42
00:02:41,430 --> 00:02:46,740
so many years on them, you can trust me to
know what's going on.

43
00:02:46,740 --> 00:02:47,930
I've got the evidence.

44
00:02:47,930 --> 00:02:52,400
Notice, like other assuring terms, it's
not giving you the evidence.

45
00:02:52,400 --> 00:02:56,150
It's saying, you're to trust me because
I've got the evidence.

46
00:02:56,150 --> 00:02:58,230
I've had the experience.

47
00:02:58,230 --> 00:03:01,320
So, it's a perfect example of assuring
because it's

48
00:03:01,320 --> 00:03:04,380
saying that he has reasons without
actually giving the reasons.

49
00:03:04,380 --> 00:03:06,910
And that's why you can't question his
reasons because

50
00:03:06,910 --> 00:03:11,070
you haven't spent considerable time on
those extraordinary lands.

51
00:03:11,070 --> 00:03:12,460
And of

52
00:03:12,460 --> 00:03:14,580
course, he follows that up with another
assuring term

53
00:03:14,580 --> 00:03:18,280
he says, and I know, you say I know.

54
00:03:18,280 --> 00:03:19,840
That's an assuring term.

55
00:03:19,840 --> 00:03:22,460
A mental assuring term because it's
describing

56
00:03:22,460 --> 00:03:25,150
the mental state as being one of
knowledge.

57
00:03:25,150 --> 00:03:27,850
Knowledge implies truth and to say I know
it is to

58
00:03:27,850 --> 00:03:33,210
imply that it's true to assure you that
it's true, 'kay?

59
00:03:33,210 --> 00:03:36,740
And what does he know?
He knows that an oil rig in their midst.

60
00:03:36,740 --> 00:03:38,920
Would have a major impact.

61
00:03:38,920 --> 00:03:41,940
Now, what about that a major impact, okay?
Now,

62
00:03:44,369 --> 00:03:46,830
he's clearly saying it would be a bad
impact but does he

63
00:03:46,830 --> 00:03:51,370
come out an say it's bad no he just says
it's major, okay?

64
00:03:51,370 --> 00:03:54,070
So, it's not evaluative term that would be

65
00:03:54,070 --> 00:03:58,240
labeled as nothing if nothing is an
option.

66
00:03:58,240 --> 00:04:02,260
So, clearly suggesting that it's bad but
he's not saying it.

67
00:04:02,260 --> 00:04:06,360
So it shouldn't be marked as an evaluative
term, okay?

68
00:04:06,360 --> 00:04:08,140
Next, what's more?

69
00:04:08,140 --> 00:04:09,480
Well, what's more is kind

70
00:04:09,480 --> 00:04:10,240
of a conjunction.

71
00:04:10,240 --> 00:04:13,870
But what came before this was an argument
that

72
00:04:13,870 --> 00:04:17,440
it would have a major impact based on his
assurance.

73
00:04:17,440 --> 00:04:20,250
And so when he says, what's more, he's
suggesting that what's going to come

74
00:04:20,250 --> 00:04:26,270
next is yet another argument for why we
shouldn't have these, these permits issue.

75
00:04:26,270 --> 00:04:29,190
He's going to tell you other bad things
about them.

76
00:04:29,190 --> 00:04:34,530
So, that can be a premise marker because
what comes after it is the premise.

77
00:04:35,740 --> 00:04:38,320
They want to drill to find oil.

78
00:04:38,320 --> 00:04:41,240
Well, we can say to find oil, it's in
order to

79
00:04:41,240 --> 00:04:46,010
find oil that is going to explain why they
want to drill.

80
00:04:46,010 --> 00:04:50,490
It's a teleological explanation as we saw
in an earlier lecture.

81
00:04:50,490 --> 00:04:55,070
And so that is going to be a reason marker
because their desire

82
00:04:55,070 --> 00:04:59,706
to find oil Is what explains their desire
to want to drill, okay?

83
00:04:59,706 --> 00:05:02,478
Inevitability.
What's that?

84
00:05:02,478 --> 00:05:03,406
Assuring.

85
00:05:03,406 --> 00:05:07,814
It's assuring you that it's obviously
true.

86
00:05:07,814 --> 00:05:10,250
There's no way around it.

87
00:05:10,250 --> 00:05:13,848
It is inevitable.
What's inevitable?

88
00:05:13,848 --> 00:05:20,560
More rigs, more roads, new pipelines,
toxic wastes and bright lights

89
00:05:20,560 --> 00:05:25,210
would follow to get the oil out, okay?
He's assuring

90
00:05:25,210 --> 00:05:27,910
you, that all of those things are going to
happen.

91
00:05:27,910 --> 00:05:32,540
The previous argument before the what's
more was simply an oil rig.

92
00:05:32,540 --> 00:05:37,370
Notice it's just an oil rig, one oil rig
up there in that sentence.

93
00:05:37,370 --> 00:05:41,770
But now we've got more rigs, more roads,
new pipelines.

94
00:05:41,770 --> 00:05:44,680
So, if an oil rig has a major impact, all
of

95
00:05:44,680 --> 00:05:47,830
this other stuff's going to have a lot
more of an impact.

96
00:05:47,830 --> 00:05:50,430
That's the point of the what's more.

97
00:05:50,430 --> 00:05:55,996
He's adding additional reasons why the
permits should not be issued, 'kay?

98
00:05:55,996 --> 00:05:58,050
And they would follow.

99
00:05:58,050 --> 00:06:00,890
That means it's going to follow, what?
In time.

100
00:06:00,890 --> 00:06:01,800
It's just temporal.

101
00:06:01,800 --> 00:06:05,694
It's not saying, anything more than it's
going to follow at a

102
00:06:05,694 --> 00:06:09,590
later time but they're going to follow, to
get the oil out.

103
00:06:09,590 --> 00:06:13,290
That explains why they would follow
because, right?

104
00:06:13,290 --> 00:06:15,990
You would need them in order to get the
oil out.

105
00:06:15,990 --> 00:06:19,770
So, that's going to be an argument marker
itself.

106
00:06:19,770 --> 00:06:22,498
Is it a reason marker or is it a
conclusion marker?

107
00:06:22,498 --> 00:06:26,925
Well, the conclusion is that all that's
going to follow, right?

108
00:06:26,925 --> 00:06:32,160
And the premise is that it's needed in
order to get the oil out.

109
00:06:32,160 --> 00:06:34,970
So, this is going to be a premise marker.

110
00:06:36,300 --> 00:06:38,290
And the conclusion is that you get all

111
00:06:38,290 --> 00:06:41,170
that stuff, the pipelines the roads the
waste and

112
00:06:41,170 --> 00:06:43,830
so on, okay?

113
00:06:43,830 --> 00:06:48,480
So those are his reasons against giving a
permit.

114
00:06:48,480 --> 00:06:52,730
And then he says the BLM couldn't see
this, okay?

115
00:06:52,730 --> 00:06:57,880
Just states it as a fact, but.
What's the but doing there?

116
00:06:57,880 --> 00:07:03,160
Remember what kind of word a but is?
But is a discounting term.

117
00:07:03,160 --> 00:07:06,190
He's answering an objection.
The objection is,

118
00:07:06,190 --> 00:07:10,840
well, you say all that would follow but
the BLM would disagree with you

119
00:07:10,840 --> 00:07:15,330
and they looked at it very carefully and
they're an authority or an expert.

120
00:07:15,330 --> 00:07:19,600
So, the answer to that objection is, the
U.S.

121
00:07:19,600 --> 00:07:21,500
Fish and Wildlife Service and the

122
00:07:21,500 --> 00:07:25,460
Environmental Protection Agency, did see
this.

123
00:07:25,460 --> 00:07:29,890
Notice that what comes after the but is
more important than what came before it.

124
00:07:29,890 --> 00:07:31,360
What he's doing is saying that there's

125
00:07:31,360 --> 00:07:36,430
a contrast between what the BLM couldn't
see and what the U.S.

126
00:07:36,430 --> 00:07:40,850
Fish and Wildlife Service and the
Environmental Protection Agency did see.

127
00:07:40,850 --> 00:07:43,990
But in addition to the contrast between
those two

128
00:07:43,990 --> 00:07:46,690
he's also saying it's more important that
the U.S.

129
00:07:46,690 --> 00:07:48,180
Fish and Wildlife Service and the

130
00:07:48,180 --> 00:07:52,750
Environmental Protection Agency did see
it, okay?

131
00:07:52,750 --> 00:07:56,600
So, what comes after the but is taken to
be more important.

132
00:07:58,880 --> 00:08:00,630
And what did they see?

133
00:08:00,630 --> 00:08:05,190
Both of these agencies recognized,
recognize implies

134
00:08:05,190 --> 00:08:07,190
you can't recognize things that aren't
true.

135
00:08:07,190 --> 00:08:09,420
You know, if you see somebody and you'd
say

136
00:08:09,420 --> 00:08:12,040
I recognize my sister and it wasn't your
sister.

137
00:08:12,040 --> 00:08:13,680
You didn't really recognize them.

138
00:08:13,680 --> 00:08:17,350
So, to say you recognize is to assure
people that it's really true.

139
00:08:17,350 --> 00:08:23,900
The devastating, now devastating can't be
good so that's e minus.

140
00:08:25,670 --> 00:08:29,570
Effects of extensive oil drilling.
Extensive is that bad?

141
00:08:29,570 --> 00:08:33,850
No, extensive oil drilling's good in the
right places.

142
00:08:33,850 --> 00:08:38,630
Gives us the kind of energy we need to be
able to accomplish the goals that we want.

143
00:08:38,630 --> 00:08:41,120
Maybe you'd wish you didn't need extensive
oil drilling.

144
00:08:41,120 --> 00:08:45,790
But, if you need it, then it's good, when
it's done in the right place.

145
00:08:45,790 --> 00:08:50,185
And that drilling would have a,
devastating effects on this area.

146
00:08:50,185 --> 00:08:58,310
And then those two agencies urged the BLM
to refuse to allow it, okay?

147
00:08:58,310 --> 00:09:01,340
Why?
In order to protect the monument.

148
00:09:01,340 --> 00:09:03,810
And again, we've seen this kind of
argument

149
00:09:03,810 --> 00:09:08,600
repeatedly at several points during this
particular op-ed.

150
00:09:08,600 --> 00:09:11,180
That's saying that the goal is to protect
the

151
00:09:11,180 --> 00:09:15,560
monument and that's what justifies urging
the BLM to refuse

152
00:09:15,560 --> 00:09:18,690
to allow it.
So this is going to be a premise marker.

153
00:09:18,690 --> 00:09:20,940
We've seen an order to protect, an order
to

154
00:09:20,940 --> 00:09:24,290
preserve because we want to protect and so
on.

155
00:09:24,290 --> 00:09:25,720
He keeps repeating that.

156
00:09:25,720 --> 00:09:27,500
That's not a bad thing.

157
00:09:27,500 --> 00:09:32,870
Many times when someone's giving an
argument, they repeat pretty much the same

158
00:09:32,870 --> 00:09:35,050
words as in other areas because they're

159
00:09:35,050 --> 00:09:38,150
giving several arguments for a single
conclusion.

160
00:09:38,150 --> 00:09:40,650
We'll actually see how those different
reasons can fit

161
00:09:40,650 --> 00:09:44,070
together in the next section of the
course, but for now all we're

162
00:09:44,070 --> 00:09:47,850
doing is marking the words in an order to,
as a premise marker.

163
00:09:47,850 --> 00:09:52,260
We finished four paragraphs!

164
00:09:52,260 --> 00:09:54,100
Alright!

165
00:09:54,100 --> 00:09:57,060
We're almost done!
Yes!

166
00:09:57,060 --> 00:10:00,910
Now, we're on to paragraph five, and it
starts, maybe

167
00:10:00,910 --> 00:10:06,000
the problem comes from giving management
responsibility for this monument to

168
00:10:06,000 --> 00:10:10,070
the BLM.
What about the word, maybe?

169
00:10:10,070 --> 00:10:14,920
The word maybe is about as clear a case as
you can get of a guarding term.

170
00:10:14,920 --> 00:10:17,830
It's saying, it might be the case, it
might not be the

171
00:10:17,830 --> 00:10:21,530
case, I'm not going to definitely claim
that's where the problem comes from.

172
00:10:21,530 --> 00:10:23,840
I'm just suggesting that maybe so somebody

173
00:10:23,840 --> 00:10:25,810
comes along and says, that's not really
true.

174
00:10:25,810 --> 00:10:28,410
I, I'm going to say, well, j, maybe it's
true.

175
00:10:28,410 --> 00:10:31,000
It's all I was claiming.
And so I'm

176
00:10:31,000 --> 00:10:36,880
now able to defend my premise, okay?
Problem, what about problem?

177
00:10:38,840 --> 00:10:40,370
Gotta be e minus, right?

178
00:10:40,370 --> 00:10:46,470
Another clear example, because you can't
have good things, if there are problems.

179
00:10:46,470 --> 00:10:49,130
Sure you can have problems on math tests
that are good

180
00:10:49,130 --> 00:10:51,800
but that's not the kind of problem we're
talking about here.

181
00:10:51,800 --> 00:10:56,630
These kinds of problems are bad and so
that word gets marked as

182
00:10:56,630 --> 00:10:57,720
e minus.

183
00:10:58,790 --> 00:11:03,490
So, the problem comes from namely, is
explained by

184
00:11:03,490 --> 00:11:07,840
giving management responsibility for this
monument to the BLM.

185
00:11:07,840 --> 00:11:12,401
So comes from can get marked as a premise
marker.

186
00:11:12,401 --> 00:11:17,260
It's the giving management responsibility
to the BLM that

187
00:11:17,260 --> 00:11:21,840
explains why there's a problem in the
first place.

188
00:11:21,840 --> 00:11:24,800
And that could be put in the form of an
argument, that explanation could.

189
00:11:26,600 --> 00:11:26,960
Okay.

190
00:11:26,960 --> 00:11:30,194
So, why is giving management
responsibility

191
00:11:30,194 --> 00:11:32,330
to the BLM create a problem?

192
00:11:32,330 --> 00:11:35,570
Because the next sentence tells you this
is the BLM's

193
00:11:35,570 --> 00:11:39,340
first national monument, the first time
they've ever done this.

194
00:11:39,340 --> 00:11:45,510
Almost all the other monuments are managed
by the National Park Service, okay?

195
00:11:45,510 --> 00:11:46,980
Nothing wrong in itself with being

196
00:11:46,980 --> 00:11:49,180
the first.
There always has to be a first.

197
00:11:49,180 --> 00:11:50,820
So there's nothing evaluative there.

198
00:11:50,820 --> 00:11:56,130
What about almost all?
Clearly guarding, right?

199
00:11:56,130 --> 00:12:01,120
Because it's not all, it's almost all.
So the claim is more defensible.

200
00:12:02,280 --> 00:12:05,790
Almost all the others are managed by the
National Park Service.

201
00:12:07,410 --> 00:12:13,220
The park service's mission is to protect
the resources under its care, okay?

202
00:12:13,220 --> 00:12:19,140
Protect good, resources good.
So those

203
00:12:19,140 --> 00:12:26,050
both get e plus.
While, what about the word while?

204
00:12:26,050 --> 00:12:27,960
It's not completely clear.

205
00:12:27,960 --> 00:12:29,980
You could read this as simply setting up a

206
00:12:29,980 --> 00:12:34,950
contrast between the park services mission
and the bureau's mission.

207
00:12:34,950 --> 00:12:38,830
But you could also read it as responding
to an objection.

208
00:12:38,830 --> 00:12:41,870
Well, doesn't the government protect those
resources?

209
00:12:41,870 --> 00:12:47,750
And the answer is well, the park service
does but the bureau has a different role.

210
00:12:47,750 --> 00:12:52,590
The bureau has always sought to
accommodate economic uses, okay?

211
00:12:52,590 --> 00:12:55,050
So, if you read that while as but and

212
00:12:55,050 --> 00:12:57,560
you replace it with but it seems to make
sense.

213
00:12:57,560 --> 00:13:01,540
The Park Service's mission is to protect,
but the bureau has

214
00:13:01,540 --> 00:13:04,400
always, it looks like you can replace it
with the discounting

215
00:13:04,400 --> 00:13:05,910
term, which means that while is

216
00:13:05,910 --> 00:13:10,340
functioning here as a discounting term,
okay?

217
00:13:10,340 --> 00:13:17,250
The bureau has always saw it, no guardian
there, it's always sought that, right?

218
00:13:17,250 --> 00:13:22,430
To accommodate economic uses of those
resources under it's care.

219
00:13:23,920 --> 00:13:29,390
Even so, starts the next sentence, what is
even so doing?

220
00:13:29,390 --> 00:13:35,990
Well, even so, is a discounting term
because it's discounting an objection.

221
00:13:37,100 --> 00:13:39,590
The objection is, well, they've always
sought

222
00:13:39,590 --> 00:13:42,510
to accommodate those uses, well, then how

223
00:13:42,510 --> 00:13:46,190
do you explain the fact that they got off
to such a good start?

224
00:13:46,190 --> 00:13:49,820
They seem to be, okay?
They seemed to be.

225
00:13:49,820 --> 00:13:52,535
That's a guarding term.
It's not saying they were.

226
00:13:52,535 --> 00:13:55,000
It's saying they seemed to be

227
00:13:55,000 --> 00:13:59,200
getting off to a good start.
Good, boy there's an evaluative plus.

228
00:13:59,200 --> 00:14:02,360
You can't beat that for a clear evaluative
plus.

229
00:14:02,360 --> 00:14:05,750
To a good start by enlisting broad public

230
00:14:05,750 --> 00:14:09,710
involvement in developing a management
plan for the area.

231
00:14:09,710 --> 00:14:14,720
Well, what was good about it?
They enlisted broad public involvement.

232
00:14:14,720 --> 00:14:16,170
Therefore, it was good.

233
00:14:16,170 --> 00:14:20,736
Looks like by enlisting could be a premise
marker.

234
00:14:20,736 --> 00:14:25,360
The premise is, enlist, they enlisted
broad public involvement.

235
00:14:25,360 --> 00:14:27,654
Therefore, they got off to a good start.

236
00:14:27,654 --> 00:14:30,603
Yet.
Whats yet?

237
00:14:30,603 --> 00:14:36,490
Another discounting term.

238
00:14:36,490 --> 00:14:38,720
The agency's decision to allow oil
drilling in the

239
00:14:38,720 --> 00:14:42,750
monument completely undercuts this process
just as it is beginning.

240
00:14:42,750 --> 00:14:46,490
The objection is, so if they go off to a
good start whats the problem

241
00:14:46,490 --> 00:14:49,028
if they listed broad public involvement,
whats the problem?

242
00:14:50,720 --> 00:14:54,790
Well, that's the objection and the
response is

243
00:14:54,790 --> 00:14:57,680
now they've decided to allow drilling in
the monument.

244
00:14:57,680 --> 00:15:03,642
And that completely undercuts the process
that did enlist public involvement, right?

245
00:15:03,642 --> 00:15:07,253
So, the yet is just counting the objection
that says there's

246
00:15:07,253 --> 00:15:12,430
no problem here, they're doin just fine,
they've got broad public involvement.

247
00:15:12,430 --> 00:15:14,660
They're saying the response to that
objection is

248
00:15:14,660 --> 00:15:18,200
but now they have not got public
involvement.

249
00:15:18,200 --> 00:15:20,910
They're doing it without a complete review
and

250
00:15:20,910 --> 00:15:23,560
so on is mentioned in the preceding
paragraph.

251
00:15:25,110 --> 00:15:27,610
The agency's decision to allow oil
drilling

252
00:15:27,610 --> 00:15:31,380
in the monument completely no guarding,
right?

253
00:15:31,380 --> 00:15:34,740
Completely undercuts when it's that strong
a word

254
00:15:34,740 --> 00:15:37,810
like completely, you can almost say it's
I'm assuring

255
00:15:37,810 --> 00:15:41,800
you it's not just partially undercutting,
it's completely undercutting.

256
00:15:41,800 --> 00:15:44,210
That's a way of, of making you confident
that

257
00:15:44,210 --> 00:15:47,415
it really does, at least partially,
undercut the process.

258
00:15:47,415 --> 00:15:50,920
And undercuts sounds like something bad.

259
00:15:50,920 --> 00:15:55,380
I suppose you might not want to mark it as
evaluative because, you know,

260
00:15:55,380 --> 00:15:59,320
you could undercut a bad process and that
would be a good thing.

261
00:15:59,320 --> 00:16:03,280
So, literally you probably should not mark
that as evaluative, but it's

262
00:16:03,280 --> 00:16:07,400
clear what Robert Redford thinks about
undercutting this process.

263
00:16:07,400 --> 00:16:08,420
And now just.

264
00:16:08,420 --> 00:16:11,520
Just stands for justice, doesn't it?

265
00:16:11,520 --> 00:16:15,640
No, remember from the very first word of
this op-ed.

266
00:16:15,640 --> 00:16:17,330
Just is nothing here.

267
00:16:17,330 --> 00:16:20,800
It means at the same time when it was
beginning.

268
00:16:20,800 --> 00:16:24,860
It's not an evaluative term, even though
just sometimes

269
00:16:24,860 --> 00:16:28,570
means something about being just or fair
or good,

270
00:16:28,570 --> 00:16:32,880
here it doesn't mean that at all.
So, we're back to the very word that we

271
00:16:32,880 --> 00:16:39,010
began this op-ed with and so that seems
like a good place to stop.

272
00:16:39,010 --> 00:16:40,590
I'll stop marking here.

273
00:16:40,590 --> 00:16:43,950
There are other things you could mark,
there are others things to discuss.

274
00:16:43,950 --> 00:16:48,020
These paragraphs, these op-eds are always
extremely complex

275
00:16:48,020 --> 00:16:51,640
and interesting to try to get the details
straight.

276
00:16:51,640 --> 00:16:53,670
But I'll give you a chance to look at

277
00:16:53,670 --> 00:16:56,480
the next three paragraphs.
They're all pretty short.

278
00:16:56,480 --> 00:16:59,820
Look at those paragraphs and see if you
can do a close analysis on those.

279
00:16:59,820 --> 00:17:05,770
Because as I've emphasized several times,
the best, indeed the only way

280
00:17:05,770 --> 00:17:10,010
to learn close analysis is to practice,
practice, practice, practice, practice.

