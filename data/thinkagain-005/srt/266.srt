1
00:00:03,040 --> 00:00:03,957
Welcome to Week Three.

2
00:00:03,957 --> 00:00:06,620
In weeks one and two, we already learned

3
00:00:06,620 --> 00:00:10,090
a lot about how to identify and analyze
arguments.

4
00:00:11,100 --> 00:00:14,940
We can do close analysis, we can identify
the premises

5
00:00:14,940 --> 00:00:20,045
and conclusions, we can put them in
standard form, what's next?

6
00:00:20,045 --> 00:00:24,330
Well, the next step is to take those parts
and put

7
00:00:24,330 --> 00:00:28,490
them in a certain order and fill in the
missing gaps.

8
00:00:28,490 --> 00:00:31,780
We need to learn how to reconstruct
arguments.

9
00:00:31,780 --> 00:00:32,392
Are you ready?

10
00:00:32,392 --> 00:00:36,466
Well there are lots of way to reconstruct.

11
00:00:36,466 --> 00:00:40,284
Think about constructing a house, or
building.

12
00:00:41,482 --> 00:00:43,785
In order to construct a good building, you
got to know

13
00:00:43,785 --> 00:00:47,330
what the goal is, what the standards of a
good building are.

14
00:00:47,330 --> 00:00:49,370
The same thing goes for reconstructing
arguments.

15
00:00:50,530 --> 00:00:53,520
In order to reconstruct an argument

16
00:00:53,520 --> 00:00:59,140
properly, we need to know what the
standards are for reconstruction.

17
00:00:59,140 --> 00:01:02,650
We're trying to reconstruct it, so as to
meet those standards.

18
00:01:02,650 --> 00:01:07,250
Because the goal is not to reconstruct the
argument in order to make it look bad.

19
00:01:07,250 --> 00:01:12,045
The point is going to be to reconstruct
arguments, so as to make them look good.

20
00:01:12,045 --> 00:01:14,860
because by making your opponents look bad

21
00:01:14,860 --> 00:01:17,530
or silly, that doesn't do anybody any
good.

22
00:01:17,530 --> 00:01:18,730
If you want to learn

23
00:01:18,730 --> 00:01:23,200
about their perspective, and you want to
learn from, their views.

24
00:01:23,200 --> 00:01:25,600
Then you need to reconstruct their
argument, so

25
00:01:25,600 --> 00:01:28,060
as to make it look as good as possible.

26
00:01:28,060 --> 00:01:31,570
And to do that,you need to know about the
standards for

27
00:01:31,570 --> 00:01:34,710
arguments, that is the standards that make
arguments good or bad.

28
00:01:35,930 --> 00:01:38,680
So what we're going to do this week, is
we're going to

29
00:01:38,680 --> 00:01:44,020
look first at some standards for
arguments, validity and soundness

30
00:01:44,020 --> 00:01:45,390
in particular.

31
00:01:45,390 --> 00:01:47,450
And then we're going to use those
standards to

32
00:01:47,450 --> 00:01:52,010
develop a method, called reconstruction or
deep analysis.

33
00:01:52,010 --> 00:01:53,470
I'll explain those terms later.

34
00:01:55,010 --> 00:01:59,080
And then we're going to apply that method
to a few concrete examples.

35
00:01:59,080 --> 00:02:02,660
In order to be able to take a passage, and
take those

36
00:02:02,660 --> 00:02:06,385
premises and conclusions and fill them out
and get a full fledged argument.

37
00:02:06,385 --> 00:02:08,670
That, if we've done it properly,

38
00:02:08,670 --> 00:02:11,266
will be as good as it can be and that we
can learn from.

39
00:02:11,266 --> 00:02:13,972
That's the goal.

40
00:02:13,972 --> 00:02:17,305
Now, because an argument consists of
premises and a conclusion.

41
00:02:17,305 --> 00:02:20,230
And the premises are supposed to be
related in the right way to

42
00:02:20,230 --> 00:02:24,500
the conclusion, there can be two main ways
for an argument to go wrong.

43
00:02:24,500 --> 00:02:27,760
Two main vices of argument you might say.

44
00:02:28,830 --> 00:02:31,192
The first is there might be something
wrong with the premises.

45
00:02:31,192 --> 00:02:34,320
In particular, they might be false, or

46
00:02:34,320 --> 00:02:35,740
at least one of them might be false.

47
00:02:37,129 --> 00:02:39,340
Second, there might be something bad about

48
00:02:39,340 --> 00:02:42,115
the relation between the premises and the
conclusion.

49
00:02:42,115 --> 00:02:46,325
The premises might fail to give a good
reason for the conclusion.

50
00:02:46,325 --> 00:02:51,230
Now, each of these problems is something
that we need to avoid.

51
00:02:51,230 --> 00:02:52,720
And when we do avoid them, we

52
00:02:52,720 --> 00:02:58,050
get the corresponding virtues, namely,
validity and soundness.

53
00:02:58,050 --> 00:02:59,640
And those are the two notions

54
00:02:59,640 --> 00:03:03,920
that we want to discuss in this lecture,
and the next.

55
00:03:03,920 --> 00:03:07,320
Let's begin with the relation between the
premises and the conclusion.

56
00:03:09,120 --> 00:03:12,010
What kind of relation between the premises
and the conclusion

57
00:03:12,010 --> 00:03:14,825
is good for an argument or makes an
argument good?

58
00:03:14,825 --> 00:03:16,990
Well, that depends.

59
00:03:16,990 --> 00:03:22,020
Some arguments are deductive, and others
are not.

60
00:03:22,020 --> 00:03:25,970
So let's focus for a moment on deductive
arguments.

61
00:03:25,970 --> 00:03:31,110
In Deductive Arguments, the conclusion is
suppose to follow from the premises.

62
00:03:31,110 --> 00:03:31,900
What does that mean?

63
00:03:31,900 --> 00:03:38,080
I mean, what does it mean for a conclusion
to follow from the premises?

64
00:03:38,080 --> 00:03:40,726
That's a really hard notion to pin down.

65
00:03:40,726 --> 00:03:44,400
So what logicians usually do and what
we're going to

66
00:03:44,400 --> 00:03:48,040
do, is focus instead on the notion of
validity.

67
00:03:48,040 --> 00:03:51,090
And the idea is that a deductive argument
is

68
00:03:51,090 --> 00:03:55,200
trying to structure itself, so that it's
valid.

69
00:03:55,200 --> 00:03:56,660
And we'll explain what validity is.

70
00:03:56,660 --> 00:03:58,710
But for now, I want to emphasize

71
00:03:58,710 --> 00:04:00,812
that we're only talking about deductive
arguments.

72
00:04:00,812 --> 00:04:03,970
There's going to be another class of
arguments called inductive

73
00:04:03,970 --> 00:04:06,850
arguments that we'll get to later in this
course.

74
00:04:06,850 --> 00:04:09,460
Where, they don't even pretend to be
valid.

75
00:04:09,460 --> 00:04:12,890
They don't even pretend that the
conclusion follows from the the premises.

76
00:04:13,940 --> 00:04:16,700
But just for simplicity, let's focus on
deductive

77
00:04:16,700 --> 00:04:18,240
arguments now.

78
00:04:18,240 --> 00:04:21,730
And the idea is that the deductive
argument, should

79
00:04:21,730 --> 00:04:24,820
be structured in such a way that it's
valid.

80
00:04:26,390 --> 00:04:29,820
Then the next question is, what's
validity?

81
00:04:29,820 --> 00:04:32,250
Let's start with a simple example.

82
00:04:32,250 --> 00:04:36,250
Suppose that you know Mary, but you don't
know her children.

83
00:04:37,940 --> 00:04:41,929
However, you do know that she has one
child who is

84
00:04:41,929 --> 00:04:46,750
pregnant, and you also know that only
daughters

85
00:04:46,750 --> 00:04:52,030
can become pregnant.
So, you have all that you need to know in

86
00:04:52,030 --> 00:04:57,120
order to draw further conclusion.
Mainly, Mary has at least one daughter.

87
00:04:59,270 --> 00:05:05,530
So here's the argument.
Mary has a child who is pregnant, only

88
00:05:05,530 --> 00:05:11,966
daughters can become pregnant, therefore,
Mary has at least one daughter.

89
00:05:11,966 --> 00:05:17,370
Now, if you think about it, there's just
no way, no possibility that both

90
00:05:17,370 --> 00:05:22,714
of those premises are true and the
conclusion's false.

91
00:05:22,714 --> 00:05:24,390
That is the feature that

92
00:05:24,390 --> 00:05:29,220
we're going to call Validity.
More generally, we can define validity

93
00:05:29,220 --> 00:05:34,060
in an argument.
so that an argument is valid, if and only

94
00:05:34,060 --> 00:05:40,420
if it's not possible for the premises to
be true and the conclusion false.

95
00:05:42,019 --> 00:05:46,190
That is, it's not possible for there to be
a situation where both of those hold.

96
00:05:46,190 --> 00:05:49,390
That is, a situation where the premises
are true and the

97
00:05:49,390 --> 00:05:50,814
conclusion is also false.

98
00:05:50,814 --> 00:05:56,510
Now that, might strike you as a pretty
simple notion.

99
00:05:56,510 --> 00:06:00,220
But actually that little word possible is
the problem.

100
00:06:00,220 --> 00:06:02,770
How do you tell what's possible or what's
not possible?

101
00:06:02,770 --> 00:06:07,510
Well, there's not mechanical solution to
that, and we'll

102
00:06:07,510 --> 00:06:10,260
struggle with that a little bit throughout
this course.

103
00:06:10,260 --> 00:06:13,900
But for now, since we're right at the
start, let's think of it this way.

104
00:06:14,910 --> 00:06:18,010
Is there any way for you to tell a
coherent

105
00:06:18,010 --> 00:06:23,050
story, where the premises are true, and
the conclusion's false.

106
00:06:23,050 --> 00:06:27,100
Can you describe a situation with that
combination of truth values?

107
00:06:27,100 --> 00:06:32,990
That is, the premises being true, and the
conclusion falls in the same situation.

108
00:06:32,990 --> 00:06:35,810
If you can tell a coherent story with that

109
00:06:35,810 --> 00:06:40,030
combination, then it's possible and the
argument's not valid.

110
00:06:40,030 --> 00:06:43,520
But if there's no way to tell a coherent
story.

111
00:06:43,520 --> 00:06:44,560
Where the premises are true and

112
00:06:44,560 --> 00:06:47,530
the conclusion's false, then the
argument's valid.

113
00:06:49,000 --> 00:06:50,910
Now let's try that test on our example.

114
00:06:52,100 --> 00:06:55,350
Mary has a child who is pregnant, only

115
00:06:55,350 --> 00:06:59,900
daughters can be pregnant, therefore, Mary
has a daughter.

116
00:06:59,900 --> 00:07:05,483
So, is there any way to tell a coherent
story where the two premises

117
00:07:05,483 --> 00:07:06,670
are true.

118
00:07:06,670 --> 00:07:08,990
That is, where Marry has a child who

119
00:07:08,990 --> 00:07:12,540
is pregnant, and only daughters can be
pregnant.

120
00:07:12,540 --> 00:07:16,530
But the conclusions false, Marry does not
have a daughter.

121
00:07:16,530 --> 00:07:23,170
Well just try, suppose Marry has only one
child and it is a son.

122
00:07:24,270 --> 00:07:27,792
There the conclusion is false, good.
What about that?

123
00:07:27,792 --> 00:07:32,040
But then, is that son pregnant?

124
00:07:32,040 --> 00:07:38,390
Well, if the son is not pregnant then the
first premise is false.

125
00:07:38,390 --> 00:07:40,112
Mary doesn't have a child who is pregnant.

126
00:07:40,112 --> 00:07:44,660
But if the son is pregnant somehow, don't
ask me how, but

127
00:07:44,660 --> 00:07:47,980
if the son is pregnant, then the second
premise is not true.

128
00:07:47,980 --> 00:07:52,600
It can't be true that only daughters can
be pregnant because this child is a son.

129
00:07:55,000 --> 00:07:57,690
Okay, what if Mary has two children?
Try that.

130
00:07:57,690 --> 00:07:59,290
Try to tell the story that way.

131
00:07:59,290 --> 00:08:01,400
Mary has a daughter and a son.

132
00:08:01,400 --> 00:08:05,515
Now, she's got a child who is pregnant,
the daughter.

133
00:08:05,515 --> 00:08:10,980
And only daughters can be pregnant, but
she has a son.

134
00:08:10,980 --> 00:08:12,830
Wait a minute, she's got a son and a
daughter.

135
00:08:12,830 --> 00:08:15,050
So now the conclusion's true.

136
00:08:15,050 --> 00:08:17,345
Because she does have a daughter even
though she also has a son.

137
00:08:17,345 --> 00:08:20,490
Well, oh wait, how about this one?

138
00:08:20,490 --> 00:08:24,620
What if Mary has a child who is

139
00:08:24,620 --> 00:08:30,410
biologically female, but sees himself

140
00:08:30,410 --> 00:08:35,626
as a male.
And so she sees that child as a male,

141
00:08:35,626 --> 00:08:41,320
but that child is pregnant, because after
all, they're biologically female.

142
00:08:42,760 --> 00:08:45,520
Now, are the premises true and the
conclusion false?

143
00:08:45,520 --> 00:08:49,046
Does that story make sense?
Wait a minute.

144
00:08:49,046 --> 00:08:55,309
Either, her child is a daughter or her
child is a son.

145
00:08:56,330 --> 00:09:01,618
Now, if it's a daughter and it's pregnant,
no problem,the conclusion's true.

146
00:09:02,680 --> 00:09:06,520
If it's a son, because that child sees

147
00:09:06,520 --> 00:09:10,270
himself as a male, then you've got a
choice.

148
00:09:11,549 --> 00:09:13,780
Well, what about the first premise?

149
00:09:13,780 --> 00:09:17,840
The first premise is going to be true, she
does have a child who is pregnant.

150
00:09:17,840 --> 00:09:20,640
But what about the second premise, only
daughters can be pregnant.

151
00:09:20,640 --> 00:09:24,030
Wait a minute, if that really is a son, if
we're going to call

152
00:09:24,030 --> 00:09:26,750
that a son, then, it's no longer true that
only daughters can be pregnant.

153
00:09:27,950 --> 00:09:31,780
So, now the second premise is false.
So, try it again.

154
00:09:31,780 --> 00:09:36,430
Try it with, you know, sex changes and try
it with hermaphrodites,

155
00:09:36,430 --> 00:09:40,710
tell the story any way you want about
Mary's children.

156
00:09:40,710 --> 00:09:48,785
And there's no way that both premises come
out true when the conclusion's false.

157
00:09:48,785 --> 00:09:52,210
That shows that the argument is valid.

158
00:09:52,210 --> 00:09:57,570
It might be just that we can't imagine the
coherent story which makes it invalid.

159
00:09:57,570 --> 00:09:59,300
But the fact that we've tried hard and

160
00:09:59,300 --> 00:10:01,560
looked at all the possibilities we can
think of.

161
00:10:01,560 --> 00:10:06,500
At least gives us a good reason to think,
that this argument is valid.

162
00:10:06,500 --> 00:10:09,200
Now, some people like to think of it in
the reverse direction.

163
00:10:10,480 --> 00:10:17,110
They say, let's imagine that the
conclusion's false, and then, if it has

164
00:10:17,110 --> 00:10:21,790
to be the case that at least one of the
premises is false, the argument's valid.

165
00:10:21,790 --> 00:10:24,730
Then you could define validity as, it's

166
00:10:24,730 --> 00:10:26,950
necessarily the case that if the
conclusion

167
00:10:26,950 --> 00:10:29,980
is false, one of the premises is false.

168
00:10:29,980 --> 00:10:33,552
Or, in every possible situation, if the

169
00:10:33,552 --> 00:10:36,330
conclusion's false, one of the premises is
false.

170
00:10:36,330 --> 00:10:40,135
We can apply this new account of validity
to the same old example.

171
00:10:40,135 --> 00:10:46,450
It's got to be the case.
That if Mary didn't have a daughter, then

172
00:10:46,450 --> 00:10:52,630
she doesn't have a child who was pregnant
or else, there are at least some children

173
00:10:52,630 --> 00:10:54,370
who are pregnant who are not daughters.

174
00:10:54,370 --> 00:10:57,110
So in other words, this case you're
reasoning back from the falsehood of

175
00:10:57,110 --> 00:11:00,490
the conclusion to at least one of the
premises has to be false.

176
00:11:00,490 --> 00:11:04,330
Where as in the earlier definition, you
were saying it's not possible in

177
00:11:04,330 --> 00:11:08,780
the situations where the premises are true
for the conclusion to be false.

178
00:11:08,780 --> 00:11:11,337
You can look at it either way, either
direction.

179
00:11:11,337 --> 00:11:15,500
Just pick the one that works for you and
go with that definition.

180
00:11:15,500 --> 00:11:17,800
Because in the end, the two definitions

181
00:11:17,800 --> 00:11:18,800
are equivalent.

182
00:11:18,800 --> 00:11:20,995
It's just a matter of what's going to help
you

183
00:11:20,995 --> 00:11:25,130
understand which arguments are valid and
which ones are not.

184
00:11:26,300 --> 00:11:29,100
In addition to understanding what validity
is, it's

185
00:11:29,100 --> 00:11:32,670
also very important to understand what
validity is not.

186
00:11:32,670 --> 00:11:36,790
A lot of people get confused by the notion
of validity in this context,

187
00:11:36,790 --> 00:11:42,980
because they're thinking that, to call an
argument valid, must be to call it good,

188
00:11:42,980 --> 00:11:46,430
right?
You call a driver's license valid.

189
00:11:46,430 --> 00:11:51,660
When its good in the eyes of the law, but
that's not what we're talking about here.

190
00:11:51,660 --> 00:11:55,970
The notion of validity is getting used by
logitions here as a technical notion.

191
00:11:55,970 --> 00:11:59,370
And its very, very, very important to
remember, that to

192
00:11:59,370 --> 00:12:02,890
call an argument valid is not to call it
good.

193
00:12:03,940 --> 00:12:08,210
For some arguments like deductive
arguments, being valid might be necessary

194
00:12:08,210 --> 00:12:10,780
for them to be good, but it's not enough.

195
00:12:10,780 --> 00:12:13,910
And we'll see a lot of examples of that
later on.

196
00:12:13,910 --> 00:12:18,890
The second point about what validity is
not, is that validity does not

197
00:12:18,890 --> 00:12:25,190
depend on whether the premises and the
conclusion are actually true or false.

198
00:12:25,190 --> 00:12:27,760
Instead, it depends on what's possible.

199
00:12:27,760 --> 00:12:30,290
Whether there's a certain combination,
true premises

200
00:12:30,290 --> 00:12:33,010
and a false conclusion, that's even
possible.

201
00:12:34,800 --> 00:12:38,960
So, whether the premise is actually true
in

202
00:12:38,960 --> 00:12:41,310
the actual world is not what's at issue.

203
00:12:41,310 --> 00:12:44,710
And we can see this by seeing that

204
00:12:44,710 --> 00:12:49,520
some arguments with false premises can
still be valid.

205
00:12:49,520 --> 00:12:53,970
And some arguments with true conclusions
can be invalid.

206
00:12:53,970 --> 00:12:55,610
So let's look at some examples of that.

207
00:12:56,800 --> 00:13:00,224
Indeed, there are four possibilities,
because remember,

208
00:13:00,224 --> 00:13:05,866
the conclusion could be true or false, and
premises,

209
00:13:05,866 --> 00:13:11,530
could be all true or at least one false.
So, we've got four

210
00:13:11,530 --> 00:13:17,325
possibilities, and all of those are
possible, except for one.

211
00:13:17,325 --> 00:13:20,750
The one combination that's not possible
for valid

212
00:13:20,750 --> 00:13:24,115
arguments is True Premises and a False
Conclusion.

213
00:13:24,115 --> 00:13:25,320
But if you've got

214
00:13:25,320 --> 00:13:29,960
True Premises and a True Conclusion, it
might be valid, it might not.

215
00:13:29,960 --> 00:13:32,000
If you've got false premises in a true

216
00:13:32,000 --> 00:13:35,060
conclusion, it might be valid it might
not.

217
00:13:35,060 --> 00:13:37,360
If you've got false premises, and a false

218
00:13:37,360 --> 00:13:39,340
conclusion, it might be valid it might
not.

219
00:13:40,710 --> 00:13:44,270
So, let's look at some examples of each of
those possibilities in order

220
00:13:44,270 --> 00:13:47,630
to better understand the relation between
premises

221
00:13:47,630 --> 00:13:50,460
and conclusion, that exists when the
argument

222
00:13:50,460 --> 00:13:51,970
is valid.

223
00:13:51,970 --> 00:13:55,030
It's hard to give examples with true
premises

224
00:13:55,030 --> 00:13:58,120
and false conclusion, or any of these
other combinations.

225
00:13:58,120 --> 00:14:01,370
When the truth is controversial.

226
00:14:01,370 --> 00:14:03,630
So we're going to have a really simple
example, and

227
00:14:03,630 --> 00:14:07,250
we're going to start just by stipulating
what the facts are.

228
00:14:07,250 --> 00:14:15,470
We're going to assume that all Ford cars
have four tires, but some Ford

229
00:14:15,470 --> 00:14:20,530
cars do not have four doors.
We're also going to

230
00:14:20,530 --> 00:14:26,640
assume that Henry's car is a Ford that has
four doors.

231
00:14:26,640 --> 00:14:31,625
And Jane's car is a Chrysler, it has only
two doors, not four doors.

232
00:14:31,625 --> 00:14:34,980
And we're just going to take those facts
for

233
00:14:34,980 --> 00:14:38,760
granted and assume that that's the
situation we're talking about.

234
00:14:38,760 --> 00:14:40,670
And then, we can give examples of

235
00:14:40,670 --> 00:14:43,520
all the combinations that we discussed
before.

236
00:14:44,850 --> 00:14:47,830
Let's begin with true premises and a true
conclusion.

237
00:14:47,830 --> 00:14:52,940
Here's an example, all Ford cars have four
ties.

238
00:14:52,940 --> 00:14:54,180
Henry's car is a Ford.

239
00:14:56,000 --> 00:15:00,012
So Henry's car had, has four tires.
Are the premises true?

240
00:15:00,012 --> 00:15:05,580
Well yeah, our assumptions tell us that
all Ford cars have four tires.

241
00:15:05,580 --> 00:15:09,760
And then Henry's car is a Ford.
What about the conclusion, is that true?

242
00:15:09,760 --> 00:15:10,580
Sure.

243
00:15:10,580 --> 00:15:14,630
That's another one of our assumptions.
Henry's car has four tires.

244
00:15:16,890 --> 00:15:19,365
But, the fact that the premises are true
and

245
00:15:19,365 --> 00:15:21,670
conclusion's also true, doesn't yet tell
us it's valid.

246
00:15:21,670 --> 00:15:24,545
Because, to be valid, it has to be
impossible

247
00:15:24,545 --> 00:15:27,500
for the premises to be true and the
conclusion false.

248
00:15:27,500 --> 00:15:29,750
There has to be no coherent story where

249
00:15:29,750 --> 00:15:33,210
the premises are true and the conclusions
false.

250
00:15:33,210 --> 00:15:34,420
So what about this case?

251
00:15:34,420 --> 00:15:37,810
Is it valid?
Well, is it possible?

252
00:15:37,810 --> 00:15:41,710
Is there any way to tell a coherent story
where,

253
00:15:41,710 --> 00:15:44,680
all Ford cars have four tires?

254
00:15:44,680 --> 00:15:51,250
Henry's car is a Ford, but Henry's car
does not have four tires just try.

255
00:15:51,250 --> 00:15:55,717
One way to tell not going to able to do
it, is to reason backwards.

256
00:15:55,717 --> 00:16:01,210
Assume that the conclusion falls, that
Henry's car does not

257
00:16:01,210 --> 00:16:06,840
have four tires, may be it got six tires.
Well, then, how could both premises

258
00:16:06,840 --> 00:16:08,120
be true?

259
00:16:08,120 --> 00:16:09,910
If Henry's car is still a Ford and it

260
00:16:09,910 --> 00:16:12,955
has six tires, then the first premise is
not true.

261
00:16:12,955 --> 00:16:15,310
Because the first premise says that all
Ford

262
00:16:15,310 --> 00:16:18,010
cars have four tires, and Henry's car
would

263
00:16:18,010 --> 00:16:22,400
then under that assumption be a Ford car

264
00:16:22,400 --> 00:16:24,840
without four tires, but with six tires
instead.

265
00:16:24,840 --> 00:16:26,189
So the first premise would be false.

266
00:16:27,330 --> 00:16:32,300
Well, another possibility is that Henry's
car has six tires and it's not a Ford.

267
00:16:32,300 --> 00:16:36,450
And then you can have the first premise
true, all Ford's have four tires.

268
00:16:36,450 --> 00:16:41,710
But you couldn't have the second premise
true, namely that Henry's car is a Ford.

269
00:16:41,710 --> 00:16:46,430
So there's no way when the conclusion is
false, for both

270
00:16:46,430 --> 00:16:51,415
premises to be true and that shows you
that the argument is valid.

271
00:16:51,415 --> 00:16:57,384
Nonetheless, there are other examples
where

272
00:16:57,384 --> 00:17:01,740
the premises are true and the conclusion's
true, but the argument is not valid.

273
00:17:01,740 --> 00:17:03,790
Instead it's invalid.

274
00:17:03,790 --> 00:17:09,310
Here's an example of that combination.
All Ford cars have four tires.

275
00:17:09,310 --> 00:17:15,020
Henry's car has four tires.
Therefore Henry's car is a Ford.

276
00:17:15,020 --> 00:17:18,920
Now in this new argument, are all the
premises true?

277
00:17:18,920 --> 00:17:22,840
Yes, the first premises says, all Ford
cars have four tires.

278
00:17:22,840 --> 00:17:25,640
And that's true by our assumption, the

279
00:17:25,640 --> 00:17:29,210
second premise says Henry's car has four
tires.

280
00:17:29,210 --> 00:17:31,490
And that's also true by our assumptions.

281
00:17:31,490 --> 00:17:33,060
And is the conclusion true?

282
00:17:33,060 --> 00:17:36,165
Yes, our assumptions also tell us that
Henry's car is a Ford.

283
00:17:36,165 --> 00:17:40,389
But, is it possible, is there any way to
tell a

284
00:17:40,389 --> 00:17:46,056
coherent story where, those premises are
true and the conclusion is false.

285
00:17:46,056 --> 00:17:46,688
Yes,

286
00:17:46,688 --> 00:17:51,680
absolutely.
All that has to happen is Jane

287
00:17:51,680 --> 00:17:56,390
and Henry switch cars.
Then, the first premises can be true,

288
00:17:56,390 --> 00:18:03,720
because all Ford cars have four tires, and
the second premise is going to be true.

289
00:18:03,720 --> 00:18:07,480
Because Henry's car has four tires, of
course now

290
00:18:07,480 --> 00:18:09,720
it's a Chrysler, because he got it from
Jane.

291
00:18:11,110 --> 00:18:11,830
But the conclusion

292
00:18:11,830 --> 00:18:13,340
can be false Henry's car is not a

293
00:18:13,340 --> 00:18:16,160
Ford, because Ford and Chrysler are
different companies.

294
00:18:16,160 --> 00:18:18,550
So if he switches cars with Jane and he

295
00:18:18,550 --> 00:18:20,555
has a Chrysler, then he doesn't have a
Ford.

296
00:18:20,555 --> 00:18:24,320
He's car is not a Ford, okay?

297
00:18:24,320 --> 00:18:27,720
So now you've got a situation, where

298
00:18:27,720 --> 00:18:30,170
the premises are true, and the conclusion
false.

299
00:18:30,170 --> 00:18:33,650
It's not an actual situation, but it's a
possible situation.

300
00:18:33,650 --> 00:18:37,100
You can tell a coherent story, where the
premises are true, and

301
00:18:37,100 --> 00:18:38,040
the conclusion's false.

302
00:18:38,040 --> 00:18:42,210
And that tells you that the argument is
invalid.

303
00:18:42,210 --> 00:18:47,130
Next, let's consider an example with false
premises and a true conclusion.

304
00:18:50,000 --> 00:18:55,940
Premise one, all Fords have four doors.
Premise two, Henry's car is a Ford.

305
00:18:57,100 --> 00:19:02,955
Conclusion, Henry's car has four doors.
Is the first premise true?

306
00:19:02,955 --> 00:19:06,150
No, it's not true that all Fords have four
doors.

307
00:19:07,760 --> 00:19:09,370
Our assumptions tell us that.

308
00:19:10,780 --> 00:19:14,620
Second, is Henry's car a Ford?
That's true.

309
00:19:15,850 --> 00:19:17,970
So one of the premises is false and the
other one is true.

310
00:19:17,970 --> 00:19:21,870
That means they're not all true, and the
conclusion, is that true?

311
00:19:21,870 --> 00:19:22,375
Yes.

312
00:19:22,375 --> 00:19:25,788
It is true that Henry's car has four
doors.

313
00:19:25,788 --> 00:19:29,680
But remember, the fact that, that's
actually the

314
00:19:29,680 --> 00:19:31,940
case doesn't tell us whether or not it's
valid.

315
00:19:31,940 --> 00:19:33,830
So is it valid?

316
00:19:33,830 --> 00:19:36,020
That depends on when it's whether it's
possible for

317
00:19:36,020 --> 00:19:38,979
the premises to be true and the conclusion
false.

318
00:19:40,800 --> 00:19:43,750
Premises aren't actually true, but is
there a possible

319
00:19:43,750 --> 00:19:46,490
story that you could tell that would be
coherent.

320
00:19:46,490 --> 00:19:49,684
Where the premises are true but the
conclusion, false.

321
00:19:49,684 --> 00:19:53,024
That's the test of validity, so lets apply
it to this case.

322
00:19:53,024 --> 00:19:59,420
Well, just imagine that the conclusion is
false.

323
00:19:59,420 --> 00:20:04,496
That, Henry's car does not have four
doors, it's only got two doors.

324
00:20:04,496 --> 00:20:05,288
Then,

325
00:20:05,288 --> 00:20:10,155
there is really only two possibilities,
Its either a Ford or it is not a Ford.

326
00:20:10,155 --> 00:20:14,590
If it is a Ford then the first premise is
false.

327
00:20:14,590 --> 00:20:17,170
It is not true that all Fords have four
doors.

328
00:20:18,930 --> 00:20:22,290
But if Henry's car is not a Ford, then the
second

329
00:20:22,290 --> 00:20:26,740
premise is false, because it says that
Henry's car is a Ford.

330
00:20:26,740 --> 00:20:31,490
So there's no coherent way in which it
could possibly be true.

331
00:20:31,490 --> 00:20:34,390
That both of these premises are true and
the conclusion's false.

332
00:20:34,390 --> 00:20:38,320
So this argument's valid, and notice that
that shows, that an

333
00:20:38,320 --> 00:20:43,050
argument can be valid, even though it's
got a false premise.

334
00:20:43,050 --> 00:20:46,590
Now, you might be thinking to yourself,
this is crazy.

335
00:20:46,590 --> 00:20:50,680
How can an argument be valid, when one of
its premises is false?

336
00:20:50,680 --> 00:20:53,325
I mean, an argument is not good when it's
premises are false.

337
00:20:53,325 --> 00:20:55,929
But notice what that does.

338
00:20:56,960 --> 00:21:01,720
That confuses the notion of valid, like in
a valid drivers license, where to

339
00:21:01,720 --> 00:21:06,530
be valid is good, with the technical
notion of validity that we're using here.

340
00:21:06,530 --> 00:21:10,160
The technical notion of validity that
we're using here, has

341
00:21:10,160 --> 00:21:13,760
to do with the relation between the
premises and the conclusion.

342
00:21:13,760 --> 00:21:18,070
And in particular it has to do with
possibilities.

343
00:21:18,070 --> 00:21:20,740
And not with the actual falsehood of the
premise.

344
00:21:20,740 --> 00:21:22,040
So what we have to ask

345
00:21:22,040 --> 00:21:25,369
ourselves is, what would happen if it
really were true?

346
00:21:26,630 --> 00:21:29,170
That all Fords have four doors.

347
00:21:29,170 --> 00:21:32,590
It's not true, in the actual world, but
we're concerned with possibility.

348
00:21:32,590 --> 00:21:40,910
And if all Fords did have four doors, and,
if Henry's car was a Ford,

349
00:21:40,910 --> 00:21:47,410
then it would have to have four doors.
So that possibility

350
00:21:47,410 --> 00:21:49,130
of the premise being true, even though

351
00:21:49,130 --> 00:21:52,930
it's not, is what's crucial for
determining validity.

352
00:21:54,000 --> 00:21:58,170
Because it's not possible for the premises
to be true and the conclusion false.

353
00:21:58,170 --> 00:22:01,610
That makes it valid in our technical sense
even if it's

354
00:22:01,610 --> 00:22:06,900
not valid in the common sense notion of
validity as goodness.

355
00:22:06,900 --> 00:22:09,270
We're not saying the arguments are good
argument, we're

356
00:22:09,270 --> 00:22:12,890
saying that is meets this technical
definition of validity that

357
00:22:12,890 --> 00:22:14,445
logicians use.

358
00:22:14,445 --> 00:22:17,960
Now the only combination of truth values
in premise

359
00:22:17,960 --> 00:22:21,530
and conclusion, that you cannot get with a
valid argument.

360
00:22:21,530 --> 00:22:25,402
Is to have true premises and a false
conclusion.

361
00:22:25,402 --> 00:22:28,456
So here's an example of that.

362
00:22:28,456 --> 00:22:33,546
Premise one, some Ford cars do not have
four doors.

363
00:22:33,546 --> 00:22:39,100
Premise two, Henry's car is a Ford.
Conclusion,

364
00:22:40,300 --> 00:22:42,100
Henry's car does not have four doors.

365
00:22:43,770 --> 00:22:48,080
The premises by our assumptions are both
true, and the conclusions false.

366
00:22:48,080 --> 00:22:50,470
And it's not valid, because it's easy to
see how it might

367
00:22:50,470 --> 00:22:54,980
be possible for the premises to be true
and the conclusion false.

368
00:22:54,980 --> 00:22:56,300
It's simple.

369
00:22:56,300 --> 00:23:00,640
Even if some Fords don't have four doors,
Henry's car

370
00:23:00,640 --> 00:23:03,770
is one of the Fords that does have four
doors.

371
00:23:03,770 --> 00:23:05,510
And then, both the premises

372
00:23:05,510 --> 00:23:07,790
can be true, and the conclusions false.

373
00:23:08,900 --> 00:23:10,470
So that's how you can get an invalid

374
00:23:10,470 --> 00:23:13,870
argument with true premises and a false
conclusion.

375
00:23:13,870 --> 00:23:15,415
But you don't even really need that.

376
00:23:15,415 --> 00:23:18,630
Look, every argument that has true
premises

377
00:23:18,630 --> 00:23:21,900
and a false conclusion has to be invalid.

378
00:23:21,900 --> 00:23:25,550
Because if it does in fact actually have
true premises and a false

379
00:23:25,550 --> 00:23:30,540
conclusion, then it's possible for it to
have true premises and a false conclusion.

380
00:23:30,540 --> 00:23:32,660
So you can know right off the bat, that
every

381
00:23:32,660 --> 00:23:36,108
argument with true premises and a false
conclusion is invalid.

382
00:23:36,108 --> 00:23:40,210
What you can't know is for the other
combinations, then you

383
00:23:40,210 --> 00:23:44,740
have to think through what's possible,
instead of simply, what's actual.

384
00:23:44,740 --> 00:23:48,710
We haven't been through all of the
possibilities, but we have seen

385
00:23:48,710 --> 00:23:53,070
that you can have invalid arguments with
true premises and true conclusions.

386
00:23:53,070 --> 00:23:54,495
And you can have valid arguments,

387
00:23:54,495 --> 00:23:58,380
with false premises and true conclusions.

388
00:23:58,380 --> 00:24:01,770
And we've got a little table that shows us
the other possibilities.

389
00:24:01,770 --> 00:24:04,728
Instead of going through all of those
possibilities myself.

390
00:24:04,728 --> 00:24:08,210
I think it would be better if you did

391
00:24:08,210 --> 00:24:11,490
a few excercises, and that'll make sure
that you

392
00:24:11,490 --> 00:24:15,290
understand this notion of validity, before
we go on

393
00:24:15,290 --> 00:24:19,026
and try to show how validity is related to
soundness.

