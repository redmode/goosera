1
00:00:04,050 --> 00:00:05,720
In the last several lectures, we learned

2
00:00:05,720 --> 00:00:09,290
about the truth functional connectives,
conjunction, and disjunction.

3
00:00:09,290 --> 00:00:11,910
And we learned about the truth functional
operator and negation.

4
00:00:13,190 --> 00:00:14,810
Now today, I would like to make a point

5
00:00:14,810 --> 00:00:19,850
about how these truth functional devices,
these connectives and operators.

6
00:00:19,850 --> 00:00:25,150
Can be combined so as to create larger
propositions out of smaller

7
00:00:25,150 --> 00:00:30,300
propositions.
So, consider this conjunction.

8
00:00:30,300 --> 00:00:38,019
For lunch, I'm going to eat this and this.
That's a conjunction.

9
00:00:38,019 --> 00:00:40,500
It's a conjunction of the proposition that
for lunch I'm going to

10
00:00:40,500 --> 00:00:44,680
eat this, and the proposition that for
lunch I'm going to eat this.

11
00:00:46,420 --> 00:00:52,020
Now if we apply negation to that
conjunction, what are we saying?

12
00:00:52,020 --> 00:00:55,350
We're saying it's not the case that for
lunch I'm going to

13
00:00:55,350 --> 00:00:57,870
eat this and this.

14
00:00:57,870 --> 00:01:01,340
Now that leaves it open, that maybe for
lunch, I'm just going to

15
00:01:01,340 --> 00:01:06,760
have one of these two things, maybe just
this thing, whatever that is.

16
00:01:08,290 --> 00:01:12,760
So if I negate a conjunction, all I'm
doing is saying,

17
00:01:12,760 --> 00:01:15,516
it's not the case that both of the two
conjuncts are true.

18
00:01:17,390 --> 00:01:22,220
But suppose I create a disjunction, out of
the two propositions,

19
00:01:22,220 --> 00:01:25,630
that for lunch I'm going to eat this, and
for lunch I'm going to eat this, right?

20
00:01:25,630 --> 00:01:27,190
I create a disjunction as follows.

21
00:01:27,190 --> 00:01:32,880
I say, for lunch, I'm either going to eat
this, or this.

22
00:01:32,880 --> 00:01:34,890
Now, if I negate that.

23
00:01:34,890 --> 00:01:36,560
If I say that it's not the case that

24
00:01:36,560 --> 00:01:39,490
for lunch I'm either going to eat this or
this.

25
00:01:39,490 --> 00:01:42,550
Then, I'm saying something that rules out
that

26
00:01:42,550 --> 00:01:44,950
I'm going to eat either of them for lunch.

27
00:01:44,950 --> 00:01:47,580
It's not just that I'm not going to eat
both of them,

28
00:01:47,580 --> 00:01:50,620
I'm not going to eat either of them.
Right?

29
00:01:50,620 --> 00:01:58,696
My lunch is not going to involve this, and
it's also not going to involve this.

30
00:01:58,696 --> 00:02:03,180
So, negating a conjunction gives us

31
00:02:03,180 --> 00:02:06,610
a different result than negating a
disjunction.

32
00:02:06,610 --> 00:02:10,710
And that's exactly what we should expect
if we look at the truth table.

33
00:02:10,710 --> 00:02:12,600
That results from applying negation

34
00:02:12,600 --> 00:02:17,070
to a conjunction, and applying negation to
a disjunction.

35
00:02:17,070 --> 00:02:19,310
Remember, when we apply negation to a

36
00:02:19,310 --> 00:02:24,680
conjunction we're applying negation to a
proposition that's

37
00:02:24,680 --> 00:02:27,000
true only when both of its two conjuncts

38
00:02:27,000 --> 00:02:28,920
are true, in every other scenario it's
false.

39
00:02:30,140 --> 00:02:33,970
So, if we apply negation to that
proposition what do we get?

40
00:02:33,970 --> 00:02:37,860
We get a proposition that's going to be
true in

41
00:02:37,860 --> 00:02:40,000
three out of four possible scenarios.

42
00:02:40,000 --> 00:02:43,230
It's going to be true whenever both of the
conjuncts are false.

43
00:02:43,230 --> 00:02:46,990
And it's going to be true whenever either
one of the contents is false.

44
00:02:46,990 --> 00:02:50,670
The only situation in which that
proposition is going to be false.

45
00:02:50,670 --> 00:02:54,230
Is when both of the conjuncts are true.

46
00:02:54,230 --> 00:02:57,610
But now, contrast that with applying
negation to a disjunction.

47
00:02:57,610 --> 00:03:00,330
When we apply a negation to a disjunction

48
00:03:00,330 --> 00:03:03,790
we're applying negation to a proposition
that's true

49
00:03:03,790 --> 00:03:08,340
when ever either one of the disjuncts is
true.

50
00:03:08,340 --> 00:03:13,880
So we end up getting a proposition that's
flase.

51
00:03:13,880 --> 00:03:15,920
In three out of four possible scenarios.

52
00:03:15,920 --> 00:03:21,530
It's false so long as either one of the
disjuncts is true.

53
00:03:21,530 --> 00:03:24,900
So, applying the negation to a disjunction
is going to give

54
00:03:24,900 --> 00:03:28,850
us a different result than applying
negation to a conjunction, and we

55
00:03:28,850 --> 00:03:30,300
can see that by looking at the

56
00:03:30,300 --> 00:03:34,894
truth tables for negation, disjunction and
conjunction.

