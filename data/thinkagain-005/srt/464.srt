1
00:00:03,490 --> 00:00:07,870
We received a wonderful pair of arguments
from

2
00:00:07,870 --> 00:00:10,710
a foreign language high school in South
Korea.

3
00:00:12,100 --> 00:00:14,640
One of the groups argued that smartphones
should be

4
00:00:14,640 --> 00:00:18,310
banned in class, and the others argued
that it shouldn't.

5
00:00:18,310 --> 00:00:21,350
So we can look at how they conflict with

6
00:00:21,350 --> 00:00:23,180
each other, and what we need to do about
that.

7
00:00:25,190 --> 00:00:27,440
So the first argument, which is try to
show

8
00:00:27,440 --> 00:00:31,420
that smartphones should be banned during
class, runs like this.

9
00:00:32,450 --> 00:00:37,630
Many students are using smartphones during
class, and this causes many problems.

10
00:00:37,630 --> 00:00:40,009
That kind of sets it up.
That's what the problem is.

11
00:00:42,160 --> 00:00:43,720
But why?

12
00:00:43,720 --> 00:00:46,370
Well, they are a lot of addictive apps
like, social

13
00:00:46,370 --> 00:00:52,510
networking and games that make students
check their smartphones continuously.

14
00:00:52,510 --> 00:00:56,100
I assume they're talking about checking
them continuously during class.

15
00:00:56,100 --> 00:00:58,920
And it's probably not really continuously.

16
00:00:58,920 --> 00:01:03,030
Like, not all the time.
But way too often.

17
00:01:03,030 --> 00:01:06,360
I know just what they're talking about,
happens in my classes.

18
00:01:08,002 --> 00:01:13,860
Checking smartphones frequently prevents
students from concentrating in class.

19
00:01:13,860 --> 00:01:15,990
Now, that tells you what the problem is.

20
00:01:15,990 --> 00:01:19,420
There's nothing wrong with smartphones in
themselves, if you can

21
00:01:19,420 --> 00:01:21,480
do that and still get something out of the
class.

22
00:01:21,480 --> 00:01:27,060
But if it prevents students from
concentrating in class, that's a problem.

23
00:01:27,060 --> 00:01:30,370
Cause that's what the class is for.
So the students can learn.

24
00:01:30,370 --> 00:01:33,010
And notice they don't say other students.
It's also

25
00:01:33,010 --> 00:01:36,425
the person using the smartphone.
They're both distracted.

26
00:01:36,425 --> 00:01:39,260
They're both prevented from concentrating
in class.

27
00:01:39,260 --> 00:01:39,990
That's a problem.

28
00:01:41,110 --> 00:01:44,090
Next, behaviors that prevent students from

29
00:01:44,090 --> 00:01:47,210
concentrating during class, should be
banned.

30
00:01:47,210 --> 00:01:49,170
We just found out that smartphones do
that.

31
00:01:49,170 --> 00:01:53,410
Therefore, smartphones should be banned
during class.

32
00:01:54,640 --> 00:01:55,400
Sounds pretty good.

33
00:01:55,400 --> 00:01:59,080
Of course, there are questions you could
raise.

34
00:01:59,080 --> 00:02:02,560
But notice it, the fact that the apps are
addictive.

35
00:02:02,560 --> 00:02:04,840
That explains why once you've got them on
your phone.

36
00:02:04,840 --> 00:02:06,630
You can't stop yourself from using them.

37
00:02:06,630 --> 00:02:09,170
And you're going to use them even in
class, when you know you shouldn't.

38
00:02:10,290 --> 00:02:11,200
Right?

39
00:02:11,200 --> 00:02:12,970
You know you should only use them outside
of class,

40
00:02:12,970 --> 00:02:15,340
but you end up abusing them inside class
cause you're addicted.

41
00:02:16,940 --> 00:02:19,360
It also says, behaviors that prevent
students

42
00:02:19,360 --> 00:02:22,030
from concentrating in class should be
banned.

43
00:02:22,030 --> 00:02:23,870
Eh, you could base questions about that.

44
00:02:24,930 --> 00:02:25,300
Right?

45
00:02:25,300 --> 00:02:27,980
Because things like hey going to the
bathroom.

46
00:02:29,280 --> 00:02:33,160
Going to the bathroom during class might
stop other students from

47
00:02:33,160 --> 00:02:37,440
concentrating, learning quite as much,
but, you gotta go, you gotta go.

48
00:02:37,440 --> 00:02:40,890
And so, we have to allow those.

49
00:02:40,890 --> 00:02:44,040
So you might want to ask the
qualifications about these are activities

50
00:02:44,040 --> 00:02:46,580
that distract other students that aren't

51
00:02:46,580 --> 00:02:50,040
really necessary and doing this smart
phoning

52
00:02:50,040 --> 00:02:52,790
during class is probably not really
necessary.

53
00:02:52,790 --> 00:02:56,770
It doesn't contribute to anybody's
educational experience.

54
00:02:56,770 --> 00:02:59,150
So, you can fix it up a little bit.

55
00:02:59,150 --> 00:03:01,320
But what I want to focus on instead of
trying

56
00:03:01,320 --> 00:03:04,490
to fix up every little detail like that,
is the structure.

57
00:03:06,850 --> 00:03:10,030
The two main steps, in this argument.

58
00:03:10,030 --> 00:03:14,098
First, there are a lot of addictive
applications or apps.

59
00:03:14,098 --> 00:03:16,100
If there are lots of apps then if
smartphones

60
00:03:16,100 --> 00:03:19,440
are not banned students will check their
Smartphones frequently.

61
00:03:19,440 --> 00:03:21,690
So if they're not banned students are
going to check them.

62
00:03:22,970 --> 00:03:24,636
Then if students check their smartphones
frequently then

63
00:03:24,636 --> 00:03:26,340
students are going to be prevented from
concentrating.

64
00:03:26,340 --> 00:03:26,994
Therefore, if they're not banned students

65
00:03:26,994 --> 00:03:29,070
are going to be prevented from
concentrating.

66
00:03:35,000 --> 00:03:38,160
And if not, banning something results in
students

67
00:03:38,160 --> 00:03:42,230
being prevented from concentrating, then
the schools ought to

68
00:03:42,230 --> 00:03:45,170
ban them because after all the point of
the

69
00:03:45,170 --> 00:03:47,630
school is education, and that's standing
in the way.

70
00:03:47,630 --> 00:03:51,790
Therefore, smartphones should be banned
during class.

71
00:03:51,790 --> 00:03:53,710
So now we've got a reconstruction.

72
00:03:53,710 --> 00:04:00,670
Where the steps are valid and the only
point I want to make for now is

73
00:04:01,980 --> 00:04:06,130
that it's got a certain kind of structure.
Is it a linear structure?

74
00:04:06,130 --> 00:04:09,370
Or is it a branching structure?
Or is it a joint structure?

75
00:04:10,380 --> 00:04:15,030
It's a linear structure.
Because the conclusion of the first bit.

76
00:04:15,030 --> 00:04:19,810
Namely number three, if smart phones are
not banned during class then the students

77
00:04:19,810 --> 00:04:25,360
will check their smartphones frequently in
class becomes a premise in the next step.

78
00:04:25,360 --> 00:04:27,130
Which leads to the conclusion five which

79
00:04:27,130 --> 00:04:30,970
becomes a premise in the argument for the
final conclusion seven.

80
00:04:30,970 --> 00:04:32,790
So this is a linear structure.

81
00:04:34,600 --> 00:04:39,500
Now, let's look at the argument on the
other side.

82
00:04:39,500 --> 00:04:42,390
The other group of high school students
from the same high school in

83
00:04:42,390 --> 00:04:45,830
South Korea argued for the opposite
conclusion,

84
00:04:46,900 --> 00:04:49,070
that smartphones should be allowed in
class.

85
00:04:49,070 --> 00:04:51,160
Of course, if they're allowed, they're not
banned.

86
00:04:51,160 --> 00:04:52,220
So,

87
00:04:52,220 --> 00:04:54,780
they're disagreeing with their
schoolmates.

88
00:04:56,210 --> 00:04:58,060
And here's their argument.

89
00:04:58,060 --> 00:05:02,070
Many people say that smartphones should be
banned in schools, but they shouldn't.

90
00:05:03,365 --> 00:05:05,740
Smartphones have apps that help you

91
00:05:05,740 --> 00:05:10,140
study, such as dictionaries for foreign
languages,

92
00:05:10,140 --> 00:05:13,379
and these students are in a foreign
language high school so that's important.

93
00:05:14,900 --> 00:05:17,290
I believe that anything that can help
students

94
00:05:17,290 --> 00:05:19,690
learn should be allowed in schools.

95
00:05:19,690 --> 00:05:22,698
Anything that can help students learn
should be allowed in school.

96
00:05:22,698 --> 00:05:27,240
Additionally, banning smartphones can
result in lack

97
00:05:27,240 --> 00:05:30,820
of ways for students to communicate during
emergencies.

98
00:05:30,820 --> 00:05:32,760
Just imagine there's an emergency and you
can't

99
00:05:32,760 --> 00:05:34,980
tell the other students that there is an
emergency.

100
00:05:34,980 --> 00:05:36,620
People might get stuck in the school.

101
00:05:36,620 --> 00:05:39,000
They might not know that they need to
leave.

102
00:05:40,320 --> 00:05:42,260
And that could lead to real problems.

103
00:05:43,430 --> 00:05:48,460
So because smartphones, can help students
study.

104
00:05:48,460 --> 00:05:52,330
And because they can help them in
emergency situations.

105
00:05:53,910 --> 00:05:56,860
communicate in emergency situations.

106
00:05:56,860 --> 00:06:02,500
Smartphones should be allowed, not banned
in schools and classrooms.

107
00:06:04,160 --> 00:06:06,770
Now again, there are lots of questions you
could raise.

108
00:06:06,770 --> 00:06:09,220
How often do these emergencies happen?

109
00:06:09,220 --> 00:06:12,730
Maybe communicating on the smartphone
during an emergency is going to

110
00:06:12,730 --> 00:06:16,250
make you go more slowly and not able to
escape.

111
00:06:16,250 --> 00:06:19,330
And so having the smartphone available
will actually

112
00:06:19,330 --> 00:06:23,600
create more deaths or injuries during an
emergency.

113
00:06:24,860 --> 00:06:28,130
And maybe using this dictionary on your
smart

114
00:06:28,130 --> 00:06:31,320
phone during class, which seems to be
helping you.

115
00:06:31,320 --> 00:06:34,250
Is actually stopping you from learning the

116
00:06:34,250 --> 00:06:35,700
language yourself.

117
00:06:35,700 --> 00:06:38,340
You're now dependent on the smartphone,
and it

118
00:06:38,340 --> 00:06:40,890
might get in the way of language learning.

119
00:06:41,910 --> 00:06:45,230
So I think you could raise a lot of
questions about this argument,

120
00:06:45,230 --> 00:06:49,600
just like the other one, but what I'm
interested in here is the structure.

121
00:06:50,960 --> 00:06:51,860
So let's look at that.

122
00:06:53,320 --> 00:06:57,860
Notice that word additionally in the
middle of their argument.

123
00:06:57,860 --> 00:06:59,490
That signals that they're really two

124
00:06:59,490 --> 00:07:00,910
arguments here.

125
00:07:00,910 --> 00:07:03,480
One is that smartphones can help students
learn.

126
00:07:03,480 --> 00:07:07,070
And here I'm simplifying quite a bit a
bit.

127
00:07:07,070 --> 00:07:10,570
Smartphones can help students learn, and
then anything that

128
00:07:10,570 --> 00:07:12,990
can help students learn should be allowed
in school.

129
00:07:14,730 --> 00:07:17,930
Therefore, smartphones should be allowed
in school.

130
00:07:17,930 --> 00:07:19,600
That's the first argument.

131
00:07:19,600 --> 00:07:22,000
First reason to allow smartphones is that
they can

132
00:07:22,000 --> 00:07:24,710
aid in education, and that's what schools
are all about.

133
00:07:25,830 --> 00:07:26,650
So they should be allowed.

134
00:07:27,700 --> 00:07:31,200
Notice the second one, is completely
independent.

135
00:07:31,200 --> 00:07:34,900
Smartphones can help students communicate
during emergencies.

136
00:07:34,900 --> 00:07:38,300
Anything that can help students
communicate during emergencies should be

137
00:07:38,300 --> 00:07:42,930
allowed in school, therefore smart phones
should be allowed in schools.

138
00:07:42,930 --> 00:07:47,240
They're separate arguments because the
second argument would also apply

139
00:07:47,240 --> 00:07:51,140
in offices, it should, would apply to
students when they're taking

140
00:07:51,140 --> 00:07:54,380
exams, where they're not supposed to be
getting information

141
00:07:54,380 --> 00:07:58,330
from dictionaries or any other information
from their smartphone.

142
00:07:58,330 --> 00:08:00,530
There still might be an emergency.

143
00:08:00,530 --> 00:08:04,170
And so the different arguments apply to
different circumstances, but they

144
00:08:04,170 --> 00:08:09,810
lead to the same conclusion, that
smartphones should be allowed in schools.

145
00:08:09,810 --> 00:08:11,560
So what kind of structure do we have here?

146
00:08:11,560 --> 00:08:16,440
Do we have a linear structure, or a
branching structure, or a joint structure?

147
00:08:17,630 --> 00:08:19,420
We have a branching structure.

148
00:08:19,420 --> 00:08:23,290
Because both sets of premises support the
same conclusion.

149
00:08:24,550 --> 00:08:26,670
Now, I want to ask one more question.

150
00:08:27,720 --> 00:08:30,810
Does that mean that this argument's better
than the first one.

151
00:08:30,810 --> 00:08:33,040
Hey, these guys have two arguments.

152
00:08:33,040 --> 00:08:36,950
The other group only had one argument.
That's gotta be better, right?

153
00:08:37,960 --> 00:08:39,440
No.

154
00:08:39,440 --> 00:08:41,240
It might seem better, because if one
argument

155
00:08:41,240 --> 00:08:43,120
fails you've got the other one left over.

156
00:08:44,150 --> 00:08:48,890
But, of course, if one argument is
stronger than either of the other two

157
00:08:48,890 --> 00:08:53,180
put together, the first group still might
have a better reason for their conclusion.

158
00:08:54,400 --> 00:08:56,910
So you can't just count the number of
arguments

159
00:08:56,910 --> 00:09:00,790
to figure out who's winning in a
disagreement like this.

160
00:09:02,290 --> 00:09:04,440
And the second point I want to make is

161
00:09:04,440 --> 00:09:06,440
that we ought to be looking for
compromises.

162
00:09:07,490 --> 00:09:09,430
When you get a conflict like this between

163
00:09:09,430 --> 00:09:15,000
two groups, one wants cell phones banned,
one wants smartphones allowed.

164
00:09:15,000 --> 00:09:18,290
Well, what the arguments do is they tell
us the

165
00:09:18,290 --> 00:09:23,080
values at stake, what you really want from
the band.

166
00:09:23,080 --> 00:09:26,620
Or what you really want by allowing the
smartphones.

167
00:09:26,620 --> 00:09:30,960
And what that tells you is how to
compromise.

168
00:09:30,960 --> 00:09:35,580
What we want to find now is some

169
00:09:35,580 --> 00:09:37,650
way to pass a regulation.

170
00:09:37,650 --> 00:09:42,800
That'll allow students to use smartphones
when they're really

171
00:09:42,800 --> 00:09:45,820
learning, like when they're using a
foreign language dictionary.

172
00:09:47,960 --> 00:09:51,180
Or, when they really need it like in an
emergency.

173
00:09:51,180 --> 00:09:53,880
But not so frequently that they're
going to

174
00:09:53,880 --> 00:09:56,890
interrupt other students, because they're
just going

175
00:09:56,890 --> 00:10:00,369
on a social network or playing some kind
of game that they're addicted to.

176
00:10:01,510 --> 00:10:04,470
So how're we going to do a regulation like
that?

177
00:10:04,470 --> 00:10:09,190
Well, maybe we could allow smartphones,
but only if they don't

178
00:10:09,190 --> 00:10:13,170
have any of those apps on them that
students are going to

179
00:10:13,170 --> 00:10:15,310
abuse and use too frequently.

180
00:10:15,310 --> 00:10:19,420
Or maybe, we're going to be able to
monitor their smartphones to

181
00:10:19,420 --> 00:10:22,540
see whether they're going on to the right
types of program.

182
00:10:22,540 --> 00:10:25,300
Dictionaries that help them learn,
emergency

183
00:10:25,300 --> 00:10:27,350
conversations to help people in trouble.

184
00:10:28,490 --> 00:10:30,720
And not using them for the ones that

185
00:10:30,720 --> 00:10:34,610
are preventing other people from learning
by distracting them.

186
00:10:34,610 --> 00:10:38,590
Now I don't know whether you can actually
have a regulation like that.

187
00:10:38,590 --> 00:10:41,770
But one of the nice features of arguments
is that they don't

188
00:10:41,770 --> 00:10:43,300
just tell you these people want

189
00:10:43,300 --> 00:10:47,190
smartphones, these people don't want
smartphones banned.

190
00:10:47,190 --> 00:10:47,690
Right?

191
00:10:48,820 --> 00:10:51,260
Instead, it tells you the reasons why.

192
00:10:51,260 --> 00:10:54,230
And by learning those reasons we get a
picture

193
00:10:54,230 --> 00:10:58,230
of what we need in order to formulate a
regulation.

194
00:10:58,230 --> 00:11:00,880
It'll serve the interests of both groups

195
00:11:00,880 --> 00:11:03,130
because they both have a really good
point.

