1
00:00:03,050 --> 00:00:06,350
The second level of language that we want
to discuss is speech acts.

2
00:00:06,350 --> 00:00:08,320
We already saw one instance in the example
of

3
00:00:08,320 --> 00:00:12,170
advising, when I advised you to floss your
teeth everyday.

4
00:00:12,170 --> 00:00:16,278
But clearest examples probably occurs in
games and in ceremonies.

5
00:00:18,160 --> 00:00:22,210
One famous incident that occurred, I
believe in a Yankees game, was when a

6
00:00:22,210 --> 00:00:28,270
batter had two strikes and three balls and
the pitcher threw the ball near the strike

7
00:00:28,270 --> 00:00:33,600
zone, and the batter didn't swing, so the
umpire didn't say anything.

8
00:00:33,600 --> 00:00:35,630
And the batter turned to the umpire and
said,

9
00:00:35,630 --> 00:00:39,790
well ump, am I out or was that a walk?

10
00:00:39,790 --> 00:00:43,920
And the umpire said, you ain't nothing
till I say so.

11
00:00:43,920 --> 00:00:46,250
And that's the lesson of speech acts.

12
00:00:46,250 --> 00:00:49,400
With a speech act the saying so that makes
you so.

13
00:00:49,400 --> 00:00:53,350
And the rules of baseball mean that you're
out and it

14
00:00:53,350 --> 00:00:56,540
was a strike if the umpire says so, but
you

15
00:00:56,540 --> 00:00:59,740
walk and it was a ball if the umpire says
so.

16
00:00:59,740 --> 00:01:02,090
Now, maybe the umpire should have called a
ball when

17
00:01:02,090 --> 00:01:04,120
he called strike or should have called a
strike when

18
00:01:04,120 --> 00:01:06,920
he called a ball, but it doesn't matter
because he

19
00:01:06,920 --> 00:01:10,057
makes a mistake and calls it a strike and
you're out.

20
00:01:10,057 --> 00:01:14,470
So the next kind of example involves
ceremonies.

21
00:01:14,470 --> 00:01:16,850
Imagine that you're at traditional
marriage ceremony

22
00:01:16,850 --> 00:01:18,360
and the bride and the groom show up

23
00:01:18,360 --> 00:01:22,380
with an officiant in an appropriate
location and the officiant says to

24
00:01:22,380 --> 00:01:26,470
the man, do you take this woman to be your
lawfully wedded wife?

25
00:01:26,470 --> 00:01:27,793
And he says, I do.

26
00:01:27,793 --> 00:01:31,040
Then the officiant turns to the bride and
says, do

27
00:01:31,040 --> 00:01:33,380
you take this man to be your lawfully
wedded husband?

28
00:01:33,380 --> 00:01:34,607
And she says, I do.

29
00:01:34,607 --> 00:01:39,580
Then the officiant says, I now pronounce
you husband and wife.

30
00:01:39,580 --> 00:01:40,675
That's pretty cool.

31
00:01:40,675 --> 00:01:43,100
By uttering those words,

32
00:01:43,100 --> 00:01:45,470
he made them husband and wife.

33
00:01:45,470 --> 00:01:49,810
The words changed their relationship in a
legal way,

34
00:01:49,810 --> 00:01:52,180
in a religious word and in a personal way.

35
00:01:52,180 --> 00:01:55,330
All of that happened just by uttering
words.

36
00:01:56,700 --> 00:02:01,390
And notice also that you can use a special
little word to explain this.

37
00:02:01,390 --> 00:02:06,790
You can say that, he thereby pronounced
them husband and wife.

38
00:02:06,790 --> 00:02:08,340
By uttering the words, I now

39
00:02:08,340 --> 00:02:11,450
pronounce you husband and wife, he thereby
pronounced

40
00:02:11,450 --> 00:02:14,290
them husband and wife because it was right
then

41
00:02:14,290 --> 00:02:16,600
and there in those words, by means of

42
00:02:16,600 --> 00:02:19,839
those words, that he made them husband and
wife.

43
00:02:20,910 --> 00:02:25,110
These words provide us with a nice general
test called the thereby test.

44
00:02:26,420 --> 00:02:28,110
Here's a certain pattern of words.

45
00:02:28,110 --> 00:02:33,980
If I say, I blank, then I thereby blank.
I blank

46
00:02:33,980 --> 00:02:36,360
by means of saying I blank.

47
00:02:37,910 --> 00:02:41,750
Now sometimes filling in that blank with
some words will make sense,

48
00:02:41,750 --> 00:02:45,160
but it won't make sense with other words,
and that will provide

49
00:02:45,160 --> 00:02:47,930
a test because when you can fill in that
blank with a

50
00:02:47,930 --> 00:02:53,530
verb and it makes sense, then that verb
names a speech act.

51
00:02:53,530 --> 00:02:59,170
So for example, if I say, I now pronounce
you man and wife, then I

52
00:02:59,170 --> 00:03:03,890
thereby pronounce you man and wife.
My speech act is pronouncing you.

53
00:03:03,890 --> 00:03:09,300
And if I say, I apologize I thereby
apologize.

54
00:03:10,500 --> 00:03:13,130
Notice that what the formula does is it
takes you

55
00:03:13,130 --> 00:03:16,680
from the words, which are in quotation
marks in the

56
00:03:16,680 --> 00:03:21,040
if clause of the thereby test to the
world, because

57
00:03:21,040 --> 00:03:24,240
when it's not inside quotation marks on
the right side

58
00:03:24,240 --> 00:03:28,050
of the thereby test it refers to the
world.

59
00:03:28,050 --> 00:03:29,870
So when it's in quotation marks it's about
the

60
00:03:29,870 --> 00:03:32,340
words and when it's not in quotation marks
on

61
00:03:32,340 --> 00:03:35,110
the right side it refers to the world and

62
00:03:35,110 --> 00:03:39,000
the formula takes you from the words to
the world.

63
00:03:39,000 --> 00:03:40,760
And that's what's tricky about it.

64
00:03:40,760 --> 00:03:44,830
It's amazing that you can actually use
your words to change

65
00:03:44,830 --> 00:03:49,310
the world, but only in the special case of
speech acts.

66
00:03:49,310 --> 00:03:52,220
Of course all of this works only in the
right circumstances.

67
00:03:52,220 --> 00:03:55,050
You can't just randomly walk up to any
couple on

68
00:03:55,050 --> 00:03:59,780
the street and say, I now pronounce you
husband and wife.

69
00:03:59,780 --> 00:04:00,630
Just try it.

70
00:04:00,630 --> 00:04:05,778
>> Excuse me, I wanted to say something.
I now

71
00:04:05,778 --> 00:04:11,084
pronounce you husband and wife.
>> Thank you.

72
00:04:11,084 --> 00:04:11,084
[LAUGH]

73
00:04:11,084 --> 00:04:15,680
>> Whoa, I'm lucky they didn't hit me.

74
00:04:15,680 --> 00:04:20,960
You obviously cannot pronounce people
husband and wife if you're

75
00:04:20,960 --> 00:04:26,080
not an officiant, they're not a bride and
a groom who have said, I do.

76
00:04:26,080 --> 00:04:28,590
It has to occur in the right
circumstances.

77
00:04:29,760 --> 00:04:32,230
And sometimes, which circumstances are the

78
00:04:32,230 --> 00:04:34,749
right circumstances will be very
controversial.

79
00:04:36,220 --> 00:04:41,010
People argue about whether a man can marry
man or a woman can marry a woman.

80
00:04:42,280 --> 00:04:47,020
The ones who think that you can't, think
that marriage has, as part

81
00:04:47,020 --> 00:04:50,790
of its appropriate circumstances, that
only

82
00:04:50,790 --> 00:04:53,150
people of different genders can get
married.

83
00:04:53,150 --> 00:04:56,090
A man can marry a woman, but can't marry
another man.

84
00:04:56,090 --> 00:05:01,138
Whereas, other people think that a man can
marry a man and a woman can marry a woman.

85
00:05:01,138 --> 00:05:05,910
So it's going to be controversial which
circumstances

86
00:05:05,910 --> 00:05:09,450
are appropriate for a marriage ceremony,
but everybody

87
00:05:09,450 --> 00:05:13,520
agrees that you can't just do it randomly
to any old couple on the street.

88
00:05:13,520 --> 00:05:16,460
So everybody agrees that there are going
to be

89
00:05:16,460 --> 00:05:19,600
limits and that the speech act works only
in

90
00:05:19,600 --> 00:05:23,210
the appropriate circumstances and we can
build that into

91
00:05:23,210 --> 00:05:26,500
the thereby test by just adding a few
words.

92
00:05:26,500 --> 00:05:26,710
If

93
00:05:26,710 --> 00:05:33,540
I say, I blank in the appropriate
circumstances, then I thereby blank.

94
00:05:33,540 --> 00:05:36,370
But it's only in the appropriate
circumstances that you

95
00:05:36,370 --> 00:05:39,340
can perform this speech act by uttering
the words.

96
00:05:40,960 --> 00:05:42,840
Now we can use the thereby test to pick
out

97
00:05:42,840 --> 00:05:46,631
speech acts because it works for a lot of
different examples.

98
00:05:46,631 --> 00:05:52,300
If I say, I promise to meet you for lunch
tomorrow in the appropriate circumstances

99
00:05:52,300 --> 00:05:54,660
that I thereby promise to meet you for
lunch tomorrow.

100
00:05:56,040 --> 00:05:57,960
So promising is a speech act.

101
00:05:57,960 --> 00:06:00,930
If I say, I thank you for inviting me to

102
00:06:00,930 --> 00:06:03,920
your party, and throwing such a great
party by the way.

103
00:06:05,080 --> 00:06:07,840
Then I do thereby thank you for inviting
me to your

104
00:06:07,840 --> 00:06:10,360
party and for throwing such a good party
by the way.

105
00:06:12,030 --> 00:06:13,810
So thanking is a speech act.

106
00:06:13,810 --> 00:06:17,770
If I say, I apologize for tripping over
your

107
00:06:17,770 --> 00:06:22,872
legs, then I thereby apologize for
tripping over your legs.

108
00:06:22,872 --> 00:06:26,700
Notice that the circumstances matter in
all these cases.

109
00:06:26,700 --> 00:06:28,730
If I say I apologize, I mean, I

110
00:06:28,730 --> 00:06:32,270
don't really feel sorry then, I did
apologize, but

111
00:06:32,270 --> 00:06:35,040
it was an insincere apology because the
circumstances

112
00:06:35,040 --> 00:06:37,270
weren't right since I didn't have the
appropriate feelings.

113
00:06:37,270 --> 00:06:39,470
But, I still did apologize.

114
00:06:39,470 --> 00:06:42,345
Now, in contrast, whether I promise you or
threaten you,

115
00:06:42,345 --> 00:06:46,050
depends on whether the thing that I
promise

116
00:06:46,050 --> 00:06:48,600
or threaten to do is something that you
want.

117
00:06:48,600 --> 00:06:51,065
If you want me to do it, then I'm
promising.

118
00:06:51,065 --> 00:06:55,146
But if you don't want me to do it, then I
might be threatening.

119
00:06:55,146 --> 00:06:58,530
So your attitudes towards the thing that
I'm going to do

120
00:06:58,530 --> 00:07:02,618
determines whether my speech act is a
promise or a threat.

121
00:07:02,618 --> 00:07:06,510
In all of these cases, the circumstances
are going to matter.

122
00:07:06,510 --> 00:07:07,361
So in that example the

123
00:07:07,361 --> 00:07:10,595
circumstances affect which speech act I'm
going to perform.

124
00:07:10,595 --> 00:07:14,450
But in other cases the circumstances
affect

125
00:07:14,450 --> 00:07:16,670
whether I really perform any speech act

126
00:07:16,670 --> 00:07:20,450
at all or fail to perform the speech act
that I was trying to.

127
00:07:20,450 --> 00:07:22,440
Here's an example on that.

128
00:07:22,440 --> 00:07:26,110
If I say to you, I bet you that Duke will
win the next

129
00:07:26,110 --> 00:07:28,860
National Championship and I think Ron
Netta

130
00:07:28,860 --> 00:07:30,925
might be foolish enough to take that bet.

131
00:07:30,925 --> 00:07:35,895
Then what if he responds by saying, no, I
won't bet you.

132
00:07:35,895 --> 00:07:39,360
Now, have I performed a speech act of
betting?

133
00:07:39,360 --> 00:07:42,640
No.
Have I performed another speech act?

134
00:07:42,640 --> 00:07:44,760
Not really.
What have I performed?

135
00:07:44,760 --> 00:07:47,690
So sometimes when the circumstances aren't
right

136
00:07:47,690 --> 00:07:49,510
you perform a different speech act and

137
00:07:49,510 --> 00:07:51,770
sometimes when the circumstances aren't
right you

138
00:07:51,770 --> 00:07:53,970
don't perform any speech act at all.

139
00:07:53,970 --> 00:07:56,020
It really is very sensitive to

140
00:07:56,020 --> 00:07:58,900
the particular circumstances in which
you're speaking.

141
00:07:58,900 --> 00:08:01,450
But why do we care about speech acts

142
00:08:01,450 --> 00:08:04,760
here, when we're supposed to be studying
arguments.

143
00:08:04,760 --> 00:08:08,000
Well that's because arguing is a speech
act.

144
00:08:08,000 --> 00:08:09,390
You argue with language.

145
00:08:09,390 --> 00:08:13,500
It's one of those things that you do in
using language is a certain way.

146
00:08:13,500 --> 00:08:16,390
You're intending to provide reasons and
you're providing what you

147
00:08:16,390 --> 00:08:19,840
take to be a reason to justify or explain
the conclusion.

148
00:08:19,840 --> 00:08:21,260
And justifying and explaining

149
00:08:21,260 --> 00:08:23,520
are other things that you do with
language.

150
00:08:23,520 --> 00:08:25,440
Those are speech acts too.

151
00:08:25,440 --> 00:08:28,720
So when we're studying arguments, we're
studying a particular kind

152
00:08:28,720 --> 00:08:32,060
of speech act and that's why it makes it
important to

153
00:08:32,060 --> 00:08:35,240
understand speech acts because we need to
view arguing in

154
00:08:35,240 --> 00:08:39,130
the context of these other speech acts
that I've just discussed.

155
00:08:39,130 --> 00:08:43,290
Now there's an awful lot more to say about
speech acts and I can't say it here.

156
00:08:43,290 --> 00:08:46,870
In these lectures I just want to give the
idea

157
00:08:46,870 --> 00:08:48,760
in a very basic and simple way.

158
00:08:48,760 --> 00:08:51,830
If you want more detail, we have a more
extended

159
00:08:51,830 --> 00:08:56,370
discussion of speech acts in the
accompanying book Understanding Arguments.

160
00:08:56,370 --> 00:09:01,070
But just to make sure you understand the
basics let's do a few exercises

161
00:09:01,070 --> 00:09:06,039
first and then in the next lecture we'll
go on to talk about conversational acts.

