1
00:00:03,360 --> 00:00:09,640
It's been a long and winding road, as the
Beatles used to say.

2
00:00:09,640 --> 00:00:15,100
But we're finally at the last stages of
reconstructing arguments.

3
00:00:15,100 --> 00:00:20,430
We've looked at stage one which is close
analysis, stage two which is get down

4
00:00:20,430 --> 00:00:25,625
to basics stage three, which is, sharpen
edges.

5
00:00:25,625 --> 00:00:28,610
Stage four, is organize parts.

6
00:00:28,610 --> 00:00:32,760
And now we're doing stage five, which is,
fill in gaps.

7
00:00:32,760 --> 00:00:36,220
And we'll also get to stage six, which is,
conclude.

8
00:00:36,220 --> 00:00:40,410
Stage five really consists of four
separate steps.

9
00:00:40,410 --> 00:00:43,380
First, we need to assess the argument for
validity.

10
00:00:43,380 --> 00:00:47,520
Then we need to add suppressed premises.
Enough of them to make it valid.

11
00:00:47,520 --> 00:00:51,360
Then we need to assess those suppressed
premises for truth or falsehood.

12
00:00:52,360 --> 00:00:54,440
And then we need to qualify

13
00:00:54,440 --> 00:00:56,790
the suppressed premises in order to make
them true.

14
00:00:56,790 --> 00:00:59,780
And the whole goal is to make the
suppressed premises both

15
00:00:59,780 --> 00:01:04,280
plausible for their truth and enough to
make the argument valid.

16
00:01:04,280 --> 00:01:08,680
So these steps within the stage really do
work

17
00:01:08,680 --> 00:01:12,280
in tandem together to try to make the
argument good.

18
00:01:12,280 --> 00:01:15,130
We already learned how to assess validity.

19
00:01:15,130 --> 00:01:17,330
You simply ask is it possible for the

20
00:01:17,330 --> 00:01:19,739
premises to be true and the conclusion
false and

21
00:01:19,739 --> 00:01:22,885
if so the argument is not valid and if not
the argument is valid.

22
00:01:22,885 --> 00:01:25,460
And the way you figure out whether it's
possible is,

23
00:01:25,460 --> 00:01:29,050
you try to tell a story or describe a
situation.

24
00:01:29,050 --> 00:01:33,400
And if you can describe a coherent
situation where

25
00:01:33,400 --> 00:01:35,640
the premises are true and the conclusion
is false.

26
00:01:35,640 --> 00:01:38,989
Then that shows that the argument's not
really valid.

27
00:01:38,989 --> 00:01:42,830
The main topic for today is what do you
do.

28
00:01:42,830 --> 00:01:44,780
When you assess the argument for validity
and you

29
00:01:44,780 --> 00:01:46,640
find out it's not valid.

30
00:01:46,640 --> 00:01:50,130
And the answer is you add suppress
premises

31
00:01:50,130 --> 00:01:52,385
enough of them to make the argument valid.

32
00:01:52,385 --> 00:01:55,000
Now that might seem like cheating.

33
00:01:55,000 --> 00:01:57,430
I mean you'd start with an argument that's
no good.

34
00:01:57,430 --> 00:02:00,580
It's not valid and then you just throw in

35
00:02:00,580 --> 00:02:02,829
some extra premises in order to make it
valid.

36
00:02:04,200 --> 00:02:06,530
Why isn't that just distorting the
argument,

37
00:02:06,530 --> 00:02:09,010
and making up something that wasn't there?

38
00:02:09,010 --> 00:02:09,560
The answer,

39
00:02:09,560 --> 00:02:12,020
is that it's not really bad.

40
00:02:12,020 --> 00:02:14,600
And if it were bad, we'd all be in bad
shape,

41
00:02:14,600 --> 00:02:18,620
because in every day life, people always
take things for granted.

42
00:02:18,620 --> 00:02:20,110
They make assumptions.

43
00:02:20,110 --> 00:02:20,931
We do it too.

44
00:02:20,931 --> 00:02:27,220
And if we didn't, boy, our arguments would
be really long and boring.

45
00:02:27,220 --> 00:02:30,390
So there's something to be said in favor
of suppressing premises,

46
00:02:30,390 --> 00:02:33,610
at least the obvious ones that people
really do take for granted,

47
00:02:35,000 --> 00:02:37,570
but we can also get tricked.

48
00:02:37,570 --> 00:02:41,760
People can suppress premises that really
are questionable and they

49
00:02:41,760 --> 00:02:44,454
just don't want us to see that they're
making that assumption.

50
00:02:44,454 --> 00:02:47,325
It is useful to fill out the argument with

51
00:02:47,325 --> 00:02:51,890
suppressed premises to make sure it really
is valid, because

52
00:02:51,890 --> 00:02:55,396
that brings those assumptions out into the
open were

53
00:02:55,396 --> 00:02:58,910
we can access whether or not they're true
of false.

54
00:02:58,910 --> 00:03:00,360
Another reason to fill in suppressed

55
00:03:00,360 --> 00:03:03,630
premises is to understand the argument
better.

56
00:03:03,630 --> 00:03:06,680
Because, if people suppress premises, then
they're showing

57
00:03:06,680 --> 00:03:09,860
us some of their footprints along the
path, but,

58
00:03:09,860 --> 00:03:12,210
if we really want to know the full path
that

59
00:03:12,210 --> 00:03:16,230
their reasoning followed, we've gotta see
every single footprint.

60
00:03:16,230 --> 00:03:19,570
So, the goal of bringing out the
suppressed premises

61
00:03:19,570 --> 00:03:23,110
is to let us trace exactly where their
reasoning went.

62
00:03:23,110 --> 00:03:25,755
From one step to another.
So there are two goals.

63
00:03:25,755 --> 00:03:31,560
One is to trace the full path every step,
and the other goal is to see if there are

64
00:03:31,560 --> 00:03:34,870
any missed steps or they're trying to hide
something

65
00:03:34,870 --> 00:03:37,450
from us by getting rid of one of their
footsteps.

66
00:03:37,450 --> 00:03:40,750
So, that's the point of bringing out
surpressed premises.

67
00:03:41,890 --> 00:03:43,440
To accomplish these goals is tricky.

68
00:03:43,440 --> 00:03:47,560
You have to find suppress premises that
are just strong enough

69
00:03:47,560 --> 00:03:50,766
to make the argument valid but not so
strong that they're going to

70
00:03:50,766 --> 00:03:50,970
be implausible.

71
00:03:50,970 --> 00:03:55,440
because you don't want to ascribe all
kinds of suppressed premises to the person

72
00:03:55,440 --> 00:03:57,760
that they didn't really believe and that

73
00:03:57,760 --> 00:04:00,440
they didn't really need for their
argument.

74
00:04:00,440 --> 00:04:02,656
So it's kind of like Goldilocks and the
Three Bears.

75
00:04:02,656 --> 00:04:09,330
You want suppressed premises to be not too
hot, and not too cold, but just right.

76
00:04:11,130 --> 00:04:15,880
Here's an example from a previous lecture.
My wife always gives me

77
00:04:15,880 --> 00:04:17,810
either a sweater or a board game.

78
00:04:19,060 --> 00:04:21,854
This box does not contain a sweater
because it rattles.

79
00:04:21,854 --> 00:04:22,976
When it's shaken.

80
00:04:22,976 --> 00:04:25,860
So this time, she must have given me a
board game.

81
00:04:25,860 --> 00:04:31,570
And we put this in standard form, this
way, first premise, this

82
00:04:31,570 --> 00:04:37,220
box rattles when I shake it, and that
shows you it doesn't contain a sweater.

83
00:04:37,220 --> 00:04:43,295
Third, she always gives me either a
sweater or a board game.

84
00:04:43,295 --> 00:04:47,434
Conclusion, this time she must have given
me a board game.

85
00:04:47,434 --> 00:04:52,832
Now the first step of this argument is
this

86
00:04:52,832 --> 00:04:58,150
box rattles when I shake it and the
conclusion there is it doesn't

87
00:04:58,150 --> 00:05:02,580
contain a sweater, that's the part of the
argument that we want to focus on here and

88
00:05:02,580 --> 00:05:05,642
ask whether that argument is valid.
No.

89
00:05:05,642 --> 00:05:10,440
The argument's not valid, because it's
possible for

90
00:05:10,440 --> 00:05:14,460
the premise to be true and the conclusion
false.

91
00:05:14,460 --> 00:05:18,306
How can that happen?
Well, my wife might be fooling me.

92
00:05:18,306 --> 00:05:20,520
She might know that I expect either a
sweater

93
00:05:20,520 --> 00:05:22,520
or a board game, so she puts a sweater in

94
00:05:22,520 --> 00:05:24,980
the box, and then she puts little rocks
around the

95
00:05:24,980 --> 00:05:27,590
outside, so when I shake it Well here is
something.

96
00:05:27,590 --> 00:05:33,440
So thats possible, and that shows the
argument is not valid.

97
00:05:33,440 --> 00:05:35,750
Well, how can we make the argument valid?

98
00:05:35,750 --> 00:05:38,356
The question here is, can we add a
suppressed premise

99
00:05:38,356 --> 00:05:42,777
that will turn this invalid argument into
a valid argument.

100
00:05:42,777 --> 00:05:44,525
Here's one that will do the trick.

101
00:05:44,525 --> 00:05:49,501
A box that contains a sweater.
Doesn't rattle when shaken.

102
00:05:49,501 --> 00:05:52,900
The other argument looks like this.

103
00:05:52,900 --> 00:05:55,949
This box rattles when I shake it.

104
00:05:55,949 --> 00:05:59,770
A box that contains a sweater doesn't
rattle when shaken.

105
00:05:59,770 --> 00:06:02,450
So this box doesn't contain a sweater.

106
00:06:02,450 --> 00:06:07,810
The explicit premise, is that this box
rattles when I shake it.

107
00:06:07,810 --> 00:06:10,820
The suppress premise is that a box

108
00:06:10,820 --> 00:06:13,720
that contains a sweater doesn't rattle
when shaken,

109
00:06:13,720 --> 00:06:15,350
and together they're supposed to support
the

110
00:06:15,350 --> 00:06:17,955
conclusion that this box doesn't contain a
sweater,

111
00:06:17,955 --> 00:06:20,920
but do they really support that
conclusion?

112
00:06:20,920 --> 00:06:22,560
Is the argument valid?

113
00:06:24,420 --> 00:06:28,240
Well, it's valid only if there's no
possibility that

114
00:06:28,240 --> 00:06:31,160
the premises are true and the conclusion
is false.

115
00:06:31,160 --> 00:06:34,520
Without the suppressed premise, we saw
that was possible, because my

116
00:06:34,520 --> 00:06:38,270
wife might be fooling me and putting rocks
around the sweater.

117
00:06:38,270 --> 00:06:41,880
So let's see if that's going to ruin the
validity of this argument.

118
00:06:41,880 --> 00:06:42,980
No,

119
00:06:42,980 --> 00:06:46,050
because, if the sweater's got rocks around
it, so

120
00:06:46,050 --> 00:06:48,870
it makes noise when I shake it, then, the

121
00:06:48,870 --> 00:06:51,290
premise that says a box that contains a
sweater

122
00:06:51,290 --> 00:06:54,350
doesn't rattle when shaken, turns out to
be false.

123
00:06:54,350 --> 00:06:55,510
So that's not a case.

124
00:06:55,510 --> 00:06:57,701
Where the premises are true and the
conclusion's

125
00:06:57,701 --> 00:07:00,030
false, because the premise is false in
that case.

126
00:07:00,030 --> 00:07:06,235
So, by adding this premise, we actually
succeeded in making the argument valid.

127
00:07:06,235 --> 00:07:08,630
The problem of course,

128
00:07:08,630 --> 00:07:14,362
is that validity is not enough for a good
argument, as we saw several lectures ago.

129
00:07:14,362 --> 00:07:19,150
You can have a valid argument that's very
bad when the argument's not sound.

130
00:07:19,150 --> 00:07:21,055
What we want really is soundness.

131
00:07:21,055 --> 00:07:25,120
So that's why we need the next step.

132
00:07:25,120 --> 00:07:28,580
Namely, check the suppressed premises for
truth.

133
00:07:28,580 --> 00:07:34,950
Assess whether they're true or false, and
if they're not true then

134
00:07:34,950 --> 00:07:39,580
you need to qualify them in order to make
them true, because you don't want to

135
00:07:39,580 --> 00:07:43,070
claim that the person giving the argument
was

136
00:07:43,070 --> 00:07:46,650
assuming this falsehood when they didn't
have to.

137
00:07:46,650 --> 00:07:49,490
So let's see if there's some way to
qualify

138
00:07:49,490 --> 00:07:52,590
this surpresssed premise in order to make
it true.

139
00:07:52,590 --> 00:07:58,202
How can we qualify this premise to make it
true?

140
00:07:58,202 --> 00:08:00,192
How are we going to

141
00:08:00,192 --> 00:08:04,570
do that, let me think, oh, what about that
little word only?

142
00:08:04,570 --> 00:08:06,578
We can add that.

143
00:08:06,578 --> 00:08:13,300
We could say, a box that contains only a
sweater doesn't rattle when shaken.

144
00:08:14,950 --> 00:08:17,840
But the word only what exactly does that
mean?

145
00:08:17,840 --> 00:08:22,410
We need to clarify that.
What exactly does the word only exclude?

146
00:08:22,410 --> 00:08:25,220
It excludes something.
That's the function of the word only.

147
00:08:25,220 --> 00:08:29,510
But what does it exclude?
Well.

148
00:08:29,510 --> 00:08:32,180
It probably excludes other things that
might make the rattling sound.

149
00:08:32,180 --> 00:08:39,260
Like, if my wife put rocks in the box.
So, we can clarify this premise by saying.

150
00:08:39,260 --> 00:08:43,770
A box that contains only a sweater and not
anything else

151
00:08:43,770 --> 00:08:49,120
that might make a rattling sound when
shaken, won't rattle when shaken.

152
00:08:50,450 --> 00:08:51,875
Well is that premise true?

153
00:08:51,875 --> 00:08:56,600
You might quibble about details but it's
close enough for now.

154
00:08:56,600 --> 00:08:59,430
What we need to do though is to go back
and

155
00:08:59,430 --> 00:09:04,455
determine whether when we put that
suppress premise in, the argument's valid.

156
00:09:04,455 --> 00:09:09,820
And the argument now looks like this.
This box does rattle when shaken.

157
00:09:10,930 --> 00:09:15,550
And a box doesn't rattle when shaken if it
contains only

158
00:09:15,550 --> 00:09:18,880
a sweater and not anything else that makes
a rattling sound.

159
00:09:20,310 --> 00:09:23,380
So this box doesn't contain a sweater.

160
00:09:24,510 --> 00:09:25,300
Is that valid?

161
00:09:26,690 --> 00:09:29,230
Well, no, for the same reason we saw

162
00:09:29,230 --> 00:09:31,600
before, because my wife might be a
trickster

163
00:09:31,600 --> 00:09:37,210
who puts rocks around my sweater in the
birthday present box, in order to fool me.

164
00:09:38,440 --> 00:09:40,615
Then, the premises can be true, and the

165
00:09:40,615 --> 00:09:42,990
conclusion, false.

166
00:09:42,990 --> 00:09:49,940
It's possible that the first premise is
true, this box rattles when I shake it.

167
00:09:49,940 --> 00:09:54,770
The second premise is true, a box doesn't
rattle when shaken if

168
00:09:54,770 --> 00:09:57,615
it contains only a sweater and nothing
else that makes a rattling sound.

169
00:09:59,810 --> 00:10:00,820
But it's false.

170
00:10:00,820 --> 00:10:04,120
If this box doesn't contain a sweater,
because

171
00:10:04,120 --> 00:10:05,645
it still does contain a sweater and it

172
00:10:05,645 --> 00:10:07,900
contains a sweater in addition to those
pesky

173
00:10:07,900 --> 00:10:09,850
little rocks that make all that rattling
noise.

174
00:10:09,850 --> 00:10:12,270
Well if the argument's not valid we got to

175
00:10:12,270 --> 00:10:15,200
back to that other step and add another
suppressed premise.

176
00:10:15,200 --> 00:10:16,730
Remember how I told you that these

177
00:10:16,730 --> 00:10:19,570
different steps within this stage work in
tandem.

178
00:10:20,640 --> 00:10:22,240
and what's happening is, you gotta check
it

179
00:10:22,240 --> 00:10:24,860
for validity, add a suppressed premise,
recheck it for

180
00:10:24,860 --> 00:10:27,450
validity, maybe add another suppressed
premise in.

181
00:10:27,450 --> 00:10:29,360
That's what we're doing now.

182
00:10:29,360 --> 00:10:32,230
So what kind of suppressed premise could
we add?

183
00:10:32,230 --> 00:10:36,130
Well, we could add, my wife's not a
trickster.

184
00:10:36,130 --> 00:10:39,560
But basically that amounts to, she
wouldn't put rocks in

185
00:10:39,560 --> 00:10:42,395
a birthday present with a sweater in order
to fool me.

186
00:10:42,395 --> 00:10:44,730
So we can make that a little more

187
00:10:44,730 --> 00:10:49,230
explicit by making the suppressed premise
something like this.

188
00:10:49,230 --> 00:10:50,100
If this box

189
00:10:50,100 --> 00:10:54,290
contains a sweater, then it contains only
a sweater and it

190
00:10:54,290 --> 00:10:58,960
doesn't include anything else that would
make a rattling sound when shaken.

191
00:11:00,040 --> 00:11:04,610
And now, we can stick that as an extra
suppressed premise into the argument.

192
00:11:04,610 --> 00:11:06,460
Now the argument looks like this.

193
00:11:08,190 --> 00:11:11,570
This box, rattles when I shake it.

194
00:11:11,570 --> 00:11:15,170
A box doesn't rattle when shaken if it
contains only a

195
00:11:15,170 --> 00:11:19,340
sweater and not anything else that makes a
rattling noise when shaken.

196
00:11:20,970 --> 00:11:25,460
If this box contains a sweater, then it
contains only

197
00:11:25,460 --> 00:11:29,450
a sweater, and doesn't contain anything
else that rattles when shaken.

198
00:11:29,450 --> 00:11:32,960
So this box does not contain a sweater.

199
00:11:33,960 --> 00:11:36,288
Now we have an argument that's valid.

200
00:11:36,288 --> 00:11:40,210
And the surpressed premises are true at
least given that my wife's

201
00:11:40,210 --> 00:11:43,860
not a trickster which she's not I assure
you,

202
00:11:43,860 --> 00:11:46,210
and it looks like we have a sound
reconstruction.

203
00:11:46,210 --> 00:11:47,500
Just what we were looking for.

204
00:11:47,500 --> 00:11:55,850
Admittedly this argument is a lot longer
and more convoluted than the original.

205
00:11:55,850 --> 00:11:59,310
And that shows why people suppress
premises instead

206
00:11:59,310 --> 00:12:01,030
of talk in the way this argument goes.

207
00:12:02,090 --> 00:12:05,230
And of course many people would be
perfectly

208
00:12:05,230 --> 00:12:08,310
well convinced by the original argument
because, they

209
00:12:08,310 --> 00:12:12,150
share the assumptions that are in the
suppressed premises.

210
00:12:12,150 --> 00:12:14,190
So why do we go to all the trouble

211
00:12:14,190 --> 00:12:17,910
to go through this process and add the
suppressed premises.

212
00:12:17,910 --> 00:12:20,262
Remember, the reason is that we

213
00:12:20,262 --> 00:12:23,920
want to understand the pathway between the
premises

214
00:12:23,920 --> 00:12:29,735
and conclusion, we want to understand how
the reasoning works step by step by step.

215
00:12:30,768 --> 00:12:33,410
And we want to do that because sometimes
people are

216
00:12:33,410 --> 00:12:36,945
going to include suppressed premises that
aren't true and we

217
00:12:36,945 --> 00:12:40,180
want to bring them out and make those
assumptions explicit

218
00:12:40,180 --> 00:12:42,411
so that we can assess them for truth and
falsehood.

219
00:12:42,411 --> 00:12:46,270
And when you're talking to somebody you
trust, you might not have to do that.

220
00:12:46,270 --> 00:12:51,385
And it's okay to suppress premises, but
when you really want to know whether the

221
00:12:51,385 --> 00:12:53,850
argument's any good that's when you
want to

222
00:12:53,850 --> 00:12:57,020
fill it out with the suppressed premises.

223
00:12:57,020 --> 00:12:58,780
The point of going into detail on this

224
00:12:58,780 --> 00:13:02,562
example, is to illustrate this stage of
reconstruction you

225
00:13:02,562 --> 00:13:06,430
want to assess the argument for validity,
and suppress

226
00:13:06,430 --> 00:13:10,015
premises that make it valid, check them
for truth.

227
00:13:10,015 --> 00:13:12,550
If they're not true, you qualify them.

228
00:13:12,550 --> 00:13:14,190
Then you go back and see whether

229
00:13:14,190 --> 00:13:17,380
that qualification made the argument not
valid anymore.

230
00:13:17,380 --> 00:13:18,720
And then you go back and forth, and

231
00:13:18,720 --> 00:13:21,300
back and forth, until you've got a sound
reconstruction.

232
00:13:22,970 --> 00:13:26,470
The same steps are going to apply to all
kinds of suppressed premises.

233
00:13:26,470 --> 00:13:30,300
And sure enough, there are all kinds of
suppressed premises.

234
00:13:30,300 --> 00:13:33,530
So let's go through a few examples a lot
more quickly in

235
00:13:33,530 --> 00:13:39,710
order to show the variety of suppressed
premises that are assumed in arguments.

236
00:13:39,710 --> 00:13:41,909
Here's one example.

237
00:13:41,909 --> 00:13:47,229
Abraham Lincoln turned 40 on February
12th,

238
00:13:47,229 --> 00:13:52,664
1849.
Therefore, Charles Darwin also turned

239
00:13:52,664 --> 00:13:57,754
40 on February 12, 1849.

240
00:13:57,754 --> 00:14:00,345
Now, is that argument valid?
No chance.

241
00:14:00,345 --> 00:14:05,200
Of course it's possible for the premise to
be true and the conclusion false.

242
00:14:06,410 --> 00:14:07,982
So we have to add a suppressed premise.

243
00:14:07,982 --> 00:14:12,560
The suppressed premise is that Abraham
Lincoln and

244
00:14:12,560 --> 00:14:15,170
Charles Darwin were born on the same day,
and

245
00:14:15,170 --> 00:14:18,390
they were it happened to be February 12,
1809.

246
00:14:18,390 --> 00:14:21,243
So now we've filled out the argument.

247
00:14:21,243 --> 00:14:26,278
Abraham Lincoln turned 40 on February 12,
1849.

248
00:14:27,530 --> 00:14:30,360
Abraham Lincoln and Charles Darwin were
born on the same day.

249
00:14:30,360 --> 00:14:36,904
Therefore, Charles Darwin also turned 40
on February 12, 1849.

250
00:14:36,904 --> 00:14:37,835
Now is the

251
00:14:37,835 --> 00:14:39,578
argument valid?
No.

252
00:14:39,578 --> 00:14:41,670
It's still not valid.

253
00:14:41,670 --> 00:14:45,380
because Darwin might have died before
1849.

254
00:14:45,380 --> 00:14:50,660
So we have to add another suppressed
premise namely that both Abraham Lincoln

255
00:14:50,660 --> 00:14:55,960
and Charles Darwin lived beyond 40.
So now we have a fuller argument.

256
00:14:57,150 --> 00:15:02,230
Abraham Lincoln turned 40 on February 12th
1849.

257
00:15:02,230 --> 00:15:02,990
Abraham Lincoln and

258
00:15:02,990 --> 00:15:09,450
Charles Darwin were born on the same day.
Both of them lived beyond the age of 40.

259
00:15:09,450 --> 00:15:17,620
Therefore, Charles Darwin also turned 40
on February 12, 1849.

260
00:15:17,620 --> 00:15:19,830
Now the argument looks pretty good.

261
00:15:19,830 --> 00:15:24,880
We had to have two suppressed premises,
but we finally have a valid argument.

262
00:15:24,880 --> 00:15:28,190
And what this shows is that sometimes the
suppressed

263
00:15:28,190 --> 00:15:31,170
premises are purely factual matters.

264
00:15:31,170 --> 00:15:33,450
In this case, that they were born on the
same day.

265
00:15:33,450 --> 00:15:36,320
And that they both lived beyond 40.

266
00:15:36,320 --> 00:15:40,510
So sometimes we have factual suppressed
premises.

267
00:15:40,510 --> 00:15:42,620
Here's another quick example.

268
00:15:42,620 --> 00:15:45,030
You ought to obey her because she's your
mother.

269
00:15:45,030 --> 00:15:46,600
Here the premise is that she's your
mother,

270
00:15:46,600 --> 00:15:49,440
and the conclusion's, you ought to obey
her.

271
00:15:49,440 --> 00:15:51,298
Well, is that argument valid?

272
00:15:51,298 --> 00:15:53,260
No way.
because it's

273
00:15:53,260 --> 00:15:58,270
possible that she's your mother, but it's
false that you ought to obey her.

274
00:15:58,270 --> 00:15:59,370
When could that happen?

275
00:15:59,370 --> 00:16:02,890
Maybe she was like abusive, or stupid, or
whatever.

276
00:16:03,970 --> 00:16:06,510
Then maybe you ought not to obey her, even
though she is your mother.

277
00:16:07,610 --> 00:16:13,640
So, we have to add a premise.
Namely, you ought to obey your mother.

278
00:16:13,640 --> 00:16:15,650
Now, we can say she's your mother, you
ought

279
00:16:15,650 --> 00:16:17,644
to obey your mother, therefor you ought to
obey her.

280
00:16:17,644 --> 00:16:21,180
But of course, that is the suppressed
premise you ought to

281
00:16:21,180 --> 00:16:25,076
obey your mother is questionable because
maybe she was abusive or stupid.

282
00:16:25,076 --> 00:16:30,250
So let's add another suppressed premise
that

283
00:16:30,250 --> 00:16:32,330
your mother was not abusive or stupid.

284
00:16:32,330 --> 00:16:35,790
Of course, we also have to qualify that
moral premise that

285
00:16:35,790 --> 00:16:39,590
you ought to obey your mother if she's not
abusive or stupid.

286
00:16:39,590 --> 00:16:42,810
And now the argument looks like this.
She's your mother.

287
00:16:42,810 --> 00:16:45,720
You ought to obey your mother if she's not
abusive or stupid.

288
00:16:47,010 --> 00:16:51,030
Your mother was not abusive or stupid,
therefore you ought to obey her.

289
00:16:51,030 --> 00:16:57,580
And notice that here, we added a moral
premise about the that fact that

290
00:16:57,580 --> 00:16:58,930
you ought to obey your mother under

291
00:16:58,930 --> 00:17:02,520
certain conditions namely she's not
abusive or stupid.

292
00:17:02,520 --> 00:17:08,400
And the second premise is she was not
abusive or stupid so we

293
00:17:08,400 --> 00:17:10,708
have a moral premise and a factual
premise.

294
00:17:10,708 --> 00:17:13,822
Both being suppressed in the argument that
you

295
00:17:13,822 --> 00:17:17,000
ought to obey her, because she's your
mother.

296
00:17:17,000 --> 00:17:17,570
Here's another.

297
00:17:17,570 --> 00:17:20,385
It's the Sabbath.
So you ought go to the Synagogue.

298
00:17:20,385 --> 00:17:26,738
Well, that's clearly not valid.
Once suppressed premises you're Jewish.

299
00:17:26,738 --> 00:17:30,130
The other suppressed premise is you
haven't

300
00:17:30,130 --> 00:17:32,606
been to Synagogue already today on this
Sabbath.

301
00:17:32,606 --> 00:17:33,911
And the third

302
00:17:33,911 --> 00:17:38,215
suppressed premise is a religious norm
namely

303
00:17:38,215 --> 00:17:41,250
Jews ought to go to the Synagogue on

304
00:17:41,250 --> 00:17:46,498
the Sabbath and you need that whole bunch
of suppressed premises in order to get.

305
00:17:46,498 --> 00:17:50,210
From the premise that is the Sabbath to

306
00:17:50,210 --> 00:17:52,558
the conclusion that you ought to go to
Synagogue.

307
00:17:52,558 --> 00:17:56,130
Now of course all of those premises might
be

308
00:17:56,130 --> 00:17:59,130
questionable, some people would question
them, some people would

309
00:17:59,130 --> 00:18:04,170
deny them, but the point here is to figure
out

310
00:18:04,170 --> 00:18:07,070
what's being assumed by somebody who gave
the original argument.

311
00:18:07,070 --> 00:18:10,810
And anybody who says it's the Sabbath so
you

312
00:18:10,810 --> 00:18:13,240
ought to go to Synagogue, seems to be
assuming

313
00:18:13,240 --> 00:18:16,000
that you're Jewish, you haven't been
already, and Jews

314
00:18:16,000 --> 00:18:17,960
ought to go to the Synagogue on the
Sabbath.

315
00:18:18,980 --> 00:18:22,060
So what these suppressed premises do is

316
00:18:22,060 --> 00:18:24,460
they bring out the assumptions that
somebody

317
00:18:24,460 --> 00:18:28,050
who gave that argument must have had in
mind.

318
00:18:28,050 --> 00:18:30,220
The last case is a little bit trickier.

319
00:18:30,220 --> 00:18:33,920
It has to do with linguistics suppressed
premises.

320
00:18:33,920 --> 00:18:38,831
Janet and Bob are first cousins, therefore
they share a grandparent.

321
00:18:38,831 --> 00:18:43,280
Now, in order to understand that argument,
we have

322
00:18:43,280 --> 00:18:48,302
to know that first cousins always share a
grandparent.

323
00:18:48,302 --> 00:18:49,760
That just

324
00:18:49,760 --> 00:18:52,240
follows from the definition of what a
first cousin is.

325
00:18:54,280 --> 00:19:00,170
But, it's not quite so obvious is that all
biological sisters are female, and

326
00:19:00,170 --> 00:19:02,330
so there's even more need to bring

327
00:19:02,330 --> 00:19:05,220
out that linguistic suppressed premise in
this case.

328
00:19:05,220 --> 00:19:08,200
But it's still not necessary to make the
argument valid.

329
00:19:08,200 --> 00:19:11,300
It's just not possible that Janet and Bob.

330
00:19:11,300 --> 00:19:15,030
Are first cousins, and they don't share, a
grand parent.

331
00:19:15,030 --> 00:19:19,450
Because the suppressed premise is purely
linguistic, so it's necessarily true.

332
00:19:19,450 --> 00:19:24,920
So, you can't possibly, be first cousins
without sharing a grandparent.

333
00:19:24,920 --> 00:19:28,845
Still, the point of bringing out
linguistic suppressed premises is to

334
00:19:28,845 --> 00:19:33,840
show every little step along the way the
argument might be valid

335
00:19:33,840 --> 00:19:39,040
without those suppressed linguistic
premises but we won't understand

336
00:19:39,040 --> 00:19:44,450
why it's valid and why the reasoning goes
through unless we add the linguistic

337
00:19:44,450 --> 00:19:46,766
suppressed premise.
So it's worth doing that.

338
00:19:46,766 --> 00:19:52,820
Sh, here's a trick.
Don't tell anybody.

339
00:19:52,820 --> 00:19:54,994
Okay?
It's just between me and you.

340
00:19:54,994 --> 00:20:02,530
You could always make any argument valid
just by adding a suppressed

341
00:20:02,530 --> 00:20:08,805
premise that says, if the premises are
true then the conclusion is true.

342
00:20:08,805 --> 00:20:13,820
But don't tell anybody because.
If

343
00:20:13,820 --> 00:20:18,600
people start doing that, and they make the
argument valid that way, with that

344
00:20:18,600 --> 00:20:25,320
suppressed premise, we're never going to
understand the pathway of reasoning.

345
00:20:25,320 --> 00:20:29,247
It makes the argument valid, but it
doesn't serve the real purpose

346
00:20:29,247 --> 00:20:34,600
of the unending suppressed premises, which
is to understand the pathway of reasoning.

347
00:20:34,600 --> 00:20:37,070
So you can do that, it's a trick.

348
00:20:37,070 --> 00:20:39,870
It makes the argument valid, but it
doesn't achieve

349
00:20:39,870 --> 00:20:42,530
our goal because our goal is not just to

350
00:20:42,530 --> 00:20:45,030
make the argument valid, it's to make the
argument

351
00:20:45,030 --> 00:20:49,820
valid so that we can understand the
pathway of reasoning.

352
00:20:51,610 --> 00:20:56,260
So, it's important to know that trick, but
don't use it.

353
00:20:56,260 --> 00:20:59,680
Unless you have to.
The examples

354
00:20:59,680 --> 00:21:02,485
so far have been pretty trivial, I admit
it.

355
00:21:02,485 --> 00:21:05,270
But the same points apply in

356
00:21:05,270 --> 00:21:09,120
very important context, such as political
debates.

357
00:21:09,120 --> 00:21:14,210
Politicians can suppress premises in
perfectly legitimate ways

358
00:21:14,210 --> 00:21:16,250
they're just trying to save time and make
their

359
00:21:16,250 --> 00:21:20,130
arguments more efficient, maybe even
sometimes clearer because you

360
00:21:20,130 --> 00:21:22,650
don't have to add all of those little
details.

361
00:21:24,220 --> 00:21:24,910
But sometimes

362
00:21:24,910 --> 00:21:28,150
politicians abuse suppressed premises.

363
00:21:28,150 --> 00:21:31,300
They take things for granted that they
shouldn't be taking for granted.

364
00:21:32,380 --> 00:21:33,410
And here's an example.

365
00:21:33,410 --> 00:21:38,030
A politician might argue, my opponent is
soft on

366
00:21:38,030 --> 00:21:42,266
crime because he's opposed to the death
penalty, well,

367
00:21:42,266 --> 00:21:45,620
that assumes as a suppressed premise that
anyone who's

368
00:21:45,620 --> 00:21:48,520
opposed to the death penalty must be soft
on crime.

369
00:21:48,520 --> 00:21:49,940
And if the politician were

370
00:21:49,940 --> 00:21:50,890
to come

371
00:21:53,200 --> 00:21:56,280
out and say that it would seem pretty
questionable.

372
00:21:56,280 --> 00:21:59,270
And that's probably why he suppresses it.

373
00:22:00,660 --> 00:22:03,540
And then another politician might say, but
my

374
00:22:03,540 --> 00:22:06,930
opponent is in favor of the death penalty
so

375
00:22:06,930 --> 00:22:09,360
he must not have read all the recent

376
00:22:09,360 --> 00:22:11,839
studies that show that the death penalty
doesn't deter.

377
00:22:13,730 --> 00:22:18,210
Well that argument assumes a suppressed
premise that if you read those studies you

378
00:22:18,210 --> 00:22:21,000
would be convinced by them and that the

379
00:22:21,000 --> 00:22:22,980
only point of the death penalty is
deterrence.

380
00:22:22,980 --> 00:22:26,320
But the point is that politicians talking
about extremely

381
00:22:26,320 --> 00:22:30,090
important issues can take things for
granted, that if

382
00:22:30,090 --> 00:22:32,400
they were brought into the light of day
would

383
00:22:32,400 --> 00:22:36,390
be very questionable, and that is why they
hide them.

384
00:22:36,390 --> 00:22:39,200
So when you're listening to people give
arguments on important

385
00:22:39,200 --> 00:22:43,220
issues in your life, then you ought to be
looking for

386
00:22:43,220 --> 00:22:47,060
these suppressed premises and asking
yourself whether or not you

387
00:22:47,060 --> 00:22:50,913
really ought to be agreeing with them
about that assumption.

388
00:22:50,913 --> 00:22:58,734
Finally, we've finished

389
00:22:58,734 --> 00:23:08,043
reconstruction.
Yippee, right?

390
00:23:08,043 --> 00:23:12,400
Oh, no, not quite.
because there's one more stage.

391
00:23:12,400 --> 00:23:14,125
And that stage is drawing a conclusion.

392
00:23:14,125 --> 00:23:16,580
Of course, if we've come up with a

393
00:23:16,580 --> 00:23:21,090
sound reconstruction then we know that the
argument is

394
00:23:21,090 --> 00:23:24,090
sound, and we know that the conclusion is

395
00:23:24,090 --> 00:23:26,895
true, because every sound argument has a
true conclusion.

396
00:23:26,895 --> 00:23:33,080
But if we don't come up with a sound
reconstruction, then what do

397
00:23:33,080 --> 00:23:38,560
we say?
Well we kind of ask whose fault is it.

398
00:23:38,560 --> 00:23:41,010
It might be the fault of the argument
maybe we couldn't come

399
00:23:41,010 --> 00:23:44,530
up with a sound reconstruction because
there just is no sound reconstruction.

400
00:23:44,530 --> 00:23:48,280
But maybe we didn't come up with a sound
reconstruction

401
00:23:48,280 --> 00:23:51,190
because we just weren't imaginative enough
or didn't try hard enough.

402
00:23:52,600 --> 00:23:55,810
Still if we try really long and hard.

403
00:23:55,810 --> 00:23:58,100
And charitably interpret the argument as

404
00:23:58,100 --> 00:24:01,550
best we can to make it look as good as we
can and we still can't

405
00:24:01,550 --> 00:24:04,680
make it sound, then we've at least got

406
00:24:04,680 --> 00:24:07,280
reason to believe that the argument's not
sound.

407
00:24:07,280 --> 00:24:10,746
Of course, that doesn't mean that the
conclusion's not

408
00:24:10,746 --> 00:24:14,040
true because unsound arguments can still
have true conclusions.

409
00:24:15,360 --> 00:24:21,221
But at least we know that this argument
doesn't prove that the conclusion's true.

410
00:24:21,221 --> 00:24:23,900
And so this method of reconstruction

411
00:24:23,900 --> 00:24:29,190
can lead us either to the belief that the
argument is sound because we found

412
00:24:29,190 --> 00:24:34,460
a sound reconstruction or to the
conclusion that the argument's not sound.

413
00:24:34,460 --> 00:24:38,830
Because we tried long and hard to find a
sound reconstruction and didn't.

414
00:24:38,830 --> 00:24:44,220
But that's still not going to show us that
the conclusion of the argument is false.

415
00:24:44,220 --> 00:24:46,760
The point of reconstruction then is to

416
00:24:46,760 --> 00:24:49,030
reach a conclusion on this issue of
whether

417
00:24:49,030 --> 00:24:50,405
the argument is sound or not.

418
00:24:50,405 --> 00:24:56,630
And if we try our best, and do it as well
as we can, and charitably, then

419
00:24:56,630 --> 00:25:00,980
we can be justified in believing that the
argument's sound or not.

