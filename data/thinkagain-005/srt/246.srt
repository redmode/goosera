1
00:00:03,150 --> 00:00:04,070
Welcome to week two.

2
00:00:04,070 --> 00:00:08,460
In week one, we talked about language in
general.

3
00:00:09,510 --> 00:00:12,530
This week, we want to talk about how
language is

4
00:00:12,530 --> 00:00:17,230
used in one particular context, namely,
the context of arguments.

5
00:00:18,240 --> 00:00:22,640
We need to understand the language of
argument in order to be able to spot

6
00:00:22,640 --> 00:00:28,210
an argument, that is to determine when a
passage contains an argument and what part

7
00:00:28,210 --> 00:00:30,620
of the passage is that argument.

8
00:00:30,620 --> 00:00:36,220
So, how can we tell when an argument is
being given?

9
00:00:36,220 --> 00:00:39,240
Well, recall the definition of argument as
a series

10
00:00:39,240 --> 00:00:42,390
of sentences, statements, or propositions,
where some of there

11
00:00:42,390 --> 00:00:45,070
premises, and one of them's a conclusion,
and the

12
00:00:45,070 --> 00:00:48,600
premises are intended to give a reason for
the conclusion.

13
00:00:48,600 --> 00:00:53,240
So the real question of when an argument
is being given comes down to the

14
00:00:53,240 --> 00:01:00,120
question of when certain sentences are
intended as reasons for other sentences.

15
00:01:00,120 --> 00:01:02,640
Now the answer is that, we can tell a

16
00:01:02,640 --> 00:01:05,990
person's intentions when they're speaking
by which words they choose.

17
00:01:05,990 --> 00:01:08,080
So there are going to be certain words
that

18
00:01:08,080 --> 00:01:11,220
indicate that some sentences are reasons
for others.

19
00:01:11,220 --> 00:01:17,580
Just compare these two sentences.
I am tall and I good at sports.

20
00:01:18,860 --> 00:01:25,020
And compare that to; I am tall, so I am
good at sports.

21
00:01:25,020 --> 00:01:29,260
Now notice that you can take the first
sentence, I am tall and I am good

22
00:01:29,260 --> 00:01:33,710
at a sports, and switch it around, I am
good at sports and I am tall.

23
00:01:33,710 --> 00:01:35,240
Switching doesn't make any difference.

24
00:01:35,240 --> 00:01:40,540
But it's very different if you say, I am
tall, so I am good at sports.

25
00:01:40,540 --> 00:01:43,550
That's very different from, I'm good at
sports,

26
00:01:43,550 --> 00:01:44,846
so I am tall.

27
00:01:44,846 --> 00:01:47,973
So we know from the fact that you can
switch around

28
00:01:47,973 --> 00:01:51,890
the and sentence and you cannot switch
around the so sentence.

29
00:01:51,890 --> 00:01:54,440
The word so introduces something very

30
00:01:54,440 --> 00:01:56,830
different from just conjoining the two
claims.

31
00:01:56,830 --> 00:01:58,270
But then, what's the difference?

32
00:01:59,270 --> 00:02:01,910
Well, the difference is that when you use

33
00:02:01,910 --> 00:02:05,620
the word and, you're simply stating the
two facts.

34
00:02:05,620 --> 00:02:06,440
I am tall.

35
00:02:06,440 --> 00:02:08,318
I am good at sports.

36
00:02:08,318 --> 00:02:09,835
And the is they're both true.

37
00:02:09,835 --> 00:02:13,190
But when you use the word so, you're
indicating

38
00:02:13,190 --> 00:02:15,920
that one of them is a reason for the
other.

39
00:02:15,920 --> 00:02:20,470
If you say, I am tall, so I am good at
sports, then

40
00:02:20,470 --> 00:02:23,355
you're suggesting that the reason why
you're good at sports is that you're tall.

41
00:02:23,355 --> 00:02:28,650
But if you say, I'm good at sports, so I
am tall.

42
00:02:28,650 --> 00:02:31,160
You're indicating that the fact that
you're good at sports

43
00:02:31,160 --> 00:02:33,390
is some kind of evidence that you must be
tall.

44
00:02:33,390 --> 00:02:37,730
Maybe because you can only be good at
sports of you're tall, which isn't true.

45
00:02:37,730 --> 00:02:39,465
That just shows it's a bad argument.

46
00:02:39,465 --> 00:02:43,130
But it is an argument, because by using
the word so, you're

47
00:02:43,130 --> 00:02:46,550
indicating that one of the sentences is
the reason for the other.

48
00:02:46,550 --> 00:02:49,210
Of course the word so is not the only word

49
00:02:49,210 --> 00:02:52,191
that plays this role in arguments or has
this function.

50
00:02:52,191 --> 00:02:57,282
You can also say, I am tall.
Therefore, I am good at sports.

51
00:02:57,282 --> 00:02:58,810
I am tall.
Thus,

52
00:02:58,810 --> 00:03:01,080
I am good at sports.
Or I am tall.

53
00:03:01,080 --> 00:03:03,795
Hence, I am good at sports.
Or I am tall.

54
00:03:03,795 --> 00:03:06,470
Accordingly, I am good at sports.

55
00:03:07,730 --> 00:03:11,700
All of these different pairs of sentences
play the same role.

56
00:03:11,700 --> 00:03:15,650
They indicate that there's an argument
there, namely, the fact that I am

57
00:03:15,650 --> 00:03:19,760
tall is a reason for the conclusion that I
am good at sports.

58
00:03:19,760 --> 00:03:24,410
So, we're going to call all these words
argument markers, because they mark or

59
00:03:24,410 --> 00:03:27,430
indicate the presence of an argument.

60
00:03:27,430 --> 00:03:33,700
Next, we want to distinguish two different
kinds of argument markers.

61
00:03:33,700 --> 00:03:40,330
So far, we've looked at so, and therefore,
and thus, and accordingly.

62
00:03:40,330 --> 00:03:45,490
Each of those indicates that the sentence
right after that is a conclusion, and

63
00:03:45,490 --> 00:03:49,580
the other sentence in the pair is a
premise.

64
00:03:49,580 --> 00:03:51,940
So, we're going to call these conclusion
markers because they

65
00:03:51,940 --> 00:03:55,140
indicate that the sentence right after
them is a conclusion.

66
00:03:55,140 --> 00:04:00,510
But the other argument markers that also
indicate arguments in the same way.

67
00:04:00,510 --> 00:04:03,108
What they indicate is that the sentence
after them

68
00:04:03,108 --> 00:04:06,815
is a reason, or a premise, not a
conclusion.

69
00:04:06,815 --> 00:04:12,400
For example, I could say, I'm good at
sports because I am tall.

70
00:04:12,400 --> 00:04:14,830
Now the word because indicates that the
fact

71
00:04:14,830 --> 00:04:19,270
I'm tall is a reason for the conclusion
that I'm good at sports.

72
00:04:19,270 --> 00:04:23,970
It doesn't mean that the sentence after
the word because is a conclusion.

73
00:04:23,970 --> 00:04:26,810
Instead, it means that the sentence after
the

74
00:04:26,810 --> 00:04:30,370
word because is a reason, or a premise.

75
00:04:30,370 --> 00:04:34,450
So, we're going to call it a reason
marker, or a premise marker.

76
00:04:34,450 --> 00:04:36,970
And there are other reason markers as
well.

77
00:04:36,970 --> 00:04:39,860
You could say, I am good at sports

78
00:04:39,860 --> 00:04:44,880
for I am tall.
I am good at sports, as I am tall.

79
00:04:44,880 --> 00:04:48,305
I am good at sports, for the reason that I
am tall.

80
00:04:48,305 --> 00:04:52,570
I'm good at sports and the reason why is
that I am tall.

81
00:04:59,810 --> 00:05:04,660
All of these words, both the conclusion
markers and the reason markers,

82
00:05:04,660 --> 00:05:10,150
indicate that there's an argument present,
but only in some cases.

83
00:05:10,150 --> 00:05:12,750
You can't just look at the word and

84
00:05:12,750 --> 00:05:14,730
figure out whether it's an argument marker
or not.

85
00:05:14,730 --> 00:05:18,150
You have to think about the role that it's
playing.

86
00:05:18,150 --> 00:05:21,690
A perfect example of that is another
reason marker, since.

87
00:05:21,690 --> 00:05:24,970
You can say, I'm good at sports since

88
00:05:24,970 --> 00:05:26,160
I am tall.

89
00:05:26,160 --> 00:05:28,920
And then it looks like you're presenting
the fact that

90
00:05:28,920 --> 00:05:33,100
you're tall as a reason why you're good at
sports.

91
00:05:33,100 --> 00:05:35,765
But the word since doesn't always play
that role.

92
00:05:35,765 --> 00:05:42,390
After all, you can say, the sun has been
up since 7 o'clock this morning.

93
00:05:42,390 --> 00:05:45,900
And that doesn't mean that somehow the sun
has an alarm

94
00:05:45,900 --> 00:05:50,220
clock that causes it to come right up at 7
o'clock.

95
00:05:50,220 --> 00:05:52,920
All it's saying is that the sun has been
up

96
00:05:52,920 --> 00:05:56,600
after the time of 7 o'clock and all times
since then.

97
00:05:56,600 --> 00:06:00,130
It doesn't indicate any kind of rational
relation.

98
00:06:00,130 --> 00:06:04,395
Such as, the fact that at 7 o'clock being
the reason why the sun came up.

99
00:06:04,395 --> 00:06:05,680
Or what about this one?

100
00:06:05,680 --> 00:06:08,520
It's been raining since my vacation began.

101
00:06:08,520 --> 00:06:09,630
Very disappointing, but you're not saying

102
00:06:09,630 --> 00:06:10,030
that it's raining because your vacation began

103
00:06:10,030 --> 00:06:10,600
as if there's some kind of plot against
you and the nature or weather.

104
00:06:10,600 --> 00:06:10,855
That would be very paranoid.

105
00:06:10,855 --> 00:06:16,406
All you're saying is that, it has been
raining

106
00:06:16,406 --> 00:06:22,460
everyday since the time when your

107
00:06:22,460 --> 00:06:29,910
vacation began, or everyday after your
vacation began.

108
00:06:29,910 --> 00:06:31,760
So the since there, indicates just a

109
00:06:31,760 --> 00:06:35,410
temporal relation, not some kind of
rational relation.

110
00:06:36,600 --> 00:06:39,170
And what this shows us is that you can't
just look

111
00:06:39,170 --> 00:06:42,330
for the word since and always mark it as
an argument marker.

112
00:06:42,330 --> 00:06:46,550
You have to think about what the word
since is doing in that context, and

113
00:06:46,550 --> 00:06:51,640
that'll be true for a lot of other reason
markers and conclusion markers as well.

114
00:06:51,640 --> 00:06:56,940
Here's another example the same point, but
with a conclusion marker, the word so.

115
00:06:56,940 --> 00:07:00,326
The word so sometimes indicates that, the
sentence after it

116
00:07:00,326 --> 00:07:03,110
is a conclusion and the sentence before
it's a reason.

117
00:07:03,110 --> 00:07:06,050
But it can also indicate something
entirely different.

118
00:07:06,050 --> 00:07:07,720
You don't need to eat so much.

119
00:07:09,000 --> 00:07:13,070
So there doesn't indicate that much is a
reason for anything.

120
00:07:14,180 --> 00:07:16,450
The word so is getting used in an entirely
different way.

121
00:07:16,450 --> 00:07:18,550
That should be obvious.

122
00:07:18,550 --> 00:07:20,710
But the point again, is that you can't
just look

123
00:07:20,710 --> 00:07:23,260
for the word so and label it as an
argument marker.

124
00:07:23,260 --> 00:07:26,940
You have to think about the function that
it's playing in the particular context.

125
00:07:26,940 --> 00:07:35,050
But then how can we tell what role a word
is playing in a particular context.

126
00:07:35,050 --> 00:07:36,410
Here's a little trick.

127
00:07:36,410 --> 00:07:40,680
Try substituting another word that's
clearly an argument marker.

128
00:07:40,680 --> 00:07:45,900
That is, it's clearly a reason marker or a
conclusion marker.

129
00:07:45,900 --> 00:07:49,290
For the original word that you weren't so
sure about.

130
00:07:50,460 --> 00:07:53,640
Here's an example.
He's so cool!

131
00:07:55,100 --> 00:07:59,740
Does that mean he's because cool?
No,

132
00:07:59,740 --> 00:08:03,530
if you substitute because for so the
meaning changes entirely.

133
00:08:04,650 --> 00:08:10,510
Not even clear what it means.
Or how about, he's therefore cool.

134
00:08:10,510 --> 00:08:12,370
That don't make much sense either.

135
00:08:12,370 --> 00:08:13,690
So in this case you can't

136
00:08:13,690 --> 00:08:18,130
substitute another argument marker because
or therefore

137
00:08:18,130 --> 00:08:23,700
for the original word so without changing
the meaning of the sentence entirely.

138
00:08:23,700 --> 00:08:25,750
And that shows you that,

139
00:08:25,750 --> 00:08:32,050
in the original sentence, the word so was
not being used, as an argument marker.

140
00:08:32,050 --> 00:08:34,550
Either a reason marker or conclusion
marker.

141
00:08:35,830 --> 00:08:36,660
Here's another example.

142
00:08:36,660 --> 00:08:39,970
Well, since he left college, he's been
unemployed.

143
00:08:41,760 --> 00:08:46,250
Well are you saying that, because he left
college he's unemployed?

144
00:08:46,250 --> 00:08:48,260
Maybe, if that's what you're saying then
you

145
00:08:48,260 --> 00:08:51,010
can substitute the word because without
changing the meaning

146
00:08:51,010 --> 00:08:52,130
of the sentence.

147
00:08:52,130 --> 00:08:57,185
And then the word since was being used as
a reason marker in that case.

148
00:08:57,185 --> 00:08:58,900
But probably not.

149
00:08:58,900 --> 00:09:02,500
Probably, the claim is simply that since
the

150
00:09:02,500 --> 00:09:05,220
time when he left college, he's been
unemployed.

151
00:09:06,260 --> 00:09:08,980
And then if that's what you mean, then
when

152
00:09:08,980 --> 00:09:12,390
you substitute because for since, it
changes the meaning.

153
00:09:14,190 --> 00:09:16,300
And that would show that

154
00:09:16,300 --> 00:09:19,580
in that context the word is not playing
the

155
00:09:19,580 --> 00:09:22,735
role of an argument marker, or a reason
marker.

156
00:09:22,735 --> 00:09:26,260
Because it changes the meaning to
substitute

157
00:09:26,260 --> 00:09:29,920
something like because that's clearly an
argument marker.

158
00:09:32,090 --> 00:09:38,050
On the other hand, you might have a
similar claim where you can substitute it.

159
00:09:39,980 --> 00:09:43,040
Since he failed out of college, he's
unemployed.

160
00:09:44,070 --> 00:09:44,410
Whoa!

161
00:09:44,410 --> 00:09:48,150
Now, if somebody says that, they don't
just mean since

162
00:09:48,150 --> 00:09:50,990
the time when he failed out of college
he's unemployed.

163
00:09:50,990 --> 00:09:53,700
Probably the meaning is not changed if you
simply

164
00:09:53,700 --> 00:09:57,840
said because he failed out of college,
he's unemployed.

165
00:09:59,960 --> 00:10:04,290
So, since you can substitute because for
since in that sentence, in

166
00:10:04,290 --> 00:10:08,560
that sentence the word since is probably
being used as an argument marker.

167
00:10:08,560 --> 00:10:09,870
That's the role it's playing.

168
00:10:09,870 --> 00:10:10,730
That's it's function.

169
00:10:10,730 --> 00:10:11,930
That's how it's being used.

170
00:10:13,090 --> 00:10:16,690
And notice also, that you can tell that
the word since

171
00:10:16,690 --> 00:10:21,110
in that sentence is a reason marker
instead of a conclusion marker.

172
00:10:21,110 --> 00:10:25,910
Because you can substitute because, and
you cannot substitute therefore.

173
00:10:26,910 --> 00:10:31,830
Therefore he failed out of college, he's
been unemployed.

174
00:10:31,830 --> 00:10:33,010
That doesn't make a lot of sense.

175
00:10:34,370 --> 00:10:39,810
So, if you can substitute because without
changing the meaning, then

176
00:10:39,810 --> 00:10:44,579
the original word, since in this case, was
being used as a reason marker.

177
00:10:46,270 --> 00:10:48,480
You cannot substitute the word therefore.

178
00:10:48,480 --> 00:10:51,780
That shows it's not being used as a
conclusion marker.

179
00:10:52,790 --> 00:10:56,000
So you can use this substitution test to
determine

180
00:10:56,000 --> 00:10:58,290
whether the word is being used as a reason
marker

181
00:10:58,290 --> 00:11:02,200
or as a conclusion marker, or as no
indicator

182
00:11:02,200 --> 00:11:05,570
whatsoever of an argument but in some
entirely different way.

183
00:11:06,910 --> 00:11:13,040
One last word that we have to talk about
is that little word if.

184
00:11:13,040 --> 00:11:18,060
Sometimes it's linked with the word then,
in an if-then clause, which is also

185
00:11:18,060 --> 00:11:19,420
called a conditional.

186
00:11:19,420 --> 00:11:22,954
And we'll talk a lot about conditionals
later in this course.

187
00:11:22,954 --> 00:11:24,850
But for now, I just want to make one
point.

188
00:11:26,390 --> 00:11:28,210
The word if might seem like an

189
00:11:28,210 --> 00:11:32,373
argument marker, because it's often used
in arguments.

190
00:11:32,373 --> 00:11:37,420
For example; I might say, If I'm rich
enough, I can buy a baseball team.

191
00:11:38,700 --> 00:11:41,773
I am rich enough, so I can buy a baseball
team.

192
00:11:41,773 --> 00:11:43,146
That would be

193
00:11:43,146 --> 00:11:47,830
an argument.
But if all I say is If I'm rich enough, I

194
00:11:47,830 --> 00:11:52,540
can buy a baseball team, when I know I'm
not rich enough.

195
00:11:52,540 --> 00:11:58,426
So, I would never assert the if clause
that says I am rich enough.

196
00:11:58,426 --> 00:12:04,270
Then, that little if sentence is not being
used to indicate an argument at all.

197
00:12:04,270 --> 00:12:08,570
Its just saying, if If I am rich enough,
then I can buy a baseball team.

198
00:12:08,570 --> 00:12:10,420
It's not saying that I am rich enough, and

199
00:12:10,420 --> 00:12:12,810
it's not saying that I can buy a baseball
team.

200
00:12:12,810 --> 00:12:18,640
So, the word if by itself does not
indicate an argument.

201
00:12:18,640 --> 00:12:23,480
It sets a pattern for argument, if one
thing then another.

202
00:12:23,480 --> 00:12:29,380
Well, the one thing, therefore, the other.
But the IF (one thing) THEN (another)

203
00:12:29,380 --> 00:12:34,330
doesn't by itself indicate any argument at
all because it doesn't assert that

204
00:12:34,330 --> 00:12:38,610
IF clause which is also called the
antecedent of the conditional.

205
00:12:38,610 --> 00:12:44,800
So we are not going to count the word if
as an argument marker.

206
00:12:44,800 --> 00:12:46,705
Now we've learned how to identify an
argument.

207
00:12:46,705 --> 00:12:48,220
It's simple, huh?

208
00:12:48,220 --> 00:12:51,980
But just to make sure you've got it
straight, let's do a few exercises.

