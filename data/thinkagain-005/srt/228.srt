1
00:00:00,062 --> 00:00:06,160
Well I hope the first lecture convinced
you that arguments really matter.

2
00:00:06,160 --> 00:00:07,885
Of course, they're not the only things
that

3
00:00:07,885 --> 00:00:10,430
matters, there's more to life than reason
and arguments.

4
00:00:10,430 --> 00:00:14,290
but they are something that matters and
they matter a lot.

5
00:00:14,290 --> 00:00:16,920
So, we need to understand arguments.

6
00:00:16,920 --> 00:00:19,740
Now, the first step in understanding
arguments

7
00:00:19,740 --> 00:00:22,920
is to figure out what arguments are.

8
00:00:22,920 --> 00:00:25,290
And the first step in understanding what
arguments are

9
00:00:25,290 --> 00:00:27,910
is to figure out what arguments are not.

10
00:00:27,910 --> 00:00:30,170
Because we want to distinguish arguments
from all

11
00:00:30,170 --> 00:00:32,610
of those things that don't count as
arguing.

12
00:00:32,610 --> 00:00:36,080
And the best source of information about
what

13
00:00:36,080 --> 00:00:40,500
arguments are not, is, of course, Monty
Python.

14
00:00:40,500 --> 00:00:41,940
Well, that was pretty silly wasn't it?

15
00:00:41,940 --> 00:00:45,640
But in the midst of all that silliness we
find some truth

16
00:00:45,640 --> 00:00:47,960
because after all, many members of

17
00:00:47,960 --> 00:00:50,330
the Monty Python troupe were philosophy
majors.

18
00:00:50,330 --> 00:00:54,360
So each room represents a kind of

19
00:00:54,360 --> 00:00:57,950
thing that we need to distinguish from
arguments.

20
00:00:57,950 --> 00:00:57,950
[INAUDIBLE]

21
00:00:57,950 --> 00:00:57,950
.

22
00:00:57,950 --> 00:01:00,140
So let's think first about getting hit on
the head lessons.

23
00:01:00,140 --> 00:01:00,140
[NOISE]

24
00:01:00,140 --> 00:01:04,690
.
Arguments are not like hitting people on

25
00:01:04,690 --> 00:01:12,129
the head, you hit poeple on the head when
you wrestle.

26
00:01:12,129 --> 00:01:12,160
[NOISE].

27
00:01:12,160 --> 00:01:14,779
The point is that arguments are not fights
you

28
00:01:14,779 --> 00:01:18,150
don't win an argument by hitting somebody
on the head.

29
00:01:18,150 --> 00:01:22,730
Sometimes little children say that their
parents are

30
00:01:22,730 --> 00:01:25,735
arguing, when they're really having a
verbal fight.

31
00:01:25,735 --> 00:01:29,400
>>All this fighting I might as well be
back with my parents.

32
00:01:29,400 --> 00:01:32,320
Dammit George, I told you if you didn't
quit drinking I'd leave you.

33
00:01:32,320 --> 00:01:33,970
Well, I guess that makes you a liar

34
00:01:33,970 --> 00:01:35,952
because I'm drunk as hell and you're still
here.

35
00:01:35,952 --> 00:01:38,020
>> But

36
00:01:38,020 --> 00:01:41,540
you can not win an argument just by
yelling at someone.

37
00:01:42,960 --> 00:01:49,500
That doesn't make the argument any better
because that's not the point of arguing.

38
00:01:49,500 --> 00:01:52,972
Another room in the Monty Python skit
involves abuse.

39
00:01:52,972 --> 00:01:57,680
>> Don't give me that you snotty-faced
heap of parrot droppings.

40
00:01:57,680 --> 00:01:59,250
>> Now abuse is one of the things you

41
00:01:59,250 --> 00:02:02,970
do with language, but it's not the same as
arguing.

42
00:02:02,970 --> 00:02:08,878
You cannot win an argument simply by
calling your opponent a stupid git.

43
00:02:08,878 --> 00:02:10,477
>> Stupid git.

44
00:02:10,477 --> 00:02:12,460
>> And the point of this course is not
to teach you

45
00:02:12,460 --> 00:02:16,790
to go back and abuse your roommate by
calling them nasty names.

46
00:02:16,790 --> 00:02:19,370
That will not help you win any argument.

47
00:02:19,370 --> 00:02:21,275
It also won't help you win any friends.

48
00:02:21,275 --> 00:02:24,480
And another room in the skit has to do
with complaining.

49
00:02:24,480 --> 00:02:24,480
>>

50
00:02:24,480 --> 00:02:24,480
[INAUDIBLE].

51
00:02:24,480 --> 00:02:31,470
>> But all those complaints don't amount
to an argument either.

52
00:02:31,470 --> 00:02:35,270
They're just expressing your emotion about
the situation.

53
00:02:35,270 --> 00:02:38,370
Arguing is something different from all of
those rooms.

54
00:02:38,370 --> 00:02:39,520
So what is arguing?

55
00:02:39,520 --> 00:02:42,625
Well, at one point one of the character
says.

56
00:02:42,625 --> 00:02:44,324
>> Well argument is not the same as
contradiction.

57
00:02:44,324 --> 00:02:45,286
>>Can be.

58
00:02:45,286 --> 00:02:46,380
>> No, it can't.

59
00:02:46,380 --> 00:02:48,090
>> So what do they mean by a
contradiction?

60
00:02:48,090 --> 00:02:50,180
In British English to say a contradiction

61
00:02:50,180 --> 00:02:54,265
is just to deny the person or contradict
what they said.

62
00:02:54,265 --> 00:03:00,110
but contradicting what the person said,
that is denying it, is not arguing.

63
00:03:00,110 --> 00:03:05,120
I can say, what do you think the best
flavor of ice cream in the world?

64
00:03:05,120 --> 00:03:06,370
Well I have my favorite.

65
00:03:06,370 --> 00:03:07,560
I know what the best flavor is.

66
00:03:07,560 --> 00:03:10,230
The best flavor is Ben and Jerry's Coconut

67
00:03:10,230 --> 00:03:13,750
Almond Fudge Chip ice cream, There's
nothing better.

68
00:03:13,750 --> 00:03:15,130
And then you say, no it isn't.

69
00:03:15,130 --> 00:03:19,060
Well you haven't argued that it isn't and
I haven't

70
00:03:19,060 --> 00:03:22,580
argued that it is, we're just disagreeing
with each other.

71
00:03:22,580 --> 00:03:27,270
We haven't given any reason for any of the
positions that we've adopted yet.

72
00:03:27,270 --> 00:03:30,120
So, as Monty Python says later on,

73
00:03:30,120 --> 00:03:35,270
in different character, argument is an
intellectual process.

74
00:03:35,270 --> 00:03:40,250
It's a process not just of asserting your
views, but of giving some kind of

75
00:03:40,250 --> 00:03:41,620
reason for your views.

76
00:03:42,750 --> 00:03:47,659
So the next definition that Monty Python
gives of an argument, is

77
00:03:47,659 --> 00:03:52,000
that an argument is a connected series of
statements to establish a proposition.

78
00:03:52,000 --> 00:03:55,930
I take it they mean intended to establish
a certain proposition.

79
00:03:55,930 --> 00:03:58,650
So that's a pretty cool definition, if you
think about

80
00:03:58,650 --> 00:04:02,010
it, because it tells you what an argument
is made of.

81
00:04:02,010 --> 00:04:05,230
It's a series of statements, and
statements are made in language,

82
00:04:05,230 --> 00:04:06,970
so arguments are made of language.

83
00:04:06,970 --> 00:04:10,090
It also tells you what the purpose of
argument is.

84
00:04:10,090 --> 00:04:14,990
The purpose of argument, they say, is to
establish a certain proposition.

85
00:04:14,990 --> 00:04:17,490
So now we have a pretty unique definition
of argument.

86
00:04:19,070 --> 00:04:21,518
This definition gives us a nice contrast.

87
00:04:21,518 --> 00:04:25,140
Because there are lots of other series of
statements or sentences that

88
00:04:25,140 --> 00:04:29,490
don't count as arguments because they're
not intended to establish a proposition.

89
00:04:29,490 --> 00:04:30,520
Consider for example

90
00:04:30,520 --> 00:04:34,190
a novel, which has statements about what's
going on,

91
00:04:34,190 --> 00:04:38,970
but it's not necessarily trying to
establish any particular proposition.

92
00:04:38,970 --> 00:04:41,910
Or a dictionary might have a series of
definitions,

93
00:04:41,910 --> 00:04:46,370
but it's not intended to establish a
certain proposition.

94
00:04:46,370 --> 00:04:51,300
Instead, novels and dictionaries order
sentences in a different way.

95
00:04:51,300 --> 00:04:55,450
They order them either chronologically or
alphabetically whereas,

96
00:04:55,450 --> 00:05:00,510
arguments are trying to put statements
into a certain structure that reflects the

97
00:05:00,510 --> 00:05:06,530
order of reasoning in order to establish
the proposition according to Monty Python.

98
00:05:06,530 --> 00:05:12,720
But Monty Python, no matter how great they
are, and they are great, didn't get it

99
00:05:12,720 --> 00:05:15,950
quite right because the purpose of an
argument

100
00:05:15,950 --> 00:05:20,890
is not always to establish a proposition
because

101
00:05:20,890 --> 00:05:24,520
some propositions that are conclusions of
arguments, we already knew.

102
00:05:25,530 --> 00:05:28,150
Consider for example a mathematical proof.

103
00:05:29,560 --> 00:05:33,190
If someone tries to prove the Pythagorean
theorem in geometry.

104
00:05:33,190 --> 00:05:35,550
People already believe the theorem.

105
00:05:36,690 --> 00:05:39,340
They already knew that it was true.

106
00:05:39,340 --> 00:05:42,560
So they weren't trying to establish the
proposition.

107
00:05:42,560 --> 00:05:46,120
But the proof does something else, it
shows you how that proposition

108
00:05:46,120 --> 00:05:48,300
is connected to the axioms of the system.

109
00:05:48,300 --> 00:05:52,240
It helps you understand why the
proposition is true.

110
00:05:52,240 --> 00:05:55,720
And, we'll see that other arguments, like
explanations, do the same thing.

111
00:05:55,720 --> 00:05:58,840
So sometimes arguments are intended to
establish a

112
00:05:58,840 --> 00:06:02,140
proposition, like Monty Python said, but
in other

113
00:06:02,140 --> 00:06:05,070
cases they're intended to help us
understand the

114
00:06:05,070 --> 00:06:08,550
proposition and the reasons why the
proposition is true.

115
00:06:08,550 --> 00:06:11,830
So we want to distinguish reasons to
believe that the proposition

116
00:06:11,830 --> 00:06:15,680
is true, from reasons why the proposition
is true.

117
00:06:15,680 --> 00:06:18,600
and arguments can do both of those things.

118
00:06:18,600 --> 00:06:22,510
So, we need a somewhat broader definition
of

119
00:06:22,510 --> 00:06:25,179
argument to cover these different kinds of
reasons.

120
00:06:26,260 --> 00:06:32,370
We'll think of an argument as a connected
series of sentences

121
00:06:32,370 --> 00:06:37,150
or statements or propositions, where some
of these sentences or statements or

122
00:06:37,150 --> 00:06:42,130
propositions or premises and one of them
is the conclusion and

123
00:06:42,130 --> 00:06:47,810
the one's that are premises are intended
to provide some kind of reason

124
00:06:47,810 --> 00:06:53,110
for the one that's the conclusion.
This definition is useful in many ways.

125
00:06:53,110 --> 00:06:56,100
First of all, it tells us what the parts

126
00:06:56,100 --> 00:06:59,250
of the arguments are, the premises and the
conclusion.

127
00:06:59,250 --> 00:07:02,580
Secondly, it tells you what the argument's
made of.

128
00:07:02,580 --> 00:07:05,550
It's made up of language because sentences

129
00:07:05,550 --> 00:07:08,340
and statements and propositions are made
in language.

130
00:07:09,590 --> 00:07:11,880
Third, it tells you the purpose of

131
00:07:11,880 --> 00:07:15,580
argument, to give a reason for the
conclusion.

132
00:07:16,790 --> 00:07:21,230
Fourth, a nice feature is that it's very
flexible

133
00:07:21,230 --> 00:07:23,710
because there are lots of different kinds
of reasons.

134
00:07:23,710 --> 00:07:26,165
We don't want our definition to be too
narrow

135
00:07:26,165 --> 00:07:27,800
because then it won't cover all the
different kinds

136
00:07:27,800 --> 00:07:31,830
of arguments, and the notion of reason
captures the different kinds of

137
00:07:31,830 --> 00:07:33,440
relations between the premises and the

138
00:07:33,440 --> 00:07:35,820
conclusion in different kinds of
arguments.

139
00:07:37,120 --> 00:07:39,070
So let's do a few quick exercises to

140
00:07:39,070 --> 00:07:41,280
make sure that you understand how this
definition works.

