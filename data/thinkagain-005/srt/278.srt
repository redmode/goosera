1
00:00:00,025 --> 00:00:06,191
Finally, we've finished reconstruction.
Yipee!

2
00:00:06,191 --> 00:00:07,484
Right?

3
00:00:07,484 --> 00:00:09,360
Oh no, not quite.

4
00:00:09,360 --> 00:00:13,370
Because there's one more stage and that
stage is drawing the conclusion.

5
00:00:13,370 --> 00:00:19,740
Of course, if we've come up with a sound
reconstruction, then we know that

6
00:00:19,740 --> 00:00:22,560
the argument's sound and we know, that

7
00:00:22,560 --> 00:00:25,070
the conclusion is true, because every
sound argument

8
00:00:25,070 --> 00:00:26,115
has a true conclusion.

9
00:00:26,115 --> 00:00:32,660
But, if we don't come up with a sound
reconstruction, then what do we say?

10
00:00:33,800 --> 00:00:39,380
Well, we gotta ask, who's fault is it.
It might be the fault of the argument.

11
00:00:39,380 --> 00:00:40,940
Maybe we couldn't come up with a sound

12
00:00:40,940 --> 00:00:43,720
reconstruction 'cuz there just is no sound
reconstruction.

13
00:00:43,720 --> 00:00:46,470
But maybe we didn't come up with a

14
00:00:46,470 --> 00:00:49,410
sound reconstruction because we just
weren't imaginative enough.

15
00:00:49,410 --> 00:00:50,390
Or didn't try hard enough.

16
00:00:51,780 --> 00:00:55,010
Still, if we try really long and hard.

17
00:00:55,010 --> 00:00:58,660
And charitably interpret the argument as
best we can to make it

18
00:00:58,660 --> 00:01:02,040
look as good as we can, and we still can't
make it sound.

19
00:01:02,040 --> 00:01:06,455
Then, we've at least got reason to believe
that the argument's not sound.

20
00:01:06,455 --> 00:01:10,360
Of course, that doesn't mean that the
conclusion's not true.

21
00:01:10,360 --> 00:01:13,250
Because unsound arguments can still have
true conclusions.

22
00:01:14,550 --> 00:01:17,180
But at least we know that this argument
doesn't

23
00:01:17,180 --> 00:01:19,910
prove that the conclusion's true.

24
00:01:19,910 --> 00:01:24,660
And so this method of reconstruction can
lead us either to the

25
00:01:24,660 --> 00:01:29,960
belief that the argument is sound because
we found the sound reconstruction.

26
00:01:29,960 --> 00:01:32,920
Or to the conclusion that the argument's
not

27
00:01:32,920 --> 00:01:35,985
sound because we tried long and hard to
find

28
00:01:35,985 --> 00:01:38,860
a sound reconstruction and didn't, but
that's still not

29
00:01:38,860 --> 00:01:40,925
going to show us that's the conclusion of
the argument.

30
00:01:40,925 --> 00:01:41,930
Is false.

31
00:01:43,420 --> 00:01:47,280
The point of reconstruction then, is to
reach a conclusion

32
00:01:47,280 --> 00:01:49,610
on this issue of whether the argument is
sound or not.

33
00:01:49,610 --> 00:01:53,300
And if we try our best and do it as well
as we can and

34
00:01:53,300 --> 00:01:57,139
charitably, then we can be justified in

35
00:01:57,139 --> 00:01:59,430
believing that the argument is sound or
not.

