1
00:00:02,940 --> 00:00:05,790
The second common move in arguments that's
supposed

2
00:00:05,790 --> 00:00:09,650
to solve the skeptical regress problem is
guarding.

3
00:00:09,650 --> 00:00:11,955
The basic idea of guarding is very simple.

4
00:00:11,955 --> 00:00:14,730
It's just making the premises of your

5
00:00:14,730 --> 00:00:17,760
argument weaker so that there will be
fewer

6
00:00:17,760 --> 00:00:19,200
ways in which your opponents can raise

7
00:00:19,200 --> 00:00:21,910
trouble for them, or show that they're
false.

8
00:00:21,910 --> 00:00:23,300
So here's an example.

9
00:00:23,300 --> 00:00:28,180
Suppose someone says, we ought not to
build any new nuclear power

10
00:00:28,180 --> 00:00:30,030
plants because they'll explode.

11
00:00:32,180 --> 00:00:32,845
Now wait a minute.

12
00:00:32,845 --> 00:00:38,277
How do you know that any of them really
will explode?

13
00:00:38,277 --> 00:00:41,660
How can you tell that in the future?

14
00:00:41,660 --> 00:00:44,430
You don't even know what kinds of
standards they're going to be built to.

15
00:00:45,840 --> 00:00:50,210
So someone could object to that argument
by saying you're not justified

16
00:00:50,210 --> 00:00:53,960
in asserting that premise that these new
nuclear power plants will explode.

17
00:00:55,100 --> 00:00:57,520
So how do you stop that problem?

18
00:00:57,520 --> 00:01:02,932
Well, you simply say, well, we ought not
to build any new nuclear power

19
00:01:02,932 --> 00:01:08,315
plants because some of them might explode.
Or even weaker,

20
00:01:08,315 --> 00:01:13,690
because I believe that some of them might
explode.

21
00:01:13,690 --> 00:01:15,260
Now we've got a premise.

22
00:01:15,260 --> 00:01:19,500
I believe that some of the new nuclear
power plants might explode.

23
00:01:20,740 --> 00:01:21,968
That's going to be hard to deny.

24
00:01:21,968 --> 00:01:22,409
I mean,

25
00:01:22,409 --> 00:01:26,170
what's an opponent going to do?
Show that you don't really believe it?

26
00:01:26,170 --> 00:01:28,930
Show that it's not true, that they might
explode, that there's

27
00:01:28,930 --> 00:01:32,740
no possibility that they'll explode, or
that any of them will explode?

28
00:01:32,740 --> 00:01:35,660
You know I couldn't mean all of them will,
just some of them.

29
00:01:35,660 --> 00:01:38,790
So by guarding the premise in this way,
you make

30
00:01:38,790 --> 00:01:43,510
it more likely to be true and less subject
to objection.

31
00:01:43,510 --> 00:01:44,830
And that's what guarding does.

32
00:01:45,980 --> 00:01:47,460
It enables you to start an

33
00:01:47,460 --> 00:01:55,394
argument with that premise if other people
agree that it's true because it's so weak.

34
00:01:55,394 --> 00:02:00,840
You get them to agree to share your
assumption by weakening your premises.

35
00:02:00,840 --> 00:02:06,900
Now of course, what happens now if someone
says wait a minute, they might explode.

36
00:02:06,900 --> 00:02:09,040
Sure, and the sun might not come up
tomorrow.

37
00:02:09,040 --> 00:02:11,625
All kinds of things might not happen or
might happen.

38
00:02:11,625 --> 00:02:16,690
Might's too weak to establish that we
shouldn't have new nuclear power plants.

39
00:02:16,690 --> 00:02:18,750
So what you're suggesting is not just that

40
00:02:18,750 --> 00:02:21,220
it might happen but that it's likely to
happen.

41
00:02:22,600 --> 00:02:25,780
And if the chance of it happening really

42
00:02:25,780 --> 00:02:32,220
is so slight as to be negligible, then
you've

43
00:02:32,220 --> 00:02:34,550
weakened the premise too much, and it's
not going

44
00:02:34,550 --> 00:02:36,860
to follow that we shouldn't have nuclear
power plants.

45
00:02:36,860 --> 00:02:43,150
So the issue's going to come down to is
the risk of them exploding, or some

46
00:02:43,150 --> 00:02:49,094
of them exploding, enough to justify the
conclusion that we ought not to have them.

47
00:02:49,094 --> 00:02:51,520
And that's going to depend on exactly how
much

48
00:02:51,520 --> 00:02:55,530
risk there is, and by looking at the
guarding term,

49
00:02:55,530 --> 00:02:58,360
at the might term, and questioning it, and
saying,

50
00:02:58,360 --> 00:02:59,995
can we replace might with probably, or
something like that?

51
00:02:59,995 --> 00:03:06,850
Then we're going to have a better handle
on how to assess the argument.

52
00:03:06,850 --> 00:03:09,870
So, when you see someone using guarding
like this you

53
00:03:09,870 --> 00:03:13,540
need to ask, why did they put in the
guard,

54
00:03:13,540 --> 00:03:16,680
And have they put in too much guarding
that it's

55
00:03:16,680 --> 00:03:20,820
weakened it so much that the conclusion no
longer follows?

56
00:03:22,180 --> 00:03:25,070
So the general trick of guarding is to
weaken the

57
00:03:25,070 --> 00:03:26,440
premise, so it's going to be harder to

58
00:03:26,440 --> 00:03:28,889
deny, and that's how you argue against
guarding.

59
00:03:28,889 --> 00:03:31,735
But there are at least three different
ways to do this.

60
00:03:31,735 --> 00:03:34,470
Okay?
One is the extent.

61
00:03:34,470 --> 00:03:38,035
The other is probability, and the other is
mental.

62
00:03:38,035 --> 00:03:44,150
First, guarding by extent.
We need a new

63
00:03:44,150 --> 00:03:50,770
alcohol officer on our campus, because all
students drink too much.

64
00:03:50,770 --> 00:03:53,940
Well that's clearly false.
Not all students drink too much.

65
00:03:55,300 --> 00:04:03,230
Most students drink too much.
Well not most, maybe not most.

66
00:04:03,230 --> 00:04:04,990
Many students drink too much.

67
00:04:06,765 --> 00:04:08,940
Okay many, too many, because it's too
much.

68
00:04:09,970 --> 00:04:12,270
Some students drink too much.

69
00:04:12,270 --> 00:04:16,200
Notice that you can guard or weaken the
claim,

70
00:04:16,200 --> 00:04:21,960
the premise, from all students drink too
much, to most students

71
00:04:21,960 --> 00:04:27,060
drink too much, to many students drink too
much to some students drink too much.

72
00:04:27,060 --> 00:04:33,370
And as you move down that scale, the
premise gets harder and harder to deny.

73
00:04:34,750 --> 00:04:40,370
So, whether this counts as guarding
depends on the expectation.

74
00:04:40,370 --> 00:04:41,280
If you expect the

75
00:04:41,280 --> 00:04:46,680
claim that most or all students drink too
much, then it's guarding to say many,

76
00:04:46,680 --> 00:04:52,050
or certainly to say some.
But, if you don't expect that many or

77
00:04:52,050 --> 00:04:58,170
most or all, than to say some, is simply
to say that you're talking about some.

78
00:04:58,170 --> 00:05:02,450
So, it's guarding when you weaken it
beyond

79
00:05:02,450 --> 00:05:05,900
what would otherwise be expected in the
context.

80
00:05:05,900 --> 00:05:06,490
This standard

81
00:05:06,490 --> 00:05:10,600
is going to be hard to apply because it
might be difficult to say what

82
00:05:10,600 --> 00:05:13,519
the expectations are of the different
people involved

83
00:05:13,519 --> 00:05:17,350
in a conversation, but that's what
guarding is.

84
00:05:17,350 --> 00:05:21,120
It's not guarding every time you use the
word many or most.

85
00:05:21,120 --> 00:05:24,475
It's only guarding when you're expecting
all,

86
00:05:24,475 --> 00:05:28,053
and the person instead merely claimed
many.

87
00:05:28,053 --> 00:05:33,670
The second kind of guarding concerns
probability.

88
00:05:33,670 --> 00:05:36,700
Some people would say it's absolutely
certain that O.J.

89
00:05:36,700 --> 00:05:37,900
Simpson killed his wife.

90
00:05:39,380 --> 00:05:41,990
And other people say well he probably
killed his

91
00:05:41,990 --> 00:05:45,590
wife, or it's likely that he killed his
wife.

92
00:05:45,590 --> 00:05:48,630
And others will say, there's a chance he
killed his

93
00:05:48,630 --> 00:05:52,230
wife, another is, will say, he might have
killed his wife.

94
00:05:52,230 --> 00:05:59,329
So when you change from, it's certain, to
it's probable, or it's likely, or there's

95
00:05:59,329 --> 00:06:01,800
a chance, he might have.

96
00:06:01,800 --> 00:06:06,200
Again, you're moving down a continuum, and
the further you move down that

97
00:06:06,200 --> 00:06:10,840
continuum, the easier it is to defend your
premise because you're claiming less.

98
00:06:12,860 --> 00:06:17,050
And the question is going to be whether
you've done it too much.

99
00:06:17,050 --> 00:06:19,280
If you simply say he might have killed his

100
00:06:19,280 --> 00:06:23,867
wife, therefore he ought to be convicted,
that's clearly wrong.

101
00:06:23,867 --> 00:06:29,580
If you say probably killed his wife, so he
ought to be convicted, that might be

102
00:06:29,580 --> 00:06:32,010
wrong, too, if there's a strong burden on

103
00:06:32,010 --> 00:06:35,020
the prosecution to prove guilt beyond a
reasonable doubt.

104
00:06:36,280 --> 00:06:38,410
But you don't want to require that it's
certain

105
00:06:38,410 --> 00:06:39,950
that he killed his wife because then

106
00:06:39,950 --> 00:06:42,880
you'll never be able to convict any
criminals.

107
00:06:42,880 --> 00:06:45,290
So you need something like almost certain.

108
00:06:45,290 --> 00:06:50,350
You know, it's a bit of a fudge word, or
beyond a reasonable doubt.

109
00:06:50,350 --> 00:06:53,440
In any case, however you assess whether or
not he

110
00:06:53,440 --> 00:06:56,750
should have been convicted depending on
how you assess how likely

111
00:06:56,750 --> 00:06:59,220
it was he really did what he was accused
of,

112
00:07:00,700 --> 00:07:04,620
apart from all those questions, the point
here is simply that

113
00:07:04,620 --> 00:07:10,030
in arguments in general, you can make the
argument more defensible by

114
00:07:10,030 --> 00:07:14,782
weakening the premise so there are fewer
ways to show that the premise is wrong.

115
00:07:14,782 --> 00:07:17,825
And that's the second type of guarding.

116
00:07:17,825 --> 00:07:21,450
Now a third type of guarding we can call
mental, because it

117
00:07:21,450 --> 00:07:24,898
has to do with the mental state of the
person asserting the premise.

118
00:07:24,898 --> 00:07:30,280
You might say, well, I know that the
President is 50 years old.

119
00:07:30,280 --> 00:07:34,320
But you might say, I believe that the
President is 50 years old.

120
00:07:34,320 --> 00:07:36,770
You might say, I tend to believe, or, I'm

121
00:07:36,770 --> 00:07:40,990
inclined to believe that the President is
50 years old.

122
00:07:40,990 --> 00:07:43,180
And there's another continuum as you move

123
00:07:43,180 --> 00:07:47,020
from knowledge to belief to inclination to
believe.

124
00:07:47,020 --> 00:07:50,110
Again, you're making the premise weaker
and weaker, which makes it

125
00:07:50,110 --> 00:07:55,330
harder and harder to question or deny or
doubt that premise.

126
00:07:55,330 --> 00:07:57,800
So you avoided a problem for your
argument, and

127
00:07:57,800 --> 00:08:01,763
potentially this is a way to, stop the
skeptical regress.

