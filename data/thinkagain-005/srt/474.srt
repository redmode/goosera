1
00:00:03,710 --> 00:00:06,950
In the last lecture we saw how the word
and in

2
00:00:06,950 --> 00:00:08,650
English can sometimes be used

3
00:00:08,650 --> 00:00:12,520
to express the truth-functional connective
conjunction.

4
00:00:12,520 --> 00:00:17,160
And we defined conjunction by means of a
truth table for conjunction.

5
00:00:17,160 --> 00:00:20,780
Now you'll notice, in the last version of
the truth table that was presented in the

6
00:00:20,780 --> 00:00:25,850
last lecture, the propositions that were
conjoined in

7
00:00:25,850 --> 00:00:29,730
that truth table were called P and Q.

8
00:00:29,730 --> 00:00:35,760
Now, you might wonder, what's up with
calling propositions P and Q?

9
00:00:35,760 --> 00:00:38,590
Why are we using letters to refer to
propositions?

10
00:00:38,590 --> 00:00:41,090
Is this some kind of strange algebra?

11
00:00:43,230 --> 00:00:46,060
Well, today's lecture answers that
question.

12
00:00:46,060 --> 00:00:48,940
Today's lecture is about variables.

13
00:00:50,330 --> 00:00:55,990
Like the variables that occur in Algebra
and similar variables that occur in

14
00:00:55,990 --> 00:01:01,880
deductive logic, in the study of the rules
for valid argument, which is exactly what

15
00:01:01,880 --> 00:01:08,940
unit two is about.
So, let's begin by talking about rules and

16
00:01:08,940 --> 00:01:12,230
why we use variables when we state rules.

17
00:01:17,800 --> 00:01:21,760
The first point to make about rules is
that rules are general.

18
00:01:23,140 --> 00:01:26,640
Rules are designed to apply to many
different possible cases.

19
00:01:27,700 --> 00:01:32,950
So for instance, if I say to one of my
kids, stop hitting your brother right now.

20
00:01:34,320 --> 00:01:37,790
Well, that's not a rule.
That's a command, right?

21
00:01:37,790 --> 00:01:41,739
I'm telling one of my kids stop hitting
your brother right now.

22
00:01:42,810 --> 00:01:45,090
But its not a rule.
It's not general.

23
00:01:45,090 --> 00:01:48,390
It doesn't apply to many different
possible people.

24
00:01:48,390 --> 00:01:51,026
It doesn't apply to many different
possible situations.

25
00:01:51,026 --> 00:01:54,390
I'm just telling someone what to do, or in
this case what not to do,

26
00:01:56,660 --> 00:02:01,910
but if I said you should never under any
circumstances hit another

27
00:02:01,910 --> 00:02:06,870
person, well now, that's a rule.
It's inherently general.

28
00:02:06,870 --> 00:02:09,470
It applies to many different possible

29
00:02:09,470 --> 00:02:11,920
situations and many different possible
people.

30
00:02:13,860 --> 00:02:16,470
So is, you should never hit your brother.

31
00:02:16,470 --> 00:02:18,410
That's also a rule, because again,

32
00:02:18,410 --> 00:02:20,800
it applies to many different possible
situations.

33
00:02:22,230 --> 00:02:27,980
So rules are general.
But commands may or may not be general.

34
00:02:30,820 --> 00:02:33,840
So, how can we express the generality of a
rule?

35
00:02:35,330 --> 00:02:39,350
Well notice, if I say something that's not
general, if I say to

36
00:02:39,350 --> 00:02:43,660
one of my kids, stop hitting your brother
right now, well, I'm using the

37
00:02:43,660 --> 00:02:50,270
expression right now to indicate that my
order, my command, applies to a particular

38
00:02:50,270 --> 00:02:55,320
situation, namely the situation that we're
in right now when I issued the order.

39
00:02:56,760 --> 00:02:59,990
But if I say you should never, under any
circumstances,

40
00:02:59,990 --> 00:03:04,250
hit another person, I'm using the pronoun
you and the

41
00:03:04,250 --> 00:03:09,200
expression never under any circumstances
to indicate that the rule

42
00:03:09,200 --> 00:03:13,940
that I'm stating applies to a multitude of
possible situations.

43
00:03:13,940 --> 00:03:17,120
In fact the pronoun you, although it's a
second person

44
00:03:17,120 --> 00:03:21,890
pronoun it needn't be understood as
referring merely to the person

45
00:03:21,890 --> 00:03:22,680
I'm addressing.

46
00:03:22,680 --> 00:03:28,380
It could be understood quite generally,
think about the Ten Commandments.

47
00:03:28,380 --> 00:03:32,561
Thou shall not kill, thou is an archaic
form

48
00:03:32,561 --> 00:03:38,530
of the second person singular you, or in
French tu.

49
00:03:38,530 --> 00:03:43,020
So, in those commandments the second
person pronoun is

50
00:03:43,020 --> 00:03:47,230
used not to refer to the specific person,
to some

51
00:03:47,230 --> 00:03:51,760
specific person addressed by the
commandment, but to refer to anyone at

52
00:03:51,760 --> 00:03:56,439
all.
You, whoever you are, should not kill.

53
00:03:56,439 --> 00:04:04,460
Now notice, generality comes in degrees.
And rules can be more or less general.

54
00:04:05,760 --> 00:04:10,225
So I could say, you should never under any
circumstances hit another person.

55
00:04:10,225 --> 00:04:11,920
Well, that's a general rule.

56
00:04:13,160 --> 00:04:13,280
But

57
00:04:13,280 --> 00:04:15,710
I could state a rule that's even more
general than that.

58
00:04:15,710 --> 00:04:17,330
I could say, you should never under

59
00:04:17,330 --> 00:04:21,190
any circumstances do violence to another
person.

60
00:04:21,190 --> 00:04:25,040
Now that's more general, cause I'm not
just prohibiting hitting.

61
00:04:25,040 --> 00:04:26,940
I'm prohibiting any kind of violence,

62
00:04:26,940 --> 00:04:32,390
whether it's hitting, kicking, biting,
smacking, slapping.

63
00:04:32,390 --> 00:04:34,980
I could make a rule that's even more
general than that.

64
00:04:34,980 --> 00:04:38,520
I could say, you should never under any
circumstances do something

65
00:04:38,520 --> 00:04:41,180
unkind to another person.

66
00:04:41,180 --> 00:04:44,720
Whether the unkind thing in question is a
form of violence

67
00:04:44,720 --> 00:04:51,330
or merely an insulting action, a demeaning
action, a condescending action.

68
00:04:51,330 --> 00:04:52,850
There are lots of different ways of doing

69
00:04:52,850 --> 00:04:55,520
things that are unkind, even if they're
not violent.

70
00:04:56,990 --> 00:05:01,670
So, here we have rules of different levels
of generality.

71
00:05:03,050 --> 00:05:03,720
If I say,

72
00:05:05,320 --> 00:05:07,870
Walter should not force his dog to kill
his

73
00:05:07,870 --> 00:05:12,090
cat, I'm saying something that has some
generality to it.

74
00:05:12,090 --> 00:05:15,700
Presumably what I'm saying doesn't simply
apply to the present

75
00:05:15,700 --> 00:05:20,540
moment, but it applies more generally to a
multitude of moments.

76
00:05:20,540 --> 00:05:24,090
Not just the present, but maybe the recent
past, maybe the

77
00:05:24,090 --> 00:05:28,730
near future, but of course, I can say
something even more general.

78
00:05:28,730 --> 00:05:30,620
I can say, people should not force

79
00:05:30,620 --> 00:05:35,570
creatures to kill innocent creatures,
right, that's more general.

80
00:05:35,570 --> 00:05:39,260
I'm not just talking about Walter, I'm
talking about people quite generally.

81
00:05:39,260 --> 00:05:41,280
I'm not just talking about Walter's dog,

82
00:05:41,280 --> 00:05:43,960
but I'm talking about creatures quite
generally.

83
00:05:43,960 --> 00:05:45,500
And I'm not just talking about Walter's

84
00:05:45,500 --> 00:05:48,950
cat, I'm talking about innocent creatures
quite generally.

85
00:05:48,950 --> 00:05:52,210
And I'm saying that a certain relationship

86
00:05:52,210 --> 00:05:55,000
between those three things should not
hold.

87
00:05:55,000 --> 00:05:56,120
People should not

88
00:05:56,120 --> 00:05:58,160
force creatures to kill innocent
creatures.

89
00:05:59,800 --> 00:06:04,340
But now, notice that last sentence is
unclear.

90
00:06:04,340 --> 00:06:07,010
In particular it has two interpretations.

91
00:06:07,010 --> 00:06:09,920
There are two ways of understanding that
last sentence.

92
00:06:11,600 --> 00:06:12,740
Right, we could label them

93
00:06:12,740 --> 00:06:16,090
plural understanding or the distributive
understanding.

94
00:06:16,090 --> 00:06:21,430
On the plural understanding, what I'm
saying is that people should not force

95
00:06:22,920 --> 00:06:27,620
a whole bunch of creatures to kill a bunch
of innocent creatures.

96
00:06:29,210 --> 00:06:35,050
On the distributive interpretation, I'm
saying that for any creature that you

97
00:06:35,050 --> 00:06:41,449
pick, people should not force that
creature to kill an innocent creature.

98
00:06:43,230 --> 00:06:48,160
Alright, so on the first interpretation of
the statement, I'm talking about groups of

99
00:06:48,160 --> 00:06:52,300
creatures, and on the second
interpretation of the statement, I'm

100
00:06:52,300 --> 00:06:57,780
talking about each one of a whole bunch of
particular creatures.

101
00:06:57,780 --> 00:07:02,270
I'm making a statement about each one of
those particular creatures, whereas

102
00:07:02,270 --> 00:07:07,747
on the first interpretation, I'm making a
statement about the group of creatures.

103
00:07:07,747 --> 00:07:14,240
Now, in ordinary language, it's not easy

104
00:07:14,240 --> 00:07:16,760
to separate out those two interpretations.

105
00:07:16,760 --> 00:07:19,790
You can see just in the last minute how
hard

106
00:07:19,790 --> 00:07:23,190
I had to work in order to distinguish
those two interpretations.

107
00:07:24,680 --> 00:07:27,990
But if we use variables, then we

108
00:07:27,990 --> 00:07:32,330
can distinguish the two interpretations
pretty straightforwardly.

109
00:07:32,330 --> 00:07:36,180
We could say in order to specify the

110
00:07:36,180 --> 00:07:39,670
distributive interpretation of that
statement, we could say,

111
00:07:39,670 --> 00:07:44,940
where x stands for any creature at all,
and y stands

112
00:07:44,940 --> 00:07:49,860
for any innocent creature at all, people
should not force x to kill y.

113
00:07:51,350 --> 00:07:58,080
So here we're using the variables, x and
y, in order to express the distributive

114
00:07:58,080 --> 00:08:03,030
interpretation of people should not force
creatures to kill innocent creatures.

115
00:08:03,030 --> 00:08:04,770
So variables can be useful

116
00:08:04,770 --> 00:08:11,560
to us in clearly expressing certain kinds
of distributively interpreted rules.

117
00:08:15,000 --> 00:08:21,600
Now, in propositional logic, variables
play an equally useful role.

118
00:08:21,600 --> 00:08:27,350
So, consider an inference that we looked
at in the last lecture.

119
00:08:27,350 --> 00:08:30,630
From the premise, I'm holding my
binoculars and looking

120
00:08:30,630 --> 00:08:36,140
through them, it follows that I'm holding
my binoculars, right?

121
00:08:36,140 --> 00:08:38,220
If it's true that I'm holding my

122
00:08:38,220 --> 00:08:40,090
binoculars and looking through them, if
that

123
00:08:40,090 --> 00:08:45,810
premise is true, then the conclusion I'm
holding my binoculars must be true.

124
00:08:45,810 --> 00:08:48,450
So that right there is a valid, deductive
argument.

125
00:08:48,450 --> 00:08:50,040
There's no possible way for the premise to

126
00:08:50,040 --> 00:08:53,840
be true, without the conclusion also being
true.

127
00:08:55,920 --> 00:08:59,120
But notice, in that deductive argument,

128
00:08:59,120 --> 00:09:02,060
there's nothing special about the
proposition,

129
00:09:02,060 --> 00:09:03,840
I'm holding my binoculars, or about

130
00:09:03,840 --> 00:09:06,190
the proposition, I'm looking through my
binoculars.

131
00:09:07,370 --> 00:09:12,720
In fact, for any argument where the
premise is the conjunction of two

132
00:09:12,720 --> 00:09:16,800
propositions, and the conclusion is one of

133
00:09:16,800 --> 00:09:21,180
those two conjoined propositions, that
argument is

134
00:09:21,180 --> 00:09:22,890
also going to be valid.

135
00:09:22,890 --> 00:09:26,900
There's also going to be no possible way
for

136
00:09:26,900 --> 00:09:30,180
the conjunction of the two propositions
that was the premise

137
00:09:30,180 --> 00:09:35,970
of that argument to be true without the
conjoined

138
00:09:35,970 --> 00:09:40,830
proposition that is the conclusion of that
argument also being true.

139
00:09:40,830 --> 00:09:43,680
In other words, there's not going to be
any possible way

140
00:09:43,680 --> 00:09:46,420
for the premise to be true while the
conclusion is false.

141
00:09:47,630 --> 00:09:57,460
So any argument of this form right here is
going to be valid.

142
00:10:00,310 --> 00:10:06,680
Now notice, in the example I just used, as
well as in the truth table from

143
00:10:06,680 --> 00:10:12,610
last lecture, I was using roman letters,
like x, or y, or P, or Q, as variables.

144
00:10:12,610 --> 00:10:16,980
But there's nothing sacrosanct about using
roman letters as variables.

145
00:10:16,980 --> 00:10:20,090
I could have used anything else, so long
as

146
00:10:20,090 --> 00:10:24,180
it's easy enough to recognize and easy
enough to reproduce.

147
00:10:25,402 --> 00:10:26,630
Here's some examples.

148
00:10:26,630 --> 00:10:29,320
I could have used a dot, or a percentage
sign, or

149
00:10:29,320 --> 00:10:32,990
a plus sign, or a happy face, or a dollar
sign.

150
00:10:32,990 --> 00:10:36,380
The problem with all of those is that,
while they're

151
00:10:36,380 --> 00:10:41,080
easy enough to produce, they're not that
easy to recognize as

152
00:10:41,080 --> 00:10:44,470
variables in propositional logic, for the
simple reason that they're

153
00:10:44,470 --> 00:10:51,570
all used to express other things besides
variables in propositional logic.

154
00:10:51,570 --> 00:10:55,870
So whereas Roman letters like x, y, P, Q
typically aren't

155
00:10:55,870 --> 00:10:59,620
used by themselves to express anything at
all, other than variables.

156
00:11:02,000 --> 00:11:04,370
So we can use them safely as variables

157
00:11:04,370 --> 00:11:07,140
and propositional logic without running
the risk that

158
00:11:07,140 --> 00:11:09,810
people won't recognize them for what they
are,

159
00:11:09,810 --> 00:11:12,170
that people will think that they're
expressing something else.

160
00:11:13,300 --> 00:11:18,400
Notice, just as we have notation, Roman
letters serving as

161
00:11:18,400 --> 00:11:24,740
notation for variables, we also have
notation to express truth functions.

162
00:11:24,740 --> 00:11:27,210
For instance, in the last lecture I
introduced

163
00:11:27,210 --> 00:11:31,420
the truth-functional connective
conjunction, and normally we

164
00:11:31,420 --> 00:11:35,290
express conjunction using this ampersand
right here.

165
00:11:38,190 --> 00:11:40,270
And there are other truth functions as
well

166
00:11:40,270 --> 00:11:43,040
that we're going to learn about in
upcoming lectures.

167
00:11:43,040 --> 00:11:46,940
One is disjunction which we normally
express using an upper case

168
00:11:46,940 --> 00:11:52,280
V and another one is negation which we can
express using

169
00:11:52,280 --> 00:11:55,610
either one of these two signs that looks a
little bit

170
00:11:55,610 --> 00:11:59,930
like a minus sign from arithmetic but not
exactly the same.

171
00:12:01,710 --> 00:12:04,260
These are two different ways of expressing
negation.

172
00:12:04,260 --> 00:12:07,810
Now, again, there's nothing sacrosanct
about this notation.

173
00:12:08,840 --> 00:12:12,480
There have been different notational
systems that people have used.

174
00:12:12,480 --> 00:12:16,190
There's nothing better about this
notational system.

175
00:12:16,190 --> 00:12:20,680
We just have to pick one, and stick to it
for the sake of consistency.

176
00:12:20,680 --> 00:12:23,110
So this is the one that we're picking, not
for

177
00:12:23,110 --> 00:12:25,650
any good reason, just because we have to
pick one.

178
00:12:26,930 --> 00:12:29,830
So in upcoming lectures, you're going to
learn about disjunction and

179
00:12:29,830 --> 00:12:31,200
about negation.

180
00:12:31,200 --> 00:12:33,390
And when we talk about those true
functions, we're

181
00:12:33,390 --> 00:12:36,690
going to use the notation that you see
here.

182
00:12:36,690 --> 00:12:38,010
Okay, see you next time.

