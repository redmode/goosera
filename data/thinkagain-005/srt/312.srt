1
00:00:03,400 --> 00:00:04,672
This week we're going to be studying

2
00:00:04,672 --> 00:00:07,980
propositional logic, which is the rules
that determine

3
00:00:07,980 --> 00:00:10,222
the validity of an argument based on the

4
00:00:10,222 --> 00:00:13,519
propositional connectives that are used in
that argument.

5
00:00:13,519 --> 00:00:16,219
Now I've said a little bit about
propositional connectives, but

6
00:00:16,219 --> 00:00:19,140
in order to say more about what
propositional connectives are, in

7
00:00:19,140 --> 00:00:22,594
particular in order to give a definition
of propositional connectives,

8
00:00:22,594 --> 00:00:25,475
I have to start out by giving a definition
of propositions.

9
00:00:25,475 --> 00:00:28,010
So, what are propositions?

10
00:00:29,030 --> 00:00:33,080
A proposition is the kind of thing that
can be true or false.

11
00:00:33,080 --> 00:00:36,430
And that can be the premise or the
conclusion of an argument.

12
00:00:37,670 --> 00:00:40,070
Let me give you some examples to
illustrate that definition.

13
00:00:45,340 --> 00:00:50,486
See these binoculars?
These binoculars are not a proposition.

14
00:00:50,486 --> 00:00:54,100
They can't be true or false, and they
can't

15
00:00:54,100 --> 00:00:56,570
be the premise, or the conclusion of an
argument.

16
00:01:00,690 --> 00:01:02,380
See this hand?

17
00:01:02,380 --> 00:01:06,570
This hand is not a proposition, this hand
can not be true or

18
00:01:06,570 --> 00:01:11,062
false, and it can't be the premise of the
conclusion of an argument.

19
00:01:11,062 --> 00:01:18,990
But now suppose I say the binoculars are
in my hand.

20
00:01:18,990 --> 00:01:23,090
Now what I just said that the binoculars
are in my hand.

21
00:01:23,090 --> 00:01:25,310
That is the kind of thing that can be true
or false.

22
00:01:25,310 --> 00:01:27,140
For instance,

23
00:01:27,140 --> 00:01:29,920
right now it's true, and right now it's
false.

24
00:01:29,920 --> 00:01:30,420
It's

25
00:01:32,380 --> 00:01:36,220
also the kind of thing that could be the
premiss or the conclusion of an argument.

26
00:01:36,220 --> 00:01:39,490
For instance I could say, the binoculars
are in my

27
00:01:39,490 --> 00:01:43,690
hand, therefore my hand is not free to
shake yours.

28
00:01:45,390 --> 00:01:46,280
Or I could say,

29
00:01:48,950 --> 00:01:52,380
you just gave me the binoculars and I
haven't let go of them.

30
00:01:52,380 --> 00:01:54,875
Therefore, the binoculars are in my hand.

31
00:01:54,875 --> 00:02:00,430
See, so that the binoculars are in my
hand, that's a proposition.

32
00:02:00,430 --> 00:02:02,280
That's the kind of thing that can be true
or false

33
00:02:02,280 --> 00:02:05,125
and that can be the permise of the
conclusion of an argument.

34
00:02:05,125 --> 00:02:11,170
Now, now that

35
00:02:11,170 --> 00:02:17,191
we know what propositions are, what's a
propositional connective?

36
00:02:17,191 --> 00:02:22,210
A propositional connective is, something
that takes propositions,

37
00:02:22,210 --> 00:02:27,460
and connects them to create new
propositions.

38
00:02:27,460 --> 00:02:30,400
For instance, sometimes in English the
word, and can

39
00:02:32,680 --> 00:02:38,480
work as a propositional connective.
So, let me give you an example of how,

40
00:02:38,480 --> 00:02:42,900
in English, the word and can sometimes
function as a propositional connective.

41
00:02:44,190 --> 00:02:48,780
So consider the proposition, I'm holding
the binoculars.

42
00:02:48,780 --> 00:02:49,670
That's a proposition.

43
00:02:49,670 --> 00:02:51,780
It's the kind of thing that can be true or
false, and it's the

44
00:02:51,780 --> 00:02:55,566
kind of thing that can be the premise or a
conclusion of an argument.

45
00:02:55,566 --> 00:02:58,080
Well, there's also the proposition,

46
00:02:58,080 --> 00:03:00,640
I'm looking through the binoculars.
Right?

47
00:03:00,640 --> 00:03:03,040
That's a kind of thing that can be true or
false, and it's the

48
00:03:03,040 --> 00:03:05,665
kind of thing that can be the premise or
the conclusion of an argument.

49
00:03:05,665 --> 00:03:10,850
Well you could use the word and to connect

50
00:03:10,850 --> 00:03:14,570
up those two propositions to make a new
proposition.

51
00:03:14,570 --> 00:03:20,610
The new proposition would be, I am holding
the binoculars and looking through them.

52
00:03:21,769 --> 00:03:23,590
See now that's a proposition.

53
00:03:23,590 --> 00:03:26,670
It's a kind of thing can be true or false,
and it's also the

54
00:03:26,670 --> 00:03:30,370
kind of thing that could be the premise or
the conclusion of an argument.

55
00:03:30,370 --> 00:03:32,740
For instance right now it's true that

56
00:03:32,740 --> 00:03:34,900
I'm holding the binoculars and looking
through them.

57
00:03:36,090 --> 00:03:37,850
Whereas right now it's false, that I'm

58
00:03:37,850 --> 00:03:42,560
holding the binoculars and looking through
them.

59
00:03:42,560 --> 00:03:44,060
I can say.

60
00:03:44,060 --> 00:03:46,860
What you're seeing right now, is really
happening.

61
00:03:46,860 --> 00:03:48,680
Therefore, I'm holding

62
00:03:48,680 --> 00:03:50,380
the binoculars and looking through them.

63
00:03:51,710 --> 00:03:56,240
Or, I could say, I'm holding the
binoculars and looking through them.

64
00:03:56,240 --> 00:03:59,580
Therefore, I should not be driving a car
right now.

65
00:04:01,010 --> 00:04:05,060
You see, so that I'm holding the
binoculars and looking through them.

66
00:04:05,060 --> 00:04:07,660
Is also a proposition.

67
00:04:07,660 --> 00:04:10,690
It's the kind of thing that can be true or
false, and it's the

68
00:04:10,690 --> 00:04:14,230
kind of thing that can be the premise or
the conclusion of an argument.

69
00:04:15,280 --> 00:04:17,960
But it's a proposition that we created

70
00:04:17,960 --> 00:04:21,330
by combining two other propositions,
namely the

71
00:04:21,330 --> 00:04:23,820
proposition I am holding the binoculars
and

72
00:04:23,820 --> 00:04:26,770
the proposition I am looking through the
binoculars.

73
00:04:26,770 --> 00:04:31,980
By combining those two prepositions with
the word and, so that's an example

74
00:04:31,980 --> 00:04:37,460
of how the word and in English can work as
a prepositional connective,

75
00:04:37,460 --> 00:04:42,617
connecting two prepositions into a third
proposition.

