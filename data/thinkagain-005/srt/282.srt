1
00:00:03,270 --> 00:00:05,700
The next stage is to sharpen edges.

2
00:00:05,700 --> 00:00:08,640
And that means clarify the premises and
break them up,

3
00:00:08,640 --> 00:00:11,980
where doing so would help understand what
they're really claiming.

4
00:00:13,380 --> 00:00:18,655
So let's look first at the BLMs argument
which simply says, Conoco has a lease.

5
00:00:18,655 --> 00:00:20,310
These lands were set aside subject to

6
00:00:20,310 --> 00:00:22,245
valid rights, therefore its hands are
tied.

7
00:00:22,245 --> 00:00:26,955
We gotta clarify first of all the
conclusion.

8
00:00:26,955 --> 00:00:27,948
Its hands are tied.

9
00:00:27,948 --> 00:00:30,350
What does that mean?

10
00:00:30,350 --> 00:00:35,640
Well, basically the BLM is claiming, I
can't do anything about Conoco.

11
00:00:35,640 --> 00:00:37,400
You know, my hands are tied.

12
00:00:37,400 --> 00:00:41,460
Don't hold me responsible.
There's nothing I can do, okay.

13
00:00:41,460 --> 00:00:45,100
They're offering an excuse.
Next premise two.

14
00:00:46,210 --> 00:00:49,060
These lands were set aside to subject to
valid existing rights.

15
00:00:49,060 --> 00:00:52,990
Well, what is set aside mean?
It means that you're not

16
00:00:52,990 --> 00:00:53,798
allowed to drill there.

17
00:00:53,798 --> 00:00:58,310
But subject evalid existing rights means
that you are allowed

18
00:00:58,310 --> 00:01:02,080
to drill there if you've got a valid
exisiting right.

19
00:01:02,080 --> 00:01:08,010
So, this premise can be restated as saying
that if Conoco does have a valid

20
00:01:08,010 --> 00:01:14,070
existing right to drill, then the BLM must
allow Conoco to drill.

21
00:01:14,070 --> 00:01:17,550
Okay.
What about breaking up premises?

22
00:01:17,550 --> 00:01:18,000
What about

23
00:01:18,000 --> 00:01:19,560
premise 1?

24
00:01:19,560 --> 00:01:22,316
Well, that's one that I think we're
going to have to break up.

25
00:01:22,316 --> 00:01:27,875
Because it says that Conoco has a lease
that gives it the right to drill.

26
00:01:27,875 --> 00:01:31,710
And gives it, we already marked as an
argument marker.

27
00:01:31,710 --> 00:01:36,070
Which suggests that the fact that Conoco
has a lease is

28
00:01:36,070 --> 00:01:38,900
supposed to be a reason why it has a right
to drill.

29
00:01:38,900 --> 00:01:41,280
So there's an argument implicit in that
one sentence.

30
00:01:42,730 --> 00:01:43,100
And that

31
00:01:43,100 --> 00:01:48,376
means that we can take this whole argument
and restate it something like this.

32
00:01:48,376 --> 00:01:52,770
Conoco has a lease, therefore Conoco has a
valid right to drill.

33
00:01:54,220 --> 00:01:58,938
If Conoco has a valid right to drill, then
the BLM must allow Conoco to drill.

34
00:01:58,938 --> 00:02:01,865
Therefore, the BLM must allow Conoco to
drill.

35
00:02:01,865 --> 00:02:08,430
That is supposed to be the central force
of the explicit premises and conclusions

36
00:02:08,430 --> 00:02:12,120
in the first part.
Where the BLM gives its argument.

37
00:02:12,120 --> 00:02:16,180
The next part is Redford's response to
this argument.

38
00:02:16,180 --> 00:02:19,220
Let's start with Redford's conclusion.
What's he trying to show?

39
00:02:19,220 --> 00:02:22,660
He's trying to show the opposite of the
BLM's argument.

40
00:02:22,660 --> 00:02:25,190
They're trying to show that their hands
are tied.

41
00:02:25,190 --> 00:02:29,320
That is, that they can't stop Conoco from
drilling.

42
00:02:29,320 --> 00:02:33,710
So Redford wants to show, that they can
stop Conoco from drilling, or

43
00:02:33,710 --> 00:02:36,490
even that they must stop Conoco from
drilling.

44
00:02:37,870 --> 00:02:39,530
So that's the conclusion he's trying to
reach.

45
00:02:39,530 --> 00:02:43,888
What he says is sounds like Washington
double speak to me.

46
00:02:43,888 --> 00:02:46,970
Well, that's 'cuz he's saying that people
in Washington always

47
00:02:46,970 --> 00:02:50,050
say their hands are tied and can't get
anything done.

48
00:02:50,050 --> 00:02:52,370
And he's going to argue that their hands
aren't

49
00:02:52,370 --> 00:02:57,090
tied because they can and must stop Conoco
from drilling.

50
00:02:57,090 --> 00:02:59,010
So we can replace the conclusion

51
00:02:59,010 --> 00:03:04,290
simply with the claim that the BLM must
stop Conoco from drilling.

52
00:03:05,430 --> 00:03:07,050
Now we know how the argument ends.

53
00:03:08,250 --> 00:03:13,480
So let's take those premises and
conclusion and number them pretty high, so

54
00:03:13,480 --> 00:03:16,440
that we can leave some room for the other
premises that come before them.

55
00:03:18,130 --> 00:03:24,540
The next thing we need to do, is to get an
argument for that central premise.

56
00:03:24,540 --> 00:03:27,740
Conoco does not have any valid right to
drill.

57
00:03:27,740 --> 00:03:31,290
No here's what Redford said that the
leases

58
00:03:31,290 --> 00:03:34,730
were originally issued without sufficient
environmental study or

59
00:03:34,730 --> 00:03:38,470
public input, and in deciding to issue a

60
00:03:38,470 --> 00:03:41,880
permit now, the BLM didn't conduct a full
analysis.

61
00:03:42,900 --> 00:03:44,870
Notice that they're two parts.

62
00:03:44,870 --> 00:03:46,400
To these claims.

63
00:03:46,400 --> 00:03:50,330
One is about the leases and the other is
about the permit.

64
00:03:50,330 --> 00:03:52,140
Because in order to have a valid right to

65
00:03:52,140 --> 00:03:57,580
drill, Conoco needs to have a lease and a
permit.

66
00:03:57,580 --> 00:04:01,520
Redford argues that there are problems
with both the lease and the permit.

67
00:04:01,520 --> 00:04:04,010
But the considerations are a little
different.

68
00:04:04,010 --> 00:04:07,364
So we need to separate those two parts
into different arguments.

69
00:04:07,364 --> 00:04:12,420
The first part of this argument concerns
the leases.

70
00:04:12,420 --> 00:04:15,890
He says that the leases were originally
issued without

71
00:04:15,890 --> 00:04:18,350
sufficient environmental study or public
input.

72
00:04:18,350 --> 00:04:22,140
Therefore, none of the leases conveyed a
valid right to drill.

73
00:04:23,660 --> 00:04:25,880
Then the second part has to do with the
permit.

74
00:04:26,900 --> 00:04:31,330
In deciding to issue a permit to drill
now, the BLM did not conduct

75
00:04:31,330 --> 00:04:36,430
a full analysis therefore, none of the
permits conveyed a valid right to drill.

76
00:04:36,430 --> 00:04:38,320
And the idea of the argument is going to

77
00:04:38,320 --> 00:04:40,930
be that the leases don't give them a right
and

78
00:04:40,930 --> 00:04:44,740
the permits don't give them a right, so
they ain't got no right.

79
00:04:44,740 --> 00:04:49,850
Wow, but that premise in that argument to
show that the permit's not valid is

80
00:04:49,850 --> 00:04:53,404
a long premise with lots of different
parts so we need to break it up.

81
00:04:53,404 --> 00:04:57,640
And we can figure out how to break it up
by looking at the

82
00:04:57,640 --> 00:05:00,150
argument markers in the part of the

83
00:05:00,150 --> 00:05:03,670
passage that in effect constitute that
premise.

84
00:05:03,670 --> 00:05:06,210
We know that there's a premise marker

85
00:05:06,210 --> 00:05:10,210
at the beginning.
Once more, a discounting term.

86
00:05:10,210 --> 00:05:15,530
But, on the basis of, is an argument
marker.

87
00:05:15,530 --> 00:05:17,320
Even, is a discounting term.

88
00:05:17,320 --> 00:05:22,299
And that breaks that long premise into
parts so we can break them into A, the

89
00:05:22,299 --> 00:05:25,030
BLM did not conduct a full analysis of

90
00:05:25,030 --> 00:05:28,550
the environmental impacts of drilling on
these incomparable lands.

91
00:05:28,550 --> 00:05:29,288
No full analysis.

92
00:05:29,288 --> 00:05:31,980
B, the BLM

93
00:05:31,980 --> 00:05:36,696
determined that there would be no
significant environmental harm, okay?

94
00:05:36,696 --> 00:05:42,120
C, the BLM conducted only an abbrevieated
review of the envorimental harm.

95
00:05:42,120 --> 00:05:49,640
And D, the BLM didn't even look at
drilling on other federal leases.

96
00:05:49,640 --> 00:05:50,140
What about B?

97
00:05:50,140 --> 00:05:53,710
The BLM determined there would be no
significant environmental impact.

98
00:05:53,710 --> 00:05:57,090
Well, that's what Redford opposes so
that's not going to be part

99
00:05:57,090 --> 00:05:57,970
of his argument.

100
00:05:57,970 --> 00:06:02,130
How do the other three claims fit
together, which is a reason for which?

101
00:06:02,130 --> 00:06:06,585
Well, now we're into a different step,
namely organize the parts.

102
00:06:06,585 --> 00:06:11,860
And it's not completely clear.
But it seems like Redford

103
00:06:11,860 --> 00:06:17,300
has 2 separate complaints.
One is that the BLM did not look

104
00:06:17,300 --> 00:06:22,598
sufficiently hard at the environmental
impact at this particular site the

105
00:06:22,598 --> 00:06:22,610
[UNKNOWN]

106
00:06:22,610 --> 00:06:24,600
plateau.

107
00:06:24,600 --> 00:06:28,340
The other complaint is that they didn't do
a comparative analysis.

108
00:06:28,340 --> 00:06:30,860
And look at other leases on other federal
lands to

109
00:06:30,860 --> 00:06:34,285
see, you know, what happens when drilling
was allowed there.

110
00:06:34,285 --> 00:06:39,220
And when permits were issued in those
other circumstances.

111
00:06:39,220 --> 00:06:41,920
So one claim is about this particular site
and the

112
00:06:41,920 --> 00:06:45,470
other claim is a lack of comparison to
other sites.

113
00:06:46,580 --> 00:06:47,640
These two points become

114
00:06:47,640 --> 00:06:51,440
even clearer in the next paragraph, if you
remember that.

115
00:06:51,440 --> 00:06:54,810
There he said, first, I've spent
considerable

116
00:06:54,810 --> 00:06:57,575
time on these extraordinary lands for
years,

117
00:06:57,575 --> 00:07:03,319
and I know that an oil rig in their midst
would have a major impact.

118
00:07:04,550 --> 00:07:06,600
So there, he's talking about the impact

119
00:07:06,600 --> 00:07:08,940
on this particular site on these
particular lands.

120
00:07:08,940 --> 00:07:11,930
But right after that, he says, what's
more.

121
00:07:11,930 --> 00:07:15,440
Indicating it's a separate argument,
what's more.

122
00:07:15,440 --> 00:07:18,715
Conoco wants to drill a well to find oil.

123
00:07:18,715 --> 00:07:21,510
Inevitably, more rigs, more roads.

124
00:07:21,510 --> 00:07:23,515
New pipelines, toxic waste.

125
00:07:23,515 --> 00:07:26,270
And bright lights would follow to get the
oil out.

126
00:07:27,480 --> 00:07:31,990
There he seems to be suggesting if he had
just looked over at the other leases,

127
00:07:31,990 --> 00:07:34,940
you'd find that when you allow oil
drilling

128
00:07:34,940 --> 00:07:37,330
a lot more happens than you ever expected

129
00:07:37,330 --> 00:07:40,980
to begin with.
I'm, of course, not agreeing with this.

130
00:07:40,980 --> 00:07:42,780
It might be true or it might not.

131
00:07:42,780 --> 00:07:45,160
My point is that, this is the structure of

132
00:07:45,160 --> 00:07:48,460
Redford's own claims that make up his own
argument.

133
00:07:48,460 --> 00:07:52,010
So there are two separate ways in which
the review failed to be full.

134
00:07:52,010 --> 00:07:56,820
The first is that the BLM conducted only
an abbreviated review, in

135
00:07:56,820 --> 00:08:00,730
the sense that they didn't look carefully
at this particular site itself.

136
00:08:00,730 --> 00:08:02,370
And the second, is that

137
00:08:02,370 --> 00:08:05,585
the BLM didn't look at drilling in other
places.

138
00:08:05,585 --> 00:08:10,410
Therefore, the conclusion is the BLM did
not conduct a full

139
00:08:10,410 --> 00:08:15,170
analysis, that seems to be what Redford's
saying in this particular sentence.

140
00:08:15,170 --> 00:08:18,835
Now let's bring it all together and clean
it up a little bit in the process.

141
00:08:18,835 --> 00:08:23,540
One, all of Conoco's leases were
originally

142
00:08:23,540 --> 00:08:28,070
issued without sufficient environmental
study or public input.

143
00:08:28,070 --> 00:08:31,510
That's supposed to support two, none of
Conoco's

144
00:08:31,510 --> 00:08:33,140
leases give it a valid right to drill.

145
00:08:34,950 --> 00:08:39,250
Then three, the BLM conducted an
abbreviated review, they

146
00:08:39,250 --> 00:08:41,866
didn't look as carefully as they should've
at the lands.

147
00:08:41,866 --> 00:08:48,693
Four, the BLM didn't look at drilling on
lands under the other federal leases.

148
00:08:48,693 --> 00:08:50,910
This is the comparative claim.

149
00:08:50,910 --> 00:08:53,810
And those two are supposed to support
five.

150
00:08:53,810 --> 00:08:59,460
The BLM did not conduct a full analysis.
And whats that's supposed to show

151
00:08:59,460 --> 00:09:04,850
is that, six, its permit does not give
Conoco a valid right to drill.

152
00:09:07,250 --> 00:09:11,570
Therefore, seven which is supposed to
follow from two and six.

153
00:09:11,570 --> 00:09:13,980
Conoco does not have a valid right to
drill.

154
00:09:13,980 --> 00:09:18,400
A, if Conoco does not have a valid right
to

155
00:09:18,400 --> 00:09:21,488
drill then the BLM must not allow Conoco
to drill.

156
00:09:21,488 --> 00:09:25,835
Therefore, the BLM must not allow Conoco
to drill.

157
00:09:25,835 --> 00:09:30,400
Make sense?
Seem fair?

158
00:09:30,400 --> 00:09:32,380
Hope so.
I think

159
00:09:32,380 --> 00:09:36,933
it's a pretty good reconstruction of what
Redford had in mind.

