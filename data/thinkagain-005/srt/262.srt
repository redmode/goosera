1
00:00:03,000 --> 00:00:05,480
On to the second paragraph.

2
00:00:05,480 --> 00:00:09,143
The first sentence is really simple, "Not
so".

3
00:00:09,143 --> 00:00:14,520
Of course, it's referring back to the last
sentence of the first paragraph, which

4
00:00:14,520 --> 00:00:22,110
said, now we thought at least some it was
safe, and saying, no, it wasn't safe.

5
00:00:22,110 --> 00:00:25,215
So, what are we going to say about this
sentence?

6
00:00:25,215 --> 00:00:28,320
Well, what about the wor- little word
"so".

7
00:00:30,000 --> 00:00:31,440
So, can be an argument marker.

8
00:00:31,440 --> 00:00:35,240
It can indicate that what follows it, is a
conclusion.

9
00:00:35,240 --> 00:00:37,850
But is that what it's doing here?
I don't think so.

10
00:00:39,180 --> 00:00:42,470
As we just saw, like in, I don't think so,
the word "so", can be used

11
00:00:42,470 --> 00:00:45,550
in many ways where it's not an argument
marker, and this is saying it's not so.

12
00:00:45,550 --> 00:00:47,420
It's not that way.

13
00:00:47,420 --> 00:00:52,816
So there's no argument here, so that would
get marked with a big N for nothing.

14
00:00:52,816 --> 00:00:55,264
Now, what about

15
00:00:55,264 --> 00:01:03,795
"shocking", as it sounds shocking.
Well, is shocking always bad?

16
00:01:03,795 --> 00:01:07,665
Remember, we saw in the first paragraph,
the word "stunning".

17
00:01:08,690 --> 00:01:11,630
Well, stunning stuns you, and shocking
shocks you, and

18
00:01:11,630 --> 00:01:13,845
it's telling you that you have some kind
of reaction,

19
00:01:13,845 --> 00:01:16,400
but it's not telling you whether that
reaction is due

20
00:01:16,400 --> 00:01:18,990
to the thing being good, or the thing
being bad.

21
00:01:18,990 --> 00:01:20,350
You can get shocked by

22
00:01:20,350 --> 00:01:21,623
something good or bad.

23
00:01:21,623 --> 00:01:25,190
It can be shockingly good, or shockingly
bad.

24
00:01:25,190 --> 00:01:29,490
And so the word "shocking" by itself,
doesn't indicate that it's

25
00:01:29,490 --> 00:01:33,740
E plus or E minus, so again, you get a
nothing.

26
00:01:33,740 --> 00:01:37,985
I mention it only because, it's clear that
Robert Redford

27
00:01:37,985 --> 00:01:43,100
thinks that "shocking" is bad, that this
should not have happened.

28
00:01:43,100 --> 00:01:45,412
He's suggesting that it's bad, but the
word

29
00:01:45,412 --> 00:01:50,695
"shocking" in itself is not an evaluative
word.

30
00:01:50,695 --> 00:01:54,703
What about "as it sounds"?

31
00:01:54,703 --> 00:01:59,440
Well, he's saying that it sounds that way.
He's not saying that it is that way.

32
00:01:59,440 --> 00:02:01,980
He's not saying that it may sound that
way.

33
00:02:01,980 --> 00:02:08,090
He's saying that it does sound that way,
which is to say, it seems that way to him.

34
00:02:08,090 --> 00:02:10,940
Which is not to say it really is true, so
he's guarding

35
00:02:10,940 --> 00:02:11,440
the claim.

36
00:02:12,480 --> 00:02:15,980
He's not saying that it really is
shocking.

37
00:02:15,980 --> 00:02:20,970
He's saying that it seems shocking, so
he's guarding the claim, in order

38
00:02:20,970 --> 00:02:25,865
to avoid someone objecting that it's
really not all that shocking after all.

39
00:02:25,865 --> 00:02:27,460
You say, well it sounds shocking.

40
00:02:27,460 --> 00:02:31,645
And in order to make that part of his
argument more defensible,

41
00:02:31,645 --> 00:02:34,870
because it's not really essential to his
argument that it's shocking or not.

42
00:02:36,270 --> 00:02:37,270
So it's supposed to be shocking.

43
00:02:38,370 --> 00:02:41,670
Clinton's Bureau of Land Management or
BLM,

44
00:02:41,670 --> 00:02:44,120
has approved oil drilling within the
monument.

45
00:02:45,680 --> 00:02:48,169
Notice, that there's no guarding at all
here.

46
00:02:48,169 --> 00:02:50,940
He just states it.
They did it.

47
00:02:50,940 --> 00:02:53,980
They approved oil drilling within the
monument.

48
00:02:53,980 --> 00:02:58,650
And that's because it's not really
something he's arguing for.

49
00:02:58,650 --> 00:03:01,460
He's actually opposed to it.
It's something

50
00:03:01,460 --> 00:03:04,850
that his opponents might support, but he
doesn't.

51
00:03:04,850 --> 00:03:08,130
So he doesn't want to guard it, since he
wants to say

52
00:03:08,130 --> 00:03:10,420
it just happened as a matter a fact, that
they approved it.

53
00:03:10,420 --> 00:03:15,048
So there's nothing that we need to mark in
that particular sentence.

54
00:03:15,048 --> 00:03:18,940
Next sentence, BLM, or the Bureau of

55
00:03:18,940 --> 00:03:22,950
Land Management, has given Conoco
Incorporated, a subsidiary

56
00:03:22,950 --> 00:03:26,560
of the corporate giant DuPont, permission
to drill for oil and gas in the heart

57
00:03:26,560 --> 00:03:29,080
of the new monument.

58
00:03:29,080 --> 00:03:32,308
Well, there's a lot going on here that we
could mention.

59
00:03:32,308 --> 00:03:36,170
You know BLM has given Conoco a
subsidiary.

60
00:03:36,170 --> 00:03:41,200
That explains how they got permission, by
siding the BLM.

61
00:03:41,200 --> 00:03:44,775
It's a subsidiary of the corporate giant
DuPont.

62
00:03:44,775 --> 00:03:49,000
He's certainly suggesting that, "giant"
suggested he's

63
00:03:49,000 --> 00:03:51,640
the small guy, you know, up against

64
00:03:51,640 --> 00:03:53,655
the big corporate giant.

65
00:03:53,655 --> 00:03:56,810
I-It might even have some connotation of
corporate

66
00:03:56,810 --> 00:04:00,236
giants being bad, but it doesn't actually
say that.

67
00:04:00,236 --> 00:04:06,188
And so again, "giant" should be marked as
nothing.

68
00:04:06,188 --> 00:04:12,270
What about "permission"?
To say that someone's permitted to do

69
00:04:12,270 --> 00:04:17,100
something, is to say that it's not wrong.
That's what permitted means.

70
00:04:17,100 --> 00:04:20,370
Now of course, if you're talking about a
legal permission?

71
00:04:20,370 --> 00:04:24,360
Then to say that it's permitted, is to say
that's not legally wrong.

72
00:04:24,360 --> 00:04:26,300
It's not forbidded by law.

73
00:04:26,300 --> 00:04:27,790
It still might be morally wrong, but at

74
00:04:27,790 --> 00:04:32,030
least it's legally permitted; means it's
not legally forbidden.

75
00:04:32,030 --> 00:04:35,530
And so if forbidden and wrong are
evaluative words, to deny

76
00:04:35,530 --> 00:04:38,580
them and say it's not wrong, looks like an
evaluative as well.

77
00:04:38,580 --> 00:04:42,500
But one of the interesting things about
this evaluative word, is it's

78
00:04:42,500 --> 00:04:45,510
not clear whether it's positive or
negative.

79
00:04:45,510 --> 00:04:49,720
It means it's not wrong, but that don't
mean it is good or is right.

80
00:04:49,720 --> 00:04:51,820
It simply means it's not forbidden.

81
00:04:51,820 --> 00:04:54,521
So it's not clear whether to put plus or
minus.

82
00:04:54,521 --> 00:04:59,741
So I'll just leave it as a plain E in that
case, okay?

83
00:04:59,741 --> 00:05:03,070
Now, what did they have permission to do?

84
00:05:03,070 --> 00:05:07,200
To drill for oil and gas in the heart of
the new monument, okay?

85
00:05:07,200 --> 00:05:07,580
That's what

86
00:05:07,580 --> 00:05:09,570
the permission was a permission to do.

87
00:05:09,570 --> 00:05:12,310
The word "to" there is not being used as
an argument marker in

88
00:05:12,310 --> 00:05:16,570
this case, because you can't say, they get
permission in order to, right?

89
00:05:16,570 --> 00:05:19,420
What they gave them permission to do, was
to drill, okay?

90
00:05:19,420 --> 00:05:20,614
What about "drill"?

91
00:05:20,614 --> 00:05:24,710
Well, clearly Robert Redford doesn't want
them to drill, so

92
00:05:24,710 --> 00:05:26,790
he thinks that's bad, but he donen't say
that's bad.

93
00:05:26,790 --> 00:05:28,142
He just calls it "drilling".

94
00:05:28,142 --> 00:05:29,920
.And they're drilling for oil and gas.

95
00:05:29,920 --> 00:05:32,465
Well a lot of people think that oil and
gas are good things,

96
00:05:32,465 --> 00:05:35,680
but they don't say here that they're good.

97
00:05:35,680 --> 00:05:38,690
They're simply saying that they're oil and
gas.

98
00:05:38,690 --> 00:05:42,070
The coolest part of this sentence ,I
think, is that metaphor at the end.

99
00:05:42,070 --> 00:05:43,630
I mean, you got this image that there's
this,

100
00:05:43,630 --> 00:05:48,700
this, poor monument, and somebody's
drilling right in it's heart.

101
00:05:48,700 --> 00:05:52,690
You know, like what could be crueler, than
to drill in the heart of a young monument.

102
00:05:52,690 --> 00:05:54,310
The poor innocent thing.

103
00:05:54,310 --> 00:05:57,670
So this metaphor of the heart, is a nice
rhetorical

104
00:05:57,670 --> 00:06:03,050
device, that fits with the drilling, and
is building up people's opposition to what

105
00:06:03,050 --> 00:06:07,053
Redford wants them to be opposed to, but
it doesn't actually give an argument.

106
00:06:07,053 --> 00:06:09,448
It's just stating it in a flowery or

107
00:06:09,448 --> 00:06:14,840
metaphorical way, that'll get their
feelings going, okay?

108
00:06:14,840 --> 00:06:16,698
You may wonder, notice he doesn't say, you
do wonder.

109
00:06:16,698 --> 00:06:18,080
He says, you may wonder.
So this is, tell me?

110
00:06:18,080 --> 00:06:23,704
A guarding term, so you should mark

111
00:06:23,704 --> 00:06:27,460
it with G.

112
00:06:27,460 --> 00:06:31,745
You may wonder, as I do, "as" is sometimes
used as an argument marker, but

113
00:06:31,745 --> 00:06:36,673
here you're not saying that you wonder
because I do, or I wonder because you do.

114
00:06:36,673 --> 00:06:39,690
The word "because" can't be substituted
for as.

115
00:06:39,690 --> 00:06:45,030
So to say "as I do," is simply to conjoin
the two and say,

116
00:06:45,030 --> 00:06:48,790
you wonder and I also wonder or you may
wonder, because I don't know

117
00:06:48,790 --> 00:06:50,580
whether you are or not, and I do.

118
00:06:50,580 --> 00:06:55,010
And what we wonder is, how can this
happen?

119
00:06:55,010 --> 00:06:56,680
Now, we have a rhetorical question.

120
00:06:56,680 --> 00:06:58,650
How can this happen?

121
00:06:58,650 --> 00:07:01,800
Well, that's obviously suggesting, it
shouldn't have happened.

122
00:07:01,800 --> 00:07:06,870
Now, how could something have gone so
wrong, as it did in this case?

123
00:07:06,870 --> 00:07:08,090
But he doesn't actually say that.

124
00:07:08,090 --> 00:07:10,350
He simply asks the rhetorical questions,
and

125
00:07:10,350 --> 00:07:12,330
that's really the trick of rhetorical
questions.

126
00:07:12,330 --> 00:07:14,030
Notice there are a bunch of them here.

127
00:07:14,030 --> 00:07:15,650
Wasn't the whole purpose this?

128
00:07:15,650 --> 00:07:18,870
Didn't the president say he was doing
this?

129
00:07:18,870 --> 00:07:23,030
Then these three sentences in a row, are
all rhetorical questions.

130
00:07:23,030 --> 00:07:25,790
So what's the trick of rhetorical
questions?

131
00:07:25,790 --> 00:07:29,460
The point of a rhetorical question, is to
get you to give the answer.

132
00:07:29,460 --> 00:07:33,090
If I say, how can this happen?

133
00:07:33,090 --> 00:07:36,970
And someone thinks to themself, well the
government messes up all the time.

134
00:07:36,970 --> 00:07:39,310
Then you've got that audience member who

135
00:07:39,310 --> 00:07:42,140
answered the question, to be saying it
themselves.

136
00:07:42,140 --> 00:07:46,690
And there's nothing more forceful in an
argument than to get your

137
00:07:46,690 --> 00:07:50,330
audience to say it themselves, when you
don't have to say it.

138
00:07:50,330 --> 00:07:52,600
And that's the trick of a rhetorical
question.

139
00:07:52,600 --> 00:07:57,480
And what Redford's doing here, is putting
three of 'em right in a row,

140
00:07:57,480 --> 00:08:01,460
so that you'll have to go along with him
three times in a row.

141
00:08:01,460 --> 00:08:04,450
And then, that obviously has an effect on
your

142
00:08:04,450 --> 00:08:08,310
feeling, like you're with him, or you
feeling like you agree with him.

143
00:08:08,310 --> 00:08:14,055
That's the effect he's trying to create by
using these rhetorical questions.

144
00:08:14,055 --> 00:08:17,200
Okay, what's the whole purpose of
creating?

145
00:08:17,200 --> 00:08:21,270
Notice, the whole purpose is to preserve.

146
00:08:21,270 --> 00:08:23,725
That phrase "the whole purpose," goes with
it,

147
00:08:23,725 --> 00:08:26,835
because we say the purpose is to preserve.

148
00:08:26,835 --> 00:08:29,540
And there as

149
00:08:29,540 --> 00:08:32,550
before, in the previous paragraph, we're
signaling an

150
00:08:32,550 --> 00:08:37,570
explanation, because if you want to
explain why Clinton

151
00:08:37,570 --> 00:08:40,880
created the monument, then the answer was,
to preserve

152
00:08:40,880 --> 00:08:43,480
the colorful cliffs and sweeping arches
and so on.

153
00:08:43,480 --> 00:08:49,280
So, this is going to be an explanation,
and to say, that's the purpose, is to say

154
00:08:49,280 --> 00:08:54,630
that, you created it because you wanted to
preserve, or in order

155
00:08:54,630 --> 00:08:55,545
to preserve.

156
00:08:55,545 --> 00:09:00,020
Then this whole purpose, marks the
conclusion,

157
00:09:00,020 --> 00:09:04,310
and the "to preserve" marks the premise.

158
00:09:04,310 --> 00:09:05,710
And we have a little argument.

159
00:09:05,710 --> 00:09:11,020
He wanted to preserve his colorful cliffs,
therefore he created the monument.

160
00:09:11,020 --> 00:09:14,570
And we've got the conclusion marked by the
whole purpose, and

161
00:09:14,570 --> 00:09:19,540
the premise, or the reason, marked by the
word "to," okay?

162
00:09:19,540 --> 00:09:24,580
Preserve, we've already seen a word a lot
like that, namely "protect".

163
00:09:24,580 --> 00:09:28,745
And we saw that when you protect
something, it has to be good.

164
00:09:28,745 --> 00:09:33,930
Preserved also means, to preserve it
against the things that would harm it.

165
00:09:33,930 --> 00:09:39,700
If harms bad, then preserving and
protecting against harm must be good.

166
00:09:39,700 --> 00:09:44,920
So we can mark that as E plus.
It's colorful cliffs, "colorful" sounds

167
00:09:44,920 --> 00:09:47,120
good, but of course colorful just means
it's colorful.

168
00:09:47,120 --> 00:09:52,400
Sweeping arches, broad and sweeping in
curves; well that sounds good.

169
00:09:52,400 --> 00:09:54,890
It sounds beautiful, the way he describes
it.

170
00:09:54,890 --> 00:09:58,020
And it surely is, as you can see in any
picture,

171
00:10:00,380 --> 00:10:04,040
but sweeping doesn't itself say its good
or beautiful, or so on.

172
00:10:04,040 --> 00:10:08,435
Another "extraordinary"; we already saw
extraordinary.

173
00:10:08,435 --> 00:10:12,750
So colorful, sweeping, and extraordinary,
they're

174
00:10:12,750 --> 00:10:14,920
certainly being used here by Redford,

175
00:10:14,920 --> 00:10:18,750
to suggest that these are good, but
they're not openly saying they're good.

176
00:10:18,750 --> 00:10:21,610
So we don't want to mark those as
evaluative words.

177
00:10:21,610 --> 00:10:25,650
But resources, that resources are things
that

178
00:10:25,650 --> 00:10:27,018
can give you abilities.

179
00:10:27,018 --> 00:10:30,042
When you have more resources, you are able
to do more.

180
00:10:30,042 --> 00:10:32,490
So abilities sounds like a good thing, and

181
00:10:32,490 --> 00:10:35,370
resources are the things that make you
more able.

182
00:10:35,370 --> 00:10:38,466
They give you more freedom and more power,
so at least,

183
00:10:38,466 --> 00:10:41,490
many people would want to mark that as an
E plus word.

184
00:10:41,490 --> 00:10:44,010
Some of these are going to be
questionable.

185
00:10:44,010 --> 00:10:47,394
They're not as obvious as others, so I'm
suggesting one

186
00:10:47,394 --> 00:10:51,498
way of interpreting this passage, and I
hope you're following along?

187
00:10:51,498 --> 00:10:53,658
But if you have some questions about

188
00:10:53,658 --> 00:10:56,466
the particular cases, that's going to be
natural.

189
00:10:56,466 --> 00:11:00,096
It's partly because our language is not
totally precise, okay?

190
00:11:00,096 --> 00:11:04,805
Large scale mineral development; well
that's not bad if it's done

191
00:11:04,805 --> 00:11:09,200
in the right places, so I don't think
that's evaluative either.

192
00:11:09,200 --> 00:11:12,450
Didn't the president say that he was
saving these lands?

193
00:11:12,450 --> 00:11:16,010
Well didn't the president say, that he was
saving these lands?

194
00:11:16,010 --> 00:11:16,550
Well that

195
00:11:16,550 --> 00:11:22,240
suggests that he's assuring you, that in
fact, he was saving these lands.

196
00:11:23,780 --> 00:11:27,570
He should know whether that's what he was
doing, since after all, that's what he,

197
00:11:27,570 --> 00:11:31,560
you know, did himself, so he should be
able to say what he did and why.

198
00:11:31,560 --> 00:11:36,880
Didn't the president say he was saving?
Saving, could be marked as E plus

199
00:11:36,880 --> 00:11:41,880
just like protect and preserve, because it
saves

200
00:11:41,880 --> 00:11:47,410
it from something bad happening; these
lands from mining companies for our

201
00:11:47,410 --> 00:11:53,385
children and grandchildren.
Now, what about the word "for"?

202
00:11:53,385 --> 00:11:57,530
This explains again, why he's saving these
lands.

203
00:11:57,530 --> 00:12:01,510
He's saving them for our grandchildren and
children.

204
00:12:01,510 --> 00:12:06,510
That means, the reason for saving these
lands, is to benefit our

205
00:12:06,510 --> 00:12:08,100
grandchildren and children.

206
00:12:08,100 --> 00:12:12,980
So "for" is going to be a reason or a
premise marker.

207
00:12:12,980 --> 00:12:16,330
It marks the reason or premise that
justifies saving the lands, and

208
00:12:16,330 --> 00:12:20,134
explains why in fact the president did
want to save the lands, okay?

209
00:12:20,134 --> 00:12:26,210
So now we're through with paragraph two
and I hope you're kind of getting

210
00:12:26,210 --> 00:12:30,814
the feel for how to do close analysis.
And so what I want to do now,

211
00:12:30,814 --> 00:12:35,480
is give you a chance to practice the skill
on your own.

212
00:12:35,480 --> 00:12:38,990
We'll put up paragraph three.

213
00:12:38,990 --> 00:12:45,390
Mark certain words, and your task in the
exercise will be, to put the right

214
00:12:45,390 --> 00:12:51,725
letter next to, or to indicate, the
function of that word in the paragraph.

215
00:12:51,725 --> 00:12:55,910
And the letter you put, should be: either
R or P for

216
00:12:57,210 --> 00:13:02,250
premise marker, C for conclusion marker, A
for assuring, G for guarding,

217
00:13:02,250 --> 00:13:07,070
D for discounting, E plus/ E minus for
positive and negative evaluation.

218
00:13:07,070 --> 00:13:11,140
And you can go through the third paragraph
yourself, in the exercise.

