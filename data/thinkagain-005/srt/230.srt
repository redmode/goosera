1
00:00:03,010 --> 00:00:03,558
Welcome back.

2
00:00:03,558 --> 00:00:07,160
In the previous lecture, we saw a
definition of

3
00:00:07,160 --> 00:00:12,410
argument as a connected series of
sentences, statements or propositions.

4
00:00:12,410 --> 00:00:15,560
Where some of those sentences, statements,
propositions are

5
00:00:15,560 --> 00:00:18,300
premises and one of them is a conclusion.

6
00:00:18,300 --> 00:00:24,340
And the premises are intended to give some
kind of reason for the conclusion.

7
00:00:24,340 --> 00:00:28,180
In this lecture we're going to look

8
00:00:28,180 --> 00:00:31,260
at the purposes for, which people give
arguments because

9
00:00:31,260 --> 00:00:36,700
the purposes are crucial in determining
what an object is.

10
00:00:36,700 --> 00:00:41,675
Take for example an artifact that you
might find in an archeological site.

11
00:00:41,675 --> 00:00:45,910
You won't be able to figure out whether
it's a really big screwdriver or a really

12
00:00:45,910 --> 00:00:53,390
small spatula unless you know whether the
people who used it intended it to screw

13
00:00:53,390 --> 00:00:56,070
screws or to pick up food that they were
cooking.

14
00:00:57,310 --> 00:01:02,360
So, to understand arguments we need to
understand the purposes for arguments.

15
00:01:02,360 --> 00:01:05,330
And that means, why does somebody bother
to give an

16
00:01:05,330 --> 00:01:09,690
argument instead of just asserting the
conclusion without an argument?

17
00:01:09,690 --> 00:01:10,816
Well, just think about it.

18
00:01:10,816 --> 00:01:12,740
If you went to a used car lot and

19
00:01:12,740 --> 00:01:15,440
the salesman said, you ought to buy that
Mustang.

20
00:01:16,870 --> 00:01:18,320
Would that convince you?

21
00:01:18,320 --> 00:01:19,890
Not a chance.

22
00:01:19,890 --> 00:01:22,510
But if the salesman said you ought to buy
that Mustang

23
00:01:22,510 --> 00:01:27,070
because it looks really cool and it goes
really fast.

24
00:01:27,070 --> 00:01:31,910
Or maybe it has great gas mileage or
whatever, and gives you

25
00:01:31,910 --> 00:01:37,530
a series of reasons, then you might be
convinced to buy the Mustang.

26
00:01:37,530 --> 00:01:40,250
So, that's one purpose of arguments to try
to

27
00:01:40,250 --> 00:01:43,400
convince you to do things or believe
things that

28
00:01:43,400 --> 00:01:46,820
you wouldn't otherwise do or believe.

29
00:01:46,820 --> 00:01:49,295
So, this purpose is persuading or
convincing.

30
00:01:49,295 --> 00:01:51,980
And if you think about it what the
salesman's trying

31
00:01:51,980 --> 00:01:55,060
to do is he's trying to change your mental
states.

32
00:01:55,060 --> 00:01:58,330
He's trying to make you believe something
that you

33
00:01:58,330 --> 00:02:01,700
didn't believe or do something that you
didn't do.

34
00:02:01,700 --> 00:02:04,959
So he's trying to bring about an effect in
the world.

35
00:02:06,050 --> 00:02:08,210
But that's just one purpose of arguments.

36
00:02:08,210 --> 00:02:08,480
We don't

37
00:02:08,480 --> 00:02:09,795
always act like salesmen.

38
00:02:09,795 --> 00:02:14,980
Sometimes, instead of trying to change
people's beliefs, we're simply trying

39
00:02:14,980 --> 00:02:18,570
to give them a reason for their belief or
for our belief.

40
00:02:20,800 --> 00:02:23,420
And to give them a reason is not
necessarily

41
00:02:23,420 --> 00:02:26,440
to convince them or persuade them or
change their beliefs.

42
00:02:28,060 --> 00:02:29,580
When we're simply trying to give them a
reason

43
00:02:29,580 --> 00:02:32,960
to believe the conclusion we're going to
call that justification.

44
00:02:32,960 --> 00:02:35,830
So, imagine that your friend, you're not a
salesman, you're a

45
00:02:35,830 --> 00:02:40,380
friend, imagine that your friend is
thinking about buying a car.

46
00:02:40,380 --> 00:02:43,086
He doesn't know which one to buy.
You might say.

47
00:02:43,086 --> 00:02:46,030
Well, I think you ought to buy the
Mustang.

48
00:02:46,030 --> 00:02:50,400
Because it looks really good and it goes
really fast and its

49
00:02:50,400 --> 00:02:54,750
actually got pretty good gas mileage and
its quiet reliable or whatever.

50
00:02:56,240 --> 00:02:59,289
You're not necessarily trying to convince
her to buy that car.

51
00:02:59,289 --> 00:03:01,360
It'd be fine with you if she bought any

52
00:03:01,360 --> 00:03:03,960
car she wanted, any car that would make
her happy.

53
00:03:03,960 --> 00:03:06,990
You're trying to talk about the reasons
for buying

54
00:03:06,990 --> 00:03:09,060
the car so that you can make your own
decision.

55
00:03:09,060 --> 00:03:11,770
And that says you're trying to justify

56
00:03:11,770 --> 00:03:15,660
that decision or that belief that Mustang
is the best car for her to buy.

57
00:03:16,790 --> 00:03:20,850
And not necessarily to convince her or
persuade her, if she

58
00:03:20,850 --> 00:03:24,850
comes up with great reason to the contrary
you're perfect happy.

59
00:03:24,850 --> 00:03:26,960
Whereas the salesman wouldn't be.

60
00:03:28,260 --> 00:03:32,390
But notice, that you might give exactly
the same reasons that the salesman did.

61
00:03:32,390 --> 00:03:34,790
Exactly the same argument that the
salesman did.

62
00:03:34,790 --> 00:03:36,770
The difference lies in the purpose

63
00:03:36,770 --> 00:03:41,250
because the salesman is trying to convince
her to change her beliefs and actions, but

64
00:03:41,250 --> 00:03:47,620
your goal with your friend is to discuss
the reasons for her decision or action.

65
00:03:47,620 --> 00:03:49,910
So, you're thinking about justification
and

66
00:03:49,910 --> 00:03:52,920
the salesman was thinking about
persuasion.

67
00:03:52,920 --> 00:03:54,680
Now, it really matters whether your goal
is

68
00:03:54,680 --> 00:03:57,430
justification or persuasion because
there's a big difference here.

69
00:03:58,520 --> 00:04:02,640
If you're trying to justify your friend's
belief or

70
00:04:02,640 --> 00:04:04,260
your friend's action.

71
00:04:04,260 --> 00:04:06,150
Then you try to give her good reasons, the

72
00:04:06,150 --> 00:04:10,850
salesman can convince her or persuade her
with bad reasons.

73
00:04:10,850 --> 00:04:12,840
So, it doesn't matter to his purposes

74
00:04:12,840 --> 00:04:14,520
whether the arguments that he gives are
any

75
00:04:14,520 --> 00:04:19,350
good or bad, as long as they work to
affect that change in the world.

76
00:04:19,350 --> 00:04:23,030
Whereas you care about whether your
arguments and your reasons

77
00:04:23,030 --> 00:04:27,880
are good reasons or arguments because,
you're trying to justify that

78
00:04:27,880 --> 00:04:29,150
belief or that action.

79
00:04:30,250 --> 00:04:33,272
And ,of course, people can try to do all
of these things at once.

80
00:04:33,272 --> 00:04:36,620
They can mix them together in various
ways.

81
00:04:36,620 --> 00:04:38,910
And that can get complicated.

82
00:04:38,910 --> 00:04:43,805
So, when someone gives you an argument,
you need to ask a series of questions.

83
00:04:43,805 --> 00:04:49,190
The first thing you need to ask is, is
this person trying to change my mind?

84
00:04:49,190 --> 00:04:53,010
Or change my behavior?
If so, then their

85
00:04:53,010 --> 00:04:56,700
goal is persuasion or to convince you.

86
00:04:58,000 --> 00:05:03,210
Then you need to ask, are they trying to
give reasons to change my mind?

87
00:05:03,210 --> 00:05:07,250
Or for believing, if I already believed
it.

88
00:05:07,250 --> 00:05:11,410
Well, if they're doing that then their
goal is justification.

89
00:05:11,410 --> 00:05:14,255
And if you go down that series of
questions

90
00:05:14,255 --> 00:05:18,070
you'll be able to understand what the
purpose of giving

91
00:05:18,070 --> 00:05:21,960
the argument is, at least for this range
of cases.

92
00:05:21,960 --> 00:05:26,740
So, let's do a few exercises just to make
sure that you understand justification

93
00:05:26,740 --> 00:05:31,250
before we go on to the next purpose of
argument, which will be explanation.

