1
00:00:03,520 --> 00:00:07,240
Last time we discussed what arguments are
for, their purposes.

2
00:00:07,240 --> 00:00:10,200
We saw that arguments have at least three
purposes.

3
00:00:10,200 --> 00:00:14,300
Namely, persuasion, justification, and
explanation.

4
00:00:14,300 --> 00:00:18,940
We also saw that one way to explain
something is to cite its purpose.

5
00:00:18,940 --> 00:00:22,760
So we can understand why Joe went to the
store by

6
00:00:22,760 --> 00:00:25,920
seeing that he went to the store because
he wanted some milk.

7
00:00:25,920 --> 00:00:28,210
So his purpose was to get milk.

8
00:00:28,210 --> 00:00:31,270
Similarly, we can understand arguments by
looking at their purposes.

9
00:00:31,270 --> 00:00:32,704
And that's what we did last time.

10
00:00:32,704 --> 00:00:36,930
But this time we're looking at a different
kind of explanation.

11
00:00:36,930 --> 00:00:39,600
And as we saw, one way to explain things
is to look at the material.

12
00:00:39,600 --> 00:00:44,400
So you want to understand why a MacBook
Air is so light?

13
00:00:44,400 --> 00:00:46,380
The answer is, it's made out of aluminum.

14
00:00:47,750 --> 00:00:50,450
Similarly, if we want to understand
arguments, we're

15
00:00:50,450 --> 00:00:53,260
going to gain understanding by looking
carefully at the

16
00:00:53,260 --> 00:00:54,670
material that they're made out of.

17
00:00:54,670 --> 00:00:59,280
And we saw that arguments are sets of
sentences, statements

18
00:00:59,280 --> 00:01:03,600
and propositions, so that means they're
made out of language.

19
00:01:03,600 --> 00:01:06,940
So in this lecture and the next few, we're
going to look

20
00:01:06,940 --> 00:01:11,210
at the nature of language in order to
better understand arguments.

21
00:01:11,210 --> 00:01:14,150
So, if we know that arguments are made out
of

22
00:01:14,150 --> 00:01:18,930
language, we know that the only creatures
who can give arguments

23
00:01:18,930 --> 00:01:20,155
are ones that can use language.

24
00:01:20,155 --> 00:01:25,170
Now some people think that other animals
can use language and

25
00:01:25,170 --> 00:01:28,590
there's a minimal kind of language that
other animals can use.

26
00:01:28,590 --> 00:01:33,390
But other animals cannot use language
that's complex enough to make arguments.

27
00:01:33,390 --> 00:01:41,366
It might seem that there's some
exceptions, here's one possibility.

28
00:01:41,366 --> 00:01:41,371
[FOREIGN].

29
00:01:41,371 --> 00:01:48,151
But no matter what it sounds

30
00:01:48,151 --> 00:01:53,720
like, this goat is not really arguing.
Maybe he's fighting.

31
00:01:53,720 --> 00:01:57,185
Maybe he's fending off what he takes to be
an enemy.

32
00:01:57,185 --> 00:01:59,330
But he's not arguing.

33
00:01:59,330 --> 00:02:02,800
So if other animals can use language, we
can't define humans

34
00:02:02,800 --> 00:02:06,470
as the animal that talks, but we can
define humans as the

35
00:02:06,470 --> 00:02:08,230
animal that argues.

36
00:02:08,230 --> 00:02:11,520
Or as Aristotle said the rational animal,
the

37
00:02:11,520 --> 00:02:15,060
animal that reasons because other animals
don't do that.

38
00:02:15,060 --> 00:02:18,710
Humans are the only one that argues and
reasons in this sense.

39
00:02:19,950 --> 00:02:22,500
So, we could understand humans and

40
00:02:22,500 --> 00:02:25,140
arguments better if we understand language
better.

41
00:02:25,140 --> 00:02:28,750
Now I can't tell you everything that needs
to be said about language,

42
00:02:28,750 --> 00:02:31,570
you'd need to take a linguistics course
for that and I recommend that

43
00:02:31,570 --> 00:02:33,800
you try one because it's very interesting.

44
00:02:33,800 --> 00:02:39,330
But here I'm only going to be able to make
four basic points about language.

45
00:02:39,330 --> 00:02:43,408
First of all, language is important.
Second, it's conventional.

46
00:02:43,408 --> 00:02:47,930
Third, it's representational, and fourth,
it's social.

47
00:02:47,930 --> 00:02:51,975
That should at least get us going in
understanding what arguments are made of.

48
00:02:51,975 --> 00:02:56,570
First, language is important.
It would

49
00:02:56,570 --> 00:03:00,640
be extremely difficult to live life
without language.

50
00:03:00,640 --> 00:03:02,510
Just try to imagine.

51
00:03:02,510 --> 00:03:03,380
What it would be like.

52
00:03:03,380 --> 00:03:08,570
It's really hard to imagine, but think
about someone like Helen Keller.

53
00:03:08,570 --> 00:03:11,610
Who was born able to see and hear, but

54
00:03:11,610 --> 00:03:15,390
very shortly thereafter, lost her ability
to see and hear.

55
00:03:16,590 --> 00:03:22,600
It was only much later in life that she
gained the ability to use language because

56
00:03:22,600 --> 00:03:25,150
she never had that in her early years.

57
00:03:25,150 --> 00:03:28,820
And when she gained that ability, she was
amazed.

58
00:03:28,820 --> 00:03:28,824
[SOUND].

59
00:03:28,824 --> 00:03:35,290
>> W-A-T-E-R, water.

60
00:03:35,290 --> 00:03:41,401
It has a name.
W-A-T.

61
00:03:41,401 --> 00:03:41,401
[SOUND]

62
00:03:41,401 --> 00:03:41,403
.

63
00:03:41,403 --> 00:03:46,634
>> When Helen Keller gained the ability
to use language, and to communicate.

64
00:03:46,634 --> 00:03:50,500
she didn't become able to see or hear, she
still

65
00:03:50,500 --> 00:03:53,310
couldn't see or hear but she could do
amazing things.

66
00:03:53,310 --> 00:03:55,355
She went around the country giving

67
00:03:55,355 --> 00:03:58,620
presentations, she graduated from Radcliff
College.

68
00:03:58,620 --> 00:04:01,280
All of that was made available to her

69
00:04:01,280 --> 00:04:05,340
simply by adding language and
communication to her life.

70
00:04:05,340 --> 00:04:07,290
So language is extremely useful.

71
00:04:07,290 --> 00:04:09,600
And that explains why it's all around us.

72
00:04:09,600 --> 00:04:14,690
Just imagine walking down the streets of a
city, and all the signs that you'd see.

73
00:04:14,690 --> 00:04:16,740
You'd just see words here, there, and
everywhere.

74
00:04:18,560 --> 00:04:19,480
And now we have a mystery.

75
00:04:19,480 --> 00:04:23,040
If we're not paying attention to language,
then how can

76
00:04:23,040 --> 00:04:26,157
we use it so well to achieve so many
purposes?

77
00:04:26,157 --> 00:04:29,380
And the answer to that lies in the second

78
00:04:29,380 --> 00:04:31,585
general feature of language that I want to
talk about.

79
00:04:31,585 --> 00:04:35,980
Namely, language is conventional.
But what's a convention?

80
00:04:37,390 --> 00:04:41,200
Remember that in the United States people
drive on the right hand side of the road.

81
00:04:41,200 --> 00:04:42,269
That's our convention.

82
00:04:43,330 --> 00:04:44,100
But what does that mean?

83
00:04:44,100 --> 00:04:46,610
It means that there's a general pattern

84
00:04:46,610 --> 00:04:49,510
of behavior That most people throughout
society

85
00:04:49,510 --> 00:04:54,675
obey on a regular basis and they criticize
people who deviate from that pattern.

86
00:04:54,675 --> 00:04:57,350
And the same applies to language.

87
00:04:57,350 --> 00:05:00,970
We have certain patterns of using words in
certain ways

88
00:05:00,970 --> 00:05:04,210
and when people deviate from those
patterns, we criticize them.

89
00:05:04,210 --> 00:05:07,245
We say they're misspeaking or it's
ungrammatical.

90
00:05:07,245 --> 00:05:10,280
Of course, conventions can vary.

91
00:05:10,280 --> 00:05:12,580
Everybody knows that there are many
countries around

92
00:05:12,580 --> 00:05:14,670
the world where people don't drive on the

93
00:05:14,670 --> 00:05:17,155
right hand side of the road, they drive on
the left hand side of the road.

94
00:05:17,155 --> 00:05:20,360
The United Kingdom's one of them, but
there are lots more.

95
00:05:21,500 --> 00:05:22,810
And the same applies to language.

96
00:05:22,810 --> 00:05:25,410
You could have the same word that's used

97
00:05:25,410 --> 00:05:28,480
to mean very different things in different
languages.

98
00:05:28,480 --> 00:05:30,810
Most notorious example is football.

99
00:05:30,810 --> 00:05:34,500
In the United States, it's used to refer
to American football.

100
00:05:35,550 --> 00:05:37,180
Whereas, in the rest of the world it

101
00:05:37,180 --> 00:05:39,826
is used to refer to what Americans call
soccer.

102
00:05:39,826 --> 00:05:43,400
And people in the rest of the world think
that America is kind of

103
00:05:43,400 --> 00:05:48,140
silly because you don't use your feet on
the ball except for punting and

104
00:05:48,140 --> 00:05:50,100
place kicking in football.

105
00:05:50,100 --> 00:05:53,590
But whether it makes sense or not, the
point here is simply that

106
00:05:53,590 --> 00:05:58,750
the conventions can vary from one part of
the world to the other.

107
00:05:58,750 --> 00:06:01,728
And of course you can do that with any
word, you

108
00:06:01,728 --> 00:06:06,516
could, in English use the word money to
refer to socks.

109
00:06:06,516 --> 00:06:11,213
at least the English language could've
done that, it didn't but it could have.

110
00:06:11,213 --> 00:06:13,351
So in this way, conventions seem

111
00:06:13,351 --> 00:06:17,225
to be kind of arbitrary, it could've been
very different.

112
00:06:17,225 --> 00:06:20,170
But language is far from completely
arbitrary,

113
00:06:20,170 --> 00:06:24,510
because the conventions of language have
limits.

114
00:06:24,510 --> 00:06:26,750
And two of these limits that I want to
emphasize

115
00:06:26,750 --> 00:06:31,190
come from the fact that language is also
representational and social.

116
00:06:31,190 --> 00:06:32,790
So first, language is representational.

117
00:06:34,080 --> 00:06:38,710
When we use language we're often trying to
refer to objects in the world and describe

118
00:06:38,710 --> 00:06:39,775
facts in the world.

119
00:06:39,775 --> 00:06:41,830
And you can't change those objects or

120
00:06:41,830 --> 00:06:44,342
those facts merely by changing your
language.

121
00:06:45,350 --> 00:06:50,080
One good story to illustrate this is about
the young Lincoln.

122
00:06:50,080 --> 00:06:54,470
When he was a lawyer, he supposedly
examined a witness during a

123
00:06:54,470 --> 00:06:58,530
trial and he said, okay how many legs does
a horse have?

124
00:06:58,530 --> 00:06:59,800
And the witness said four.

125
00:07:00,950 --> 00:07:03,930
And then Lincoln said, well if we call a
tail

126
00:07:03,930 --> 00:07:07,770
a leg.
Then how many legs does a horse have?

127
00:07:07,770 --> 00:07:13,170
And the witness said, well, then, I
suppose the horse would have five legs.

128
00:07:13,170 --> 00:07:15,190
And Lincoln said absolutely not, that's
wrong.

129
00:07:15,190 --> 00:07:18,180
Calling a tail a leg doesn't make it a
leg.

130
00:07:18,180 --> 00:07:22,100
And the point of the story, whether it's
true historically or

131
00:07:22,100 --> 00:07:27,300
not, is that language cannot change the
facts of the world.

132
00:07:27,300 --> 00:07:29,040
It can't make horses have five

133
00:07:29,040 --> 00:07:35,650
legs if you merely change your language.
Here's another example, suppose that

134
00:07:35,650 --> 00:07:39,590
you don't have much money but you happen
to have a lot of socks in your drawer.

135
00:07:40,940 --> 00:07:45,090
Well, you could say, I'm going to use the
word money to refer

136
00:07:45,090 --> 00:07:48,390
to socks, and now all of a sudden, I've
got lots of money.

137
00:07:48,390 --> 00:07:51,880
I'm not poor anymore.
It ain't going to work.

138
00:07:51,880 --> 00:07:54,160
And that's because language,

139
00:07:54,160 --> 00:07:59,460
again, can't change your financial
situation because that's a fact about

140
00:07:59,460 --> 00:08:04,615
the world, not about how you're using the
word socks, or the word money.

141
00:08:04,615 --> 00:08:07,360
And the other limit on the conventions of

142
00:08:07,360 --> 00:08:10,330
language, comes from the fact that
language is social.

143
00:08:10,330 --> 00:08:16,370
Sure, sometimes we talk to ourselves and
use language to write things down.

144
00:08:16,370 --> 00:08:19,250
Write notes to ourselves, for example,
without other

145
00:08:19,250 --> 00:08:20,042
people around.

146
00:08:20,042 --> 00:08:24,280
But basically language evolved because of
its social function.

147
00:08:24,280 --> 00:08:27,520
And what that means is that there's a

148
00:08:27,520 --> 00:08:32,190
point in following the conventions of the
language.

149
00:08:32,190 --> 00:08:35,315
as shared by the rest of the society that
speaks that language.

150
00:08:35,315 --> 00:08:37,120
You know, I've always thought that it

151
00:08:37,120 --> 00:08:40,220
was kind of silly that grapefruits are
called grapefruits.

152
00:08:40,220 --> 00:08:44,360
Sure they're fruits, but they don't look
like grapes at all.

153
00:08:44,360 --> 00:08:45,840
They look more like lemons.

154
00:08:45,840 --> 00:08:50,570
They're like really big lemons and that's
why

155
00:08:50,570 --> 00:08:53,560
I think they ought to be called mega
lemons.

156
00:08:54,600 --> 00:08:59,970
But if I went to a restaurant and I wanted
to order grapefruit juice, so I'd turn to

157
00:08:59,970 --> 00:09:04,430
the the service person and said I'd like
some

158
00:09:04,430 --> 00:09:08,670
mega lemon juice, I probably wouldn't get
what I wanted.

159
00:09:08,670 --> 00:09:09,800
And so even

160
00:09:09,800 --> 00:09:12,010
if I think the language is not using the

161
00:09:12,010 --> 00:09:15,240
right conventions, there's a point in
following the conventions

162
00:09:15,240 --> 00:09:17,570
of the language in order to be able to

163
00:09:17,570 --> 00:09:20,070
communicate with other people and get what
I want.

164
00:09:20,070 --> 00:09:25,480
And again, the great philosophers Monty
Python saw this very well when

165
00:09:25,480 --> 00:09:29,615
they produced their little clip called,
The Man Who Speaks Only in Anagrams.

166
00:09:29,615 --> 00:09:34,950
>> Our first guest in the studio tonight
is a man who talks entirely in anagrams.

167
00:09:34,950 --> 00:09:34,950
>>

168
00:09:34,950 --> 00:09:34,950
[UNKNOWN].

169
00:09:34,950 --> 00:09:39,800
>> Do you enjoy this?
>> I stom certainly od.

170
00:09:39,800 --> 00:09:41,206
Revy chum so.

171
00:09:41,206 --> 00:09:42,971
>> And, what's your name?
>> Hamrag.

172
00:09:42,971 --> 00:09:45,880
Hamrag Yatlerot.
>> So, the point is obvious.

173
00:09:45,880 --> 00:09:47,670
Namely, language is shared.

174
00:09:47,670 --> 00:09:52,530
And once it's shared, then it makes sense
to actually follow the conventions of

175
00:09:52,530 --> 00:10:00,480
society even if you don't like them.
Overall then, language is important and

176
00:10:00,480 --> 00:10:06,360
it's conventional in ways that might seem
arbitrary but actually is limited in

177
00:10:06,360 --> 00:10:12,010
important ways by the fact that language
is also representational and social.

178
00:10:12,010 --> 00:10:14,430
But it's kind of cheap to say language is
conventional.

179
00:10:14,430 --> 00:10:15,950
Which are the conventions?

180
00:10:15,950 --> 00:10:19,360
Which are the rules that language follows?

181
00:10:19,360 --> 00:10:22,460
And this is actually extremely complex
because language

182
00:10:22,460 --> 00:10:25,380
follows rules or conventions at many
different levels.

183
00:10:25,380 --> 00:10:27,350
Just take a real simple example, you walk
into

184
00:10:27,350 --> 00:10:30,190
a pizza shop and you say give me
pepperoni.

185
00:10:31,380 --> 00:10:37,150
Well the person then fixes a pepperoni
pizza and you pay for it.

186
00:10:37,150 --> 00:10:40,590
But how did that work, that you said give
me pepperoni?

187
00:10:40,590 --> 00:10:45,640
Well, first of all notice that you had to
use words that were meaningful to

188
00:10:45,640 --> 00:10:50,630
the person you were speaking to.
Gimme wasn't a word in English a long

189
00:10:50,630 --> 00:10:54,270
time ago, but this person understands
gimme as

190
00:10:54,270 --> 00:10:57,600
a word, and therefore they can understand
it.

191
00:10:57,600 --> 00:11:00,770
But in addition to those semantic
constraints.

192
00:11:00,770 --> 00:11:02,790
You also have to have physical production
constraints.

193
00:11:02,790 --> 00:11:04,430
You have to say it loud enough.

194
00:11:04,430 --> 00:11:07,930
If the pizza shop is really noisy, then
you have to speak pretty

195
00:11:07,930 --> 00:11:13,430
loudly to get the person behind the
counter to understand what you're saying.

196
00:11:13,430 --> 00:11:15,740
You also have to put

197
00:11:15,740 --> 00:11:17,660
the words in the right order.

198
00:11:17,660 --> 00:11:23,280
If instead of saying, gimme a pepperoni
pizza, you said pizza a gimme pepperoni.

199
00:11:23,280 --> 00:11:26,047
They might not understand at all what
you're saying.

200
00:11:26,047 --> 00:11:30,310
So there are structural combination rules
that you have to follow as well.

201
00:11:30,310 --> 00:11:32,730
And there are also etiquette rules.

202
00:11:32,730 --> 00:11:35,418
In some pizza places if you just said,
gimme

203
00:11:35,418 --> 00:11:39,530
pepperoni, the waiter might say, well,
forget it sir.

204
00:11:39,530 --> 00:11:40,795
I don't serve such

205
00:11:40,795 --> 00:11:42,530
impolite people.

206
00:11:42,530 --> 00:11:46,210
I certainly would say that to my son if my
son said, give me pepperoni.

207
00:11:46,210 --> 00:11:49,830
I wouldn't get him a pizza.
I'd say you need to ask me properly.

208
00:11:49,830 --> 00:11:54,320
so rules of etiquette can also get in the
way of communication and cooperation.

209
00:11:54,320 --> 00:11:57,120
So language operates at all of these
levels.

210
00:11:57,120 --> 00:12:00,510
Physical production, semantics are the
meanings of words,

211
00:12:00,510 --> 00:12:03,550
syntax are the rules of grammar and
etiquette.

212
00:12:04,660 --> 00:12:06,060
Now all of this might seem obvious

213
00:12:06,060 --> 00:12:09,100
to you and, and probably should be obvious
to you.

214
00:12:09,100 --> 00:12:11,500
but the rules of the language are not
always obvious

215
00:12:11,500 --> 00:12:13,510
and that's what we're going to be learning
throughout this course.

216
00:12:13,510 --> 00:12:16,612
I'll start with a simple example what's
this?

217
00:12:16,612 --> 00:12:20,530
Well, that is a finger.
Okay?

218
00:12:20,530 --> 00:12:22,412
But what's this?

219
00:12:22,412 --> 00:12:25,270
Haaaaaaa.
That is a singer.

220
00:12:25,270 --> 00:12:27,018
This is not a finger.

221
00:12:27,018 --> 00:12:31,960
Haaaaaaa.
And that's not a singer.

222
00:12:31,960 --> 00:12:34,640
Why do we pronounce the words finger with
a

223
00:12:34,640 --> 00:12:39,390
hard g and the word singer with a soft g.

224
00:12:39,390 --> 00:12:41,880
That's a rule that we all follow, but

225
00:12:43,010 --> 00:12:45,610
very few people know the rule behind that
pronunciation.

226
00:12:45,610 --> 00:12:50,270
So, do you know the rule?

227
00:12:50,270 --> 00:12:58,951
Take a little while and think about it.
Have you

228
00:12:58,951 --> 00:13:04,044
got it

229
00:13:04,044 --> 00:13:11,180
yet?
Okay,

230
00:13:11,180 --> 00:13:15,440
I'll tell you the answer.
When a word ends in nger and is

231
00:13:15,440 --> 00:13:19,890
derived from a verb that ends in ng then
you get a soft g like singer.

232
00:13:19,890 --> 00:13:24,250
But when the word that ends in nger is not
derived

233
00:13:24,250 --> 00:13:26,890
from a verb that ends in ng then you get
either a

234
00:13:26,890 --> 00:13:32,070
hard g like finger or a kind of medium g
like plunger or danger.

235
00:13:34,300 --> 00:13:36,790
Now, when do you get that medium g or that
hard g?

236
00:13:36,790 --> 00:13:38,240
That's a trickier question.

237
00:13:38,240 --> 00:13:41,010
I don't know the answer to that one, which
shows that we

238
00:13:41,010 --> 00:13:46,980
can all use language according to rules
without knowing what the rules are.

239
00:13:46,980 --> 00:13:49,905
We don't have to be conscious of the rules
at all.

240
00:13:49,905 --> 00:13:51,320
And a lot of what we're going to be

241
00:13:51,320 --> 00:13:54,260
doing in this course, is looking behind
our language

242
00:13:54,260 --> 00:13:56,330
to try to figure out the rules that

243
00:13:56,330 --> 00:13:59,310
govern the way we use language, especially
when we're

244
00:13:59,310 --> 00:14:02,850
making arguments in order to better
understand what we're doing.

245
00:14:02,850 --> 00:14:05,790
Some of the answers we give will be
obvious, once you

246
00:14:05,790 --> 00:14:09,220
mention them, but I bet you hadn't thought
of them before.

