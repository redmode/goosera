1
00:00:00,050 --> 00:00:05,064
In the last lecture, we learned about
propositions of

2
00:00:05,064 --> 00:00:08,990
the forms that we labeled A, E, I, and O.

3
00:00:08,990 --> 00:00:12,912
We learned how to represent these
propositions verbally, and

4
00:00:12,912 --> 00:00:16,570
also how to represent them visually using
Venn diagrams.

5
00:00:17,600 --> 00:00:21,083
Today, I want to apply those lessons to
study a kind

6
00:00:21,083 --> 00:00:26,340
of argument that we're going to call an
immediate categorical inference.

7
00:00:26,340 --> 00:00:29,760
So, what's an immediate categorical
inference?

8
00:00:29,760 --> 00:00:33,168
An immediate categorical inference is an
inference with

9
00:00:33,168 --> 00:00:36,240
just one premise and of course, one
conclusion.

10
00:00:36,240 --> 00:00:39,936
Where Each of those two statements, both
the premise and

11
00:00:39,936 --> 00:00:43,000
the conclusion are of the form A, E, I, or
O.

12
00:00:43,000 --> 00:00:45,180
They needn't be of the same form.

13
00:00:45,180 --> 00:00:49,940
One of them could be, let's say, the E
form, and the other one of the O form.

14
00:00:51,730 --> 00:00:51,954
Or,

15
00:00:51,954 --> 00:00:52,520
whatever.

16
00:00:52,520 --> 00:00:58,040
They needn't be of the same form, but
they're each of one of those four forms.

17
00:01:00,080 --> 00:01:03,806
Now, these are very simple inferences but
we're

18
00:01:03,806 --> 00:01:07,532
going to study them today because we're
going to see

19
00:01:07,532 --> 00:01:10,205
how we can use Venn diagrams to show that

20
00:01:10,205 --> 00:01:14,180
some of these inferences are valid and
others aren't.

21
00:01:15,910 --> 00:01:20,128
Now, before we do that, let me say
something about the kinds of

22
00:01:20,128 --> 00:01:23,532
propositions that are involved in these
inferences,

23
00:01:23,532 --> 00:01:26,690
the A, E, I, and O propositions.

24
00:01:26,690 --> 00:01:29,351
Remember what those letters stand for.

25
00:01:29,351 --> 00:01:33,201
the A proposition is a proposition of the
form all Fs are Gs,

26
00:01:33,201 --> 00:01:38,020
all things that fall into one category
also fall into a second category.

27
00:01:38,020 --> 00:01:41,590
An E proposition is a proposition before
no Fs or Gs, nothing

28
00:01:41,590 --> 00:01:45,900
that falls into the first category also
falls into the second category.

29
00:01:45,900 --> 00:01:48,780
An I proposition, is a proposition of the
form some Fs

30
00:01:48,780 --> 00:01:51,916
are Gs, that's to say something that falls
into the first

31
00:01:51,916 --> 00:01:54,270
category falls into the second category.

32
00:01:54,270 --> 00:01:57,808
And an O proposition, is a proposition of
the form some Fs are not Gs,

33
00:01:57,808 --> 00:02:00,065
right, something that falls into the first

34
00:02:00,065 --> 00:02:03,000
category does not fall into the second
category.

35
00:02:03,000 --> 00:02:07,902
Now notice, in each of those propositions,
there's one category

36
00:02:07,902 --> 00:02:11,900
that I'm indicating by use of the
schematic letter F.

37
00:02:11,900 --> 00:02:17,120
There's one category that gets directly
modified by

38
00:02:17,120 --> 00:02:20,360
the quantifier, right.
The F category.

39
00:02:20,360 --> 00:02:22,568
Right, I'm talking about, in the A
proposition, I'm

40
00:02:22,568 --> 00:02:24,740
talking about all things that fall into
that category.

41
00:02:24,740 --> 00:02:29,490
In the E proposition, I'm talking about no
things that fall into that category.

42
00:02:29,490 --> 00:02:30,948
In the I or the O proposition I'm

43
00:02:30,948 --> 00:02:33,670
talking about, some things that fall into
that category.

44
00:02:33,670 --> 00:02:37,120
So there's a category that gets directly
modified by the quantifier.

45
00:02:37,120 --> 00:02:40,260
And then there's another category that
doesn't.

46
00:02:40,260 --> 00:02:42,290
Okay.
Let's introduce terminology

47
00:02:42,290 --> 00:02:44,320
to distinguish these two categories.

48
00:02:44,320 --> 00:02:48,890
We'll talk about the two categories as the
subject term and the predicate term.

49
00:02:50,340 --> 00:02:54,102
So the category that gets directly
modified by the quantifier, the

50
00:02:54,102 --> 00:02:58,180
F category, is what we're going to call
the subject term.

51
00:02:58,180 --> 00:03:00,930
And the category that doesn't get directly
modified by the

52
00:03:00,930 --> 00:03:03,832
quantifier is what we're going to call the
predicate term.

53
00:03:03,832 --> 00:03:07,542
You'll see in our later lecture about
syllogisms why this

54
00:03:07,542 --> 00:03:09,670
terminology is going to be useful.

55
00:03:09,670 --> 00:03:11,622
It's not going to be obvious today why

56
00:03:11,622 --> 00:03:14,395
it's useful but it will eventually become
useful.

57
00:03:14,395 --> 00:03:17,394
Okay.

58
00:03:17,394 --> 00:03:20,870
So, Fs are subject terms, Gs are predicate
terms,

59
00:03:20,870 --> 00:03:25,926
and immediate categorical inferences are
inferences that have one premise

60
00:03:25,926 --> 00:03:28,928
with a subject term and a predicate term,
and

61
00:03:28,928 --> 00:03:32,918
one conclusion with a subject term and a
predicate term.

62
00:03:32,918 --> 00:03:35,250
Alright.

63
00:03:35,250 --> 00:03:41,080
What kinds of immediate categorical
inferences are there?

64
00:03:41,080 --> 00:03:43,200
Well, there are lots, but the most common
kind

65
00:03:43,200 --> 00:03:46,580
of immediate categorical inference is one
that we'll call conversion.

66
00:03:46,580 --> 00:03:51,368
A conversion inference is an inference in
which the conclusion just

67
00:03:51,368 --> 00:03:56,680
switches the subject term and predicate
term as they occur in the premise.

68
00:03:58,010 --> 00:04:03,092
So if the premises of the form, no Fs or
Gs, let's

69
00:04:03,092 --> 00:04:08,230
say no Duke students are, NFL football
players.

70
00:04:08,230 --> 00:04:13,336
Then the conclusion would be of the form
no Gs

71
00:04:13,336 --> 00:04:18,439
or Fs.
No NFL football players are Duke

72
00:04:18,439 --> 00:04:23,379
students.
That's an example of an immediate

73
00:04:23,379 --> 00:04:27,040
categorical inference, that's a conversion
inference.

74
00:04:27,040 --> 00:04:31,730
It converts one E proposition to another E
proposition.

75
00:04:33,490 --> 00:04:37,522
You could do it for other forms of
propositions, so for instance, consider

76
00:04:37,522 --> 00:04:41,520
the conversion inference from an A
proposition to another A proposition.

77
00:04:41,520 --> 00:04:49,022
Consider the inference from all Duke
students are NFL football players,

78
00:04:49,022 --> 00:04:53,620
to the conclusion, all NFL football
players

79
00:04:53,620 --> 00:04:58,285
are Duke students.
Now notice the first of those two

80
00:04:58,285 --> 00:05:04,279
conversion inferences is plausibly valid
whereas the second one isn't.

81
00:05:06,940 --> 00:05:07,700
Why is that?

82
00:05:09,300 --> 00:05:14,905
Well we can see why that is if we use Venn
Diagrams to visually represent

83
00:05:14,905 --> 00:05:20,130
the information contained in the premises
of those inferences.

84
00:05:20,130 --> 00:05:24,720
More generally, I can say right now that
conversion inferences

85
00:05:24,720 --> 00:05:28,110
are valid for propositions of the E and I
form.

86
00:05:28,110 --> 00:05:31,974
But they're not valid for propositions of
the A or

87
00:05:31,974 --> 00:05:32,663
O form.

88
00:05:32,663 --> 00:05:38,570
And we can understand why that is using
Venn diagrams, right.

89
00:05:38,570 --> 00:05:43,442
So, why is it that conversion inferences
from the

90
00:05:43,442 --> 00:05:46,953
A form, to the A form are not valid?

91
00:05:46,953 --> 00:05:51,227
Why are they not valid, well let's
consider.

92
00:05:51,227 --> 00:05:53,919
What are you saying when you say all Fs
are Gs?

93
00:05:53,919 --> 00:05:57,073
Well, you're saying that whatever Fs there

94
00:05:57,073 --> 00:06:01,961
are they're not outside the G circle,
they're all in the G circle.

95
00:06:01,961 --> 00:06:06,079
So, you represent that information by
shading in the portion of the F

96
00:06:06,079 --> 00:06:08,493
circle that's outside the G circle, in

97
00:06:08,493 --> 00:06:11,409
order to indicate that there's nothing
there.

98
00:06:11,409 --> 00:06:15,357
Okay, but now once you've shaded in that
portion of the

99
00:06:15,357 --> 00:06:18,625
F circle does that tell you that all Gs
are Fs?

100
00:06:18,625 --> 00:06:22,178
No.
Of course it leaves open that

101
00:06:22,178 --> 00:06:26,280
all Gs are Fs.
I mean it could be that all the Gs there

102
00:06:26,280 --> 00:06:32,680
are, are in here, but shading in the
portion of the F circle that's outside

103
00:06:32,680 --> 00:06:38,420
the G circle also leaves it open that
there are plenty of Gs out here.

104
00:06:38,420 --> 00:06:42,372
So once we look at the Venn diagram from
the inference for all Fs

105
00:06:42,372 --> 00:06:46,856
are Gs to all Gs are Fs, we understand why
that inference is not valid,

106
00:06:46,856 --> 00:06:47,484
right.

107
00:06:49,080 --> 00:06:53,616
Shading in the portion of the F circle
outside the G circle leaves

108
00:06:53,616 --> 00:06:58,060
open that there are plenty of Gs that are
outside the F circle.

109
00:06:59,170 --> 00:07:01,030
Okay, what about the second kind of

110
00:07:01,030 --> 00:07:04,060
conversion inference, for propositions of
the E form.

111
00:07:04,060 --> 00:07:07,770
From no Fs or Gs, to no Gs or Fs.

112
00:07:07,770 --> 00:07:11,830
Well let's see, how would we represent no
Fs or Gs?

113
00:07:11,830 --> 00:07:14,140
Well we do that, by shading in the portion

114
00:07:14,140 --> 00:07:16,700
of the F circle that's inside the G
circle.

115
00:07:16,700 --> 00:07:21,360
Alright, so that shows that no Fs are in
the G circle.

116
00:07:21,360 --> 00:07:22,180
No Fs are Gs.

117
00:07:23,250 --> 00:07:26,562
But now, look, if no Fs are Gs, then we
can

118
00:07:26,562 --> 00:07:30,370
just read off from that that no Gs are Fs,
right.

119
00:07:30,370 --> 00:07:33,615
If there are any Gs, they can't be in the
area that's shaded in,

120
00:07:33,615 --> 00:07:37,450
right, because what it means to shade in
the area is that there's nothing there.

121
00:07:37,450 --> 00:07:41,370
So whatever Gs there are, they can't be
inside the F circle.

122
00:07:41,370 --> 00:07:43,380
They've got to be outside the F circle.

123
00:07:43,380 --> 00:07:49,850
So, if it's true that no Fs are Gs, then
it's got to be true that no Gs are Fs.

124
00:07:49,850 --> 00:07:55,360
The Venn diagram for no Fs are Gs shows us
that.

125
00:07:55,360 --> 00:07:57,592
So conversion inferences are invalid for A

126
00:07:57,592 --> 00:08:01,190
propositions, but they're valid for E
propositions.

127
00:08:01,190 --> 00:08:02,990
What about I propositions?

128
00:08:02,990 --> 00:08:03,480
Well let's see.

129
00:08:03,480 --> 00:08:06,050
I propositions have the form some Fs are
Gs.

130
00:08:06,050 --> 00:08:09,050
Well how would we represent some Fs are
Gs?

131
00:08:09,050 --> 00:08:12,740
We would indicate that there is something
that's inside the F circle.

132
00:08:12,740 --> 00:08:15,080
It's an F, but it's also a G.

133
00:08:15,080 --> 00:08:17,978
It's in the G circle.
We'd indicate it with an x.

134
00:08:17,978 --> 00:08:21,950
Okay, but notice, once we indicate.

135
00:08:21,950 --> 00:08:24,143
By means of that x, that there's something
inside

136
00:08:24,143 --> 00:08:26,150
the F circle that's also inside the G
circle.

137
00:08:26,150 --> 00:08:27,986
You can just read off from that,

138
00:08:27,986 --> 00:08:32,490
that there's something inside the G circle
that's also inside the F circle.

139
00:08:32,490 --> 00:08:35,630
In other words, some Gs are Fs.

140
00:08:35,630 --> 00:08:40,180
So, once again, from the Venn diagram, you
can see that if

141
00:08:40,180 --> 00:08:45,070
some Fs are Gs is true, then some Gs are
Fs has gotta be true.

142
00:08:45,070 --> 00:08:51,340
In other words, the conversion inference
for an I proposition has got to be valid.

143
00:08:53,155 --> 00:08:57,980
Okay, finally, what about O propositions?
Some Fs are not Gs.

144
00:08:57,980 --> 00:09:01,250
Well, let's see.
Some Fs are not Gs.

145
00:09:01,250 --> 00:09:02,758
How would you diagram that?

146
00:09:02,758 --> 00:09:08,020
Well, some Fs, so you want to use an x to
indicate that there is an F.

147
00:09:08,020 --> 00:09:11,300
But it's not a G so it's gotta be outside
the G circle.

148
00:09:11,300 --> 00:09:14,996
So there, you draw an x that's inside the
F circle to

149
00:09:14,996 --> 00:09:18,692
show that it is an F, but it's outside the
G circle,

150
00:09:18,692 --> 00:09:20,360
so it's not a G.

151
00:09:20,360 --> 00:09:23,940
That's how you represent that some Fs are
not Gs.

152
00:09:23,940 --> 00:09:29,175
Okay, now does that imply that some Gs are
not F?

153
00:09:29,175 --> 00:09:32,890
No.

154
00:09:32,890 --> 00:09:38,270
Maybe there are no Gs at all, for all that
you've just been told.

155
00:09:38,270 --> 00:09:40,675
Right, you're told that some Fs are not G,

156
00:09:40,675 --> 00:09:43,180
that doesn't tell you that there are any
Gs.

157
00:09:43,180 --> 00:09:44,093
Or, if there

158
00:09:44,093 --> 00:09:46,950
are any Gs, maybe all the Gs are in here.

159
00:09:46,950 --> 00:09:49,354
You don't know whether there are any Gs in
here.

160
00:09:51,170 --> 00:09:54,876
So, from the premise that some Fs are not

161
00:09:54,876 --> 00:09:58,940
Gs, you can't infer that some Gs are not
F.

162
00:09:58,940 --> 00:10:03,710
It doesn't follow that some Gs are not F,
and the Venn diagram shows us why.

163
00:10:03,710 --> 00:10:09,260
So, this last conversion inference on
propositions of the

164
00:10:09,260 --> 00:10:11,800
O form.
Is not valid.

165
00:10:11,800 --> 00:10:14,032
And once again, you can see that from

166
00:10:14,032 --> 00:10:17,040
the Venn diagram for propositions of the O
form.

167
00:10:18,250 --> 00:10:24,442
So, this shows how Venn diagrams, simple,
two-circle Venn diagrams, can be used

168
00:10:24,442 --> 00:10:30,445
to establish the validity or invalidity of
immediate categorical inferences.

169
00:10:30,445 --> 00:10:31,180
Okay.

170
00:10:31,180 --> 00:10:35,760
Next time, we'll consider inferences of
the more complicated kind.

171
00:10:35,760 --> 00:10:36,870
See you next time.

